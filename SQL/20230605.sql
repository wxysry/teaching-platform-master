-- 新增新平台模拟分组
CREATE TABLE `new_contest_group` (
                                 `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '新平台模拟分组id',
                                 `group_name` varchar(50) DEFAULT NULL COMMENT '分组名称',
                                 `deleted` tinyint(4) NOT NULL DEFAULT '0' COMMENT '是否删除',
                                 `update_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
                                 `create_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
                                 PRIMARY KEY (`id`),
                                 UNIQUE KEY `group_name` (`group_name`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;




-- 新增 用户-新平台模拟分组
CREATE TABLE `user_new_contest_group` (
                                      `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '用户新平台模拟分组权限',
                                      `user_id` int(11) NOT NULL COMMENT '用户id',
                                      `group_id` int(11) NOT NULL COMMENT '新平台模拟分组id',
                                      `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
                                      PRIMARY KEY (`id`),
                                      UNIQUE KEY `(user_id,group_id)` (`user_id`,`group_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;


-- 新增 新平台模拟题库
CREATE TABLE `new_ct_subject` (
                              `subject_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '题库id',
                              `subject_number` int(10) NOT NULL COMMENT '题库号',
                              `subject_name` varchar(100) DEFAULT NULL COMMENT '题库名称',
                              `group_id` int(11) NOT NULL COMMENT '题库所属分组',
                              `upload` tinyint(4) NOT NULL DEFAULT '0' COMMENT '是否已导入题库的规则表',
                              `spy_attachment` varchar(255) DEFAULT NULL COMMENT '间谍附件，第1年到第4年用|隔开',
                              `rule_attachment` varchar(255) DEFAULT NULL COMMENT '规则说明附件',
                              `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
                              PRIMARY KEY (`subject_id`),
                              UNIQUE KEY `subject_number` (`subject_number`),
                              KEY `create_time` (`create_time`),
                              KEY `group_id` (`group_id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;




-- 新增 新平台模拟-竞赛
CREATE TABLE `new_contest` (
                           `contest_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '竞赛id',
                           `subject_number` int(11) NOT NULL COMMENT '题库号',
                           `title` varchar(100) DEFAULT NULL COMMENT '竞赛标题',
                           `open_time_start` datetime DEFAULT NULL COMMENT '竞赛起始时间',
                           `open_time_end` datetime DEFAULT NULL COMMENT '竞赛结束时间',
                           `teacher_id` int(11) DEFAULT NULL COMMENT '教师id',
                           `is_publish` tinyint(4) NOT NULL DEFAULT '0' COMMENT '是否发布竞赛',
                           `student_num` int(11) DEFAULT NULL COMMENT '学生数量',
                           `publish_all` tinyint(4) DEFAULT '1' COMMENT '0表示向指定考生发布，1表示向全部考生发布',
                           `equity` mediumint(9) NOT NULL DEFAULT '0' COMMENT '初始权益，即初始资金',
                           `restart` tinyint(4) NOT NULL DEFAULT '1' COMMENT '是否可以重新经营',
                           `restore` tinyint(4) NOT NULL DEFAULT '1' COMMENT '是否可以还原本年',
                           `restore_season` tinyint(4) NOT NULL COMMENT '是否可以还原本季',
                           `restore_order` tinyint(4) NOT NULL DEFAULT '0' COMMENT '是否可以重新选单',
                           `export_score` tinyint(4) NOT NULL DEFAULT '0' COMMENT '是否可以导出成绩',
                           `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
                           PRIMARY KEY (`contest_id`),
                           KEY `subject_number` (`subject_number`),
                           KEY `teacher_id` (`teacher_id`),
                           KEY `open_time_end` (`open_time_end`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;