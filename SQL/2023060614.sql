-- 规则GZ_cs
CREATE TABLE `new_ct_gz_cs` (
                                `cs_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '规则id',
                                `subject_number` int(11) NOT NULL COMMENT 'subject_number',
                                `punish` int(11) DEFAULT NULL COMMENT '违约金比例',
                                `inventory_discount_product` int(11) DEFAULT NULL COMMENT '出售产成品',
                                `inventory_discount_material` int(11) DEFAULT NULL COMMENT '出售原材料',
                                `loan_ceiling` int(11) DEFAULT NULL COMMENT '贷款倍数',
                                `cash` int(11) DEFAULT NULL COMMENT '初始现金',
                                `overhaul` int(11) DEFAULT NULL COMMENT '管理费',
                                `min` int(11) DEFAULT NULL COMMENT '最小得单金额',
                                `tolerance` int(11) DEFAULT NULL COMMENT '得单公差',
                                `information` int(11) DEFAULT NULL COMMENT '信息费',
                                `emergence_multiple_material` int(11) DEFAULT NULL COMMENT '紧急采购原材料',
                                `emergence_multiple_product` int(11) DEFAULT NULL COMMENT '紧急采购产成品',
                                `income_tax` int(11) DEFAULT NULL COMMENT '所得税率',
                                `order_timeout` int(11) DEFAULT NULL COMMENT '选单时间',
                                `order_fix_time` int(11) DEFAULT NULL COMMENT '首单增加时间',
                                `auction_timeout` int(11) DEFAULT NULL COMMENT '拍卖时间',
                                `auction_thread` int(11) DEFAULT NULL COMMENT '拍卖张数',
                                `max_line` int(11) DEFAULT NULL COMMENT '生产线最大数量',
                                PRIMARY KEY (`cs_id`),
                                UNIQUE KEY `subject_number` (`subject_number`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;


-- 规则ct_gz_ad
CREATE TABLE `new_ct_gz_ad` (
                                `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '初始广告',
                                `subject_num` int(11) NOT NULL COMMENT '题号',
                                `year` tinyint(4) NOT NULL COMMENT '年份',
                                `group_num` varchar(100) DEFAULT NULL COMMENT '组号',
                                `local_p1` decimal(15,5) DEFAULT '0.00000' COMMENT '本地P1',
                                `regional_p1` decimal(15,5) DEFAULT '0.00000' COMMENT '区域P1',
                                `national_p1` decimal(15,5) DEFAULT '0.00000' COMMENT '国内P1',
                                `asian_p1` decimal(15,5) DEFAULT '0.00000' COMMENT '亚洲P1',
                                `international_p1` decimal(15,5) DEFAULT '0.00000' COMMENT '国际P1',
                                `local_p2` decimal(15,5) DEFAULT '0.00000' COMMENT '本地P2',
                                `regional_p2` decimal(15,5) DEFAULT '0.00000' COMMENT '区域P2',
                                `national_p2` decimal(15,5) DEFAULT '0.00000' COMMENT '国内P2',
                                `asian_p2` decimal(15,5) DEFAULT '0.00000' COMMENT '亚洲P2',
                                `international_p2` decimal(15,5) DEFAULT '0.00000' COMMENT '国际P2',
                                `local_p3` decimal(15,5) DEFAULT '0.00000' COMMENT '本地P3',
                                `regional_p3` decimal(15,5) DEFAULT '0.00000' COMMENT '区域P3',
                                `national_p3` decimal(15,5) DEFAULT '0.00000' COMMENT '国内P3',
                                `asian_p3` decimal(15,5) DEFAULT '0.00000' COMMENT '亚洲P3',
                                `international_p3` decimal(15,5) DEFAULT '0.00000' COMMENT '国际P3',
                                `local_p4` decimal(15,5) DEFAULT '0.00000' COMMENT '本地P4',
                                `regional_p4` decimal(15,5) DEFAULT '0.00000' COMMENT '区域P4',
                                `national_p4` decimal(15,5) DEFAULT '0.00000' COMMENT '国内P4',
                                `asian_p4` decimal(15,5) DEFAULT '0.00000' COMMENT '亚洲P4',
                                `international_p4` decimal(15,5) DEFAULT '0.00000' COMMENT '国际P4',
                                `local_p5` decimal(15,5) DEFAULT '0.00000' COMMENT '本地P5',
                                `regional_p5` decimal(15,5) DEFAULT '0.00000' COMMENT '区域P5',
                                `national_p5` decimal(15,5) DEFAULT '0.00000' COMMENT '国内P5',
                                `asian_p5` decimal(15,5) DEFAULT '0.00000' COMMENT '亚洲P5',
                                `international_p5` decimal(15,5) DEFAULT '0.00000' COMMENT '国际P5',
                                PRIMARY KEY (`id`),
                                UNIQUE KEY `subject_num` (`subject_num`,`year`,`group_num`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;


-- ISO规则表gz_ISO
CREATE TABLE `new_ct_gz_iso` (
                                 `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
                                 `ci_id` int(11) NOT NULL COMMENT 'ISO规则表 ISO类型',
                                 `subject_number` int(11) NOT NULL COMMENT '题库号',
                                 `ci_name` varchar(255) DEFAULT NULL COMMENT 'ISO名称',
                                 `ci_develop_fee` int(11) NOT NULL COMMENT '研发费用',
                                 `ci_develop_date` int(11) NOT NULL COMMENT '研发周期',
                                 PRIMARY KEY (`id`),
                                 UNIQUE KEY `ci_id` (`ci_id`,`subject_number`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;


-- 市场订单gz_order
CREATE TABLE `new_ct_gz_order` (
                                   `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
                                   `co_id` varchar(50) NOT NULL COMMENT '市场订单 订单编号',
                                   `subject_number` int(11) DEFAULT NULL COMMENT '题库号',
                                   `date` tinyint(4) NOT NULL COMMENT '年份',
                                   `quarterly` int(11) DEFAULT NULL COMMENT '季度',
                                   `cm_id` int(11) DEFAULT NULL COMMENT '市场',
                                   `cp_id` varchar(50) DEFAULT NULL COMMENT '产品',
                                   `num` int(11) DEFAULT NULL COMMENT '数量',
                                   `total_price` int(11) DEFAULT NULL COMMENT '总价',
                                   `delivery_date` tinyint(4) DEFAULT NULL COMMENT '交货期',
                                   `payment_date` tinyint(4) DEFAULT NULL COMMENT '账期',
                                   `ci_id` int(11) DEFAULT NULL COMMENT 'ISO',
                                   `xz_round` int(11) DEFAULT NULL COMMENT '选走轮次',
                                   `xz_group` varchar(50) DEFAULT NULL COMMENT '选走组号',
                                   PRIMARY KEY (`id`),
                                   UNIQUE KEY `co_id` (`co_id`,`subject_number`,`date`,`quarterly`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


-- 市场规则表gz_market
CREATE TABLE `new_ct_gz_market` (
                                    `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
                                    `cm_id` int(10) NOT NULL COMMENT '市场规则表编号',
                                    `subject_number` int(11) DEFAULT NULL COMMENT '题库号',
                                    `cm_name` varchar(255) NOT NULL COMMENT '市场名称',
                                    `cm_develop_fee` int(11) NOT NULL COMMENT '研发费用',
                                    `cm_develop_date` int(11) NOT NULL COMMENT '研发周期',
                                    PRIMARY KEY (`id`),
                                    UNIQUE KEY `cm_id` (`cm_id`,`subject_number`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;


-- 原料规则表gz_material
CREATE TABLE `new_ct_gz_material` (
                                      `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
                                      `cm_id` int(10) NOT NULL COMMENT '原料规则表编号',
                                      `subject_number` int(11) DEFAULT NULL COMMENT '题库号',
                                      `cn_name` varchar(255) DEFAULT NULL COMMENT '原料名称',
                                      `cm_lead_date` int(10) unsigned DEFAULT NULL COMMENT '采购周期',
                                      `cm_buy_fee` int(10) unsigned DEFAULT NULL COMMENT '费用',
                                      `cm_pay_date` int(10) unsigned DEFAULT NULL COMMENT '付款周期',
                                      PRIMARY KEY (`id`),
                                      UNIQUE KEY `cm_id` (`cm_id`,`subject_number`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;



-- 产品规则表gz_product
CREATE TABLE `new_ct_gz_product` (
                                     `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
                                     `cp_id` int(11) NOT NULL COMMENT '产品规则表编号',
                                     `subject_number` int(11) DEFAULT NULL COMMENT '题库号',
                                     `cp_name` varchar(255) DEFAULT NULL COMMENT '名称',
                                     `cp_processing_fee` int(11) NOT NULL COMMENT '开发费用',
                                     `cp_develop_date` int(11) NOT NULL COMMENT '开发周期',
                                     `cp_develop_fee` int(11) NOT NULL COMMENT '加工费',
                                     `cp_direct_cost` int(11) NOT NULL COMMENT '直接成本',
                                     PRIMARY KEY (`id`),
                                     UNIQUE KEY `cp_id` (`cp_id`,`subject_number`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


-- 生产规则表gz_producing
CREATE TABLE `new_ct_gz_producing` (
                                       `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
                                       `cp_id` int(10) NOT NULL COMMENT '生产规则表id',
                                       `subject_number` int(11) NOT NULL COMMENT '题库号',
                                       `cp_name` varchar(255) DEFAULT NULL COMMENT '产品名称',
                                       `cp_mid` int(11) NOT NULL COMMENT '原材料规则id',
                                       `cp_num` int(11) NOT NULL COMMENT '所需数量',
                                       PRIMARY KEY (`id`),
                                       UNIQUE KEY `cp_id` (`cp_id`,`subject_number`,`cp_mid`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;



-- 生产线规则表gz_product_line
CREATE TABLE `new_ct_gz_product_line` (
                                          `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
                                          `cpl_id` int(10) NOT NULL COMMENT '生产线编号',
                                          `subject_num` int(11) NOT NULL COMMENT '题库号',
                                          `cpl_name` varchar(32) NOT NULL COMMENT '生产线名称',
                                          `cpl_buy_fee` int(11) NOT NULL COMMENT '每次投资费用',
                                          `cpl_install_date` tinyint(4) NOT NULL COMMENT '投资周期',
                                          `cpl_produce_date` tinyint(4) NOT NULL COMMENT '生产时间',
                                          `cpl_transfer_date` tinyint(4) NOT NULL COMMENT '转产周期',
                                          `cpl_transfer_fee` int(11) NOT NULL COMMENT '转产费用',
                                          `cpl_maintenance_fee` int(11) NOT NULL COMMENT '维修费（W/每年）',
                                          `cpl_scarp_fee` int(11) NOT NULL COMMENT '折旧费',
                                          `cpl_dep_date` tinyint(4) NOT NULL COMMENT '折旧年限',
                                          `cpl_dep_fee` int(11) NOT NULL COMMENT '残值',

                                          `cpl_senior_worker` int(11) NOT NULL COMMENT '需要高级工人',
                                          `cpl_ordinary_worker` int(11) NOT NULL COMMENT '需要普通工人',
                                          `cpl_production` int(11) NOT NULL COMMENT '基础产量',
                                          PRIMARY KEY (`id`),
                                          UNIQUE KEY `cpl_id` (`cpl_id`,`subject_num`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;




-- 产品设计gz_product_design（新增）

CREATE TABLE `new_ct_gz_product_design` (
                                            `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
                                            `subject_num` int(11) NOT NULL COMMENT '题库号',
                                            `feature_name` varchar(255) NOT NULL COMMENT '特征名称',
                                            `design_num` varchar(255) NOT NULL COMMENT '编码',
                                            `cost_markup` int(11) NOT NULL COMMENT '成本加成',
                                            `design_cost` int(11) NOT NULL COMMENT '设计费用',
                                            `unit_cost_upgrade` int(11) NOT NULL COMMENT '升级单位成本',
                                            `initial_value` int(11) NOT NULL COMMENT '初始值',
                                            `upper_limit` int(11) NOT NULL COMMENT '上限',
                                            PRIMARY KEY (`id`),
                                            UNIQUE KEY `design_num` (`subject_num`,`design_num`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


-- 工人招聘gz_worker_recruit（新增）
CREATE TABLE `new_ct_gz_worker_recruit` (
                                            `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
                                            `subject_num` int(11) NOT NULL COMMENT '题库号',
                                            `recruit_name` varchar(255) NOT NULL COMMENT '名称',
                                            `recruit_num` varchar(255) NOT NULL COMMENT '编码',
                                            `init_sal_expect` int(11) NOT NULL COMMENT '初始期望工资(元)',
                                            `piece` int(11) NOT NULL COMMENT '计件',
                                            `qty_per_qtr` int(11) NOT NULL COMMENT '每季度数量',
                                            `mult_bonus` decimal(20,2) NOT NULL COMMENT '倍数加成(%)',
                                            PRIMARY KEY (`id`),
                                            UNIQUE KEY `recruit_num` (`subject_num`,`recruit_num`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



-- 工人培训gz_worder_train(新增)
CREATE TABLE `new_ct_gz_worker_train` (
                                          `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
                                          `subject_num` int(11) NOT NULL COMMENT '题库号',
                                          `training_name` varchar(255) NOT NULL COMMENT '培训名称',
                                          `cash_cost` int(11) NOT NULL COMMENT '消耗现金(元)',
                                          `time_cost_quarter` int(11) NOT NULL COMMENT '消耗时间(季)',
                                          `original_position` varchar(255) NOT NULL COMMENT '原岗位',
                                          `trained_position` varchar(255) NOT NULL COMMENT '培训后岗位',
                                          `salary_increase_percent` decimal(20,2) NOT NULL COMMENT '工资涨幅(%)',
                                          PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


-- 贷款规则gz_loan(新增)

CREATE TABLE `new_ct_gz_loan` (
                                  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
                                  `subject_num` int(11) NOT NULL COMMENT '题库号',
                                  `loan_name` varchar(255) NOT NULL COMMENT '贷款名称',
                                  `loan_num`  varchar(255) NOT NULL COMMENT '贷款编码',
                                  `loan_max` decimal(20,2) NOT NULL COMMENT '额度上限(倍)',
                                  `loan_time` int(11) NOT NULL COMMENT '贷款时间(季)',
                                  `loan_repayment` varchar(255) NOT NULL COMMENT '还款方式',
                                  `loan_rate` decimal(20,2)  NOT NULL COMMENT '利率(%)',
                                  PRIMARY KEY (`id`),
                                  UNIQUE KEY `loan_num` (`subject_num`,`loan_num`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;




-- 贴现规则gz_discount（新增）

CREATE TABLE `new_ct_gz_discount` (
                                      `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
                                      `subject_num` int(11) NOT NULL COMMENT '题库号',
                                      `discount_name` varchar(255) NOT NULL COMMENT '贷款名称',
                                      `discount_num`  varchar(255) NOT NULL COMMENT '贷款编码',
                                      `discount_time` int(11) NOT NULL COMMENT '收款期(季)',
                                      `discount_interest` decimal(20,2)  NOT NULL COMMENT '贴息(%)',
                                       PRIMARY KEY (`id`),
                                       UNIQUE KEY `discount_num` (`subject_num`,`discount_num`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;




-- 班次规则gz_classes(新增)

CREATE TABLE `new_ct_gz_classes` (
                                     `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
                                     `subject_num` int(11) NOT NULL COMMENT '题库号',
                                     `classes_name` varchar(255) NOT NULL COMMENT '班次名称',
                                     `classes_num`  varchar(255) NOT NULL COMMENT '班次编码',
                                     `output_multi` decimal(20,2) NOT NULL COMMENT '产量加成(倍)',
                                     `efficiency_loss` decimal(20,2)  NOT NULL COMMENT '效率损失(%)',
                                     PRIMARY KEY (`id`),
                                     UNIQUE KEY `classes_num` (`subject_num`,`classes_num`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



-- 员工激励gz_worker_incentive(新增)
CREATE TABLE `new_ct_gz_worker_incentive` (
                                              `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
                                              `subject_num` int(11) NOT NULL COMMENT '题库号',
                                              `incentive_name` varchar(255) NOT NULL COMMENT '激励名称',
                                              `incentive_num`  varchar(255) NOT NULL COMMENT '编码',
                                              `efficiency_percent` decimal(20,2)  NOT NULL COMMENT '提升效率比例(%)',
                                              PRIMARY KEY (`id`),
                                              UNIQUE KEY `incentive_num` (`subject_num`,`incentive_num`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



-- 数字化岗位gz_num(新增)
CREATE TABLE `new_ct_gz_num` (
                                 `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
                                 `subject_num` int(11) NOT NULL COMMENT '题库号',
                                 `post_num`  varchar(255) NOT NULL COMMENT '岗位编码',
                                 `consume_money` int(11)  NOT NULL COMMENT '消耗金钱(元)',
                                 `time_cost_quarter` int(11) NOT NULL COMMENT '消耗时间(季)',
                                 PRIMARY KEY (`id`),
                                 UNIQUE KEY `post_num` (`subject_num`,`post_num`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


