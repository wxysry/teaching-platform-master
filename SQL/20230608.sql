CREATE TABLE `new_contest_student` (
                                       `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '竞赛模拟的学生',
                                       `student_id` int(11) NOT NULL COMMENT '学生id',
                                       `contest_id` int(11) NOT NULL COMMENT '竞赛id',
                                       `start` tinyint(4) NOT NULL DEFAULT '0' COMMENT '是否已开始经营',
                                       `score` decimal(5,2) DEFAULT NULL COMMENT '分数',
                                       `progress` varchar(255) DEFAULT NULL COMMENT '经营进度，前端用于显示按钮',
                                       `date` mediumint(9) DEFAULT NULL COMMENT '经营时间进度,年份+季度+进度',
                                       `market_list` varchar(255) DEFAULT NULL COMMENT '选单进度，前端需要',
                                       `active_name` varchar(255) DEFAULT NULL COMMENT '选单进度，前端需要',
                                       `error_report_year` varchar(10) NOT NULL DEFAULT '' COMMENT '报表填写错误年数，1,2,3表示第1、2、3年填写错误',
                                       `finish_time` datetime DEFAULT NULL COMMENT '完成时间，为空表示未完成',
                                       PRIMARY KEY (`id`),
                                       UNIQUE KEY `(student_id,contest_id)` (`student_id`,`contest_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;