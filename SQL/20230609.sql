CREATE TABLE `new_ct_bank_loan` (
  `bl_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '贷款记录表',
  `student_id` int(11) NOT NULL,
  `contest_id` int(11) NOT NULL,
  `bl_type` tinyint(4) NOT NULL COMMENT '贷款类型:1表示长贷，2表示短贷',
  `bl_fee` int(11) NOT NULL COMMENT '贷款金额',
  `bl_add_time` tinyint(4) NOT NULL COMMENT '贷款时间',
  `bl_remain_time` tinyint(4) NOT NULL COMMENT '贷款时长',
  `bl_repayment_date` tinyint(4) NOT NULL COMMENT '还款时间',
  PRIMARY KEY (`bl_id`),
  KEY `(contest_id,student_id)` (`contest_id`,`student_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;



CREATE TABLE `new_ct_cashflow` (
  `cashflow_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '现金流量表id',
  `student_id` int(11) NOT NULL COMMENT '学生id',
  `contest_id` int(11) NOT NULL COMMENT '考试id',
  `c_action` varchar(255) NOT NULL COMMENT '操作类型',
  `c_in` int(11) NOT NULL COMMENT '资金增加',
  `c_out` int(11) NOT NULL COMMENT '资金减少',
  `c_surplus` int(11) NOT NULL COMMENT '现金',
  `c_comment` varchar(255) DEFAULT NULL COMMENT '详情',
  `c_date` tinyint(4) NOT NULL COMMENT '操作时间',
  PRIMARY KEY (`cashflow_id`),
  KEY `c_action` (`c_action`),
  KEY `(contest_id,student_id)` (`contest_id`,`student_id`)
) ENGINE=InnoDB AUTO_INCREMENT=78 DEFAULT CHARSET=utf8;


CREATE TABLE `new_ct_dd_material` (
  `dd_material_id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '原料采购记录',
  `om_cmid` int(10) NOT NULL COMMENT '原料id',
  `om_num` int(11) DEFAULT NULL COMMENT '数量',
  `student_id` int(11) DEFAULT NULL,
  `contest_id` int(11) DEFAULT NULL,
  `remain_date` tinyint(4) DEFAULT NULL COMMENT '剩余时间',
  `purchase_date` tinyint(4) DEFAULT NULL COMMENT '采购时间',
  PRIMARY KEY (`dd_material_id`),
  KEY `(contest_id,student_id)` (`contest_id`,`student_id`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8;


CREATE TABLE `new_ct_kc_material` (
  `material_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `student_id` int(11) DEFAULT NULL,
  `contest_id` int(11) DEFAULT NULL,
  `im_cm_id` int(11) DEFAULT NULL COMMENT '原料id',
  `im_num` int(11) DEFAULT NULL COMMENT '剩余数量',
  PRIMARY KEY (`material_id`),
  KEY `(contest_id,student_id)` (`contest_id`,`student_id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;

CREATE TABLE `new_ct_kc_product` (
  `kc_product_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '产成品库存',
  `student_id` int(11) DEFAULT NULL,
  `contest_id` int(11) DEFAULT NULL,
  `ip_cp_id` int(11) DEFAULT NULL COMMENT '产品id',
  `ip_num` int(11) DEFAULT NULL COMMENT '库存数量',
  PRIMARY KEY (`kc_product_id`),
  KEY `(contest_id,student_id)` (`contest_id`,`student_id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;


CREATE TABLE `new_ct_workshop` (
  `workshop_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `student_id` int(11) NOT NULL,
  `contest_id` int(11) NOT NULL,
  `w_cwid` int(11) NOT NULL COMMENT '厂房规则表里的id',
  `w_status` tinyint(4) NOT NULL COMMENT '租售类型:0表示BUY,1表示RENT',
  `w_surplus_capacity` int(11) DEFAULT NULL COMMENT '剩余容量',
  `w_pay_date` int(11) DEFAULT NULL COMMENT '最后付租',
  `get_date` tinyint(4) DEFAULT NULL COMMENT '置办时间',
  PRIMARY KEY (`workshop_id`),
  KEY `(contest_id,student_id)` (`contest_id`,`student_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

CREATE TABLE `new_ct_xz_order` (
  `order_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `co_id` varchar(50) NOT NULL COMMENT '订单编号',
  `student_id` int(11) NOT NULL,
  `contest_id` int(11) NOT NULL,
  `date` tinyint(4) DEFAULT NULL COMMENT '年份',
  `cm_id` int(11) DEFAULT NULL COMMENT '市场',
  `cp_id` int(11) DEFAULT NULL COMMENT '产品',
  `num` int(11) DEFAULT NULL COMMENT '数量',
  `total_price` int(11) DEFAULT NULL COMMENT '总价',
  `delivery_date` tinyint(4) DEFAULT NULL COMMENT '交货期',
  `payment_date` tinyint(4) DEFAULT NULL COMMENT '账期',
  `iso_id` int(11) DEFAULT NULL COMMENT 'ISO',
  `commit_date` tinyint(4) DEFAULT NULL COMMENT '交单时间',
  PRIMARY KEY (`order_id`),
  KEY `(contest_id,student_id)` (`contest_id`,`student_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

CREATE TABLE `new_ct_gz_iso` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ci_id` int(11) NOT NULL COMMENT 'ISO规则表 ISO类型',
  `subject_number` int(11) NOT NULL COMMENT '题库号',
  `ci_name` varchar(255) DEFAULT NULL COMMENT 'ISO名称',
  `ci_develop_fee` int(11) NOT NULL COMMENT '研发费用',
  `ci_develop_date` int(11) NOT NULL COMMENT '研发周期',
  PRIMARY KEY (`id`),
  UNIQUE KEY `ci_id` (`ci_id`,`subject_number`)
) ENGINE=InnoDB AUTO_INCREMENT=243 DEFAULT CHARSET=utf8;


CREATE TABLE `new_ct_yf_market` (
  `market_id` int(11) NOT NULL AUTO_INCREMENT,
  `student_id` int(11) DEFAULT NULL,
  `contest_id` int(11) DEFAULT NULL,
  `dm_cm_id` int(11) DEFAULT NULL COMMENT '市场规则id',
  `dm_total_date` tinyint(4) DEFAULT NULL COMMENT '总研发时间',
  `dm_now_date` tinyint(4) DEFAULT NULL COMMENT '已研发时间',
  `dm_remain_date` tinyint(4) DEFAULT NULL COMMENT '剩余研发时间',
  `dm_finish_date` tinyint(4) DEFAULT NULL COMMENT '研发完成时间',
  PRIMARY KEY (`market_id`),
  KEY `(contest_id,student_id)` (`contest_id`,`student_id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;

CREATE TABLE `new_ct_gz_workshop` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `cw_id` int(11) NOT NULL COMMENT '编号',
  `subject_num` int(11) NOT NULL COMMENT '题库号',
  `cw_name` varchar(32) NOT NULL COMMENT '厂房名称',
  `cw_buy_fee` int(11) NOT NULL COMMENT '购买费用',
  `cw_rent_fee` int(11) NOT NULL COMMENT '租金',
  `cw_sell_fee` int(11) NOT NULL COMMENT '售价',
  `cw_capacity` int(11) NOT NULL COMMENT '生产线容量',
  PRIMARY KEY (`id`),
  UNIQUE KEY `cw_id` (`cw_id`,`subject_num`)
) ENGINE=InnoDB AUTO_INCREMENT=359 DEFAULT CHARSET=utf8;


CREATE TABLE `new_ct_line` (
  `line_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `student_id` int(11) NOT NULL,
  `contest_id` int(11) NOT NULL,
  `workshop_id` int(11) NOT NULL COMMENT '所属厂房id（ct_workshop）',
  `pl_wid` int(11) NOT NULL COMMENT '所属厂房类型id(ct_gz_workshop)',
  `pl_cplid` int(11) NOT NULL COMMENT '生产线类型id',
  `pl_cpid` int(11) NOT NULL COMMENT '生产产品id',
  `pl_invest` int(11) NOT NULL COMMENT '原值',
  `pl_dep_total` int(11) NOT NULL COMMENT '累计折旧',
  `pl_product_add_date` tinyint(4) DEFAULT '0' COMMENT '开产时间',
  `pl_producting_date` tinyint(4) DEFAULT '0' COMMENT '剩余生产时间',
  `pl_finish_date` tinyint(4) DEFAULT '0' COMMENT '建成时间',
  `pl_remain_date` tinyint(4) DEFAULT '0' COMMENT '在建剩余时间',
  `pl_add_time` tinyint(4) DEFAULT '0' COMMENT '开建时间',
  `pl_transfer_add_date` tinyint(4) DEFAULT '0' COMMENT '转产时间',
  `pl_transfer_date` tinyint(4) DEFAULT '0' COMMENT '转产剩余时间',
  PRIMARY KEY (`line_id`),
  KEY `(contest_id,student_id)` (`contest_id`,`student_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;


CREATE TABLE `new_ct_yf_product` (
  `product_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `student_id` int(11) DEFAULT NULL,
  `contest_id` int(11) DEFAULT NULL,
  `dp_cp_id` int(11) DEFAULT NULL COMMENT '产品规则id',
  `dp_total_date` tinyint(4) DEFAULT NULL COMMENT '总研发时间',
  `dp_now_date` tinyint(4) DEFAULT NULL COMMENT '已研发时间',
  `dp_remain_date` tinyint(4) DEFAULT NULL COMMENT '剩余研发时间',
  `dp_finish_date` tinyint(4) DEFAULT NULL COMMENT '研发完成时间',
  PRIMARY KEY (`product_id`),
  KEY `(contest_id,student_id)` (`contest_id`,`student_id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;




