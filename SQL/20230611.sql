CREATE TABLE `new_ct_balance` (
                                  `balance_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '资产负债表id',
                                  `student_id` int(11) NOT NULL COMMENT '学生id',
                                  `contest_id` int(11) NOT NULL COMMENT '考试id',
                                  `bs_isxt` tinyint(4) NOT NULL COMMENT '填写人：1代表系统自动生成，0代表学生录入',
                                  `bs_year` tinyint(4) NOT NULL COMMENT '年份',
                                  `bs_cash` int(11) NOT NULL COMMENT '现金',
                                  `bs_receivable` int(11) NOT NULL COMMENT '应收款',
                                  `bs_product_in_process` int(11) DEFAULT NULL COMMENT '在制品',
                                  `bs_product` int(11) DEFAULT NULL COMMENT '产成品',
                                  `bs_material` int(11) DEFAULT NULL COMMENT '原材料',
                                  `bs_total_current_asset` int(11) NOT NULL COMMENT '流动资产合计',
                                  `bs_workshop` int(11) DEFAULT NULL COMMENT '厂房',
                                  `bs_equipment` int(11) DEFAULT NULL COMMENT '生产线',
                                  `bs_project_on_construction` int(11) DEFAULT NULL COMMENT '在建',
                                  `bs_total_fixed_asset` int(11) NOT NULL COMMENT '固定资产合计',
                                  `bs_total_asset` int(11) NOT NULL COMMENT '资产合计',
                                  `bs_long_loan` int(11) NOT NULL COMMENT '长期贷款',
                                  `bs_short_loan` int(11) NOT NULL COMMENT '短期贷款',
                                  `bs_tax` int(11) NOT NULL COMMENT '应交税费',
                                  `bs_total_liability` int(11) NOT NULL COMMENT '负债合计',
                                  `bs_equity` int(11) NOT NULL COMMENT '股东资本',
                                  `bs_retained_earning` int(11) NOT NULL COMMENT '利润留存',
                                  `bs_annual_net_profit` int(11) NOT NULL COMMENT '年度净利',
                                  `bs_total_equity` int(11) NOT NULL COMMENT '所有者权益',
                                  `bs_total` int(11) NOT NULL COMMENT '负债所有者权益合计',
                                  PRIMARY KEY (`balance_id`),
                                  KEY `(contest_id,student_id)` (`contest_id`,`student_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;



CREATE TABLE `new_ct_balance_like` (
                                       `balance_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '资产负债表id',
                                       `student_id` int(11) NOT NULL COMMENT '学生id',
                                       `contest_id` int(11) NOT NULL COMMENT '考试id',
                                       `bs_isxt` tinyint(4) NOT NULL COMMENT '填写人：1代表系统自动生成，0代表学生录入',
                                       `bs_year` tinyint(4) NOT NULL COMMENT '年份',
                                       `bs_cash` int(11) NOT NULL COMMENT '现金',
                                       `bs_receivable` int(11) NOT NULL COMMENT '应收款',
                                       `bs_product_in_process` int(11) DEFAULT NULL COMMENT '在制品',
                                       `bs_product` int(11) DEFAULT NULL COMMENT '产成品',
                                       `bs_material` int(11) DEFAULT NULL COMMENT '原材料',
                                       `bs_total_current_asset` int(11) NOT NULL COMMENT '流动资产合计',
                                       `bs_workshop` int(11) DEFAULT NULL COMMENT '厂房',
                                       `bs_equipment` int(11) DEFAULT NULL COMMENT '生产线',
                                       `bs_project_on_construction` int(11) DEFAULT NULL COMMENT '在建',
                                       `bs_total_fixed_asset` int(11) NOT NULL COMMENT '固定资产合计',
                                       `bs_total_asset` int(11) NOT NULL COMMENT '资产合计',
                                       `bs_long_loan` int(11) NOT NULL COMMENT '长期贷款',
                                       `bs_short_loan` int(11) NOT NULL COMMENT '短期贷款',
                                       `bs_tax` int(11) NOT NULL COMMENT '应交税费',
                                       `bs_total_liability` int(11) NOT NULL COMMENT '负债合计',
                                       `bs_equity` int(11) NOT NULL COMMENT '股东资本',
                                       `bs_retained_earning` int(11) NOT NULL COMMENT '利润留存',
                                       `bs_annual_net_profit` int(11) NOT NULL COMMENT '年度净利',
                                       `bs_total_equity` int(11) NOT NULL COMMENT '所有者权益',
                                       `bs_total` int(11) NOT NULL COMMENT '负债所有者权益合计',
                                       PRIMARY KEY (`balance_id`),
                                       KEY `(contest_id,student_id)` (`contest_id`,`student_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;



CREATE TABLE `new_ct_balance_season_like` (
                                              `balance_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '资产负债表id',
                                              `student_id` int(11) NOT NULL COMMENT '学生id',
                                              `contest_id` int(11) NOT NULL COMMENT '考试id',
                                              `bs_isxt` tinyint(4) NOT NULL COMMENT '填写人：1代表系统自动生成，0代表学生录入',
                                              `bs_year` tinyint(4) NOT NULL COMMENT '年份',
                                              `bs_cash` int(11) NOT NULL COMMENT '现金',
                                              `bs_receivable` int(11) NOT NULL COMMENT '应收款',
                                              `bs_product_in_process` int(11) DEFAULT NULL COMMENT '在制品',
                                              `bs_product` int(11) DEFAULT NULL COMMENT '产成品',
                                              `bs_material` int(11) DEFAULT NULL COMMENT '原材料',
                                              `bs_total_current_asset` int(11) NOT NULL COMMENT '流动资产合计',
                                              `bs_workshop` int(11) DEFAULT NULL COMMENT '厂房',
                                              `bs_equipment` int(11) DEFAULT NULL COMMENT '生产线',
                                              `bs_project_on_construction` int(11) DEFAULT NULL COMMENT '在建',
                                              `bs_total_fixed_asset` int(11) NOT NULL COMMENT '固定资产合计',
                                              `bs_total_asset` int(11) NOT NULL COMMENT '资产合计',
                                              `bs_long_loan` int(11) NOT NULL COMMENT '长期贷款',
                                              `bs_short_loan` int(11) NOT NULL COMMENT '短期贷款',
                                              `bs_tax` int(11) NOT NULL COMMENT '应交税费',
                                              `bs_total_liability` int(11) NOT NULL COMMENT '负债合计',
                                              `bs_equity` int(11) NOT NULL COMMENT '股东资本',
                                              `bs_retained_earning` int(11) NOT NULL COMMENT '利润留存',
                                              `bs_annual_net_profit` int(11) NOT NULL COMMENT '年度净利',
                                              `bs_total_equity` int(11) NOT NULL COMMENT '所有者权益',
                                              `bs_total` int(11) NOT NULL COMMENT '负债所有者权益合计',
                                              PRIMARY KEY (`balance_id`),
                                              KEY `(contest_id,student_id)` (`contest_id`,`student_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;





CREATE TABLE `new_ct_cashflow_like` (
                                        `cashflow_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '现金流量表id',
                                        `student_id` int(11) NOT NULL COMMENT '学生id',
                                        `contest_id` int(11) NOT NULL COMMENT '考试id',
                                        `c_action` varchar(255) NOT NULL COMMENT '操作类型',
                                        `c_in` int(11) NOT NULL COMMENT '资金增加',
                                        `c_out` int(11) NOT NULL COMMENT '资金减少',
                                        `c_surplus` int(11) NOT NULL COMMENT '现金',
                                        `c_comment` varchar(255) DEFAULT NULL COMMENT '详情',
                                        `c_date` tinyint(4) NOT NULL COMMENT '操作时间',
                                        PRIMARY KEY (`cashflow_id`),
                                        KEY `c_action` (`c_action`),
                                        KEY `(contest_id,student_id)` (`contest_id`,`student_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;



CREATE TABLE `new_ct_cashflow_season_like` (
                                               `cashflow_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '现金流量表id',
                                               `student_id` int(11) NOT NULL COMMENT '学生id',
                                               `contest_id` int(11) NOT NULL COMMENT '考试id',
                                               `c_action` varchar(255) NOT NULL COMMENT '操作类型',
                                               `c_in` int(11) NOT NULL COMMENT '资金增加',
                                               `c_out` int(11) NOT NULL COMMENT '资金减少',
                                               `c_surplus` int(11) NOT NULL COMMENT '现金',
                                               `c_comment` varchar(255) DEFAULT NULL COMMENT '详情',
                                               `c_date` tinyint(4) NOT NULL COMMENT '操作时间',
                                               PRIMARY KEY (`cashflow_id`),
                                               KEY `c_action` (`c_action`),
                                               KEY `(contest_id,student_id)` (`contest_id`,`student_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;






CREATE TABLE `new_ct_mn_choose` (
                                    `choose_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
                                    `student_id` int(11) DEFAULT NULL,
                                    `contest_id` int(11) DEFAULT NULL,
                                    `co_id` varchar(50) DEFAULT NULL COMMENT '定单编号',
                                    `date` int(11) DEFAULT NULL COMMENT '年份',
                                    `cm_id` int(11) DEFAULT NULL COMMENT '市场',
                                    `cp_id` int(11) DEFAULT NULL COMMENT '产品编号',
                                    `num` int(11) DEFAULT NULL COMMENT '数量',
                                    `total_price` int(11) DEFAULT NULL COMMENT '总价',
                                    `delivery_date` int(11) DEFAULT NULL COMMENT '交货期',
                                    `payment_day` int(11) DEFAULT NULL COMMENT '账期',
                                    `ci_id` int(11) DEFAULT NULL COMMENT 'ISO编号，为空代表不需要任何iso认证，为3代表需要全部iso认证，为1或2代表需要1或2认证',
                                    `xz_round` int(11) DEFAULT NULL COMMENT '选走轮次',
                                    `xz_group` varchar(50) DEFAULT NULL COMMENT '选走组号',
                                    PRIMARY KEY (`choose_id`),
                                    KEY `(contest_id,student_id)` (`contest_id`,`student_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


CREATE TABLE `new_ct_mn_choose_like` (
                                         `choose_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
                                         `student_id` int(11) DEFAULT NULL,
                                         `contest_id` int(11) DEFAULT NULL,
                                         `co_id` varchar(50) DEFAULT NULL COMMENT '定单编号',
                                         `date` int(11) DEFAULT NULL COMMENT '年份',
                                         `cm_id` int(11) DEFAULT NULL COMMENT '市场',
                                         `cp_id` int(11) DEFAULT NULL COMMENT '产品编号',
                                         `num` int(11) DEFAULT NULL COMMENT '数量',
                                         `total_price` int(11) DEFAULT NULL COMMENT '总价',
                                         `delivery_date` int(11) DEFAULT NULL COMMENT '交货期',
                                         `payment_day` int(11) DEFAULT NULL COMMENT '账期',
                                         `ci_id` int(11) DEFAULT NULL COMMENT 'ISO',
                                         `xz_round` int(11) DEFAULT NULL COMMENT '选走轮次',
                                         `xz_group` varchar(50) DEFAULT NULL COMMENT '选走组号',
                                         PRIMARY KEY (`choose_id`),
                                         KEY `(contest_id,student_id)` (`contest_id`,`student_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


CREATE TABLE `new_ct_mn_choose_season_like` (
                                                `choose_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
                                                `student_id` int(11) DEFAULT NULL,
                                                `contest_id` int(11) DEFAULT NULL,
                                                `co_id` varchar(50) DEFAULT NULL COMMENT '定单编号',
                                                `date` int(11) DEFAULT NULL COMMENT '年份',
                                                `cm_id` int(11) DEFAULT NULL COMMENT '市场',
                                                `cp_id` int(11) DEFAULT NULL COMMENT '产品编号',
                                                `num` int(11) DEFAULT NULL COMMENT '数量',
                                                `total_price` int(11) DEFAULT NULL COMMENT '总价',
                                                `delivery_date` int(11) DEFAULT NULL COMMENT '交货期',
                                                `payment_day` int(11) DEFAULT NULL COMMENT '账期',
                                                `ci_id` int(11) DEFAULT NULL COMMENT 'ISO编号，为空代表不需要任何iso认证，为3代表需要全部iso认证，为1或2代表需要1或2认证',
                                                `xz_round` int(11) DEFAULT NULL COMMENT '选走轮次',
                                                `xz_group` varchar(50) DEFAULT NULL COMMENT '选走组号',
                                                PRIMARY KEY (`choose_id`),
                                                KEY `(contest_id,student_id)` (`contest_id`,`student_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


CREATE TABLE `new_ct_mn_ad` (
                                `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '模拟广告',
                                `student_id` int(11) DEFAULT NULL COMMENT '学生id',
                                `contest_id` int(11) DEFAULT NULL COMMENT '考试id',
                                `year` tinyint(4) NOT NULL COMMENT '年份',
                                `group_num` varchar(100) DEFAULT NULL COMMENT '组号',
                                `local_p1` decimal(15,5) DEFAULT NULL COMMENT '本地P1',
                                `regional_p1` decimal(15,5) DEFAULT NULL COMMENT '区域P1',
                                `national_p1` decimal(15,5) DEFAULT NULL COMMENT '国内P1',
                                `asian_p1` decimal(15,5) DEFAULT NULL COMMENT '亚洲P1',
                                `international_p1` decimal(15,5) DEFAULT NULL COMMENT '国际P1',
                                `local_p2` decimal(15,5) DEFAULT NULL COMMENT '本地P2',
                                `regional_p2` decimal(15,5) DEFAULT NULL COMMENT '区域P2',
                                `national_p2` decimal(15,5) DEFAULT NULL COMMENT '国内P2',
                                `asian_p2` decimal(15,5) DEFAULT NULL COMMENT '亚洲P2',
                                `international_p2` decimal(15,5) DEFAULT NULL COMMENT '国际P2',
                                `local_p3` decimal(15,5) DEFAULT NULL COMMENT '本地P3',
                                `regional_p3` decimal(15,5) DEFAULT NULL COMMENT '区域P3',
                                `national_p3` decimal(15,5) DEFAULT NULL COMMENT '国内P3',
                                `asian_p3` decimal(15,5) DEFAULT NULL COMMENT '亚洲P3',
                                `international_p3` decimal(15,5) DEFAULT NULL COMMENT '国际P3',
                                `local_p4` decimal(15,5) DEFAULT NULL COMMENT '本地P4',
                                `regional_p4` decimal(15,5) DEFAULT NULL COMMENT '区域P4',
                                `national_p4` decimal(15,5) DEFAULT NULL COMMENT '国内P4',
                                `asian_p4` decimal(15,5) DEFAULT NULL COMMENT '亚洲P4',
                                `international_p4` decimal(15,5) DEFAULT NULL COMMENT '国际P4',
                                `local_p5` decimal(15,5) DEFAULT NULL COMMENT '本地P5',
                                `regional_p5` decimal(15,5) DEFAULT NULL COMMENT '区域P5',
                                `national_p5` decimal(15,5) DEFAULT NULL COMMENT '国内P5',
                                `asian_p5` decimal(15,5) DEFAULT NULL COMMENT '亚洲P5',
                                `international_p5` decimal(15,5) DEFAULT NULL COMMENT '国际P5',
                                PRIMARY KEY (`id`),
                                KEY `(contest_id,student_id)` (`contest_id`,`student_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;







CREATE TABLE `new_ct_mn_ad_like` (
                                     `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '模拟广告',
                                     `student_id` int(11) DEFAULT NULL COMMENT '学生id',
                                     `contest_id` int(11) DEFAULT NULL COMMENT '考试id',
                                     `year` tinyint(4) NOT NULL COMMENT '年份',
                                     `group_num` varchar(255) DEFAULT NULL COMMENT '组号',
                                     `local_p1` decimal(15,5) DEFAULT NULL COMMENT '本地P1',
                                     `regional_p1` decimal(15,5) DEFAULT NULL COMMENT '区域P1',
                                     `national_p1` decimal(15,5) DEFAULT NULL COMMENT '国内P1',
                                     `asian_p1` decimal(15,5) DEFAULT NULL COMMENT '亚洲P1',
                                     `international_p1` decimal(15,5) DEFAULT NULL COMMENT '国际P1',
                                     `local_p2` decimal(15,5) DEFAULT NULL COMMENT '本地P2',
                                     `regional_p2` decimal(15,5) DEFAULT NULL COMMENT '区域P2',
                                     `national_p2` decimal(15,5) DEFAULT NULL COMMENT '国内P2',
                                     `asian_p2` decimal(15,5) DEFAULT NULL COMMENT '亚洲P2',
                                     `international_p2` decimal(15,5) DEFAULT NULL COMMENT '国际P2',
                                     `local_p3` decimal(15,5) DEFAULT NULL COMMENT '本地P3',
                                     `regional_p3` decimal(15,5) DEFAULT NULL COMMENT '区域P3',
                                     `national_p3` decimal(15,5) DEFAULT NULL COMMENT '国内P3',
                                     `asian_p3` decimal(15,5) DEFAULT NULL COMMENT '亚洲P3',
                                     `international_p3` decimal(15,5) DEFAULT NULL COMMENT '国际P3',
                                     `local_p4` decimal(15,5) DEFAULT NULL COMMENT '本地P4',
                                     `regional_p4` decimal(15,5) DEFAULT NULL COMMENT '区域P4',
                                     `national_p4` decimal(15,5) DEFAULT NULL COMMENT '国内P4',
                                     `asian_p4` decimal(15,5) DEFAULT NULL COMMENT '亚洲P4',
                                     `international_p4` decimal(15,5) DEFAULT NULL COMMENT '国际P4',
                                     `local_p5` decimal(15,5) DEFAULT NULL COMMENT '本地P5',
                                     `regional_p5` decimal(15,5) DEFAULT NULL COMMENT '区域P5',
                                     `national_p5` decimal(15,5) DEFAULT NULL COMMENT '国内P5',
                                     `asian_p5` decimal(15,5) DEFAULT NULL COMMENT '亚洲P5',
                                     `international_p5` decimal(15,5) DEFAULT NULL COMMENT '国际P5',
                                     PRIMARY KEY (`id`),
                                     KEY `(contest_id,student_id)` (`contest_id`,`student_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;








CREATE TABLE `new_ct_mn_ad_season_like` (
                                            `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '模拟广告',
                                            `student_id` int(11) DEFAULT NULL COMMENT '学生id',
                                            `contest_id` int(11) DEFAULT NULL COMMENT '考试id',
                                            `year` tinyint(4) NOT NULL COMMENT '年份',
                                            `group_num` varchar(100) DEFAULT NULL COMMENT '组号',
                                            `local_p1` decimal(15,5) DEFAULT NULL COMMENT '本地P1',
                                            `regional_p1` decimal(15,5) DEFAULT NULL COMMENT '区域P1',
                                            `national_p1` decimal(15,5) DEFAULT NULL COMMENT '国内P1',
                                            `asian_p1` decimal(15,5) DEFAULT NULL COMMENT '亚洲P1',
                                            `international_p1` decimal(15,5) DEFAULT NULL COMMENT '国际P1',
                                            `local_p2` decimal(15,5) DEFAULT NULL COMMENT '本地P2',
                                            `regional_p2` decimal(15,5) DEFAULT NULL COMMENT '区域P2',
                                            `national_p2` decimal(15,5) DEFAULT NULL COMMENT '国内P2',
                                            `asian_p2` decimal(15,5) DEFAULT NULL COMMENT '亚洲P2',
                                            `international_p2` decimal(15,5) DEFAULT NULL COMMENT '国际P2',
                                            `local_p3` decimal(15,5) DEFAULT NULL COMMENT '本地P3',
                                            `regional_p3` decimal(15,5) DEFAULT NULL COMMENT '区域P3',
                                            `national_p3` decimal(15,5) DEFAULT NULL COMMENT '国内P3',
                                            `asian_p3` decimal(15,5) DEFAULT NULL COMMENT '亚洲P3',
                                            `international_p3` decimal(15,5) DEFAULT NULL COMMENT '国际P3',
                                            `local_p4` decimal(15,5) DEFAULT NULL COMMENT '本地P4',
                                            `regional_p4` decimal(15,5) DEFAULT NULL COMMENT '区域P4',
                                            `national_p4` decimal(15,5) DEFAULT NULL COMMENT '国内P4',
                                            `asian_p4` decimal(15,5) DEFAULT NULL COMMENT '亚洲P4',
                                            `international_p4` decimal(15,5) DEFAULT NULL COMMENT '国际P4',
                                            `local_p5` decimal(15,5) DEFAULT NULL COMMENT '本地P5',
                                            `regional_p5` decimal(15,5) DEFAULT NULL COMMENT '区域P5',
                                            `national_p5` decimal(15,5) DEFAULT NULL COMMENT '国内P5',
                                            `asian_p5` decimal(15,5) DEFAULT NULL COMMENT '亚洲P5',
                                            `international_p5` decimal(15,5) DEFAULT NULL COMMENT '国际P5',
                                            PRIMARY KEY (`id`),
                                            KEY `(contest_id,student_id)` (`contest_id`,`student_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;








CREATE TABLE `new_ct_charges` (
                                  `charges_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '费用表id',
                                  `student_id` int(11) NOT NULL COMMENT '学生id',
                                  `contest_id` int(11) NOT NULL COMMENT '考试id',
                                  `bs_isxt` tinyint(4) NOT NULL COMMENT '填写人：1代表系统自动生成，0代表学生录入',
                                  `c_date` tinyint(4) NOT NULL COMMENT '年份',
                                  `c_overhaul` int(11) NOT NULL COMMENT '管理费用',
                                  `c_ad` int(11) NOT NULL COMMENT '广告费用',
                                  `c_maintenance` int(11) NOT NULL COMMENT '维修费',
                                  `c_damage` int(11) NOT NULL COMMENT '损失',
                                  `c_transfer` int(11) NOT NULL COMMENT '转产费',
                                  `c_rent` int(11) NOT NULL COMMENT '租金',
                                  `c_develop_market` int(11) NOT NULL COMMENT '市场开拓',
                                  `c_develop_iso` int(11) NOT NULL COMMENT 'ISO开拓',
                                  `c_develop_product` int(11) NOT NULL COMMENT '产品研发',
                                  `c_information` int(11) NOT NULL COMMENT '合计',
                                  PRIMARY KEY (`charges_id`),
                                  KEY `(contest_id,student_id)` (`contest_id`,`student_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;








CREATE TABLE `new_ct_charges_like` (
                                       `charges_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '费用表id',
                                       `student_id` int(11) NOT NULL COMMENT '学生id',
                                       `contest_id` int(11) NOT NULL COMMENT '考试id',
                                       `bs_isxt` tinyint(4) NOT NULL COMMENT '填写人：1代表系统自动生成，0代表学生录入',
                                       `c_date` tinyint(4) NOT NULL COMMENT '年份',
                                       `c_overhaul` int(11) NOT NULL COMMENT '管理费用',
                                       `c_ad` int(11) NOT NULL COMMENT '广告费用',
                                       `c_maintenance` int(11) NOT NULL COMMENT '维修费',
                                       `c_damage` int(11) NOT NULL COMMENT '损失',
                                       `c_transfer` int(11) NOT NULL COMMENT '转产费',
                                       `c_rent` int(11) NOT NULL COMMENT '租金',
                                       `c_develop_market` int(11) NOT NULL COMMENT '市场开拓',
                                       `c_develop_iso` int(11) NOT NULL COMMENT 'ISO开拓',
                                       `c_develop_product` int(11) NOT NULL COMMENT '产品研发',
                                       `c_information` int(11) NOT NULL COMMENT '合计',
                                       PRIMARY KEY (`charges_id`),
                                       KEY `(contest_id,student_id)` (`contest_id`,`student_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;




CREATE TABLE `new_ct_charges_season_like` (
                                              `charges_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '费用表id',
                                              `student_id` int(11) NOT NULL COMMENT '学生id',
                                              `contest_id` int(11) NOT NULL COMMENT '考试id',
                                              `bs_isxt` tinyint(4) NOT NULL COMMENT '填写人：1代表系统自动生成，0代表学生录入',
                                              `c_date` tinyint(4) NOT NULL COMMENT '年份',
                                              `c_overhaul` int(11) NOT NULL COMMENT '管理费用',
                                              `c_ad` int(11) NOT NULL COMMENT '广告费用',
                                              `c_maintenance` int(11) NOT NULL COMMENT '维修费',
                                              `c_damage` int(11) NOT NULL COMMENT '损失',
                                              `c_transfer` int(11) NOT NULL COMMENT '转产费',
                                              `c_rent` int(11) NOT NULL COMMENT '租金',
                                              `c_develop_market` int(11) NOT NULL COMMENT '市场开拓',
                                              `c_develop_iso` int(11) NOT NULL COMMENT 'ISO开拓',
                                              `c_develop_product` int(11) NOT NULL COMMENT '产品研发',
                                              `c_information` int(11) NOT NULL COMMENT '合计',
                                              PRIMARY KEY (`charges_id`),
                                              KEY `(contest_id,student_id)` (`contest_id`,`student_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;







CREATE TABLE `new_ct_fee` (
                              `fee_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '应收款',
                              `student_id` int(11) NOT NULL,
                              `contest_id` int(11) NOT NULL,
                              `r_fee` int(11) NOT NULL COMMENT '金额',
                              `r_remain_date` tinyint(4) NOT NULL COMMENT '账期',
                              PRIMARY KEY (`fee_id`),
                              KEY `(contest_id,student_id)` (`contest_id`,`student_id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;



CREATE TABLE `new_ct_fee_like` (
                                   `fee_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '应收款',
                                   `student_id` int(11) NOT NULL,
                                   `contest_id` int(11) NOT NULL,
                                   `r_fee` int(11) NOT NULL COMMENT '金额',
                                   `r_remain_date` tinyint(4) NOT NULL COMMENT '账期',
                                   PRIMARY KEY (`fee_id`),
                                   KEY `(contest_id,student_id)` (`contest_id`,`student_id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;


CREATE TABLE `new_ct_fee_season_like` (
                                          `fee_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '应收款',
                                          `student_id` int(11) NOT NULL,
                                          `contest_id` int(11) NOT NULL,
                                          `r_fee` int(11) NOT NULL COMMENT '金额',
                                          `r_remain_date` tinyint(4) NOT NULL COMMENT '账期',
                                          PRIMARY KEY (`fee_id`),
                                          KEY `(contest_id,student_id)` (`contest_id`,`student_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;






CREATE TABLE `new_ct_profit_chart` (
                                       `profit_chart_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '利润表id',
                                       `student_id` int(11) NOT NULL COMMENT '学生id',
                                       `contest_id` int(11) NOT NULL COMMENT '考试id',
                                       `bs_isxt` tinyint(4) NOT NULL COMMENT '填写人：1代表系统自动生成，0代表学生录入',
                                       `pc_date` tinyint(4) NOT NULL COMMENT '年份',
                                       `pc_sales` int(11) NOT NULL COMMENT '销售额',
                                       `pc_direct_cost` int(11) NOT NULL COMMENT '成本',
                                       `pc_goods_profit` int(11) NOT NULL COMMENT '毛利',
                                       `pc_total` int(11) NOT NULL COMMENT '管理费用',
                                       `pc_profit_before_dep` int(11) NOT NULL COMMENT '折旧前利润',
                                       `pc_dep` int(11) NOT NULL COMMENT '折旧',
                                       `pc_profit_before_interests` int(11) NOT NULL COMMENT '财务费用前利润',
                                       `pc_finance_fee` int(11) NOT NULL COMMENT '财务费用',
                                       `pc_profit_before_tax` int(11) NOT NULL COMMENT '税前利润',
                                       `pc_tax` int(11) NOT NULL COMMENT '所得税',
                                       `pc_annual_net_profit` int(11) NOT NULL COMMENT '净利润',
                                       PRIMARY KEY (`profit_chart_id`),
                                       KEY `(contest_id,student_id)` (`contest_id`,`student_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;




CREATE TABLE `new_ct_profit_chart_like` (
                                            `profit_chart_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '利润表id',
                                            `student_id` int(11) NOT NULL COMMENT '学生id',
                                            `contest_id` int(11) NOT NULL COMMENT '考试id',
                                            `bs_isxt` tinyint(4) NOT NULL COMMENT '填写人：1代表系统自动生成，0代表学生录入',
                                            `pc_date` tinyint(4) NOT NULL COMMENT '年份',
                                            `pc_sales` int(11) NOT NULL COMMENT '销售额',
                                            `pc_direct_cost` int(11) NOT NULL COMMENT '成本',
                                            `pc_goods_profit` int(11) NOT NULL COMMENT '毛利',
                                            `pc_total` int(11) NOT NULL COMMENT '管理费用',
                                            `pc_profit_before_dep` int(11) NOT NULL COMMENT '折旧前利润',
                                            `pc_dep` int(11) NOT NULL COMMENT '折旧',
                                            `pc_profit_before_interests` int(11) NOT NULL COMMENT '财务费用前利润',
                                            `pc_finance_fee` int(11) NOT NULL COMMENT '财务费用',
                                            `pc_profit_before_tax` int(11) NOT NULL COMMENT '税前利润',
                                            `pc_tax` int(11) NOT NULL COMMENT '所得税',
                                            `pc_annual_net_profit` int(11) NOT NULL COMMENT '净利润',
                                            PRIMARY KEY (`profit_chart_id`),
                                            KEY `(contest_id,student_id)` (`contest_id`,`student_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



CREATE TABLE `new_ct_profit_chart_season_like` (
                                                   `profit_chart_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '利润表id',
                                                   `student_id` int(11) NOT NULL COMMENT '学生id',
                                                   `contest_id` int(11) NOT NULL COMMENT '考试id',
                                                   `bs_isxt` tinyint(4) NOT NULL COMMENT '填写人：1代表系统自动生成，0代表学生录入',
                                                   `pc_date` tinyint(4) NOT NULL COMMENT '年份',
                                                   `pc_sales` int(11) NOT NULL COMMENT '销售额',
                                                   `pc_direct_cost` int(11) NOT NULL COMMENT '成本',
                                                   `pc_goods_profit` int(11) NOT NULL COMMENT '毛利',
                                                   `pc_total` int(11) NOT NULL COMMENT '管理费用',
                                                   `pc_profit_before_dep` int(11) NOT NULL COMMENT '折旧前利润',
                                                   `pc_dep` int(11) NOT NULL COMMENT '折旧',
                                                   `pc_profit_before_interests` int(11) NOT NULL COMMENT '财务费用前利润',
                                                   `pc_finance_fee` int(11) NOT NULL COMMENT '财务费用',
                                                   `pc_profit_before_tax` int(11) NOT NULL COMMENT '税前利润',
                                                   `pc_tax` int(11) NOT NULL COMMENT '所得税',
                                                   `pc_annual_net_profit` int(11) NOT NULL COMMENT '净利润',
                                                   PRIMARY KEY (`profit_chart_id`),
                                                   KEY `(contest_id,student_id)` (`contest_id`,`student_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;







CREATE TABLE `new_ct_t_time` (
                                 `time_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
                                 `student_id` int(11) NOT NULL COMMENT '学生id',
                                 `contest_id` int(11) NOT NULL COMMENT '竞赛id',
                                 `date` tinyint(4) NOT NULL COMMENT '考试时间',
                                 PRIMARY KEY (`time_id`),
                                 KEY `(contest_id,student_id)` (`contest_id`,`student_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;



CREATE TABLE `new_ct_t_time_like` (
                                      `time_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
                                      `student_id` int(11) NOT NULL COMMENT '学生id',
                                      `contest_id` int(11) NOT NULL COMMENT '竞赛id',
                                      `date` tinyint(4) NOT NULL COMMENT '考试时间',
                                      PRIMARY KEY (`time_id`),
                                      KEY `(contest_id,student_id)` (`contest_id`,`student_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;



CREATE TABLE `new_ct_t_time_season_like` (
                                             `time_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
                                             `student_id` int(11) NOT NULL COMMENT '学生id',
                                             `contest_id` int(11) NOT NULL COMMENT '竞赛id',
                                             `date` tinyint(4) NOT NULL COMMENT '考试时间',
                                             PRIMARY KEY (`time_id`),
                                             KEY `(contest_id,student_id)` (`contest_id`,`student_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;





CREATE TABLE `new_ct_yf_iso` (
                                 `iso_id` int(11) NOT NULL AUTO_INCREMENT,
                                 `student_id` int(11) NOT NULL,
                                 `contest_id` int(11) NOT NULL,
                                 `di_ciid` int(11) DEFAULT NULL COMMENT 'ISO规则_id',
                                 `di_total_date` tinyint(4) DEFAULT NULL COMMENT '总研发时间',
                                 `di_now_date` tinyint(4) DEFAULT NULL COMMENT '已研发时间',
                                 `di_remain_date` tinyint(4) DEFAULT NULL COMMENT '剩余研发时间',
                                 `di_finish_date` tinyint(4) DEFAULT NULL COMMENT '研发完成时间',
                                 PRIMARY KEY (`iso_id`),
                                 KEY `(contest_id,student_id)` (`contest_id`,`student_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


CREATE TABLE `new_ct_yf_iso_like` (
                                      `iso_id` int(11) NOT NULL AUTO_INCREMENT,
                                      `student_id` int(11) NOT NULL,
                                      `contest_id` int(11) NOT NULL,
                                      `di_ciid` int(11) DEFAULT NULL COMMENT 'ISO规则_id',
                                      `di_total_date` tinyint(4) DEFAULT NULL COMMENT '总研发时间',
                                      `di_now_date` tinyint(4) DEFAULT NULL COMMENT '已研发时间',
                                      `di_remain_date` tinyint(4) DEFAULT NULL COMMENT '剩余研发时间',
                                      `di_finish_date` tinyint(4) DEFAULT NULL COMMENT '研发完成时间',
                                      PRIMARY KEY (`iso_id`),
                                      KEY `(contest_id,student_id)` (`contest_id`,`student_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


CREATE TABLE `new_ct_yf_iso_season_like` (
                                             `iso_id` int(11) NOT NULL AUTO_INCREMENT,
                                             `student_id` int(11) NOT NULL,
                                             `contest_id` int(11) NOT NULL,
                                             `di_ciid` int(11) DEFAULT NULL COMMENT 'ISO规则_id',
                                             `di_total_date` tinyint(4) DEFAULT NULL COMMENT '总研发时间',
                                             `di_now_date` tinyint(4) DEFAULT NULL COMMENT '已研发时间',
                                             `di_remain_date` tinyint(4) DEFAULT NULL COMMENT '剩余研发时间',
                                             `di_finish_date` tinyint(4) DEFAULT NULL COMMENT '研发完成时间',
                                             PRIMARY KEY (`iso_id`),
                                             KEY `(contest_id,student_id)` (`contest_id`,`student_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;