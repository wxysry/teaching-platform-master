ALTER table new_ct_bank_loan ADD repayment_type VARCHAR(4)
ALTER table new_ct_bank_loan ADD interest  decimal(20,2)
ALTER table new_ct_bank_loan ADD rate  decimal(20,2) COMMENT '利率',
ALTER table new_ct_bank_loan ADD bl_name VARCHAR(50)

CREATE TABLE `new_ct_bank_loan_like` (
  `bl_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '贷款记录表',
  `student_id` int(11) NOT NULL,
  `contest_id` int(11) NOT NULL,
  `bl_type` tinyint(4) NOT NULL COMMENT '贷款类型:1表示长贷，2表示短贷',
  `bl_fee` int(11) NOT NULL COMMENT '贷款金额',
  `bl_add_time` tinyint(4) NOT NULL COMMENT '贷款时间',
  `bl_remain_time` tinyint(4) NOT NULL COMMENT '贷款时长',
  `bl_repayment_date` tinyint(4) NOT NULL COMMENT '还款时间',
  PRIMARY KEY (`bl_id`),
  KEY `(contest_id,student_id)` (`contest_id`,`student_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;


CREATE TABLE `new_ct_dd_material_like` (
  `dd_material_id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '原料采购记录',
  `om_cmid` int(10) NOT NULL COMMENT '原料id',
  `om_num` int(11) DEFAULT NULL COMMENT '数量',
  `student_id` int(11) DEFAULT NULL,
  `contest_id` int(11) DEFAULT NULL,
  `remain_date` tinyint(4) DEFAULT NULL COMMENT '剩余时间',
  `purchase_date` tinyint(4) DEFAULT NULL COMMENT '采购时间',
  PRIMARY KEY (`dd_material_id`),
  KEY `(contest_id,student_id)` (`contest_id`,`student_id`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8;

CREATE TABLE `new_ct_fee_like` (
  `fee_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '应收款',
  `student_id` int(11) NOT NULL,
  `contest_id` int(11) NOT NULL,
  `r_fee` int(11) NOT NULL COMMENT '金额',
  `r_remain_date` tinyint(4) NOT NULL COMMENT '账期',
  PRIMARY KEY (`fee_id`),
  KEY `(contest_id,student_id)` (`contest_id`,`student_id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;


CREATE TABLE `new_ct_kc_material_like` (
  `material_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `student_id` int(11) DEFAULT NULL,
  `contest_id` int(11) DEFAULT NULL,
  `im_cm_id` int(11) DEFAULT NULL COMMENT '原料id',
  `im_num` int(11) DEFAULT NULL COMMENT '剩余数量',
  PRIMARY KEY (`material_id`),
  KEY `(contest_id,student_id)` (`contest_id`,`student_id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;

CREATE TABLE `new_ct_kc_product_like` (
  `kc_product_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `student_id` int(11) DEFAULT NULL,
  `contest_id` int(11) DEFAULT NULL,
  `ip_cp_id` int(11) DEFAULT NULL COMMENT '产品id',
  `ip_num` int(11) DEFAULT NULL COMMENT '库存数量',
  PRIMARY KEY (`kc_product_id`),
  KEY `(contest_id,student_id)` (`contest_id`,`student_id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;

CREATE TABLE `new_ct_line_like` (
  `line_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `student_id` int(11) NOT NULL,
  `contest_id` int(11) NOT NULL,
  `workshop_id` int(11) NOT NULL COMMENT '所属厂房id（ct_workshop）',
  `pl_wid` int(11) NOT NULL COMMENT '所属厂房类型id(ct_gz_workshop)',
  `pl_cplid` int(11) NOT NULL COMMENT '生产线类型id',
  `pl_cpid` int(11) NOT NULL COMMENT '生产产品id',
  `pl_invest` int(11) NOT NULL COMMENT '原值',
  `pl_dep_total` int(11) NOT NULL COMMENT '累计折旧',
  `pl_product_add_date` tinyint(4) DEFAULT '0' COMMENT '开产时间',
  `pl_producting_date` tinyint(4) DEFAULT '0' COMMENT '剩余生产时间',
  `pl_finish_date` tinyint(4) DEFAULT '0' COMMENT '建成时间',
  `pl_remain_date` tinyint(4) DEFAULT '0' COMMENT '在建剩余时间',
  `pl_add_time` tinyint(4) DEFAULT '0' COMMENT '开建时间',
  `pl_transfer_add_date` tinyint(4) DEFAULT '0' COMMENT '转产时间',
  `pl_transfer_date` tinyint(4) DEFAULT '0' COMMENT '转产剩余时间',
  PRIMARY KEY (`line_id`),
  KEY `(contest_id,student_id)` (`contest_id`,`student_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

CREATE TABLE `new_ct_profit_chart_like` (
  `profit_chart_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '利润表id',
  `student_id` int(11) NOT NULL COMMENT '学生id',
  `contest_id` int(11) NOT NULL COMMENT '考试id',
  `bs_isxt` tinyint(4) NOT NULL COMMENT '填写人：1代表系统自动生成，0代表学生录入',
  `pc_date` tinyint(4) NOT NULL COMMENT '年份',
  `pc_sales` int(11) NOT NULL COMMENT '销售额',
  `pc_direct_cost` int(11) NOT NULL COMMENT '成本',
  `pc_goods_profit` int(11) NOT NULL COMMENT '毛利',
  `pc_total` int(11) NOT NULL COMMENT '管理费用',
  `pc_profit_before_dep` int(11) NOT NULL COMMENT '折旧前利润',
  `pc_dep` int(11) NOT NULL COMMENT '折旧',
  `pc_profit_before_interests` int(11) NOT NULL COMMENT '财务费用前利润',
  `pc_finance_fee` int(11) NOT NULL COMMENT '财务费用',
  `pc_profit_before_tax` int(11) NOT NULL COMMENT '税前利润',
  `pc_tax` int(11) NOT NULL COMMENT '所得税',
  `pc_annual_net_profit` int(11) NOT NULL COMMENT '净利润',
  PRIMARY KEY (`profit_chart_id`),
  KEY `(contest_id,student_id)` (`contest_id`,`student_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;


CREATE TABLE `new_ct_t_time_like` (
  `time_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `student_id` int(11) NOT NULL COMMENT '学生id',
  `contest_id` int(11) NOT NULL COMMENT '竞赛id',
  `date` tinyint(4) NOT NULL COMMENT '考试时间',
  PRIMARY KEY (`time_id`),
  KEY `(contest_id,student_id)` (`contest_id`,`student_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

CREATE TABLE `new_ct_workshop_like` (
  `workshop_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `student_id` int(11) NOT NULL,
  `contest_id` int(11) NOT NULL,
  `w_cwid` int(11) DEFAULT NULL,
  `w_status` tinyint(4) NOT NULL COMMENT '租售类型:0表示BUY,1表示RENT',
  `w_surplus_capacity` int(11) DEFAULT NULL COMMENT '剩余容量',
  `w_pay_date` int(11) DEFAULT NULL COMMENT '最后付租',
  `get_date` tinyint(4) DEFAULT NULL COMMENT '置办时间',
  PRIMARY KEY (`workshop_id`),
  KEY `(contest_id,student_id)` (`contest_id`,`student_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;


CREATE TABLE `new_ct_xz_order_like` (
  `order_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `co_id` varchar(50) NOT NULL COMMENT '订单id',
  `student_id` int(11) NOT NULL,
  `contest_id` int(11) NOT NULL,
  `date` tinyint(4) DEFAULT NULL COMMENT '年份',
  `cm_id` int(11) DEFAULT NULL COMMENT '市场',
  `cp_id` int(11) DEFAULT NULL COMMENT '产品',
  `num` int(11) DEFAULT NULL COMMENT '数量',
  `total_price` int(11) DEFAULT NULL COMMENT '总价',
  `delivery_date` tinyint(4) DEFAULT NULL COMMENT '交货期',
  `payment_date` tinyint(4) DEFAULT NULL COMMENT '账期',
  `iso_id` int(11) DEFAULT NULL COMMENT 'ISO',
  `commit_date` tinyint(4) DEFAULT NULL COMMENT '交单时间',
  PRIMARY KEY (`order_id`),
  KEY `(contest_id,student_id)` (`contest_id`,`student_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

CREATE TABLE `new_ct_yf_iso_like` (
  `iso_id` int(11) NOT NULL AUTO_INCREMENT,
  `student_id` int(11) NOT NULL,
  `contest_id` int(11) NOT NULL,
  `di_ciid` int(11) DEFAULT NULL COMMENT 'ISO规则_id',
  `di_total_date` tinyint(4) DEFAULT NULL COMMENT '总研发时间',
  `di_now_date` tinyint(4) DEFAULT NULL COMMENT '已研发时间',
  `di_remain_date` tinyint(4) DEFAULT NULL COMMENT '剩余研发时间',
  `di_finish_date` tinyint(4) DEFAULT NULL COMMENT '研发完成时间',
  PRIMARY KEY (`iso_id`),
  KEY `(contest_id,student_id)` (`contest_id`,`student_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;


CREATE TABLE `new_ct_yf_market_like` (
  `market_id` int(11) NOT NULL AUTO_INCREMENT,
  `student_id` int(11) DEFAULT NULL,
  `contest_id` int(11) DEFAULT NULL,
  `dm_cm_id` int(11) DEFAULT NULL COMMENT '市场规则id',
  `dm_total_date` tinyint(4) DEFAULT NULL COMMENT '总研发时间',
  `dm_now_date` tinyint(4) DEFAULT NULL COMMENT '已研发时间',
  `dm_remain_date` tinyint(4) DEFAULT NULL COMMENT '剩余研发时间',
  `dm_finish_date` tinyint(4) DEFAULT NULL COMMENT '研发完成时间',
  PRIMARY KEY (`market_id`),
  KEY `(contest_id,student_id)` (`contest_id`,`student_id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;

CREATE TABLE `new_ct_yf_product_like` (
  `product_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `student_id` int(11) DEFAULT NULL,
  `contest_id` int(11) DEFAULT NULL,
  `dp_cp_id` int(11) DEFAULT NULL COMMENT '产品规则id',
  `dp_total_date` tinyint(4) DEFAULT NULL COMMENT '总研发时间',
  `dp_now_date` tinyint(4) DEFAULT NULL COMMENT '已研发时间',
  `dp_remain_date` tinyint(4) DEFAULT NULL COMMENT '剩余研发时间',
  `dp_finish_date` tinyint(4) DEFAULT NULL COMMENT '研发完成时间',
  PRIMARY KEY (`product_id`),
  KEY `(contest_id,student_id)` (`contest_id`,`student_id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;

CREATE TABLE `new_ct_bank_loan_season_like` (
  `bl_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '贷款记录表',
  `student_id` int(11) NOT NULL,
  `contest_id` int(11) NOT NULL,
  `bl_type` tinyint(4) NOT NULL COMMENT '贷款类型:1表示长贷，2表示短贷',
  `bl_fee` int(11) NOT NULL COMMENT '贷款金额',
  `bl_add_time` tinyint(4) NOT NULL COMMENT '贷款时间',
  `bl_remain_time` tinyint(4) NOT NULL COMMENT '贷款时长',
  `bl_repayment_date` tinyint(4) NOT NULL COMMENT '还款时间',
  PRIMARY KEY (`bl_id`),
  KEY `(contest_id,student_id)` (`contest_id`,`student_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

CREATE TABLE `new_ct_dd_material_season_like` (
  `dd_material_id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '原料采购记录',
  `om_cmid` int(10) NOT NULL COMMENT '原料id',
  `om_num` int(11) DEFAULT NULL COMMENT '数量',
  `student_id` int(11) DEFAULT NULL,
  `contest_id` int(11) DEFAULT NULL,
  `remain_date` tinyint(4) DEFAULT NULL COMMENT '剩余时间',
  `purchase_date` tinyint(4) DEFAULT NULL COMMENT '采购时间',
  PRIMARY KEY (`dd_material_id`),
  KEY `(contest_id,student_id)` (`contest_id`,`student_id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

CREATE TABLE `new_ct_fee_season_like` (
  `fee_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '应收款',
  `student_id` int(11) NOT NULL,
  `contest_id` int(11) NOT NULL,
  `r_fee` int(11) NOT NULL COMMENT '金额',
  `r_remain_date` tinyint(4) NOT NULL COMMENT '账期',
  PRIMARY KEY (`fee_id`),
  KEY `(contest_id,student_id)` (`contest_id`,`student_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

CREATE TABLE `new_ct_kc_material_season_like` (
  `material_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `student_id` int(11) DEFAULT NULL,
  `contest_id` int(11) DEFAULT NULL,
  `im_cm_id` int(11) DEFAULT NULL COMMENT '原料id',
  `im_num` int(11) DEFAULT NULL COMMENT '剩余数量',
  PRIMARY KEY (`material_id`),
  KEY `(contest_id,student_id)` (`contest_id`,`student_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

CREATE TABLE `new_ct_kc_product_season_like` (
  `kc_product_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '产成品库存',
  `student_id` int(11) DEFAULT NULL,
  `contest_id` int(11) DEFAULT NULL,
  `ip_cp_id` int(11) DEFAULT NULL COMMENT '产品id',
  `ip_num` int(11) DEFAULT NULL COMMENT '库存数量',
  PRIMARY KEY (`kc_product_id`),
  KEY `(contest_id,student_id)` (`contest_id`,`student_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;


CREATE TABLE `new_ct_line_season_like` (
  `line_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `student_id` int(11) NOT NULL,
  `contest_id` int(11) NOT NULL,
  `workshop_id` int(11) NOT NULL COMMENT '所属厂房id（ct_workshop）',
  `pl_wid` int(11) NOT NULL COMMENT '所属厂房类型id(ct_gz_workshop)',
  `pl_cplid` int(11) NOT NULL COMMENT '生产线类型id',
  `pl_cpid` int(11) NOT NULL COMMENT '生产产品id',
  `pl_invest` int(11) NOT NULL COMMENT '原值',
  `pl_dep_total` int(11) NOT NULL COMMENT '累计折旧',
  `pl_product_add_date` tinyint(4) DEFAULT '0' COMMENT '开产时间',
  `pl_producting_date` tinyint(4) DEFAULT '0' COMMENT '剩余生产时间',
  `pl_finish_date` tinyint(4) DEFAULT '0' COMMENT '建成时间',
  `pl_remain_date` tinyint(4) DEFAULT '0' COMMENT '在建剩余时间',
  `pl_add_time` tinyint(4) DEFAULT '0' COMMENT '开建时间',
  `pl_transfer_add_date` tinyint(4) DEFAULT '0' COMMENT '转产时间',
  `pl_transfer_date` tinyint(4) DEFAULT '0' COMMENT '转产剩余时间',
  PRIMARY KEY (`line_id`),
  KEY `(contest_id,student_id)` (`contest_id`,`student_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;


CREATE TABLE `new_ct_profit_chart_season_like` (
  `profit_chart_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '利润表id',
  `student_id` int(11) NOT NULL COMMENT '学生id',
  `contest_id` int(11) NOT NULL COMMENT '考试id',
  `bs_isxt` tinyint(4) NOT NULL COMMENT '填写人：1代表系统自动生成，0代表学生录入',
  `pc_date` tinyint(4) NOT NULL COMMENT '年份',
  `pc_sales` int(11) NOT NULL COMMENT '销售额',
  `pc_direct_cost` int(11) NOT NULL COMMENT '成本',
  `pc_goods_profit` int(11) NOT NULL COMMENT '毛利',
  `pc_total` int(11) NOT NULL COMMENT '管理费用',
  `pc_profit_before_dep` int(11) NOT NULL COMMENT '折旧前利润',
  `pc_dep` int(11) NOT NULL COMMENT '折旧',
  `pc_profit_before_interests` int(11) NOT NULL COMMENT '财务费用前利润',
  `pc_finance_fee` int(11) NOT NULL COMMENT '财务费用',
  `pc_profit_before_tax` int(11) NOT NULL COMMENT '税前利润',
  `pc_tax` int(11) NOT NULL COMMENT '所得税',
  `pc_annual_net_profit` int(11) NOT NULL COMMENT '净利润',
  PRIMARY KEY (`profit_chart_id`),
  KEY `(contest_id,student_id)` (`contest_id`,`student_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;


CREATE TABLE `new_ct_t_time_season_like` (
  `time_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `student_id` int(11) NOT NULL COMMENT '学生id',
  `contest_id` int(11) NOT NULL COMMENT '竞赛id',
  `date` tinyint(4) NOT NULL COMMENT '考试时间',
  PRIMARY KEY (`time_id`),
  KEY `(contest_id,student_id)` (`contest_id`,`student_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;


CREATE TABLE `new_ct_workshop_season_like` (
  `workshop_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `student_id` int(11) NOT NULL,
  `contest_id` int(11) NOT NULL,
  `w_cwid` int(11) NOT NULL COMMENT '厂房规则表里的id',
  `w_status` tinyint(4) NOT NULL COMMENT '租售类型:0表示BUY,1表示RENT',
  `w_surplus_capacity` int(11) DEFAULT NULL COMMENT '剩余容量',
  `w_pay_date` int(11) DEFAULT NULL COMMENT '最后付租',
  `get_date` tinyint(4) DEFAULT NULL COMMENT '置办时间',
  PRIMARY KEY (`workshop_id`),
  KEY `(contest_id,student_id)` (`contest_id`,`student_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

CREATE TABLE `new_ct_xz_order_season_like` (
  `order_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `co_id` varchar(50) NOT NULL COMMENT '订单编号',
  `student_id` int(11) NOT NULL,
  `contest_id` int(11) NOT NULL,
  `date` tinyint(4) DEFAULT NULL COMMENT '年份',
  `cm_id` int(11) DEFAULT NULL COMMENT '市场',
  `cp_id` int(11) DEFAULT NULL COMMENT '产品',
  `num` int(11) DEFAULT NULL COMMENT '数量',
  `total_price` int(11) DEFAULT NULL COMMENT '总价',
  `delivery_date` tinyint(4) DEFAULT NULL COMMENT '交货期',
  `payment_date` tinyint(4) DEFAULT NULL COMMENT '账期',
  `iso_id` int(11) DEFAULT NULL COMMENT 'ISO',
  `commit_date` tinyint(4) DEFAULT NULL COMMENT '交单时间',
  PRIMARY KEY (`order_id`),
  KEY `(contest_id,student_id)` (`contest_id`,`student_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

CREATE TABLE `new_ct_yf_iso_season_like` (
  `iso_id` int(11) NOT NULL AUTO_INCREMENT,
  `student_id` int(11) NOT NULL,
  `contest_id` int(11) NOT NULL,
  `di_ciid` int(11) DEFAULT NULL COMMENT 'ISO规则_id',
  `di_total_date` tinyint(4) DEFAULT NULL COMMENT '总研发时间',
  `di_now_date` tinyint(4) DEFAULT NULL COMMENT '已研发时间',
  `di_remain_date` tinyint(4) DEFAULT NULL COMMENT '剩余研发时间',
  `di_finish_date` tinyint(4) DEFAULT NULL COMMENT '研发完成时间',
  PRIMARY KEY (`iso_id`),
  KEY `(contest_id,student_id)` (`contest_id`,`student_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;


CREATE TABLE `new_ct_yf_market_season_like` (
  `market_id` int(11) NOT NULL AUTO_INCREMENT,
  `student_id` int(11) DEFAULT NULL,
  `contest_id` int(11) DEFAULT NULL,
  `dm_cm_id` int(11) DEFAULT NULL COMMENT '市场规则id',
  `dm_total_date` tinyint(4) DEFAULT NULL COMMENT '总研发时间',
  `dm_now_date` tinyint(4) DEFAULT NULL COMMENT '已研发时间',
  `dm_remain_date` tinyint(4) DEFAULT NULL COMMENT '剩余研发时间',
  `dm_finish_date` tinyint(4) DEFAULT NULL COMMENT '研发完成时间',
  PRIMARY KEY (`market_id`),
  KEY `(contest_id,student_id)` (`contest_id`,`student_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

CREATE TABLE `new_ct_yf_product_season_like` (
  `product_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `student_id` int(11) DEFAULT NULL,
  `contest_id` int(11) DEFAULT NULL,
  `dp_cp_id` int(11) DEFAULT NULL COMMENT '产品规则id',
  `dp_total_date` tinyint(4) DEFAULT NULL COMMENT '总研发时间',
  `dp_now_date` tinyint(4) DEFAULT NULL COMMENT '已研发时间',
  `dp_remain_date` tinyint(4) DEFAULT NULL COMMENT '剩余研发时间',
  `dp_finish_date` tinyint(4) DEFAULT NULL COMMENT '研发完成时间',
  PRIMARY KEY (`product_id`),
  KEY `(contest_id,student_id)` (`contest_id`,`student_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

CREATE TABLE `new_ct_profit_chart_like` (
  `profit_chart_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '利润表id',
  `student_id` int(11) NOT NULL COMMENT '学生id',
  `contest_id` int(11) NOT NULL COMMENT '考试id',
  `bs_isxt` tinyint(4) NOT NULL COMMENT '填写人：1代表系统自动生成，0代表学生录入',
  `pc_date` tinyint(4) NOT NULL COMMENT '年份',
  `pc_sales` int(11) NOT NULL COMMENT '销售额',
  `pc_direct_cost` int(11) NOT NULL COMMENT '成本',
  `pc_goods_profit` int(11) NOT NULL COMMENT '毛利',
  `pc_total` int(11) NOT NULL COMMENT '管理费用',
  `pc_profit_before_dep` int(11) NOT NULL COMMENT '折旧前利润',
  `pc_dep` int(11) NOT NULL COMMENT '折旧',
  `pc_profit_before_interests` int(11) NOT NULL COMMENT '财务费用前利润',
  `pc_finance_fee` int(11) NOT NULL COMMENT '财务费用',
  `pc_profit_before_tax` int(11) NOT NULL COMMENT '税前利润',
  `pc_tax` int(11) NOT NULL COMMENT '所得税',
  `pc_annual_net_profit` int(11) NOT NULL COMMENT '净利润',
  PRIMARY KEY (`profit_chart_id`),
  KEY `(contest_id,student_id)` (`contest_id`,`student_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

CREATE TABLE `new_ct_profit_chart` (
  `profit_chart_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '利润表id',
  `student_id` int(11) NOT NULL COMMENT '学生id',
  `contest_id` int(11) NOT NULL COMMENT '考试id',
  `bs_isxt` tinyint(4) NOT NULL COMMENT '填写人：1代表系统自动生成，0代表学生录入',
  `pc_date` tinyint(4) NOT NULL COMMENT '年份',
  `pc_sales` int(11) NOT NULL COMMENT '销售额',
  `pc_direct_cost` int(11) NOT NULL COMMENT '成本',
  `pc_goods_profit` int(11) NOT NULL COMMENT '毛利',
  `pc_total` int(11) NOT NULL COMMENT '管理费用',
  `pc_profit_before_dep` int(11) NOT NULL COMMENT '折旧前利润',
  `pc_dep` int(11) NOT NULL COMMENT '折旧',
  `pc_profit_before_interests` int(11) NOT NULL COMMENT '财务费用前利润',
  `pc_finance_fee` int(11) NOT NULL COMMENT '财务费用',
  `pc_profit_before_tax` int(11) NOT NULL COMMENT '税前利润',
  `pc_tax` int(11) NOT NULL COMMENT '所得税',
  `pc_annual_net_profit` int(11) NOT NULL COMMENT '净利润',
  PRIMARY KEY (`profit_chart_id`),
  KEY `(contest_id,student_id)` (`contest_id`,`student_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

CREATE TABLE `new_ct_t_time` (
  `time_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `student_id` int(11) NOT NULL COMMENT '学生id',
  `contest_id` int(11) NOT NULL COMMENT '竞赛id',
  `date` tinyint(4) NOT NULL COMMENT '考试时间',
  PRIMARY KEY (`time_id`),
  KEY `(contest_id,student_id)` (`contest_id`,`student_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;