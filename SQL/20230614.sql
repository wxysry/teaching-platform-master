CREATE TABLE `new_ct_worker` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '工人ID',
  `student_id` int(11) NOT NULL COMMENT '学生id',
  `contest_id` int(11) NOT NULL COMMENT '竞赛ID',
  `worker_name` varchar(50) NOT NULL COMMENT '姓名',
  `worker_num` int(11) NOT NULL COMMENT '序号',
  `recruit_name` varchar(20) NOT NULL COMMENT '等级',
  `recruit_num` varchar(20) NOT NULL COMMENT '等级编码',
  `mult_bonus` int(11) NOT NULL COMMENT '倍数加成(%)',
  `init_sal` int(11) NOT NULL COMMENT '工资(元)',
  `start_work_time` int(11) NOT NULL COMMENT '招聘时间',
  `is_work` int(11) NOT NULL COMMENT '是否在职',
  `piece` int(11) DEFAULT NULL COMMENT '计件',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;