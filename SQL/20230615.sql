ALTER TABLE new_ct_gz_material ADD COLUMN num INT(10) COMMENT '数量';


CREATE TABLE `new_ct_dd_material` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '原料采购记录',
  `om_cmid` int(10) NOT NULL COMMENT '原料编号',
	`cm_name` VARCHAR(50) NOT NULL COMMENT '原料名称',
	 `student_id` int(11) DEFAULT NULL,
  `contest_id` int(11) DEFAULT NULL,
  `om_num` int(11) DEFAULT NULL COMMENT '数量',

  `remain_date` tinyint(4) DEFAULT NULL COMMENT '收货期',
  `purchase_date` tinyint(4) DEFAULT NULL COMMENT '采购时间',
	  `is_inventory` tinyint(4) DEFAULT NULL COMMENT '是否入库',
			  `pay_date` tinyint(4) DEFAULT NULL COMMENT '付款周期（账期）',
							  `is_pay` tinyint(4) DEFAULT NULL COMMENT '是否付款',
															  `price`  int(11) DEFAULT NULL COMMENT '价格',
	`num`  int(11) DEFAULT NULL COMMENT '数量',
  PRIMARY KEY (`id`),
  KEY `(contest_id,student_id)` (`contest_id`,`student_id`)
) ENGINE=InnoDB AUTO_INCREMENT=65 DEFAULT CHARSET=utf8;





ALTER TABLE new_ct_kc_material ADD material_name VARCHAR(11) DEFAULT NULL COMMENT '原料名称';
ALTER TABLE new_ct_kc_material ADD in_inventory_date tinyint(4) DEFAULT NULL COMMENT '入库日期';
ALTER TABLE new_ct_kc_material ADD material_price int(11) DEFAULT NULL COMMENT '成本';