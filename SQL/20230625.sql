alter table new_ct_kc_product ADD COLUMN line_id INT(10) COMMENT '生产线id';
alter table new_ct_kc_product ADD COLUMN is_inventory INT(10) COMMENT '是否入库';


alter table new_ct_yf_product ADD COLUMN dp_start_date INT(10) COMMENT '申请时间';
alter table new_ct_yf_product ADD COLUMN dp_state varchar (10) COMMENT '状态';

alter table new_ct_yf_market ADD COLUMN dm_start_date INT(10) COMMENT '申请时间';
alter table new_ct_yf_market ADD COLUMN dm_state varchar(10) COMMENT '状态';

alter table new_ct_yf_iso ADD COLUMN di_start_date INT(10) COMMENT '申请时间';
alter table new_ct_yf_iso ADD COLUMN di_state varchar(10) COMMENT '状态';