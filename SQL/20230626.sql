alter table new_ct_kc_product ADD COLUMN inventory_date INT(10) COMMENT '入库日期';
alter table new_ct_kc_product ADD COLUMN real_cost varchar (10) COMMENT '真实成本';

alter table new_ct_fee ADD COLUMN borrower varchar (10) COMMENT '借方';
alter table new_ct_fee ADD COLUMN lender varchar (10) COMMENT '贷方';
alter table new_ct_fee ADD COLUMN remarks varchar (10) COMMENT '备注';
alter table new_ct_fee ADD COLUMN payment_date varchar (10) COMMENT '收款日期';
alter table new_ct_fee ADD COLUMN type varchar (10) COMMENT '类型';
