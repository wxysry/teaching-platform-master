-- new_ct_gz_ad 增加季度
ALTER TABLE `new_ct_gz_ad` ADD COLUMN `quarterly` INT(11) DEFAULT NULL COMMENT '季度' AFTER `year`;

ALTER TABLE `new_ct_mn_ad` ADD COLUMN `quarterly` INT(11) DEFAULT NULL COMMENT '季度' AFTER `year`;

ALTER TABLE `new_ct_mn_ad_like` ADD COLUMN `quarterly` INT(11) DEFAULT NULL COMMENT '季度' AFTER `year`;

ALTER TABLE `new_ct_mn_ad_season_like` ADD COLUMN `quarterly` INT(11) DEFAULT NULL COMMENT '季度' AFTER `year`;




ALTER TABLE `new_ct_mn_choose` ADD COLUMN `quarterly` INT(11) DEFAULT NULL COMMENT '季度' AFTER `date`;

ALTER TABLE `new_ct_mn_choose_like` ADD COLUMN `quarterly` INT(11) DEFAULT NULL COMMENT '季度' AFTER `date`;

ALTER TABLE `new_ct_mn_choose_season_like` ADD COLUMN `quarterly` INT(11) DEFAULT NULL COMMENT '季度' AFTER `date`;


ALTER TABLE `new_ct_line` ADD COLUMN `real_production` INT(11) DEFAULT NULL COMMENT '产量';
ALTER TABLE `new_ct_line` ADD COLUMN `classes_id` INT(11) DEFAULT NULL COMMENT '班次id';
ALTER TABLE new_ct_dd_material DROP COLUMN om_num;




