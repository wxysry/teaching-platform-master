-- 综合费用表
-- new_ct_charges
drop table new_ct_charges;
drop table new_ct_charges_like;
drop table new_ct_charges_season_like;
CREATE TABLE `new_ct_charges` (
                                  `charges_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '费用表id',
                                  `student_id` int(11) NOT NULL COMMENT '学生id',
                                  `contest_id` int(11) NOT NULL COMMENT '考试id',
                                  `bs_isxt` tinyint(4) NOT NULL COMMENT '填写人：1代表系统自动生成，0代表学生录入',
                                  `c_date` tinyint(4) NOT NULL COMMENT '年份',
                                  `c_overhaul` int(11) NOT NULL COMMENT '管理费用',
                                  `c_ad` int(11) NOT NULL COMMENT '广告费用',
                                  `c_maintenance` int(11) NOT NULL COMMENT '维修费',
                                  `c_transfer` int(11) NOT NULL COMMENT '转产费',
                                  `c_develop_market` int(11) NOT NULL COMMENT '市场开拓',
                                  `c_develop_product` int(11) NOT NULL COMMENT '产品研发',
                                  `c_develop_iso` int(11) NOT NULL COMMENT 'ISO开拓',
                                  `c_information` int(11) NOT NULL COMMENT '信息费',
                                  `c_hr` int(11) NOT NULL COMMENT '人力费',
                                  `c_digitalization` int(11) NOT NULL COMMENT '数字化研发费',
                                  `c_total` int(11) NOT NULL COMMENT '合计',
                                  `is_submit` varchar(1) NOT NULL DEFAULT 'Y' COMMENT '是否已提交,N未提交 Y已提交',
                                  PRIMARY KEY (`charges_id`),
                                  KEY `(contest_id,student_id)` (`contest_id`,`student_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



CREATE TABLE `new_ct_charges_like` (
                                       `charges_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '费用表id',
                                       `student_id` int(11) NOT NULL COMMENT '学生id',
                                       `contest_id` int(11) NOT NULL COMMENT '考试id',
                                       `bs_isxt` tinyint(4) NOT NULL COMMENT '填写人：1代表系统自动生成，0代表学生录入',
                                       `c_date` tinyint(4) NOT NULL COMMENT '年份',
                                       `c_overhaul` int(11) NOT NULL COMMENT '管理费用',
                                       `c_ad` int(11) NOT NULL COMMENT '广告费用',
                                       `c_maintenance` int(11) NOT NULL COMMENT '维修费',
                                       `c_transfer` int(11) NOT NULL COMMENT '转产费',
                                       `c_develop_market` int(11) NOT NULL COMMENT '市场开拓',
                                       `c_develop_product` int(11) NOT NULL COMMENT '产品研发',
                                       `c_develop_iso` int(11) NOT NULL COMMENT 'ISO开拓',
                                       `c_information` int(11) NOT NULL COMMENT '信息费',
                                       `c_hr` int(11) NOT NULL COMMENT '人力费',
                                       `c_digitalization` int(11) NOT NULL COMMENT '数字化研发费',
                                       `c_total` int(11) NOT NULL COMMENT '合计',
                                       `is_submit` varchar(1) NOT NULL DEFAULT 'Y' COMMENT '是否已提交,N未提交 Y已提交',
                                       PRIMARY KEY (`charges_id`),
                                       KEY `(contest_id,student_id)` (`contest_id`,`student_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



CREATE TABLE `new_ct_charges_season_like` (
                                              `charges_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '费用表id',
                                              `student_id` int(11) NOT NULL COMMENT '学生id',
                                              `contest_id` int(11) NOT NULL COMMENT '考试id',
                                              `bs_isxt` tinyint(4) NOT NULL COMMENT '填写人：1代表系统自动生成，0代表学生录入',
                                              `c_date` tinyint(4) NOT NULL COMMENT '年份',
                                              `c_overhaul` int(11) NOT NULL COMMENT '管理费用',
                                              `c_ad` int(11) NOT NULL COMMENT '广告费用',
                                              `c_maintenance` int(11) NOT NULL COMMENT '维修费',
                                              `c_transfer` int(11) NOT NULL COMMENT '转产费',
                                              `c_develop_market` int(11) NOT NULL COMMENT '市场开拓',
                                              `c_develop_product` int(11) NOT NULL COMMENT '产品研发',
                                              `c_develop_iso` int(11) NOT NULL COMMENT 'ISO开拓',
                                              `c_information` int(11) NOT NULL COMMENT '信息费',
                                              `c_hr` int(11) NOT NULL COMMENT '人力费',
                                              `c_digitalization` int(11) NOT NULL COMMENT '数字化研发费',
                                              `c_total` int(11) NOT NULL COMMENT '合计',
                                              `is_submit` varchar(1) NOT NULL DEFAULT 'Y' COMMENT '是否已提交,N未提交 Y已提交',
                                              PRIMARY KEY (`charges_id`),
                                              KEY `(contest_id,student_id)` (`contest_id`,`student_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;








-- 利润表
-- new_ct_profit_chart
drop table new_ct_profit_chart;
drop table new_ct_profit_chart_like;
drop table new_ct_profit_chart_season_like;

CREATE TABLE `new_ct_profit_chart` (
                                       `profit_chart_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '利润表id',
                                       `student_id` int(11) NOT NULL COMMENT '学生id',
                                       `contest_id` int(11) NOT NULL COMMENT '考试id',
                                       `bs_isxt` tinyint(4) NOT NULL COMMENT '填写人：1代表系统自动生成，0代表学生录入',
                                       `pc_date` tinyint(4) NOT NULL COMMENT '年份',
                                       `pc_sales` int(11) NOT NULL COMMENT '销售额',
                                       `pc_direct_cost` int(11) NOT NULL COMMENT '成本',
                                       `pc_goods_profit` int(11) NOT NULL COMMENT '毛利',
                                       `pc_total` int(11) NOT NULL COMMENT '综合费用',
                                       `pc_profit_before_dep` int(11) NOT NULL COMMENT '折旧前利润',
                                       `pc_dep` int(11) NOT NULL COMMENT '折旧',
                                       `pc_profit_before_interests` int(11) NOT NULL COMMENT '支付利息前利润',
                                       `pc_finance_fee` int(11) NOT NULL COMMENT '财务费用',
                                       `pc_non_operating` int(11) NOT NULL COMMENT '营业外收支',
                                       `pc_profit_before_tax` int(11) NOT NULL COMMENT '税前利润',
                                       `pc_tax` int(11) NOT NULL COMMENT '所得税',
                                       `pc_annual_net_profit` int(11) NOT NULL COMMENT '净利润',
                                       `is_submit` varchar(1) NOT NULL DEFAULT 'Y' COMMENT '是否已提交,N未提交 Y已提交',
                                       PRIMARY KEY (`profit_chart_id`),
                                       KEY `(contest_id,student_id)` (`contest_id`,`student_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


CREATE TABLE `new_ct_profit_chart_like` (
                                            `profit_chart_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '利润表id',
                                            `student_id` int(11) NOT NULL COMMENT '学生id',
                                            `contest_id` int(11) NOT NULL COMMENT '考试id',
                                            `bs_isxt` tinyint(4) NOT NULL COMMENT '填写人：1代表系统自动生成，0代表学生录入',
                                            `pc_date` tinyint(4) NOT NULL COMMENT '年份',
                                            `pc_sales` int(11) NOT NULL COMMENT '销售额',
                                            `pc_direct_cost` int(11) NOT NULL COMMENT '成本',
                                            `pc_goods_profit` int(11) NOT NULL COMMENT '毛利',
                                            `pc_total` int(11) NOT NULL COMMENT '综合费用',
                                            `pc_profit_before_dep` int(11) NOT NULL COMMENT '折旧前利润',
                                            `pc_dep` int(11) NOT NULL COMMENT '折旧',
                                            `pc_profit_before_interests` int(11) NOT NULL COMMENT '支付利息前利润',
                                            `pc_finance_fee` int(11) NOT NULL COMMENT '财务费用',
                                            `pc_non_operating` int(11) NOT NULL COMMENT '营业外收支',
                                            `pc_profit_before_tax` int(11) NOT NULL COMMENT '税前利润',
                                            `pc_tax` int(11) NOT NULL COMMENT '所得税',
                                            `pc_annual_net_profit` int(11) NOT NULL COMMENT '净利润',
                                            `is_submit` varchar(1) NOT NULL DEFAULT 'Y' COMMENT '是否已提交,N未提交 Y已提交',
                                            PRIMARY KEY (`profit_chart_id`),
                                            KEY `(contest_id,student_id)` (`contest_id`,`student_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


CREATE TABLE `new_ct_profit_chart_season_like` (
                                                   `profit_chart_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '利润表id',
                                                   `student_id` int(11) NOT NULL COMMENT '学生id',
                                                   `contest_id` int(11) NOT NULL COMMENT '考试id',
                                                   `bs_isxt` tinyint(4) NOT NULL COMMENT '填写人：1代表系统自动生成，0代表学生录入',
                                                   `pc_date` tinyint(4) NOT NULL COMMENT '年份',
                                                   `pc_sales` int(11) NOT NULL COMMENT '销售额',
                                                   `pc_direct_cost` int(11) NOT NULL COMMENT '成本',
                                                   `pc_goods_profit` int(11) NOT NULL COMMENT '毛利',
                                                   `pc_total` int(11) NOT NULL COMMENT '综合费用',
                                                   `pc_profit_before_dep` int(11) NOT NULL COMMENT '折旧前利润',
                                                   `pc_dep` int(11) NOT NULL COMMENT '折旧',
                                                   `pc_profit_before_interests` int(11) NOT NULL COMMENT '支付利息前利润',
                                                   `pc_finance_fee` int(11) NOT NULL COMMENT '财务费用',
                                                   `pc_non_operating` int(11) NOT NULL COMMENT '营业外收支',
                                                   `pc_profit_before_tax` int(11) NOT NULL COMMENT '税前利润',
                                                   `pc_tax` int(11) NOT NULL COMMENT '所得税',
                                                   `pc_annual_net_profit` int(11) NOT NULL COMMENT '净利润',
                                                   `is_submit` varchar(1) NOT NULL DEFAULT 'Y' COMMENT '是否已提交,N未提交 Y已提交',
                                                   PRIMARY KEY (`profit_chart_id`),
                                                   KEY `(contest_id,student_id)` (`contest_id`,`student_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;









drop table new_ct_balance;
drop table new_ct_balance_like;
drop table new_ct_balance_season_like;

-- 资产负债表
-- new_ct_balance
CREATE TABLE `new_ct_balance` (
                                  `balance_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '资产负债表id',
                                  `student_id` int(11) NOT NULL COMMENT '学生id',
                                  `contest_id` int(11) NOT NULL COMMENT '考试id',
                                  `bs_isxt` tinyint(4) NOT NULL COMMENT '填写人：1代表系统自动生成，0代表学生录入',
                                  `bs_year` tinyint(4) NOT NULL COMMENT '年份',
                                  `bs_cash` int(11) NOT NULL COMMENT '现金',
                                  `bs_receivable` int(11) NOT NULL COMMENT '应收款',
                                  `bs_product_in_process` int(11) DEFAULT NULL COMMENT '在制品',
                                  `bs_product` int(11) DEFAULT NULL COMMENT '产成品',
                                  `bs_material` int(11) DEFAULT NULL COMMENT '原材料',
                                  `bs_total_current_asset` int(11) NOT NULL COMMENT '流动资产合计',
                                  `bs_equipment` int(11) DEFAULT NULL COMMENT '机器和设备',
                                  `bs_project_on_construction` int(11) DEFAULT NULL COMMENT '在建工程',
                                  `bs_total_fixed_asset` int(11) NOT NULL COMMENT '固定资产合计',
                                  `bs_total_asset` int(11) NOT NULL COMMENT '资产合计',
                                  `bs_long_loan` int(11) NOT NULL COMMENT '长期贷款',
                                  `bs_short_loan` int(11) NOT NULL COMMENT '短期贷款',
    -- 其他应付款
                                  `bs_other_pay` int(11) NOT NULL COMMENT '其他应付款',
                                  `bs_tax` int(11) NOT NULL COMMENT '应交税费',
                                  `bs_total_liability` int(11) NOT NULL COMMENT '负债合计',
                                  `bs_equity` int(11) NOT NULL COMMENT '股东资本',
                                  `bs_retained_earning` int(11) NOT NULL COMMENT '利润留存',
                                  `bs_annual_net_profit` int(11) NOT NULL COMMENT '年度净利',
                                  `bs_total_equity` int(11) NOT NULL COMMENT '所有者权益',
                                  `bs_total` int(11) NOT NULL COMMENT '负债所有者权益合计',
                                  `is_submit` varchar(1) NOT NULL DEFAULT 'Y' COMMENT '是否已提交,N未提交 Y已提交',
                                  PRIMARY KEY (`balance_id`),
                                  KEY `(contest_id,student_id)` (`contest_id`,`student_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;




CREATE TABLE `new_ct_balance_like` (
                                       `balance_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '资产负债表id',
                                       `student_id` int(11) NOT NULL COMMENT '学生id',
                                       `contest_id` int(11) NOT NULL COMMENT '考试id',
                                       `bs_isxt` tinyint(4) NOT NULL COMMENT '填写人：1代表系统自动生成，0代表学生录入',
                                       `bs_year` tinyint(4) NOT NULL COMMENT '年份',
                                       `bs_cash` int(11) NOT NULL COMMENT '现金',
                                       `bs_receivable` int(11) NOT NULL COMMENT '应收款',
                                       `bs_product_in_process` int(11) DEFAULT NULL COMMENT '在制品',
                                       `bs_product` int(11) DEFAULT NULL COMMENT '产成品',
                                       `bs_material` int(11) DEFAULT NULL COMMENT '原材料',
                                       `bs_total_current_asset` int(11) NOT NULL COMMENT '流动资产合计',
                                       `bs_equipment` int(11) DEFAULT NULL COMMENT '机器和设备',
                                       `bs_project_on_construction` int(11) DEFAULT NULL COMMENT '在建工程',
                                       `bs_total_fixed_asset` int(11) NOT NULL COMMENT '固定资产合计',
                                       `bs_total_asset` int(11) NOT NULL COMMENT '资产合计',
                                       `bs_long_loan` int(11) NOT NULL COMMENT '长期贷款',
                                       `bs_short_loan` int(11) NOT NULL COMMENT '短期贷款',
    -- 其他应付款
                                       `bs_other_pay` int(11) NOT NULL COMMENT '其他应付款',
                                       `bs_tax` int(11) NOT NULL COMMENT '应交税费',
                                       `bs_total_liability` int(11) NOT NULL COMMENT '负债合计',
                                       `bs_equity` int(11) NOT NULL COMMENT '股东资本',
                                       `bs_retained_earning` int(11) NOT NULL COMMENT '利润留存',
                                       `bs_annual_net_profit` int(11) NOT NULL COMMENT '年度净利',
                                       `bs_total_equity` int(11) NOT NULL COMMENT '所有者权益',
                                       `bs_total` int(11) NOT NULL COMMENT '负债所有者权益合计',
                                       `is_submit` varchar(1) NOT NULL DEFAULT 'Y' COMMENT '是否已提交,N未提交 Y已提交',
                                       PRIMARY KEY (`balance_id`),
                                       KEY `(contest_id,student_id)` (`contest_id`,`student_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;


CREATE TABLE `new_ct_balance_season_like` (
                                              `balance_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '资产负债表id',
                                              `student_id` int(11) NOT NULL COMMENT '学生id',
                                              `contest_id` int(11) NOT NULL COMMENT '考试id',
                                              `bs_isxt` tinyint(4) NOT NULL COMMENT '填写人：1代表系统自动生成，0代表学生录入',
                                              `bs_year` tinyint(4) NOT NULL COMMENT '年份',
                                              `bs_cash` int(11) NOT NULL COMMENT '现金',
                                              `bs_receivable` int(11) NOT NULL COMMENT '应收款',
                                              `bs_product_in_process` int(11) DEFAULT NULL COMMENT '在制品',
                                              `bs_product` int(11) DEFAULT NULL COMMENT '产成品',
                                              `bs_material` int(11) DEFAULT NULL COMMENT '原材料',
                                              `bs_total_current_asset` int(11) NOT NULL COMMENT '流动资产合计',
                                              `bs_equipment` int(11) DEFAULT NULL COMMENT '机器和设备',
                                              `bs_project_on_construction` int(11) DEFAULT NULL COMMENT '在建工程',
                                              `bs_total_fixed_asset` int(11) NOT NULL COMMENT '固定资产合计',
                                              `bs_total_asset` int(11) NOT NULL COMMENT '资产合计',
                                              `bs_long_loan` int(11) NOT NULL COMMENT '长期贷款',
                                              `bs_short_loan` int(11) NOT NULL COMMENT '短期贷款',
    -- 其他应付款
                                              `bs_other_pay` int(11) NOT NULL COMMENT '其他应付款',
                                              `bs_tax` int(11) NOT NULL COMMENT '应交税费',
                                              `bs_total_liability` int(11) NOT NULL COMMENT '负债合计',
                                              `bs_equity` int(11) NOT NULL COMMENT '股东资本',
                                              `bs_retained_earning` int(11) NOT NULL COMMENT '利润留存',
                                              `bs_annual_net_profit` int(11) NOT NULL COMMENT '年度净利',
                                              `bs_total_equity` int(11) NOT NULL COMMENT '所有者权益',
                                              `bs_total` int(11) NOT NULL COMMENT '负债所有者权益合计',
                                              `is_submit` varchar(1) NOT NULL DEFAULT 'Y' COMMENT '是否已提交,N未提交 Y已提交',
                                              PRIMARY KEY (`balance_id`),
                                              KEY `(contest_id,student_id)` (`contest_id`,`student_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;






-- 财务指标


CREATE TABLE `new_ct_financial_target` (
                                           `ft_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '财务指标表id',
                                           `student_id` int(11) NOT NULL COMMENT '学生id',
                                           `contest_id` int(11) NOT NULL COMMENT '考试id',
                                           `ft_isxt` tinyint(4) NOT NULL COMMENT '填写人：1代表系统自动生成，0代表学生录入',
                                           `ft_year` tinyint(4) NOT NULL COMMENT '年份',
                                           `ft_current_rate` decimal(8,2) NOT NULL COMMENT '流动比率',
                                           `ft_quick_rate` decimal(8,2) NOT NULL COMMENT '速动比率',
                                           `ft_debt_rate` decimal(8,2) NOT NULL COMMENT '资产负债率',
                                           `ft_equity_rate` decimal(8,2) NOT NULL COMMENT '产权比率',
                                           `ft_net_profit_rate` decimal(8,2) NOT NULL COMMENT '营业净利润率',
                                           `ft_cost_expense_rate` decimal(8,2) NOT NULL COMMENT '成本费用率',
                                           `ft_return_assets_rate` decimal(8,2) NOT NULL COMMENT '资产报酬率',
                                           `ft_return_equity_rate` decimal(8,2) NOT NULL COMMENT '净资产收益率',
                                           `ft_revenue_growth_rate` decimal(8,2) NOT NULL COMMENT '营业收入增长率',
                                           `ft_appreciation_rate` decimal(8,2) NOT NULL COMMENT '资本保值增值率',
                                           `ft_total_growth_rate` decimal(8,2) NOT NULL COMMENT '总资产增长率',
                                           `ft_inventory_rate` decimal(8,2) NOT NULL COMMENT '总资产增长率',
                                           `ft_inventory_days` decimal(8,2) NOT NULL COMMENT '库存周转天数',
                                           `ft_receivable_rate` decimal(8,2) NOT NULL COMMENT '应收账款周转率',
                                           `ft_receivable_days` decimal(8,2) NOT NULL COMMENT '应收账款周转天数',
                                           `ft_cash_period` decimal(8,2) NOT NULL COMMENT '现金周转期',
                                           `is_submit` varchar(1) NOT NULL DEFAULT 'Y' COMMENT '是否已提交,N未提交 Y已提交',
                                           PRIMARY KEY (`ft_id`),
                                           KEY `(contest_id,student_id)` (`contest_id`,`student_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;


CREATE TABLE `new_ct_financial_target_like` (
                                                `ft_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '财务指标表id',
                                                `student_id` int(11) NOT NULL COMMENT '学生id',
                                                `contest_id` int(11) NOT NULL COMMENT '考试id',
                                                `ft_isxt` tinyint(4) NOT NULL COMMENT '填写人：1代表系统自动生成，0代表学生录入',
                                                `ft_year` tinyint(4) NOT NULL COMMENT '年份',
                                                `ft_current_rate` decimal(8,2) NOT NULL COMMENT '流动比率',
                                                `ft_quick_rate` decimal(8,2) NOT NULL COMMENT '速动比率',
                                                `ft_debt_rate` decimal(8,2) NOT NULL COMMENT '资产负债率',
                                                `ft_equity_rate` decimal(8,2) NOT NULL COMMENT '产权比率',
                                                `ft_net_profit_rate` decimal(8,2) NOT NULL COMMENT '营业净利润率',
                                                `ft_cost_expense_rate` decimal(8,2) NOT NULL COMMENT '成本费用率',
                                                `ft_return_assets_rate` decimal(8,2) NOT NULL COMMENT '资产报酬率',
                                                `ft_return_equity_rate` decimal(8,2) NOT NULL COMMENT '净资产收益率',
                                                `ft_revenue_growth_rate` decimal(8,2) NOT NULL COMMENT '营业收入增长率',
                                                `ft_appreciation_rate` decimal(8,2) NOT NULL COMMENT '资本保值增值率',
                                                `ft_total_growth_rate` decimal(8,2) NOT NULL COMMENT '总资产增长率',
                                                `ft_inventory_rate` decimal(8,2) NOT NULL COMMENT '总资产增长率',
                                                `ft_inventory_days` decimal(8,2) NOT NULL COMMENT '库存周转天数',
                                                `ft_receivable_rate` decimal(8,2) NOT NULL COMMENT '应收账款周转率',
                                                `ft_receivable_days` decimal(8,2) NOT NULL COMMENT '应收账款周转天数',
                                                `ft_cash_period` decimal(8,2) NOT NULL COMMENT '现金周转期',
                                                `is_submit` varchar(1) NOT NULL DEFAULT 'Y' COMMENT '是否已提交,N未提交 Y已提交',
                                                PRIMARY KEY (`ft_id`),
                                                KEY `(contest_id,student_id)` (`contest_id`,`student_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;


CREATE TABLE `new_ct_financial_target_season_like` (
                                                       `ft_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '财务指标表id',
                                                       `student_id` int(11) NOT NULL COMMENT '学生id',
                                                       `contest_id` int(11) NOT NULL COMMENT '考试id',
                                                       `ft_isxt` tinyint(4) NOT NULL COMMENT '填写人：1代表系统自动生成，0代表学生录入',
                                                       `ft_year` tinyint(4) NOT NULL COMMENT '年份',
                                                       `ft_current_rate` decimal(8,2) NOT NULL COMMENT '流动比率',
                                                       `ft_quick_rate` decimal(8,2) NOT NULL COMMENT '速动比率',
                                                       `ft_debt_rate` decimal(8,2) NOT NULL COMMENT '资产负债率',
                                                       `ft_equity_rate` decimal(8,2) NOT NULL COMMENT '产权比率',
                                                       `ft_net_profit_rate` decimal(8,2) NOT NULL COMMENT '营业净利润率',
                                                       `ft_cost_expense_rate` decimal(8,2) NOT NULL COMMENT '成本费用率',
                                                       `ft_return_assets_rate` decimal(8,2) NOT NULL COMMENT '资产报酬率',
                                                       `ft_return_equity_rate` decimal(8,2) NOT NULL COMMENT '净资产收益率',
                                                       `ft_revenue_growth_rate` decimal(8,2) NOT NULL COMMENT '营业收入增长率',
                                                       `ft_appreciation_rate` decimal(8,2) NOT NULL COMMENT '资本保值增值率',
                                                       `ft_total_growth_rate` decimal(8,2) NOT NULL COMMENT '总资产增长率',
                                                       `ft_inventory_rate` decimal(8,2) NOT NULL COMMENT '总资产增长率',
                                                       `ft_inventory_days` decimal(8,2) NOT NULL COMMENT '库存周转天数',
                                                       `ft_receivable_rate` decimal(8,2) NOT NULL COMMENT '应收账款周转率',
                                                       `ft_receivable_days` decimal(8,2) NOT NULL COMMENT '应收账款周转天数',
                                                       `ft_cash_period` decimal(8,2) NOT NULL COMMENT '现金周转期',
                                                       `is_submit` varchar(1) NOT NULL DEFAULT 'Y' COMMENT '是否已提交,N未提交 Y已提交',
                                                       PRIMARY KEY (`ft_id`),
                                                       KEY `(contest_id,student_id)` (`contest_id`,`student_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;





