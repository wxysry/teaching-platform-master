ALTER TABLE new_ct_xz_order ADD COLUMN quarterly INT(11) DEFAULT NULL COMMENT '季度' after date;
CREATE TABLE `new_ct_xz_order_like` (
                                        `order_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
                                        `co_id` varchar(50) NOT NULL COMMENT '订单编号',
                                        `student_id` int(11) NOT NULL,
                                        `contest_id` int(11) NOT NULL,
                                        `date` tinyint(4) DEFAULT NULL COMMENT '年份',
                                        `quarterly` int(11) DEFAULT NULL COMMENT '季度',
                                        `cm_id` int(11) DEFAULT NULL COMMENT '市场',
                                        `cp_id` int(11) DEFAULT NULL COMMENT '产品',
                                        `num` int(11) DEFAULT NULL COMMENT '数量',
                                        `total_price` int(11) DEFAULT NULL COMMENT '总价',
                                        `delivery_date` tinyint(4) DEFAULT NULL COMMENT '交货期',
                                        `payment_date` tinyint(4) DEFAULT NULL COMMENT '账期',
                                        `iso_id` int(11) DEFAULT NULL COMMENT 'ISO',
                                        `commit_date` tinyint(4) DEFAULT NULL COMMENT '交单时间',
                                        PRIMARY KEY (`order_id`),
                                        KEY `(contest_id,student_id)` (`contest_id`,`student_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;


CREATE TABLE `new_ct_xz_order_season_like` (
                                               `order_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
                                               `co_id` varchar(50) NOT NULL COMMENT '订单编号',
                                               `student_id` int(11) NOT NULL,
                                               `contest_id` int(11) NOT NULL,
                                               `date` tinyint(4) DEFAULT NULL COMMENT '年份',
                                               `quarterly` int(11) DEFAULT NULL COMMENT '季度',
                                               `cm_id` int(11) DEFAULT NULL COMMENT '市场',
                                               `cp_id` int(11) DEFAULT NULL COMMENT '产品',
                                               `num` int(11) DEFAULT NULL COMMENT '数量',
                                               `total_price` int(11) DEFAULT NULL COMMENT '总价',
                                               `delivery_date` tinyint(4) DEFAULT NULL COMMENT '交货期',
                                               `payment_date` tinyint(4) DEFAULT NULL COMMENT '账期',
                                               `iso_id` int(11) DEFAULT NULL COMMENT 'ISO',
                                               `commit_date` tinyint(4) DEFAULT NULL COMMENT '交单时间',
                                               PRIMARY KEY (`order_id`),
                                               KEY `(contest_id,student_id)` (`contest_id`,`student_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;