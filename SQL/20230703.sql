drop table IF EXISTS  new_ct_bank_loan_like;
drop table IF EXISTS  new_ct_bank_loan_season_like;

CREATE TABLE `new_ct_bank_loan_like` (
                                         `bl_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '贷款记录表',
                                         `student_id` int(11) NOT NULL,
                                         `contest_id` int(11) NOT NULL,
                                         `bl_type` tinyint(4) NOT NULL COMMENT '贷款类型:1表示长贷，2表示短贷',
                                         `bl_fee` int(11) NOT NULL COMMENT '贷款金额',
                                         `bl_add_time` tinyint(4) NOT NULL COMMENT '贷款时间',
                                         `bl_remain_time` tinyint(4) NOT NULL COMMENT '贷款时长',
                                         `bl_repayment_date` tinyint(4) NOT NULL COMMENT '还款时间',
                                         `interest` decimal(20,2) DEFAULT NULL COMMENT '利息',
                                         `rate` decimal(20,2) DEFAULT NULL COMMENT '利率',
                                         `bl_name` varchar(50) DEFAULT NULL COMMENT '贷款名称',
                                         `repayment_type` varchar(4) DEFAULT NULL COMMENT '还款方式',
                                         PRIMARY KEY (`bl_id`),
                                         KEY `(contest_id,student_id)` (`contest_id`,`student_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

CREATE TABLE `new_ct_bank_loan_season_like` (
                                                `bl_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '贷款记录表',
                                                `student_id` int(11) NOT NULL,
                                                `contest_id` int(11) NOT NULL,
                                                `bl_type` tinyint(4) NOT NULL COMMENT '贷款类型:1表示长贷，2表示短贷',
                                                `bl_fee` int(11) NOT NULL COMMENT '贷款金额',
                                                `bl_add_time` tinyint(4) NOT NULL COMMENT '贷款时间',
                                                `bl_remain_time` tinyint(4) NOT NULL COMMENT '贷款时长',
                                                `bl_repayment_date` tinyint(4) NOT NULL COMMENT '还款时间',
                                                `interest` decimal(20,2) DEFAULT NULL COMMENT '利息',
                                                `rate` decimal(20,2) DEFAULT NULL COMMENT '利率',
                                                `bl_name` varchar(50) DEFAULT NULL COMMENT '贷款名称',
                                                `repayment_type` varchar(4) DEFAULT NULL COMMENT '还款方式',
                                                PRIMARY KEY (`bl_id`),
                                                KEY `(contest_id,student_id)` (`contest_id`,`student_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;


CREATE TABLE `new_ct_corporate` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '社会责任分',
  `score` int(11) NOT NULL COMMENT '分数',
  `student_id` int(11) NOT NULL COMMENT '用户id',
  `contest_id` int(11) NOT NULL COMMENT '竞赛id',

  PRIMARY KEY (`id`),
  KEY `(contest_id,student_id)` (`contest_id`,`student_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;



CREATE TABLE `new_ct_corporate_like` (
                                    `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '社会责任分',
                                    `score` int(11) NOT NULL COMMENT '分数',
                                    `student_id` int(11) NOT NULL COMMENT '用户id',
                                    `contest_id` int(11) NOT NULL COMMENT '竞赛id',
                                    PRIMARY KEY (`id`),
                                    KEY `(contest_id,student_id)` (`contest_id`,`student_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;


CREATE TABLE `new_ct_corporate_season_like` (
                                         `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '社会责任分',
                                         `score` int(11) NOT NULL COMMENT '分数',
                                         `student_id` int(11) NOT NULL COMMENT '用户id',
                                         `contest_id` int(11) NOT NULL COMMENT '竞赛id',
                                         PRIMARY KEY (`id`),
                                         KEY `(contest_id,student_id)` (`contest_id`,`student_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;