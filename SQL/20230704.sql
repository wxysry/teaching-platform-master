CREATE TABLE `new_ct_dd_material_like` (
                                           `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '原料采购记录',
                                           `om_cmid` int(10) NOT NULL COMMENT '原料编号',
                                           `cm_name` varchar(50) NOT NULL COMMENT '原料名称',
                                           `student_id` int(11) DEFAULT NULL,
                                           `contest_id` int(11) DEFAULT NULL,
                                           `remain_date` tinyint(4) DEFAULT NULL COMMENT '收货期',
                                           `purchase_date` tinyint(4) DEFAULT NULL COMMENT '采购时间',
                                           `is_inventory` tinyint(4) DEFAULT NULL COMMENT '是否入库',
                                           `pay_date` tinyint(4) DEFAULT NULL COMMENT '付款周期（账期）',
                                           `is_pay` tinyint(4) DEFAULT NULL COMMENT '是否付款',
                                           `price` int(11) DEFAULT NULL COMMENT '价格',
                                           `num` int(11) DEFAULT NULL COMMENT '数量',
                                           PRIMARY KEY (`id`),
                                           KEY `(contest_id,student_id)` (`contest_id`,`student_id`)
) ENGINE=InnoDB AUTO_INCREMENT=65 DEFAULT CHARSET=utf8;


CREATE TABLE `new_ct_dd_material_season_like` (
                                                  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '原料采购记录',
                                                  `om_cmid` int(10) NOT NULL COMMENT '原料编号',
                                                  `cm_name` varchar(50) NOT NULL COMMENT '原料名称',
                                                  `student_id` int(11) DEFAULT NULL,
                                                  `contest_id` int(11) DEFAULT NULL,
                                                  `remain_date` tinyint(4) DEFAULT NULL COMMENT '收货期',
                                                  `purchase_date` tinyint(4) DEFAULT NULL COMMENT '采购时间',
                                                  `is_inventory` tinyint(4) DEFAULT NULL COMMENT '是否入库',
                                                  `pay_date` tinyint(4) DEFAULT NULL COMMENT '付款周期（账期）',
                                                  `is_pay` tinyint(4) DEFAULT NULL COMMENT '是否付款',
                                                  `price` int(11) DEFAULT NULL COMMENT '价格',
                                                  `num` int(11) DEFAULT NULL COMMENT '数量',
                                                  PRIMARY KEY (`id`),
                                                  KEY `(contest_id,student_id)` (`contest_id`,`student_id`)
) ENGINE=InnoDB AUTO_INCREMENT=65 DEFAULT CHARSET=utf8;



CREATE TABLE `new_ct_kc_material_like` (
                                           `material_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
                                           `student_id` int(11) DEFAULT NULL,
                                           `contest_id` int(11) DEFAULT NULL,
                                           `im_cm_id` int(11) DEFAULT NULL COMMENT '原料id',
                                           `im_num` int(11) DEFAULT NULL COMMENT '剩余数量',
                                           `material_name` varchar(11) DEFAULT NULL COMMENT '原料名称',
                                           `in_inventory_date` tinyint(4) DEFAULT NULL COMMENT '入库日期',
                                           `material_price` int(11) DEFAULT NULL COMMENT '成本',
                                           PRIMARY KEY (`material_id`),
                                           KEY `(contest_id,student_id)` (`contest_id`,`student_id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8;



CREATE TABLE `new_ct_kc_material_season_like` (
                                                  `material_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
                                                  `student_id` int(11) DEFAULT NULL,
                                                  `contest_id` int(11) DEFAULT NULL,
                                                  `im_cm_id` int(11) DEFAULT NULL COMMENT '原料id',
                                                  `im_num` int(11) DEFAULT NULL COMMENT '剩余数量',
                                                  `material_name` varchar(11) DEFAULT NULL COMMENT '原料名称',
                                                  `in_inventory_date` tinyint(4) DEFAULT NULL COMMENT '入库日期',
                                                  `material_price` int(11) DEFAULT NULL COMMENT '成本',
                                                  PRIMARY KEY (`material_id`),
                                                  KEY `(contest_id,student_id)` (`contest_id`,`student_id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8;



CREATE TABLE `new_ct_kc_product_like` (
                                          `kc_product_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '产成品库存',
                                          `student_id` int(11) DEFAULT NULL,
                                          `contest_id` int(11) DEFAULT NULL,
                                          `ip_cp_id` int(11) DEFAULT NULL COMMENT '产品id',
                                          `ip_num` int(11) DEFAULT NULL COMMENT '库存数量',
                                          `line_id` int(10) DEFAULT NULL COMMENT '生产线id',
                                          `is_inventory` int(10) DEFAULT NULL COMMENT '是否入库',
                                          `inventory_date` int(10) DEFAULT NULL COMMENT '入库日期',
                                          `real_cost` varchar(10) DEFAULT NULL COMMENT '真实成本',
                                          PRIMARY KEY (`kc_product_id`),
                                          KEY `(contest_id,student_id)` (`contest_id`,`student_id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8;



CREATE TABLE `new_ct_kc_product_season_like` (
                                                 `kc_product_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '产成品库存',
                                                 `student_id` int(11) DEFAULT NULL,
                                                 `contest_id` int(11) DEFAULT NULL,
                                                 `ip_cp_id` int(11) DEFAULT NULL COMMENT '产品id',
                                                 `ip_num` int(11) DEFAULT NULL COMMENT '库存数量',
                                                 `line_id` int(10) DEFAULT NULL COMMENT '生产线id',
                                                 `is_inventory` int(10) DEFAULT NULL COMMENT '是否入库',
                                                 `inventory_date` int(10) DEFAULT NULL COMMENT '入库日期',
                                                 `real_cost` varchar(10) DEFAULT NULL COMMENT '真实成本',
                                                 PRIMARY KEY (`kc_product_id`),
                                                 KEY `(contest_id,student_id)` (`contest_id`,`student_id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8;



CREATE TABLE `new_ct_line_like` (
                                    `line_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
                                    `student_id` int(11) NOT NULL,
                                    `contest_id` int(11) NOT NULL,
                                    `pl_cplid` int(11) NOT NULL COMMENT '生产线类型id',
                                    `pl_cpid` int(11) NOT NULL COMMENT '生产产品id',
                                    `pl_invest` int(11) NOT NULL COMMENT '原值',
                                    `pl_dep_total` int(11) NOT NULL COMMENT '累计折旧',
                                    `pl_product_add_date` tinyint(4) DEFAULT '0' COMMENT '开产时间',
                                    `pl_producting_date` tinyint(4) DEFAULT '0' COMMENT '剩余生产时间',
                                    `pl_finish_date` tinyint(4) DEFAULT '0' COMMENT '建成时间',
                                    `pl_remain_date` tinyint(4) DEFAULT '0' COMMENT '在建剩余时间',
                                    `pl_add_time` tinyint(4) DEFAULT '0' COMMENT '开建时间',
                                    `pl_transfer_add_date` tinyint(4) DEFAULT '0' COMMENT '转产时间',
                                    `pl_transfer_date` tinyint(4) DEFAULT '0' COMMENT '转产剩余时间',
                                    `status` varchar(20) DEFAULT NULL,
                                    `real_production` int(11) DEFAULT NULL COMMENT '产量',
                                    `classes_id` int(11) DEFAULT NULL COMMENT '班次id',
                                    PRIMARY KEY (`line_id`),
                                    KEY `(contest_id,student_id)` (`contest_id`,`student_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;



CREATE TABLE `new_ct_line_season_like` (
                                           `line_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
                                           `student_id` int(11) NOT NULL,
                                           `contest_id` int(11) NOT NULL,
                                           `pl_cplid` int(11) NOT NULL COMMENT '生产线类型id',
                                           `pl_cpid` int(11) NOT NULL COMMENT '生产产品id',
                                           `pl_invest` int(11) NOT NULL COMMENT '原值',
                                           `pl_dep_total` int(11) NOT NULL COMMENT '累计折旧',
                                           `pl_product_add_date` tinyint(4) DEFAULT '0' COMMENT '开产时间',
                                           `pl_producting_date` tinyint(4) DEFAULT '0' COMMENT '剩余生产时间',
                                           `pl_finish_date` tinyint(4) DEFAULT '0' COMMENT '建成时间',
                                           `pl_remain_date` tinyint(4) DEFAULT '0' COMMENT '在建剩余时间',
                                           `pl_add_time` tinyint(4) DEFAULT '0' COMMENT '开建时间',
                                           `pl_transfer_add_date` tinyint(4) DEFAULT '0' COMMENT '转产时间',
                                           `pl_transfer_date` tinyint(4) DEFAULT '0' COMMENT '转产剩余时间',
                                           `status` varchar(20) DEFAULT NULL,
                                           `real_production` int(11) DEFAULT NULL COMMENT '产量',
                                           `classes_id` int(11) DEFAULT NULL COMMENT '班次id',
                                           PRIMARY KEY (`line_id`),
                                           KEY `(contest_id,student_id)` (`contest_id`,`student_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;



CREATE TABLE `new_ct_yf_market_like` (
                                         `market_id` int(11) NOT NULL AUTO_INCREMENT,
                                         `student_id` int(11) DEFAULT NULL,
                                         `contest_id` int(11) DEFAULT NULL,
                                         `dm_cm_id` int(11) DEFAULT NULL COMMENT '市场规则id',
                                         `dm_total_date` tinyint(4) DEFAULT NULL COMMENT '总研发时间',
                                         `dm_now_date` tinyint(4) DEFAULT NULL COMMENT '已研发时间',
                                         `dm_remain_date` tinyint(4) DEFAULT NULL COMMENT '剩余研发时间',
                                         `dm_finish_date` tinyint(4) DEFAULT NULL COMMENT '研发完成时间',
                                         `dm_start_date` int(10) DEFAULT NULL COMMENT '申请时间',
                                         `dm_state` varchar(10) DEFAULT NULL COMMENT '状态',
                                         PRIMARY KEY (`market_id`),
                                         KEY `(contest_id,student_id)` (`contest_id`,`student_id`)
) ENGINE=InnoDB AUTO_INCREMENT=36 DEFAULT CHARSET=utf8;


CREATE TABLE `new_ct_yf_market_season_like` (
                                                `market_id` int(11) NOT NULL AUTO_INCREMENT,
                                                `student_id` int(11) DEFAULT NULL,
                                                `contest_id` int(11) DEFAULT NULL,
                                                `dm_cm_id` int(11) DEFAULT NULL COMMENT '市场规则id',
                                                `dm_total_date` tinyint(4) DEFAULT NULL COMMENT '总研发时间',
                                                `dm_now_date` tinyint(4) DEFAULT NULL COMMENT '已研发时间',
                                                `dm_remain_date` tinyint(4) DEFAULT NULL COMMENT '剩余研发时间',
                                                `dm_finish_date` tinyint(4) DEFAULT NULL COMMENT '研发完成时间',
                                                `dm_start_date` int(10) DEFAULT NULL COMMENT '申请时间',
                                                `dm_state` varchar(10) DEFAULT NULL COMMENT '状态',
                                                PRIMARY KEY (`market_id`),
                                                KEY `(contest_id,student_id)` (`contest_id`,`student_id`)
) ENGINE=InnoDB AUTO_INCREMENT=36 DEFAULT CHARSET=utf8;


CREATE TABLE `new_ct_yf_num_like` (
                                      `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
                                      `student_id` int(11) DEFAULT NULL,
                                      `contest_id` int(11) DEFAULT NULL,
                                      `num_id` int(11) DEFAULT NULL,
                                      `start_date` int(11) DEFAULT NULL COMMENT '开始时间',
                                      `finish_date` tinyint(4) DEFAULT NULL COMMENT '结束时间',
                                      `consume_money` tinyint(4) DEFAULT NULL COMMENT '消耗金钱(元)',
                                      `state` varchar(10) DEFAULT NULL COMMENT '状态',
                                      PRIMARY KEY (`id`),
                                      KEY `(contest_id,student_id)` (`contest_id`,`student_id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8;


CREATE TABLE `new_ct_yf_num_season_like` (
                                             `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
                                             `student_id` int(11) DEFAULT NULL,
                                             `contest_id` int(11) DEFAULT NULL,
                                             `num_id` int(11) DEFAULT NULL,
                                             `start_date` int(11) DEFAULT NULL COMMENT '开始时间',
                                             `finish_date` tinyint(4) DEFAULT NULL COMMENT '结束时间',
                                             `consume_money` tinyint(4) DEFAULT NULL COMMENT '消耗金钱(元)',
                                             `state` varchar(10) DEFAULT NULL COMMENT '状态',
                                             PRIMARY KEY (`id`),
                                             KEY `(contest_id,student_id)` (`contest_id`,`student_id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8;


CREATE TABLE `new_ct_yf_product_like` (
                                          `product_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
                                          `student_id` int(11) DEFAULT NULL,
                                          `contest_id` int(11) DEFAULT NULL,
                                          `dp_cp_id` int(11) DEFAULT NULL COMMENT '产品规则id',
                                          `dp_total_date` tinyint(4) DEFAULT NULL COMMENT '总研发时间',
                                          `dp_now_date` tinyint(4) DEFAULT NULL COMMENT '已研发时间',
                                          `dp_remain_date` tinyint(4) DEFAULT NULL COMMENT '剩余研发时间',
                                          `dp_finish_date` tinyint(4) DEFAULT NULL COMMENT '研发完成时间',
                                          `dp_start_date` int(10) DEFAULT NULL COMMENT '申请时间',
                                          `dp_state` varchar(10) DEFAULT NULL COMMENT '状态',
                                          PRIMARY KEY (`product_id`),
                                          KEY `(contest_id,student_id)` (`contest_id`,`student_id`)
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=utf8;



CREATE TABLE `new_ct_yf_product_season_like` (
                                                 `product_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
                                                 `student_id` int(11) DEFAULT NULL,
                                                 `contest_id` int(11) DEFAULT NULL,
                                                 `dp_cp_id` int(11) DEFAULT NULL COMMENT '产品规则id',
                                                 `dp_total_date` tinyint(4) DEFAULT NULL COMMENT '总研发时间',
                                                 `dp_now_date` tinyint(4) DEFAULT NULL COMMENT '已研发时间',
                                                 `dp_remain_date` tinyint(4) DEFAULT NULL COMMENT '剩余研发时间',
                                                 `dp_finish_date` tinyint(4) DEFAULT NULL COMMENT '研发完成时间',
                                                 `dp_start_date` int(10) DEFAULT NULL COMMENT '申请时间',
                                                 `dp_state` varchar(10) DEFAULT NULL COMMENT '状态',
                                                 PRIMARY KEY (`product_id`),
                                                 KEY `(contest_id,student_id)` (`contest_id`,`student_id`)
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=utf8;

ALTER TABLE new_ct_xz_order ADD COLUMN total_cost INT(11) DEFAULT NULL COMMENT '成本' ;

ALTER TABLE new_ct_xz_order_season_like ADD COLUMN total_cost INT(11) DEFAULT NULL COMMENT '成本' ;

ALTER TABLE new_ct_xz_order_like ADD COLUMN total_cost INT(11) DEFAULT NULL COMMENT '成本' ;


CREATE TABLE `new_ct_worker_season_like` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '工人ID',
  `student_id` int(11) NOT NULL COMMENT '学生id',
  `contest_id` int(11) NOT NULL COMMENT '竞赛ID',
  `worker_name` varchar(50) NOT NULL COMMENT '姓名',
  `worker_num` int(11) NOT NULL COMMENT '序号',
  `recruit_name` varchar(20) NOT NULL COMMENT '等级',
  `recruit_num` varchar(20) NOT NULL COMMENT '等级编码',
  `mult_bonus` int(11) NOT NULL COMMENT '倍数加成(%)',
  `init_sal` int(11) NOT NULL COMMENT '工资(元)',
  `start_work_time` int(11) NOT NULL COMMENT '招聘时间',
  `is_work` int(11) NOT NULL COMMENT '是否在职',
  `piece` int(11) DEFAULT NULL COMMENT '计件',
  `line_id` int(11) DEFAULT NULL COMMENT '产线id',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=34 DEFAULT CHARSET=utf8;

CREATE TABLE `new_ct_worker_like` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '工人ID',
  `student_id` int(11) NOT NULL COMMENT '学生id',
  `contest_id` int(11) NOT NULL COMMENT '竞赛ID',
  `worker_name` varchar(50) NOT NULL COMMENT '姓名',
  `worker_num` int(11) NOT NULL COMMENT '序号',
  `recruit_name` varchar(20) NOT NULL COMMENT '等级',
  `recruit_num` varchar(20) NOT NULL COMMENT '等级编码',
  `mult_bonus` int(11) NOT NULL COMMENT '倍数加成(%)',
  `init_sal` int(11) NOT NULL COMMENT '工资(元)',
  `start_work_time` int(11) NOT NULL COMMENT '招聘时间',
  `is_work` int(11) NOT NULL COMMENT '是否在职',
  `piece` int(11) DEFAULT NULL COMMENT '计件',
  `line_id` int(11) DEFAULT NULL COMMENT '产线id',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=34 DEFAULT CHARSET=utf8;

-- ddddddddddddddddddd
drop table new_ct_dd_material_like;
drop table new_ct_dd_material_season_like;
CREATE TABLE `new_ct_dd_material_season_like` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '原料采购记录',
  `om_cmid` int(10) DEFAULT NULL COMMENT '原料编号',
  `cm_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '原料名称',
  `student_id` int(11) DEFAULT NULL,
  `contest_id` int(11) DEFAULT NULL,
  `remain_date` tinyint(4) DEFAULT NULL COMMENT '收货期',
  `purchase_date` tinyint(4) DEFAULT NULL COMMENT '采购时间',
  `is_inventory` tinyint(4) DEFAULT NULL COMMENT '是否入库',
  `pay_date` tinyint(4) DEFAULT NULL COMMENT '付款周期（账期）',
  `is_pay` tinyint(4) DEFAULT NULL COMMENT '是否付款',
  `price` int(11) DEFAULT NULL COMMENT '价格',
  `num` int(11) DEFAULT NULL COMMENT '数量',
  PRIMARY KEY (`id`),
  KEY `(contest_id,student_id)` (`contest_id`,`student_id`)
) ENGINE=InnoDB AUTO_INCREMENT=107 DEFAULT CHARSET=utf8;

CREATE TABLE `new_ct_dd_material_like` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '原料采购记录',
  `om_cmid` int(10) DEFAULT NULL COMMENT '原料编号',
  `cm_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '原料名称',
  `student_id` int(11) DEFAULT NULL,
  `contest_id` int(11) DEFAULT NULL,
  `remain_date` tinyint(4) DEFAULT NULL COMMENT '收货期',
  `purchase_date` tinyint(4) DEFAULT NULL COMMENT '采购时间',
  `is_inventory` tinyint(4) DEFAULT NULL COMMENT '是否入库',
  `pay_date` tinyint(4) DEFAULT NULL COMMENT '付款周期（账期）',
  `is_pay` tinyint(4) DEFAULT NULL COMMENT '是否付款',
  `price` int(11) DEFAULT NULL COMMENT '价格',
  `num` int(11) DEFAULT NULL COMMENT '数量',
  PRIMARY KEY (`id`),
  KEY `(contest_id,student_id)` (`contest_id`,`student_id`)
) ENGINE=InnoDB AUTO_INCREMENT=107 DEFAULT CHARSET=utf8;


drop table new_ct_fee_like;
drop table new_ct_fee_season_like;
CREATE TABLE `new_ct_fee_season_like` (
  `fee_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '应收款',
  `student_id` int(11) NOT NULL,
  `contest_id` int(11) NOT NULL,
  `r_fee` int(11) NOT NULL COMMENT '金额',
  `r_remain_date` tinyint(4) NOT NULL COMMENT '账期',
  `borrower` varchar(10) DEFAULT NULL COMMENT '借方',
  `lender` varchar(10) DEFAULT NULL COMMENT '贷方',
  `remarks` varchar(10) DEFAULT NULL COMMENT '备注',
  `payment_date` varchar(10) DEFAULT NULL COMMENT '收款日期',
  `type` varchar(10) DEFAULT NULL COMMENT '类型',
  PRIMARY KEY (`fee_id`),
  KEY `(contest_id,student_id)` (`contest_id`,`student_id`)
) ENGINE=InnoDB AUTO_INCREMENT=114 DEFAULT CHARSET=utf8;

CREATE TABLE `new_ct_fee_like` (
  `fee_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '应收款',
  `student_id` int(11) NOT NULL,
  `contest_id` int(11) NOT NULL,
  `r_fee` int(11) NOT NULL COMMENT '金额',
  `r_remain_date` tinyint(4) NOT NULL COMMENT '账期',
  `borrower` varchar(10) DEFAULT NULL COMMENT '借方',
  `lender` varchar(10) DEFAULT NULL COMMENT '贷方',
  `remarks` varchar(10) DEFAULT NULL COMMENT '备注',
  `payment_date` varchar(10) DEFAULT NULL COMMENT '收款日期',
  `type` varchar(10) DEFAULT NULL COMMENT '类型',
  PRIMARY KEY (`fee_id`),
  KEY `(contest_id,student_id)` (`contest_id`,`student_id`)
) ENGINE=InnoDB AUTO_INCREMENT=114 DEFAULT CHARSET=utf8;


drop table new_ct_kc_material_like;
drop table new_ct_kc_material_season_like;
CREATE TABLE `new_ct_kc_material_like` (
  `material_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `student_id` int(11) DEFAULT NULL,
  `contest_id` int(11) DEFAULT NULL,
  `im_cm_id` int(11) DEFAULT NULL COMMENT '原料id',
  `im_num` int(11) DEFAULT NULL COMMENT '剩余数量',
  `material_name` varchar(11) DEFAULT NULL COMMENT '原料名称',
  `in_inventory_date` tinyint(4) DEFAULT NULL COMMENT '入库日期',
  `material_price` int(11) DEFAULT NULL COMMENT '成本',
  PRIMARY KEY (`material_id`),
  KEY `(contest_id,student_id)` (`contest_id`,`student_id`)
) ENGINE=InnoDB AUTO_INCREMENT=82 DEFAULT CHARSET=utf8;

CREATE TABLE `new_ct_kc_material_season_like` (
  `material_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `student_id` int(11) DEFAULT NULL,
  `contest_id` int(11) DEFAULT NULL,
  `im_cm_id` int(11) DEFAULT NULL COMMENT '原料id',
  `im_num` int(11) DEFAULT NULL COMMENT '剩余数量',
  `material_name` varchar(11) DEFAULT NULL COMMENT '原料名称',
  `in_inventory_date` tinyint(4) DEFAULT NULL COMMENT '入库日期',
  `material_price` int(11) DEFAULT NULL COMMENT '成本',
  PRIMARY KEY (`material_id`),
  KEY `(contest_id,student_id)` (`contest_id`,`student_id`)
) ENGINE=InnoDB AUTO_INCREMENT=82 DEFAULT CHARSET=utf8;

drop table new_ct_kc_product_like;
drop table new_ct_kc_product_season_like;
CREATE TABLE `new_ct_kc_product_like` (
  `kc_product_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '产成品库存',
  `student_id` int(11) DEFAULT NULL,
  `contest_id` int(11) DEFAULT NULL,
  `ip_cp_id` int(11) DEFAULT NULL COMMENT '产品id',
  `ip_num` int(11) DEFAULT NULL COMMENT '库存数量',
  `line_id` int(10) DEFAULT NULL COMMENT '生产线id',
  `is_inventory` int(10) DEFAULT NULL COMMENT '是否入库',
  `inventory_date` int(10) DEFAULT NULL COMMENT '入库日期',
  `real_cost` varchar(10) DEFAULT NULL COMMENT '真实成本',
  PRIMARY KEY (`kc_product_id`),
  KEY `(contest_id,student_id)` (`contest_id`,`student_id`)
) ENGINE=InnoDB AUTO_INCREMENT=77 DEFAULT CHARSET=utf8;

CREATE TABLE `new_ct_kc_product_season_like` (
  `kc_product_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '产成品库存',
  `student_id` int(11) DEFAULT NULL,
  `contest_id` int(11) DEFAULT NULL,
  `ip_cp_id` int(11) DEFAULT NULL COMMENT '产品id',
  `ip_num` int(11) DEFAULT NULL COMMENT '库存数量',
  `line_id` int(10) DEFAULT NULL COMMENT '生产线id',
  `is_inventory` int(10) DEFAULT NULL COMMENT '是否入库',
  `inventory_date` int(10) DEFAULT NULL COMMENT '入库日期',
  `real_cost` varchar(10) DEFAULT NULL COMMENT '真实成本',
  PRIMARY KEY (`kc_product_id`),
  KEY `(contest_id,student_id)` (`contest_id`,`student_id`)
) ENGINE=InnoDB AUTO_INCREMENT=77 DEFAULT CHARSET=utf8;


drop table new_ct_line_like;
drop table new_ct_line_season_like;
CREATE TABLE `new_ct_line_like` (
  `line_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `student_id` int(11) NOT NULL,
  `contest_id` int(11) NOT NULL,
  `pl_cplid` int(11) NOT NULL COMMENT '生产线类型id',
  `pl_cpid` int(11) NOT NULL COMMENT '生产产品id',
  `pl_invest` int(11) NOT NULL COMMENT '原值',
  `pl_dep_total` int(11) NOT NULL COMMENT '累计折旧',
  `pl_product_add_date` tinyint(4) DEFAULT '0' COMMENT '开产时间',
  `pl_producting_date` tinyint(4) DEFAULT '0' COMMENT '剩余生产时间',
  `pl_finish_date` tinyint(4) DEFAULT '0' COMMENT '建成时间',
  `pl_remain_date` tinyint(4) DEFAULT '0' COMMENT '在建剩余时间',
  `pl_add_time` tinyint(4) DEFAULT '0' COMMENT '开建时间',
  `pl_transfer_add_date` tinyint(4) DEFAULT '0' COMMENT '转产时间',
  `pl_transfer_date` tinyint(4) DEFAULT '0' COMMENT '转产剩余时间',
  `status` varchar(20) DEFAULT NULL,
  `real_production` int(11) DEFAULT NULL COMMENT '产量',
  `classes_id` int(11) DEFAULT NULL COMMENT '班次id',
  PRIMARY KEY (`line_id`),
  KEY `(contest_id,student_id)` (`contest_id`,`student_id`)
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=utf8;

CREATE TABLE `new_ct_line_season_like` (
  `line_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `student_id` int(11) NOT NULL,
  `contest_id` int(11) NOT NULL,
  `pl_cplid` int(11) NOT NULL COMMENT '生产线类型id',
  `pl_cpid` int(11) NOT NULL COMMENT '生产产品id',
  `pl_invest` int(11) NOT NULL COMMENT '原值',
  `pl_dep_total` int(11) NOT NULL COMMENT '累计折旧',
  `pl_product_add_date` tinyint(4) DEFAULT '0' COMMENT '开产时间',
  `pl_producting_date` tinyint(4) DEFAULT '0' COMMENT '剩余生产时间',
  `pl_finish_date` tinyint(4) DEFAULT '0' COMMENT '建成时间',
  `pl_remain_date` tinyint(4) DEFAULT '0' COMMENT '在建剩余时间',
  `pl_add_time` tinyint(4) DEFAULT '0' COMMENT '开建时间',
  `pl_transfer_add_date` tinyint(4) DEFAULT '0' COMMENT '转产时间',
  `pl_transfer_date` tinyint(4) DEFAULT '0' COMMENT '转产剩余时间',
  `status` varchar(20) DEFAULT NULL,
  `real_production` int(11) DEFAULT NULL COMMENT '产量',
  `classes_id` int(11) DEFAULT NULL COMMENT '班次id',
  PRIMARY KEY (`line_id`),
  KEY `(contest_id,student_id)` (`contest_id`,`student_id`)
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=utf8;


drop table new_ct_xz_order_like;
drop table new_ct_xz_order_season_like;
CREATE TABLE `new_ct_xz_order_like` (
  `order_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `co_id` varchar(50) NOT NULL COMMENT '订单编号',
  `student_id` int(11) NOT NULL,
  `contest_id` int(11) NOT NULL,
  `date` tinyint(4) DEFAULT NULL COMMENT '年份',
  `quarterly` int(11) DEFAULT NULL COMMENT '季度',
  `cm_id` int(11) DEFAULT NULL COMMENT '市场',
  `cp_id` int(11) DEFAULT NULL COMMENT '产品',
  `num` int(11) DEFAULT NULL COMMENT '数量',
  `total_price` int(11) DEFAULT NULL COMMENT '总价',
  `delivery_date` tinyint(4) DEFAULT NULL COMMENT '交货期',
  `payment_date` tinyint(4) DEFAULT NULL COMMENT '账期',
  `iso_id` int(11) DEFAULT NULL COMMENT 'ISO',
  `commit_date` tinyint(4) DEFAULT NULL COMMENT '交单时间',
  `total_cost` int(11) DEFAULT NULL COMMENT '成本',
  PRIMARY KEY (`order_id`),
  KEY `(contest_id,student_id)` (`contest_id`,`student_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

CREATE TABLE `new_ct_xz_order_season_like` (
  `order_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `co_id` varchar(50) NOT NULL COMMENT '订单编号',
  `student_id` int(11) NOT NULL,
  `contest_id` int(11) NOT NULL,
  `date` tinyint(4) DEFAULT NULL COMMENT '年份',
  `quarterly` int(11) DEFAULT NULL COMMENT '季度',
  `cm_id` int(11) DEFAULT NULL COMMENT '市场',
  `cp_id` int(11) DEFAULT NULL COMMENT '产品',
  `num` int(11) DEFAULT NULL COMMENT '数量',
  `total_price` int(11) DEFAULT NULL COMMENT '总价',
  `delivery_date` tinyint(4) DEFAULT NULL COMMENT '交货期',
  `payment_date` tinyint(4) DEFAULT NULL COMMENT '账期',
  `iso_id` int(11) DEFAULT NULL COMMENT 'ISO',
  `commit_date` tinyint(4) DEFAULT NULL COMMENT '交单时间',
  `total_cost` int(11) DEFAULT NULL COMMENT '成本',
  PRIMARY KEY (`order_id`),
  KEY `(contest_id,student_id)` (`contest_id`,`student_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

drop table new_ct_yf_iso_like;
drop table new_ct_yf_iso_season_like;
CREATE TABLE `new_ct_yf_iso_like` (
  `iso_id` int(11) NOT NULL AUTO_INCREMENT,
  `student_id` int(11) NOT NULL,
  `contest_id` int(11) NOT NULL,
  `di_ciid` int(11) DEFAULT NULL COMMENT 'ISO规则_id',
  `di_total_date` tinyint(4) DEFAULT NULL COMMENT '总研发时间',
  `di_now_date` tinyint(4) DEFAULT NULL COMMENT '已研发时间',
  `di_remain_date` tinyint(4) DEFAULT NULL COMMENT '剩余研发时间',
  `di_finish_date` tinyint(4) DEFAULT NULL COMMENT '研发完成时间',
  `di_start_date` int(10) DEFAULT NULL COMMENT '申请时间',
  `di_state` varchar(10) DEFAULT NULL COMMENT '状态',
  PRIMARY KEY (`iso_id`),
  KEY `(contest_id,student_id)` (`contest_id`,`student_id`)
) ENGINE=InnoDB AUTO_INCREMENT=67 DEFAULT CHARSET=utf8;

CREATE TABLE `new_ct_yf_iso_season_like` (
  `iso_id` int(11) NOT NULL AUTO_INCREMENT,
  `student_id` int(11) NOT NULL,
  `contest_id` int(11) NOT NULL,
  `di_ciid` int(11) DEFAULT NULL COMMENT 'ISO规则_id',
  `di_total_date` tinyint(4) DEFAULT NULL COMMENT '总研发时间',
  `di_now_date` tinyint(4) DEFAULT NULL COMMENT '已研发时间',
  `di_remain_date` tinyint(4) DEFAULT NULL COMMENT '剩余研发时间',
  `di_finish_date` tinyint(4) DEFAULT NULL COMMENT '研发完成时间',
  `di_start_date` int(10) DEFAULT NULL COMMENT '申请时间',
  `di_state` varchar(10) DEFAULT NULL COMMENT '状态',
  PRIMARY KEY (`iso_id`),
  KEY `(contest_id,student_id)` (`contest_id`,`student_id`)
) ENGINE=InnoDB AUTO_INCREMENT=67 DEFAULT CHARSET=utf8;


drop table new_ct_yf_product_like;
drop table new_ct_yf_product_season_like;
CREATE TABLE `new_ct_yf_product_like` (
  `product_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `student_id` int(11) DEFAULT NULL,
  `contest_id` int(11) DEFAULT NULL,
  `dp_cp_id` int(11) DEFAULT NULL COMMENT '产品规则id',
  `dp_total_date` tinyint(4) DEFAULT NULL COMMENT '总研发时间',
  `dp_now_date` tinyint(4) DEFAULT NULL COMMENT '已研发时间',
  `dp_remain_date` tinyint(4) DEFAULT NULL COMMENT '剩余研发时间',
  `dp_finish_date` tinyint(4) DEFAULT NULL COMMENT '研发完成时间',
  `dp_start_date` int(10) DEFAULT NULL COMMENT '申请时间',
  `dp_state` varchar(10) DEFAULT NULL COMMENT '状态',
  PRIMARY KEY (`product_id`),
  KEY `(contest_id,student_id)` (`contest_id`,`student_id`)
) ENGINE=InnoDB AUTO_INCREMENT=129 DEFAULT CHARSET=utf8;

CREATE TABLE `new_ct_yf_product_season_like` (
  `product_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `student_id` int(11) DEFAULT NULL,
  `contest_id` int(11) DEFAULT NULL,
  `dp_cp_id` int(11) DEFAULT NULL COMMENT '产品规则id',
  `dp_total_date` tinyint(4) DEFAULT NULL COMMENT '总研发时间',
  `dp_now_date` tinyint(4) DEFAULT NULL COMMENT '已研发时间',
  `dp_remain_date` tinyint(4) DEFAULT NULL COMMENT '剩余研发时间',
  `dp_finish_date` tinyint(4) DEFAULT NULL COMMENT '研发完成时间',
  `dp_start_date` int(10) DEFAULT NULL COMMENT '申请时间',
  `dp_state` varchar(10) DEFAULT NULL COMMENT '状态',
  PRIMARY KEY (`product_id`),
  KEY `(contest_id,student_id)` (`contest_id`,`student_id`)
) ENGINE=InnoDB AUTO_INCREMENT=129 DEFAULT CHARSET=utf8;

drop table new_ct_yf_market_like;
drop table new_ct_yf_market_season_like;
CREATE TABLE `new_ct_yf_market_like` (
  `market_id` int(11) NOT NULL AUTO_INCREMENT,
  `student_id` int(11) DEFAULT NULL,
  `contest_id` int(11) DEFAULT NULL,
  `dm_cm_id` int(11) DEFAULT NULL COMMENT '市场规则id',
  `dm_total_date` tinyint(4) DEFAULT NULL COMMENT '总研发时间',
  `dm_now_date` tinyint(4) DEFAULT NULL COMMENT '已研发时间',
  `dm_remain_date` tinyint(4) DEFAULT NULL COMMENT '剩余研发时间',
  `dm_finish_date` tinyint(4) DEFAULT NULL COMMENT '研发完成时间',
  `dm_start_date` int(10) DEFAULT NULL COMMENT '申请时间',
  `dm_state` varchar(10) DEFAULT NULL COMMENT '状态',
  PRIMARY KEY (`market_id`),
  KEY `(contest_id,student_id)` (`contest_id`,`student_id`)
) ENGINE=InnoDB AUTO_INCREMENT=166 DEFAULT CHARSET=utf8;

CREATE TABLE `new_ct_yf_market_season_like` (
  `market_id` int(11) NOT NULL AUTO_INCREMENT,
  `student_id` int(11) DEFAULT NULL,
  `contest_id` int(11) DEFAULT NULL,
  `dm_cm_id` int(11) DEFAULT NULL COMMENT '市场规则id',
  `dm_total_date` tinyint(4) DEFAULT NULL COMMENT '总研发时间',
  `dm_now_date` tinyint(4) DEFAULT NULL COMMENT '已研发时间',
  `dm_remain_date` tinyint(4) DEFAULT NULL COMMENT '剩余研发时间',
  `dm_finish_date` tinyint(4) DEFAULT NULL COMMENT '研发完成时间',
  `dm_start_date` int(10) DEFAULT NULL COMMENT '申请时间',
  `dm_state` varchar(10) DEFAULT NULL COMMENT '状态',
  PRIMARY KEY (`market_id`),
  KEY `(contest_id,student_id)` (`contest_id`,`student_id`)
) ENGINE=InnoDB AUTO_INCREMENT=166 DEFAULT CHARSET=utf8;