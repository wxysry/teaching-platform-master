drop table new_ct_yf_num;
drop table new_ct_yf_num_like;
drop table new_ct_yf_num_season_like;

CREATE TABLE `new_ct_yf_num` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `student_id` int(11) DEFAULT NULL,
  `contest_id` int(11) DEFAULT NULL,
  `num_id` int(11) DEFAULT NULL,
  `start_date` int(11) DEFAULT NULL COMMENT '开始时间',
  `finish_date` tinyint(4) DEFAULT NULL COMMENT '结束时间',
  `consume_money` int(11) NOT NULL COMMENT '消耗金钱(元)',
  `state` varchar(10) DEFAULT NULL COMMENT '状态',
  PRIMARY KEY (`id`),
  KEY `(contest_id,student_id)` (`contest_id`,`student_id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8;

CREATE TABLE `new_ct_yf_num_like` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `student_id` int(11) DEFAULT NULL,
  `contest_id` int(11) DEFAULT NULL,
  `num_id` int(11) DEFAULT NULL,
  `start_date` int(11) DEFAULT NULL COMMENT '开始时间',
  `finish_date` tinyint(4) DEFAULT NULL COMMENT '结束时间',
  `consume_money` int(11) NOT NULL COMMENT '消耗金钱(元)',
  `state` varchar(10) DEFAULT NULL COMMENT '状态',
  PRIMARY KEY (`id`),
  KEY `(contest_id,student_id)` (`contest_id`,`student_id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8;

CREATE TABLE `new_ct_yf_num_season_like` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `student_id` int(11) DEFAULT NULL,
  `contest_id` int(11) DEFAULT NULL,
  `num_id` int(11) DEFAULT NULL,
  `start_date` int(11) DEFAULT NULL COMMENT '开始时间',
  `finish_date` tinyint(4) DEFAULT NULL COMMENT '结束时间',
  `consume_money` int(11) NOT NULL COMMENT '消耗金钱(元)',
  `state` varchar(10) DEFAULT NULL COMMENT '状态',
  PRIMARY KEY (`id`),
  KEY `(contest_id,student_id)` (`contest_id`,`student_id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8;