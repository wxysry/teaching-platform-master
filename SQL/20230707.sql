CREATE TABLE `new_ct_corporate_like` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '社会责任分',
  `score` int(11) NOT NULL COMMENT '分数',
  `student_id` int(11) NOT NULL COMMENT '用户id',
  `contest_id` int(11) NOT NULL COMMENT '竞赛id',
  PRIMARY KEY (`id`),
  KEY `(contest_id,student_id)` (`contest_id`,`student_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;


CREATE TABLE `new_ct_corporate_season_like` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '社会责任分',
  `score` int(11) NOT NULL COMMENT '分数',
  `student_id` int(11) NOT NULL COMMENT '用户id',
  `contest_id` int(11) NOT NULL COMMENT '竞赛id',
  PRIMARY KEY (`id`),
  KEY `(contest_id,student_id)` (`contest_id`,`student_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;