CREATE TABLE `gb_ct_balance` (
  `balance_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '资产负债表id',
  `student_id` int(11) NOT NULL COMMENT '学生id',
  `contest_id` int(11) NOT NULL COMMENT '考试id',
  `bs_isxt` tinyint(4) NOT NULL COMMENT '填写人：1代表系统自动生成，0代表学生录入',
  `bs_year` tinyint(4) NOT NULL COMMENT '年份',
  `bs_cash` int(11) NOT NULL COMMENT '现金',
  `bs_receivable` int(11) NOT NULL COMMENT '应收款',
  `bs_product_in_process` int(11) DEFAULT NULL COMMENT '在制品',
  `bs_product` int(11) DEFAULT NULL COMMENT '产成品',
  `bs_material` int(11) DEFAULT NULL COMMENT '原材料',
  `bs_total_current_asset` int(11) NOT NULL COMMENT '流动资产合计',
  `bs_equipment` int(11) DEFAULT NULL COMMENT '机器和设备',
  `bs_project_on_construction` int(11) DEFAULT NULL COMMENT '在建工程',
  `bs_total_fixed_asset` int(11) NOT NULL COMMENT '固定资产合计',
  `bs_total_asset` int(11) NOT NULL COMMENT '资产合计',
  `bs_long_loan` int(11) NOT NULL COMMENT '长期贷款',
  `bs_short_loan` int(11) NOT NULL COMMENT '短期贷款',
  `bs_other_pay` int(11) NOT NULL COMMENT '其他应付款',
  `bs_tax` int(11) NOT NULL COMMENT '应交税费',
  `bs_total_liability` int(11) NOT NULL COMMENT '负债合计',
  `bs_equity` int(11) NOT NULL COMMENT '股东资本',
  `bs_retained_earning` int(11) NOT NULL COMMENT '利润留存',
  `bs_annual_net_profit` int(11) NOT NULL COMMENT '年度净利',
  `bs_total_equity` int(11) NOT NULL COMMENT '所有者权益',
  `bs_total` int(11) NOT NULL COMMENT '负债所有者权益合计',
  `is_submit` varchar(1) NOT NULL DEFAULT 'Y' COMMENT '是否已提交,N未提交 Y已提交',
  PRIMARY KEY (`balance_id`),
  KEY `(contest_id,student_id)` (`contest_id`,`student_id`)
) ENGINE=InnoDB AUTO_INCREMENT=439 DEFAULT CHARSET=utf8;

CREATE TABLE `gb_ct_bank_loan` (
  `bl_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '贷款记录表',
  `student_id` int(11) NOT NULL,
  `contest_id` int(11) NOT NULL,
  `bl_type` tinyint(4) NOT NULL COMMENT '贷款类型:1表示长贷，2表示短贷',
  `bl_fee` int(11) NOT NULL COMMENT '贷款金额',
  `bl_add_time` tinyint(4) NOT NULL COMMENT '贷款时间',
  `bl_remain_time` tinyint(4) NOT NULL COMMENT '贷款时长',
  `bl_repayment_date` tinyint(4) NOT NULL COMMENT '还款时间',
  `repayment_type` varchar(4) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `interest` decimal(20,2) DEFAULT NULL,
  `bl_name` varchar(50) DEFAULT NULL,
  `rate` decimal(20,2) DEFAULT NULL COMMENT '利率',
  PRIMARY KEY (`bl_id`),
  KEY `(contest_id,student_id)` (`contest_id`,`student_id`)
) ENGINE=InnoDB AUTO_INCREMENT=73 DEFAULT CHARSET=utf8;

CREATE TABLE `gb_ct_cashflow` (
  `cashflow_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '现金流量表id',
  `student_id` int(11) NOT NULL COMMENT '学生id',
  `contest_id` int(11) NOT NULL COMMENT '考试id',
  `c_action` varchar(255) NOT NULL COMMENT '操作类型',
  `c_in` int(11) NOT NULL COMMENT '资金增加',
  `c_out` int(11) NOT NULL COMMENT '资金减少',
  `c_surplus` int(11) NOT NULL COMMENT '现金',
  `c_comment` varchar(255) DEFAULT NULL COMMENT '详情',
  `c_date` tinyint(4) NOT NULL COMMENT '操作时间',
  PRIMARY KEY (`cashflow_id`),
  KEY `c_action` (`c_action`),
  KEY `(contest_id,student_id)` (`contest_id`,`student_id`)
) ENGINE=InnoDB AUTO_INCREMENT=927 DEFAULT CHARSET=utf8;

CREATE TABLE `gb_ct_charges` (
  `charges_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '费用表id',
  `student_id` int(11) NOT NULL COMMENT '学生id',
  `contest_id` int(11) NOT NULL COMMENT '考试id',
  `bs_isxt` tinyint(4) NOT NULL COMMENT '填写人：1代表系统自动生成，0代表学生录入',
  `c_date` tinyint(4) NOT NULL COMMENT '年份',
  `c_overhaul` int(11) NOT NULL COMMENT '管理费用',
  `c_ad` int(11) NOT NULL COMMENT '广告费用',
  `c_maintenance` int(11) NOT NULL COMMENT '维修费',
  `c_transfer` int(11) NOT NULL COMMENT '转产费',
  `c_develop_market` int(11) NOT NULL COMMENT '市场开拓',
  `c_develop_product` int(11) NOT NULL COMMENT '产品研发',
  `c_develop_iso` int(11) NOT NULL COMMENT 'ISO开拓',
  `c_information` int(11) NOT NULL COMMENT '信息费',
  `c_hr` int(11) NOT NULL COMMENT '人力费',
  `c_digitalization` int(11) NOT NULL COMMENT '数字化研发费',
  `c_total` int(11) NOT NULL COMMENT '合计',
  `is_submit` varchar(1) NOT NULL DEFAULT 'Y' COMMENT '是否已提交,N未提交 Y已提交',
  PRIMARY KEY (`charges_id`),
  KEY `(contest_id,student_id)` (`contest_id`,`student_id`)
) ENGINE=InnoDB AUTO_INCREMENT=387 DEFAULT CHARSET=utf8;


CREATE TABLE `gb_ct_corporate` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '社会责任分',
  `score` int(11) NOT NULL COMMENT '分数',
  `student_id` int(11) NOT NULL COMMENT '用户id',
  `contest_id` int(11) NOT NULL COMMENT '竞赛id',
  `action` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `(contest_id,student_id)` (`contest_id`,`student_id`)
) ENGINE=InnoDB AUTO_INCREMENT=99 DEFAULT CHARSET=utf8;

CREATE TABLE `gb_ct_dd_material` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '原料采购记录',
  `om_cmid` int(10) DEFAULT NULL COMMENT '原料编号',
  `cm_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '原料名称',
  `student_id` int(11) DEFAULT NULL,
  `contest_id` int(11) DEFAULT NULL,
  `remain_date` tinyint(4) DEFAULT NULL COMMENT '收货期',
  `purchase_date` tinyint(4) DEFAULT NULL COMMENT '采购时间',
  `is_inventory` tinyint(4) DEFAULT NULL COMMENT '是否入库',
  `pay_date` tinyint(4) DEFAULT NULL COMMENT '付款周期（账期）',
  `is_pay` tinyint(4) DEFAULT NULL COMMENT '是否付款',
  `price` int(11) DEFAULT NULL COMMENT '价格',
  `num` int(11) DEFAULT NULL COMMENT '数量',
  PRIMARY KEY (`id`),
  KEY `(contest_id,student_id)` (`contest_id`,`student_id`)
) ENGINE=InnoDB AUTO_INCREMENT=158 DEFAULT CHARSET=utf8;

CREATE TABLE `gb_ct_fee` (
  `fee_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '应收款',
  `student_id` int(11) NOT NULL,
  `contest_id` int(11) NOT NULL,
  `r_fee` int(11) NOT NULL COMMENT '金额',
  `r_remain_date` tinyint(4) NOT NULL COMMENT '账期',
  `borrower` varchar(10) DEFAULT NULL COMMENT '借方',
  `lender` varchar(10) DEFAULT NULL COMMENT '贷方',
  `remarks` varchar(10) DEFAULT NULL COMMENT '备注',
  `payment_date` varchar(10) DEFAULT NULL COMMENT '收款日期',
  `type` varchar(10) DEFAULT NULL COMMENT '类型',
  PRIMARY KEY (`fee_id`),
  KEY `(contest_id,student_id)` (`contest_id`,`student_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4870 DEFAULT CHARSET=utf8;


CREATE TABLE `gb_ct_financial_target` (
  `ft_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '财务指标表id',
  `student_id` int(11) NOT NULL COMMENT '学生id',
  `contest_id` int(11) NOT NULL COMMENT '考试id',
  `ft_isxt` tinyint(4) NOT NULL COMMENT '填写人：1代表系统自动生成，0代表学生录入',
  `ft_year` tinyint(4) NOT NULL COMMENT '年份',
  `ft_current_rate` decimal(8,2) NOT NULL COMMENT '流动比率',
  `ft_quick_rate` decimal(8,2) NOT NULL COMMENT '速动比率',
  `ft_debt_rate` decimal(8,2) NOT NULL COMMENT '资产负债率',
  `ft_equity_rate` decimal(8,2) NOT NULL COMMENT '产权比率',
  `ft_net_profit_rate` decimal(8,2) NOT NULL COMMENT '营业净利润率',
  `ft_cost_expense_rate` decimal(8,2) NOT NULL COMMENT '成本费用率',
  `ft_return_assets_rate` decimal(8,2) NOT NULL COMMENT '资产报酬率',
  `ft_return_equity_rate` decimal(8,2) NOT NULL COMMENT '净资产收益率',
  `ft_revenue_growth_rate` decimal(8,2) NOT NULL COMMENT '营业收入增长率',
  `ft_appreciation_rate` decimal(8,2) NOT NULL COMMENT '资本保值增值率',
  `ft_total_growth_rate` decimal(8,2) NOT NULL COMMENT '总资产增长率',
  `ft_inventory_rate` decimal(8,2) NOT NULL COMMENT '总资产增长率',
  `ft_inventory_days` decimal(8,2) NOT NULL COMMENT '库存周转天数',
  `ft_receivable_rate` decimal(8,2) NOT NULL COMMENT '应收账款周转率',
  `ft_receivable_days` decimal(8,2) NOT NULL COMMENT '应收账款周转天数',
  `ft_cash_period` decimal(8,2) NOT NULL COMMENT '现金周转期',
  `is_submit` varchar(1) NOT NULL DEFAULT 'Y' COMMENT '是否已提交,N未提交 Y已提交',
  PRIMARY KEY (`ft_id`),
  KEY `(contest_id,student_id)` (`contest_id`,`student_id`)
) ENGINE=InnoDB AUTO_INCREMENT=340 DEFAULT CHARSET=utf8;

CREATE TABLE `gb_ct_kc_material` (
  `material_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `student_id` int(11) DEFAULT NULL,
  `contest_id` int(11) DEFAULT NULL,
  `im_cm_id` int(11) DEFAULT NULL COMMENT '原料id',
  `im_num` int(11) DEFAULT NULL COMMENT '剩余数量',
  `material_name` varchar(11) DEFAULT NULL COMMENT '原料名称',
  `in_inventory_date` tinyint(4) DEFAULT NULL COMMENT '入库日期',
  `material_price` int(11) DEFAULT NULL COMMENT '成本',
  PRIMARY KEY (`material_id`),
  KEY `(contest_id,student_id)` (`contest_id`,`student_id`)
) ENGINE=InnoDB AUTO_INCREMENT=178 DEFAULT CHARSET=utf8;

CREATE TABLE `gb_ct_kc_product` (
  `kc_product_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '产成品库存',
  `student_id` int(11) DEFAULT NULL,
  `contest_id` int(11) DEFAULT NULL,
  `ip_cp_id` int(11) DEFAULT NULL COMMENT '产品id',
  `ip_num` int(11) DEFAULT NULL COMMENT '库存数量',
  `line_id` int(10) DEFAULT NULL COMMENT '生产线id',
  `is_inventory` int(10) DEFAULT NULL COMMENT '是否入库',
  `inventory_date` int(10) DEFAULT NULL COMMENT '入库日期',
  `real_cost` varchar(10) DEFAULT NULL COMMENT '真实成本',
  PRIMARY KEY (`kc_product_id`),
  KEY `(contest_id,student_id)` (`contest_id`,`student_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1305 DEFAULT CHARSET=utf8;

CREATE TABLE `gb_ct_line` (
  `line_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `student_id` int(11) NOT NULL,
  `contest_id` int(11) NOT NULL,
  `pl_cplid` int(11) NOT NULL COMMENT '生产线类型id',
  `pl_cpid` int(11) NOT NULL COMMENT '生产产品id',
  `pl_invest` int(11) NOT NULL COMMENT '原值',
  `pl_dep_total` int(11) NOT NULL COMMENT '累计折旧',
  `pl_product_add_date` tinyint(4) DEFAULT '0' COMMENT '开产时间',
  `pl_producting_date` tinyint(4) DEFAULT '0' COMMENT '剩余生产时间',
  `pl_finish_date` tinyint(4) DEFAULT '0' COMMENT '建成时间',
  `pl_remain_date` tinyint(4) DEFAULT '0' COMMENT '在建剩余时间',
  `pl_add_time` tinyint(4) DEFAULT '0' COMMENT '开建时间',
  `pl_transfer_add_date` tinyint(4) DEFAULT '0' COMMENT '转产时间',
  `pl_transfer_date` tinyint(4) DEFAULT '0' COMMENT '转产剩余时间',
  `status` varchar(20) DEFAULT NULL,
  `real_production` int(11) DEFAULT NULL COMMENT '产量',
  `classes_id` int(11) DEFAULT NULL COMMENT '班次id',
  PRIMARY KEY (`line_id`),
  KEY `(contest_id,student_id)` (`contest_id`,`student_id`)
) ENGINE=InnoDB AUTO_INCREMENT=67 DEFAULT CHARSET=utf8;


CREATE TABLE `gb_ct_mn_ad` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '模拟广告',
  `student_id` int(11) DEFAULT NULL COMMENT '学生id',
  `contest_id` int(11) DEFAULT NULL COMMENT '考试id',
  `year` tinyint(4) NOT NULL COMMENT '年份',
  `quarterly` int(11) DEFAULT NULL COMMENT '季度',
  `group_num` varchar(100) DEFAULT NULL COMMENT '组号',
  `local_p1` decimal(15,5) DEFAULT NULL COMMENT '本地P1',
  `regional_p1` decimal(15,5) DEFAULT NULL COMMENT '区域P1',
  `national_p1` decimal(15,5) DEFAULT NULL COMMENT '国内P1',
  `asian_p1` decimal(15,5) DEFAULT NULL COMMENT '亚洲P1',
  `international_p1` decimal(15,5) DEFAULT NULL COMMENT '国际P1',
  `local_p2` decimal(15,5) DEFAULT NULL COMMENT '本地P2',
  `regional_p2` decimal(15,5) DEFAULT NULL COMMENT '区域P2',
  `national_p2` decimal(15,5) DEFAULT NULL COMMENT '国内P2',
  `asian_p2` decimal(15,5) DEFAULT NULL COMMENT '亚洲P2',
  `international_p2` decimal(15,5) DEFAULT NULL COMMENT '国际P2',
  `local_p3` decimal(15,5) DEFAULT NULL COMMENT '本地P3',
  `regional_p3` decimal(15,5) DEFAULT NULL COMMENT '区域P3',
  `national_p3` decimal(15,5) DEFAULT NULL COMMENT '国内P3',
  `asian_p3` decimal(15,5) DEFAULT NULL COMMENT '亚洲P3',
  `international_p3` decimal(15,5) DEFAULT NULL COMMENT '国际P3',
  `local_p4` decimal(15,5) DEFAULT NULL COMMENT '本地P4',
  `regional_p4` decimal(15,5) DEFAULT NULL COMMENT '区域P4',
  `national_p4` decimal(15,5) DEFAULT NULL COMMENT '国内P4',
  `asian_p4` decimal(15,5) DEFAULT NULL COMMENT '亚洲P4',
  `international_p4` decimal(15,5) DEFAULT NULL COMMENT '国际P4',
  `local_p5` decimal(15,5) DEFAULT NULL COMMENT '本地P5',
  `regional_p5` decimal(15,5) DEFAULT NULL COMMENT '区域P5',
  `national_p5` decimal(15,5) DEFAULT NULL COMMENT '国内P5',
  `asian_p5` decimal(15,5) DEFAULT NULL COMMENT '亚洲P5',
  `international_p5` decimal(15,5) DEFAULT NULL COMMENT '国际P5',
  PRIMARY KEY (`id`),
  KEY `(contest_id,student_id)` (`contest_id`,`student_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3065 DEFAULT CHARSET=utf8;


CREATE TABLE `gb_ct_mn_choose` (
  `choose_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `student_id` int(11) DEFAULT NULL,
  `contest_id` int(11) DEFAULT NULL,
  `co_id` varchar(50) DEFAULT NULL COMMENT '定单编号',
  `date` int(11) DEFAULT NULL COMMENT '年份',
  `quarterly` int(11) DEFAULT NULL COMMENT '季度',
  `cm_id` int(11) DEFAULT NULL COMMENT '市场',
  `cp_id` int(11) DEFAULT NULL COMMENT '产品编号',
  `num` int(11) DEFAULT NULL COMMENT '数量',
  `total_price` int(11) DEFAULT NULL COMMENT '总价',
  `delivery_date` int(11) DEFAULT NULL COMMENT '交货期',
  `payment_day` int(11) DEFAULT NULL COMMENT '账期',
  `ci_id` int(11) DEFAULT NULL COMMENT 'ISO编号，为空代表不需要任何iso认证，为3代表需要全部iso认证，为1或2代表需要1或2认证',
  `xz_round` int(11) DEFAULT NULL COMMENT '选走轮次',
  `xz_group` varchar(50) DEFAULT NULL COMMENT '选走组号',
  PRIMARY KEY (`choose_id`),
  KEY `(contest_id,student_id)` (`contest_id`,`student_id`)
) ENGINE=InnoDB AUTO_INCREMENT=26883 DEFAULT CHARSET=utf8;


CREATE TABLE `gb_ct_profit_chart` (
  `profit_chart_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '利润表id',
  `student_id` int(11) NOT NULL COMMENT '学生id',
  `contest_id` int(11) NOT NULL COMMENT '考试id',
  `bs_isxt` tinyint(4) NOT NULL COMMENT '填写人：1代表系统自动生成，0代表学生录入',
  `pc_date` tinyint(4) NOT NULL COMMENT '年份',
  `pc_sales` int(11) NOT NULL COMMENT '销售额',
  `pc_direct_cost` int(11) NOT NULL COMMENT '成本',
  `pc_goods_profit` int(11) NOT NULL COMMENT '毛利',
  `pc_total` int(11) NOT NULL COMMENT '综合费用',
  `pc_profit_before_dep` int(11) NOT NULL COMMENT '折旧前利润',
  `pc_dep` int(11) NOT NULL COMMENT '折旧',
  `pc_profit_before_interests` int(11) NOT NULL COMMENT '支付利息前利润',
  `pc_finance_fee` int(11) NOT NULL COMMENT '财务费用',
  `pc_non_operating` int(11) NOT NULL COMMENT '营业外收支',
  `pc_profit_before_tax` int(11) NOT NULL COMMENT '税前利润',
  `pc_tax` int(11) NOT NULL COMMENT '所得税',
  `pc_annual_net_profit` int(11) NOT NULL COMMENT '净利润',
  `is_submit` varchar(1) NOT NULL DEFAULT 'Y' COMMENT '是否已提交,N未提交 Y已提交',
  PRIMARY KEY (`profit_chart_id`),
  KEY `(contest_id,student_id)` (`contest_id`,`student_id`)
) ENGINE=InnoDB AUTO_INCREMENT=387 DEFAULT CHARSET=utf8;


CREATE TABLE `gb_ct_t_time` (
  `time_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `student_id` int(11) NOT NULL COMMENT '学生id',
  `contest_id` int(11) NOT NULL COMMENT '竞赛id',
  `date` tinyint(4) NOT NULL COMMENT '考试时间',
  PRIMARY KEY (`time_id`),
  KEY `(contest_id,student_id)` (`contest_id`,`student_id`)
) ENGINE=InnoDB AUTO_INCREMENT=66 DEFAULT CHARSET=utf8;


CREATE TABLE `gb_ct_worker` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '工人ID',
  `student_id` int(11) NOT NULL COMMENT '学生id',
  `contest_id` int(11) NOT NULL COMMENT '竞赛ID',
  `worker_name` varchar(50) NOT NULL COMMENT '姓名',
  `worker_num` int(11) NOT NULL COMMENT '序号',
  `recruit_name` varchar(20) NOT NULL COMMENT '等级',
  `recruit_num` varchar(20) NOT NULL COMMENT '等级编码',
  `mult_bonus` int(11) NOT NULL COMMENT '倍数加成(%)',
  `init_sal` int(11) NOT NULL COMMENT '工资(元)',
  `start_work_time` int(11) NOT NULL COMMENT '招聘时间',
  `is_work` int(11) NOT NULL COMMENT '是否在职',
  `piece` int(11) DEFAULT NULL COMMENT '计件',
  `line_id` int(11) DEFAULT NULL COMMENT '产线id',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=137 DEFAULT CHARSET=utf8;

CREATE TABLE `gb_ct_xz_order` (
  `order_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `co_id` varchar(50) NOT NULL COMMENT '订单编号',
  `student_id` int(11) NOT NULL,
  `contest_id` int(11) NOT NULL,
  `date` tinyint(4) DEFAULT NULL COMMENT '年份',
  `quarterly` int(11) DEFAULT NULL COMMENT '季度',
  `cm_id` int(11) DEFAULT NULL COMMENT '市场',
  `cp_id` int(11) DEFAULT NULL COMMENT '产品',
  `num` int(11) DEFAULT NULL COMMENT '数量',
  `total_price` int(11) DEFAULT NULL COMMENT '总价',
  `delivery_date` tinyint(4) DEFAULT NULL COMMENT '交货期',
  `payment_date` tinyint(4) DEFAULT NULL COMMENT '账期',
  `iso_id` int(11) DEFAULT NULL COMMENT 'ISO',
  `commit_date` tinyint(4) DEFAULT NULL COMMENT '交单时间',
  `total_cost` int(11) DEFAULT NULL COMMENT '成本',
  PRIMARY KEY (`order_id`),
  KEY `(contest_id,student_id)` (`contest_id`,`student_id`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8;


CREATE TABLE `gb_ct_yf_iso` (
  `iso_id` int(11) NOT NULL AUTO_INCREMENT,
  `student_id` int(11) NOT NULL,
  `contest_id` int(11) NOT NULL,
  `di_ciid` int(11) DEFAULT NULL COMMENT 'ISO规则_id',
  `di_total_date` tinyint(4) DEFAULT NULL COMMENT '总研发时间',
  `di_now_date` tinyint(4) DEFAULT NULL COMMENT '已研发时间',
  `di_remain_date` tinyint(4) DEFAULT NULL COMMENT '剩余研发时间',
  `di_finish_date` tinyint(4) DEFAULT NULL COMMENT '研发完成时间',
  `di_start_date` int(10) DEFAULT NULL COMMENT '申请时间',
  `di_state` varchar(10) DEFAULT NULL COMMENT '状态',
  PRIMARY KEY (`iso_id`),
  KEY `(contest_id,student_id)` (`contest_id`,`student_id`)
) ENGINE=InnoDB AUTO_INCREMENT=165 DEFAULT CHARSET=utf8;


CREATE TABLE `gb_ct_yf_market` (
  `market_id` int(11) NOT NULL AUTO_INCREMENT,
  `student_id` int(11) DEFAULT NULL,
  `contest_id` int(11) DEFAULT NULL,
  `dm_cm_id` int(11) DEFAULT NULL COMMENT '市场规则id',
  `dm_total_date` tinyint(4) DEFAULT NULL COMMENT '总研发时间',
  `dm_now_date` tinyint(4) DEFAULT NULL COMMENT '已研发时间',
  `dm_remain_date` tinyint(4) DEFAULT NULL COMMENT '剩余研发时间',
  `dm_finish_date` tinyint(4) DEFAULT NULL COMMENT '研发完成时间',
  `dm_start_date` int(10) DEFAULT NULL COMMENT '申请时间',
  `dm_state` varchar(10) DEFAULT NULL COMMENT '状态',
  PRIMARY KEY (`market_id`),
  KEY `(contest_id,student_id)` (`contest_id`,`student_id`)
) ENGINE=InnoDB AUTO_INCREMENT=411 DEFAULT CHARSET=utf8;

CREATE TABLE `gb_ct_yf_num` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `student_id` int(11) DEFAULT NULL,
  `contest_id` int(11) DEFAULT NULL,
  `num_id` int(11) DEFAULT NULL,
  `start_date` int(11) DEFAULT NULL COMMENT '开始时间',
  `finish_date` tinyint(4) DEFAULT NULL COMMENT '结束时间',
  `consume_money` int(11) NOT NULL COMMENT '消耗金钱(元)',
  `state` varchar(10) DEFAULT NULL COMMENT '状态',
  PRIMARY KEY (`id`),
  KEY `(contest_id,student_id)` (`contest_id`,`student_id`)
) ENGINE=InnoDB AUTO_INCREMENT=131 DEFAULT CHARSET=utf8;


CREATE TABLE `gb_ct_yf_product` (
  `product_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `student_id` int(11) DEFAULT NULL,
  `contest_id` int(11) DEFAULT NULL,
  `dp_cp_id` int(11) DEFAULT NULL COMMENT '产品规则id',
  `dp_total_date` tinyint(4) DEFAULT NULL COMMENT '总研发时间',
  `dp_now_date` tinyint(4) DEFAULT NULL COMMENT '已研发时间',
  `dp_remain_date` tinyint(4) DEFAULT NULL COMMENT '剩余研发时间',
  `dp_finish_date` tinyint(4) DEFAULT NULL COMMENT '研发完成时间',
  `dp_start_date` int(10) DEFAULT NULL COMMENT '申请时间',
  `dp_state` varchar(10) DEFAULT NULL COMMENT '状态',
  PRIMARY KEY (`product_id`),
  KEY `(contest_id,student_id)` (`contest_id`,`student_id`)
) ENGINE=InnoDB AUTO_INCREMENT=325 DEFAULT CHARSET=utf8;

CREATE TABLE `gb_re_payment_info` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `student_id` int(11) DEFAULT NULL,
  `contest_id` int(11) DEFAULT NULL,
  `loan_id` int(11) DEFAULT NULL COMMENT '贷款ID',
  `bl_add_time` int(11) DEFAULT NULL COMMENT '贷款时间',
  `pay_time` int(10) DEFAULT NULL COMMENT '缴费时间',
  `payment` varchar(50) DEFAULT NULL COMMENT '款项',
  `amount` decimal(20,2) DEFAULT NULL COMMENT '金额',
  `description` varchar(50) DEFAULT NULL COMMENT 'description',
  PRIMARY KEY (`id`),
  KEY `(contest_id,student_id)` (`contest_id`,`student_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1437 DEFAULT CHARSET=utf8;

