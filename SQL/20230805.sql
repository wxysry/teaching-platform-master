ALTER TABLE gb_contest add teacher_date tinyint(4) NOT NULL COMMENT '教师时间';
ALTER TABLE gb_contest add is_syn tinyint(4) NOT NULL COMMENT '是否统一时间进度';
ALTER TABLE gb_contest add last_fair_order_date tinyint(4) NOT NULL COMMENT '最后一次订货会时间';
ALTER TABLE gb_contest add is_suspend tinyint(4) NOT NULL COMMENT '是否暂停，0未暂停，1暂停';
ALTER TABLE gb_contest add remain_time int(10) NOT NULL DEFAULT 0 COMMENT '当前回合剩余时间,单位毫秒';
ALTER TABLE gb_contest add count_down varchar(255) DEFAULT NULL COMMENT '回合倒计时，用于设置各个回合的时间';
ALTER TABLE gb_contest add traditional_mode tinyint(4) NOT NULL COMMENT '是否传统模式';

/*删除无用字段*/
ALTER TABLE `gb_contest`
DROP COLUMN `open_time_start`,
DROP COLUMN `open_time_end`,
DROP COLUMN `restart`,
DROP COLUMN `restore`,
DROP COLUMN `restore_season`,
DROP COLUMN `restore_order`,
DROP COLUMN `export_score`;

ALTER TABLE gb_contest add market_list varchar(500) DEFAULT NULL COMMENT '市场的选单状态';
ALTER TABLE gb_contest add market_sort varchar(500) DEFAULT NULL COMMENT '选单市场排序';
ALTER TABLE gb_contest add product_sort varchar(500) DEFAULT NULL COMMENT '选单产品排序';

ALTER TABLE gb_contest DROP is_suspend;
ALTER TABLE gb_contest add is_suspend  tinyint(4) NOT NULL DEFAULT '1' COMMENT '0未暂停，1暂停';
ALTER TABLE gb_contest DROP last_fair_order_date;
ALTER TABLE gb_contest add last_fair_order_date tinyint(4) DEFAULT NULL COMMENT '最后一次订货会时间';

=======================7。26
CREATE TABLE `gb_ct_capital` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '注资记录id',
  `student_id` int(11) NOT NULL,
  `contest_id` int(11) NOT NULL,
  `ca_date` tinyint(4) NOT NULL COMMENT '当前时间',
  `add_capital` int(11) NOT NULL COMMENT '注资金额',
  `material_info` varchar(500) DEFAULT NULL COMMENT '一键下载',
  PRIMARY KEY (`id`),
  KEY `(contest_id,student_id)` (`contest_id`,`student_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4870 DEFAULT CHARSET=utf8;

CREATE TABLE `gb_ct_announcement` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '公告信息id',
  `contest_id` int(11) NOT NULL,
  `an_date` tinyint(4) NOT NULL COMMENT '公告时间',
  `info` varchar(255) DEFAULT NULL COMMENT '消息内容',
	`financial` varchar(255) DEFAULT NULL COMMENT '企业报表',
	`advertising` varchar(255) DEFAULT NULL COMMENT '广告下发',
	`all_data` varchar(255) DEFAULT NULL COMMENT '一键下载',
	  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`),
  KEY `(contest_id)` (`contest_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4870 DEFAULT CHARSET=utf8;

