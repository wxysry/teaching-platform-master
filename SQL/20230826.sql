-- 创建存储过程计算时间差
CREATE FUNCTION calSeasonInterval(start INT, end INT) RETURNS INT DETERMINISTIC
BEGIN
    DECLARE startYear INT;
    DECLARE startQuarter INT;
    DECLARE endYear INT;
    DECLARE endQuarter INT;
    DECLARE startTotalQuarter INT;
    DECLARE endTotalQuarter INT;

    SET startYear = start DIV 10;
    SET startQuarter = start MOD 10;
    SET endYear = end DIV 10;
    SET endQuarter = end MOD 10;
    SET startTotalQuarter = startYear * 4 + startQuarter;
    SET endTotalQuarter = endYear * 4 + endQuarter;

RETURN endTotalQuarter - startTotalQuarter;
END;




CREATE TABLE `new_contest_student_like` (
                                            `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '竞赛模拟的学生',
                                            `student_id` int(11) NOT NULL COMMENT '学生id',
                                            `contest_id` int(11) NOT NULL COMMENT '竞赛id',
                                            `start` tinyint(4) NOT NULL DEFAULT '0' COMMENT '是否已开始经营',
                                            `score` decimal(5,2) DEFAULT NULL COMMENT '分数',
                                            `progress` varchar(255) DEFAULT NULL COMMENT '经营进度，前端用于显示按钮',
                                            `date` mediumint(9) DEFAULT NULL COMMENT '经营时间进度,年份+季度+进度',
                                            `market_list` varchar(255) DEFAULT NULL COMMENT '选单进度，前端需要',
                                            `active_name` varchar(255) DEFAULT NULL COMMENT '选单进度，前端需要',
                                            `error_report_year` varchar(20) DEFAULT NULL,
                                            `finish_time` datetime DEFAULT NULL COMMENT '完成时间，为空表示未完成',
                                            PRIMARY KEY (`id`),
                                            UNIQUE KEY `(student_id,contest_id)` (`student_id`,`contest_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;



CREATE TABLE `new_contest_student_season_like` (
                                                   `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '竞赛模拟的学生',
                                                   `student_id` int(11) NOT NULL COMMENT '学生id',
                                                   `contest_id` int(11) NOT NULL COMMENT '竞赛id',
                                                   `start` tinyint(4) NOT NULL DEFAULT '0' COMMENT '是否已开始经营',
                                                   `score` decimal(5,2) DEFAULT NULL COMMENT '分数',
                                                   `progress` varchar(255) DEFAULT NULL COMMENT '经营进度，前端用于显示按钮',
                                                   `date` mediumint(9) DEFAULT NULL COMMENT '经营时间进度,年份+季度+进度',
                                                   `market_list` varchar(255) DEFAULT NULL COMMENT '选单进度，前端需要',
                                                   `active_name` varchar(255) DEFAULT NULL COMMENT '选单进度，前端需要',
                                                   `error_report_year` varchar(20) DEFAULT NULL,
                                                   `finish_time` datetime DEFAULT NULL COMMENT '完成时间，为空表示未完成',
                                                   PRIMARY KEY (`id`),
                                                   UNIQUE KEY `(student_id,contest_id)` (`student_id`,`contest_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;