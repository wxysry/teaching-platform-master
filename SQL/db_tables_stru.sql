/*
SQLyog Professional v12.09 (64 bit)
MySQL - 5.7.24-0ubuntu0.16.04.1 : Database - teaching_platform
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
USE `teaching_platform`;

/*Table structure for table `contest` */

DROP TABLE IF EXISTS `contest`;

CREATE TABLE `contest` (
  `contest_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '竞赛id',
  `subject_number` int(11) NOT NULL COMMENT '题库号',
  `title` varchar(100) DEFAULT NULL COMMENT '竞赛标题',
  `open_time_start` datetime DEFAULT NULL COMMENT '竞赛起始时间',
  `open_time_end` datetime DEFAULT NULL COMMENT '竞赛结束时间',
  `teacher_id` int(11) DEFAULT NULL COMMENT '教师id',
  `is_publish` tinyint(4) NOT NULL DEFAULT '0' COMMENT '是否发布竞赛',
  `student_num` int(11) DEFAULT NULL COMMENT '学生数量',
  `publish_all` tinyint(4) DEFAULT '1' COMMENT '0表示向指定考生发布，1表示向全部考生发布',
  `equity` mediumint(9) NOT NULL DEFAULT '0' COMMENT '初始权益，即初始资金',
  `restart` tinyint(4) NOT NULL DEFAULT '1' COMMENT '是否可以重新经营',
  `restore` tinyint(4) NOT NULL DEFAULT '1' COMMENT '是否可以还原本年',
  `restore_order` tinyint(4) NOT NULL DEFAULT '0' COMMENT '是否可以重新选单',
  `export_score` tinyint(4) NOT NULL DEFAULT '0' COMMENT '是否可以导出成绩',
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`contest_id`),
  KEY `subject_number` (`subject_number`),
  KEY `teacher_id` (`teacher_id`),
  KEY `open_time_end` (`open_time_end`)
) ENGINE=InnoDB AUTO_INCREMENT=167 DEFAULT CHARSET=utf8;

/*Table structure for table `contest_group` */

DROP TABLE IF EXISTS `contest_group`;

CREATE TABLE `contest_group` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '竞赛模拟分组id',
  `group_name` varchar(50) DEFAULT NULL COMMENT '分组名称',
  `deleted` tinyint(4) NOT NULL DEFAULT '0' COMMENT '是否删除',
  `update_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `create_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `group_name` (`group_name`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;

/*Table structure for table `contest_student` */

DROP TABLE IF EXISTS `contest_student`;

CREATE TABLE `contest_student` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '竞赛模拟的学生',
  `student_id` int(11) NOT NULL COMMENT '学生id',
  `contest_id` int(11) NOT NULL COMMENT '竞赛id',
  `start` tinyint(4) NOT NULL DEFAULT '0' COMMENT '是否已开始经营',
  `score` decimal(5,2) DEFAULT NULL COMMENT '分数',
  `progress` varchar(255) DEFAULT NULL COMMENT '经营进度，前端用于显示按钮',
  `date` mediumint(9) DEFAULT NULL COMMENT '经营时间进度,年份+季度+进度',
  `market_list` varchar(255) DEFAULT NULL COMMENT '选单进度，前端需要',
  `active_name` varchar(255) DEFAULT NULL COMMENT '选单进度，前端需要',
  `error_report_year` varchar(10) NOT NULL DEFAULT '' COMMENT '报表填写错误年数，1,2,3表示第1、2、3年填写错误',
  `finish_time` datetime DEFAULT NULL COMMENT '完成时间，为空表示未完成',
  PRIMARY KEY (`id`),
  UNIQUE KEY `(student_id,contest_id)` (`student_id`,`contest_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2190 DEFAULT CHARSET=utf8;

/*Table structure for table `ct_balance` */

DROP TABLE IF EXISTS `ct_balance`;

CREATE TABLE `ct_balance` (
  `balance_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '资产负债表id',
  `student_id` int(11) NOT NULL COMMENT '学生id',
  `contest_id` int(11) NOT NULL COMMENT '考试id',
  `bs_isxt` tinyint(4) NOT NULL COMMENT '填写人：1代表系统自动生成，0代表学生录入',
  `bs_year` tinyint(4) NOT NULL COMMENT '年份',
  `bs_cash` int(11) NOT NULL COMMENT '现金',
  `bs_receivable` int(11) NOT NULL COMMENT '应收款',
  `bs_product_in_process` int(11) DEFAULT NULL COMMENT '在制品',
  `bs_product` int(11) DEFAULT NULL COMMENT '产成品',
  `bs_material` int(11) DEFAULT NULL COMMENT '原材料',
  `bs_total_current_asset` int(11) NOT NULL COMMENT '流动资产合计',
  `bs_workshop` int(11) DEFAULT NULL COMMENT '厂房',
  `bs_equipment` int(11) DEFAULT NULL COMMENT '生产线',
  `bs_project_on_construction` int(11) DEFAULT NULL COMMENT '在建',
  `bs_total_fixed_asset` int(11) NOT NULL COMMENT '固定资产合计',
  `bs_total_asset` int(11) NOT NULL COMMENT '资产合计',
  `bs_long_loan` int(11) NOT NULL COMMENT '长期贷款',
  `bs_short_loan` int(11) NOT NULL COMMENT '短期贷款',
  `bs_tax` int(11) NOT NULL COMMENT '应交税费',
  `bs_total_liability` int(11) NOT NULL COMMENT '负债合计',
  `bs_equity` int(11) NOT NULL COMMENT '股东资本',
  `bs_retained_earning` int(11) NOT NULL COMMENT '利润留存',
  `bs_annual_net_profit` int(11) NOT NULL COMMENT '年度净利',
  `bs_total_equity` int(11) NOT NULL COMMENT '所有者权益',
  `bs_total` int(11) NOT NULL COMMENT '负债所有者权益合计',
  PRIMARY KEY (`balance_id`),
  KEY `(contest_id,student_id)` (`contest_id`,`student_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2766 DEFAULT CHARSET=utf8;

/*Table structure for table `ct_balance_like` */

DROP TABLE IF EXISTS `ct_balance_like`;

CREATE TABLE `ct_balance_like` (
  `balance_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '资产负债表id',
  `student_id` int(11) NOT NULL COMMENT '学生id',
  `contest_id` int(11) NOT NULL COMMENT '考试id',
  `bs_isxt` tinyint(4) NOT NULL COMMENT '填写人：1代表系统自动生成，0代表学生录入',
  `bs_year` tinyint(4) NOT NULL COMMENT '年份',
  `bs_cash` int(11) NOT NULL COMMENT '现金',
  `bs_receivable` int(11) NOT NULL COMMENT '应收款',
  `bs_product_in_process` int(11) DEFAULT NULL COMMENT '在制品',
  `bs_product` int(11) DEFAULT NULL COMMENT '产成品',
  `bs_material` int(11) DEFAULT NULL COMMENT '原材料',
  `bs_total_current_asset` int(11) NOT NULL COMMENT '流动资产合计',
  `bs_workshop` int(11) DEFAULT NULL COMMENT '厂房',
  `bs_equipment` int(11) DEFAULT NULL COMMENT '生产线',
  `bs_project_on_construction` int(11) DEFAULT NULL COMMENT '在建',
  `bs_total_fixed_asset` int(11) NOT NULL COMMENT '固定资产合计',
  `bs_total_asset` int(11) NOT NULL COMMENT '资产合计',
  `bs_long_loan` int(11) NOT NULL COMMENT '长期贷款',
  `bs_short_loan` int(11) NOT NULL COMMENT '短期贷款',
  `bs_tax` int(11) NOT NULL COMMENT '应交税费',
  `bs_total_liability` int(11) NOT NULL COMMENT '负债合计',
  `bs_equity` int(11) NOT NULL COMMENT '股东资本',
  `bs_retained_earning` int(11) NOT NULL COMMENT '利润留存',
  `bs_annual_net_profit` int(11) NOT NULL COMMENT '年度净利',
  `bs_total_equity` int(11) NOT NULL COMMENT '所有者权益',
  `bs_total` int(11) NOT NULL COMMENT '负债所有者权益合计',
  PRIMARY KEY (`balance_id`),
  KEY `(contest_id,student_id)` (`contest_id`,`student_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2765 DEFAULT CHARSET=utf8;

/*Table structure for table `ct_bank_loan` */

DROP TABLE IF EXISTS `ct_bank_loan`;

CREATE TABLE `ct_bank_loan` (
  `bl_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '贷款记录表',
  `student_id` int(11) NOT NULL,
  `contest_id` int(11) NOT NULL,
  `bl_type` tinyint(4) NOT NULL COMMENT '贷款类型:1表示长贷，2表示短贷',
  `bl_fee` int(11) NOT NULL COMMENT '贷款金额',
  `bl_add_time` tinyint(4) NOT NULL COMMENT '贷款时间',
  `bl_remain_time` tinyint(4) NOT NULL COMMENT '贷款时长',
  `bl_repayment_date` tinyint(4) NOT NULL COMMENT '还款时间',
  PRIMARY KEY (`bl_id`),
  KEY `(contest_id,student_id)` (`contest_id`,`student_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4449 DEFAULT CHARSET=utf8;

/*Table structure for table `ct_bank_loan_like` */

DROP TABLE IF EXISTS `ct_bank_loan_like`;

CREATE TABLE `ct_bank_loan_like` (
  `bl_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '贷款记录表',
  `student_id` int(11) NOT NULL,
  `contest_id` int(11) NOT NULL,
  `bl_type` tinyint(4) NOT NULL COMMENT '贷款类型:1表示长贷，2表示短贷',
  `bl_fee` int(11) NOT NULL COMMENT '贷款金额',
  `bl_add_time` tinyint(4) NOT NULL COMMENT '贷款时间',
  `bl_remain_time` tinyint(4) NOT NULL COMMENT '贷款时长',
  `bl_repayment_date` tinyint(4) NOT NULL COMMENT '还款时间',
  PRIMARY KEY (`bl_id`),
  KEY `(contest_id,student_id)` (`contest_id`,`student_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4445 DEFAULT CHARSET=utf8;

/*Table structure for table `ct_cashflow` */

DROP TABLE IF EXISTS `ct_cashflow`;

CREATE TABLE `ct_cashflow` (
  `cashflow_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '现金流量表id',
  `student_id` int(11) NOT NULL COMMENT '学生id',
  `contest_id` int(11) NOT NULL COMMENT '考试id',
  `c_action` varchar(255) NOT NULL COMMENT '操作类型',
  `c_in` int(11) NOT NULL COMMENT '资金增加',
  `c_out` int(11) NOT NULL COMMENT '资金减少',
  `c_surplus` int(11) NOT NULL COMMENT '现金',
  `c_comment` varchar(255) DEFAULT NULL COMMENT '详情',
  `c_date` tinyint(4) NOT NULL COMMENT '操作时间',
  PRIMARY KEY (`cashflow_id`),
  KEY `c_action` (`c_action`),
  KEY `(contest_id,student_id)` (`contest_id`,`student_id`)
) ENGINE=InnoDB AUTO_INCREMENT=65328 DEFAULT CHARSET=utf8;

/*Table structure for table `ct_cashflow_like` */

DROP TABLE IF EXISTS `ct_cashflow_like`;

CREATE TABLE `ct_cashflow_like` (
  `cashflow_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '现金流量表id',
  `student_id` int(11) NOT NULL COMMENT '学生id',
  `contest_id` int(11) NOT NULL COMMENT '考试id',
  `c_action` varchar(255) NOT NULL COMMENT '操作类型',
  `c_in` int(11) NOT NULL COMMENT '资金增加',
  `c_out` int(11) NOT NULL COMMENT '资金减少',
  `c_surplus` int(11) NOT NULL COMMENT '现金',
  `c_comment` varchar(255) DEFAULT NULL COMMENT '详情',
  `c_date` tinyint(4) NOT NULL COMMENT '操作时间',
  PRIMARY KEY (`cashflow_id`),
  KEY `c_action` (`c_action`),
  KEY `(contest_id,student_id)` (`contest_id`,`student_id`)
) ENGINE=InnoDB AUTO_INCREMENT=65296 DEFAULT CHARSET=utf8;

/*Table structure for table `ct_charges` */

DROP TABLE IF EXISTS `ct_charges`;

CREATE TABLE `ct_charges` (
  `charges_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '费用表id',
  `student_id` int(11) NOT NULL COMMENT '学生id',
  `contest_id` int(11) NOT NULL COMMENT '考试id',
  `bs_isxt` tinyint(4) NOT NULL COMMENT '填写人：1代表系统自动生成，0代表学生录入',
  `c_date` tinyint(4) NOT NULL COMMENT '年份',
  `c_overhaul` int(11) NOT NULL COMMENT '管理费用',
  `c_ad` int(11) NOT NULL COMMENT '广告费用',
  `c_maintenance` int(11) NOT NULL COMMENT '维修费',
  `c_damage` int(11) NOT NULL COMMENT '损失',
  `c_transfer` int(11) NOT NULL COMMENT '转产费',
  `c_rent` int(11) NOT NULL COMMENT '租金',
  `c_develop_market` int(11) NOT NULL COMMENT '市场开拓',
  `c_develop_iso` int(11) NOT NULL COMMENT 'ISO开拓',
  `c_develop_product` int(11) NOT NULL COMMENT '产品研发',
  `c_information` int(11) NOT NULL COMMENT '合计',
  PRIMARY KEY (`charges_id`),
  KEY `(contest_id,student_id)` (`contest_id`,`student_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2165 DEFAULT CHARSET=utf8;

/*Table structure for table `ct_charges_like` */

DROP TABLE IF EXISTS `ct_charges_like`;

CREATE TABLE `ct_charges_like` (
  `charges_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '费用表id',
  `student_id` int(11) NOT NULL COMMENT '学生id',
  `contest_id` int(11) NOT NULL COMMENT '考试id',
  `bs_isxt` tinyint(4) NOT NULL COMMENT '填写人：1代表系统自动生成，0代表学生录入',
  `c_date` tinyint(4) NOT NULL COMMENT '年份',
  `c_overhaul` int(11) NOT NULL COMMENT '管理费用',
  `c_ad` int(11) NOT NULL COMMENT '广告费用',
  `c_maintenance` int(11) NOT NULL COMMENT '维修费',
  `c_damage` int(11) NOT NULL COMMENT '损失',
  `c_transfer` int(11) NOT NULL COMMENT '转产费',
  `c_rent` int(11) NOT NULL COMMENT '租金',
  `c_develop_market` int(11) NOT NULL COMMENT '市场开拓',
  `c_develop_iso` int(11) NOT NULL COMMENT 'ISO开拓',
  `c_develop_product` int(11) NOT NULL COMMENT '产品研发',
  `c_information` int(11) NOT NULL COMMENT '合计',
  PRIMARY KEY (`charges_id`),
  KEY `(contest_id,student_id)` (`contest_id`,`student_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2162 DEFAULT CHARSET=utf8;

/*Table structure for table `ct_dd_material` */

DROP TABLE IF EXISTS `ct_dd_material`;

CREATE TABLE `ct_dd_material` (
  `dd_material_id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '原料采购记录',
  `om_cmid` int(10) NOT NULL COMMENT '原料id',
  `om_num` int(11) DEFAULT NULL COMMENT '数量',
  `student_id` int(11) DEFAULT NULL,
  `contest_id` int(11) DEFAULT NULL,
  `remain_date` tinyint(4) DEFAULT NULL COMMENT '剩余时间',
  `purchase_date` tinyint(4) DEFAULT NULL COMMENT '采购时间',
  PRIMARY KEY (`dd_material_id`),
  KEY `(contest_id,student_id)` (`contest_id`,`student_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5337 DEFAULT CHARSET=utf8;

/*Table structure for table `ct_dd_material_like` */

DROP TABLE IF EXISTS `ct_dd_material_like`;

CREATE TABLE `ct_dd_material_like` (
  `dd_material_id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '原料采购记录',
  `om_cmid` int(10) NOT NULL COMMENT '原料id',
  `om_num` int(11) DEFAULT NULL COMMENT '数量',
  `student_id` int(11) DEFAULT NULL,
  `contest_id` int(11) DEFAULT NULL,
  `remain_date` tinyint(4) DEFAULT NULL COMMENT '剩余时间',
  `purchase_date` tinyint(4) DEFAULT NULL COMMENT '采购时间',
  PRIMARY KEY (`dd_material_id`),
  KEY `(contest_id,student_id)` (`contest_id`,`student_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5337 DEFAULT CHARSET=utf8;

/*Table structure for table `ct_fee` */

DROP TABLE IF EXISTS `ct_fee`;

CREATE TABLE `ct_fee` (
  `fee_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '应收款',
  `student_id` int(11) NOT NULL,
  `contest_id` int(11) NOT NULL,
  `r_fee` int(11) NOT NULL COMMENT '金额',
  `r_remain_date` tinyint(4) NOT NULL COMMENT '账期',
  PRIMARY KEY (`fee_id`),
  KEY `(contest_id,student_id)` (`contest_id`,`student_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2733 DEFAULT CHARSET=utf8;

/*Table structure for table `ct_fee_like` */

DROP TABLE IF EXISTS `ct_fee_like`;

CREATE TABLE `ct_fee_like` (
  `fee_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '应收款',
  `student_id` int(11) NOT NULL,
  `contest_id` int(11) NOT NULL,
  `r_fee` int(11) NOT NULL COMMENT '金额',
  `r_remain_date` tinyint(4) NOT NULL COMMENT '账期',
  PRIMARY KEY (`fee_id`),
  KEY `(contest_id,student_id)` (`contest_id`,`student_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2733 DEFAULT CHARSET=utf8;

/*Table structure for table `ct_gz_ad` */

DROP TABLE IF EXISTS `ct_gz_ad`;

CREATE TABLE `ct_gz_ad` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '初始广告',
  `subject_num` int(11) NOT NULL COMMENT '题号',
  `year` tinyint(4) NOT NULL COMMENT '年份',
  `group_num` varchar(100) DEFAULT NULL COMMENT '组号',
  `local_p1` decimal(15,5) DEFAULT '0.00000' COMMENT '本地P1',
  `regional_p1` decimal(15,5) DEFAULT '0.00000' COMMENT '区域P1',
  `national_p1` decimal(15,5) DEFAULT '0.00000' COMMENT '国内P1',
  `asian_p1` decimal(15,5) DEFAULT '0.00000' COMMENT '亚洲P1',
  `international_p1` decimal(15,5) DEFAULT '0.00000' COMMENT '国际P1',
  `local_p2` decimal(15,5) DEFAULT '0.00000' COMMENT '本地P2',
  `regional_p2` decimal(15,5) DEFAULT '0.00000' COMMENT '区域P2',
  `national_p2` decimal(15,5) DEFAULT '0.00000' COMMENT '国内P2',
  `asian_p2` decimal(15,5) DEFAULT '0.00000' COMMENT '亚洲P2',
  `international_p2` decimal(15,5) DEFAULT '0.00000' COMMENT '国际P2',
  `local_p3` decimal(15,5) DEFAULT '0.00000' COMMENT '本地P3',
  `regional_p3` decimal(15,5) DEFAULT '0.00000' COMMENT '区域P3',
  `national_p3` decimal(15,5) DEFAULT '0.00000' COMMENT '国内P3',
  `asian_p3` decimal(15,5) DEFAULT '0.00000' COMMENT '亚洲P3',
  `international_p3` decimal(15,5) DEFAULT '0.00000' COMMENT '国际P3',
  `local_p4` decimal(15,5) DEFAULT '0.00000' COMMENT '本地P4',
  `regional_p4` decimal(15,5) DEFAULT '0.00000' COMMENT '区域P4',
  `national_p4` decimal(15,5) DEFAULT '0.00000' COMMENT '国内P4',
  `asian_p4` decimal(15,5) DEFAULT '0.00000' COMMENT '亚洲P4',
  `international_p4` decimal(15,5) DEFAULT '0.00000' COMMENT '国际P4',
  `local_p5` decimal(15,5) DEFAULT '0.00000' COMMENT '本地P5',
  `regional_p5` decimal(15,5) DEFAULT '0.00000' COMMENT '区域P5',
  `national_p5` decimal(15,5) DEFAULT '0.00000' COMMENT '国内P5',
  `asian_p5` decimal(15,5) DEFAULT '0.00000' COMMENT '亚洲P5',
  `international_p5` decimal(15,5) DEFAULT '0.00000' COMMENT '国际P5',
  PRIMARY KEY (`id`),
  UNIQUE KEY `subject_num` (`subject_num`,`year`,`group_num`)
) ENGINE=InnoDB AUTO_INCREMENT=3418 DEFAULT CHARSET=utf8;

/*Table structure for table `ct_gz_cs` */

DROP TABLE IF EXISTS `ct_gz_cs`;

CREATE TABLE `ct_gz_cs` (
  `cs_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '规则id',
  `subject_number` int(11) NOT NULL COMMENT 'subject_number',
  `punish` int(11) DEFAULT NULL COMMENT '违约金比例',
  `inventory_discount_product` int(11) DEFAULT NULL COMMENT '出售产成品',
  `inventory_discount_material` int(11) DEFAULT NULL COMMENT '出售原材料',
  `loan_ceiling` int(11) DEFAULT NULL COMMENT '贷款倍数',
  `long_loan_interests` int(11) DEFAULT NULL COMMENT '长贷利息',
  `short_loan_interests` int(11) DEFAULT NULL COMMENT '短贷利息',
  `discount_12` decimal(10,2) DEFAULT NULL COMMENT '1、2账期贴现',
  `discount_34` decimal(10,2) DEFAULT NULL COMMENT '3、4账期贴现',
  `cash` int(11) DEFAULT NULL COMMENT '初始现金',
  `overhaul` int(11) DEFAULT NULL COMMENT '管理费',
  `min` int(11) DEFAULT NULL COMMENT '最小得单金额',
  `information` int(11) DEFAULT NULL COMMENT '信息费',
  `emergence_multiple_material` int(11) DEFAULT NULL COMMENT '紧急采购原材料',
  `emergence_multiple_product` int(11) DEFAULT NULL COMMENT '紧急采购产成品',
  `income_tax` int(11) DEFAULT NULL COMMENT '所得税率',
  `manage_max` int(11) DEFAULT NULL COMMENT '最大经营年限',
  `long_loan_max` int(11) DEFAULT NULL COMMENT '最大长贷年限',
  `order_timeout` int(11) DEFAULT NULL COMMENT '选单时间',
  `spy_period` int(11) DEFAULT NULL COMMENT '间谍时间',
  `spy_interval` int(11) DEFAULT NULL COMMENT '间谍等待',
  `order_fix_time` int(11) DEFAULT NULL COMMENT '首单增加时间',
  `auction_timeout` int(11) DEFAULT NULL COMMENT '拍卖时间',
  `auction_thread` int(11) DEFAULT NULL COMMENT '拍卖张数',
  `max_workshop` int(11) DEFAULT NULL COMMENT '厂房最大数',
  PRIMARY KEY (`cs_id`),
  UNIQUE KEY `subject_number` (`subject_number`)
) ENGINE=InnoDB AUTO_INCREMENT=69 DEFAULT CHARSET=utf8;

/*Table structure for table `ct_gz_iso` */

DROP TABLE IF EXISTS `ct_gz_iso`;

CREATE TABLE `ct_gz_iso` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ci_id` int(11) NOT NULL COMMENT 'ISO规则表 ISO类型',
  `subject_number` int(11) NOT NULL COMMENT '题库号',
  `ci_name` varchar(255) DEFAULT NULL COMMENT 'ISO名称',
  `ci_develop_fee` int(11) NOT NULL COMMENT '研发费用',
  `ci_develop_date` int(11) NOT NULL COMMENT '研发周期',
  PRIMARY KEY (`id`),
  UNIQUE KEY `ci_id` (`ci_id`,`subject_number`)
) ENGINE=InnoDB AUTO_INCREMENT=137 DEFAULT CHARSET=utf8;

/*Table structure for table `ct_gz_market` */

DROP TABLE IF EXISTS `ct_gz_market`;

CREATE TABLE `ct_gz_market` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `cm_id` int(10) NOT NULL COMMENT '市场规则表编号',
  `subject_number` int(11) DEFAULT NULL COMMENT '题库号',
  `cm_name` varchar(255) NOT NULL COMMENT '市场名称',
  `cm_develop_fee` int(11) NOT NULL COMMENT '研发费用',
  `cm_develop_date` int(11) NOT NULL COMMENT '研发周期',
  PRIMARY KEY (`id`),
  UNIQUE KEY `cm_id` (`cm_id`,`subject_number`)
) ENGINE=InnoDB AUTO_INCREMENT=341 DEFAULT CHARSET=utf8;

/*Table structure for table `ct_gz_material` */

DROP TABLE IF EXISTS `ct_gz_material`;

CREATE TABLE `ct_gz_material` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `cm_id` int(10) NOT NULL COMMENT '原料规则表编号',
  `subject_number` int(11) DEFAULT NULL COMMENT '题库号',
  `cn_name` varchar(255) DEFAULT NULL COMMENT '原料名称',
  `cm_lead_date` int(10) unsigned DEFAULT NULL COMMENT '采购周期',
  `cm_buy_fee` int(10) unsigned DEFAULT NULL COMMENT '费用',
  PRIMARY KEY (`id`),
  UNIQUE KEY `cm_id` (`cm_id`,`subject_number`)
) ENGINE=InnoDB AUTO_INCREMENT=273 DEFAULT CHARSET=utf8;

/*Table structure for table `ct_gz_order` */

DROP TABLE IF EXISTS `ct_gz_order`;

CREATE TABLE `ct_gz_order` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `co_id` varchar(50) NOT NULL COMMENT '市场订单 订单编号',
  `subject_number` int(11) DEFAULT NULL COMMENT '题库号',
  `date` tinyint(4) NOT NULL COMMENT '年份',
  `cm_id` int(11) DEFAULT NULL COMMENT '市场',
  `cp_id` varchar(50) DEFAULT NULL COMMENT '产品',
  `num` int(11) DEFAULT NULL COMMENT '数量',
  `total_price` int(11) DEFAULT NULL COMMENT '总价',
  `delivery_date` tinyint(4) DEFAULT NULL COMMENT '交货期',
  `payment_date` tinyint(4) DEFAULT NULL COMMENT '账期',
  `ci_id` int(11) DEFAULT NULL COMMENT 'ISO',
  `xz_round` int(11) DEFAULT NULL COMMENT '选走轮次',
  `xz_group` varchar(50) DEFAULT NULL COMMENT '选走组号',
  PRIMARY KEY (`id`),
  UNIQUE KEY `co_id` (`co_id`,`subject_number`,`date`)
) ENGINE=InnoDB AUTO_INCREMENT=36235 DEFAULT CHARSET=utf8;

/*Table structure for table `ct_gz_producing` */

DROP TABLE IF EXISTS `ct_gz_producing`;

CREATE TABLE `ct_gz_producing` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `cp_id` int(10) NOT NULL COMMENT '生产规则表id',
  `subject_number` int(11) NOT NULL COMMENT '题库号',
  `cp_name` varchar(255) DEFAULT NULL COMMENT '产品名称',
  `cp_mid` int(11) NOT NULL COMMENT '原材料规则id',
  `cp_num` int(11) NOT NULL COMMENT '所需数量',
  PRIMARY KEY (`id`),
  UNIQUE KEY `cp_id` (`cp_id`,`subject_number`,`cp_mid`)
) ENGINE=InnoDB AUTO_INCREMENT=592 DEFAULT CHARSET=utf8;

/*Table structure for table `ct_gz_product` */

DROP TABLE IF EXISTS `ct_gz_product`;

CREATE TABLE `ct_gz_product` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `cp_id` int(11) NOT NULL COMMENT '产品规则表编号',
  `subject_number` int(11) DEFAULT NULL COMMENT '题库号',
  `cp_name` varchar(255) DEFAULT NULL COMMENT '名称',
  `cp_processing_fee` int(11) NOT NULL COMMENT '开发费用',
  `cp_develop_date` int(11) NOT NULL COMMENT '开发周期',
  `cp_develop_fee` int(11) NOT NULL COMMENT '加工费',
  `cp_direct_cost` int(11) NOT NULL COMMENT '直接成本',
  PRIMARY KEY (`id`),
  UNIQUE KEY `cp_id` (`cp_id`,`subject_number`)
) ENGINE=InnoDB AUTO_INCREMENT=273 DEFAULT CHARSET=utf8;

/*Table structure for table `ct_gz_product_line` */

DROP TABLE IF EXISTS `ct_gz_product_line`;

CREATE TABLE `ct_gz_product_line` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `cpl_id` int(10) NOT NULL COMMENT '生产线编号',
  `subject_num` int(11) NOT NULL COMMENT '题库号',
  `cpl_name` varchar(32) NOT NULL COMMENT '生产线名称',
  `cpl_buy_fee` int(11) NOT NULL COMMENT '每次投资费用',
  `cpl_install_date` tinyint(4) NOT NULL COMMENT '投资周期',
  `cpl_produce_date` tinyint(4) NOT NULL COMMENT '生产时间',
  `cpl_transfer_date` tinyint(4) NOT NULL COMMENT '转产周期',
  `cpl_transfer_fee` int(11) NOT NULL COMMENT '转产费用',
  `cpl_maintenance_fee` int(11) NOT NULL COMMENT '维修费（W/每年）',
  `cpl_scarp_fee` int(11) NOT NULL COMMENT '折旧费',
  `cpl_dep_date` tinyint(4) NOT NULL COMMENT '折旧年限',
  `cpl_dep_fee` int(11) NOT NULL COMMENT '残值',
  PRIMARY KEY (`id`),
  UNIQUE KEY `cpl_id` (`cpl_id`,`subject_num`)
) ENGINE=InnoDB AUTO_INCREMENT=263 DEFAULT CHARSET=utf8;

/*Table structure for table `ct_gz_workshop` */

DROP TABLE IF EXISTS `ct_gz_workshop`;

CREATE TABLE `ct_gz_workshop` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `cw_id` int(11) NOT NULL COMMENT '编号',
  `subject_num` int(11) NOT NULL COMMENT '题库号',
  `cw_name` varchar(32) NOT NULL COMMENT '厂房名称',
  `cw_buy_fee` int(11) NOT NULL COMMENT '购买费用',
  `cw_rent_fee` int(11) NOT NULL COMMENT '租金',
  `cw_sell_fee` int(11) NOT NULL COMMENT '售价',
  `cw_capacity` int(11) NOT NULL COMMENT '生产线容量',
  PRIMARY KEY (`id`),
  UNIQUE KEY `cw_id` (`cw_id`,`subject_num`)
) ENGINE=InnoDB AUTO_INCREMENT=205 DEFAULT CHARSET=utf8;

/*Table structure for table `ct_kc_material` */

DROP TABLE IF EXISTS `ct_kc_material`;

CREATE TABLE `ct_kc_material` (
  `material_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `student_id` int(11) DEFAULT NULL,
  `contest_id` int(11) DEFAULT NULL,
  `im_cm_id` int(11) DEFAULT NULL COMMENT '原料id',
  `im_num` int(11) DEFAULT NULL COMMENT '剩余数量',
  PRIMARY KEY (`material_id`),
  KEY `(contest_id,student_id)` (`contest_id`,`student_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2669 DEFAULT CHARSET=utf8;

/*Table structure for table `ct_kc_material_like` */

DROP TABLE IF EXISTS `ct_kc_material_like`;

CREATE TABLE `ct_kc_material_like` (
  `material_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `student_id` int(11) DEFAULT NULL,
  `contest_id` int(11) DEFAULT NULL,
  `im_cm_id` int(11) DEFAULT NULL COMMENT '原料id',
  `im_num` int(11) DEFAULT NULL COMMENT '剩余数量',
  PRIMARY KEY (`material_id`),
  KEY `(contest_id,student_id)` (`contest_id`,`student_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2669 DEFAULT CHARSET=utf8;

/*Table structure for table `ct_kc_product` */

DROP TABLE IF EXISTS `ct_kc_product`;

CREATE TABLE `ct_kc_product` (
  `kc_product_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '产成品库存',
  `student_id` int(11) DEFAULT NULL,
  `contest_id` int(11) DEFAULT NULL,
  `ip_cp_id` int(11) DEFAULT NULL COMMENT '产品id',
  `ip_num` int(11) DEFAULT NULL COMMENT '库存数量',
  PRIMARY KEY (`kc_product_id`),
  KEY `(contest_id,student_id)` (`contest_id`,`student_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2669 DEFAULT CHARSET=utf8;

/*Table structure for table `ct_kc_product_like` */

DROP TABLE IF EXISTS `ct_kc_product_like`;

CREATE TABLE `ct_kc_product_like` (
  `kc_product_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `student_id` int(11) DEFAULT NULL,
  `contest_id` int(11) DEFAULT NULL,
  `ip_cp_id` int(11) DEFAULT NULL COMMENT '产品id',
  `ip_num` int(11) DEFAULT NULL COMMENT '库存数量',
  PRIMARY KEY (`kc_product_id`),
  KEY `(contest_id,student_id)` (`contest_id`,`student_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2669 DEFAULT CHARSET=utf8;

/*Table structure for table `ct_line` */

DROP TABLE IF EXISTS `ct_line`;

CREATE TABLE `ct_line` (
  `line_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `student_id` int(11) NOT NULL,
  `contest_id` int(11) NOT NULL,
  `workshop_id` int(11) NOT NULL COMMENT '所属厂房id（ct_workshop）',
  `pl_wid` int(11) NOT NULL COMMENT '所属厂房类型id(ct_gz_workshop)',
  `pl_cplid` int(11) NOT NULL COMMENT '生产线类型id',
  `pl_cpid` int(11) NOT NULL COMMENT '生产产品id',
  `pl_invest` int(11) NOT NULL COMMENT '原值',
  `pl_dep_total` int(11) NOT NULL COMMENT '累计折旧',
  `pl_product_add_date` tinyint(4) DEFAULT '0' COMMENT '开产时间',
  `pl_producting_date` tinyint(4) DEFAULT '0' COMMENT '剩余生产时间',
  `pl_finish_date` tinyint(4) DEFAULT '0' COMMENT '建成时间',
  `pl_remain_date` tinyint(4) DEFAULT '0' COMMENT '在建剩余时间',
  `pl_add_time` tinyint(4) DEFAULT '0' COMMENT '开建时间',
  `pl_transfer_add_date` tinyint(4) DEFAULT '0' COMMENT '转产时间',
  `pl_transfer_date` tinyint(4) DEFAULT '0' COMMENT '转产剩余时间',
  PRIMARY KEY (`line_id`),
  KEY `(contest_id,student_id)` (`contest_id`,`student_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4078 DEFAULT CHARSET=utf8;

/*Table structure for table `ct_line_like` */

DROP TABLE IF EXISTS `ct_line_like`;

CREATE TABLE `ct_line_like` (
  `line_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `student_id` int(11) NOT NULL,
  `contest_id` int(11) NOT NULL,
  `workshop_id` int(11) NOT NULL COMMENT '所属厂房id（ct_workshop）',
  `pl_wid` int(11) NOT NULL COMMENT '所属厂房类型id(ct_gz_workshop)',
  `pl_cplid` int(11) NOT NULL COMMENT '生产线类型id',
  `pl_cpid` int(11) NOT NULL COMMENT '生产产品id',
  `pl_invest` int(11) NOT NULL COMMENT '原值',
  `pl_dep_total` int(11) NOT NULL COMMENT '累计折旧',
  `pl_product_add_date` tinyint(4) DEFAULT '0' COMMENT '开产时间',
  `pl_producting_date` tinyint(4) DEFAULT '0' COMMENT '剩余生产时间',
  `pl_finish_date` tinyint(4) DEFAULT '0' COMMENT '建成时间',
  `pl_remain_date` tinyint(4) DEFAULT '0' COMMENT '在建剩余时间',
  `pl_add_time` tinyint(4) DEFAULT '0' COMMENT '开建时间',
  `pl_transfer_add_date` tinyint(4) DEFAULT '0' COMMENT '转产时间',
  `pl_transfer_date` tinyint(4) DEFAULT '0' COMMENT '转产剩余时间',
  PRIMARY KEY (`line_id`),
  KEY `(contest_id,student_id)` (`contest_id`,`student_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4072 DEFAULT CHARSET=utf8;

/*Table structure for table `ct_mn_ad` */

DROP TABLE IF EXISTS `ct_mn_ad`;

CREATE TABLE `ct_mn_ad` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '模拟广告',
  `student_id` int(11) DEFAULT NULL COMMENT '学生id',
  `contest_id` int(11) DEFAULT NULL COMMENT '考试id',
  `year` tinyint(4) NOT NULL COMMENT '年份',
  `group_num` varchar(100) DEFAULT NULL COMMENT '组号',
  `local_p1` decimal(15,5) DEFAULT NULL COMMENT '本地P1',
  `regional_p1` decimal(15,5) DEFAULT NULL COMMENT '区域P1',
  `national_p1` decimal(15,5) DEFAULT NULL COMMENT '国内P1',
  `asian_p1` decimal(15,5) DEFAULT NULL COMMENT '亚洲P1',
  `international_p1` decimal(15,5) DEFAULT NULL COMMENT '国际P1',
  `local_p2` decimal(15,5) DEFAULT NULL COMMENT '本地P2',
  `regional_p2` decimal(15,5) DEFAULT NULL COMMENT '区域P2',
  `national_p2` decimal(15,5) DEFAULT NULL COMMENT '国内P2',
  `asian_p2` decimal(15,5) DEFAULT NULL COMMENT '亚洲P2',
  `international_p2` decimal(15,5) DEFAULT NULL COMMENT '国际P2',
  `local_p3` decimal(15,5) DEFAULT NULL COMMENT '本地P3',
  `regional_p3` decimal(15,5) DEFAULT NULL COMMENT '区域P3',
  `national_p3` decimal(15,5) DEFAULT NULL COMMENT '国内P3',
  `asian_p3` decimal(15,5) DEFAULT NULL COMMENT '亚洲P3',
  `international_p3` decimal(15,5) DEFAULT NULL COMMENT '国际P3',
  `local_p4` decimal(15,5) DEFAULT NULL COMMENT '本地P4',
  `regional_p4` decimal(15,5) DEFAULT NULL COMMENT '区域P4',
  `national_p4` decimal(15,5) DEFAULT NULL COMMENT '国内P4',
  `asian_p4` decimal(15,5) DEFAULT NULL COMMENT '亚洲P4',
  `international_p4` decimal(15,5) DEFAULT NULL COMMENT '国际P4',
  `local_p5` decimal(15,5) DEFAULT NULL COMMENT '本地P5',
  `regional_p5` decimal(15,5) DEFAULT NULL COMMENT '区域P5',
  `national_p5` decimal(15,5) DEFAULT NULL COMMENT '国内P5',
  `asian_p5` decimal(15,5) DEFAULT NULL COMMENT '亚洲P5',
  `international_p5` decimal(15,5) DEFAULT NULL COMMENT '国际P5',
  PRIMARY KEY (`id`),
  KEY `(contest_id,student_id)` (`contest_id`,`student_id`)
) ENGINE=InnoDB AUTO_INCREMENT=38075 DEFAULT CHARSET=utf8;

/*Table structure for table `ct_mn_ad_like` */

DROP TABLE IF EXISTS `ct_mn_ad_like`;

CREATE TABLE `ct_mn_ad_like` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '模拟广告',
  `student_id` int(11) DEFAULT NULL COMMENT '学生id',
  `contest_id` int(11) DEFAULT NULL COMMENT '考试id',
  `year` tinyint(4) NOT NULL COMMENT '年份',
  `group_num` varchar(255) DEFAULT NULL COMMENT '组号',
  `local_p1` decimal(15,5) DEFAULT NULL COMMENT '本地P1',
  `regional_p1` decimal(15,5) DEFAULT NULL COMMENT '区域P1',
  `national_p1` decimal(15,5) DEFAULT NULL COMMENT '国内P1',
  `asian_p1` decimal(15,5) DEFAULT NULL COMMENT '亚洲P1',
  `international_p1` decimal(15,5) DEFAULT NULL COMMENT '国际P1',
  `local_p2` decimal(15,5) DEFAULT NULL COMMENT '本地P2',
  `regional_p2` decimal(15,5) DEFAULT NULL COMMENT '区域P2',
  `national_p2` decimal(15,5) DEFAULT NULL COMMENT '国内P2',
  `asian_p2` decimal(15,5) DEFAULT NULL COMMENT '亚洲P2',
  `international_p2` decimal(15,5) DEFAULT NULL COMMENT '国际P2',
  `local_p3` decimal(15,5) DEFAULT NULL COMMENT '本地P3',
  `regional_p3` decimal(15,5) DEFAULT NULL COMMENT '区域P3',
  `national_p3` decimal(15,5) DEFAULT NULL COMMENT '国内P3',
  `asian_p3` decimal(15,5) DEFAULT NULL COMMENT '亚洲P3',
  `international_p3` decimal(15,5) DEFAULT NULL COMMENT '国际P3',
  `local_p4` decimal(15,5) DEFAULT NULL COMMENT '本地P4',
  `regional_p4` decimal(15,5) DEFAULT NULL COMMENT '区域P4',
  `national_p4` decimal(15,5) DEFAULT NULL COMMENT '国内P4',
  `asian_p4` decimal(15,5) DEFAULT NULL COMMENT '亚洲P4',
  `international_p4` decimal(15,5) DEFAULT NULL COMMENT '国际P4',
  `local_p5` decimal(15,5) DEFAULT NULL COMMENT '本地P5',
  `regional_p5` decimal(15,5) DEFAULT NULL COMMENT '区域P5',
  `national_p5` decimal(15,5) DEFAULT NULL COMMENT '国内P5',
  `asian_p5` decimal(15,5) DEFAULT NULL COMMENT '亚洲P5',
  `international_p5` decimal(15,5) DEFAULT NULL COMMENT '国际P5',
  PRIMARY KEY (`id`),
  KEY `(contest_id,student_id)` (`contest_id`,`student_id`)
) ENGINE=InnoDB AUTO_INCREMENT=38074 DEFAULT CHARSET=utf8;

/*Table structure for table `ct_mn_choose` */

DROP TABLE IF EXISTS `ct_mn_choose`;

CREATE TABLE `ct_mn_choose` (
  `choose_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `student_id` int(11) DEFAULT NULL,
  `contest_id` int(11) DEFAULT NULL,
  `co_id` varchar(50) DEFAULT NULL COMMENT '定单编号',
  `date` int(11) DEFAULT NULL COMMENT '年份',
  `cm_id` int(11) DEFAULT NULL COMMENT '市场',
  `cp_id` int(11) DEFAULT NULL COMMENT '产品编号',
  `num` int(11) DEFAULT NULL COMMENT '数量',
  `total_price` int(11) DEFAULT NULL COMMENT '总价',
  `delivery_date` int(11) DEFAULT NULL COMMENT '交货期',
  `payment_day` int(11) DEFAULT NULL COMMENT '账期',
  `ci_id` int(11) DEFAULT NULL COMMENT 'ISO编号，为空代表不需要任何iso认证，为3代表需要全部iso认证，为1或2代表需要1或2认证',
  `xz_round` int(11) DEFAULT NULL COMMENT '选走轮次',
  `xz_group` varchar(50) DEFAULT NULL COMMENT '选走组号',
  PRIMARY KEY (`choose_id`),
  KEY `(contest_id,student_id)` (`contest_id`,`student_id`)
) ENGINE=InnoDB AUTO_INCREMENT=393999 DEFAULT CHARSET=utf8;

/*Table structure for table `ct_mn_choose_like` */

DROP TABLE IF EXISTS `ct_mn_choose_like`;

CREATE TABLE `ct_mn_choose_like` (
  `choose_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `student_id` int(11) DEFAULT NULL,
  `contest_id` int(11) DEFAULT NULL,
  `co_id` varchar(50) DEFAULT NULL COMMENT '定单编号',
  `date` int(11) DEFAULT NULL COMMENT '年份',
  `cm_id` int(11) DEFAULT NULL COMMENT '市场',
  `cp_id` int(11) DEFAULT NULL COMMENT '产品编号',
  `num` int(11) DEFAULT NULL COMMENT '数量',
  `total_price` int(11) DEFAULT NULL COMMENT '总价',
  `delivery_date` int(11) DEFAULT NULL COMMENT '交货期',
  `payment_day` int(11) DEFAULT NULL COMMENT '账期',
  `ci_id` int(11) DEFAULT NULL COMMENT 'ISO',
  `xz_round` int(11) DEFAULT NULL COMMENT '选走轮次',
  `xz_group` varchar(50) DEFAULT NULL COMMENT '选走组号',
  PRIMARY KEY (`choose_id`),
  KEY `(contest_id,student_id)` (`contest_id`,`student_id`)
) ENGINE=InnoDB AUTO_INCREMENT=393999 DEFAULT CHARSET=utf8;

/*Table structure for table `ct_profit_chart` */

DROP TABLE IF EXISTS `ct_profit_chart`;

CREATE TABLE `ct_profit_chart` (
  `profit_chart_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '利润表id',
  `student_id` int(11) NOT NULL COMMENT '学生id',
  `contest_id` int(11) NOT NULL COMMENT '考试id',
  `bs_isxt` tinyint(4) NOT NULL COMMENT '填写人：1代表系统自动生成，0代表学生录入',
  `pc_date` tinyint(4) NOT NULL COMMENT '年份',
  `pc_sales` int(11) NOT NULL COMMENT '销售额',
  `pc_direct_cost` int(11) NOT NULL COMMENT '成本',
  `pc_goods_profit` int(11) NOT NULL COMMENT '毛利',
  `pc_total` int(11) NOT NULL COMMENT '管理费用',
  `pc_profit_before_dep` int(11) NOT NULL COMMENT '折旧前利润',
  `pc_dep` int(11) NOT NULL COMMENT '折旧',
  `pc_profit_before_interests` int(11) NOT NULL COMMENT '财务费用前利润',
  `pc_finance_fee` int(11) NOT NULL COMMENT '财务费用',
  `pc_profit_before_tax` int(11) NOT NULL COMMENT '税前利润',
  `pc_tax` int(11) NOT NULL COMMENT '所得税',
  `pc_annual_net_profit` int(11) NOT NULL COMMENT '净利润',
  PRIMARY KEY (`profit_chart_id`),
  KEY `(contest_id,student_id)` (`contest_id`,`student_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2150 DEFAULT CHARSET=utf8;

/*Table structure for table `ct_profit_chart_like` */

DROP TABLE IF EXISTS `ct_profit_chart_like`;

CREATE TABLE `ct_profit_chart_like` (
  `profit_chart_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '利润表id',
  `student_id` int(11) NOT NULL COMMENT '学生id',
  `contest_id` int(11) NOT NULL COMMENT '考试id',
  `bs_isxt` tinyint(4) NOT NULL COMMENT '填写人：1代表系统自动生成，0代表学生录入',
  `pc_date` tinyint(4) NOT NULL COMMENT '年份',
  `pc_sales` int(11) NOT NULL COMMENT '销售额',
  `pc_direct_cost` int(11) NOT NULL COMMENT '成本',
  `pc_goods_profit` int(11) NOT NULL COMMENT '毛利',
  `pc_total` int(11) NOT NULL COMMENT '管理费用',
  `pc_profit_before_dep` int(11) NOT NULL COMMENT '折旧前利润',
  `pc_dep` int(11) NOT NULL COMMENT '折旧',
  `pc_profit_before_interests` int(11) NOT NULL COMMENT '财务费用前利润',
  `pc_finance_fee` int(11) NOT NULL COMMENT '财务费用',
  `pc_profit_before_tax` int(11) NOT NULL COMMENT '税前利润',
  `pc_tax` int(11) NOT NULL COMMENT '所得税',
  `pc_annual_net_profit` int(11) NOT NULL COMMENT '净利润',
  PRIMARY KEY (`profit_chart_id`),
  KEY `(contest_id,student_id)` (`contest_id`,`student_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2147 DEFAULT CHARSET=utf8;

/*Table structure for table `ct_subject` */

DROP TABLE IF EXISTS `ct_subject`;

CREATE TABLE `ct_subject` (
  `subject_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '题库id',
  `subject_number` int(10) NOT NULL COMMENT '题库号',
  `subject_name` varchar(100) DEFAULT NULL COMMENT '题库名称',
  `group_id` int(11) NOT NULL COMMENT '题库所属分组',
  `upload` tinyint(4) NOT NULL DEFAULT '0' COMMENT '是否已导入题库的规则表',
  `spy_attachment` varchar(255) DEFAULT NULL COMMENT '间谍附件，第1年到第4年用|隔开',
  `rule_attachment` varchar(255) DEFAULT NULL COMMENT '规则说明附件',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  PRIMARY KEY (`subject_id`),
  UNIQUE KEY `subject_number` (`subject_number`),
  KEY `create_time` (`create_time`),
  KEY `group_id` (`group_id`)
) ENGINE=InnoDB AUTO_INCREMENT=50 DEFAULT CHARSET=utf8;

/*Table structure for table `ct_t_time` */

DROP TABLE IF EXISTS `ct_t_time`;

CREATE TABLE `ct_t_time` (
  `time_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `student_id` int(11) NOT NULL COMMENT '学生id',
  `contest_id` int(11) NOT NULL COMMENT '竞赛id',
  `date` tinyint(4) NOT NULL COMMENT '考试时间',
  PRIMARY KEY (`time_id`),
  KEY `(contest_id,student_id)` (`contest_id`,`student_id`)
) ENGINE=InnoDB AUTO_INCREMENT=684 DEFAULT CHARSET=utf8;

/*Table structure for table `ct_t_time_like` */

DROP TABLE IF EXISTS `ct_t_time_like`;

CREATE TABLE `ct_t_time_like` (
  `time_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `student_id` int(11) NOT NULL COMMENT '学生id',
  `contest_id` int(11) NOT NULL COMMENT '竞赛id',
  `date` tinyint(4) NOT NULL COMMENT '考试时间',
  PRIMARY KEY (`time_id`),
  KEY `(contest_id,student_id)` (`contest_id`,`student_id`)
) ENGINE=InnoDB AUTO_INCREMENT=684 DEFAULT CHARSET=utf8;

/*Table structure for table `ct_workshop` */

DROP TABLE IF EXISTS `ct_workshop`;

CREATE TABLE `ct_workshop` (
  `workshop_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `student_id` int(11) NOT NULL,
  `contest_id` int(11) NOT NULL,
  `w_cwid` int(11) NOT NULL COMMENT '厂房规则表里的id',
  `w_status` tinyint(4) NOT NULL COMMENT '租售类型:0表示BUY,1表示RENT',
  `w_surplus_capacity` int(11) DEFAULT NULL COMMENT '剩余容量',
  `w_pay_date` int(11) DEFAULT NULL COMMENT '最后付租',
  `get_date` tinyint(4) DEFAULT NULL COMMENT '置办时间',
  PRIMARY KEY (`workshop_id`),
  KEY `(contest_id,student_id)` (`contest_id`,`student_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1470 DEFAULT CHARSET=utf8;

/*Table structure for table `ct_workshop_like` */

DROP TABLE IF EXISTS `ct_workshop_like`;

CREATE TABLE `ct_workshop_like` (
  `workshop_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `student_id` int(11) NOT NULL,
  `contest_id` int(11) NOT NULL,
  `w_cwid` int(11) DEFAULT NULL,
  `w_status` tinyint(4) NOT NULL COMMENT '租售类型:0表示BUY,1表示RENT',
  `w_surplus_capacity` int(11) DEFAULT NULL COMMENT '剩余容量',
  `w_pay_date` int(11) DEFAULT NULL COMMENT '最后付租',
  `get_date` tinyint(4) DEFAULT NULL COMMENT '置办时间',
  PRIMARY KEY (`workshop_id`),
  KEY `(contest_id,student_id)` (`contest_id`,`student_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1467 DEFAULT CHARSET=utf8;

/*Table structure for table `ct_xz_order` */

DROP TABLE IF EXISTS `ct_xz_order`;

CREATE TABLE `ct_xz_order` (
  `order_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `co_id` varchar(50) NOT NULL COMMENT '订单编号',
  `student_id` int(11) NOT NULL,
  `contest_id` int(11) NOT NULL,
  `date` tinyint(4) DEFAULT NULL COMMENT '年份',
  `cm_id` int(11) DEFAULT NULL COMMENT '市场',
  `cp_id` int(11) DEFAULT NULL COMMENT '产品',
  `num` int(11) DEFAULT NULL COMMENT '数量',
  `total_price` int(11) DEFAULT NULL COMMENT '总价',
  `delivery_date` tinyint(4) DEFAULT NULL COMMENT '交货期',
  `payment_date` tinyint(4) DEFAULT NULL COMMENT '账期',
  `iso_id` int(11) DEFAULT NULL COMMENT 'ISO',
  `commit_date` tinyint(4) DEFAULT NULL COMMENT '交单时间',
  PRIMARY KEY (`order_id`),
  KEY `(contest_id,student_id)` (`contest_id`,`student_id`)
) ENGINE=InnoDB AUTO_INCREMENT=8531 DEFAULT CHARSET=utf8;

/*Table structure for table `ct_xz_order_like` */

DROP TABLE IF EXISTS `ct_xz_order_like`;

CREATE TABLE `ct_xz_order_like` (
  `order_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `co_id` varchar(50) NOT NULL COMMENT '订单id',
  `student_id` int(11) NOT NULL,
  `contest_id` int(11) NOT NULL,
  `date` tinyint(4) DEFAULT NULL COMMENT '年份',
  `cm_id` int(11) DEFAULT NULL COMMENT '市场',
  `cp_id` int(11) DEFAULT NULL COMMENT '产品',
  `num` int(11) DEFAULT NULL COMMENT '数量',
  `total_price` int(11) DEFAULT NULL COMMENT '总价',
  `delivery_date` tinyint(4) DEFAULT NULL COMMENT '交货期',
  `payment_date` tinyint(4) DEFAULT NULL COMMENT '账期',
  `iso_id` int(11) DEFAULT NULL COMMENT 'ISO',
  `commit_date` tinyint(4) DEFAULT NULL COMMENT '交单时间',
  PRIMARY KEY (`order_id`),
  KEY `(contest_id,student_id)` (`contest_id`,`student_id`)
) ENGINE=InnoDB AUTO_INCREMENT=8506 DEFAULT CHARSET=utf8;

/*Table structure for table `ct_yf_iso` */

DROP TABLE IF EXISTS `ct_yf_iso`;

CREATE TABLE `ct_yf_iso` (
  `iso_id` int(11) NOT NULL AUTO_INCREMENT,
  `student_id` int(11) NOT NULL,
  `contest_id` int(11) NOT NULL,
  `di_ciid` int(11) DEFAULT NULL COMMENT 'ISO规则_id',
  `di_total_date` tinyint(4) DEFAULT NULL COMMENT '总研发时间',
  `di_now_date` tinyint(4) DEFAULT NULL COMMENT '已研发时间',
  `di_remain_date` tinyint(4) DEFAULT NULL COMMENT '剩余研发时间',
  `di_finish_date` tinyint(4) DEFAULT NULL COMMENT '研发完成时间',
  PRIMARY KEY (`iso_id`),
  KEY `(contest_id,student_id)` (`contest_id`,`student_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1335 DEFAULT CHARSET=utf8;

/*Table structure for table `ct_yf_iso_like` */

DROP TABLE IF EXISTS `ct_yf_iso_like`;

CREATE TABLE `ct_yf_iso_like` (
  `iso_id` int(11) NOT NULL AUTO_INCREMENT,
  `student_id` int(11) NOT NULL,
  `contest_id` int(11) NOT NULL,
  `di_ciid` int(11) DEFAULT NULL COMMENT 'ISO规则_id',
  `di_total_date` tinyint(4) DEFAULT NULL COMMENT '总研发时间',
  `di_now_date` tinyint(4) DEFAULT NULL COMMENT '已研发时间',
  `di_remain_date` tinyint(4) DEFAULT NULL COMMENT '剩余研发时间',
  `di_finish_date` tinyint(4) DEFAULT NULL COMMENT '研发完成时间',
  PRIMARY KEY (`iso_id`),
  KEY `(contest_id,student_id)` (`contest_id`,`student_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1335 DEFAULT CHARSET=utf8;

/*Table structure for table `ct_yf_market` */

DROP TABLE IF EXISTS `ct_yf_market`;

CREATE TABLE `ct_yf_market` (
  `market_id` int(11) NOT NULL AUTO_INCREMENT,
  `student_id` int(11) DEFAULT NULL,
  `contest_id` int(11) DEFAULT NULL,
  `dm_cm_id` int(11) DEFAULT NULL COMMENT '市场规则id',
  `dm_total_date` tinyint(4) DEFAULT NULL COMMENT '总研发时间',
  `dm_now_date` tinyint(4) DEFAULT NULL COMMENT '已研发时间',
  `dm_remain_date` tinyint(4) DEFAULT NULL COMMENT '剩余研发时间',
  `dm_finish_date` tinyint(4) DEFAULT NULL COMMENT '研发完成时间',
  PRIMARY KEY (`market_id`),
  KEY `(contest_id,student_id)` (`contest_id`,`student_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3336 DEFAULT CHARSET=utf8;

/*Table structure for table `ct_yf_market_like` */

DROP TABLE IF EXISTS `ct_yf_market_like`;

CREATE TABLE `ct_yf_market_like` (
  `market_id` int(11) NOT NULL AUTO_INCREMENT,
  `student_id` int(11) DEFAULT NULL,
  `contest_id` int(11) DEFAULT NULL,
  `dm_cm_id` int(11) DEFAULT NULL COMMENT '市场规则id',
  `dm_total_date` tinyint(4) DEFAULT NULL COMMENT '总研发时间',
  `dm_now_date` tinyint(4) DEFAULT NULL COMMENT '已研发时间',
  `dm_remain_date` tinyint(4) DEFAULT NULL COMMENT '剩余研发时间',
  `dm_finish_date` tinyint(4) DEFAULT NULL COMMENT '研发完成时间',
  PRIMARY KEY (`market_id`),
  KEY `(contest_id,student_id)` (`contest_id`,`student_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3336 DEFAULT CHARSET=utf8;

/*Table structure for table `ct_yf_product` */

DROP TABLE IF EXISTS `ct_yf_product`;

CREATE TABLE `ct_yf_product` (
  `product_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `student_id` int(11) DEFAULT NULL,
  `contest_id` int(11) DEFAULT NULL,
  `dp_cp_id` int(11) DEFAULT NULL COMMENT '产品规则id',
  `dp_total_date` tinyint(4) DEFAULT NULL COMMENT '总研发时间',
  `dp_now_date` tinyint(4) DEFAULT NULL COMMENT '已研发时间',
  `dp_remain_date` tinyint(4) DEFAULT NULL COMMENT '剩余研发时间',
  `dp_finish_date` tinyint(4) DEFAULT NULL COMMENT '研发完成时间',
  PRIMARY KEY (`product_id`),
  KEY `(contest_id,student_id)` (`contest_id`,`student_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2669 DEFAULT CHARSET=utf8;

/*Table structure for table `ct_yf_product_like` */

DROP TABLE IF EXISTS `ct_yf_product_like`;

CREATE TABLE `ct_yf_product_like` (
  `product_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `student_id` int(11) DEFAULT NULL,
  `contest_id` int(11) DEFAULT NULL,
  `dp_cp_id` int(11) DEFAULT NULL COMMENT '产品规则id',
  `dp_total_date` tinyint(4) DEFAULT NULL COMMENT '总研发时间',
  `dp_now_date` tinyint(4) DEFAULT NULL COMMENT '已研发时间',
  `dp_remain_date` tinyint(4) DEFAULT NULL COMMENT '剩余研发时间',
  `dp_finish_date` tinyint(4) DEFAULT NULL COMMENT '研发完成时间',
  PRIMARY KEY (`product_id`),
  KEY `(contest_id,student_id)` (`contest_id`,`student_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2669 DEFAULT CHARSET=utf8;

/*Table structure for table `exam` */

DROP TABLE IF EXISTS `exam`;

CREATE TABLE `exam` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '学生的考试的id',
  `paper_id` int(11) NOT NULL COMMENT '试卷id',
  `student_id` int(11) NOT NULL COMMENT '考生id',
  `teacher_id` int(11) NOT NULL COMMENT '教师id',
  `exam_time` mediumint(9) NOT NULL COMMENT '考试时间（分钟）',
  `is_hand_in` tinyint(4) NOT NULL DEFAULT '0' COMMENT '是否已交卷,0表示未交卷，1表示已交卷',
  `problem_num` mediumint(9) DEFAULT NULL COMMENT '试题总数',
  `correct_complete_time` datetime DEFAULT NULL COMMENT '批卷完成时间，为空表示未完成',
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `(student_id,paper_id)` (`student_id`,`paper_id`),
  KEY `teacher_id` (`teacher_id`),
  KEY `paper_id` (`paper_id`),
  KEY `correct_complete_time` (`correct_complete_time`)
) ENGINE=InnoDB AUTO_INCREMENT=2856 DEFAULT CHARSET=utf8;

/*Table structure for table `exam_problem` */

DROP TABLE IF EXISTS `exam_problem`;

CREATE TABLE `exam_problem` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '学生的考题答案',
  `exam_id` int(11) NOT NULL COMMENT '学生的考试的id',
  `paper_id` int(11) NOT NULL COMMENT '试卷id',
  `problem_id` int(11) NOT NULL COMMENT '考题id',
  `student_answer` text COMMENT '学生答案，选择题多个选项从小到大排序后逗号隔开',
  `score` decimal(5,2) DEFAULT NULL COMMENT '本题分数',
  `student_score` decimal(5,2) DEFAULT NULL COMMENT '学生分数',
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `exam_id` (`exam_id`),
  KEY `problem_id` (`problem_id`),
  KEY `paper_id` (`paper_id`)
) ENGINE=InnoDB AUTO_INCREMENT=74496 DEFAULT CHARSET=utf8;

/*Table structure for table `exam_record` */

DROP TABLE IF EXISTS `exam_record`;

CREATE TABLE `exam_record` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '每次答题记录的id',
  `exam_id` int(11) NOT NULL COMMENT '考题',
  `start_time` datetime NOT NULL COMMENT '开始答题时间',
  `end_time` datetime NOT NULL COMMENT '结束答题时间（默认为理论上的结束时间，如果提前退出就更新这个时间，便于计算）',
  PRIMARY KEY (`id`),
  KEY `exam_id` (`exam_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1943 DEFAULT CHARSET=utf8;

/*Table structure for table `paper` */

DROP TABLE IF EXISTS `paper`;

CREATE TABLE `paper` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '试卷id',
  `title` varchar(100) DEFAULT NULL COMMENT '试卷标题',
  `cover` varchar(255) DEFAULT NULL COMMENT '试卷封面图',
  `exam_time` mediumint(9) NOT NULL COMMENT '考试时间（分钟）',
  `open_time_start` datetime NOT NULL COMMENT '试卷开放起始时间',
  `open_time_end` datetime NOT NULL COMMENT '试卷开放结束时间',
  `pass_grade` decimal(5,2) DEFAULT NULL COMMENT '及格分数',
  `exam_explain` varchar(500) DEFAULT NULL COMMENT '考试说明',
  `teacher_id` int(11) NOT NULL COMMENT '教师的user_id',
  `is_publish` tinyint(4) NOT NULL DEFAULT '0' COMMENT '是否发布试卷',
  `publish_all` tinyint(4) DEFAULT '1' COMMENT '0表示向指定考生发布，1表示向全部考生发布',
  `problem_num` mediumint(9) NOT NULL DEFAULT '0' COMMENT '试题数量',
  `total_score` decimal(5,2) NOT NULL DEFAULT '0.00' COMMENT '试卷总分',
  `student_num` mediumint(9) DEFAULT '0' COMMENT '考生人数',
  `open_answer` tinyint(4) DEFAULT '1' COMMENT '阅卷完成后，是否向学生公开答案，0表示不公开，1表示公开',
  `show_all_problem` tinyint(4) DEFAULT '1' COMMENT '阅卷完成后，学生查看试题范围，0表示只查看错题，1表示查看全部',
  `update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `teacher_id` (`teacher_id`)
) ENGINE=InnoDB AUTO_INCREMENT=238 DEFAULT CHARSET=utf8;

/*Table structure for table `paper_problem` */

DROP TABLE IF EXISTS `paper_problem`;

CREATE TABLE `paper_problem` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '试卷的考题',
  `paper_id` int(11) NOT NULL COMMENT '试卷id',
  `problem_id` int(11) NOT NULL COMMENT '试题id',
  `score` decimal(5,2) NOT NULL COMMENT '试题总分',
  `create_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `paper_id` (`paper_id`),
  KEY `problem_id` (`problem_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7440 DEFAULT CHARSET=utf8;

/*Table structure for table `paper_student` */

DROP TABLE IF EXISTS `paper_student`;

CREATE TABLE `paper_student` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '考生',
  `paper_id` int(11) NOT NULL COMMENT '试卷id',
  `student_id` int(11) NOT NULL COMMENT '学生id',
  PRIMARY KEY (`id`),
  KEY `paper_id` (`paper_id`)
) ENGINE=InnoDB AUTO_INCREMENT=252 DEFAULT CHARSET=utf8;

/*Table structure for table `ppt_course` */

DROP TABLE IF EXISTS `ppt_course`;

CREATE TABLE `ppt_course` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'ppt教程',
  `group_id` int(11) NOT NULL COMMENT 'ppt分组的id',
  `upload_user_id` int(11) NOT NULL COMMENT '上传者id',
  `cover` varchar(100) DEFAULT NULL COMMENT '封面',
  `ppt_name` varchar(255) DEFAULT NULL COMMENT 'ppt名称',
  `file_name` varchar(255) DEFAULT NULL COMMENT 'ppt文件名称',
  `folder_name` varchar(255) DEFAULT NULL COMMENT 'ppt转成图片后存储的文件夹',
  `pic_num` tinyint(4) DEFAULT NULL COMMENT 'ppt转成图片的数量（所有图片根据编号命名）',
  `intro` varchar(255) DEFAULT NULL COMMENT '简介',
  `update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `group_id` (`group_id`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8;

/*Table structure for table `ppt_group` */

DROP TABLE IF EXISTS `ppt_group`;

CREATE TABLE `ppt_group` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'ppt分组的id',
  `group_name` varchar(50) NOT NULL COMMENT 'ppt分组名称',
  `deleted` tinyint(4) NOT NULL DEFAULT '0' COMMENT '是否删除',
  `update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `group_name` (`group_name`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8;

/*Table structure for table `problem` */

DROP TABLE IF EXISTS `problem`;

CREATE TABLE `problem` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '考题',
  `type_id` int(11) NOT NULL COMMENT '考题类型的id',
  `group_id` int(11) NOT NULL COMMENT '考题分组的id(一级分组)',
  `subgroup_id` int(11) NOT NULL COMMENT '考题分组的id(二级分组)',
  `is_choice` tinyint(4) NOT NULL COMMENT '是否是选择题，0表示非选择题，1表示单选题，2表示多选题',
  `title` text COMMENT '题目描述',
  `pic` varchar(500) DEFAULT NULL COMMENT '题目图片名称，用|隔开',
  `answer` text COMMENT '考题答案，选择题多个选项从小到大排序后逗号隔开，填空题每个空之间用|隔开，表格题同一表之间逗号隔开，不同表之间|隔开',
  `analysis` text COMMENT '考题解析',
  `score_point` text COMMENT '评分要点',
  `attachment` varchar(500) DEFAULT NULL COMMENT '附件,多个附近用|隔开',
  `intro` varchar(100) DEFAULT NULL COMMENT '简介',
  `create_user_id` int(11) DEFAULT NULL COMMENT '添加考题的人员id',
  `update_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `create_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `type_id` (`type_id`),
  KEY `create_user_id` (`create_user_id`),
  KEY `subgroup_id` (`subgroup_id`),
  KEY `group_id` (`group_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2190 DEFAULT CHARSET=utf8;

/*Table structure for table `problem_group` */

DROP TABLE IF EXISTS `problem_group`;

CREATE TABLE `problem_group` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '考题分组id',
  `father_id` int(11) NOT NULL DEFAULT '0' COMMENT '父级分组的id，第一级为0',
  `group_name` varchar(50) NOT NULL COMMENT '分组名称',
  `deleted` tinyint(4) NOT NULL DEFAULT '0' COMMENT '是否删除',
  `update_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `create_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `(father_id,group_name)` (`father_id`,`group_name`)
) ENGINE=InnoDB AUTO_INCREMENT=103 DEFAULT CHARSET=utf8;

/*Table structure for table `problem_option` */

DROP TABLE IF EXISTS `problem_option`;

CREATE TABLE `problem_option` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '考题选项的id',
  `problem_id` int(11) NOT NULL COMMENT '考题id',
  `option` varchar(255) DEFAULT NULL COMMENT '选项描述',
  `number` tinyint(4) DEFAULT NULL COMMENT '选项序号',
  `update_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `create_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `(problem_id,number)` (`problem_id`,`number`)
) ENGINE=InnoDB AUTO_INCREMENT=5347 DEFAULT CHARSET=utf8;

/*Table structure for table `problem_type` */

DROP TABLE IF EXISTS `problem_type`;

CREATE TABLE `problem_type` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '考题类型',
  `type_name` varchar(50) NOT NULL COMMENT '类型名称',
  `type_code` int(11) NOT NULL COMMENT '类型唯一编号，上传考题时根据该编号确定考题类型',
  `auto_judge` tinyint(4) DEFAULT NULL COMMENT '是否系统自动判题',
  `update_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `create_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `type_code` (`type_code`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8;

/*Table structure for table `resources` */

DROP TABLE IF EXISTS `resources`;

CREATE TABLE `resources` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '资源id',
  `original_name` varchar(100) DEFAULT NULL COMMENT '文件原始名称（用户上传时的名称）',
  `file_name` varchar(100) DEFAULT NULL COMMENT '服务器文件名',
  `upload_user_id` int(11) DEFAULT NULL COMMENT '上传者id',
  `type` tinyint(4) DEFAULT NULL COMMENT '文件类型，0表示附件，1表示图片',
  `intro` varchar(200) DEFAULT NULL COMMENT '简介',
  `update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `file_name` (`file_name`)
) ENGINE=InnoDB AUTO_INCREMENT=354 DEFAULT CHARSET=utf8;

/*Table structure for table `sys_role` */

DROP TABLE IF EXISTS `sys_role`;

CREATE TABLE `sys_role` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '角色id',
  `role_code` varchar(50) NOT NULL COMMENT '角色标识',
  `role_name` varchar(50) NOT NULL COMMENT '角色名称',
  `update_time` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `create_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `role_code` (`role_code`),
  UNIQUE KEY `role_name` (`role_name`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

/*Table structure for table `sys_user` */

DROP TABLE IF EXISTS `sys_user`;

CREATE TABLE `sys_user` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '用户id',
  `role_id` int(11) NOT NULL COMMENT '角色id',
  `account` varchar(50) CHARACTER SET utf8 NOT NULL COMMENT '账号',
  `password` varchar(255) CHARACTER SET utf8 NOT NULL COMMENT '密码',
  `name` varchar(50) CHARACTER SET utf8 DEFAULT NULL COMMENT '姓名',
  `email` varchar(100) CHARACTER SET utf8 DEFAULT NULL COMMENT '邮箱',
  `phone` varchar(50) CHARACTER SET utf8 DEFAULT NULL COMMENT '电话',
  `school_name` varchar(50) CHARACTER SET utf8 DEFAULT NULL COMMENT '所属学校名称',
  `member_num` smallint(6) NOT NULL DEFAULT '0' COMMENT '可开设学生账号数',
  `contest_num` smallint(6) NOT NULL DEFAULT '1' COMMENT '可同时发布竞赛模拟数',
  `create_user_id` int(11) NOT NULL COMMENT '添加该账号的用户id',
  `forbid` tinyint(4) NOT NULL DEFAULT '0' COMMENT '是否禁止登录',
  `deleted` tinyint(4) NOT NULL DEFAULT '0' COMMENT '是否删除',
  `update_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `create_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `account` (`account`),
  UNIQUE KEY `email` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=1399 DEFAULT CHARSET=latin1;

/*Table structure for table `teach_class` */

DROP TABLE IF EXISTS `teach_class`;

CREATE TABLE `teach_class` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '教学班级的id',
  `teacher_id` int(11) NOT NULL COMMENT '教师id',
  `student_id` int(11) NOT NULL COMMENT '学生id',
  `create_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `(teacher_id,student_id)` (`student_id`,`teacher_id`),
  KEY `teacher_id` (`teacher_id`),
  KEY `student_id` (`student_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1274 DEFAULT CHARSET=utf8;

/*Table structure for table `user_contest_group` */

DROP TABLE IF EXISTS `user_contest_group`;

CREATE TABLE `user_contest_group` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '用户竞赛模拟分组权限',
  `user_id` int(11) NOT NULL COMMENT '用户id',
  `group_id` int(11) NOT NULL COMMENT '竞赛模拟分组id',
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `(user_id,group_id)` (`user_id`,`group_id`)
) ENGINE=InnoDB AUTO_INCREMENT=970 DEFAULT CHARSET=utf8;

/*Table structure for table `user_log` */

DROP TABLE IF EXISTS `user_log`;

CREATE TABLE `user_log` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '用户操作日志的id',
  `ip` varchar(50) DEFAULT NULL COMMENT '用户ip',
  `account` varchar(50) DEFAULT NULL COMMENT '账号',
  `name` varchar(50) DEFAULT NULL COMMENT '姓名',
  `operation` varchar(30) DEFAULT NULL COMMENT '操作描述',
  `os` varchar(30) DEFAULT NULL COMMENT '操作系统',
  `browser` varchar(30) DEFAULT NULL COMMENT '浏览器',
  `result` varchar(30) DEFAULT NULL COMMENT '请求结果',
  `url` varchar(255) DEFAULT NULL COMMENT 'url',
  `create_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2167 DEFAULT CHARSET=utf8;

/*Table structure for table `user_ppt` */

DROP TABLE IF EXISTS `user_ppt`;

CREATE TABLE `user_ppt` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '学生可查看的ppt',
  `user_id` int(11) NOT NULL COMMENT '用户id',
  `ppt_id` int(11) NOT NULL COMMENT 'ppt教程的id',
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `(user_id,ppt_id)` (`user_id`,`ppt_id`)
) ENGINE=InnoDB AUTO_INCREMENT=355 DEFAULT CHARSET=utf8;

/*Table structure for table `user_ppt_group` */

DROP TABLE IF EXISTS `user_ppt_group`;

CREATE TABLE `user_ppt_group` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '用户可查看的ppt分组',
  `user_id` int(11) NOT NULL COMMENT '用户id',
  `group_id` int(11) NOT NULL COMMENT 'ppt分组id',
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `(user_id,group_id)` (`user_id`,`group_id`)
) ENGINE=InnoDB AUTO_INCREMENT=339 DEFAULT CHARSET=utf8;

/*Table structure for table `user_problem_group` */

DROP TABLE IF EXISTS `user_problem_group`;

CREATE TABLE `user_problem_group` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '用户分组授权',
  `group_id` int(11) NOT NULL COMMENT '考题分组的id(一级分组)',
  `user_id` int(11) NOT NULL COMMENT '用户id',
  `create_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `(user_id,group_id)` (`user_id`,`group_id`)
) ENGINE=InnoDB AUTO_INCREMENT=569 DEFAULT CHARSET=utf8;

/*Table structure for table `user_video` */

DROP TABLE IF EXISTS `user_video`;

CREATE TABLE `user_video` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '学生可查看的视频',
  `user_id` int(11) NOT NULL COMMENT '用户id',
  `video_id` int(11) NOT NULL COMMENT '视频教程的id',
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `(user_id,video_id)` (`user_id`,`video_id`)
) ENGINE=InnoDB AUTO_INCREMENT=481 DEFAULT CHARSET=utf8;

/*Table structure for table `user_video_group` */

DROP TABLE IF EXISTS `user_video_group`;

CREATE TABLE `user_video_group` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '用户可查看的视频分组',
  `user_id` int(11) NOT NULL COMMENT '用户id',
  `group_id` int(11) NOT NULL COMMENT '视频分组的id',
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `(user_id,group_id)` (`user_id`,`group_id`)
) ENGINE=InnoDB AUTO_INCREMENT=465 DEFAULT CHARSET=utf8;

/*Table structure for table `video_course` */

DROP TABLE IF EXISTS `video_course`;

CREATE TABLE `video_course` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '视频教程的id',
  `group_id` int(11) NOT NULL COMMENT '分组id',
  `upload_user_id` int(11) NOT NULL COMMENT '上传者id',
  `cover` varchar(100) DEFAULT NULL COMMENT '封面',
  `video_name` varchar(50) DEFAULT NULL COMMENT '视频名称',
  `file_name` varchar(255) DEFAULT NULL COMMENT '视频文件名',
  `intro` varchar(500) DEFAULT NULL COMMENT '简介',
  `update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `group_id` (`group_id`)
) ENGINE=InnoDB AUTO_INCREMENT=53 DEFAULT CHARSET=utf8;

/*Table structure for table `video_group` */

DROP TABLE IF EXISTS `video_group`;

CREATE TABLE `video_group` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '视频分组的id',
  `group_name` varchar(50) NOT NULL COMMENT '分组名称',
  `deleted` tinyint(4) NOT NULL DEFAULT '0' COMMENT '是否删除',
  `update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `group_name` (`group_name`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
