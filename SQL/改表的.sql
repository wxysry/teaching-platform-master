ALTER TABLE gb_contest_student ADD COLUMN every_season_end_time varchar(500) COMMENT '每个季度结束时间';
ALTER TABLE gb_contest ADD COLUMN fair_order_state tinyint(4) NOT NULL DEFAULT '0' COMMENT '订货会阶段';
ALTER TABLE gb_ct_mn_ad ADD COLUMN create_time timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
