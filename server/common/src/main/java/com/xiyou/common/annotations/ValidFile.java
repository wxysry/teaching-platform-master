package com.xiyou.common.annotations;

/**
 * @Author: tangcan
 * @Description: 验证文件格式
 * @Param:
 * @date: 2019/6/18
 */

import com.xiyou.common.validator.MultipartFileValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target({ElementType.FIELD, ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = MultipartFileValidator.class)
public @interface ValidFile {
    String DEFAULT_MAXSIZE = "-1";
    String DEFAULT_MINSIZE = "0KB";

    /*
    文件格式
     */
    String[] file() default {};

    /*
    是否区分大小写
     */
    boolean ignoreCase() default false;

    /*
    是否允许上传空文件
     */
    boolean allowEmpty() default false;

    /*
    文件大小限制，必须小于等于SpringMVC中文件上传配置
    单位 "MB"、"KB"
     */
    String maxSize() default DEFAULT_MAXSIZE;

    String minSize() default DEFAULT_MINSIZE;

    /*
    错误信息提示
     */
    String message() default "The uploaded file is not verified.";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
