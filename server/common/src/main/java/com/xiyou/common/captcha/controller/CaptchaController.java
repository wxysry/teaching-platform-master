package com.xiyou.common.captcha.controller;

import com.xiyou.common.captcha.Captcha;
import com.xiyou.common.captcha.service.CaptchaService;
import com.xiyou.common.utils.R;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

/**
 * @program: multi-module
 * @description: 图片验证码Captcha
 * @author: tangcan
 * @create: 2019-06-18 21:59
 **/
@RestController
@Api(tags = "图片验证码")
@RequestMapping("/tp/captcha")
public class CaptchaController {
    @Autowired
    private CaptchaService captchaService;

    @ResponseBody
    @GetMapping("/get")
    @ApiOperation(value = "获取")
    public R getCaptcha() {
        Map<String, Object> returnMap = new HashMap<>();
        Captcha captcha = captchaService.getCaptcha();
        returnMap.put("base64", captcha.getBase64());
        returnMap.put("captchaToken", captcha.getCaptchaToken());
        return R.success(returnMap);
    }
}
