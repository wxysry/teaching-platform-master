package com.xiyou.common.captcha.service;

import com.xiyou.common.captcha.Captcha;
import com.xiyou.common.enums.CodeEnum;
import com.xiyou.common.exception.CustomException;
import com.xiyou.common.redis.utils.RedisCache;
import com.xiyou.common.redis.utils.RedisKey;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Random;
import java.util.UUID;

/**
 * @description:
 * @author: tangcan
 * @create: 2019-04-26 17:07
 **/
@Component
public class CaptchaService {
    // 随机产生数字与字母组合的字符串
    private static String RAND_STRING = "23456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghjkmnpqrstuvwxyz";
    private static int WIDTH = 95;// 图片宽
    private static int HEIGHT = 25;// 图片高
    private static int LINE_SIZE = 30;// 干扰线数量
    private static int STRING_NUM = 4;// 随机产生字符数量
    private static int EXPIRE = 5 * 60;

    private final RedisCache redisCache;

    public CaptchaService(RedisCache redisCache) {
        this.redisCache = redisCache;
    }

    /**
     * @Author: tangcan
     * @Description: 获取验证码图片的base64数据，并缓存验证码
     * @Param: [ip]
     * @date: 2019/6/18
     */
    public Captcha getCaptcha() {
        // 绘制随机字符
        StringBuilder randomString = new StringBuilder();
        for (int i = 1; i <= STRING_NUM; i++) {
            String rand = getRandomString(random.nextInt(RAND_STRING.length()));
            randomString.append(rand);
        }
        Captcha captcha = new Captcha();
        captcha.setCode(randomString.toString())
                .setCaptchaToken(UUID.randomUUID().toString());
        captcha.setBase64(drawCaptcha(captcha.getCode()));
        redisCache.set(RedisKey.get("captcha", "token", captcha.getCaptchaToken()), captcha.getCode(), EXPIRE);

        return captcha;
    }

    public boolean checkCode(String captchaToken, String code) {
        if (StringUtils.isAnyBlank(captchaToken, code)) {
            return false;
        }
        String codeCache = (String) redisCache.get(RedisKey.get("captcha", "token", captchaToken));
        if (codeCache == null) {
            redisCache.delete(RedisKey.get("captcha", "token", captchaToken));
            throw new CustomException(CodeEnum.CODE_EXPIRE);
        }
        // 忽略大小写进行比较
        if (!code.toUpperCase().equals(codeCache.toUpperCase())) {
            return false;
        }
        redisCache.delete(RedisKey.get("captcha", "token", captchaToken));
        return true;
    }

    /**
     * 获取验证码数据
     */
    private String drawCaptcha(String code) {
        // BufferedImage类是具有缓冲区的Image类,Image类是用于描述图像信息的类
        BufferedImage image = new BufferedImage(WIDTH, HEIGHT, BufferedImage.TYPE_INT_BGR);
        Graphics g = image.getGraphics();// 产生Image对象的Graphics对象,改对象可以在图像上进行各种绘制操作
        g.fillRect(0, 0, WIDTH, HEIGHT);//图片大小
        g.setFont(new Font(null, Font.ROMAN_BASELINE, 22));//字体大小
        g.setColor(getRandColor(110, 133));//字体颜色
        // 绘制干扰线
        for (int i = 0; i <= LINE_SIZE; i++) {
            drowLine(g);
        }
        // 绘制随机字符
        for (int i = 1; i <= STRING_NUM; i++) {
            drowString(g, String.valueOf(code.charAt(i - 1)), i);
        }
        g.dispose();
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        // 将内存中的图片通过流动形式输出到客户端
        try {
            ImageIO.write(image, "jpg", out);
        } catch (IOException e) {
            e.printStackTrace();
        }
        byte[] bytes = out.toByteArray();
        /*
        返回验证码图片的base64数据
         */
        return Base64.encodeBase64String(bytes);
    }


    private Random random = new Random();

    /**
     * 获得字体
     */
    private Font getFont() {
        return new Font("Fixedsys", Font.CENTER_BASELINE, 18);
    }

    /**
     * 获得颜色
     */
    private Color getRandColor(int fc, int bc) {
        if (fc > 255)
            fc = 255;
        if (bc > 255)
            bc = 255;
        int r = fc + random.nextInt(bc - fc - 16);
        int g = fc + random.nextInt(bc - fc - 14);
        int b = fc + random.nextInt(bc - fc - 18);
        return new Color(r, g, b);
    }

    /**
     * 绘制字符串
     */
    private void drowString(Graphics g, String rand, int i) {
        g.setFont(getFont());
        g.setColor(new Color(random.nextInt(101), random.nextInt(111), random
                .nextInt(121)));
        g.translate(random.nextInt(3), random.nextInt(3));
        g.drawString(rand, 13 * i, 16);
    }

    /**
     * 绘制干扰线
     */
    private void drowLine(Graphics g) {
        int x = random.nextInt(WIDTH);
        int y = random.nextInt(HEIGHT);
        int xl = random.nextInt(20);
        int yl = random.nextInt(17);
        g.drawLine(x, y, x + xl, y + yl);
    }

    /**
     * 获取随机的字符
     */
    private String getRandomString(int num) {
        return String.valueOf(RAND_STRING.charAt(num));
    }
}
