package com.xiyou.common.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * @program: attendance
 * @description: swagger的启动类配置
 * @author: tangcan
 * @create: 2018-11-28 13:36
 **/
@Configuration
@EnableSwagger2
public class Swagger2 {
    @Value("${swagger.enable}")
    private String ENABLE;
    @Value("${swagger.title}")
    private String TITLE;
    @Value("${swagger.version}")
    private String VERSION;
    @Value("${swagger.terms-of-service-url}")
    private String TERMS_OF_SERVICE_URL;

    /**
     * swagger2的配置文件，这里可以配置swagger2的一些基本的内容，比如扫描的包等等
     */
    @Bean
    public Docket createRestApi() {
        return new Docket(DocumentationType.SWAGGER_2)
                .enable(Boolean.parseBoolean(ENABLE))
                .apiInfo(apiInfo())
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.xiyou"))
                .paths(PathSelectors.any())
                .build();
    }

    /**
     * 构建 api文档的详细信息函数,注意这里的注解引用的是哪个
     */
    private ApiInfo apiInfo() {
        return new ApiInfoBuilder()
                .title(TITLE)
                .version(VERSION)
                .termsOfServiceUrl(TERMS_OF_SERVICE_URL)
                .build();
    }
}
