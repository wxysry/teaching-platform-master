package com.xiyou.common.constants;

public interface FileType {
    /*
    Excel文件
     */
    String XLS = "xls";
    String XLSX = "xlsx";
    /*
    Word文件
     */
    String DOC = "doc";
    String DOCX = "docx";
    /*
    pdf文件
     */
    String PDF = "pdf";
    /*
    压缩文件
     */
    String RAR = "rar";
    String ZIP = "zip";
    /*
    视频文件
     */
    String AVI = "avi";
    String MP4 = "mp4";
    String RM = "rm";
    String MOV = "mov";
    /*
    记事本
     */
    String TXT = "txt";
    /*
    PPT
     */
    String PPT = "ppt";
    String PPTX = "pptx";
    /*
    图片
     */
    String BMP = "bmp";
    String GIF = "gif";
    String JPG = "jpg";
    String PNG = "png";
}
