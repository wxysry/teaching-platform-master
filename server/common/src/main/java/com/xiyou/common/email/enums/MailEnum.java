package com.xiyou.common.email.enums;

public enum MailEnum {
    CODE_MAIL("邮箱验证码", "code_email.ftl"),

    ;
    String type;
    String ftl;

    MailEnum(String type, String ftl) {
        this.type = type;
        this.ftl = ftl;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getFtl() {
        return ftl;
    }

    public void setFtl(String ftl) {
        this.ftl = ftl;
    }
}
