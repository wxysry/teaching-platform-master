package com.xiyou.common.email.service;


import com.xiyou.common.enums.CodeEnum;
import com.xiyou.common.email.enums.MailEnum;
import com.xiyou.common.exception.CustomException;
import freemarker.template.Configuration;
import freemarker.template.Template;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import org.springframework.ui.freemarker.FreeMarkerTemplateUtils;

import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import java.io.UnsupportedEncodingException;
import java.security.Security;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Properties;

/**
 * @program: attendance
 * @description: 发送邮件的服务
 * @author: tangcan
 * @create: 2018-12-16 22:25
 **/
@Component
public class Mail {
    @Value("${spring.mail.host}")
    private String FROM_MAIL_SMTP;
    @Value("${spring.mail.username}")
    private String FROM_MAIL_NAME;
    @Value("${spring.mail.password}")
    private String FROM_MAIL_PASS;

    private Configuration configuration = new Configuration(Configuration.VERSION_2_3_23);

    /**
     * @Author: tangcan
     * @Description: 发送给多个用户
     * @Param: [mailEnum, model, userList]
     * @date: 2018/12/17
     */
    @Async
    public void sendMail(MailEnum mailEnum, Map<String, Object> model, List<String> userList) {
        if (userList == null || userList.size() == 0) {
            return;
        }
        int size = userList.size();
        String users = userList.get(0);
        for (int i = 1; i < size; i++) {
            users += "," + userList.get(i);
        }
        sendMail(mailEnum, model, users);
    }

    /**
     * @Author: tangcan
     * @Description: 发送给多个用户
     * users用逗号隔开
     * 由于25端口被云服务器禁用，所以使用465端口
     * @Param: [mailEnum, model, users]
     * @date: 2018/12/17
     */
    @Async
    public void sendMail(MailEnum mailEnum, Map<String, Object> model, String users) {
        if (users == null || users.length() == 0) {
            return;
        }
        String content;
        try {
            configuration.setClassForTemplateLoading(this.getClass(), "/email");
            Template template = configuration.getTemplate(mailEnum.getFtl());
            content = FreeMarkerTemplateUtils.processTemplateIntoString(template, model);
        } catch (Exception e) {
            e.printStackTrace();
            throw new CustomException(CodeEnum.SEND_MAIL_ERROR);
        }

        try {
            Security.addProvider(new com.sun.net.ssl.internal.ssl.Provider());
            final String SSL_FACTORY = "javax.net.ssl.SSLSocketFactory";
            final Properties p = System.getProperties();
            p.setProperty("mail.smtp.host", FROM_MAIL_SMTP);
            p.setProperty("mail.smtp.auth", "true");
            p.setProperty("mail.smtp.user", FROM_MAIL_NAME);
            p.setProperty("mail.smtp.pass", FROM_MAIL_PASS);
            p.setProperty("mail.smtp.socketFactory.class", SSL_FACTORY);
            p.setProperty("mail.smtp.socketFactory.fallback", "false");
            /**
             * 邮箱发送服务器端口,这里设置为465端口
             */
            p.setProperty("mail.smtp.port", "465");
            p.setProperty("mail.smtp.socketFactory.port", "465");

            /**
             * 根据邮件会话属性和密码验证器构造一个发送邮件的session
             */
            Session session = Session.getInstance(p, new Authenticator() {
                protected PasswordAuthentication getPasswordAuthentication() {
                    return new PasswordAuthentication(p.getProperty("mail.smtp.user"), p.getProperty("mail.smtp.pass"));
                }
            });
            Message message = new MimeMessage(session);
            /**
             * 消息发送的主题
             */
            message.setSubject(mailEnum.getType());
            /**
             * 接受消息的人
             */
            message.setReplyTo(InternetAddress.parse(FROM_MAIL_NAME));
            /**
             * 消息的发送者
             */
            message.setFrom(new InternetAddress(p.getProperty("mail.smtp.user"), "验证码"));
            /**
             * 创建邮件的接收者地址，并设置到邮件消息中
             */
            String[] split = users.split(",");
            InternetAddress[] tos = new InternetAddress[split.length];
            for (int i = 0; i < split.length; i++) {
                tos[i] = new InternetAddress(split[i]);
            }
            message.setRecipients(Message.RecipientType.TO, tos);
            /**
             * 消息发送的时间
             */
            message.setSentDate(new Date());
            Multipart mainPart = new MimeMultipart();
            /**
             * 创建一个包含HTML内容的MimeBodyPart
             */
            BodyPart html = new MimeBodyPart();
            /**
             * 设置HTML内容
             */
            html.setContent(content, "text/html; charset=utf-8");
            mainPart.addBodyPart(html);
            /**
             * 将MiniMultipart对象设置为邮件内容
             */
            message.setContent(mainPart);
            message.saveChanges();
            Transport.send(message);
        } catch (MessagingException | UnsupportedEncodingException e) {
            throw new CustomException(CodeEnum.SEND_MAIL_ERROR);
        }
    }
}
