package com.xiyou.common.email.service;


import com.xiyou.common.email.enums.MailEnum;
import com.xiyou.common.enums.CodeEnum;
import com.xiyou.common.exception.CustomException;
import com.xiyou.common.redis.utils.RedisCache;
import com.xiyou.common.redis.utils.RedisKey;
import com.xiyou.common.utils.DateUtil;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * @description:
 * @author: tangcan
 * @create: 2019-04-26 19:07
 **/
@Component
public class MailService {
    @Autowired
    private RedisCache redisCache;
    @Autowired
    private Mail mail;

    private static final int EMAIL_EXPIRE = 30 * 60;

    public void sendMailCode(String email) {
        sendMailCode(email, MailEnum.CODE_MAIL, null);
    }

    public void sendMailCode(String email, MailEnum mailEnum, Map<String, Object> model) {
        if (email == null) {
            return;
        }
        if (model == null) {
            model = new HashMap<>();
        }
        String code = getRandNumber();
        redisCache.set(RedisKey.get("email", "code", email), code, EMAIL_EXPIRE);
        model.put("date", DateUtil.format(new Date(), "yyyy年MM月dd日 HH:mm:ss"));
        model.put("code", code);
        mail.sendMail(mailEnum, model, email);
    }

    public boolean checkCode(String email, String code) {
        if (StringUtils.isAnyBlank(email, code)) {
            return false;
        }
        String codeCache = (String) redisCache.get(RedisKey.get("email", "code", email));
        if (codeCache == null) {
            throw new CustomException(CodeEnum.CODE_EXPIRE);
        }
        if (!code.equals(codeCache)) {
            return false;
        }
        return true;
    }

    /**
     * @Author: dym
     * @Description: 随机出来的数乘上900000 得到一个0-900000的数，再加上100000就得到100000-1000000的六位数
     * @Param: []
     * @return: java.lang.String
     * @date: 2018/11/26
     */
    public String getRandNumber() {
        int num = (int) (Math.random() * 900000) + 100000;
        return Integer.toString(num);
    }
}
