package com.xiyou.common.log.aop;

import com.alibaba.druid.support.json.JSONUtils;
import com.xiyou.common.utils.NetworkUtil;
import com.xiyou.common.log.annotations.OptLog;
import com.xiyou.common.log.entity.UserLog;
import com.xiyou.common.log.components.SaveUserLog;
import com.xiyou.common.shiro.utils.ShiroUtil;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.*;
import org.aspectj.lang.reflect.MethodSignature;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;


/**
 * @program: attendance
 * @description: 日志记录
 * @author: tangcan
 * @create: 2018-11-26 14:38
 **/
@Aspect
@Component
public class LogAspect {
    private final Logger log = LoggerFactory.getLogger(this.getClass());


    /*
    不存库，控制台打印
     */
    @Pointcut("execution(public * com.xiyou.*.controller..*.*(..))")
    public void webLog() {
    }

    @Around("webLog()")
    public Object doAround(ProceedingJoinPoint pjp) throws Throwable {
        /*
          接收到请求
         */
        ServletRequestAttributes attributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        MethodSignature signature = (MethodSignature) pjp.getSignature();
        assert attributes != null;
        HttpServletRequest request = attributes.getRequest();

        log.info("-------------------------------------------");
        /*
          获取真实的ip地址
         */
        log.info("IP 地址 : " + NetworkUtil.getIpAddress(request));
        /*
          记录请求
         */
        log.info("请求方式 : " + request.getMethod());
        log.info("请求接口 : " + request.getRequestURI());
        log.info("调用方法 : " + pjp.getSignature().getDeclaringTypeName() + "." + pjp.getSignature().getName());
        log.info("请求参数 : " + JSONUtils.toJSONString(getParamMap(signature.getParameterNames(), pjp.getArgs())));
        long startTime = System.currentTimeMillis();
        /*
          ob 为方法的返回值
         */
        Object ob = pjp.proceed();
        log.info("请求耗时 : " + (System.currentTimeMillis() - startTime) + " ms");
        if (ob != null) {
            log.info("返回字段 : " + ob);
        }
        log.info("-------------------------------------------");
        return ob;
    }

    /**
     * @Author: tangcan
     * @Description: 获取请求的参数
     * @Param: [argNames, request]
     * @date: 2019/6/16
     */
    private Map<String, String> getParamMap(String[] argNames, Object[] argValues) {
        Map<String, String> paramMap = new HashMap<>();
        int paramSize = argNames.length;
        for (int i = 0; i < paramSize; i++) {
            paramMap.put(argNames[i], String.valueOf(argValues[i]));
        }
        return paramMap;
    }
}
