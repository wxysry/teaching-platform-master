package com.xiyou.common.log.dao;

import com.xiyou.common.log.entity.UserLog;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author tangcan
 * @since 2019-06-14
 */
public interface UserLogMapper extends BaseMapper<UserLog> {

}
