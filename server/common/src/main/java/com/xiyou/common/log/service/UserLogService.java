package com.xiyou.common.log.service;

import com.xiyou.common.log.entity.UserLog;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author tangcan
 * @since 2019-06-14
 */
public interface UserLogService extends IService<UserLog> {

}
