package com.xiyou.common.office.utils;

import com.alibaba.excel.EasyExcelFactory;
import com.alibaba.excel.ExcelWriter;
import com.alibaba.excel.event.AnalysisEventListener;
import com.alibaba.excel.metadata.BaseRowModel;
import com.alibaba.excel.metadata.Sheet;
import com.alibaba.excel.util.CollectionUtils;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

import static com.alibaba.excel.EasyExcelFactory.readBySax;

/**
 * @author xingzi
 * @date 2019 07 22  21:12
 */
public class EasyExcelUtil {
    private static Sheet initSheet;

    static {
        initSheet = new Sheet(1, 0);
        initSheet.setSheetName("sheet");
        //设置自适应宽度
        initSheet.setAutoWidth(Boolean.TRUE);
    }

    /**
     * @Author: tangcan
     * @Description: 读入多sheet的excel，数据存入实体类中，无参数校验
     * @Param: [inputStream, sheet]
     * @date: 2019/8/28
     */
    public static <T> List<T> readExcel(InputStream inputStream, Sheet sheet) {
        if (sheet.getClazz() == null) {
            return null;
        }
        List<T> list = new ArrayList<>();
        List<Object> objects = EasyExcelFactory.read(inputStream, sheet);
        objects.forEach(
                o -> list.add((T) o)
        );
        return list;
    }

    public static void readExcel(InputStream inputStream, Sheet sheet, AnalysisEventListener listener) {

        readBySax(inputStream, sheet, listener);

    }

    /**
     * 写文件
     *
     * @param filePath 路径
     * @param data     list data
     * @param sheet    表格
     */
    public static String writeExcelTemplate(String filePath, List<? extends BaseRowModel> data, Sheet sheet) {
        if (CollectionUtils.isEmpty(data)) {
            return "";
        }
        try (OutputStream outputStream = new FileOutputStream(filePath)) {
            sheet = (sheet != null) ? sheet : initSheet;
            sheet.setClazz(data.get(0).getClass());

            ExcelWriter writer;
            writer = EasyExcelFactory.getWriter(outputStream);
            writer.write(data, sheet);
            writer.finish();
            return filePath;
        } catch (IOException e) {
            e.printStackTrace();
            return "";
        }
    }
}
