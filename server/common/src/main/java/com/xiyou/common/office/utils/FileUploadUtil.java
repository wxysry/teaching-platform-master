package com.xiyou.common.office.utils;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;
import java.util.UUID;

/**
 * @program: multi-module
 * @description: 文件上传
 * @author: tangcan
 * @create: 2019-09-10 19:39
 **/
public class FileUploadUtil {
    /**
     * @Author: tangcan
     * @Description: 文件批量上传，返回文件名称，用逗号隔开
     * @Param: [files, savePath]
     * @date: 2019/9/10
     */
    public static String uploadFiles(MultipartFile[] files, String savePath) {
        // 检查参数合法性
        if (ArrayUtils.isEmpty(files) || StringUtils.isBlank(savePath)) {
            return null;
        }
        StringBuilder buffer = new StringBuilder();
        // 单独处理每个文件
        for (MultipartFile file : files) {
            // 文件原始名
            String originalFilename = file.getOriginalFilename();
            assert originalFilename != null;
            // 取出文件后缀
            String suffixName = originalFilename.substring(originalFilename.lastIndexOf("."));
            // 文件重命名
            String fileName = getRandomFileName() + suffixName;
            // 文件存放路径+文件名称
            String filePath = savePath + fileName;
            try {
                File dest = new File(filePath);
                if (!dest.getParentFile().exists()) {
                    dest.getParentFile().mkdirs();
                }
                file.transferTo(dest);
            } catch (IOException e) {
                e.printStackTrace();
                return null;
            }
            // 使用逗号隔开
            if (buffer.length() > 0) {
                buffer.append(',');
            }
            buffer.append(fileName);
        }
        return buffer.toString();
    }

    /**
     * @Author: tangcan
     * @Description: 获取随机文件名
     * @Param: []
     * @date: 2019/9/10
     */
    private static String getRandomFileName() {
        // UUID前10位 + 当前时间
        return UUID.randomUUID().toString().replaceAll("-", "").substring(0, 10)
                + "D" + LocalDateTime.now().toString()
                .replaceAll(":", "")
                .replaceAll("-", "")
                .replaceAll("\\.", "");
    }
}
