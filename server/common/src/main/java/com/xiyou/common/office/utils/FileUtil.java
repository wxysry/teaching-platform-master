package com.xiyou.common.office.utils;

import com.alibaba.excel.support.ExcelTypeEnum;
import com.xiyou.common.enums.CodeEnum;
import com.xiyou.common.exception.CustomException;
import com.xiyou.common.utils.CommonUtil;
import net.coobird.thumbnailator.Thumbnails;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.net.URLEncoder;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

/**
 * @program: multi-module
 * @description: 文件操作工具
 * @author: tangcan
 * @create: 2019-07-09 14:56
 **/
public class FileUtil {
    private static Logger log = LoggerFactory.getLogger(FileUtil.class);
    /*
    生成随机文件名，不压缩
     */
    public static String upload(MultipartFile file, String parentPath) {
        String realFileName = CommonUtil.getUniqueFileName(file.getOriginalFilename());
        return upload(file, parentPath, realFileName, false);
    }

    /*
    生成随机文件名，可选择是否压缩
     */
    public static String upload(MultipartFile file, String parentPath, boolean compress) {
        String realFileName = CommonUtil.getUniqueFileName(file.getOriginalFilename());
        return upload(file, parentPath, realFileName, compress);
    }

    /*
    指定文件名，不压缩
     */
    public static String upload(MultipartFile file, String parentPath, String fileName) {
        return upload(file, parentPath, fileName, false);
    }

    /*
    指定文件名，可选择是否压缩
     */
    public static String upload(MultipartFile file, String savePath, String fileName, boolean compress) {
        if (file == null || fileName == null) {
            return null;
        }
        String originalFilename = file.getOriginalFilename();
        assert originalFilename != null;
        String suffixName = originalFilename.substring(originalFilename.lastIndexOf("."));
        String realFileName = fileName + suffixName;
        String picPath = savePath + realFileName;
        try {
            File dest = new File(picPath);
            if (!dest.getParentFile().exists()) {
                dest.getParentFile().mkdirs();
            }
            file.transferTo(dest);
        } catch (IOException e) {
            throw new CustomException(CodeEnum.FILE_UPLOAD_FAIL.getCode(), e.getMessage());
        }
        if (compress) {
            compressImage(picPath);
        }
        return realFileName;
    }

    /*
    导出任意文件格式
     */
    public static void exportFile(HttpServletResponse response, String picPath, String fileName) throws IOException {
        // 设置文件ContentType类型，这样设置，会自动判断下载文件类型
        response.setContentType("multipart/form-data");
        response.setHeader("Content-Disposition", "attachment;fileName=" + URLEncoder.encode(fileName, "utf-8"));
        fileExport(response, picPath);
    }

    /*
    导出图片
     */
    public static void exportPic(HttpServletResponse response, String picPath) throws IOException {
        response.setContentType("image/jpeg");
        fileExport(response, picPath);
    }

    /*
    导出视频
     */
    public static void exportVideo(HttpServletResponse response, String picPath) throws IOException {
        response.setContentType("application/octet-stream");
        fileExport(response, picPath);
    }

    /*
    文件导出具体实现
     */
    private static void fileExport(HttpServletResponse response, String filePath) throws IOException {
        File file = new File(filePath);
        BufferedInputStream is = new BufferedInputStream(new FileInputStream(file));
        OutputStream os = new BufferedOutputStream(response.getOutputStream());
        /*
          一次仅传输1K，不会溢出
         */
        int buf_size = 1024;
        byte[] buffer = new byte[buf_size];
        int len;
        while (-1 != (len = is.read(buffer, 0, buf_size))) {
            os.write(buffer, 0, len);
        }
        os.flush();
        os.close();
    }

    /*
     * 压缩图片
     */
    public static void compressImage(String picPath) {
        compressImage(picPath, 200);
    }

    public static void compressImage(String picPath, int maxSize) {
        if (StringUtils.isBlank(picPath)) {
            return;
        }
        File file = new File(picPath);
        int size = (int) ((file.length() + 0.5) / 1024);
        log.info("压缩前大小：" + size + "kb");
        if (size < maxSize) {
            log.info("压缩后大小：" + size + "kb");
            return;
        }
        try {
            while (size > maxSize) {
                double accuracy = getAccuracy(size);
                Thumbnails.of(picPath).scale(accuracy).outputQuality(accuracy).toFile(picPath);
                size = (int) ((file.length() + 0.5) / 1024);
            }
            log.info("压缩后大小：" + size + "kb");
        } catch (Exception ignored) {
        }
    }

    private static double getAccuracy(long size) {
        double accuracy;
        if (size < 900) {
            accuracy = 0.85;
        } else if (size < 2047) {
            accuracy = 0.6;
        } else if (size < 3275) {
            accuracy = 0.44;
        } else {
            accuracy = 0.4;
        }
        return accuracy;
    }

    /*
    文件压缩并下载
    由于每个文件不方便进行中文命名，因此单独使用fileNameList对应每个文件的名称
     */
    public static void toZipAndDownload(HttpServletResponse response, List<File> files, List<String> fileNameList, String zipName) throws IOException {
        // 设置文件ContentType类型，这样设置，会自动判断下载文件类型
        response.setContentType("multipart/form-data");
        response.setHeader("Content-Disposition", "attachment;fileName=" + URLEncoder.encode(zipName, "utf-8"));
        byte[] buffer = new byte[1024];
        try (ZipOutputStream zout = new ZipOutputStream(response.getOutputStream())) {
            int i = 0;
            for (File file : files) {
                FileInputStream fis = new FileInputStream(file);
                zout.putNextEntry(new ZipEntry(fileNameList.get(i)));
                i++;
                int len;
                while ((len = fis.read(buffer)) != -1) {
                    zout.write(buffer, 0, len);
                }
                zout.flush();
                zout.closeEntry();
                fis.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 把多个文件保存为一个压缩文件
     * @param files
     * @param saveFile
     * @param fileNameList
     * @throws FileNotFoundException
     */
    public static void saveFileToZip(List<File> files,File saveFile,List<String> fileNameList) throws FileNotFoundException {
        byte[] buffer = new byte[1024];
        try (
                FileOutputStream fos = new FileOutputStream(saveFile);
                ZipOutputStream zout = new ZipOutputStream(fos);
             )
        {
            int i = 0;
            for (File file : files) {
                FileInputStream fis = new FileInputStream(file);
                zout.putNextEntry(new ZipEntry(fileNameList.get(i)));
                i++;
                int len;
                while ((len = fis.read(buffer)) != -1) {
                    zout.write(buffer, 0, len);
                }
                zout.flush();
                zout.closeEntry();
                fis.close();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 创建Excel文件
     * @param tempFilePath
     * @param uniqueFileName
     * @return
     */
    public static File createExcelFile(String tempFilePath,String uniqueFileName){
        File file = new File(tempFilePath+uniqueFileName+ ExcelTypeEnum.XLSX.getValue());
        if (!file.getParentFile().exists()) {
            file.getParentFile().mkdirs();
        }
        return file;
    }
    /**
     * 创建Zip文件
     * @param tempFilePath
     * @param uniqueFileName
     * @return
     */
    public static File createZipFile(String tempFilePath,String uniqueFileName){
        File file = new File(tempFilePath+uniqueFileName+".zip");
        if (!file.getParentFile().exists()) {
            file.getParentFile().mkdirs();
        }
        return file;
    }
}
