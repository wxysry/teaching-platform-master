package com.xiyou.common.office.utils;

import com.xiyou.common.utils.CommonUtil;
import org.apache.commons.io.FileUtils;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.rendering.PDFRenderer;
import org.artofsolving.jodconverter.OfficeDocumentConverter;
import org.artofsolving.jodconverter.office.DefaultOfficeManagerConfiguration;
import org.artofsolving.jodconverter.office.OfficeManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.*;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.regex.Pattern;

/**
 * @program: multi-module
 * @description: ppt转图片工具
 * @author: tangcan
 * @create: 2019-07-05 19:52
 **/
public class PPTUtil {
    private static Logger logger = LoggerFactory.getLogger(PPTUtil.class);

    /**
     * 打开libreOffice服务的方法
     */
    public static String getLibreOfficeHome() {
        String osName = System.getProperty("os.name");

        if (Pattern.matches("Linux.*", osName)) {
            //获取linux系统下libreoffice主程序的位置
            logger.info("获取Linux系统LibreOffice路径");
            return "/usr/lib/libreoffice";
        } else if (Pattern.matches("Windows.*", osName)) {
            //获取windows系统下libreoffice主程序的位置
            logger.info("获取windows系统LibreOffice路径");
            return CommonUtil.getCatalinaHome() + File.separator + "LibreOffice";
        }
        return null;
    }

    /**
     * 转换libreoffice支持的文件为pdf
     */
    public static void libreOffice2PDF(File inputfile, File outputfile) {
        String LibreOffice_HOME = getLibreOfficeHome();
        if (LibreOffice_HOME == null) {
            return;
        }
        String fileName = inputfile.getName();
        logger.info(new SimpleDateFormat("yyyy-MM-dd HH-mm-ss").format(new Date()) + "文件" + inputfile.getName());
        if (fileName.substring(fileName.lastIndexOf(".")).equalsIgnoreCase(".txt")) {
            TXTHandler(inputfile);
        }
        DefaultOfficeManagerConfiguration configuration = new DefaultOfficeManagerConfiguration();
        // libreOffice的安装目录
        configuration.setOfficeHome(new File(LibreOffice_HOME));
        // 端口号
        configuration.setPortNumber(8100);
        configuration.setTaskExecutionTimeout(1000 * 60 * 25L);
//         设置任务执行超时为10分钟
        configuration.setTaskQueueTimeout(1000 * 60 * 60 * 24L);
//         设置任务队列超时为24小时
        OfficeManager officeManager = configuration.buildOfficeManager();
        officeManager.start();
        logger.info(new Date().toString() + "开始转换......");
        OfficeDocumentConverter converter = new OfficeDocumentConverter(officeManager);
        converter.getFormatRegistry();
        try {
            converter.convert(inputfile, outputfile);
        } catch (Exception e) {
            e.printStackTrace();
            logger.info("转换失败");
        } finally {
            officeManager.stop();
        }
        logger.info(new Date().toString() + "转换结束....");
    }

    /**
     * 转换txt文件编码的方法
     */
    public static void TXTHandler(File file) {
        //或GBK
        String code = "gb2312";
        byte[] head = new byte[3];
        try {
            InputStream inputStream = new FileInputStream(file);
            inputStream.read(head);
            if (head[0] == -1 && head[1] == -2) {
                code = "UTF-16";
            } else if (head[0] == -2 && head[1] == -1) {
                code = "Unicode";
            } else if (head[0] == -17 && head[1] == -69 && head[2] == -65) {
                code = "UTF-8";
            }
            inputStream.close();

            if (code.equals("UTF-8")) {
                return;
            }
            String str = FileUtils.readFileToString(file, code);
            FileUtils.writeStringToFile(file, str, "UTF-8");
            logger.info("转码结束");
        } catch (IOException e) {
            e.printStackTrace();
        }

    }


    public static int pDfToPictures(String pdfSourceName, String picTargetName) {
        File file = new File(pdfSourceName);
        int pageCount = 0;
        try {
            PDDocument doc = PDDocument.load(file);
            PDFRenderer renderer = new PDFRenderer(doc);
            pageCount = doc.getNumberOfPages();
            for (int i = 0; i < pageCount; i++) {
                BufferedImage image = renderer.renderImageWithDPI(i, 150);
                //BufferedImage image = renderer.renderImage(i, 2.5f);
                File newFile = new File(picTargetName + (i + 1) + ".png");
                if (!newFile.getParentFile().exists()) {
                    newFile.getParentFile().mkdirs();
                }
                ImageIO.write(image, "PNG", newFile);
            }
            return pageCount;
        } catch (IOException e) {
            e.printStackTrace();
            return pageCount;
        }
    }

    /**
     * 先把PPT转化为pdf,在把pdf转成图片
     *
     * @param pptSourceName ppt文件名
     * @param pdfName       pdf文件名
     * @param targetPath    输出图片路径
     */
    public static int pptToPictures(String pptSourceName, String pdfName, String targetPath) {
        File f = new File(pptSourceName);
        File pdfFile = new File(pdfName);
        if (!pdfFile.getParentFile().exists()) {
            pdfFile.getParentFile().mkdirs();
        }
        libreOffice2PDF(f, new File(pdfName));
        return pDfToPictures(pdfName, targetPath);
    }

}
