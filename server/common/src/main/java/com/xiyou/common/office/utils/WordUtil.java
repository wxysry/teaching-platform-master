package com.xiyou.common.office.utils;

import cn.afterturn.easypoi.word.WordExportUtil;
import org.apache.poi.xwpf.usermodel.XWPFDocument;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Map;

/**
 * @program: multi-module
 * @description: word工具
 * @author: tangcan
 * @create: 2019-07-18 20:08
 **/
public class WordUtil {
    public static String exportWordTemplateToFile(String templateFilePath, String saveFilePath, String saveFileName, Map<String, Object> paramMap) {
        File file = new File(saveFilePath + "temp.docx");
        if (!file.getParentFile().exists()) {
            file.getParentFile().mkdirs();
        }
        try {
            file.createNewFile();
        } catch (IOException e) {
            e.printStackTrace();
        }
        try (FileOutputStream os = new FileOutputStream(file)) {
            XWPFDocument document = WordExportUtil.exportWord07(templateFilePath, paramMap);
            document.write(os);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return file.getAbsolutePath();
    }
}
