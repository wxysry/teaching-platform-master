package com.xiyou.common.redis.config;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.core.*;
import org.springframework.data.redis.listener.RedisMessageListenerContainer;
import org.springframework.data.redis.serializer.StringRedisSerializer;

@Configuration
public class RedisConfig {

    @Autowired
    private RedisConnectionFactory factory;

    @Bean
    public RedisTemplate<String, Object> redisTemplate() {

        RedisTemplate<String, Object> redisTemplate = new RedisTemplate<>();
        redisTemplate.setConnectionFactory(factory);

        // Jackson2JsonRedisSerializer jackson2JsonRedisSerializer = new Jackson2JsonRedisSerializer(Object.class);
        // ObjectMapper om = new ObjectMapper();
        // om.setVisibility(PropertyAccessor.ALL, JsonAutoDetect.Visibility.ANY);
        // om.enableDefaultTyping(ObjectMapper.DefaultTyping.NON_FINAL);
        // jackson2JsonRedisSerializer.setObjectMapper(om);
        StringRedisSerializer stringRedisSerializer = new StringRedisSerializer();
        //序列化设置 ，这样计算是正常显示的数据，也能正常存储和获取
        redisTemplate.setKeySerializer(stringRedisSerializer);
        // redisTemplate.setValueSerializer(jackson2JsonRedisSerializer);
        redisTemplate.setHashKeySerializer(stringRedisSerializer);
        // redisTemplate.setHashValueSerializer(jackson2JsonRedisSerializer);

        return redisTemplate;

    }


    @Bean
    public RedisMessageListenerContainer container(RedisConnectionFactory connectionFactory) {
        RedisMessageListenerContainer container = new RedisMessageListenerContainer();
        container.setConnectionFactory(connectionFactory);
        return container;
    }


    // @Bean
    // public HashOperations<String, String, Object> hashOperations(RedisTemplate<String, Object> redisTemplate) {
    //     return redisTemplate.opsForHash();
    // }
    //
    // @Bean
    // public ValueOperations<String, String> valueOperations(RedisTemplate<String, String> redisTemplate) {
    //     return redisTemplate.opsForValue();
    // }
    //
    // @Bean
    // public ListOperations<String, Object> listOperations(RedisTemplate<String, Object> redisTemplate) {
    //     return redisTemplate.opsForList();
    // }
    //
    // @Bean
    // public SetOperations<String, Object> setOperations(RedisTemplate<String, Object> redisTemplate) {
    //     return redisTemplate.opsForSet();
    // }
    //
    // @Bean
    // public ZSetOperations<String, Object> zSetOperations(RedisTemplate<String, Object> redisTemplate) {
    //     return redisTemplate.opsForZSet();
    // }

}
