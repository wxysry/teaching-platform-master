package com.xiyou.common.redis.utils;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.connection.Message;
import org.springframework.data.redis.connection.MessageListener;
import org.springframework.data.redis.listener.PatternTopic;
import org.springframework.stereotype.Component;

/**
 * @author 吕辉耀
 * @PROJECT_NAME: teaching-platform-master-new
 * @Description:
 * @creat 2023-07-23 17
 */
@Component
public class RedisExpiredListener implements MessageListener {
    /**Redis 并不保证生存时间（TTL）变为 0 的键会立即被删除： 如果程序没有访问这个过期键， 或者带有生存时间的键非常多的话，
     * 那么在键的生存时间变为 0 ， 直到键真正被删除这中间， 可能会有一段比较显著的时间间隔。
     */
    @Autowired
    private RedisCache redisCache;
    //监听主题
    private final PatternTopic topic = new PatternTopic("__keyevent@*__:expired");
    @Override
    public void onMessage(Message message, byte[] bytes) {
        String key = new String(message.getBody());
        if("tradeFair1".equals(key) || "tradeFair2".equals(key)){
            Object o = redisCache.get(key);
            //TODO  解析value值 执行下个学生开始选单 执行完后重新写一个value进去

        }
    }
}
