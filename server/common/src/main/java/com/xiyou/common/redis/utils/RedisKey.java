package com.xiyou.common.redis.utils;

/**
 * @program: multi-module
 * @description: RedisKey的设计
 * @author: tangcan
 * @create: 2019-06-17 20:09
 **/
public class RedisKey {

    /**
     * @Author: tangcan
     * @Description: 根据类型和值定义key
     * @Param: [type, value]
     * @date: 2019/6/28
     */
    public static String get(String type, String value) {
        return type + ":" + value;
    }

    /**
     * redis的key
     * 形式为：
     * 表名:主键名:主键值:列名
     *
     * @param tableName     表名
     * @param majorKey      主键名
     * @param majorKeyValue 主键值
     * @param column        列名
     * @return
     */
    public static String get(String tableName, String majorKey, String majorKeyValue, String column) {
        StringBuffer buffer = new StringBuffer();
        buffer.append(tableName).append(":");
        buffer.append(majorKey).append(":");
        buffer.append(majorKeyValue).append(":");
        buffer.append(column);
        return buffer.toString();
    }

    /**
     * redis的key
     * 形式为：
     * 表名:主键名:主键值
     *
     * @param tableName     表名
     * @param majorKey      主键名
     * @param majorKeyValue 主键值
     * @return
     */
    public static String get(String tableName, String majorKey, String majorKeyValue) {
        String buffer = tableName + ":" +
                majorKey + ":" +
                majorKeyValue;
        return buffer;
    }
}
