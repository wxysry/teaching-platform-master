package com.xiyou.common.shiro.service;

import com.alibaba.fastjson.JSONObject;
import com.xiyou.common.redis.utils.RedisCache;
import com.xiyou.common.shiro.jwt.JWTToken;
import com.xiyou.common.shiro.jwt.JWTUtil;
import com.xiyou.common.redis.utils.RedisKey;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @program: attendance
 * @description: JWTTokenService实现类
 * @author: tangcan
 * @create: 2018-12-03 15:00
 **/
@Component
@Slf4j
public class JWTTokenService {
    @Autowired
    private RedisCache redisCache;

    public JWTToken get(String token) {
        return (JWTToken) redisCache.get(RedisKey.get("token", token));
    }

    public void update(JWTToken jwtToken) {
        String userIdKey = RedisKey.get("user_id", String.valueOf(jwtToken.getUserId()));
        redisCache.set(userIdKey, jwtToken.getToken(), JWTUtil.ACCESS_TIME_SECOND);
        redisCache.set(getKey(jwtToken), jwtToken, JWTUtil.ACCESS_TIME_SECOND);
        redisCache.sSet(RedisKey.get("user_token", String.valueOf(jwtToken.getUserId())),jwtToken.getToken(),JWTUtil.ACCESS_TIME_SECOND);
    }

    public void remove(JWTToken jwtToken) {
        if (jwtToken == null) {
            return;
        }
        redisCache.delete(RedisKey.get("user_id", String.valueOf(jwtToken.getUserId())));

        //删除 sys_user_info
        String redisKey = RedisKey.get("sys_user_info", "user_id", String.valueOf(jwtToken.getUserId()));
        redisCache.delete(redisKey);

        deleteAllCache(jwtToken.getToken());
    }

    public void save(JWTToken jwtToken) {
        // 限制单点登录
        String userIdKey = RedisKey.get("user_id", String.valueOf(jwtToken.getUserId()));
//        String oldToken = (String) redisCache.get(userIdKey);
//        if (oldToken != null) {
//            log.error("删除老的token");
//            deleteAllCache(oldToken);
//        }
        log.error("保存新的token,{},value:{}",userIdKey,jwtToken.getToken());
        redisCache.set(userIdKey, jwtToken.getToken(), JWTUtil.ACCESS_TIME_SECOND);
        redisCache.set(getKey(jwtToken), jwtToken, JWTUtil.ACCESS_TIME_SECOND);
        log.error("保存新的token,{},value:{}",getKey(jwtToken), JSONObject.toJSONString(jwtToken));
        redisCache.sSet(RedisKey.get("user_token", String.valueOf(jwtToken.getUserId())),jwtToken.getToken(),JWTUtil.ACCESS_TIME_SECOND);
    }

    /*
    删除所有相关的缓存
    */
    private void deleteAllCache(String token) {
        if (StringUtils.isBlank(token)) {
            return;
        }
        String oldTokenKey = RedisKey.get("token", token);
        log.error("删除token缓存,tokenKey:{}",oldTokenKey);
        redisCache.delete(oldTokenKey);
        // 取消shiro的缓存记录
        // "shiro:cache:com.xiyou.main.shiro.ShiroRealm.authorizationCache:eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9"
        // "shiro:cache:authenticationCache:JWTToken(token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9, activeTime=null, userId=null, account=null, name=null)"
        redisCache.delete("shiro:cache:com.xiyou.main.shiro.ShiroRealm.authorizationCache:" + token);
        redisCache.delete("shiro:cache:authenticationCache:" + new JWTToken().setToken(token));
    }

    private String getKey(JWTToken jwtToken) {
        return RedisKey.get("token", jwtToken.getToken());
    }
}
