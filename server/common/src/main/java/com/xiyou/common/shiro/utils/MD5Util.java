package com.xiyou.common.shiro.utils;

import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.crypto.hash.SimpleHash;
import org.apache.shiro.util.ByteSource;

/**
 * MD5加密
 */
public class MD5Util {

    private static final String SALT = "@swust_acm_306";

    private static final String ALGORITH_NAME = "md5";

    private static final int HASH_ITERATIONS = 2;

    public static String encrypt(String username, String password) {
        return new SimpleHash(ALGORITH_NAME, password, ByteSource.Util.bytes(username + SALT),
                HASH_ITERATIONS).toHex();
    }

    public static String encrypt(String password) {
        return new SimpleHash(ALGORITH_NAME, password, ByteSource.Util.bytes(SALT),
                HASH_ITERATIONS).toHex();
    }


    public static boolean verifyPwd(String username, String password, String encryptPwd) {
        if (StringUtils.isBlank(password) || StringUtils.isBlank(encryptPwd)) {
            return false;
        }
        return encryptPwd.equals(encrypt(username, password));
    }

    /**
     * @Author: tangcan
     * @Description: 计算密码加密值
     * @Param: [args]
     * @date: 2019/3/28
     */
    public static void main(String[] args) {
        System.out.println(encrypt("chuangdao", "123456"));
    }
}
