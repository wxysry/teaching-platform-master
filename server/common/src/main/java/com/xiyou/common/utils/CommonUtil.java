package com.xiyou.common.utils;

import com.xiyou.common.shiro.utils.MD5Util;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.util.*;

/**
 * @description: 公用方法
 * @author: tangcan
 * @create: 2019-04-25 21:05
 **/
public class CommonUtil {
    private static Logger log = LoggerFactory.getLogger(CommonUtil.class);

    public static boolean checkPageAndLimit(Integer page, Integer limit) {
        if (page != null && limit != null && page > 0 && limit > 0) {
            return true;
        }
        return false;
    }

    /**
     * @Author: tangcan
     * @Description: 计算Map的初始容量
     * @Param: [size]
     * @date: 2019/1/4
     */
    public static int getMapInitCap(int size) {
        int cap = (int) (size / 0.75 + 1);
        int x = (int) (Math.log(cap) / Math.log(2));
        return 1 << x;
    }

    /**
     * @Author: tangcan
     * @Description: 计算Set的初始容量
     * @Param: [size]
     * @date: 2019/1/4
     */
    public static int getSetInitCap(int size) {
        int cap = (int) ((double) size / 0.75);
        int x = (int) (Math.log(cap) / Math.log(2));
        return 1 << x;
    }

    /**
     * @Author: tangcan
     * @Description: 计算List的初始容量
     * @Param: [size]
     * @date: 2019/1/4
     */
    public static int getListInitCap(int size) {
        int cap = (int) ((double) size * 3.0 / 2.0);
        int x = (int) (Math.log(cap) / Math.log(2));
        return 1 << x;
    }

    public static String getClassesPath() {
        String os = System.getProperty("os.name");
        String path = Thread.currentThread().getContextClassLoader().getResource(".").getPath();
        try {
            path = java.net.URLDecoder.decode(path, "utf-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        if (os.toLowerCase().startsWith("win")) {
            /*
            去掉前面的 /
             */
            return path.substring(1);
        }
        return path;
    }

    public static String getCatalinaHome() {

        // 获取tomcat所在路径
        String os = System.getProperty("os.name");
        if (os.toLowerCase().startsWith("win")) {
            return System.getProperty("catalina.home") + File.separator;
        }
        return "";
    }

    public static String getUniqueFileName(String fileName) {
        String newName = DateUtil.getCurrentTime() + " " + MD5Util.encrypt(fileName);
        newName = newName.replaceAll(":", "-");
        return newName;
    }

}
