package com.xiyou.common.utils;


import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @description:
 * @author: tangcan
 * @create: 2019-04-26 19:20
 **/
public class DateUtil {

    private static String ymdhms = "yyyy-MM-dd HH:mm:ss";
    private static String ymd = "yyyy-MM-dd";
    public static SimpleDateFormat ymdSDF = new SimpleDateFormat(ymd);
    public static SimpleDateFormat yyyyMMddHHmmss = new SimpleDateFormat(ymdhms);
    public static SimpleDateFormat yyyyMMddHHmm = new SimpleDateFormat("yyyy-MM-dd HH:mm");
    public static SimpleDateFormat yyyyMMdd = new SimpleDateFormat("yyyy-MM-dd");

    /**
     * s
     *
     * @Author: tangcan
     * @Description: 格式化时间
     * @Param: [date, format]
     * @date: 2019/6/18
     */
    public static String format(Date date, String format) {
        if (date == null) {
            return "";
        }
        SimpleDateFormat df = new SimpleDateFormat(format);
        return df.format(date);
    }

    /**
     * 获得当前时间(字符串类型)
     * 格式：2014-12-02 10:38:53
     *
     * @return String
     */
    public static String getCurrentTime() {
        return yyyyMMddHHmmss.format(new Date());
    }

}
