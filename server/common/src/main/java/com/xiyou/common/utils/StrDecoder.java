package com.xiyou.common.utils;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

/**
 * @program: multi-module
 * @description:
 * @author: tangcan
 * @create: 2019-11-15 15:25
 **/
public class StrDecoder {
    /*
    将字符串解码成key value形式存入map中
    如：a=123&b=232 或a=123;b=342
     */
    public static Map<String, String> str2ParamMap(String str, String separator) {
        Map<String, String> parameters = new HashMap<>();
        if (str == null) {
            return parameters;
        }
        str = str.replaceAll(" ", "");
        Arrays.stream(str.split(separator))
                .filter(p -> p.contains("="))
                .map(p -> p.split("="))
                .forEach(arr -> parameters.put(arr[0], arr[1]));
        return parameters;
    }
}
