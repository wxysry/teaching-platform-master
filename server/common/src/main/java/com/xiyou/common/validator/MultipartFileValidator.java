package com.xiyou.common.validator;

import com.xiyou.common.annotations.ValidFile;
import org.apache.shiro.util.Assert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.web.servlet.MultipartProperties;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

/**
 * @program: multi-module
 * @description: 文件验证器
 * @author: tangcan
 * @create: 2019-06-18 14:51
 **/
public class MultipartFileValidator implements ConstraintValidator<ValidFile, MultipartFile> {

    private long minSize = 0;
    private long maxSize = -1;

    private ValidFile validFile;

    private String[] file;

    @Override
    public void initialize(ValidFile constraintAnnotation) {
        this.validFile = constraintAnnotation;
        this.file = validFile.file();
        if (ValidFile.DEFAULT_MAXSIZE.equals(constraintAnnotation.maxSize())) {
            this.maxSize = -1;
        } else {
            this.maxSize = parseSize(constraintAnnotation.maxSize());
        }
        //文件上传的最小值
        this.minSize = parseSize(constraintAnnotation.minSize());
    }


    @Override
    public boolean isValid(MultipartFile multipartFile, ConstraintValidatorContext cvc) {
        String fieldName = multipartFile.getName();
        if (multipartFile.isEmpty()) {
            if (validFile.allowEmpty()) {
                return true;
            }
            setMessage("上传文件不能为空,参数名：" + fieldName, cvc);
            return false;
        }

        String fileName = multipartFile.getOriginalFilename();
        if (fileName == null) {
            setMessage("文件上传错误,参数名：" + fieldName, cvc);
            return false;
        }
        if (validFile.ignoreCase()) {
            fileName = fileName.toLowerCase();
        }

        // 文件后缀
        String suffixName = fileName.substring(fileName.lastIndexOf(".") + 1);
        boolean correct = false;
        StringBuilder buffer = new StringBuilder();
        for (int i = 0; i < file.length; i++) {
            if (file[i].equals(suffixName)) {
                correct = true;
                break;
            }
            if (i == 0) {
                buffer.append(file[i]);
            } else {
                buffer.append(",").append(file[i]);
            }
        }
        if (!correct) {
            setMessage("只能上传" + buffer.toString() + "格式的文件", cvc);
            return false;
        }
        //上传文件字节数
        long size = multipartFile.getSize() / 1024;
        if (size < this.minSize) {
            setMessage("文件不能小于" + (int) (minSize / 1024) + "KB", cvc);
            return false;
        }
        if (maxSize != -1 && size > this.maxSize) {
            setMessage("文件不能大于" + (int) (maxSize / 1024) + "KB", cvc);
            return false;
        }

        return true;
    }

    /**
     * @Author: tangcan
     * @Description: 验证错误信息提示
     * @Param: [message, cvc]
     * @date: 2019/6/18
     */
    private void setMessage(String message, ConstraintValidatorContext cvc) {
        cvc.disableDefaultConstraintViolation();
        cvc.buildConstraintViolationWithTemplate(message)
                .addConstraintViolation();
    }

    /**
     * @Author: tangcan
     * @Description: 提取文件大小
     * @Param: [size]
     * @date: 2019/6/18
     */
    private long parseSize(String size) {
        Assert.hasLength(size, "Size must not be empty");
        size = size.toUpperCase();
        int length = size.length();
        Assert.isTrue(2 <= length, "Size is wrong");
        /*
        单位为B的情况
         */
        if (size.charAt(length - 2) >= '0' && size.charAt(length - 2) <= '9') {
            return Long.valueOf(size.substring(0, length - 1));
        }
        String substring = size.substring(0, length - 2);
        if (size.endsWith("KB")) {
            return Long.valueOf(substring) * 1024;
        }
        if (size.endsWith("MB")) {
            return Long.valueOf(substring) * 1024 * 1024;
        }
        return Long.valueOf(size);
    }

}
