package com.xiyou.main;

import com.baomidou.mybatisplus.annotation.DbType;
import com.baomidou.mybatisplus.generator.AutoGenerator;
import com.baomidou.mybatisplus.generator.config.DataSourceConfig;
import com.baomidou.mybatisplus.generator.config.GlobalConfig;
import com.baomidou.mybatisplus.generator.config.PackageConfig;
import com.baomidou.mybatisplus.generator.config.StrategyConfig;
import com.baomidou.mybatisplus.generator.config.rules.NamingStrategy;

/**
 * @program: attendance
 * @description: Mybatis Plus代码生成器
 * 禁止执行！！！！！！！！！！！！！！
 * @author: tangcan
 * @create: 2018-11-27 15:08
 **/
public class MPGeneration {
    public static void main(String[] args) {
        AutoGenerator mpg = new AutoGenerator();

        // 全局配置
        GlobalConfig gc = new GlobalConfig();
        gc.setSwagger2(true);
        gc.setOutputDir("C://Users//wangxy//Desktop//teaching-platform-master//server//main//src//main//java");
        gc.setFileOverride(false);
        gc.setActiveRecord(true);// 不需要ActiveRecord特性的请改为false
        gc.setEnableCache(false);// XML 二级缓存
        gc.setBaseResultMap(true);// XML ResultMap
        gc.setBaseColumnList(false);// XML columList
        gc.setAuthor("wangxingyu");// 作者

        // 自定义文件命名，注意 %s 会自动填充表实体属性！
        gc.setControllerName("%sController");
        gc.setServiceName("%sService");
        gc.setServiceImplName("%sServiceImpl");
        gc.setMapperName("%sMapper");
        gc.setXmlName("%sMapper");
        mpg.setGlobalConfig(gc);

        // 数据源配置
        DataSourceConfig dsc = new DataSourceConfig();
        dsc.setDbType(DbType.MYSQL);
        dsc.setDriverName("com.mysql.cj.jdbc.Driver");
        dsc.setUsername("teaching_platform");
        dsc.setPassword("@swust_acm_306");
        dsc.setUrl("jdbc:mysql://localhost:33061/teaching_platform?useSSL=false&useUnicode=true&characterEncoding=utf-8&serverTimezone=Asia/Shanghai&allowMultiQueries=true&rewriteBatchedStatements=true");
        mpg.setDataSource(dsc);

        // 策略配置
        StrategyConfig strategy = new StrategyConfig();
        // strategy.setTablePrefix(new String[]{"mmall_"});// 此处可以修改为您的表前缀
        strategy.setNaming(NamingStrategy.underline_to_camel);// 表名生成策略
        strategy.setInclude("ngb_ct_member_index"); // 需要生成的表

        strategy.setSuperControllerClass("com.xiyou.common.controller.BaseController");
        strategy.setRestControllerStyle(true);
        strategy.setEntityLombokModel(true);
        strategy.setSuperServiceClass(null);
        strategy.setSuperServiceImplClass(null);
        strategy.setSuperMapperClass(null);

        mpg.setStrategy(strategy);

        // 包配置
        PackageConfig pc = new PackageConfig();
        pc.setParent("com.xiyou.main");
        pc.setController("controller.ngbcontest");
        pc.setService("service.ngbcontest");
        pc.setServiceImpl("service.ngbcontest.impl");
        pc.setMapper("dao.ngbcontest");
        pc.setEntity("entity.ngbcontest");
        pc.setXml("mapper.ngbcontest");
        mpg.setPackageInfo(pc);

        // 执行生成
        mpg.execute();
    }
}
