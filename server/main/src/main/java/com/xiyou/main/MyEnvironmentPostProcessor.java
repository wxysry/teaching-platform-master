package com.xiyou.main;

import org.springframework.beans.factory.config.YamlPropertiesFactoryBean;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.env.EnvironmentPostProcessor;
import org.springframework.core.env.ConfigurableEnvironment;
import org.springframework.core.env.MutablePropertySources;
import org.springframework.core.env.PropertiesPropertySource;
import org.springframework.core.io.FileSystemResource;

import java.io.File;

/**
 * 如果存在外部配置YML文件，则加载外部配置YML文件
 */
public class MyEnvironmentPostProcessor implements EnvironmentPostProcessor {
    /**
     * 外部 yml 配置文件名
     */
    String YAML_NAME = "application-out.yml";

    @Override
    public void postProcessEnvironment(ConfigurableEnvironment environment, SpringApplication application) {
        // 获取tomcat路径
        String tomcat = System.getProperty("catalina.home");
        String path = tomcat + File.separator + "conf" + File.separator + YAML_NAME;
        // 读取外部 yml 配置
        FileSystemResource fsr = new FileSystemResource(path);
        if (fsr.exists()) {
            YamlPropertiesFactoryBean yaml = new YamlPropertiesFactoryBean();
            yaml.setResources(fsr);
            MutablePropertySources propertySources = environment.getPropertySources();
            // 优先外部YML配置
            propertySources.addFirst(new PropertiesPropertySource("Config", yaml.getObject()));
        }
    }
}
