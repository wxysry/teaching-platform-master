package com.xiyou.main.async.contest;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.xiyou.main.dao.contest.*;
import com.xiyou.main.entity.contest.*;
import com.xiyou.main.entity.exam.SysUser;
import com.xiyou.main.entity.exam.VideoGroup;
import com.xiyou.main.service.contest.CtMnAdService;
import com.xiyou.main.service.exam.SysUserService;
import com.xiyou.main.vo.contest.MaterialVo;
import com.xiyou.main.vo.contest.ProductVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.AsyncResult;
import org.springframework.stereotype.Service;

import java.lang.reflect.Field;
import java.util.*;
import java.util.concurrent.Future;
import java.util.stream.Collectors;

/**
 * @program: multi-module
 * @description: 学生竞赛结果的数据处理
 * @author: tangcan
 * @create: 2019-09-02 17:58
 **/
@Service
public class ContestResultAsyncService {
    @Autowired
    @Lazy
    private CtDdMaterialMapper ctDdMaterialMapper;
    @Autowired
    @Lazy
    private CtKcMaterialMapper ctKcMaterialMapper;
    @Autowired
    @Lazy
    private CtKcProductMapper ctKcProductMapper;
    @Autowired
    @Lazy
    private CtFeeMapper ctFeeMapper;
    @Autowired
    @Lazy
    private CtBankLoanMapper ctBankLoanMapper;
    @Autowired
    @Lazy
    private CtCashflowMapper cashflowMapper;
    @Autowired
    @Lazy
    private CtYfMarketMapper ctYfMarketMapper;
    @Autowired
    @Lazy
    private CtYfIsoMapper ctYfIsoMapper;
    @Autowired
    @Lazy
    private CtYfProductMapper ctYfProductMapper;
    @Autowired
    @Lazy
    private CtWorkshopMapper ctWorkshopMapper;
    @Autowired
    @Lazy
    private CtLineMapper ctLineMapper;
    @Autowired
    @Lazy
    private CtXzOrderMapper ctXzOrderMapper;
    @Autowired
    @Lazy
    private CtGzIsoMapper ctGzIsoMapper;
    @Autowired
    @Lazy
    private CtBalanceMapper ctBalanceMapper;
    @Autowired
    @Lazy
    private CtChargesMapper ctChargesMapper;
    @Autowired
    @Lazy
    private CtProfitChartMapper ctProfitChartMapper;
    @Autowired
    @Lazy
    private SysUserService sysUserService;
    @Autowired
    @Lazy
    private CtMnAdService ctMnAdService;


    /*
    库存信息
     */
    @Async
    public Future<Map<String, Object>> getKCXX(Integer contestId, Integer studentId) {
        Map<String, Object> kcxx = new HashMap<>();
        // 原料订购
        List<Map<String, Object>> yldg = new ArrayList<>();
        List<CtDdMaterial> ddMaterialList = ctDdMaterialMapper.list(contestId, studentId);
        for (CtDdMaterial ddMaterial : ddMaterialList) {
            Map<String, Object> map = new HashMap<>();
            map.put("mc", ddMaterial.getCmName());
            map.put("sl", ddMaterial.getOmNum());
            map.put("sysj", ddMaterial.getRemainDate());
            map.put("dgsj", getDate(ddMaterial.getPurchaseDate()));
            yldg.add(map);
        }
        kcxx.put("yldg", yldg);
        // 原料库存
        List<Map<String, Object>> ylkc = new ArrayList<>();
        List<MaterialVo> kcMaterialList = ctKcMaterialMapper.listKc(contestId, studentId);
        for (MaterialVo materialVo : kcMaterialList) {
            Map<String, Object> map = new HashMap<>();
            map.put("mc", materialVo.getMaterialName());
            map.put("sl", materialVo.getImNum());
            ylkc.add(map);
        }
        kcxx.put("ylkc", ylkc);
        // 产品库存
        List<Map<String, Object>> cpkc = new ArrayList<>();
        List<ProductVo> kcProductList = ctKcProductMapper.listKc(contestId, studentId);
        for (ProductVo productVo : kcProductList) {
            Map<String, Object> map = new HashMap<>();
            map.put("mc", productVo.getProductName());
            map.put("sl", productVo.getIpNum());
            cpkc.add(map);
        }
        kcxx.put("cpkc", cpkc);
        return new AsyncResult<>(kcxx);
    }

    /*
    银行贷款
     */
    @Async
    public Future<Map<String, Object>> getYHDK(Integer contestId, Integer studentId, Integer date) {
        Map<String, Object> yhdk = new HashMap<>();
        // 应收款
        List<Map<String, Object>> ysk = new ArrayList<>();
        List<CtFee> ctFeeList = ctFeeMapper.getList(contestId, studentId);
        for (CtFee ctFee : ctFeeList) {
            Map<String, Object> map = new HashMap<>();
            map.put("syzq", ctFee.getRRemainDate() + "季");
            map.put("je", ctFee.getRFee() + "W");
            ysk.add(map);
        }
        yhdk.put("ysk", ysk);
        // 长期贷款
        List<Map<String, Object>> cqdk = new ArrayList<>();
        // 短期贷款
        List<Map<String, Object>> dqdk = new ArrayList<>();
        List<CtBankLoan> bankLoanList = ctBankLoanMapper.getList(contestId, studentId);
        for (CtBankLoan bankLoan : bankLoanList) {
            Map<String, Object> map = new HashMap<>();
            map.put("je", bankLoan.getBlFee());
            map.put("dksj", getDate(bankLoan.getBlAddTime()));
            if (bankLoan.getBlType() == 1) {
                // 长贷：计算剩余时间
                if (bankLoan.getBlRepaymentDate() == null || date == null) {
                    map.put("sysj", "-");
                } else {
                    int year = bankLoan.getBlRepaymentDate() / 10 - date / 10;
                    if (year < 0) year = 0;
                    map.put("sysj", "" + year + "年");
                }
                cqdk.add(map);
            } else {
                // 短贷：计算剩余时间
                if (bankLoan.getBlRepaymentDate() == null || date == null) {
                    map.put("sysj", "-");
                } else {
                    int quarter = bankLoan.getBlRepaymentDate() % 10 - date % 10;
                    if (quarter < 0 || ((bankLoan.getBlRepaymentDate() / 10) < (date / 10))) quarter = 0;
                    map.put("sysj", "" + quarter + "季");
                }
                dqdk.add(map);
            }
        }
        yhdk.put("cqdk", cqdk);
        yhdk.put("dqdk", dqdk);
        return new AsyncResult<>(yhdk);
    }

    /*
    研发认证
     */
    @Async
    public Future<Map<String, Object>> getYFRZ(Integer contestId, Integer studentId) {
        Map<String, Object> yfrz = new HashMap<>();
        // 市场开拓
        List<Map<String, Object>> sckt = new ArrayList<>();
        List<CtYfMarket> ctYfMarketList = ctYfMarketMapper.getList(contestId, studentId);
        for (CtYfMarket ctYfMarket : ctYfMarketList) {
            Map<String, Object> map = new HashMap<>();
            map.put("mc", ctYfMarket.getCmName());
            map.put("ktf", ctYfMarket.getCmDevelopFee() + "W");
            map.put("zq", ctYfMarket.getCmDevelopDate() + "年");
            map.put("sysj", format(ctYfMarket.getDmRemainDate()));
            map.put("wcsj", getDate(ctYfMarket.getDmFinishDate()));
            sckt.add(map);
        }
        yfrz.put("sckt", sckt);
        // 产品研发
        List<Map<String, Object>> cpyf = new ArrayList<>();
        List<CtYfProduct> ctYfProductList = ctYfProductMapper.getList(contestId, studentId);
        for (CtYfProduct ctYfProduct : ctYfProductList) {
            Map<String, Object> map = new HashMap<>();
            map.put("mc", ctYfProduct.getCpName());
            map.put("yff", ctYfProduct.getCpProcessingFee() + "W");
            map.put("zq", ctYfProduct.getCpDevelopDate() + "季");
            map.put("sysj", format(ctYfProduct.getDpRemainDate()));
            map.put("wcsj", getDate(ctYfProduct.getDpFinishDate()));
            cpyf.add(map);
        }
        yfrz.put("cpyf", cpyf);
        // ISO认证
        List<Map<String, Object>> isorz = new ArrayList<>();
        List<CtYfIso> ctYfIsoList = ctYfIsoMapper.getList(contestId, studentId);
        for (CtYfIso ctYfIso : ctYfIsoList) {
            Map<String, Object> map = new HashMap<>();
            map.put("mc", ctYfIso.getCiName());
            map.put("yff", ctYfIso.getCiDevelopFee() + "W");
            map.put("zq", ctYfIso.getCiDevelopDate() + "年");
            map.put("sysj", format(ctYfIso.getDiRemainDate()));
            map.put("wcsj", getDate(ctYfIso.getDiFinishDate()));
            isorz.add(map);
        }
        yfrz.put("isorz", isorz);
        return new AsyncResult<>(yfrz);
    }

    /*
    现金流量表
     */
    @Async
    public Future<Map<String, Object>> getXJLLB(Integer contestId, Integer studentId) {
        // 获取所有现金流量数据
        List<CtCashflow> cashflowList = cashflowMapper.getList(contestId, studentId);
        Map<String, Object> xjllb = new HashMap<>();
        List<Map<String, Object>> mapList = new ArrayList<>();
        int id = 0;
        for (CtCashflow ctCashflow : cashflowList) {
            Map<String, Object> map = new HashMap<>();
            map.put("id", ++id);
            map.put("dz", ctCashflow.getCAction());
            map.put("zj", ctCashflow.getCIn() - ctCashflow.getCOut());
            map.put("ye", ctCashflow.getCSurplus());
            map.put("sj", getDate(ctCashflow.getCDate()));
            map.put("bz", ctCashflow.getCComment());
            mapList.add(map);
        }
        xjllb.put("xjllb", mapList);
        return new AsyncResult<>(xjllb);
    }

    private String format(Integer number) {
        if (number == null) {
            return "-";
        }
        return String.valueOf(number);
    }

    private String getDate(Integer date) {
        if (date == null || date == 0) {
            return "-";
        }
        return "第" + date / 10 + "年" + date % 10 + "季";
    }

    /*
    厂房与生产线
     */
    public Future<Map<String, Object>> getCFYSCX(Integer contestId, Integer studentId) {
        Map<String, Object> returnMap = new HashMap<>();
        // 厂房信息
        List<CtWorkshop> ctWorkshopList = ctWorkshopMapper.getList(contestId, studentId);
        List<Map<String, Object>> cfxx = new ArrayList<>();
        Map<String, Object> map;
        for (CtWorkshop ctWorkshop : ctWorkshopList) {
            map = new HashMap<>();
            map.put("id", ctWorkshop.getWorkshopId());
            map.put("mc", ctWorkshop.getCwName());
            map.put("zt", ctWorkshop.getWStatus() == 0 ? "购买" : "租用");
            map.put("rl", ctWorkshop.getCwCapacity());
            map.put("gj", ctWorkshop.getCwBuyFee() + "W");
            map.put("zj", ctWorkshop.getCwRentFee() + "W");
            map.put("sj", ctWorkshop.getCwSellFee() + "W");
            map.put("zhfz", getDate(ctWorkshop.getWPayDate()));
            map.put("zbsj", getDate(ctWorkshop.getGetDate()));
            cfxx.add(map);
        }
        returnMap.put("cfxx", cfxx);
        // 生产线信息
        List<CtLine> ctLineList = ctLineMapper.getList(contestId, studentId);
        List<Map<String, Object>> scxxx = new ArrayList<>();
        for (CtLine ctLine : ctLineList) {
            map = new HashMap<>();
            // 状态
            String status = "-";
            if (ctLine.getPlProductingDate() == null && ctLine.getPlTransferDate() == null && ctLine.getPlRemainDate() == null) {
                status = "空闲";
            } else if (ctLine.getPlTransferDate() != null) {
                status = "转产";
            } else if (ctLine.getPlProductingDate() != null) {
                status = "在产";
            } else if (ctLine.getPlRemainDate() != null) {
                status = "在建";
            }

            map.put("id", ctLine.getLineId());
            map.put("mc", ctLine.getCplName());
            map.put("cf", ctLine.getCwName() + "(" + ctLine.getWorkshopId() + ")");
            map.put("cp", ctLine.getCpName());
            map.put("zt", status);
            map.put("ljtz", ctLine.getPlDepTotal() + "W");
            map.put("kcsj", getDate(ctLine.getPlProductAddDate()));
            map.put("zcsj", getDate(ctLine.getPlTransferAddDate()));
            map.put("sysj", (ctLine.getPlRemainDate() == null ? "-" : (ctLine.getPlRemainDate() + "季")));
            map.put("jcsj", getDate(ctLine.getPlFinishDate()));
            map.put("kjsj", getDate(ctLine.getPlAddTime()));
            scxxx.add(map);
        }
        returnMap.put("scxxx", scxxx);
        return new AsyncResult<>(returnMap);
    }

    /*
    订单列表
     */
    public Future<Map<String, Object>> getDDXX(Integer contestId, Integer studentId, Integer date) {
        Map<String, Object> returnMap = new HashMap<>();
        // 订单列表
        List<CtXzOrder> ctXzOrderList = ctXzOrderMapper.getList(contestId, studentId);
        List<CtGzIso> ctGzIsoList = ctGzIsoMapper.list(contestId);
        Map<Integer, String> isoMap = ctGzIsoList.stream().collect(Collectors.toMap(CtGzIso::getCiId, CtGzIso::getCiName, (k1, k2) -> k1));
        List<Map<String, Object>> ddlb = new ArrayList<>();
        for (CtXzOrder ctXzOrder : ctXzOrderList) {
            Map<String, Object> map = new HashMap<>();

            // 状态
            String status;
            if (ctXzOrder.getCommitDate() != null && ctXzOrder.getCommitDate() > 0) {
                status = "已交货";
            } else if (date == null) {
                status = "-";
            } else if ((date / 10 == ctXzOrder.getDate() && date % 10 > ctXzOrder.getDeliveryDate())
                    || ((date / 10) > ctXzOrder.getDate())) {
                status = "已违约";
            } else {
                status = "未到期";
            }

            map.put("ddbh", ctXzOrder.getCoId());
            map.put("sc", ctXzOrder.getCmName());
            map.put("cp", ctXzOrder.getCpName());
            map.put("sl", ctXzOrder.getNum());
            map.put("zj", ctXzOrder.getTotalPrice());
            map.put("zt", status);
            map.put("ddnf", "第" + ctXzOrder.getDate() + "年");
            map.put("jhq", ctXzOrder.getDeliveryDate() + "季");
            map.put("zq", ctXzOrder.getPaymentDate() + "季");
            map.put("iso", getISO(isoMap, ctXzOrder.getIsoId()));
            map.put("jhsj", getDate(ctXzOrder.getCommitDate()));
            ddlb.add(map);
        }
        returnMap.put("ddlb", ddlb);
        return new AsyncResult<>(returnMap);
    }

    private String getISO(Map<Integer, String> isoMap, Integer isoNum) {
        if (isoNum == null || isoMap == null) {
            return "-";
        }
        if (isoNum < 3) {
            return isoMap.get(isoNum);
        }
        return isoMap.get(1) + "\\" + isoMap.get(2);
    }

    /*
    企业财务报表
     */
    public Future<Map<String, Object>> getQYCWBB(Integer contestId, Integer studentId) {
        Map<String, Object> returnMap = new HashMap<>();
        // 综合费用表
        QueryWrapper<CtCharges> wrapper1 = new QueryWrapper<>();
        wrapper1.eq("contest_id", contestId).eq("student_id", studentId);
        List<CtCharges> ctChargesList = ctChargesMapper.selectList(wrapper1);
        // 年份+类型（第1年系统为11，第1年用户为10）映射到对象
        Map<Integer, CtCharges> ctChargesMap = new HashMap<>();
        for (CtCharges ctCharges : ctChargesList) {
            Integer num = ctCharges.getCDate() * 10 + ctCharges.getBsIsxt();
            CtCharges charges = ctChargesMap.get(num);
            if (charges == null) {
                ctChargesMap.put(num, ctCharges);
            } else {
                charges.add(ctCharges);
            }
        }
        // 数据放到list中
        List<Map<String, Object>> zhfyb = new ArrayList<>();
        // 循环年（1~4）和类型（0~1）
        zhfyb.add(this.getChargesFieldValueMap(ctChargesMap, "cOverhaul"));
        zhfyb.add(this.getChargesFieldValueMap(ctChargesMap, "cAd"));
        zhfyb.add(this.getChargesFieldValueMap(ctChargesMap, "cMaintenance"));
        zhfyb.add(this.getChargesFieldValueMap(ctChargesMap, "cTransfer"));
        zhfyb.add(this.getChargesFieldValueMap(ctChargesMap, "cRent"));
        zhfyb.add(this.getChargesFieldValueMap(ctChargesMap, "cDevelopMarket"));
        zhfyb.add(this.getChargesFieldValueMap(ctChargesMap, "cDevelopProduct"));
        zhfyb.add(this.getChargesFieldValueMap(ctChargesMap, "cDevelopIso"));
        zhfyb.add(this.getChargesFieldValueMap(ctChargesMap, "cDamage"));
        zhfyb.add(this.getChargesFieldValueMap(ctChargesMap, "cInformation"));
        returnMap.put("zhfyb", zhfyb);


        // 利润表
        QueryWrapper<CtProfitChart> wrapper2 = new QueryWrapper<>();
        wrapper2.eq("contest_id", contestId).eq("student_id", studentId);
        List<CtProfitChart> ctProfitChartList = ctProfitChartMapper.selectList(wrapper2);
        Map<Integer, CtProfitChart> ctProfitChartMap = new HashMap<>();
        for (CtProfitChart ctProfitChart : ctProfitChartList) {
            Integer num = ctProfitChart.getPcDate() * 10 + ctProfitChart.getBsIsxt();
            CtProfitChart profitChart = ctProfitChartMap.get(num);
            if (profitChart == null) {
                ctProfitChartMap.put(num, ctProfitChart);
            } else {
                profitChart.add(ctProfitChart);
            }
        }
        // 数据放到list中
        List<Map<String, Object>> lrb = new ArrayList<>();
        lrb.add(this.getProfitChartFieldValueMap(ctProfitChartMap, "pcSales"));
        lrb.add(this.getProfitChartFieldValueMap(ctProfitChartMap, "pcDirectCost"));
        lrb.add(this.getProfitChartFieldValueMap(ctProfitChartMap, "pcGoodsProfit"));
        lrb.add(this.getProfitChartFieldValueMap(ctProfitChartMap, "pcTotal"));
        lrb.add(this.getProfitChartFieldValueMap(ctProfitChartMap, "pcProfitBeforeDep"));
        lrb.add(this.getProfitChartFieldValueMap(ctProfitChartMap, "pcDep"));
        lrb.add(this.getProfitChartFieldValueMap(ctProfitChartMap, "pcProfitBeforeInterests"));
        lrb.add(this.getProfitChartFieldValueMap(ctProfitChartMap, "pcFinanceFee"));
        lrb.add(this.getProfitChartFieldValueMap(ctProfitChartMap, "pcProfitBeforeTax"));
        lrb.add(this.getProfitChartFieldValueMap(ctProfitChartMap, "pcTax"));
        lrb.add(this.getProfitChartFieldValueMap(ctProfitChartMap, "pcAnnualNetProfit"));
        returnMap.put("lrb", lrb);

        // 资产负债表
        QueryWrapper<CtBalance> wrapper3 = new QueryWrapper<>();
        wrapper3.eq("contest_id", contestId).eq("student_id", studentId);
        List<CtBalance> ctBalanceList = ctBalanceMapper.selectList(wrapper3);
        Map<Integer, CtBalance> ctBalanceMap = new HashMap<>();
        for (CtBalance ctBalance : ctBalanceList) {
            Integer num = ctBalance.getBsYear() * 10 + ctBalance.getBsIsxt();
            CtBalance balance = ctBalanceMap.get(num);
            if (balance == null) {
                ctBalanceMap.put(num, ctBalance);
            } else {
                balance.add(ctBalance);
            }
        }
        // 数据放到list中
        List<Map<String, Object>> zcfzb = new ArrayList<>(32);
        zcfzb.add(this.getBalanceFieldValueMap(ctBalanceMap, "bsCash"));
        zcfzb.add(this.getBalanceFieldValueMap(ctBalanceMap, "bsReceivable"));
        zcfzb.add(this.getBalanceFieldValueMap(ctBalanceMap, "bsProductInProcess"));
        zcfzb.add(this.getBalanceFieldValueMap(ctBalanceMap, "bsProduct"));
        zcfzb.add(this.getBalanceFieldValueMap(ctBalanceMap, "bsMaterial"));
        zcfzb.add(this.getBalanceFieldValueMap(ctBalanceMap, "bsTotalCurrentAsset"));
        zcfzb.add(this.getBalanceFieldValueMap(ctBalanceMap, "bsWorkshop"));
        zcfzb.add(this.getBalanceFieldValueMap(ctBalanceMap, "bsEquipment"));
        zcfzb.add(this.getBalanceFieldValueMap(ctBalanceMap, "bsProjectOnConstruction"));
        zcfzb.add(this.getBalanceFieldValueMap(ctBalanceMap, "bsTotalFixedAsset"));
        zcfzb.add(this.getBalanceFieldValueMap(ctBalanceMap, "bsTotalAsset"));
        zcfzb.add(this.getBalanceFieldValueMap(ctBalanceMap, "bsLongLoan"));
        zcfzb.add(this.getBalanceFieldValueMap(ctBalanceMap, "bsShortLoan"));
        // 特别贷款无
        Map<String, Object> map = new HashMap<>();
        for (int i = 1; i <= 6; i++) {
            for (int j = 1; j >= 0; j--) {
                int num = i * 10 + j;
                map.put("t" + num, "-");
            }
        }
        zcfzb.add(map);
        zcfzb.add(this.getBalanceFieldValueMap(ctBalanceMap, "bsTax"));
        zcfzb.add(this.getBalanceFieldValueMap(ctBalanceMap, "bsTotalLiability"));
        zcfzb.add(this.getBalanceFieldValueMap(ctBalanceMap, "bsEquity"));
        zcfzb.add(this.getBalanceFieldValueMap(ctBalanceMap, "bsRetainedEarning"));
        zcfzb.add(this.getBalanceFieldValueMap(ctBalanceMap, "bsAnnualNetProfit"));
        zcfzb.add(this.getBalanceFieldValueMap(ctBalanceMap, "bsTotalEquity"));
        zcfzb.add(this.getBalanceFieldValueMap(ctBalanceMap, "bsTotal"));
        returnMap.put("zcfzb", zcfzb);

        return new AsyncResult<>(returnMap);
    }

    /*
    四年广告投放
     */
    public Future<Map<String, Object>> getGGTF(Integer contestId, Integer studentId) {
        Map<String, Object> returnMap = new HashMap<>();
        SysUser sysUser = sysUserService.getById(studentId);
        if (sysUser == null) {
            return new AsyncResult<>(returnMap);
        }
        String name = sysUser.getName();
        List<CtMnAd> ctMnAdList = ctMnAdService.getAdListByGroupNum(contestId, studentId, name);
        Map<Integer, CtMnAd> ctMnAdMap = ctMnAdList.stream().collect(Collectors.toMap(CtMnAd::getYear, p -> p, (k1, k2) -> k1));
        List<Map<String, Object>> data;
        Map<String, Object> map;
        for (int i = 2; i <= 6; i++) {
            data = new ArrayList<>();
            CtMnAd ctMnAd = ctMnAdMap.get(i);
            if (ctMnAd == null) {
                ctMnAd = new CtMnAd();
            }
            map = new HashMap<>();
            map.put("local", this.convertToString(ctMnAd.getLocalP1()));
            map.put("region", this.convertToString(ctMnAd.getRegionalP1()));
            map.put("nation", this.convertToString(ctMnAd.getNationalP1()));
            map.put("asian", this.convertToString(ctMnAd.getAsianP1()));
            map.put("inter", this.convertToString(ctMnAd.getInternationalP1()));
            data.add(map);

            map = new HashMap<>();
            map.put("local", this.convertToString(ctMnAd.getLocalP2()));
            map.put("region", this.convertToString(ctMnAd.getRegionalP2()));
            map.put("nation", this.convertToString(ctMnAd.getNationalP2()));
            map.put("asian", this.convertToString(ctMnAd.getAsianP2()));
            map.put("inter", this.convertToString(ctMnAd.getInternationalP2()));
            data.add(map);

            map = new HashMap<>();
            map.put("local", this.convertToString(ctMnAd.getLocalP3()));
            map.put("region", this.convertToString(ctMnAd.getRegionalP3()));
            map.put("nation", this.convertToString(ctMnAd.getNationalP3()));
            map.put("asian", this.convertToString(ctMnAd.getAsianP3()));
            map.put("inter", this.convertToString(ctMnAd.getInternationalP3()));
            data.add(map);

            map = new HashMap<>();
            map.put("local", this.convertToString(ctMnAd.getLocalP4()));
            map.put("region", this.convertToString(ctMnAd.getRegionalP4()));
            map.put("nation", this.convertToString(ctMnAd.getNationalP4()));
            map.put("asian", this.convertToString(ctMnAd.getAsianP4()));
            map.put("inter", this.convertToString(ctMnAd.getInternationalP4()));
            data.add(map);

            map = new HashMap<>();
            map.put("local", this.convertToString(ctMnAd.getLocalP5()));
            map.put("region", this.convertToString(ctMnAd.getRegionalP5()));
            map.put("nation", this.convertToString(ctMnAd.getNationalP5()));
            map.put("asian", this.convertToString(ctMnAd.getAsianP5()));
            map.put("inter", this.convertToString(ctMnAd.getInternationalP5()));
            data.add(map);

            returnMap.put("y" + i, data);
        }
        return new AsyncResult<>(returnMap);
    }

    private Map<String, Object> getProfitChartFieldValueMap(Map<Integer, CtProfitChart> paramMap, String fieldName) {
        Map<String, Object> map = new HashMap<>();
        for (int i = 1; i <= 6; i++) {
            for (int j = 1; j >= 0; j--) {
                Integer num = i * 10 + j;
                CtProfitChart ctProfitChart = paramMap.get(num);
                if (ctProfitChart == null) {
                    map.put("t" + num, "-");
                } else {
                    map.put("t" + num, this.getFieldValueByFieldName(fieldName, ctProfitChart));
                }
            }
        }
        return map;
    }

    private Map<String, Object> getChargesFieldValueMap(Map<Integer, CtCharges> paramMap, String fieldName) {
        Map<String, Object> map = new HashMap<>();
        for (int i = 1; i <= 6; i++) {
            for (int j = 1; j >= 0; j--) {
                Integer num = i * 10 + j;
                CtCharges ctCharges = paramMap.get(num);
                if (ctCharges == null) {
                    map.put("t" + num, "-");
                } else {
                    map.put("t" + num, this.getFieldValueByFieldName(fieldName, ctCharges));
                }
            }
        }
        return map;
    }

    private Map<String, Object> getBalanceFieldValueMap(Map<Integer, CtBalance> paramMap, String fieldName) {
        Map<String, Object> map = new HashMap<>();
        for (int i = 1; i <= 6; i++) {
            for (int j = 1; j >= 0; j--) {
                Integer num = i * 10 + j;
                CtBalance ctBalance = paramMap.get(num);
                if (ctBalance == null) {
                    map.put("t" + num, "-");
                } else {
                    map.put("t" + num, this.getFieldValueByFieldName(fieldName, ctBalance));
                }
            }
        }
        return map;
    }

    /*
    反射：根据属性名获取属性值
     */
    private Integer getFieldValueByFieldName(String fieldName, Object object) {
        try {
            Field field = object.getClass().getDeclaredField(fieldName);
            //设置对象的访问权限，保证对private的属性的访问
            field.setAccessible(true);
            return (Integer) field.get(object);
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
    }

    private String formatNumber(Integer number) {
        if (number == null) {
            return "-";
        }
        return String.valueOf(number);
    }

    private String convertToString(Double localP1) {
        return localP1 == null ? "" : String.valueOf((int) Math.floor(localP1));
    }

}
