package com.xiyou.main.async.exam;

import com.xiyou.main.service.exam.ExamProblemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

/**
 * @program: multi-module
 * @description: 自动判题
 * @author: tangcan
 * @create: 2019-07-16 18:26
 **/
@Service
public class AutoJudeAsyncService {
    @Autowired
    @Lazy
    private ExamProblemService examProblemService;

    @Async
    public void autoJudge(Integer examId) {
        examProblemService.autoJudge(examId);
    }
}
