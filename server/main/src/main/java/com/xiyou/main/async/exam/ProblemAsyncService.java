package com.xiyou.main.async.exam;

import com.xiyou.main.entity.exam.ProblemOption;
import com.xiyou.main.entity.exam.Resources;
import com.xiyou.main.params.exam.ProblemInfo;
import com.xiyou.main.service.exam.ProblemOptionService;
import com.xiyou.main.service.exam.ResourcesService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.AsyncResult;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.concurrent.Future;
import java.util.stream.Collectors;

/**
 * @program: multi-module
 * @description:
 * @author: tangcan
 * @create: 2019-07-07 13:22
 **/
@Service
public class ProblemAsyncService {
    @Autowired
    @Lazy
    private ProblemOptionService problemOptionService;
    @Autowired
    @Lazy
    private ResourcesService resourcesService;

    @Async
    public void saveProblemOption(List<ProblemOption> optionList) {
        problemOptionService.insertBatch(optionList);
    }

    @Async
    public Future<Map<Integer, List<ProblemOption>>> getOptionMap(List<Integer> problemIdList) {
        if (problemIdList == null || problemIdList.size() == 0) {
            return new AsyncResult<>(new HashMap<>());
        }
        List<ProblemOption> optionList = problemOptionService.getByProblemIdList(problemIdList);
        Map<Integer, List<ProblemOption>> optionMap = optionList.stream().collect(Collectors.groupingBy(ProblemOption::getProblemId));
        return new AsyncResult<>(optionMap);
    }

    @Async
    public Future<Map<String, Resources>> getResourcesMap(List<ProblemInfo> problemList) {
        if (problemList == null || problemList.size() == 0) {
            return new AsyncResult<>(new HashMap<>());
        }
        List<String> fileNameList = new ArrayList<>();
        for (ProblemInfo problem : problemList) {
            if (StringUtils.isNotBlank(problem.getAttachment())) {
                String[] files = problem.getAttachment().split("\\|");
                for (String file : files) {
                    if (StringUtils.isNotBlank(file)) {
                        fileNameList.add(file);
                    }
                }
            }
            if (StringUtils.isNotBlank(problem.getPic())) {
                String[] files = problem.getPic().split("\\|");
                for (String file : files) {
                    if (StringUtils.isNotBlank(file)) {
                        fileNameList.add(file);
                    }
                }
            }
        }
        fileNameList = fileNameList.stream().distinct().collect(Collectors.toList());

        List<Resources> resourcesList = resourcesService.getByFileNameList(fileNameList);

        // 映射fileName->Resources，方便查询
        Map<String, Resources> resourcesMap = resourcesList.stream().collect(Collectors.toMap(Resources::getFileName, p -> p, (k1, k2) -> k1));

        return new AsyncResult<>(resourcesMap);
    }
}
