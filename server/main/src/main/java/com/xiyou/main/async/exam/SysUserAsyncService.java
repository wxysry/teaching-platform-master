package com.xiyou.main.async.exam;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.xiyou.main.entity.exam.SysUser;
import com.xiyou.main.service.exam.SysUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.AsyncResult;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.concurrent.Future;

/**
 * @program: multi-module
 * @description:
 * @author: tangcan
 * @create: 2019-06-27 18:30
 **/
@Service
public class SysUserAsyncService {
    @Autowired
    @Lazy
    private SysUserService sysUserService;

    @Async
    public Future<List<SysUser>> getByEmailList(List<String> emailList) {
        QueryWrapper<SysUser> wrapper = new QueryWrapper<>();
        wrapper.select("id", "account", "email").in("email", emailList);
        return new AsyncResult<>(sysUserService.list(wrapper));
    }
}
