package com.xiyou.main.async.exam;

import com.xiyou.main.entity.contest.UserContestGroup;
import com.xiyou.main.entity.exam.UserPptGroup;
import com.xiyou.main.entity.exam.UserProblemGroup;
import com.xiyou.main.entity.exam.UserVideoGroup;
import com.xiyou.main.entity.gbcontest.UserGbContestGroup;
import com.xiyou.main.entity.newcontest.UserNewContestGroup;
import com.xiyou.main.entity.ngbcontest.UserNGbContestGroup;
import com.xiyou.main.service.exam.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.AsyncResult;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Future;
import java.util.stream.Collectors;

/**
 * @program: multi-module
 * @description: 用户各分组权限异步任务
 * @author: tangcan
 * @create: 2019-07-10 10:35
 **/
@Service
public class UserGroupAsyncService {
    @Autowired
    @Lazy
    private UserProblemGroupService userProblemGroupService;

    @Autowired
    @Lazy
    private UserVideoGroupService userVideoGroupService;

    @Autowired
    @Lazy
    private UserPptGroupService userPptGroupService;

    @Autowired
    @Lazy
    private UserContestGroupService userContestGroupService;

    @Autowired
    @Lazy
    private UserNewContestGroupService userNewContestGroupService;

    @Autowired
    @Lazy
    private UserGbContestGroupService userGbContestGroupService;

    @Autowired
    @Lazy
    private UserNGbContestGroupService userNGbContestGroupService;

    @Async
    public Future<Map<Integer, List<UserProblemGroup>>> getProblemGroupByUserIdList(List<Integer> userIdList) {
        if (userIdList == null || userIdList.size() == 0) {
            return new AsyncResult<>(new HashMap<>());
        }
        List<UserProblemGroup> problemGroupList = userProblemGroupService.getByUserIdList(userIdList);
        // 根据userId分组
        Map<Integer, List<UserProblemGroup>> groupMap = problemGroupList.stream().collect(Collectors.groupingBy(UserProblemGroup::getUserId));
        return new AsyncResult<>(groupMap);
    }

    @Async
    public Future<Map<Integer, List<UserVideoGroup>>> getVideoGroupByUserIdList(List<Integer> userIdList) {
        if (userIdList == null || userIdList.size() == 0) {
            return new AsyncResult<>(new HashMap<>());
        }
        List<UserVideoGroup> videoGroupList = userVideoGroupService.getByUserIdList(userIdList);
        // 根据userId分组
        Map<Integer, List<UserVideoGroup>> groupMap = videoGroupList.stream().collect(Collectors.groupingBy(UserVideoGroup::getUserId));
        return new AsyncResult<>(groupMap);
    }

    @Async
    public Future<Map<Integer, List<UserPptGroup>>> getPptGroupByUserIdList(List<Integer> userIdList) {
        if (userIdList == null || userIdList.size() == 0) {
            return new AsyncResult<>(new HashMap<>());
        }
        List<UserPptGroup> pptGroupList = userPptGroupService.getByUserIdList(userIdList);
        // 根据userId分组
        Map<Integer, List<UserPptGroup>> groupMap = pptGroupList.stream().collect(Collectors.groupingBy(UserPptGroup::getUserId));
        return new AsyncResult<>(groupMap);
    }

    @Async
    public Future<Map<Integer, List<UserContestGroup>>> getContestGroupByUserIdList(List<Integer> userIdList) {
        if (userIdList == null || userIdList.size() == 0) {
            return new AsyncResult<>(new HashMap<>());
        }
        List<UserContestGroup> contestGroupList = userContestGroupService.getByUserIdList(userIdList);
        // 根据userId分组
        Map<Integer, List<UserContestGroup>> groupMap = contestGroupList.stream().collect(Collectors.groupingBy(UserContestGroup::getUserId));
        return new AsyncResult<>(groupMap);
    }


    @Async
    public Future<Map<Integer, List<UserNewContestGroup>>> getNewContestGroupByUserIdList(List<Integer> userIdList) {
        if (userIdList == null || userIdList.size() == 0) {
            return new AsyncResult<>(new HashMap<>());
        }
        List<UserNewContestGroup> newContestGroupList = userNewContestGroupService.getByUserIdList(userIdList);
        // 根据userId分组
        Map<Integer, List<UserNewContestGroup>> groupMap = newContestGroupList.stream().collect(Collectors.groupingBy(UserNewContestGroup::getUserId));
        return new AsyncResult<>(groupMap);
    }


    @Async
    public Future<Map<Integer, List<UserGbContestGroup>>> getGbContestGroupByUserIdList(List<Integer> userIdList) {
        if (userIdList == null || userIdList.size() == 0) {
            return new AsyncResult<>(new HashMap<>());
        }
        List<UserGbContestGroup> gbContestGroupList = userGbContestGroupService.getByUserIdList(userIdList);
        // 根据userId分组
        Map<Integer, List<UserGbContestGroup>> groupMap = gbContestGroupList.stream().collect(Collectors.groupingBy(UserGbContestGroup::getUserId));
        return new AsyncResult<>(groupMap);
    }


    @Async
    public Future<Map<Integer, List<UserNGbContestGroup>>> getNGbContestGroupByUserIdList(List<Integer> userIdList) {
        if (userIdList == null || userIdList.size() == 0) {
            return new AsyncResult<>(new HashMap<>());
        }
        List<UserNGbContestGroup> ngbContestGroupList = userNGbContestGroupService.getByUserIdList(userIdList);
        // 根据userId分组
        Map<Integer, List<UserNGbContestGroup>> groupMap = ngbContestGroupList.stream().collect(Collectors.groupingBy(UserNGbContestGroup::getUserId));
        return new AsyncResult<>(groupMap);
    }
}
