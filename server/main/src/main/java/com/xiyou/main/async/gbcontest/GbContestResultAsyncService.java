package com.xiyou.main.async.gbcontest;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.xiyou.common.utils.DateUtil;
import com.xiyou.main.dao.gbcontest.*;
import com.xiyou.main.entity.exam.SysUser;
import com.xiyou.main.entity.gbcontest.*;
import com.xiyou.main.service.exam.SysUserService;
import com.xiyou.main.service.gbcontest.GbCtMnAdService;
import com.xiyou.main.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.AsyncResult;
import org.springframework.stereotype.Service;

import java.lang.reflect.Field;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Future;
import java.util.stream.Collectors;

/**
 * @program: multi-module
 * @description: 学生竞赛结果的数据处理
 * @author: tangcan
 * @create: 2019-09-02 17:58
 **/
@Service
public class GbContestResultAsyncService {
    @Autowired
    @Lazy
    private GbCtDdMaterialMapper ctDdMaterialMapper;
    @Autowired
    @Lazy
    private GbCtKcMaterialMapper ctKcMaterialMapper;
    @Autowired
    @Lazy
    private GbCtKcProductMapper ctKcProductMapper;
    @Autowired
    @Lazy
    private GbCtFeeMapper ctFeeMapper;
    @Autowired
    @Lazy
    private GbCtBankLoanMapper ctBankLoanMapper;
    @Autowired
    @Lazy
    private GbCtCashflowMapper cashflowMapper;
    @Autowired
    @Lazy
    private GbCtYfMarketMapper ctYfMarketMapper;
    @Autowired
    @Lazy
    private GbCtYfIsoMapper ctYfIsoMapper;
    @Autowired
    @Lazy
    private GbCtYfProductMapper ctYfProductMapper;
    @Autowired
    @Lazy
    private GbCtLineMapper ctLineMapper;
    @Autowired
    @Lazy
    private GbCtXzOrderMapper ctXzOrderMapper;
    @Autowired
    @Lazy
    private GbCtGzIsoMapper ctGzIsoMapper;
    @Autowired
    @Lazy
    private GbCtBalanceMapper ctBalanceMapper;
    @Autowired
    @Lazy
    private GbCtChargesMapper ctChargesMapper;
    @Autowired
    @Lazy
    private GbCtProfitChartMapper ctProfitChartMapper;
    @Autowired
    @Lazy
    private SysUserService sysUserService;
    @Autowired
    @Lazy
    private GbCtMnAdService ctMnAdService;
    @Autowired
    @Lazy
    private GbCtYfNumMapper gbCtYfNumMapper;


    /*
    库存信息
     */
    @Async
    public Future<Map<String, Object>> getKCXX(Integer contestId, Integer studentId) {
        Map<String, Object> kcxx = new HashMap<>();
        // 原料订购
        List<Map<String, Object>> yldg = new ArrayList<>();
        List<GbCtDdMaterial> ddMaterialList = ctDdMaterialMapper.selectList(new LambdaQueryWrapper<GbCtDdMaterial>()
                        .eq(GbCtDdMaterial::getContestId,contestId)
                        .eq(GbCtDdMaterial::getStudentId,studentId)
                        .eq(GbCtDdMaterial::getIsInventory,0)
                );
        for (GbCtDdMaterial ddMaterial : ddMaterialList) {
            Map<String, Object> map = new HashMap<>();
            map.put("mc", ddMaterial.getCmName());
            map.put("sl", ddMaterial.getNum());
            map.put("sysj", getDate(ddMaterial.getRemainDate()));
            map.put("dgsj", getDate(ddMaterial.getPurchaseDate()));
            yldg.add(map);
        }
        kcxx.put("yldg", yldg);
        // 原料库存
        List<Map<String, Object>> ylkc = new ArrayList<>();
        List<GbCtKcMaterial> kcMaterialList = ctKcMaterialMapper.listKcGroupNumber(contestId, studentId);
        for (GbCtKcMaterial m : kcMaterialList) {
            Map<String, Object> map = new HashMap<>();
            map.put("mc", m.getMaterialName());
            map.put("sl", m.getImNum());
            ylkc.add(map);
        }
        kcxx.put("ylkc", ylkc);
        // 产品库存
        List<Map<String, Object>> cpkc = new ArrayList<>();
        List<GbCtKcProduct> kcProductList = ctKcProductMapper.listKc(contestId, studentId);
        for (GbCtKcProduct p : kcProductList) {
            Map<String, Object> map = new HashMap<>();
            map.put("mc", p.getCpName());
            map.put("sl", p.getIpNum());
            cpkc.add(map);
        }
        kcxx.put("cpkc", cpkc);
        return new AsyncResult<>(kcxx);
    }

    /*
    银行贷款
     */
    @Async
    public Future<Map<String, Object>> getYHDK(Integer contestId, Integer studentId, Integer date) {
        Map<String, Object> yhdk = new HashMap<>();
        // 应收款
        List<Map<String, Object>> ysk = new ArrayList<>();
        List<GbCtFee> ctFeeList = ctFeeMapper.getRecList(contestId, studentId);
        for (GbCtFee ctFee : ctFeeList) {
            Map<String, Object> map = new HashMap<>();
            map.put("syzq", ctFee.getRRemainDate() + "季");
            map.put("je", ctFee.getRFee() + "元");
            ysk.add(map);
        }
        yhdk.put("ysk", ysk);
        // 银行贷款
        List<Map<String, Object>> dk = new ArrayList<>();
        List<GbCtBankLoan> bankLoanList = ctBankLoanMapper.getList(contestId, studentId);
        for (GbCtBankLoan bankLoan : bankLoanList) {
            Map<String, Object> map = new HashMap<>();
            map.put("dkmc", bankLoan.getBlName());
            map.put("dksj", getDate(bankLoan.getBlAddTime()));
            map.put("dksc", bankLoan.getBlRemainTime());
            map.put("je", bankLoan.getBlFee());
            int sysj = DateUtils.cutSysYearAndSeasonTime(bankLoan.getBlRepaymentDate(),date);
            map.put("sysj",sysj < 0 ? 0:(sysj/10 == 0 ? "" : sysj/10+"年")+(sysj%10 == 0 ? "" : sysj%10+"季"));
            dk.add(map);
//            if (bankLoan.getBlType() == 1) {
//                // 长贷：计算剩余时间
//                if (bankLoan.getBlRepaymentDate() == null || date == null) {
//                    map.put("sysj", "-");
//                } else {
//                    int year = bankLoan.getBlRepaymentDate() / 10 - date / 10;
//                    if (year < 0) year = 0;
//                    map.put("sysj", "" + year + "年");
//                }
//                dk.add(map);
//            } else {
//                // 短贷：计算剩余时间
//                if (bankLoan.getBlRepaymentDate() == null || date == null) {
//                    map.put("sysj", "-");
//                } else {
//                    int quarter = bankLoan.getBlRepaymentDate() % 10 - date % 10;
//                    if (quarter < 0 || ((bankLoan.getBlRepaymentDate() / 10) < (date / 10))) quarter = 0;
//                    map.put("sysj", "" + quarter + "季");
//                }
//                dk.add(map);
//            }
        }
        yhdk.put("dk", dk);
        return new AsyncResult<>(yhdk);
    }

    /*
    研发认证
     */
    @Async
    public Future<Map<String, Object>> getYFRZ(Integer contestId, Integer studentId) {
        Map<String, Object> yfrz = new HashMap<>();
        // 市场开拓
        List<Map<String, Object>> sckt = new ArrayList<>();
        List<GbCtYfMarket> ctYfMarketList = ctYfMarketMapper.getList(contestId, studentId);
        for (GbCtYfMarket ctYfMarket : ctYfMarketList) {
            Map<String, Object> map = new HashMap<>();
            map.put("mc", ctYfMarket.getCmName());
            map.put("ktf", ctYfMarket.getCmDevelopFee() + "元");
            map.put("zq", ctYfMarket.getCmDevelopDate() + "季");
            map.put("sysj", getDate(ctYfMarket.getDmStartDate()));//申请时间
            map.put("wcsj", getDate(ctYfMarket.getDmFinishDate()));//完成时间
            sckt.add(map);
        }
        yfrz.put("sckt", sckt);
        // 产品研发
        List<Map<String, Object>> cpyf = new ArrayList<>();
        List<GbCtYfProduct> ctYfProductList = ctYfProductMapper.getList(contestId, studentId);
        for (GbCtYfProduct ctYfProduct : ctYfProductList) {
            Map<String, Object> map = new HashMap<>();
            map.put("mc", ctYfProduct.getCpName());
            map.put("yff", ctYfProduct.getCpProcessingFee() + "元");
            map.put("zq", ctYfProduct.getCpDevelopDate() + "季");
            map.put("sysj", getDate(ctYfProduct.getDpStartDate()));//申请时间
            map.put("wcsj", getDate(ctYfProduct.getDpFinishDate()));//完成时间
            cpyf.add(map);
        }
        yfrz.put("cpyf", cpyf);
        // ISO认证
        List<Map<String, Object>> isorz = new ArrayList<>();
        List<GbCtYfIso> ctYfIsoList = ctYfIsoMapper.getList(contestId, studentId);
        for (GbCtYfIso ctYfIso : ctYfIsoList) {
            Map<String, Object> map = new HashMap<>();
            map.put("mc", ctYfIso.getCiName());
            map.put("yff", ctYfIso.getCiDevelopFee() + "元");
            map.put("zq", ctYfIso.getCiDevelopDate() + "季");
            map.put("sysj", getDate(ctYfIso.getDiStartDate()));//申请时间
            map.put("wcsj", getDate(ctYfIso.getDiFinishDate()));//完成时间
            isorz.add(map);
        }
        yfrz.put("isorz", isorz);
        // 数字化研发
        List<Map<String, Object>> numrz = new ArrayList<>();
        List<GbCtYfNum> gbCtYfNumList = gbCtYfNumMapper.getList(contestId, studentId);
        for (GbCtYfNum gbCtYfNum : gbCtYfNumList) {
            Map<String, Object> map = new HashMap<>();
            map.put("mc", "数字化");
            map.put("yff", gbCtYfNum.getConsumeMoney() + "元");
            map.put("zq", gbCtYfNum.getTimeCostQuarter() + "季");
            map.put("sysj", getDate(gbCtYfNum.getStartDate()));//申请时间
            map.put("wcsj", getDate(gbCtYfNum.getFinishDate()));//完成时间
            numrz.add(map);
        }
        yfrz.put("numrz", numrz);
        return new AsyncResult<>(yfrz);
    }

    /*
    现金流量表
     */
    @Async
    public Future<Map<String, Object>> getXJLLB(Integer contestId, Integer studentId) {
        // 获取所有现金流量数据
        List<GbCtCashflow> cashflowList = cashflowMapper.getList(contestId, studentId);
        Map<String, Object> xjllb = new HashMap<>();
        List<Map<String, Object>> mapList = new ArrayList<>();
        int id = 0;
        for (GbCtCashflow ctCashflow : cashflowList) {
            Map<String, Object> map = new HashMap<>();
            map.put("id", ++id);
            map.put("dz", ctCashflow.getCAction());
            map.put("zj", ctCashflow.getCIn() - ctCashflow.getCOut());
            map.put("ye", ctCashflow.getCSurplus());
            map.put("sj", getDate(ctCashflow.getCDate()));
            map.put("bz", ctCashflow.getCComment());
            mapList.add(map);
        }
        xjllb.put("xjllb", mapList);
        return new AsyncResult<>(xjllb);
    }

    private String format(Integer number) {
        if (number == null) {
            return "-";
        }
        return String.valueOf(number);
    }

    private String getDate(Integer date) {
        if (date == null || date == 0) {
            return "-";
        }
        return "第" + date / 10 + "年" + date % 10 + "季";
    }

    /*
    生产线
     */
    public Future<Map<String, Object>> getCFYSCX(Integer contestId, Integer studentId) {
        Map<String, Object> returnMap = new HashMap<>();

        // 生产线信息
        List<GbCtLine> ctLineList = ctLineMapper.getList(contestId, studentId);
        List<Map<String, Object>> scxxx = new ArrayList<>();
        for (GbCtLine ctLine : ctLineList) {
            Map<String, Object> map = new HashMap<>();


            map.put("id", ctLine.getLineId());
            map.put("mc", ctLine.getCplName());
            map.put("cp", ctLine.getCpName());
            map.put("zt", ctLine.getStatus());
            map.put("ljtz", ctLine.getPlInvest() + "元");
            map.put("kcsj", getDate(ctLine.getPlProductAddDate()));
            map.put("zcsj", getDate(ctLine.getPlTransferAddDate()));
//            map.put("sysj", (ctLine.getPlRemainDate() == null ? "-" : (ctLine.getPlRemainDate() + "季")));
            map.put("jcsj", getDate(ctLine.getPlFinishDate()));
            map.put("kjsj", getDate(ctLine.getPlAddTime()));
            scxxx.add(map);
        }
        returnMap.put("scxxx", scxxx);
        return new AsyncResult<>(returnMap);
    }

    /*
    订单列表
     */
    public Future<Map<String, Object>> getDDXX(Integer contestId, Integer studentId, Integer date) {
        Map<String, Object> returnMap = new HashMap<>();
        // 订单列表
        List<GbCtXzOrder> ctXzOrderList = ctXzOrderMapper.getList(contestId, studentId);
        List<GbCtGzIso> ctGzIsoList = ctGzIsoMapper.list(contestId);
        Map<Integer, String> isoMap = ctGzIsoList.stream().collect(Collectors.toMap(GbCtGzIso::getCiId, GbCtGzIso::getCiName, (k1, k2) -> k1));
        List<Map<String, Object>> ddlb = new ArrayList<>();
        for (GbCtXzOrder ctXzOrder : ctXzOrderList) {
            Map<String, Object> map = new HashMap<>();

            // 状态
            String status;
            if (ctXzOrder.getCommitDate() != null && ctXzOrder.getCommitDate() > 0) {
                status = "已交货";
            } else if (date == null) {
                status = "-";
            } else if ((date / 10 == ctXzOrder.getDate() && date % 10 > ctXzOrder.getDeliveryDate())
                    || ((date / 10) > ctXzOrder.getDate())) {
                status = "已违约";
            } else {
                status = "未到期";
            }

            map.put("ddbh", ctXzOrder.getCoId());
            map.put("sc", ctXzOrder.getCmName());
            map.put("cp", ctXzOrder.getCpName());
            map.put("sl", ctXzOrder.getNum());
            map.put("zj", ctXzOrder.getTotalPrice());
            map.put("zt", status);
            map.put("ddnf", "第" + ctXzOrder.getDate() + "年");
            map.put("jhq", ctXzOrder.getDeliveryDate() + "季");
            map.put("zq", ctXzOrder.getPaymentDate() + "季");
            map.put("iso", getISO(isoMap, ctXzOrder.getIsoId()));
            map.put("jhsj", getDate(ctXzOrder.getCommitDate()));
            ddlb.add(map);
        }
        returnMap.put("ddlb", ddlb);
        return new AsyncResult<>(returnMap);
    }

    private String getISO(Map<Integer, String> isoMap, Integer isoNum) {
        if (isoNum == null || isoMap == null) {
            return "-";
        }
        if (isoNum < 3) {
            return isoMap.get(isoNum);
        }
        return isoMap.get(1) + "\\" + isoMap.get(2);
    }

    /*
    企业财务报表
     */
    public Future<Map<String, Object>> getQYCWBB(Integer contestId, Integer studentId) {
        Map<String, Object> returnMap = new HashMap<>();
        // 综合费用表
        QueryWrapper<GbCtCharges> wrapper1 = new QueryWrapper<>();
        wrapper1.eq("contest_id", contestId).eq("student_id", studentId);
        List<GbCtCharges> ctChargesList = ctChargesMapper.selectList(wrapper1);
        // 年份+类型（第1年系统为11，第1年用户为10）映射到对象
        Map<Integer, GbCtCharges> ctChargesMap = new HashMap<>();
        for (GbCtCharges ctCharges : ctChargesList) {
            Integer num = ctCharges.getCDate() * 10 + ctCharges.getBsIsxt();
            GbCtCharges charges = ctChargesMap.get(num);
            if (charges == null) {
                ctChargesMap.put(num, ctCharges);
            } else {
                charges.add(ctCharges);
            }
        }
        // 数据放到list中
        List<Map<String, Object>> zhfyb = new ArrayList<>();
        // 循环年（1~4）和类型（0~1）
        zhfyb.add(this.getChargesFieldValueMap(ctChargesMap, "cOverhaul"));
        zhfyb.add(this.getChargesFieldValueMap(ctChargesMap, "cAd"));
        zhfyb.add(this.getChargesFieldValueMap(ctChargesMap, "cMaintenance"));
        zhfyb.add(this.getChargesFieldValueMap(ctChargesMap, "cTransfer"));
        zhfyb.add(this.getChargesFieldValueMap(ctChargesMap, "cDevelopMarket"));
        zhfyb.add(this.getChargesFieldValueMap(ctChargesMap, "cDevelopProduct"));
        zhfyb.add(this.getChargesFieldValueMap(ctChargesMap, "cDevelopIso"));
        zhfyb.add(this.getChargesFieldValueMap(ctChargesMap, "cInformation"));
        zhfyb.add(this.getChargesFieldValueMap(ctChargesMap, "cHr"));
        zhfyb.add(this.getChargesFieldValueMap(ctChargesMap, "cDigitalization"));
        returnMap.put("zhfyb", zhfyb);


        // 利润表
        QueryWrapper<GbCtProfitChart> wrapper2 = new QueryWrapper<>();
        wrapper2.eq("contest_id", contestId).eq("student_id", studentId);
        List<GbCtProfitChart> ctProfitChartList = ctProfitChartMapper.selectList(wrapper2);
        Map<Integer, GbCtProfitChart> ctProfitChartMap = new HashMap<>();
        for (GbCtProfitChart ctProfitChart : ctProfitChartList) {
            Integer num = ctProfitChart.getPcDate() * 10 + ctProfitChart.getBsIsxt();
            GbCtProfitChart profitChart = ctProfitChartMap.get(num);
            if (profitChart == null) {
                ctProfitChartMap.put(num, ctProfitChart);
            } else {
                profitChart.add(ctProfitChart);
            }
        }
        // 数据放到list中
        List<Map<String, Object>> lrb = new ArrayList<>();
        lrb.add(this.getProfitChartFieldValueMap(ctProfitChartMap, "pcSales"));
        lrb.add(this.getProfitChartFieldValueMap(ctProfitChartMap, "pcDirectCost"));
        lrb.add(this.getProfitChartFieldValueMap(ctProfitChartMap, "pcGoodsProfit"));
        lrb.add(this.getProfitChartFieldValueMap(ctProfitChartMap, "pcTotal"));
        lrb.add(this.getProfitChartFieldValueMap(ctProfitChartMap, "pcProfitBeforeDep"));
        lrb.add(this.getProfitChartFieldValueMap(ctProfitChartMap, "pcDep"));
        lrb.add(this.getProfitChartFieldValueMap(ctProfitChartMap, "pcProfitBeforeInterests"));
        lrb.add(this.getProfitChartFieldValueMap(ctProfitChartMap, "pcFinanceFee"));
        lrb.add(this.getProfitChartFieldValueMap(ctProfitChartMap, "pcNonOperating"));
        lrb.add(this.getProfitChartFieldValueMap(ctProfitChartMap, "pcProfitBeforeTax"));
        lrb.add(this.getProfitChartFieldValueMap(ctProfitChartMap, "pcTax"));
        lrb.add(this.getProfitChartFieldValueMap(ctProfitChartMap, "pcAnnualNetProfit"));
        returnMap.put("lrb", lrb);

        // 资产负债表
        QueryWrapper<GbCtBalance> wrapper3 = new QueryWrapper<>();
        wrapper3.eq("contest_id", contestId).eq("student_id", studentId);
        List<GbCtBalance> ctBalanceList = ctBalanceMapper.selectList(wrapper3);
        Map<Integer, GbCtBalance> ctBalanceMap = new HashMap<>();
        for (GbCtBalance ctBalance : ctBalanceList) {
            Integer num = ctBalance.getBsYear() * 10 + ctBalance.getBsIsxt();
            GbCtBalance balance = ctBalanceMap.get(num);
            if (balance == null) {
                ctBalanceMap.put(num, ctBalance);
            } else {
                balance.add(ctBalance);
            }
        }
        // 数据放到list中
        List<Map<String, Object>> zcfzb = new ArrayList<>(32);
        zcfzb.add(this.getBalanceFieldValueMap(ctBalanceMap, "bsCash"));
        zcfzb.add(this.getBalanceFieldValueMap(ctBalanceMap, "bsReceivable"));
        zcfzb.add(this.getBalanceFieldValueMap(ctBalanceMap, "bsProductInProcess"));
        zcfzb.add(this.getBalanceFieldValueMap(ctBalanceMap, "bsProduct"));
        zcfzb.add(this.getBalanceFieldValueMap(ctBalanceMap, "bsMaterial"));
        zcfzb.add(this.getBalanceFieldValueMap(ctBalanceMap, "bsTotalCurrentAsset"));
        zcfzb.add(this.getBalanceFieldValueMap(ctBalanceMap, "bsEquipment"));
        zcfzb.add(this.getBalanceFieldValueMap(ctBalanceMap, "bsProjectOnConstruction"));
        zcfzb.add(this.getBalanceFieldValueMap(ctBalanceMap, "bsTotalFixedAsset"));
        zcfzb.add(this.getBalanceFieldValueMap(ctBalanceMap, "bsTotalAsset"));
        zcfzb.add(this.getBalanceFieldValueMap(ctBalanceMap, "bsLongLoan"));
        zcfzb.add(this.getBalanceFieldValueMap(ctBalanceMap, "bsShortLoan"));
        zcfzb.add(this.getBalanceFieldValueMap(ctBalanceMap, "bsOtherPay"));
        zcfzb.add(this.getBalanceFieldValueMap(ctBalanceMap, "bsTax"));
        zcfzb.add(this.getBalanceFieldValueMap(ctBalanceMap, "bsTotalLiability"));
        zcfzb.add(this.getBalanceFieldValueMap(ctBalanceMap, "bsEquity"));
        zcfzb.add(this.getBalanceFieldValueMap(ctBalanceMap, "bsRetainedEarning"));
        zcfzb.add(this.getBalanceFieldValueMap(ctBalanceMap, "bsAnnualNetProfit"));
        zcfzb.add(this.getBalanceFieldValueMap(ctBalanceMap, "bsTotalEquity"));
        zcfzb.add(this.getBalanceFieldValueMap(ctBalanceMap, "bsTotal"));
        returnMap.put("zcfzb", zcfzb);
        // 财务指标表
        QueryWrapper<GbCtFinancialTarget> wrapper4 = new QueryWrapper<>();
        wrapper4.eq("contest_id", contestId).eq("student_id", studentId);
        List<GbCtFinancialTarget> gbCtFinancialTargetList = gbCtFinancialTargetMapper.selectList(wrapper4);
        Map<Integer, GbCtFinancialTarget> gbCtFinancialTargetMap = new HashMap<>();
        for (GbCtFinancialTarget financialTarget : gbCtFinancialTargetList) {
            Integer num = financialTarget.getFtYear() * 10 + financialTarget.getFtIsxt();
            GbCtFinancialTarget gbCtFinancialTarget = gbCtFinancialTargetMap.get(num);
            if (gbCtFinancialTarget == null) {
                gbCtFinancialTargetMap.put(num, financialTarget);
            } else {
                gbCtFinancialTarget.add(financialTarget);
            }
        }
        // 数据放到list中
        List<Map<String, Object>> cwzbb = new ArrayList<>(32);
        cwzbb.add(this.getFinancialTargetFieldValueMap(gbCtFinancialTargetMap, "ftCurrentRate"));
        cwzbb.add(this.getFinancialTargetFieldValueMap(gbCtFinancialTargetMap, "ftQuickRate"));
        cwzbb.add(this.getFinancialTargetFieldValueMap(gbCtFinancialTargetMap, "ftDebtRate"));
        cwzbb.add(this.getFinancialTargetFieldValueMap(gbCtFinancialTargetMap, "ftEquityRate"));
        cwzbb.add(this.getFinancialTargetFieldValueMap(gbCtFinancialTargetMap, "ftNetProfitRate"));
        cwzbb.add(this.getFinancialTargetFieldValueMap(gbCtFinancialTargetMap, "ftCostExpenseRate"));
        cwzbb.add(this.getFinancialTargetFieldValueMap(gbCtFinancialTargetMap, "ftReturnAssetsRate"));
        cwzbb.add(this.getFinancialTargetFieldValueMap(gbCtFinancialTargetMap, "ftReturnEquityRate"));
        cwzbb.add(this.getFinancialTargetFieldValueMap(gbCtFinancialTargetMap, "ftRevenueGrowthRate"));
        cwzbb.add(this.getFinancialTargetFieldValueMap(gbCtFinancialTargetMap, "ftAppreciationRate"));
        cwzbb.add(this.getFinancialTargetFieldValueMap(gbCtFinancialTargetMap, "ftTotalGrowthRate"));
        cwzbb.add(this.getFinancialTargetFieldValueMap(gbCtFinancialTargetMap, "ftInventoryRate"));
        cwzbb.add(this.getFinancialTargetFieldValueMap(gbCtFinancialTargetMap, "ftInventoryDays"));
        cwzbb.add(this.getFinancialTargetFieldValueMap(gbCtFinancialTargetMap, "ftReceivableRate"));
        cwzbb.add(this.getFinancialTargetFieldValueMap(gbCtFinancialTargetMap, "ftReceivableDays"));
        cwzbb.add(this.getFinancialTargetFieldValueMap(gbCtFinancialTargetMap, "ftCashPeriod"));
        returnMap.put("cwzbb", cwzbb);

        return new AsyncResult<>(returnMap);
    }
    @Autowired
    private GbCtFinancialTargetMapper gbCtFinancialTargetMapper;
    /*
    四年广告投放
     */
    public Future<Map<String, Object>> getGGTF(Integer contestId, Integer studentId) {
        Map<String, Object> returnMap = new HashMap<>();
        SysUser sysUser = sysUserService.getById(studentId);
        if (sysUser == null) {
            return new AsyncResult<>(returnMap);
        }
        String name = sysUser.getName();
        List<GbCtMnAd> ctMnAdList = ctMnAdService.getAdListByGroupNum(contestId, studentId, name);
        Map<Integer, GbCtMnAd> ctMnAdMap = ctMnAdList.stream().collect(Collectors.toMap(p ->(p.getYear()*10+p.getQuarterly()), p -> p, (k1, k2) -> k1));
        List<Map<String, Object>> data;
        Integer[] yq = {21,22,23,24,31,32,33,34,41,42,43,44,51,52,53,54,61,62,63,64};
        Map<String, Object> map;
        for (Integer i : yq) {
            data = new ArrayList<>();
            GbCtMnAd ctMnAd = ctMnAdMap.get(i);
            if (ctMnAd == null) {
                ctMnAd = new GbCtMnAd();
            }
            map = new HashMap<>();
            map.put("local", this.convertToString(ctMnAd.getLocalP1()));
            map.put("region", this.convertToString(ctMnAd.getRegionalP1()));
            map.put("nation", this.convertToString(ctMnAd.getNationalP1()));
            map.put("asian", this.convertToString(ctMnAd.getAsianP1()));
            map.put("inter", this.convertToString(ctMnAd.getInternationalP1()));
            data.add(map);

            map = new HashMap<>();
            map.put("local", this.convertToString(ctMnAd.getLocalP2()));
            map.put("region", this.convertToString(ctMnAd.getRegionalP2()));
            map.put("nation", this.convertToString(ctMnAd.getNationalP2()));
            map.put("asian", this.convertToString(ctMnAd.getAsianP2()));
            map.put("inter", this.convertToString(ctMnAd.getInternationalP2()));
            data.add(map);

            map = new HashMap<>();
            map.put("local", this.convertToString(ctMnAd.getLocalP3()));
            map.put("region", this.convertToString(ctMnAd.getRegionalP3()));
            map.put("nation", this.convertToString(ctMnAd.getNationalP3()));
            map.put("asian", this.convertToString(ctMnAd.getAsianP3()));
            map.put("inter", this.convertToString(ctMnAd.getInternationalP3()));
            data.add(map);

            map = new HashMap<>();
            map.put("local", this.convertToString(ctMnAd.getLocalP4()));
            map.put("region", this.convertToString(ctMnAd.getRegionalP4()));
            map.put("nation", this.convertToString(ctMnAd.getNationalP4()));
            map.put("asian", this.convertToString(ctMnAd.getAsianP4()));
            map.put("inter", this.convertToString(ctMnAd.getInternationalP4()));
            data.add(map);

            map = new HashMap<>();
            map.put("local", this.convertToString(ctMnAd.getLocalP5()));
            map.put("region", this.convertToString(ctMnAd.getRegionalP5()));
            map.put("nation", this.convertToString(ctMnAd.getNationalP5()));
            map.put("asian", this.convertToString(ctMnAd.getAsianP5()));
            map.put("inter", this.convertToString(ctMnAd.getInternationalP5()));
            data.add(map);

            returnMap.put("y" + i, data);
        }
        return new AsyncResult<>(returnMap);
    }

    private Map<String, Object> getProfitChartFieldValueMap(Map<Integer, GbCtProfitChart> paramMap, String fieldName) {
        Map<String, Object> map = new HashMap<>();
        for (int i = 1; i <= 6; i++) {
            for (int j = 1; j >= 0; j--) {
                Integer num = i * 10 + j;
                GbCtProfitChart ctProfitChart = paramMap.get(num);
                if (ctProfitChart == null) {
                    map.put("t" + num, "-");
                } else {
                    map.put("t" + num, this.getFieldValueByFieldName(fieldName, ctProfitChart));
                }
            }
        }
        return map;
    }

    private Map<String, Object> getChargesFieldValueMap(Map<Integer, GbCtCharges> paramMap, String fieldName) {
        Map<String, Object> map = new HashMap<>();
        for (int i = 1; i <= 6; i++) {
            for (int j = 1; j >= 0; j--) {
                Integer num = i * 10 + j;
                GbCtCharges ctCharges = paramMap.get(num);
                if (ctCharges == null) {
                    map.put("t" + num, "-");
                } else {
                    map.put("t" + num, this.getFieldValueByFieldName(fieldName, ctCharges));
                }
            }
        }
        return map;
    }

    private Map<String, Object> getBalanceFieldValueMap(Map<Integer, GbCtBalance> paramMap, String fieldName) {
        Map<String, Object> map = new HashMap<>();
        for (int i = 1; i <= 6; i++) {
            for (int j = 1; j >= 0; j--) {
                Integer num = i * 10 + j;
                GbCtBalance ctBalance = paramMap.get(num);
                if (ctBalance == null) {
                    map.put("t" + num, "-");
                } else {
                    map.put("t" + num, this.getFieldValueByFieldName(fieldName, ctBalance));
                }
            }
        }
        return map;
    }

    private Map<String, Object> getFinancialTargetFieldValueMap(Map<Integer, GbCtFinancialTarget> paramMap, String fieldName) {
        Map<String, Object> map = new HashMap<>();
        for (int i = 1; i <= 6; i++) {
            for (int j = 1; j >= 0; j--) {
                Integer num = i * 10 + j;
                GbCtFinancialTarget ctBalance = paramMap.get(num);
                if (ctBalance == null) {
                    map.put("t" + num, "-");
                } else {
                    map.put("t" + num, this.getFieldValueBigDeByFieldName(fieldName, ctBalance));
                }
            }
        }
        return map;
    }

    /*
    反射：根据属性名获取属性值
     */
    private Integer getFieldValueByFieldName(String fieldName, Object object) {
        try {
            Field field = object.getClass().getDeclaredField(fieldName);
            //设置对象的访问权限，保证对private的属性的访问
            field.setAccessible(true);
            return (Integer) field.get(object);
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
    }

    /*
    反射：根据属性名获取属性值
     */
    private BigDecimal getFieldValueBigDeByFieldName(String fieldName, Object object) {
        try {
            Field field = object.getClass().getDeclaredField(fieldName);
            //设置对象的访问权限，保证对private的属性的访问
            field.setAccessible(true);
            return (BigDecimal) field.get(object);
        } catch (Exception e) {
            e.printStackTrace();
            return BigDecimal.valueOf(0.0);
        }
    }

    private String formatNumber(Integer number) {
        if (number == null) {
            return "-";
        }
        return String.valueOf(number);
    }

    private String convertToString(Double localP1) {
        return localP1 == null ? "" : String.valueOf((int) Math.floor(localP1));
    }

}
