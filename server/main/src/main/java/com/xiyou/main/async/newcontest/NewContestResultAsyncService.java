package com.xiyou.main.async.newcontest;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.xiyou.main.dao.contest.*;
import com.xiyou.main.dao.newcontest.*;
import com.xiyou.main.entity.contest.*;
import com.xiyou.main.entity.exam.SysUser;
import com.xiyou.main.entity.newcontest.*;
import com.xiyou.main.params.newcontest.NewNumParam;
import com.xiyou.main.service.contest.CtMnAdService;
import com.xiyou.main.service.exam.SysUserService;
import com.xiyou.main.service.newcontest.NewCtMnAdService;
import com.xiyou.main.vo.contest.MaterialVo;
import com.xiyou.main.vo.contest.ProductVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.AsyncResult;
import org.springframework.stereotype.Service;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Future;
import java.util.stream.Collectors;

/**
 * @program: multi-module
 * @description: 学生竞赛结果的数据处理
 * @author: tangcan
 * @create: 2019-09-02 17:58
 **/
@Service
public class NewContestResultAsyncService {
    @Autowired
    @Lazy
    private NewCtDdMaterialMapper ctDdMaterialMapper;
    @Autowired
    @Lazy
    private NewCtKcMaterialMapper ctKcMaterialMapper;
    @Autowired
    @Lazy
    private NewCtKcProductMapper ctKcProductMapper;
    @Autowired
    @Lazy
    private NewCtFeeMapper ctFeeMapper;
    @Autowired
    @Lazy
    private NewCtBankLoanMapper ctBankLoanMapper;
    @Autowired
    @Lazy
    private NewCtCashflowMapper cashflowMapper;
    @Autowired
    @Lazy
    private NewCtYfMarketMapper ctYfMarketMapper;
    @Autowired
    @Lazy
    private NewCtYfIsoMapper ctYfIsoMapper;
    @Autowired
    @Lazy
    private NewCtYfProductMapper ctYfProductMapper;
    @Autowired
    @Lazy
    private NewCtLineMapper ctLineMapper;
    @Autowired
    @Lazy
    private NewCtXzOrderMapper ctXzOrderMapper;
    @Autowired
    @Lazy
    private NewCtGzIsoMapper ctGzIsoMapper;
    @Autowired
    @Lazy
    private NewCtBalanceMapper ctBalanceMapper;
    @Autowired
    @Lazy
    private NewCtChargesMapper ctChargesMapper;
    @Autowired
    @Lazy
    private NewCtProfitChartMapper ctProfitChartMapper;
    @Autowired
    @Lazy
    private SysUserService sysUserService;
    @Autowired
    @Lazy
    private NewCtMnAdService ctMnAdService;
    @Autowired
    @Lazy
    private NewCtYfNumMapper newCtYfNumMapper;


    /*
    库存信息
     */
    @Async
    public Future<Map<String, Object>> getKCXX(Integer contestId, Integer studentId) {
        Map<String, Object> kcxx = new HashMap<>();
        // 原料订购
        List<Map<String, Object>> yldg = new ArrayList<>();
        List<NewCtDdMaterial> ddMaterialList = ctDdMaterialMapper.list(contestId, studentId);
        for (NewCtDdMaterial ddMaterial : ddMaterialList) {
            Map<String, Object> map = new HashMap<>();
            map.put("mc", ddMaterial.getCmName());
            map.put("sl", ddMaterial.getNum());
            map.put("sysj", ddMaterial.getRemainDate());
            map.put("dgsj", getDate(ddMaterial.getPurchaseDate()));
            yldg.add(map);
        }
        kcxx.put("yldg", yldg);
        // 原料库存
        List<Map<String, Object>> ylkc = new ArrayList<>();
        List<NewCtKcMaterial> kcMaterialList = ctKcMaterialMapper.listKcGroupNumber(contestId, studentId);
        for (NewCtKcMaterial m : kcMaterialList) {
            Map<String, Object> map = new HashMap<>();
            map.put("mc", m.getMaterialName());
            map.put("sl", m.getImNum());
            ylkc.add(map);
        }
        kcxx.put("ylkc", ylkc);
        // 产品库存
        List<Map<String, Object>> cpkc = new ArrayList<>();
        List<NewCtKcProduct> kcProductList = ctKcProductMapper.listKc(contestId, studentId);
        for (NewCtKcProduct p : kcProductList) {
            Map<String, Object> map = new HashMap<>();
            map.put("mc", p.getCpName());
            map.put("sl", p.getIpNum());
            cpkc.add(map);
        }
        kcxx.put("cpkc", cpkc);
        return new AsyncResult<>(kcxx);
    }

    /*
    银行贷款
     */
    @Async
    public Future<Map<String, Object>> getYHDK(Integer contestId, Integer studentId, Integer date) {
        Map<String, Object> yhdk = new HashMap<>();
        // 应收款
        List<Map<String, Object>> ysk = new ArrayList<>();
        List<NewCtFee> ctFeeList = ctFeeMapper.getList(contestId, studentId);
        for (NewCtFee ctFee : ctFeeList) {
            Map<String, Object> map = new HashMap<>();
            map.put("syzq", ctFee.getRRemainDate() + "季");
            map.put("je", ctFee.getRFee() + "元");
            ysk.add(map);
        }
        yhdk.put("ysk", ysk);
        // 长期贷款
        List<Map<String, Object>> cqdk = new ArrayList<>();
        // 短期贷款
        List<Map<String, Object>> dqdk = new ArrayList<>();
        List<NewCtBankLoan> bankLoanList = ctBankLoanMapper.getList(contestId, studentId);
        for (NewCtBankLoan bankLoan : bankLoanList) {
            Map<String, Object> map = new HashMap<>();
            map.put("je", bankLoan.getBlFee());
            map.put("dksj", getDate(bankLoan.getBlAddTime()));
            if (bankLoan.getBlType() == 1) {
                // 长贷：计算剩余时间
                if (bankLoan.getBlRepaymentDate() == null || date == null) {
                    map.put("sysj", "-");
                } else {
                    int year = bankLoan.getBlRepaymentDate() / 10 - date / 10;
                    if (year < 0) year = 0;
                    map.put("sysj", "" + year + "年");
                }
                cqdk.add(map);
            } else {
                // 短贷：计算剩余时间
                if (bankLoan.getBlRepaymentDate() == null || date == null) {
                    map.put("sysj", "-");
                } else {
                    int quarter = bankLoan.getBlRepaymentDate() % 10 - date % 10;
                    if (quarter < 0 || ((bankLoan.getBlRepaymentDate() / 10) < (date / 10))) quarter = 0;
                    map.put("sysj", "" + quarter + "季");
                }
                dqdk.add(map);
            }
        }
        yhdk.put("cqdk", cqdk);
        yhdk.put("dqdk", dqdk);
        return new AsyncResult<>(yhdk);
    }

    /*
    研发认证
     */
    @Async
    public Future<Map<String, Object>> getYFRZ(Integer contestId, Integer studentId) {
        Map<String, Object> yfrz = new HashMap<>();
        // 市场开拓
        List<Map<String, Object>> sckt = new ArrayList<>();
        List<NewCtYfMarket> ctYfMarketList = ctYfMarketMapper.getList(contestId, studentId);
        for (NewCtYfMarket ctYfMarket : ctYfMarketList) {
            Map<String, Object> map = new HashMap<>();
            map.put("mc", ctYfMarket.getCmName());
            map.put("ktf", ctYfMarket.getCmDevelopFee() + "元");
            map.put("zq", ctYfMarket.getCmDevelopDate() + "季");
            map.put("sysj", getDate(ctYfMarket.getDmStartDate()));//申请时间
            map.put("wcsj", getDate(ctYfMarket.getDmFinishDate()));//完成时间
            sckt.add(map);
        }
        yfrz.put("sckt", sckt);
        // 产品研发
        List<Map<String, Object>> cpyf = new ArrayList<>();
        List<NewCtYfProduct> ctYfProductList = ctYfProductMapper.getList(contestId, studentId);
        for (NewCtYfProduct ctYfProduct : ctYfProductList) {
            Map<String, Object> map = new HashMap<>();
            map.put("mc", ctYfProduct.getCpName());
            map.put("yff", ctYfProduct.getCpProcessingFee() + "元");
            map.put("zq", ctYfProduct.getCpDevelopDate() + "季");
            map.put("sysj", getDate(ctYfProduct.getDpStartDate()));//申请时间
            map.put("wcsj", getDate(ctYfProduct.getDpFinishDate()));//完成时间
            cpyf.add(map);
        }
        yfrz.put("cpyf", cpyf);
        // ISO认证
        List<Map<String, Object>> isorz = new ArrayList<>();
        List<NewCtYfIso> ctYfIsoList = ctYfIsoMapper.getList(contestId, studentId);
        for (NewCtYfIso ctYfIso : ctYfIsoList) {
            Map<String, Object> map = new HashMap<>();
            map.put("mc", ctYfIso.getCiName());
            map.put("yff", ctYfIso.getCiDevelopFee() + "元");
            map.put("zq", ctYfIso.getCiDevelopDate() + "季");
            map.put("sysj", getDate(ctYfIso.getDiStartDate()));//申请时间
            map.put("wcsj", getDate(ctYfIso.getDiFinishDate()));//完成时间
            isorz.add(map);
        }
        yfrz.put("isorz", isorz);
        // 数字化研发
        List<Map<String, Object>> numrz = new ArrayList<>();
        List<NewCtYfNum> newCtYfNumList = newCtYfNumMapper.getList(contestId, studentId);
        for (NewCtYfNum newCtYfNum : newCtYfNumList) {
            Map<String, Object> map = new HashMap<>();
            map.put("mc", "数字化");
            map.put("yff", newCtYfNum.getConsumeMoney() + "元");
            map.put("zq", newCtYfNum.getTimeCostQuarter() + "季");
            map.put("sysj", getDate(newCtYfNum.getStartDate()));//申请时间
            map.put("wcsj", getDate(newCtYfNum.getFinishDate()));//完成时间
            numrz.add(map);
        }
        yfrz.put("numrz", numrz);
        return new AsyncResult<>(yfrz);
    }

    /*
    现金流量表
     */
    @Async
    public Future<Map<String, Object>> getXJLLB(Integer contestId, Integer studentId) {
        // 获取所有现金流量数据
        List<NewCtCashflow> cashflowList = cashflowMapper.getList(contestId, studentId);
        Map<String, Object> xjllb = new HashMap<>();
        List<Map<String, Object>> mapList = new ArrayList<>();
        int id = 0;
        for (NewCtCashflow ctCashflow : cashflowList) {
            Map<String, Object> map = new HashMap<>();
            map.put("id", ++id);
            map.put("dz", ctCashflow.getCAction());
            map.put("zj", ctCashflow.getCIn() - ctCashflow.getCOut());
            map.put("ye", ctCashflow.getCSurplus());
            map.put("sj", getDate(ctCashflow.getCDate()));
            map.put("bz", ctCashflow.getCComment());
            mapList.add(map);
        }
        xjllb.put("xjllb", mapList);
        return new AsyncResult<>(xjllb);
    }

    private String format(Integer number) {
        if (number == null) {
            return "-";
        }
        return String.valueOf(number);
    }

    private String getDate(Integer date) {
        if (date == null || date == 0) {
            return "-";
        }
        return "第" + date / 10 + "年" + date % 10 + "季";
    }

    /*
    生产线
     */
    public Future<Map<String, Object>> getCFYSCX(Integer contestId, Integer studentId) {
        Map<String, Object> returnMap = new HashMap<>();

        // 生产线信息
        List<NewCtLine> ctLineList = ctLineMapper.getList(contestId, studentId);
        List<Map<String, Object>> scxxx = new ArrayList<>();
        for (NewCtLine ctLine : ctLineList) {
            Map<String, Object> map = new HashMap<>();
            // 状态
            map.put("id", ctLine.getLineId());
            map.put("mc", ctLine.getCplName());
            map.put("cp", ctLine.getCpName());
            map.put("zt", ctLine.getStatus());
            map.put("ljtz", ctLine.getCplDepFee() + "元");
            map.put("kcsj", getDate(ctLine.getPlProductAddDate()));
            map.put("zcsj", getDate(ctLine.getPlTransferAddDate()));
//            map.put("sysj", (ctLine.getPlRemainDate() == null ? "-" : (ctLine.getPlRemainDate() + "季")));
            map.put("jcsj", getDate(ctLine.getPlFinishDate()));
            map.put("kjsj", getDate(ctLine.getPlAddTime()));
            scxxx.add(map);
        }
        returnMap.put("scxxx", scxxx);
        return new AsyncResult<>(returnMap);
    }

    /*
    订单列表
     */
    public Future<Map<String, Object>> getDDXX(Integer contestId, Integer studentId, Integer date) {
        Map<String, Object> returnMap = new HashMap<>();
        // 订单列表
        List<NewCtXzOrder> ctXzOrderList = ctXzOrderMapper.getList(contestId, studentId);
        List<NewCtGzIso> ctGzIsoList = ctGzIsoMapper.list(contestId);
        Map<Integer, String> isoMap = ctGzIsoList.stream().collect(Collectors.toMap(NewCtGzIso::getCiId, NewCtGzIso::getCiName, (k1, k2) -> k1));
        List<Map<String, Object>> ddlb = new ArrayList<>();
        for (NewCtXzOrder ctXzOrder : ctXzOrderList) {
            Map<String, Object> map = new HashMap<>();

            // 状态
            String status;
            if (ctXzOrder.getCommitDate() != null && ctXzOrder.getCommitDate() > 0) {
                status = "已交货";
            } else if (date == null) {
                status = "-";
            } else if ((date / 10 == ctXzOrder.getDate() && date % 10 > ctXzOrder.getDeliveryDate())
                    || ((date / 10) > ctXzOrder.getDate())) {
                status = "已违约";
            } else {
                status = "未到期";
            }

            map.put("ddbh", ctXzOrder.getCoId());
            map.put("sc", ctXzOrder.getCmName());
            map.put("cp", ctXzOrder.getCpName());
            map.put("sl", ctXzOrder.getNum());
            map.put("zj", ctXzOrder.getTotalPrice());
            map.put("zt", status);
            map.put("ddnf", "第" + ctXzOrder.getDate() + "年");
            map.put("jhq", ctXzOrder.getDeliveryDate() + "季");
            map.put("zq", ctXzOrder.getPaymentDate() + "季");
            map.put("iso", getISO(isoMap, ctXzOrder.getIsoId()));
            map.put("jhsj", getDate(ctXzOrder.getCommitDate()));
            ddlb.add(map);
        }
        returnMap.put("ddlb", ddlb);
        return new AsyncResult<>(returnMap);
    }

    private String getISO(Map<Integer, String> isoMap, Integer isoNum) {
        if (isoNum == null || isoMap == null) {
            return "-";
        }
        if (isoNum < 3) {
            return isoMap.get(isoNum);
        }
        return isoMap.get(1) + "\\" + isoMap.get(2);
    }

    /*
    企业财务报表
     */
    public Future<Map<String, Object>> getQYCWBB(Integer contestId, Integer studentId) {
        Map<String, Object> returnMap = new HashMap<>();
        // 综合费用表
        QueryWrapper<NewCtCharges> wrapper1 = new QueryWrapper<>();
        wrapper1.eq("contest_id", contestId).eq("student_id", studentId);
        List<NewCtCharges> ctChargesList = ctChargesMapper.selectList(wrapper1);
        // 年份+类型（第1年系统为11，第1年用户为10）映射到对象
        Map<Integer, NewCtCharges> ctChargesMap = new HashMap<>();
        for (NewCtCharges ctCharges : ctChargesList) {
            Integer num = ctCharges.getCDate() * 10 + ctCharges.getBsIsxt();
            NewCtCharges charges = ctChargesMap.get(num);
            if (charges == null) {
                ctChargesMap.put(num, ctCharges);
            } else {
                charges.add(ctCharges);
            }
        }
        // 数据放到list中
        List<Map<String, Object>> zhfyb = new ArrayList<>();
        // 循环年（1~4）和类型（0~1）
        zhfyb.add(this.getChargesFieldValueMap(ctChargesMap, "cOverhaul"));
        zhfyb.add(this.getChargesFieldValueMap(ctChargesMap, "cAd"));
        zhfyb.add(this.getChargesFieldValueMap(ctChargesMap, "cMaintenance"));
        zhfyb.add(this.getChargesFieldValueMap(ctChargesMap, "cTransfer"));
        zhfyb.add(this.getChargesFieldValueMap(ctChargesMap, "cDevelopMarket"));
        zhfyb.add(this.getChargesFieldValueMap(ctChargesMap, "cDevelopProduct"));
        zhfyb.add(this.getChargesFieldValueMap(ctChargesMap, "cDevelopIso"));
        zhfyb.add(this.getChargesFieldValueMap(ctChargesMap, "cInformation"));
        zhfyb.add(this.getChargesFieldValueMap(ctChargesMap, "cHr"));
        zhfyb.add(this.getChargesFieldValueMap(ctChargesMap, "cDigitalization"));
        returnMap.put("zhfyb", zhfyb);


        // 利润表
        QueryWrapper<NewCtProfitChart> wrapper2 = new QueryWrapper<>();
        wrapper2.eq("contest_id", contestId).eq("student_id", studentId);
        List<NewCtProfitChart> ctProfitChartList = ctProfitChartMapper.selectList(wrapper2);
        Map<Integer, NewCtProfitChart> ctProfitChartMap = new HashMap<>();
        for (NewCtProfitChart ctProfitChart : ctProfitChartList) {
            Integer num = ctProfitChart.getPcDate() * 10 + ctProfitChart.getBsIsxt();
            NewCtProfitChart profitChart = ctProfitChartMap.get(num);
            if (profitChart == null) {
                ctProfitChartMap.put(num, ctProfitChart);
            } else {
                profitChart.add(ctProfitChart);
            }
        }
        // 数据放到list中
        List<Map<String, Object>> lrb = new ArrayList<>();
        lrb.add(this.getProfitChartFieldValueMap(ctProfitChartMap, "pcSales"));
        lrb.add(this.getProfitChartFieldValueMap(ctProfitChartMap, "pcDirectCost"));
        lrb.add(this.getProfitChartFieldValueMap(ctProfitChartMap, "pcGoodsProfit"));
        lrb.add(this.getProfitChartFieldValueMap(ctProfitChartMap, "pcTotal"));
        lrb.add(this.getProfitChartFieldValueMap(ctProfitChartMap, "pcProfitBeforeDep"));
        lrb.add(this.getProfitChartFieldValueMap(ctProfitChartMap, "pcDep"));
        lrb.add(this.getProfitChartFieldValueMap(ctProfitChartMap, "pcProfitBeforeInterests"));
        lrb.add(this.getProfitChartFieldValueMap(ctProfitChartMap, "pcFinanceFee"));
        lrb.add(this.getProfitChartFieldValueMap(ctProfitChartMap, "pcNonOperating"));
        lrb.add(this.getProfitChartFieldValueMap(ctProfitChartMap, "pcProfitBeforeTax"));
        lrb.add(this.getProfitChartFieldValueMap(ctProfitChartMap, "pcTax"));
        lrb.add(this.getProfitChartFieldValueMap(ctProfitChartMap, "pcAnnualNetProfit"));
        returnMap.put("lrb", lrb);

        // 资产负债表
        QueryWrapper<NewCtBalance> wrapper3 = new QueryWrapper<>();
        wrapper3.eq("contest_id", contestId).eq("student_id", studentId);
        List<NewCtBalance> ctBalanceList = ctBalanceMapper.selectList(wrapper3);
        Map<Integer, NewCtBalance> ctBalanceMap = new HashMap<>();
        for (NewCtBalance ctBalance : ctBalanceList) {
            Integer num = ctBalance.getBsYear() * 10 + ctBalance.getBsIsxt();
            NewCtBalance balance = ctBalanceMap.get(num);
            if (balance == null) {
                ctBalanceMap.put(num, ctBalance);
            } else {
                balance.add(ctBalance);
            }
        }
        // 数据放到list中
        List<Map<String, Object>> zcfzb = new ArrayList<>(32);
        zcfzb.add(this.getBalanceFieldValueMap(ctBalanceMap, "bsCash"));
        zcfzb.add(this.getBalanceFieldValueMap(ctBalanceMap, "bsReceivable"));
        zcfzb.add(this.getBalanceFieldValueMap(ctBalanceMap, "bsProductInProcess"));
        zcfzb.add(this.getBalanceFieldValueMap(ctBalanceMap, "bsProduct"));
        zcfzb.add(this.getBalanceFieldValueMap(ctBalanceMap, "bsMaterial"));
        zcfzb.add(this.getBalanceFieldValueMap(ctBalanceMap, "bsTotalCurrentAsset"));
        zcfzb.add(this.getBalanceFieldValueMap(ctBalanceMap, "bsEquipment"));
        zcfzb.add(this.getBalanceFieldValueMap(ctBalanceMap, "bsProjectOnConstruction"));
        zcfzb.add(this.getBalanceFieldValueMap(ctBalanceMap, "bsTotalFixedAsset"));
        zcfzb.add(this.getBalanceFieldValueMap(ctBalanceMap, "bsTotalAsset"));
        zcfzb.add(this.getBalanceFieldValueMap(ctBalanceMap, "bsLongLoan"));
        zcfzb.add(this.getBalanceFieldValueMap(ctBalanceMap, "bsShortLoan"));
//        // 特别贷款无
//        Map<String, Object> map = new HashMap<>();
//        for (int i = 1; i <= 6; i++) {
//            for (int j = 1; j >= 0; j--) {
//                int num = i * 10 + j;
//                map.put("t" + num, "-");
//            }
//        }
//        zcfzb.add(map);
        zcfzb.add(this.getBalanceFieldValueMap(ctBalanceMap, "bsOtherPay"));
        zcfzb.add(this.getBalanceFieldValueMap(ctBalanceMap, "bsTax"));
        zcfzb.add(this.getBalanceFieldValueMap(ctBalanceMap, "bsTotalLiability"));
        zcfzb.add(this.getBalanceFieldValueMap(ctBalanceMap, "bsEquity"));
        zcfzb.add(this.getBalanceFieldValueMap(ctBalanceMap, "bsRetainedEarning"));
        zcfzb.add(this.getBalanceFieldValueMap(ctBalanceMap, "bsAnnualNetProfit"));
        zcfzb.add(this.getBalanceFieldValueMap(ctBalanceMap, "bsTotalEquity"));
        zcfzb.add(this.getBalanceFieldValueMap(ctBalanceMap, "bsTotal"));
        returnMap.put("zcfzb", zcfzb);

        return new AsyncResult<>(returnMap);
    }

    /*
    四年广告投放
     */
    public Future<Map<String, Object>> getGGTF(Integer contestId, Integer studentId) {
        Map<String, Object> returnMap = new HashMap<>();
        SysUser sysUser = sysUserService.getById(studentId);
        if (sysUser == null) {
            return new AsyncResult<>(returnMap);
        }
        String name = sysUser.getName();
        List<NewCtMnAd> ctMnAdList = ctMnAdService.getAdListByGroupNum(contestId, studentId, sysUser.getAccount());
        Map<Integer, NewCtMnAd> ctMnAdMap = ctMnAdList.stream().collect(Collectors.toMap(p ->(p.getYear()*10+p.getQuarterly()), p -> p, (k1, k2) -> k1));
        List<Map<String, Object>> data;
        Integer[] yq = {21,22,23,24,31,32,33,34,41,42,43,44,51,52,53,54,61,62,63,64};
        Map<String, Object> map;
        for (Integer i : yq) {
            data = new ArrayList<>();
            NewCtMnAd ctMnAd = ctMnAdMap.get(i);
            if (ctMnAd == null) {
                ctMnAd = new NewCtMnAd();
            }
            map = new HashMap<>();
            map.put("local", this.convertToString(ctMnAd.getLocalP1()));
            map.put("region", this.convertToString(ctMnAd.getRegionalP1()));
            map.put("nation", this.convertToString(ctMnAd.getNationalP1()));
            map.put("asian", this.convertToString(ctMnAd.getAsianP1()));
            map.put("inter", this.convertToString(ctMnAd.getInternationalP1()));
            data.add(map);

            map = new HashMap<>();
            map.put("local", this.convertToString(ctMnAd.getLocalP2()));
            map.put("region", this.convertToString(ctMnAd.getRegionalP2()));
            map.put("nation", this.convertToString(ctMnAd.getNationalP2()));
            map.put("asian", this.convertToString(ctMnAd.getAsianP2()));
            map.put("inter", this.convertToString(ctMnAd.getInternationalP2()));
            data.add(map);

            map = new HashMap<>();
            map.put("local", this.convertToString(ctMnAd.getLocalP3()));
            map.put("region", this.convertToString(ctMnAd.getRegionalP3()));
            map.put("nation", this.convertToString(ctMnAd.getNationalP3()));
            map.put("asian", this.convertToString(ctMnAd.getAsianP3()));
            map.put("inter", this.convertToString(ctMnAd.getInternationalP3()));
            data.add(map);

            map = new HashMap<>();
            map.put("local", this.convertToString(ctMnAd.getLocalP4()));
            map.put("region", this.convertToString(ctMnAd.getRegionalP4()));
            map.put("nation", this.convertToString(ctMnAd.getNationalP4()));
            map.put("asian", this.convertToString(ctMnAd.getAsianP4()));
            map.put("inter", this.convertToString(ctMnAd.getInternationalP4()));
            data.add(map);

            map = new HashMap<>();
            map.put("local", this.convertToString(ctMnAd.getLocalP5()));
            map.put("region", this.convertToString(ctMnAd.getRegionalP5()));
            map.put("nation", this.convertToString(ctMnAd.getNationalP5()));
            map.put("asian", this.convertToString(ctMnAd.getAsianP5()));
            map.put("inter", this.convertToString(ctMnAd.getInternationalP5()));
            data.add(map);

            returnMap.put("y" + i, data);
        }
        return new AsyncResult<>(returnMap);
    }

    private Map<String, Object> getProfitChartFieldValueMap(Map<Integer, NewCtProfitChart> paramMap, String fieldName) {
        Map<String, Object> map = new HashMap<>();
        for (int i = 1; i <= 6; i++) {
            for (int j = 1; j >= 0; j--) {
                Integer num = i * 10 + j;
                NewCtProfitChart ctProfitChart = paramMap.get(num);
                if (ctProfitChart == null) {
                    map.put("t" + num, "-");
                } else {
                    map.put("t" + num, this.getFieldValueByFieldName(fieldName, ctProfitChart));
                }
            }
        }
        return map;
    }

    private Map<String, Object> getChargesFieldValueMap(Map<Integer, NewCtCharges> paramMap, String fieldName) {
        Map<String, Object> map = new HashMap<>();
        for (int i = 1; i <= 6; i++) {
            for (int j = 1; j >= 0; j--) {
                Integer num = i * 10 + j;
                NewCtCharges ctCharges = paramMap.get(num);
                if (ctCharges == null) {
                    map.put("t" + num, "-");
                } else {
                    map.put("t" + num, this.getFieldValueByFieldName(fieldName, ctCharges));
                }
            }
        }
        return map;
    }

    private Map<String, Object> getBalanceFieldValueMap(Map<Integer, NewCtBalance> paramMap, String fieldName) {
        Map<String, Object> map = new HashMap<>();
        for (int i = 1; i <= 6; i++) {
            for (int j = 1; j >= 0; j--) {
                Integer num = i * 10 + j;
                NewCtBalance ctBalance = paramMap.get(num);
                if (ctBalance == null) {
                    map.put("t" + num, "-");
                } else {
                    map.put("t" + num, this.getFieldValueByFieldName(fieldName, ctBalance));
                }
            }
        }
        return map;
    }

    /*
    反射：根据属性名获取属性值
     */
    private Integer getFieldValueByFieldName(String fieldName, Object object) {
        try {
            Field field = object.getClass().getDeclaredField(fieldName);
            //设置对象的访问权限，保证对private的属性的访问
            field.setAccessible(true);
            return (Integer) field.get(object);
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
    }

    private String formatNumber(Integer number) {
        if (number == null) {
            return "-";
        }
        return String.valueOf(number);
    }

    private String convertToString(Double localP1) {
        return localP1 == null ? "" : String.valueOf((int) Math.floor(localP1));
    }

}
