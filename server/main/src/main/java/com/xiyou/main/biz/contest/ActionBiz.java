package com.xiyou.main.biz.contest;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.xiyou.common.enums.CodeEnum;
import com.xiyou.common.utils.R;
import com.xiyou.main.dao.contest.*;
import com.xiyou.main.entity.contest.*;
import com.xiyou.main.enums.CashFlowActionEnum;
import com.xiyou.main.enums.ErrorEnum;
import com.xiyou.main.params.contest.*;
import com.xiyou.main.service.contest.*;
import com.xiyou.main.vo.contest.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author dingyoumeng
 * @since 2019/07/25
 */
@Service
public class ActionBiz {

    @Autowired
    private CtFeeService ctFeeService;
    @Autowired
    CtCashflowService ctCashflowService;
    @Autowired
    CtXzOrderService ctXzOrderService;
    @Autowired
    CtKcProductService ctKcProductService;
    @Autowired
    CtWorkshopService ctWorkshopService;
    @Autowired
    CtGzWorkshopService ctGzWorkshopService;
    @Autowired
    CtGzProductService ctGzProductService;
    @Autowired
    CtYfProductService ctYfProductService;
    @Autowired
    CtGzCsService ctGzCsService;
    @Autowired
    CtKcMaterialService ctKcMaterialService;
    @Autowired
    CtGzMaterialService ctGzMaterialService;
    @Autowired
    CtYfMarketService ctYfMarketService;
    @Autowired
    CtXzOrderMapper ctXzOrderMapper;
    @Autowired
    CtFeeMapper ctFeeMapper;
    @Autowired
    private CtGzProductMapper ctGzProductMapper;
    @Autowired
    private CtGzIsoMapper ctGzIsoMapper;
    @Autowired
    private RestoreDataBiz restoreDataBiz;


    public R checkReceivables(Integer userId, Integer contestId) {
        QueryWrapper<CtFee> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("student_id", userId);
        queryWrapper.eq("contest_id", contestId);
        queryWrapper.eq("r_remain_date", 1);
        CtFee ctFee = ctFeeService.getOne(queryWrapper);
        Map<String, Object> resultMap = new HashMap<>(16);
        resultMap.put("fee", ctFee);
        return R.success(resultMap);
    }

    @Transactional
    public R confirmReceivables(Integer userId, OnlineConfirm confirm) {
        Integer contestId = confirm.getContestId();
        QueryWrapper<CtFee> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("student_id", userId);
        queryWrapper.eq("contest_id", contestId);
        queryWrapper.eq("r_remain_date", 1);
        CtFee ctFee = ctFeeService.getOne(queryWrapper);
        if (ctFee == null) {
            return R.success();
        }
        Integer cash = ctCashflowService.getCash(userId, contestId);
        int remain = cash + ctFee.getRFee();
        //插入消费记录
        CtCashflow ctCashflow = new CtCashflow();
        ctCashflow.setCDate(confirm.getDate())
                .setCSurplus(remain)
                .setCIn(ctFee.getRFee())
                .setCOut(0)
                .setCAction(CashFlowActionEnum.UPDATE_RECEIVABLE.getAction())
                .setContestId(confirm.getContestId())
                .setStudentId(userId)
                .setCComment("更新应收款" + ctFee.getRFee() + "W");
        ctCashflowService.save(ctCashflow);
        // Fee中，更新，账期为2的赋值给1，账期为3的赋值给2，账期为4的赋值给3
        List<CtFee> ctFeeList = ctFeeMapper.getList(userId, contestId);
        for (int i = 0; i < ctFeeList.size(); i++) {
            CtFee fee1 = ctFeeList.get(i);
            if (i == ctFeeList.size() - 1) {
                fee1.setRFee(0);
            } else {
                CtFee fee2 = ctFeeList.get(i + 1);
                fee1.setRFee(fee2.getRFee());
            }
        }
        ctFeeService.updateBatchById(ctFeeList);
        return R.success();
    }

    public R listOrder(Integer userId, Integer contestId, Integer date) {
        Map<String, Object> queryMap = new HashMap<>();
        queryMap.put("studentId", userId);
        queryMap.put("contestId", contestId);
        queryMap.put("year", date / 10);
        queryMap.put("date", date);
        //交单时间为空
        queryMap.put("commitDate", 0);
        List<XzOrderVo> list = ctXzOrderService.listDeliveryOrder(queryMap);
        List<CtGzIso> ctGzIsoList = ctGzIsoMapper.list(contestId);
        Map<Integer, String> isoNameMap = ctGzIsoList.stream().collect(Collectors.toMap(CtGzIso::getCiId, CtGzIso::getCiName, (k1, k2) -> k1));
        list.forEach(p -> {
            if (p.getIsoId() == null) {
                p.setCiName("-");
            } else if (p.getIsoId() == 3) {
                p.setCiName(isoNameMap.get(1) + " / " + isoNameMap.get(2));
            } else {
                p.setCiName(isoNameMap.get(p.getIsoId()));
            }
        });
        Map<String, Object> resultMap = new HashMap<>(16);
        resultMap.put("list", list);
        return R.success(resultMap);
    }

    @Transactional
    public R deliveryOrder(Integer userId, Integer contestId, Integer orderId, Integer date) {
        CtXzOrder order = ctXzOrderService.getById(orderId);
        if (order == null) {
            return R.error(CodeEnum.PARAM_ERROR.getCode(), "无此订单");
        }
        CtGzProduct ctGzProduct = ctGzProductMapper.get(contestId, order.getCpId());
        if (ctGzProduct == null) {
            return R.error(CodeEnum.PARAM_ERROR.getCode(), "无此订单");
        }
        QueryWrapper<CtKcProduct> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("student_id", userId);
        queryWrapper.eq("contest_id", contestId);
        queryWrapper.eq("ip_cp_id", ctGzProduct.getId());
        CtKcProduct product = ctKcProductService.getOne(queryWrapper);
        if (product == null) {
            return ErrorEnum.PRODUCT_NOT_ENOUGH.getR();
        }
        int remain = product.getIpNum();
        if (remain < order.getNum()) {
            return ErrorEnum.PRODUCT_NOT_ENOUGH.getR();
        }
        order.setCommitDate(date);
        ctXzOrderService.updateById(order);
        product.setIpNum(product.getIpNum() - order.getNum());
        ctKcProductService.updateById(product);
        int paymentDate = order.getPaymentDate();
        int cash = ctCashflowService.getCash(userId, contestId);
        if (paymentDate == 0) {
            CtCashflow ctCashflow = new CtCashflow();
            ctCashflow.setCDate(date)
                    .setCSurplus(cash + order.getTotalPrice())
                    .setCIn(order.getTotalPrice())
                    .setCOut(0)
                    .setCAction(CashFlowActionEnum.DELIVERY_ORDER.getAction())
                    .setContestId(contestId)
                    .setStudentId(userId)
                    .setCComment("交货订单" + order.getTotalPrice() + "W");
            ctCashflowService.save(ctCashflow);
        } else {
            QueryWrapper<CtFee> query = new QueryWrapper<>();
            query.eq("student_id", userId);
            query.eq("contest_id", contestId);
            query.eq("r_remain_date", paymentDate);
            CtFee ctFee = ctFeeService.getOne(query);
            int fee = ctFee.getRFee();
            ctFee.setRFee(fee + order.getTotalPrice());
            ctFeeService.updateById(ctFee);
        }
        return R.success();
    }

    public R workshopSellList(Integer userId, Integer contestId) {
        Map<String, Object> queryMap = new HashMap<>();
        queryMap.put("studentId", userId);
        queryMap.put("contestId", contestId);
        queryMap.put("wStatus", 0);
        List<WorkshopVo> list = ctWorkshopService.list(queryMap);
        Map<String, Object> resultMap = new HashMap<>(16);
        resultMap.put("list", list);
        return R.success(resultMap);
    }

    public R workshopSurrenderList(Integer userId, Integer contestId) {
        Map<String, Object> queryMap = new HashMap<>();
        queryMap.put("studentId", userId);
        queryMap.put("contestId", contestId);
        queryMap.put("wStatus", 1);
        List<WorkshopVo> list = ctWorkshopService.list(queryMap);
        list = list.stream().filter(p -> p.getCwCapacity().equals(p.getWSurplusCapacity())).collect(Collectors.toList());
        Map<String, Object> resultMap = new HashMap<>(16);
        resultMap.put("list", list);
        return R.success(resultMap);
    }

    public R zzmList(Integer userId, Integer contestId, Integer date) {
        Map<String, Object> queryMap = new HashMap<>();
        queryMap.put("studentId", userId);
        queryMap.put("contestId", contestId);
        queryMap.put("wStatus", 1);
        if (date != null) {
            queryMap.put("payDate", date - 10);
        }
        List<WorkshopVo> list = ctWorkshopService.list(queryMap);
        Map<String, Object> resultMap = new HashMap<>(16);
        resultMap.put("list", list);
        return R.success(resultMap);
    }

    @Transactional
    public R workshopSell(Integer userId, Integer contestId, Integer workshopId, Integer date) {
        CtWorkshop workshop = ctWorkshopService.getById(workshopId);
        CtGzWorkshop ws = ctGzWorkshopService.getById(workshop.getWCwid());
        int cash = ctCashflowService.getCash(userId, contestId);
        if (workshop.getWSurplusCapacity().compareTo(ws.getCwCapacity()) != 0) {
            if (cash < ws.getCwRentFee()) {
                return ErrorEnum.CASH_NOT_ENOUGH.getR();
            }
            CtCashflow ctCashflow = new CtCashflow();
            ctCashflow.setCDate(date)
                    .setCSurplus(cash - ws.getCwRentFee())
                    .setCIn(0)
                    .setCOut(ws.getCwRentFee())
                    .setCAction(CashFlowActionEnum.BUY_TO_RENT_WORKSHOP.getAction())
                    .setContestId(contestId)
                    .setStudentId(userId)
                    .setCComment("买转租厂房：支付租金" + ws.getCwRentFee());
            ctCashflowService.save(ctCashflow);
            workshop.setWStatus(1);
            workshop.setWPayDate(date);
            ctWorkshopService.updateById(workshop);
            QueryWrapper<CtFee> queryWrapper = new QueryWrapper<>();
            queryWrapper.eq("student_id", userId);
            queryWrapper.eq("contest_id", contestId);
            queryWrapper.eq("r_remain_date", 4);
            CtFee ctFee = ctFeeService.getOne(queryWrapper);
            int fee = ctFee.getRFee();
            ctFee.setRFee(fee + ws.getCwSellFee());
            ctFeeService.updateById(ctFee);
        } else {
            QueryWrapper<CtFee> queryWrapper = new QueryWrapper<>();
            queryWrapper.eq("student_id", userId);
            queryWrapper.eq("contest_id", contestId);
            queryWrapper.eq("r_remain_date", 4);
            CtFee ctFee = ctFeeService.getOne(queryWrapper);
            int fee = ctFee.getRFee();
            ctFee.setRFee(fee + ws.getCwSellFee());
            ctFeeService.updateById(ctFee);
            ctWorkshopService.removeById(workshopId);
        }
        return R.success();
    }


    public R surrender(Integer workshopId) {
        ctWorkshopService.removeById(workshopId);
        return R.success();
    }

    //zzm - 租转买
    @Transactional
    public R zzm(Integer userId, Integer contestId, Integer workshopId, Integer date) {
        int cash = ctCashflowService.getCash(userId, contestId);
        CtWorkshop workshop = ctWorkshopService.getById(workshopId);
        CtGzWorkshop ws = ctGzWorkshopService.getById(workshop.getWCwid());
        if (cash < ws.getCwBuyFee()) {
            return ErrorEnum.CASH_NOT_ENOUGH.getR();
        }
        CtCashflow ctCashflow = new CtCashflow();
        ctCashflow.setCDate(date)
                .setCSurplus(cash - ws.getCwBuyFee())
                .setCIn(0)
                .setCOut(ws.getCwBuyFee())
                .setCAction(CashFlowActionEnum.BUY_WORKSHOP.getAction())
                .setContestId(contestId)
                .setStudentId(userId)
                .setCComment("购买厂房" + ws.getCwBuyFee());
        ctCashflowService.save(ctCashflow);
        workshop.setWPayDate(0)
                .setWStatus(0);
        ctWorkshopService.updateById(workshop);
        return R.success();
    }

    public R yfProductList(Integer userId, Integer contestId) {
        List<YfProductVO> list = ctGzProductService.listYfProduct(userId, contestId);
        Map<String, Object> resultMap = new HashMap<>(16);
        resultMap.put("list", list);
        return R.success(resultMap);
    }

    @Transactional
    public R yfProduct(Integer userId, YfParam yfParam) {
        List<Integer> cpIds = yfParam.getCpIds();
        List<CtGzProduct> productList = ctGzProductService.getListByIds(cpIds);
        int depFeeSum = productList.stream()
                .mapToInt(CtGzProduct::getCpProcessingFee)
                .sum();
        int cash = ctCashflowService.getCash(userId, yfParam.getContestId());
        int remain = cash - depFeeSum;
        if (remain < 0) {
            return ErrorEnum.CASH_NOT_ENOUGH.getR();
        }
        CtCashflow ctCashflow = new CtCashflow();
        ctCashflow.setCDate(yfParam.getDate())
                .setCSurplus(remain)
                .setCIn(0)
                .setCOut(depFeeSum)
                .setCAction(CashFlowActionEnum.DEVELOP_PRODUCT.getAction())
                .setContestId(yfParam.getContestId())
                .setStudentId(userId)
                .setCComment("扣减产品研发" + depFeeSum + "W");
        ctCashflowService.save(ctCashflow);
        for (CtGzProduct product : productList) {
            QueryWrapper<CtYfProduct> queryWrapper = new QueryWrapper<>();
            queryWrapper.eq("student_id", userId);
            queryWrapper.eq("contest_id", yfParam.getContestId());
            queryWrapper.eq("dp_cp_id", product.getId());
            CtYfProduct p = ctYfProductService.getOne(queryWrapper);
            if (p != null) {
                Integer remainDate = p.getDpRemainDate();
                if (remainDate != null && remainDate != 0) {
                    p.setDpRemainDate(remainDate - 1);
                    Integer nowDate = p.getDpNowDate();
                    p.setDpNowDate(nowDate + 1);
                    ctYfProductService.updateById(p);
                }
            }
        }
        return R.success();
    }

    @Transactional
    public R endingSeason(Integer userId, Integer contestId, Integer date) {
        int cash = ctCashflowService.getCash(userId, contestId);

        Map<String, Object> queryMap = new HashMap<>();
        queryMap.put("studentId", userId);
        queryMap.put("contestId", contestId);
        queryMap.put("date", date - 10);
        queryMap.put("wStatus", 1);
        Integer rent = ctWorkshopService.queryWorkshopRentMoney(queryMap);

        if (rent == null) {
            rent = 0;
        }

        CtGzCs gzCs = ctGzCsService.getByContestId(contestId);

        int remain = cash - rent - gzCs.getOverhaul();
        if (remain < 0) {
            return ErrorEnum.CASH_NOT_ENOUGH.getR();
        }
        CtCashflow ctCashflow = new CtCashflow();
        ctCashflow.setCDate(date)
                .setCSurplus(cash - gzCs.getOverhaul())
                .setCIn(0)
                .setCOut(gzCs.getOverhaul())
                .setCAction(CashFlowActionEnum.PAY_OVERHAUL.getAction())
                .setContestId(contestId)
                .setStudentId(userId)
                .setCComment("支付管理费" + gzCs.getOverhaul() + "W");
        ctCashflowService.save(ctCashflow);

        CtCashflow ctCashflow2 = new CtCashflow();
        ctCashflow2.setCDate(date)
                .setCSurplus(cash - gzCs.getOverhaul() - rent)
                .setCIn(0)
                .setCOut(rent)
                .setCAction(CashFlowActionEnum.RENT_WORKSHOP.getAction())
                .setContestId(contestId)
                .setStudentId(userId)
                .setCComment("支付厂房租金" + rent + "W");
        ctCashflowService.save(ctCashflow2);

        // 更新最后付租时间
        ctWorkshopService.updateWpayDate(queryMap);

        // 更新产品研发
        ctYfProductService.updateEndingSeason(userId, contestId, date);
        //备份当季数据
        restoreDataBiz.backupSeasonData(contestId, userId);
        return R.success();
    }

    public R showFee(Integer userId, Integer contestId) {
        QueryWrapper<CtFee> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("student_id", userId);
        queryWrapper.eq("contest_id", contestId);
        queryWrapper.orderByAsc("r_remain_date");
        List<CtFee> list = ctFeeService.list(queryWrapper);
        Map<String, Object> resultMap = new HashMap<>(16);
        resultMap.put("list", list);
        return R.success(resultMap);
    }

    @Transactional
    public R discount(DiscountParam discountParam, Integer userId) {
        QueryWrapper<CtFee> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("student_id", userId);
        queryWrapper.eq("contest_id", discountParam.getContestId());
        queryWrapper.orderByAsc("r_remain_date");
        List<CtFee> list = ctFeeService.list(queryWrapper);
        if (list.get(0).getRFee() < discountParam.getFee1()
                || list.get(1).getRFee() < discountParam.getFee2()
                || list.get(2).getRFee() < discountParam.getFee3()
                || list.get(3).getRFee() < discountParam.getFee4()) {
            return ErrorEnum.DISCOUNT_ERROR.getR();
        }
        CtGzCs ctGzCs = ctGzCsService.getByContestId(discountParam.getContestId());
        int cash = ctCashflowService.getCash(userId, discountParam.getContestId());
        int sumFee = discountParam.getFee1() + discountParam.getFee2()
                + discountParam.getFee3() + discountParam.getFee4();
        int out = (int) (Math.ceil((double) (discountParam.getFee1() + discountParam.getFee2()) * ctGzCs.getDiscount12() / 100.0)
                + Math.ceil((double) (discountParam.getFee3() + discountParam.getFee4()) * ctGzCs.getDiscount34() / 100.0));
        int remain = cash + sumFee - out;

        CtCashflow ctCashflow = new CtCashflow();
        ctCashflow.setCDate(discountParam.getDate())
                .setCSurplus(remain)
                .setCIn(sumFee)
                .setCOut(out)
                .setCAction(CashFlowActionEnum.DISCOUNT.getAction())
                .setContestId(discountParam.getContestId())
                .setStudentId(userId)
                .setCComment("贴现");
        ctCashflowService.save(ctCashflow);

        CtFee fee1 = list.get(0);
        CtFee fee2 = list.get(1);
        CtFee fee3 = list.get(2);
        CtFee fee4 = list.get(3);
        fee1.setRFee(fee1.getRFee() - discountParam.getFee1());
        fee2.setRFee(fee2.getRFee() - discountParam.getFee2());
        fee3.setRFee(fee3.getRFee() - discountParam.getFee3());
        fee4.setRFee(fee4.getRFee() - discountParam.getFee4());
        ctFeeService.updateBatchById(list);
        return R.success();
    }

    public R listKc(Integer contestId, Integer userId) {
        List<MaterialVo> kcMaterialList = ctKcMaterialService.listKc(contestId, userId);
        Map<String, Object> resultMap = new HashMap<>(16);
        List<ProductVo> kcProductList = ctKcProductService.listKc(contestId, userId);
        resultMap.put("kcMaterialList", kcMaterialList);
        resultMap.put("kcProductList", kcProductList);
        return R.success(resultMap);
    }

    @Transactional
    public R buyMaterial(Integer userId, UrgentBuyParam param) {
        List<UrgentBuyParam.Material> materials = param.getMaterials();
        List<MaterialVo> kcMaterialList = ctKcMaterialService.listKc(param.getContestId(), userId);
        Map<Integer, Integer> map = new HashMap<>(16);
        for (MaterialVo m : kcMaterialList) {
            map.put(m.getImCmId(), m.getUrgentPrice());
        }

        int cash = ctCashflowService.getCash(userId, param.getContestId());
        int out = 0;
        for (UrgentBuyParam.Material m : materials) {
            if (m.getNum() > 0) {
                out += m.getNum() * map.get(m.getImCmId());
            }
        }
        if (out == 0) {
            return R.success();
        }
        int remain = cash - out;
        if (remain < 0) {
            return ErrorEnum.CASH_NOT_ENOUGH.getR();
        }
        CtGzCs ctGzCs = ctGzCsService.getByContestId(param.getContestId());
        // 紧急采购成本
        int cost = out / ctGzCs.getEmergenceMultipleMaterial();
        // 紧急采购损失
        int loss = out - cost;
        CtCashflow ctCashflow = new CtCashflow();
        ctCashflow.setCDate(param.getDate())
                .setCSurplus(cash - cost)
                .setCIn(0)
                .setCOut(cost)
                .setCAction(CashFlowActionEnum.URGENT_MATERIAL_COST.getAction())
                .setContestId(param.getContestId())
                .setStudentId(userId)
                .setCComment("原材料紧急采购成本" + cost + "W");
        ctCashflowService.save(ctCashflow);

        CtCashflow ctCashflow2 = new CtCashflow();
        ctCashflow2.setCDate(param.getDate())
                .setCSurplus(cash - cost - loss)
                .setCIn(0)
                .setCOut(loss)
                .setCAction(CashFlowActionEnum.URGENT_MATERIAL_LOSS.getAction())
                .setContestId(param.getContestId())
                .setStudentId(userId)
                .setCComment("原材料紧急采购损失" + loss + "W");
        ctCashflowService.save(ctCashflow2);

        Map<Integer, Integer> materialMap = new HashMap<>(16);
        for (UrgentBuyParam.Material m : materials) {
            materialMap.put(m.getImCmId(), m.getNum());
        }
        QueryWrapper<CtKcMaterial> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("student_id", userId);
        queryWrapper.eq("contest_id", param.getContestId());
        List<CtKcMaterial> ctKcMaterialList = ctKcMaterialService.list(queryWrapper);
        //更新库存
        for (CtKcMaterial m : ctKcMaterialList) {
            m.setImNum(m.getImNum() + materialMap.get(m.getImCmId()));
        }
        ctKcMaterialService.updateBatchById(ctKcMaterialList);
        return R.success();
    }

    @Transactional
    public R buyProduct(Integer userId, UrgentBuyParam param) {
        List<UrgentBuyParam.Product> products = param.getProducts();
        List<ProductVo> kcMaterialList = ctKcProductService.listKc(param.getContestId(), userId);
        Map<Integer, Integer> map = new HashMap<>(16);
        for (ProductVo m : kcMaterialList) {
            map.put(m.getIpCpId(), m.getUrgentPrice());
        }

        int cash = ctCashflowService.getCash(userId, param.getContestId());
        int out = 0;
        for (UrgentBuyParam.Product m : products) {
            if (m.getNum() > 0) {
                out += m.getNum() * map.get(m.getIpCpId());
            }
        }
        if (out == 0) {
            return R.success();
        }
        int remain = cash - out;
        if (remain < 0) {
            return ErrorEnum.CASH_NOT_ENOUGH.getR();
        }
        CtGzCs ctGzCs = ctGzCsService.getByContestId(param.getContestId());
        // 紧急采购成本
        int cost = out / ctGzCs.getEmergenceMultipleProduct();
        // 紧急采购损失
        int loss = out - cost;
        CtCashflow ctCashflow = new CtCashflow();
        ctCashflow.setCDate(param.getDate())
                .setCSurplus(cash - cost)
                .setCIn(0)
                .setCOut(cost)
                .setCAction(CashFlowActionEnum.URGENT_PRODUCT_COST.getAction())
                .setContestId(param.getContestId())
                .setStudentId(userId)
                .setCComment("产成品紧急采购成本" + cost + "W");
        ctCashflowService.save(ctCashflow);

        CtCashflow ctCashflow2 = new CtCashflow();
        ctCashflow2.setCDate(param.getDate())
                .setCSurplus(cash - cost - loss)
                .setCIn(0)
                .setCOut(loss)
                .setCAction(CashFlowActionEnum.URGENT_PRODUCT_LOSS.getAction())
                .setContestId(param.getContestId())
                .setStudentId(userId)
                .setCComment("产成品紧急采购损失" + loss + "W");
        ctCashflowService.save(ctCashflow2);

        Map<Integer, Integer> materialMap = new HashMap<>(16);
        for (UrgentBuyParam.Product m : products) {
            materialMap.put(m.getIpCpId(), m.getNum());
        }
        QueryWrapper<CtKcProduct> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("student_id", userId);
        queryWrapper.eq("contest_id", param.getContestId());
        List<CtKcProduct> ctKcProductList = ctKcProductService.list(queryWrapper);
        //更新库存
        for (CtKcProduct m : ctKcProductList) {
            m.setIpNum(m.getIpNum() + materialMap.get(m.getIpCpId()));
        }
        ctKcProductService.updateBatchById(ctKcProductList);
        return R.success();
    }

    public R listSellKc(Integer contestId, Integer userId) {
        List<MaterialVo> kcMaterialList = ctKcMaterialService.listSellKc(contestId, userId);
        List<ProductVo> kcProductList = ctKcProductService.listSellKc(contestId, userId);
        Map<String, Object> resultMap = new HashMap<>();
        resultMap.put("kcMaterialList", kcMaterialList);
        resultMap.put("kcProductList", kcProductList);
        return R.success(resultMap);
    }

    @Transactional
    public R sellMaterial(Integer userId, UrgentBuyParam param) {
        QueryWrapper<CtKcMaterial> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("student_id", userId);
        queryWrapper.eq("contest_id", param.getContestId());
        List<CtKcMaterial> materialList = ctKcMaterialService.list(queryWrapper);

        List<MaterialVo> kcMaterialList = ctKcMaterialService.listSellKc(param.getContestId(), userId);
        Map<Integer, Double> sellPriceMap = new HashMap<>();
        Map<Integer, Integer> directPriceMap = new HashMap<>();
        Map<Integer, Integer> kcMap = new HashMap<>();
        for (MaterialVo m : kcMaterialList) {
            sellPriceMap.put(m.getImCmId(), m.getSellPrice());
            directPriceMap.put(m.getImCmId(), m.getDirectCost());
            kcMap.put(m.getImCmId(), m.getImNum());
        }
        Map<Integer, Integer> sellMap = new HashMap<>();
        List<UrgentBuyParam.Material> materials = param.getMaterials();
        for (UrgentBuyParam.Material m : materials) {
            sellMap.put(m.getImCmId(), m.getNum());
            if (m.getNum() > kcMap.get(m.getImCmId())) {
                return ErrorEnum.PRODUCT_NOT_ENOUGH.getR();
            }
        }
        // 卖的价格
        int cost = 0;
        // 本来的本钱
        int directCost = 0;
        for (UrgentBuyParam.Material m : materials) {
            cost += Math.floor((double) m.getNum() * sellPriceMap.get(m.getImCmId()));
            directCost += m.getNum() * directPriceMap.get(m.getImCmId());
        }
        int loss = directCost - cost;
        int cash = ctCashflowService.getCash(userId, param.getContestId());

        CtCashflow ctCashflow = new CtCashflow();
        ctCashflow.setCDate(param.getDate())
                .setCSurplus(cash + directCost - loss)
                .setCIn(directCost)
                .setCOut(loss)
                .setCAction(CashFlowActionEnum.SELL_MATERIAL.getAction())
                .setContestId(param.getContestId())
                .setStudentId(userId)
                .setCComment("出售原材料");
        ctCashflowService.save(ctCashflow);

        //更新库存
        for (CtKcMaterial m : materialList) {
            m.setImNum(m.getImNum() - sellMap.get(m.getImCmId()));
        }
        ctKcMaterialService.updateBatchById(materialList);
        return R.success();
    }

    @Transactional
    public R sellProduct(Integer userId, UrgentBuyParam param) {
        QueryWrapper<CtKcProduct> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("student_id", userId);
        queryWrapper.eq("contest_id", param.getContestId());
        List<CtKcProduct> productList = ctKcProductService.list(queryWrapper);

        List<ProductVo> kcProductList = ctKcProductService.listSellKc(param.getContestId(), userId);
        Map<Integer, Double> sellPriceMap = new HashMap<>();
        Map<Integer, Integer> directPriceMap = new HashMap<>();
        Map<Integer, Integer> kcMap = new HashMap<>();
        for (ProductVo m : kcProductList) {
            sellPriceMap.put(m.getIpCpId(), m.getSellPrice());
            directPriceMap.put(m.getIpCpId(), m.getDirectCost());
            kcMap.put(m.getIpCpId(), m.getIpNum());
        }
        Map<Integer, Integer> sellMap = new HashMap<>();
        List<UrgentBuyParam.Product> products = param.getProducts();
        for (UrgentBuyParam.Product m : products) {
            sellMap.put(m.getIpCpId(), m.getNum());
            if (m.getNum() > kcMap.get(m.getIpCpId())) {
                return ErrorEnum.PRODUCT_NOT_ENOUGH.getR();
            }
        }
        // 卖的价格
        int cost = 0;
        // 本来的本钱
        int directCost = 0;
        for (UrgentBuyParam.Product m : products) {
            cost += Math.floor((double) m.getNum() * sellPriceMap.get(m.getIpCpId()));
            directCost += m.getNum() * directPriceMap.get(m.getIpCpId());
        }
        int loss = directCost - cost;
        int cash = ctCashflowService.getCash(userId, param.getContestId());

        CtCashflow ctCashflow = new CtCashflow();
        ctCashflow.setCDate(param.getDate())
                .setCSurplus(cash + directCost - loss)
                .setCIn(directCost)
                .setCOut(loss)
                .setCAction(CashFlowActionEnum.SELL_PRODUCT.getAction())
                .setContestId(param.getContestId())
                .setStudentId(userId)
                .setCComment("出售产成品");
        ctCashflowService.save(ctCashflow);

        //更新库存
        for (CtKcProduct m : productList) {
            m.setIpNum(m.getIpNum() - sellMap.get(m.getIpCpId()));
        }
        ctKcProductService.updateBatchById(productList);
        return R.success();
    }

    public R listWorkshopDiscount(Integer userId, Integer contestId) {
        Map<String, Object> queryMap = new HashMap<>();
        queryMap.put("studentId", userId);
        queryMap.put("contestId", contestId);
        queryMap.put("wStatus", 0);
        List<WorkshopVo> list = ctWorkshopService.list(queryMap);
        Map<String, Object> resultMap = new HashMap<>(16);
        resultMap.put("list", list);
        return R.success(resultMap);
    }

    @Transactional
    public R workshopDiscount(Integer userId, Integer contestId, List<Integer> workshopIds, Integer date) {
        int cash = ctCashflowService.getCash(userId, contestId);
        List<WorkshopVo> workshops = ctWorkshopService.listWorkshop(workshopIds);
        int in = workshops.stream()
                .mapToInt(WorkshopVo::getCwBuyFee)
                .sum();
        CtGzCs ctGzCs = ctGzCsService.getByContestId(contestId);
        int out = (int) Math.ceil(ctGzCs.getDiscount34() / 100.0 * in);
        int remain = cash + in - out;
        CtCashflow ctCashflow = new CtCashflow();
        ctCashflow.setCDate(date)
                .setCSurplus(remain)
                .setCIn(in)
                .setCOut(out)
                .setCAction(CashFlowActionEnum.WORKSHOP_DISCOUNT.getAction())
                .setContestId(contestId)
                .setStudentId(userId)
                .setCComment("厂房贴现,增加" + (in - out) + "W");
        ctCashflowService.save(ctCashflow);

        //贴现转租
        int out2 = workshops.stream()
                .mapToInt(WorkshopVo::getCwRentFee)
                .sum();
        CtCashflow ctCashflow2 = new CtCashflow();
        ctCashflow2.setCDate(date)
                .setCSurplus(remain - out2)
                .setCIn(0)
                .setCOut(out2)
                .setCAction(CashFlowActionEnum.RENT_WORKSHOP.getAction())
                .setContestId(contestId)
                .setStudentId(userId)
                .setCComment("租用厂房" + out2 + "W");
        ctCashflowService.save(ctCashflow2);
        //刷新workshop
        List<CtWorkshop> workshopList = new ArrayList<>();
        for (WorkshopVo w : workshops) {
            CtWorkshop workshop = new CtWorkshop();
            workshop.setWStatus(1);
            workshop.setWPayDate(date);
            workshop.setWorkshopId(w.getWorkshopId());
            workshopList.add(workshop);
        }
        ctWorkshopService.updateBatchById(workshopList);

        return R.success();
    }

    public R xzorder(Integer studentId, Integer contestId, Integer date) {
        List<CtXzOrder> list = ctXzOrderMapper.getList(contestId, studentId);
        list.forEach(p -> {
            String status = "-";
            if (p.getCommitDate() != null && p.getCommitDate() > 0) {
                status = "已交货";
            } else if ((date / 10 == p.getDate() && date % 10 > p.getDeliveryDate()) || ((date / 10) > p.getDate())) {
                status = "已违约";
            } else {
                status = "未到期";
            }
            p.setStatus(status);
        });
        Map<String, Object> resultMap = new HashMap<>(16);
        resultMap.put("list", list);
        return R.success(resultMap);
    }

    public R marketList(Integer userId, Integer contestId) {
        List<MarketVo> list = ctYfMarketService.listYfMarket(userId, contestId);
        Map<String, Object> resultMap = new HashMap<>(16);
        resultMap.put("list", list);
        return R.success(resultMap);
    }

    @Transactional
    public R marketYf(Integer userId, MarketYfParam param) {
        List<MarketVo> list = ctYfMarketService.listYfMarket(userId, param.getContestId());
        Map<Integer, Integer> map = new HashMap<>(16);
        for (MarketVo m : list) {
            map.put(m.getMarketId(), m.getCmDevelopFee());
        }
        int cash = ctCashflowService.getCash(userId, param.getContestId());
        List<Integer> marketIds = param.getMarketIds();
        int sum = 0;
        for (Integer ids : marketIds) {
            Integer fee = map.get(ids);
            sum += (fee == null ? 0 : fee);
        }
        int remain = cash - sum;
        if (remain < 0) {
            return ErrorEnum.CASH_NOT_ENOUGH.getR();
        }
        CtCashflow ctCashflow = new CtCashflow();
        ctCashflow.setCDate(param.getDate())
                .setCSurplus(remain)
                .setCIn(0)
                .setCOut(sum)
                .setCAction(CashFlowActionEnum.DEVELOP_MARKET.getAction())
                .setContestId(param.getContestId())
                .setStudentId(userId)
                .setCComment("扣减市场开拓" + sum + "W");
        ctCashflowService.save(ctCashflow);
        List<CtYfMarket> markets = new ArrayList<>();

        for (MarketVo m : list) {
            for (Integer id : marketIds) {
                // 选择的市场
                if (m.getMarketId().equals(id)) {
                    markets.add(new CtYfMarket().setMarketId(m.getMarketId())
                            .setDmNowDate(m.getDmNowDate() + 1)
                            .setDmRemainDate(m.getDmRemainDate() - 1)
                            .setContestId(param.getContestId())
                            .setStudentId(userId));
                    break;
                }
            }
        }
        ctYfMarketService.updateBatchById(markets);
        return R.success();
    }


}
