package com.xiyou.main.biz.contest;

import com.xiyou.common.enums.CodeEnum;
import com.xiyou.common.utils.R;
import com.xiyou.main.entity.contest.CtBankLoan;
import com.xiyou.main.entity.contest.CtCashflow;
import com.xiyou.main.enums.CashFlowActionEnum;
import com.xiyou.main.enums.ErrorEnum;
import com.xiyou.main.service.contest.CtBalanceService;
import com.xiyou.main.service.contest.CtBankLoanService;
import com.xiyou.main.service.contest.CtCashflowService;
import com.xiyou.main.service.contest.CtGzCsService;
import com.xiyou.main.vo.contest.CashFolowEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.Map;

/**
 * @author: zhengxiaodong
 * @description: 申请长贷
 * @since: 2019-07-23 20:16:43
 **/
@Service
public class ApplyForLoanBiz {

    @Autowired
    CtCashflowService cashflowService;
    @Autowired
    CtBankLoanService ctBankLoanService;
    @Autowired
    CtGzCsService ctGzCsService;
    @Autowired
    CtBalanceService ctBalanceService;


    /**
     * 申请贷款-获取最大贷款额度&需贷款年限
     *
     * @param cashFolowEntity
     * @return
     */
    public R getMaxLoanAmountInfo(CashFolowEntity cashFolowEntity) {

        Map<String, Object> map = new HashMap<>();

        if (cashFolowEntity.getBlType() == 1) {
            // 需贷款年限
            Integer loanTerm = ctGzCsService.getLongLoanMax(cashFolowEntity.getContestId());
            map.put("loanTerm", loanTerm == null ? 0 : loanTerm);
        }
        map.put("maxLoanAmount", ctBalanceService.getMaxLoanAmount(cashFolowEntity));

        return R.success().put("data", map);
    }

    /**
     * 申请贷款-确认
     */
    @Transactional
    public R applyForLoanCommit(CashFolowEntity cashFolowEntity) {
        Integer loanAmount = cashFolowEntity.getLoanAmount();
        Integer maxLoanAmount = ctBalanceService.getMaxLoanAmount(cashFolowEntity);
        if (loanAmount == null || loanAmount < 10 || loanAmount > maxLoanAmount) {
            return ErrorEnum.LOAN_ERROR.getR();
        }

        //插入贷款记录
        CtBankLoan loan = new CtBankLoan();
        loan.setContestId(cashFolowEntity.getContestId())
                .setStudentId(cashFolowEntity.getStudentId())
                .setBlType(cashFolowEntity.getBlType())
                .setBlAddTime(cashFolowEntity.getCurrentTime())
                .setBlFee(loanAmount);
        if (cashFolowEntity.getBlType().equals(1)) {
            // 长贷
            Integer loanTerm = cashFolowEntity.getLoanTerm();
            if (loanTerm == null) {
                return R.error(CodeEnum.OTHER_ERROR.getCode(), "请选择贷款年限");
            }
            loan.setBlRemainTime(loanTerm)
                    .setBlRepaymentDate(loanTerm * 10 + cashFolowEntity.getCurrentTime());
        } else {
            // 短贷
            loan.setBlRemainTime(1).setBlRepaymentDate(10 + cashFolowEntity.getCurrentTime());
        }
        ctBankLoanService.save(loan);

        //插入现金交易记录
        Integer cash = cashflowService.getCash(cashFolowEntity.getStudentId(), cashFolowEntity.getContestId());
        CtCashflow ctCashflow = new CtCashflow();
        ctCashflow.setStudentId(cashFolowEntity.getStudentId())
                .setContestId(cashFolowEntity.getContestId())
                .setCIn(loanAmount)
                .setCOut(0)
                .setCDate(cashFolowEntity.getCurrentTime())
                .setCSurplus(cash + loanAmount);
        if (cashFolowEntity.getBlType().equals(1)) {
            ctCashflow.setCAction(CashFlowActionEnum.LONG_LOAN.getAction())
                    .setCComment("长期贷款" + loanAmount + "W");
        } else {
            ctCashflow.setCAction(CashFlowActionEnum.SHORT_LOAN.getAction())
                    .setCComment("短期贷款" + loanAmount + "W");
        }
        cashflowService.save(ctCashflow);
        return R.success();
    }
}
