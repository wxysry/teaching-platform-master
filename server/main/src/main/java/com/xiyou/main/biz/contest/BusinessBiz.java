package com.xiyou.main.biz.contest;

import com.xiyou.common.enums.CodeEnum;
import com.xiyou.common.utils.R;
import com.xiyou.main.dao.contest.*;
import com.xiyou.main.entity.contest.*;
import com.xiyou.main.service.contest.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @program: multi-module
 * @description:
 * @author: tangcan
 * @create: 2019-08-26 15:10
 **/
@Service
public class BusinessBiz {
    @Autowired
    private ContestStudentService contestStudentService;
    @Autowired
    private ContestStudentMapper contestStudentMapper;
    @Autowired
    private CtBalanceService ctBalanceService;
    @Autowired
    private CtCashflowService ctCashflowService;
    @Autowired
    private CtGzIsoMapper ctGzIsoMapper;
    @Autowired
    private CtGzProductMapper ctGzProductMapper;
    @Autowired
    private CtYfProductMapper ctYfProductMapper;
    @Autowired
    private CtGzMarketMapper ctGzMarketMapper;
    @Autowired
    private CtGzMaterialMapper ctGzMaterialMapper;
    @Autowired
    private CtKcMaterialMapper ctKcMaterialMapper;
    @Autowired
    private CtKcProductMapper ctKcProductMapper;
    @Autowired
    private CtDdMaterialMapper ctDdMaterialMapper;
    @Autowired
    private CtFeeMapper ctFeeMapper;
    @Autowired
    private CtTTimeService ctTTimeService;
    @Autowired
    private CtMnChooseMapper ctMnChooseMapper;
    @Autowired
    private CtMnAdMapper ctMnAdMapper;
    @Autowired
    private CtGzOrderMapper ctGzOrderMapper;
    @Autowired
    private CtGzAdMapper ctGzAdMapper;
    @Autowired
    private RestoreDataBiz restoreDataBiz;
    @Autowired
    private ContestService contestService;
    @Autowired
    private CtYfIsoMapper ctYfIsoMapper;
    @Autowired
    private CtYfMarketMapper ctYfMarketMapper;
    @Autowired
    private CtSubjectMapper ctSubjectMapper;

    @Transactional
    public R start(Integer studentId, Integer contestId) {
        ContestStudent contestStudent = contestStudentService.get(contestId, studentId);
        if (contestStudent == null) {
            return R.error(CodeEnum.OTHER_ERROR.getCode(), "竞赛不存在");
        }
        if (contestStudent.getStart() == 1) {
            return R.error(CodeEnum.OTHER_ERROR.getCode(), "已开始经营");
        }
        if (contestStudent.getFinishTime() != null) {
            return R.error(CodeEnum.OTHER_ERROR.getCode(), "已完成该竞赛模拟");
        }
        // 初始权益作为初始资金
        Contest contest = contestService.getById(contestId);

        Integer cash = contest == null ? 0 : contest.getEquity();

        // balance新增一条记录
        // 学生ID、考试ID、1、0、规则表该套题中的cash、0、0、0、0、规则表该套题中的cash、0、0、0、0、规则表该套题中的cash、0、0、0、0、规则表该套题中的cash、0、0、规则表该套题中的cash、规则表该套题中的cash
        CtBalance ctBalance = new CtBalance()
                .setStudentId(studentId)
                .setContestId(contestId)
                .setBsIsxt(1)
                .setBsYear(0)
                .setBsCash(cash)
                .setBsReceivable(0)
                .setBsProductInProcess(0)
                .setBsProduct(0)
                .setBsMaterial(0)
                .setBsTotalCurrentAsset(cash)
                .setBsWorkshop(0)
                .setBsEquipment(0)
                .setBsProjectOnConstruction(0)
                .setBsTotalFixedAsset(0)
                .setBsTotalAsset(cash)
                .setBsLongLoan(0)
                .setBsShortLoan(0)
                .setBsTax(0)
                .setBsTotalLiability(0)
                .setBsEquity(cash)
                .setBsRetainedEarning(0)
                .setBsAnnualNetProfit(0)
                .setBsTotalEquity(cash)
                .setBsTotal(cash);
        ctBalanceService.save(ctBalance);

        // Cashflow新增一条记录，具体为：
        // 学生ID、考试ID、顺序号、开始经营、规则表该套题中的cash、0、规则表该套题中的cash、公司成立、10
        CtCashflow cashflow = new CtCashflow()
                .setStudentId(studentId)
                .setContestId(contestId)
                .setCAction("开始经营")
                .setCIn(cash)
                .setCOut(0)
                .setCSurplus(cash)
                .setCComment("公司成立")
                .setCDate(10);
        ctCashflowService.save(cashflow);

        // ISO研发情况新增记录，具体为：
        // 该套规则中含有的所有ISO编号各生成一条记录，ISO编号=ISO编号，总研发时间=研发周期，已研发时间=0，剩余研发时间=总研发周期，完成时间 is null
        List<CtGzIso> ctGzIsoList = ctGzIsoMapper.list(contestId);
        List<CtYfIso> ctYfIsoList = new ArrayList<>();
        for (CtGzIso ctGzIso : ctGzIsoList) {
            ctYfIsoList.add(new CtYfIso()
                    .setStudentId(studentId)
                    .setContestId(contestId)
                    //直接关联id，取消关联编号
                    .setDiCiid(ctGzIso.getId())
                    .setDiTotalDate(ctGzIso.getCiDevelopDate())
                    .setDiNowDate(0)
                    .setDiRemainDate(ctGzIso.getCiDevelopDate())
                    .setDiFinishDate(null));
        }
        if (ctYfIsoList.size() > 0) {
            ctYfIsoMapper.insertBatch(ctYfIsoList);
        }
        // 市场研发、产品研发同ISO
        List<CtGzMarket> ctGzMarketList = ctGzMarketMapper.list(contestId);
        List<CtYfMarket> ctYfMarketList = new ArrayList<>();
        for (CtGzMarket ctGzMarket : ctGzMarketList) {
            ctYfMarketList.add(new CtYfMarket()
                    .setStudentId(studentId)
                    .setContestId(contestId)
                    //直接关联id，取消关联编号
                    .setDmCmId(ctGzMarket.getId())
                    .setDmTotalDate(ctGzMarket.getCmDevelopDate())
                    .setDmNowDate(0)
                    .setDmRemainDate(ctGzMarket.getCmDevelopDate())
                    .setDmFinishDate(null));
        }
        if (ctYfMarketList.size() > 0) {
            ctYfMarketMapper.insertBatch(ctYfMarketList);
        }

        List<CtGzProduct> ctGzProductList = ctGzProductMapper.getList(contestId);
        List<CtYfProduct> ctYfProductList = new ArrayList<>();
        for (CtGzProduct ctGzProduct : ctGzProductList) {
            ctYfProductList.add(new CtYfProduct()
                    .setStudentId(studentId)
                    .setContestId(contestId)
                    //取消关联编号，直接关联主键
                    .setDpCpId(ctGzProduct.getId())
                    .setDpTotalDate(ctGzProduct.getCpDevelopDate())
                    .setDpNowDate(0)
                    .setDpRemainDate(ctGzProduct.getCpDevelopDate())
                    .setDpFinishDate(null));
        }
        if (ctYfProductList.size() > 0) {
            ctYfProductMapper.insertBatch(ctYfProductList);
        }

        // 原料库存新增记录，具体为：
        // 该套规则中含有的所有原材料各生成一条记录，原料编号=原料编号，剩余数量=0
        List<CtGzMaterial> ctGzMaterialList = ctGzMaterialMapper.list(contestId);
        List<CtKcMaterial> ctKcMaterialList = new ArrayList<>();
        for (CtGzMaterial ctGzMaterial : ctGzMaterialList) {
            ctKcMaterialList.add(new CtKcMaterial()
                    .setStudentId(studentId)
                    .setContestId(contestId)
                    //直接关联id，取消关联编号
                    .setImCmId(ctGzMaterial.getId())
                    .setImNum(0));
        }
        if (ctKcMaterialList.size() > 0) {
            ctKcMaterialMapper.insertBatch(ctKcMaterialList);
        }

        // 产成品库存同理
        List<CtKcProduct> ctKcProductList = new ArrayList<>();
        for (CtGzProduct ctGzProduct : ctGzProductList) {
            ctKcProductList.add(new CtKcProduct()
                    .setStudentId(studentId)
                    .setContestId(contestId)
                    //直接关联id，取消关联编号
                    .setIpCpId(ctGzProduct.getId())
                    .setIpNum(0));
        }
        if (ctKcProductList.size() > 0) {
            ctKcProductMapper.insertBatch(ctKcProductList);
        }

        // 原料采购记录新增记录，具体为：
        // 该套规则中含有的所有原材料各生成两条记录，原料编号=原料编号，数量=0，剩余时间一条为0，一条为1，采购时间10
        List<CtDdMaterial> ctDdMaterialList = new ArrayList<>();
        for (CtGzMaterial ctGzMaterial : ctGzMaterialList) {
            ctDdMaterialList.add(new CtDdMaterial()
                    .setStudentId(studentId)
                    .setContestId(contestId)
                    //取消关联编号，直接关联主键id
                    .setOmCmid(ctGzMaterial.getId())
                    .setOmNum(0)
                    .setRemainDate(0)
                    .setPurchaseDate(10));
            ctDdMaterialList.add(new CtDdMaterial()
                    .setStudentId(studentId)
                    .setContestId(contestId)
                    //直接关联id，取消关联编号
                    .setOmCmid(ctGzMaterial.getId())
                    .setOmNum(0)
                    .setRemainDate(1)
                    .setPurchaseDate(10));
        }
        if (ctDdMaterialList.size() > 0) {
            ctDdMaterialMapper.insertBatch(ctDdMaterialList);
        }

        // 应收款表新增记录，具体为：
        // ID,ID，0，1     ID,ID,0,2     ID,ID,0,3    ID,ID,0,4
        List<CtFee> ctFeeList = new ArrayList<>();
        ctFeeList.add(new CtFee().setStudentId(studentId).setContestId(contestId).setRFee(0).setRRemainDate(1));
        ctFeeList.add(new CtFee().setStudentId(studentId).setContestId(contestId).setRFee(0).setRRemainDate(2));
        ctFeeList.add(new CtFee().setStudentId(studentId).setContestId(contestId).setRFee(0).setRRemainDate(3));
        ctFeeList.add(new CtFee().setStudentId(studentId).setContestId(contestId).setRFee(0).setRRemainDate(4));
        if (ctFeeList.size() > 0) {
            ctFeeMapper.insertBatch(ctFeeList);
        }

        // 时间表新增记录，具体为
        // ID,ID,10
        ctTTimeService.save(new CtTTime().setStudentId(studentId).setContestId(contestId).setDate(10));

        // 模拟选单mn_choose新增数据=市场定单gz_order为该套的
        List<CtMnChoose> ctMnChooseList = ctGzOrderMapper.getList(contestId);
        if (ctMnChooseList != null) {
            for (CtMnChoose ctMnChoose : ctMnChooseList) {
                ctMnChoose.setStudentId(studentId).setContestId(contestId);
            }
            if (ctMnChooseList.size() > 0) {
                ctMnChooseMapper.insertBatch(ctMnChooseList);
            }
        }

        // 模拟广告mn_ad新增数据=初始广告gz_ad为该套的
        List<CtMnAd> ctMnAdList = ctGzAdMapper.get(contestId);
        for (CtMnAd ctMnAd : ctMnAdList) {
            ctMnAd.setStudentId(studentId).setContestId(contestId);
        }
        if (ctMnAdList.size() > 0) {
            ctMnAdMapper.insertBatch(ctMnAdList);
        }

        // 更新开始状态
        contestStudentMapper.updateStart(contestId, studentId);

        // 完成以上操作后，除了规则表，其余备份到新的表。
        restoreDataBiz.backupData(contestId, studentId);
        return R.success();
    }

    public R saveProgress(Integer studentId, Integer contestId, Integer date, String progress) {
        contestStudentMapper.saveProgress(studentId, contestId, date, progress);
        return R.success();
    }

    public R getProgress(Integer studentId, Integer contestId) {
        ContestStudent contestStudent = contestStudentMapper.get(contestId, studentId);
        if (contestStudent == null) {
            return R.error(CodeEnum.PARAM_ERROR.getCode(), "竞赛不存在");
        }
        if (contestStudent.getFinishTime() != null) {
            return R.error(CodeEnum.OTHER_ERROR.getCode(), "已完成该竞赛模拟");
        }
        Contest contest = contestService.getById(contestId);
        if (contest.getOpenTimeEnd().isBefore(LocalDateTime.now())) {
            return R.error(CodeEnum.OTHER_ERROR.getCode(), "竞赛模拟已结束");
        }
        Map<String, Object> returnMap = new HashMap<>();
        returnMap.put("restore", contest.getRestore());
        returnMap.put("restoreSeason", contest.getRestoreSeason());
        returnMap.put("restart", contest.getRestart());
        returnMap.put("title", contest.getTitle());
        returnMap.put("progress", contestStudent.getProgress());
        returnMap.put("date", contestStudent.getDate());
        return R.success(returnMap);
    }

    @Transactional
    public R end(Integer studentId, Integer contestId) {
        ContestStudent contestStudent = contestStudentMapper.get(contestId, studentId);
        if (contestStudent == null) {
            return R.error(CodeEnum.OTHER_ERROR);
        }
        if (contestStudent.getFinishTime() != null) {
            return R.error(CodeEnum.OTHER_ERROR.getCode(), "已完成该竞赛模拟");
        }
        contestStudent.setFinishTime(LocalDateTime.now());
        contestStudentService.updateById(contestStudent);
        // 清除本年度备份的数据
        restoreDataBiz.removeLikeTableData(contestId, studentId);
        // 清除季度备份数据
        restoreDataBiz.removeSeasonLikeTableData(contestId,studentId);

        return R.success();
    }

    public R saveXzorder(ContestStudent contestStudent) {
        contestStudentMapper.saveXzorder(contestStudent);
        return R.success();
    }

    public R getXzorder(Integer studentId, Integer contestId) {
        ContestStudent contestStudent = contestStudentMapper.get(contestId, studentId);
        Contest contest = contestService.getById(contestId);
        if (contestStudent == null || contest == null) {
            return R.error(CodeEnum.PARAM_ERROR.getCode(), "竞赛不存在");
        }
        Map<String, Object> returnMap = new HashMap<>();
        returnMap.put("marketList", contestStudent.getMarketList());
        returnMap.put("activeName", contestStudent.getActiveName());
        // 是否可以重新选单
        returnMap.put("restoreOrder", contest.getRestoreOrder());
        return R.success(returnMap);
    }

    @Transactional
    public R restart(Integer studentId, Integer contestId) {
        Contest contest = contestService.getById(contestId);
        if (contest == null) {
            return R.error(CodeEnum.OTHER_ERROR.getCode(), "无此竞赛");
        }
        ContestStudent contestStudent = contestStudentService.get(contestId, studentId);
        if (contestStudent == null) {
            return R.error(CodeEnum.OTHER_ERROR.getCode(), "无此竞赛");
        }

        // 清除经营进度
        contestStudentMapper.removeProgress(contestId, studentId);

        // 删除经营数据
        ctSubjectMapper.removeLikeTableData(contestId, studentId);
        ctSubjectMapper.removeSeasonLikeTableData(contestId,studentId);
        ctSubjectMapper.removeMainTableData(contestId, studentId);

        return R.success();
    }
}
