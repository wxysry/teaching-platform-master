package com.xiyou.main.biz.contest;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.xiyou.common.enums.CodeEnum;
import com.xiyou.common.office.utils.EasyPOIUtil;
import com.xiyou.common.utils.CommonUtil;
import com.xiyou.common.utils.R;
import com.xiyou.main.dao.contest.CtSubjectMapper;
import com.xiyou.main.dao.exam.ResourcesMapper;
import com.xiyou.main.entity.contest.Contest;
import com.xiyou.main.entity.contest.ContestStudent;
import com.xiyou.main.entity.contest.CtGzCs;
import com.xiyou.main.entity.contest.CtSubject;
import com.xiyou.main.entity.exam.Resources;
import com.xiyou.main.entity.exam.SysUser;
import com.xiyou.main.params.contest.ContestParam;
import com.xiyou.main.params.contest.ContestStudentParam;
import com.xiyou.main.service.contest.ContestService;
import com.xiyou.main.service.contest.ContestStudentService;
import com.xiyou.main.service.contest.CtGzCsService;
import com.xiyou.main.service.exam.SysUserService;
import com.xiyou.main.service.exam.TeachClassService;
import com.xiyou.main.vo.contest.ContestScore;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @program: multi-module
 * @description:
 * @author: tangcan
 * @create: 2019-08-25 10:39
 **/
@Service
public class ContestBiz {
    @Autowired
    private ContestService contestService;
    @Autowired
    private TeachClassService teachClassService;
    @Autowired
    private ContestStudentService contestStudentService;
    @Autowired
    private CtSubjectMapper ctSubjectMapper;
    @Autowired
    private ResourcesMapper resourcesMapper;
    @Autowired
    private SysUserService sysUserService;
    @Autowired
    private CtGzCsService ctGzCsService;

    public R listByTeacher(ContestParam contestParam) {
        Page<Contest> page = contestService.getPage(contestParam);
        for (Contest contest : page.getRecords()) {
            // 竞赛状态
            int status;
            LocalDateTime now = LocalDateTime.now();
            if (contest.getIsPublish() == 0) {
                status = 0;
            } else if (now.isBefore(contest.getOpenTimeStart())) {
                // 未开始
                status = 1;
            } else if (now.isAfter(contest.getOpenTimeEnd())) {
                // 已结束
                status = 3;
            } else {
                // 竞赛中
                status = 2;
            }
            contest.setStatus(status);
        }
        Map<String, Object> returnMap = new HashMap<>();
        returnMap.put("total", page.getTotal());
        returnMap.put("list", page.getRecords());
        return R.success(returnMap);
    }

    public R listByStudent(ContestParam contestParam) {
        Page<Contest> page = contestService.getStudentContestPage(contestParam);
        for (Contest contest : page.getRecords()) {
            // 竞赛状态
            int status;
            LocalDateTime now = LocalDateTime.now();
            if (now.isBefore(contest.getOpenTimeStart())) {
                // 未开始
                status = 0;
            } else if (now.isAfter(contest.getOpenTimeEnd())) {
                // 已结束
                status = 2;
            } else {
                // 竞赛中
                status = 1;
            }
            contest.setStatus(status);
        }
        Map<String, Object> returnMap = new HashMap<>();
        returnMap.put("total", page.getTotal());
        returnMap.put("list", page.getRecords());
        return R.success(returnMap);
    }

    public R get(Integer contestId) {
        Map<String, Object> returnMap = new HashMap<>();
        Contest contest = contestService.getById(contestId);
        if (contest == null) {
            return R.error(CodeEnum.OTHER_ERROR.getCode(), "无此竞赛");
        }
        CtSubject ctSubject = ctSubjectMapper.getBySubjectNumber(contest.getSubjectNumber());
        if (ctSubject != null) {
            contest.setGroupId(ctSubject.getGroupId());
            contest.setStudentList(contestStudentService.getStudentIdList(contestId));
        }
        int start = 0;
        if (contest.getIsPublish() == 1 && contest.getOpenTimeStart().isBefore(LocalDateTime.now())) {
            start = 1;
        }
        returnMap.put("start", start);
        returnMap.put("info", contest);

        return R.success(returnMap);
    }

    public R add(Contest contest) {
        List<Integer> studentIdList;
        if (contest.getPublishAll() != null && contest.getPublishAll() == 1) {
            studentIdList = teachClassService.getStudentIdList(contest.getTeacherId());
        } else {
            studentIdList = contest.getStudentList();
        }
        if (studentIdList != null) {
            studentIdList = studentIdList.stream().distinct().collect(Collectors.toList());
        }
        contest.setStudentNum(studentIdList == null ? 0 : studentIdList.size());
        contestService.save(contest);
        if (studentIdList != null && studentIdList.size() > 0) {
            contestStudentService.insertBatch(contest.getContestId(), studentIdList);
        }

        return R.success().put("contestId", contest.getContestId());
    }

    public R update(Contest contest) {
        if (contest.getContestId() == null) {
            return R.error(CodeEnum.PARAM_MISS.getCode(), "请选择竞赛");
        }
        Contest contest1 = contestService.getById(contest.getContestId());
        if (contest1 == null || !contest1.getTeacherId().equals(contest.getTeacherId())) {
            return R.error(CodeEnum.PARAM_ERROR.getCode(), "无此竞赛");
        }
        if (contest1.getIsPublish() == 1 && contest1.getOpenTimeEnd().isBefore(LocalDateTime.now())) {
            return R.error(CodeEnum.PARAM_ERROR.getCode(), "竞赛模拟已结束，不能更新");
        }
        // 未开始才能修改学生
        boolean start = (contest.getIsPublish() == 1 && contest.getOpenTimeStart().isBefore(LocalDateTime.now()));
        if (!start) {
            List<Integer> studentIdList;
            if (contest.getPublishAll() != null && contest.getPublishAll() == 1) {
                studentIdList = teachClassService.getStudentIdList(contest.getTeacherId());
            } else {
                studentIdList = contest.getStudentList();
            }
            if (studentIdList != null) {
                studentIdList = studentIdList.stream().distinct().collect(Collectors.toList());
            }
            contest.setStudentNum(studentIdList == null ? 0 : studentIdList.size());

            // 先删掉原来的学生
            contestStudentService.remove(contest.getContestId());
            if (studentIdList != null && studentIdList.size() > 0) {
                // 再插入
                contestStudentService.insertBatch(contest.getContestId(), studentIdList);
            }
        }
        if (start) {
            // 开始了就不再更改初始权益
            contest.setEquity(null);
        }
        contestService.updateById(contest);
        return R.success();
    }

    public R delete(Integer contestId) {
        contestService.removeById(contestId);
        QueryWrapper<ContestStudent> wrapper = new QueryWrapper<>();
        wrapper.eq("contest_id", contestId);
        contestStudentService.remove(wrapper);
        return R.success();
    }

    public R publish(Integer teacherId, Integer contestId) {
        SysUser user = sysUserService.getById(teacherId);
        // 只能同时发布一个竞赛
        if (contestService.getUnEndContest(teacherId) >= user.getContestNum()) {
            return R.error(CodeEnum.OTHER_ERROR.getCode(), "只能同时发布 " + user.getContestNum() + " 套竞赛模拟");
        }
        Contest contest = contestService.getById(contestId);
        if (contest == null || !contest.getTeacherId().equals(teacherId)) {
            return R.error(CodeEnum.PARAM_ERROR.getCode(), "竞赛不存在");
        }
        if (contest.getIsPublish() == 1) {
            return R.error(CodeEnum.OTHER_ERROR.getCode(), "竞赛已发布！");
        }
        if (contest.getStudentNum() == 0) {
            return R.error(CodeEnum.OTHER_ERROR.getCode(), "暂无考生，不能发布！");
        }
        if (contest.getOpenTimeEnd().isBefore(LocalDateTime.now())) {
            return R.error(CodeEnum.OTHER_ERROR.getCode(), "发布失败！开放时间已过！");
        }
        contestService.publish(teacherId, contestId);
        return R.success();
    }

    public R studentList(ContestStudentParam contestStudentParam) {
        Page<ContestStudent> page = contestStudentService.getPage(contestStudentParam);
        Map<String, Object> returnMap = new HashMap<>();
        returnMap.put("total", page.getTotal());
        returnMap.put("list", page.getRecords());
        return R.success(returnMap);
    }

    public R getSpyAttachment(Integer contestId, Integer year) {
        CtSubject ctSubject = ctSubjectMapper.getByContestId(contestId);
        if (ctSubject == null || StringUtils.isBlank(ctSubject.getSpyAttachment())) {
            return R.error(CodeEnum.OTHER_ERROR.getCode(), "暂无间谍");
        }
        String[] spys = ctSubject.getSpyAttachment().split("\\|");
        if (spys.length < year || year <= 0) {
            return R.error(CodeEnum.OTHER_ERROR.getCode(), "暂无间谍");
        }
        Resources resources = resourcesMapper.get(spys[year - 1]);
        if (resources == null) {
            return R.error(CodeEnum.OTHER_ERROR.getCode(), "暂无间谍");
        }
        return R.success().put("info", resources);
    }

    public R getRuleAttachment(Integer contestId) {
        Resources resources = ctSubjectMapper.getRuleAttachment(contestId);
        if (resources == null) {
            return R.error(CodeEnum.OTHER_ERROR.getCode(), "暂无规则预测详单");
        }
        return R.success().put("info", resources);
    }

    public R studentScore(Integer id, Double score) {
        contestStudentService.updateScore(id, score);
        return R.success();
    }

    public R getEquity(Integer subjectNumber) {
        // 规则表该套题中的cash
        CtGzCs ctGzCs = ctGzCsService.getBySubjectNumber(subjectNumber);
        Integer cash = ctGzCs == null ? 0 : ctGzCs.getCash();
        return R.success().put("equity", cash);
    }

    public void downloadScore(Integer contestId, HttpServletResponse response) {
        Contest contest = contestService.getById(contestId);
        if (contest == null) {
            return;
        }
        List<ContestScore> contestScoreList = contestStudentService.getScoreList(contestId);
        Map<String, Object> paramMap = new HashMap<>();
        List<Map<String, Object>> mapList = new ArrayList<>(CommonUtil.getListInitCap(contestScoreList.size()));
        int num = 0;
        Map<String, Object> map;
        for (ContestScore contestScore : contestScoreList) {
            map = new HashMap<>();
            map.put("id", ++num);
            map.put("name", contestScore.getName());
            map.put("account", contestScore.getAccount());
            map.put("school", contestScore.getSchool());
            map.put("equity", contestScore.getEquity() == null ? "-" : (contestScore.getEquity() + "W"));
            map.put("score", contestScore.getScore() == null ? "-" : contestScore.getScore());
            mapList.add(map);
        }
        paramMap.put("maplist", mapList);
        String paperName = "《" + contest.getTitle() + "》";
        paramMap.put("title", paperName);
        try {
            EasyPOIUtil.exportMultiSheetExcelByMap(response, paperName + "竞赛成绩" + ".xlsx", CommonUtil.getClassesPath() + "templates/contest_score.xlsx", paramMap, false);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
