package com.xiyou.main.biz.contest;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.xiyou.common.enums.CodeEnum;
import com.xiyou.common.utils.R;
import com.xiyou.main.dao.contest.CtSubjectMapper;
import com.xiyou.main.entity.contest.CtSubject;
import com.xiyou.main.params.contest.SubjectParam;
import com.xiyou.main.service.contest.CtSubjectService;
import com.xiyou.main.service.exam.ResourcesService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @program: multi-module
 * @description:
 * @author: tangcan
 * @create: 2019-08-25 13:01
 **/
@Service
public class CtSubjectBiz {
    @Autowired
    private CtSubjectService ctSubjectService;
    @Autowired
    private CtSubjectMapper ctSubjectMapper;
    @Autowired
    private ResourcesService resourcesService;

    public R listByAdmin(SubjectParam subjectParam) {
        Page<CtSubject> page = ctSubjectService.listByAdmin(subjectParam);
        Map<String, Object> returnMap = new HashMap<>();
        returnMap.put("list", page.getRecords());
        returnMap.put("total", page.getTotal());
        return R.success(returnMap);
    }

    public R listByTeacher(Integer groupId) {
        List<CtSubject> list = ctSubjectService.listByTeacher(groupId);
        return R.success().put("list", list);
    }

    public R add(CtSubject ctSubject) {
        if (ctSubjectMapper.getBySubjectNumber(ctSubject.getSubjectNumber()) != null) {
            return R.error(CodeEnum.PARAM_ERROR.getCode(), "该题库号已存在");
        }
        if (ctSubject.getSpyAttachment() != null) {
            ctSubject.setSpyAttachment(ctSubject.getSpyAttachment().replaceAll(" ", ""));
        }
        if (ctSubject.getRuleAttachment() != null) {
            ctSubject.setRuleAttachment(ctSubject.getRuleAttachment().replaceAll(" ", ""));
        }
        if (StringUtils.isNotBlank(ctSubject.getRuleAttachment())) {
            if (!resourcesService.check(ctSubject.getRuleAttachment())) {
                return R.error(CodeEnum.PARAM_ERROR.getCode(), "该规则预测详单不存在");
            }
        } else {
            ctSubject.setRuleAttachment(null);
        }

        ctSubjectService.save(ctSubject);
        return R.success();
    }

    public R update(CtSubject ctSubject) {
        if (ctSubject.getSubjectId() == null) {
            return R.error(CodeEnum.PARAM_ERROR.getCode(), "请选择题库");
        }
        CtSubject subject = ctSubjectMapper.getBySubjectNumber(ctSubject.getSubjectNumber());
        if (subject != null && !subject.getSubjectId().equals(ctSubject.getSubjectId())) {
            return R.error(CodeEnum.PARAM_ERROR.getCode(), "该题库号已存在");
        } else if (subject == null) {
            ctSubject.setUpload(0);
        }
        if (ctSubject.getSpyAttachment() != null) {
            ctSubject.setSpyAttachment(ctSubject.getSpyAttachment().replaceAll(" ", ""));
        }
        if (ctSubject.getRuleAttachment() != null) {
            ctSubject.setRuleAttachment(ctSubject.getRuleAttachment().replaceAll(" ", ""));
        }
        if (StringUtils.isNotBlank(ctSubject.getRuleAttachment())) {
            if (!resourcesService.check(ctSubject.getRuleAttachment())) {
                return R.error(CodeEnum.PARAM_ERROR.getCode(), "该规则预测详单不存在");
            }
        } else {
            ctSubject.setRuleAttachment(null);
        }
        ctSubjectMapper.update(ctSubject);
        return R.success();
    }

    public R get(Integer subjectId) {
        Map<String, Object> returnMap = new HashMap<>();
        returnMap.put("info", ctSubjectMapper.getById(subjectId));
        return R.success(returnMap);
    }

    public R delete(Integer subjectId) {
        CtSubject ctSubject = ctSubjectMapper.getById(subjectId);
        if (ctSubject == null) {
            return R.success();
        }
        ctSubjectService.removeById(subjectId);
        // 删除规则表中的数据
        ctSubjectMapper.removeGz(ctSubject.getSubjectNumber());
        return R.success();
    }

    public R cascadeByTeacher(Integer teacherId) {
        return R.success().put("list", ctSubjectMapper.getTeacherSubject(teacherId));
    }
}
