package com.xiyou.main.biz.contest;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.xiyou.common.enums.CodeEnum;
import com.xiyou.common.utils.R;
import com.xiyou.main.entity.contest.*;
import com.xiyou.main.entity.exam.SysUser;
import com.xiyou.main.service.contest.*;
import com.xiyou.main.service.exam.SysUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @program: multi-module
 * @description:
 * @author: tangcan
 * @create: 2019-07-25 15:03
 **/
@Service
public class DeliverAdsBiz {
    @Autowired
    private CtYfMarketService ctYfMarketService;
    @Autowired
    private ContestStudentService contestStudentService;
    @Autowired
    private CtMnAdService ctMnAdService;
    @Autowired
    private CtCashflowService cashflowService;
    @Autowired
    private CtBankLoanService ctBankLoanService;
    @Autowired
    private CtGzCsService ctGzCsService;
    @Autowired
    private CtProfitChartService ctProfitChartService;
    @Autowired
    private CtCashflowService ctCashflowService;
    @Autowired
    private SysUserService sysUserService;
    @Autowired
    private CtChargesService ctChargesService;
    @Autowired
    private CtBalanceService ctBalanceService;

    public R finishYfMarket(Integer studentId, Integer contestId) {
        List<String> hadYfFinishMarketList = ctYfMarketService.getHadYfFinishMarkets(studentId, contestId);
        Map<String, Object> returnMap = new HashMap<>();
        returnMap.put("marketList", hadYfFinishMarketList);
        return R.success(returnMap);
    }

    @Transactional
    public R deliverAds(Integer studentId, CtMnAd mnAd) {
        if (mnAd.getStudentId() == null) {
            mnAd.setStudentId(studentId);
        }
        int now = mnAd.getYear();
        if (mnAd.getYear() > 10) {
            mnAd.setYear(mnAd.getYear() / 10);
        }
        // 所有填写金额之和
        double cashSum = mnAd.getSum();
        // 现金
        int cash = cashflowService.getCash(mnAd.getStudentId(), mnAd.getContestId());
        // 贷款记录
        List<CtBankLoan> bankLoanList = ctBankLoanService.list(studentId, mnAd.getContestId());
        // 今年到期的长贷
        int repaymentLongLoan = 0;
        // 所有长贷
        int allLongLoan = 0;
        for (CtBankLoan p : bankLoanList) {
            if (p.getBlType() == 1) {
                allLongLoan += p.getBlFee();
                if (p.getBlRepaymentDate() / 10 == now / 10) {
                    repaymentLongLoan += p.getBlFee();
                }
            }
        }

        // gz_cs.长期贷款利息
        CtGzCs ctGzCs = ctGzCsService.getByContestId(mnAd.getContestId());
        double longLoanInterestsRate = (ctGzCs == null ? 0.0 :  ctGzCs.getLongLoanInterests() / 100.0);
        int longLoanInterest = (int) Math.round(allLongLoan *longLoanInterestsRate);

        // 上一年报表计算的所得税
        List<CtProfitChart> profitChartList = ctProfitChartService.list(new CtProfitChart().setStudentId(mnAd.getStudentId()).setContestId(mnAd.getContestId()).setPcDate(mnAd.getYear() - 1));
        int lastYearTax = profitChartList.stream().mapToInt(CtProfitChart::getPcTax).sum();

        // 判断所有填写金额之和是否<现金-今年到期的长贷-长贷利息（=round(所有长贷之和*gz_cs.长期贷款利息）)-上一年报表计算的所得税
        // 如果大于，则报错【现金不足】
        if (cashSum > (cash - repaymentLongLoan - longLoanInterest - lastYearTax)) {
            return R.error(CodeEnum.OTHER_ERROR.getCode(), "现金不足");
        }

        // 设置组号为学生姓名,记录写入mn_ad
        SysUser sysUser = sysUserService.getById(studentId);
        mnAd.setGroupNum(sysUser.getName());
        // 设置本地P1+（本地填写所有广告额）*0.01，区域P1+（区域填写所有广告额）*0.01……本地P2+（本地填写所有广告额）*0.01
        mnAd.setValue();

        ctMnAdService.save(mnAd);

        // Cashflow表写入ID，支付广告费，0，所有填写金额之和，现金+流入-流出，投放广告费，当前时间
        ctCashflowService.save(new CtCashflow().setStudentId(studentId)
                .setContestId(mnAd.getContestId())
                .setCAction("支付广告费")
                .setCIn(0)
                .setCOut((int) cashSum)
                .setCSurplus(cash - (int) cashSum)
                .setCComment("投放广告费" + (int) cashSum + "W")
                .setCDate(now));
        // Cashflow表写入ID，支付所得税，0，上一年报表计算的所得税，现金+流入-流出，支付所得税，当前时间
        ctCashflowService.save(new CtCashflow().setStudentId(studentId)
                .setContestId(mnAd.getContestId())
                .setCAction("支付所得税")
                .setCIn(0)
                .setCOut(lastYearTax)
                .setCSurplus(cash - (int) cashSum - lastYearTax)
                .setCComment("支付所得税" + lastYearTax + "W")
                .setCDate(now));
        // Cashflow表写入ID，归还长期贷款，0，今年到期的长贷，现金+流入-流出，归还长期贷款，当前时间
        ctCashflowService.save(new CtCashflow().setStudentId(studentId)
                .setContestId(mnAd.getContestId())
                .setCAction("归还长期贷款")
                .setCIn(0)
                .setCOut(repaymentLongLoan)
                .setCSurplus(cash - (int) cashSum - lastYearTax - repaymentLongLoan)
                .setCComment("归还长期贷款" + repaymentLongLoan + "W")
                .setCDate(now));
        // Cashflow表写入ID，归还长期贷款利息，0，长贷利息（=round(所有长贷之和*gz_cs.长期贷款利息），现金+流入-流出，归还长期贷款利息，当前时间
        ctCashflowService.save(new CtCashflow().setStudentId(studentId)
                .setContestId(mnAd.getContestId())
                .setCAction("归还长期贷款利息")
                .setCIn(0)
                .setCOut(longLoanInterest)
                .setCSurplus(cash - (int) cashSum - lastYearTax - repaymentLongLoan - longLoanInterest)
                .setCComment("归还长期贷款利息" + longLoanInterest + "W")
                .setCDate(now));
        // 更新bank_loan表，删除还款时间等于当前时间的记录
        QueryWrapper<CtBankLoan> wrapper = new QueryWrapper<>();
        wrapper.eq("bl_repayment_date", now)
                .eq("student_id", studentId)
                .eq("contest_id", mnAd.getContestId())
                .eq("bl_type", 1);
        ctBankLoanService.remove(wrapper);

        // 已经投放广告，此时如果还没填写报表，说明报表错误
        // 填写报表的时候三张一起的，所以检查其中一张报表就行
        boolean correct = true;
        CtCharges ctCharges = ctChargesService.getOne(new CtCharges()
                .setStudentId(studentId)
                .setContestId(mnAd.getContestId())
                .setBsIsxt(0)
                .setCDate(mnAd.getYear() - 1));
        if (ctCharges == null) {
            correct = false;
        }
        // else {
        //     CtProfitChart ctProfitChart = ctProfitChartService.getOne(new CtProfitChart()
        //             .setStudentId(studentId)
        //             .setContestId(mnAd.getContestId())
        //             .setBsIsxt(0)
        //             .setPcDate(mnAd.getYear() - 1));
        //     if (ctProfitChart == null) {
        //         correct = false;
        //     } else {
        //         CtBalance ctBalance = ctBalanceService.getOne(new CtBalance()
        //                 .setStudentId(studentId)
        //                 .setContestId(mnAd.getContestId())
        //                 .setBsIsxt(0)
        //                 .setBsYear(mnAd.getYear() - 1));
        //         if (ctBalance == null) {
        //             correct = false;
        //         }
        //     }
        // }
        if (!correct) {
            ContestStudent contestStudent = contestStudentService.get(mnAd.getContestId(), studentId);
            contestStudentService.updateErrorReportYear(contestStudent, mnAd.getYear() - 1);
        }
        return R.success();
    }
}
