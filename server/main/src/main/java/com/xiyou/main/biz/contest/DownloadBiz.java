package com.xiyou.main.biz.contest;

import com.alibaba.excel.ExcelWriter;
import com.alibaba.excel.metadata.Font;
import com.alibaba.excel.metadata.Sheet;
import com.alibaba.excel.metadata.Table;
import com.alibaba.excel.metadata.TableStyle;
import com.alibaba.excel.support.ExcelTypeEnum;
import com.xiyou.common.office.utils.EasyPOIUtil;
import com.xiyou.common.utils.CommonUtil;
import com.xiyou.main.async.contest.ContestResultAsyncService;
import com.xiyou.main.dao.contest.ContestStudentMapper;
import com.xiyou.main.entity.contest.ContestStudent;
import com.xiyou.main.entity.contest.CtMnAd;
import com.xiyou.main.service.contest.CtMnAdService;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.*;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

/**
 * @program: multi-module
 * @description:
 * @author: tangcan
 * @create: 2019-08-28 14:23
 **/
@Service
public class DownloadBiz {

    @Autowired
    private CtMnAdService ctMnAdService;
    @Autowired
    private ContestStudentMapper contestStudentMapper;
    @Autowired
    private ContestResultAsyncService contestResultAsyncService;

    public void ad(HttpServletResponse response, Integer studentId, Integer contestId, Integer date) {
        String name = "第" + date / 10 + "年初广告投放";
        try {
            response.setHeader("Content-Disposition", "attachment;filename=" + URLEncoder.encode(name + ".xlsx", "utf-8"));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        ExcelWriter writer = null;
        try {
            writer = new ExcelWriter(response.getOutputStream(), ExcelTypeEnum.XLSX);
        } catch (IOException e) {
            e.printStackTrace();
        }
        assert writer != null;

        /*
        获取广告投放情况
         */
        List<CtMnAd> ctMnAdList = ctMnAdService.getAdList(contestId, studentId, date / 10);
        if (ctMnAdList.size() == 0) {
            ctMnAdList.add(new CtMnAd());
        }

        // 设置每列宽度
        Map<Integer, Integer> columnWidth = new HashMap<>();
        for (int i = 0; i < 6; i++) {
            columnWidth.put(i, 2000);
        }

        Sheet sheet = new Sheet(1, 0);
        sheet.setColumnWidthMap(columnWidth);
        sheet.setSheetName(name);
        sheet.setStartRow(1);   // 前面空一行

        /*
        表格式
         */
        TableStyle tableStyle = new TableStyle();
        Font font = new Font();
        font.setFontName("Arial");
        font.setFontHeightInPoints((short) 10);
        tableStyle.setTableHeadFont(font);
        tableStyle.setTableHeadBackGroundColor(IndexedColors.GREY_25_PERCENT);
        tableStyle.setTableContentFont(font);
        tableStyle.setTableContentBackGroundColor(IndexedColors.WHITE);

        // 表格数
        int i = 0;
        String[] strs = {"产品", "本地", "区域", "国内", "亚洲", "国际"};
        for (CtMnAd ctMnAd : ctMnAdList) {
            List<List<String>> head = new ArrayList<>();
            String title = ctMnAd.getGroupNum() + "广告投放情况";
            for (int j = 0; j < 6; j++) {
                List<String> headCoulumn = new ArrayList<>();
                // 第一行的头（名称一样则自动合并成一个单元格）
                headCoulumn.add(title);
                // 第二行的头
                headCoulumn.add(strs[j]);
                head.add(headCoulumn);
            }

            List<List<String>> data = new ArrayList<>();
            data.add(Arrays.asList("P1",
                    this.convertToString(ctMnAd.getLocalP1()),
                    this.convertToString(ctMnAd.getRegionalP1()),
                    this.convertToString(ctMnAd.getNationalP1()),
                    this.convertToString(ctMnAd.getAsianP1()),
                    this.convertToString(ctMnAd.getInternationalP1())));

            data.add(Arrays.asList("P2",
                    this.convertToString(ctMnAd.getLocalP2()),
                    this.convertToString(ctMnAd.getRegionalP2()),
                    this.convertToString(ctMnAd.getNationalP2()),
                    this.convertToString(ctMnAd.getAsianP2()),
                    this.convertToString(ctMnAd.getInternationalP2())));

            data.add(Arrays.asList("P3",
                    this.convertToString(ctMnAd.getLocalP3()),
                    this.convertToString(ctMnAd.getRegionalP3()),
                    this.convertToString(ctMnAd.getNationalP3()),
                    this.convertToString(ctMnAd.getAsianP3()),
                    this.convertToString(ctMnAd.getInternationalP3())));

            data.add(Arrays.asList("P4",
                    this.convertToString(ctMnAd.getLocalP4()),
                    this.convertToString(ctMnAd.getRegionalP4()),
                    this.convertToString(ctMnAd.getNationalP4()),
                    this.convertToString(ctMnAd.getAsianP4()),
                    this.convertToString(ctMnAd.getInternationalP4())));

            data.add(Arrays.asList("P5",
                    this.convertToString(ctMnAd.getLocalP5()),
                    this.convertToString(ctMnAd.getRegionalP5()),
                    this.convertToString(ctMnAd.getNationalP5()),
                    this.convertToString(ctMnAd.getAsianP5()),
                    this.convertToString(ctMnAd.getInternationalP5())));

            Table table1 = new Table(++i);
            table1.setHead(head);
            table1.setTableStyle(tableStyle);
            writer.write0(data, sheet, table1);
        }
        writer.finish();
    }

    private String convertToString(Double localP1) {
        return localP1 == null ? "" : String.valueOf((int) Math.floor(localP1));
    }

    public void downloadResult(HttpServletResponse response, Integer contestId, Integer studentId) {
        ContestStudent contestStudent = contestStudentMapper.get(contestId, studentId);
        if (contestStudent == null) {
            return;
        }

        Integer date = contestStudent.getDate() == null ? null : (contestStudent.getDate() / 10);

        // 库存信息
        Map<String, Object> kcxx;
        Future<Map<String, Object>> kcxxFuture = contestResultAsyncService.getKCXX(contestId, studentId);
        // 银行贷款
        Map<String, Object> yhdk;
        Future<Map<String, Object>> yhdkFuture = contestResultAsyncService.getYHDK(contestId, studentId, date);
        // 研发认证
        Map<String, Object> yfrz;
        Future<Map<String, Object>> yfrzFuture = contestResultAsyncService.getYFRZ(contestId, studentId);
        // 厂房与生产线
        Map<String, Object> cfyscx;
        Future<Map<String, Object>> cfyscxFuture = contestResultAsyncService.getCFYSCX(contestId, studentId);
        // 订单信息
        Map<String, Object> ddxx;
        Future<Map<String, Object>> ddxxFuture = contestResultAsyncService.getDDXX(contestId, studentId, date);
        // 现金流量表
        Map<String, Object> xjllb;
        Future<Map<String, Object>> xjllbFuture = contestResultAsyncService.getXJLLB(contestId, studentId);
        // 企业财务报表
        Map<String, Object> qycwbb;
        Future<Map<String, Object>> qycwbbFuture = contestResultAsyncService.getQYCWBB(contestId, studentId);
        // 四年广告投放
        Map<String, Object> ggtf;
        Future<Map<String, Object>> ggtfFuture = contestResultAsyncService.getGGTF(contestId, studentId);

        // 所有异步任务执行完成
        while (true) {
            if (kcxxFuture.isDone() &&
                    yhdkFuture.isDone() &&
                    yfrzFuture.isDone() &&
                    cfyscxFuture.isDone() &&
                    ddxxFuture.isDone() &&
                    xjllbFuture.isDone() &&
                    qycwbbFuture.isDone() &&
                    ggtfFuture.isDone()) {
                break;
            }
        }

        // 获取异步任务返回的数据
        try {
            kcxx = kcxxFuture.get();
            yhdk = yhdkFuture.get();
            cfyscx = cfyscxFuture.get();
            ddxx = ddxxFuture.get();
            yfrz = yfrzFuture.get();
            xjllb = xjllbFuture.get();
            qycwbb = qycwbbFuture.get();
            ggtf = ggtfFuture.get();
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
            return;
        }

        /*
        数据封装
         */
        Map<String, Object> param = new HashMap<>();
        param.putAll(kcxx);
        param.putAll(yhdk);
        param.putAll(yfrz);
        param.putAll(cfyscx);
        param.putAll(ddxx);
        param.putAll(xjllb);
        param.putAll(qycwbb);
        param.putAll(ggtf);

        String filePath = CommonUtil.getClassesPath() + "templates/contest_result.xlsx";
        String fileName = contestStudent.getContestName() + "-" + contestStudent.getStudentName();
        try {
            EasyPOIUtil.exportMultiSheetExcelByMap(response, fileName + ".xlsx", filePath, param, true);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
