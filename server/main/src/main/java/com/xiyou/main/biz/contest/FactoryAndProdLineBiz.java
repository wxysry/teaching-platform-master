package com.xiyou.main.biz.contest;

import com.xiyou.common.enums.CodeEnum;
import com.xiyou.common.utils.R;
import com.xiyou.main.entity.contest.CtCashflow;
import com.xiyou.main.entity.contest.CtGzWorkshop;
import com.xiyou.main.entity.contest.CtLine;
import com.xiyou.main.entity.contest.CtWorkshop;
import com.xiyou.main.service.contest.*;
import com.xiyou.main.vo.contest.CashFolowEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author: zhengxiaodong
 * @description: 租购厂房&新建、在建生产线P13-P15
 * @since: 2019-07-27 14:01:09
 **/
@Service
public class FactoryAndProdLineBiz {

    @Autowired
    CtGzWorkshopService ctGzWorkshopService;
    @Autowired
    CtGzCsService ctGzCsService;
    @Autowired
    CtCashflowService ctCashflowService;
    @Autowired
    CtWorkshopService ctWorkshopService;
    @Autowired
    CtGzProductService ctGzProductService;
    @Autowired
    CtLineService ctLineService;
    @Autowired
    CtGzProductLineService ctGzProductLineService;

    /**
     * @Description: 购租厂房->获取厂房列表
     * 如果workshop中该组记录数=gz_cs.maxworkshop，则下拉框无数据
     * @Author:zhengxiaodong
     * @Param: [contestId]
     * @Return: com.xiyou.common.utils.R
     * @Date: 2019/7/27
     */
    public R getGzWorkshopList(Integer contestId, Integer studentId) {

        List<CtWorkshop> workshopList = ctWorkshopService.getList(contestId, studentId);
        Integer maxWorkshop = ctGzCsService.getMaxWorkshop(contestId);

        if (workshopList.size() == maxWorkshop) {
            return R.success().put("data", new ArrayList<>());
        }
        return R.success().put("data", ctGzWorkshopService.getWorkshopInfo(contestId));
    }

    /**
     * @Description: 购租厂房->确认
     * @Author:zhengxiaodong
     * @Param: [cashFolowEntity]
     * @Return: com.xiyou.common.utils.R
     * @Date: 2019/7/27
     */
    @Transactional
    public R confirmBuyOrRentWorkshop(CashFolowEntity cashFolowEntity) {

        // 获取学生对应现金
        Integer cash = ctCashflowService.getCash(cashFolowEntity.getStudentId(), cashFolowEntity.getContestId());
        // 获取cwid对应的workshop信息
        CtGzWorkshop workshopInfo = ctGzWorkshopService.getById(cashFolowEntity.getCwid());

        // Workshop实体类
        CtWorkshop ctWorkshop = new CtWorkshop();

        // 购买厂房
        if (cashFolowEntity.getBuyPattern().equals(0)) {
            if (cash < workshopInfo.getCwBuyFee()) {
                return R.error(CodeEnum.OTHER_ERROR.getCode(), "现金不足");
            }
            // Cashflow表中新增
            CtCashflow ctCashflow = new CtCashflow();
            ctCashflow.setStudentId(cashFolowEntity.getStudentId())
                    .setContestId(cashFolowEntity.getContestId())
                    .setCAction("购买厂房")
                    .setCIn(0)
                    .setCOut(workshopInfo.getCwBuyFee())
                    .setCSurplus(cash - workshopInfo.getCwBuyFee())
                    .setCComment("购买厂房支出" + workshopInfo.getCwBuyFee() + "W")
                    .setCDate(cashFolowEntity.getCurrentTime());
            ctCashflowService.save(ctCashflow);
            // 租售类型：0表示BUY
            ctWorkshop.setWStatus(0);
        }
        // 租用厂房
        if (cashFolowEntity.getBuyPattern().equals(1)) {
            if (cash < workshopInfo.getCwRentFee()) {
                return R.error(CodeEnum.OTHER_ERROR.getCode(), "现金不足");
            }
            // Cashflow表中新增
            CtCashflow ctCashflow = new CtCashflow();
            ctCashflow.setStudentId(cashFolowEntity.getStudentId());
            ctCashflow.setContestId(cashFolowEntity.getContestId());
            ctCashflow.setCAction("租用厂房");
            ctCashflow.setCIn(0);
            ctCashflow.setCOut(workshopInfo.getCwRentFee());
            ctCashflow.setCSurplus(cash - workshopInfo.getCwRentFee());
            ctCashflow.setCComment("租用厂房支出" + workshopInfo.getCwRentFee() + "W");
            ctCashflow.setCDate(cashFolowEntity.getCurrentTime());
            ctCashflowService.save(ctCashflow);
            // 租售类型：1表示RENT
            ctWorkshop.setWStatus(1);
            // if为租，则当前时间，否则为null
            ctWorkshop.setWPayDate(cashFolowEntity.getCurrentTime());
        }
        // Workshop表新增记录
        ctWorkshop.setWCwid(cashFolowEntity.getCwid());
        ctWorkshop.setWSurplusCapacity(workshopInfo.getCwCapacity());
        ctWorkshop.setContestId(cashFolowEntity.getContestId()).setStudentId(cashFolowEntity.getStudentId());
        ctWorkshop.setGetDate(cashFolowEntity.getCurrentTime());
        ctWorkshopService.save(ctWorkshop);

        return R.success();
    }

    /**
     * @Description: 获取新建生产线相关信息
     * @Author:zhengxiaodong
     * @Param: [contestId]
     * @Return: com.xiyou.common.utils.R
     * @Date: 2019/7/27
     */
    public R newProductLineInfo(Integer contestId) {

        Map<String, Object> map = new HashMap<>();
        List<CtGzWorkshop> workshopList = ctGzWorkshopService.getWorkshopInfo(contestId);
        List<String> productCPNames = ctGzProductService.getProductCPName(contestId);

        map.put("workshopList", workshopList);
        map.put("cpName", productCPNames);

        return R.success().put("data", map);
    }

    /**
     * @Description: 确认新建生产线
     * @Author:zhengxiaodong
     * @Param: [cashFolowEntity]
     * @Return: com.xiyou.common.utils.R
     * @Date: 2019/7/27
     */
    public R confirmAddNewProductLine(CashFolowEntity cashFolowEntity) {

        Integer cash = ctCashflowService.getCash(cashFolowEntity.getStudentId(), cashFolowEntity.getContestId());
        // 查询生产线投资时间
        Integer cplInstallDate = ctGzProductLineService.getProductLineInfo(cashFolowEntity);

        if (cash < cashFolowEntity.getPayCashMoney()) {
            return R.error(CodeEnum.OTHER_ERROR.getCode(), "现金不足");
        }

        // Cashflow表新增ID
        CtCashflow ctCashflow = new CtCashflow();
        ctCashflow.setStudentId(cashFolowEntity.getStudentId());
        ctCashflow.setContestId(cashFolowEntity.getContestId());
        ctCashflow.setCAction("新建生产线");
        ctCashflow.setCIn(0);
        ctCashflow.setCOut(cashFolowEntity.getPayCashMoney());
        ctCashflow.setCSurplus(cash - cashFolowEntity.getPayCashMoney());
        ctCashflow.setCComment("新建: " + cashFolowEntity.getCPLName() + "产品: " + cashFolowEntity.getCPName());
        ctCashflow.setCDate(cashFolowEntity.getCurrentTime());
        ctCashflowService.save(ctCashflow);

        // Line表新增ID 
        CtLine ctLine = new CtLine();
        ctLine.setStudentId(cashFolowEntity.getStudentId());
        ctLine.setContestId(cashFolowEntity.getContestId());
        // 所属厂房id
        ctLine.setPlWid(cashFolowEntity.getCwid());
        // 生产线类型id
        ctLine.setPlCplid(cashFolowEntity.getCplId());
        // 生产产品id
        ctLine.setPlCpid(cashFolowEntity.getCPId());

        ctLine.setPlInvest(cashFolowEntity.getPayCashMoney());
        ctLine.setPlDepTotal(0);
        // if（该生产线的投资时间=1，则为当前时间，否则为空）
        if (cplInstallDate.equals(1)) {
            ctLine.setPlFinishDate(cashFolowEntity.getCurrentTime());
        }
        // 投资时间-1
        ctLine.setPlRemainDate(cplInstallDate - 1);
        ctLine.setPlAddTime(cashFolowEntity.getCurrentTime());

        ctLineService.save(ctLine);

        // Workshop表更新当前使用厂房剩余容量=剩余容量-1
        ctWorkshopService.tallyDown(cashFolowEntity.getCwid());

        return R.success();
    }


}
