package com.xiyou.main.biz.contest;

import com.xiyou.common.enums.CodeEnum;
import com.xiyou.common.utils.R;
import com.xiyou.main.entity.contest.ContestStudent;
import com.xiyou.main.entity.contest.CtBalance;
import com.xiyou.main.entity.contest.CtCharges;
import com.xiyou.main.entity.contest.CtProfitChart;
import com.xiyou.main.service.contest.ContestStudentService;
import com.xiyou.main.service.contest.CtBalanceService;
import com.xiyou.main.service.contest.CtChargesService;
import com.xiyou.main.service.contest.CtProfitChartService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @program: multi-module
 * @description:
 * @author: tangcan
 * @create: 2019-07-24 21:42
 **/
@Service
public class FillReportBiz {
    @Autowired
    private CtProfitChartService ctProfitChartService;
    @Autowired
    private CtChargesService ctChargesService;
    @Autowired
    private CtBalanceService ctBalanceService;
    @Autowired
    private ContestStudentService contestStudentService;

    @Transactional
    public R addCharges(Integer studentId, CtCharges charges) {
        if (charges.getStudentId() == null) {
            charges.setStudentId(studentId);
        }
        boolean correct = true;
        // 同时判断这三张表当年填入0和1的记录是否相同，如果不同则提示报表错误，否则提示报表正确。
        CtCharges sysCharges = ctChargesService.getSys(studentId, charges.getContestId(), charges.getCDate());
        if (sysCharges == null || !charges.getCOverhaul().equals(sysCharges.getCOverhaul()) || !charges.getCAd().equals(sysCharges.getCAd()) || !charges.getCMaintenance().equals(sysCharges.getCMaintenance()) ||
                !charges.getCDamage().equals(sysCharges.getCDamage()) || !charges.getCTransfer().equals(sysCharges.getCTransfer()) ||
                !charges.getCRent().equals(sysCharges.getCRent()) || !charges.getCDevelopMarket().equals(sysCharges.getCDevelopMarket()) ||
                !charges.getCDevelopIso().equals(sysCharges.getCDevelopIso()) || !charges.getCDevelopProduct().equals(sysCharges.getCDevelopProduct()) ||
                !charges.getCInformation().equals(sysCharges.getCInformation())) {
            correct = false;
        }
        charges.setBsIsxt(0);
        if (charges.getCDate() > 10) {
            charges.setCDate(charges.getCDate() / 10);
        }
        ctChargesService.save(charges);
        if (correct) {
            return R.success("报表正确");
        }
        int year = charges.getCDate() >= 10 ? (charges.getCDate() / 10) : charges.getCDate();
        ContestStudent contestStudent = contestStudentService.get(charges.getContestId(), studentId);
        contestStudentService.updateErrorReportYear(contestStudent, year);
        return R.success("报表错误");
    }

    @Transactional
    public R addProfitChart(Integer studentId, CtProfitChart profitChart) {
        if (profitChart.getStudentId() == null) {
            profitChart.setStudentId(studentId);
        }
        boolean correct = true;
        // 同时判断这三张表当年填入0和1的记录是否相同，如果不同则提示报表错误，否则提示报表正确。
        CtProfitChart sysProfitChart = ctProfitChartService.getSys(profitChart);
        if (sysProfitChart == null || !profitChart.getPcSales().equals(sysProfitChart.getPcSales()) || !profitChart.getPcDirectCost().equals(sysProfitChart.getPcDirectCost()) ||
                !profitChart.getPcGoodsProfit().equals(sysProfitChart.getPcGoodsProfit()) || !profitChart.getPcTotal().equals(sysProfitChart.getPcTotal()) ||
                !profitChart.getPcProfitBeforeDep().equals(sysProfitChart.getPcProfitBeforeDep()) || !profitChart.getPcDep().equals(sysProfitChart.getPcDep()) ||
                !profitChart.getPcProfitBeforeInterests().equals(sysProfitChart.getPcProfitBeforeInterests()) || !profitChart.getPcFinanceFee().equals(sysProfitChart.getPcFinanceFee()) ||
                !profitChart.getPcProfitBeforeTax().equals(sysProfitChart.getPcProfitBeforeTax()) || !profitChart.getPcTax().equals(sysProfitChart.getPcTax()) ||
                !profitChart.getPcAnnualNetProfit().equals(sysProfitChart.getPcAnnualNetProfit())) {
            correct = false;
        }
        profitChart.setBsIsxt(0);
        if (profitChart.getPcDate() > 10) {
            profitChart.setPcDate(profitChart.getPcDate() / 10);
        }
        ctProfitChartService.save(profitChart);
        if (correct) {
            return R.success("报表正确");
        }
        int year = profitChart.getPcDate() >= 10 ? (profitChart.getPcDate() / 10) : profitChart.getPcDate();
        ContestStudent contestStudent = contestStudentService.get(profitChart.getContestId(), studentId);
        contestStudentService.updateErrorReportYear(contestStudent, year);
        return R.success("报表错误");
    }

    @Transactional
    public R addBalance(Integer studentId, CtBalance balance) {
        if (balance.getStudentId() == null) {
            balance.setStudentId(studentId);
        }
        boolean correct = true;
        // 同时判断这三张表当年填入0和1的记录是否相同，如果不同则提示报表错误，否则提示报表正确。
        CtBalance sysBalance = ctBalanceService.get(balance);
        if (sysBalance == null || !balance.getBsCash().equals(sysBalance.getBsCash()) || !balance.getBsReceivable().equals(sysBalance.getBsReceivable()) ||
                !balance.getBsProductInProcess().equals(sysBalance.getBsProductInProcess()) || !balance.getBsProduct().equals(sysBalance.getBsProduct()) ||
                !balance.getBsMaterial().equals(sysBalance.getBsMaterial()) || !balance.getBsTotalCurrentAsset().equals(sysBalance.getBsTotalCurrentAsset()) ||
                !balance.getBsWorkshop().equals(sysBalance.getBsWorkshop()) || !balance.getBsEquipment().equals(sysBalance.getBsEquipment()) ||
                !balance.getBsProjectOnConstruction().equals(sysBalance.getBsProjectOnConstruction()) || !balance.getBsTotalFixedAsset().equals(sysBalance.getBsTotalFixedAsset()) ||
                !balance.getBsTotalAsset().equals(sysBalance.getBsTotalAsset()) || !balance.getBsLongLoan().equals(sysBalance.getBsLongLoan()) ||
                !balance.getBsShortLoan().equals(sysBalance.getBsShortLoan()) || !balance.getBsTax().equals(sysBalance.getBsTax()) ||
                !balance.getBsTotalLiability().equals(sysBalance.getBsTotalLiability()) || !balance.getBsEquity().equals(sysBalance.getBsEquity()) ||
                !balance.getBsRetainedEarning().equals(sysBalance.getBsRetainedEarning()) || !balance.getBsAnnualNetProfit().equals(sysBalance.getBsAnnualNetProfit()) ||
                !balance.getBsTotalEquity().equals(sysBalance.getBsTotalEquity()) || !balance.getBsTotal().equals(sysBalance.getBsTotal())) {
            correct = false;
        }
        balance.setBsIsxt(0);
        if (balance.getBsYear() > 10) {
            balance.setBsYear(balance.getBsYear() / 10);
        }
        ctBalanceService.save(balance);
        if (correct) {
            return R.success("报表正确");
        }
        int year = balance.getBsYear() >= 10 ? (balance.getBsYear() / 10) : balance.getBsYear();
        ContestStudent contestStudent = contestStudentService.get(balance.getContestId(), studentId);
        contestStudentService.updateErrorReportYear(contestStudent, year);
        return R.success("报表错误");
    }
}
