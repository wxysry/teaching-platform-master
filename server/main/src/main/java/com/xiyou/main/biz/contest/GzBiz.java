package com.xiyou.main.biz.contest;

import com.alibaba.excel.metadata.Sheet;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.xiyou.common.enums.CodeEnum;
import com.xiyou.common.utils.R;
import com.xiyou.main.dao.contest.*;
import com.xiyou.main.entity.contest.*;
import com.xiyou.main.pojo.contest.*;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.xiyou.common.office.utils.EasyExcelUtil.readExcel;

/**
 * @program: multi-module
 * @description:
 * @author: tangcan
 * @create: 2019-08-27 18:18
 **/
@Service
public class GzBiz {
    private Logger log = LoggerFactory.getLogger(this.getClass());
    @Autowired
    private GzExcelMapper gzExcelMapper;
    @Autowired
    private CtSubjectMapper ctSubjectMapper;
    @Autowired
    private CtGzCsMapper ctGzCsMapper;
    @Autowired
    private CtGzIsoMapper ctGzIsoMapper;
    @Autowired
    private CtGzMarketMapper ctGzMarketMapper;
    @Autowired
    private CtGzMaterialMapper ctGzMaterialMapper;
    @Autowired
    private CtGzProductMapper ctGzProductMapper;
    @Autowired
    private CtGzProducingMapper ctGzProducingMapper;
    @Autowired
    private CtGzProductLineMapper ctGzProductLineMapper;
    @Autowired
    private CtGzWorkshopMapper ctGzWorkshopMapper;
    @Transactional
    public R upload(MultipartFile multipartFile, Integer subjectNumber) {
        CtSubject ctSubject = ctSubjectMapper.getBySubjectNumber(subjectNumber);
        if (ctSubject == null) {
            return R.error(CodeEnum.OTHER_ERROR.getCode(), "请选择要上传的题库");
        }
        InputStream inputStream;
        /*
        由于不能直接重复使用inputstream
        因此先做如下处理
         */
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        try {
            inputStream = multipartFile.getInputStream();
            byte[] buffer = new byte[1024];
            int len;
            while ((len = inputStream.read(buffer)) > -1) {
                baos.write(buffer, 0, len);
            }
            baos.flush();
        } catch (IOException e) {
            log.error(e.getMessage());
            return R.error(CodeEnum.FILE_UPLOAD_FAIL.getCode(), e.getMessage());
        }

        Sheet sheet;
        /*
        规则gz_cs
         */
        sheet = new Sheet(1, 2, CtGzCsModel.class);
        List<CtGzCsModel> ctGzCsModelList;
        try {
            ctGzCsModelList = readExcel(new ByteArrayInputStream(baos.toByteArray()), sheet);
        } catch (Exception e) {
            return R.error(CodeEnum.FILE_UPLOAD_FAIL.getCode(), "规则gz_cs表" + e.getMessage());
        }
        if (ctGzCsModelList == null || ctGzCsModelList.size() == 0) {
            return R.error(CodeEnum.FILE_UPLOAD_FAIL.getCode(), "上传失败！规则gz_cs表无数据");
        }
        for (CtGzCsModel ctGzCsModel : ctGzCsModelList) {
            if (ctGzCsModel.getSubjectNumber() == null) {
                return R.error(CodeEnum.FILE_UPLOAD_FAIL.getCode(), "规则gz_cs表存在多余空行");
            }
            if (!subjectNumber.equals(ctGzCsModel.getSubjectNumber())) {
                return R.error(CodeEnum.FILE_UPLOAD_FAIL.getCode(), "规则gz_cs表题库号应该是：" + subjectNumber);
            }
        }

        /*
        初始广告gz_ad
         */
        sheet = new Sheet(2, 1, CtGzAdModel.class);
        List<CtGzAdModel> ctGzAdModelList;
        try {
            ctGzAdModelList = readExcel(new ByteArrayInputStream(baos.toByteArray()), sheet);
        } catch (Exception e) {
            return R.error(CodeEnum.FILE_UPLOAD_FAIL.getCode(), "初始广告gz_ad表" + e.getMessage());
        }
        if (ctGzAdModelList == null || ctGzAdModelList.size() == 0) {
            return R.error(CodeEnum.FILE_UPLOAD_FAIL.getCode(), "上传失败！初始广告gz_ad表无数据");
        }
        for (CtGzAdModel ctGzAdModel : ctGzAdModelList) {
            if (ctGzAdModel.getSubjectNum() == null) {
                return R.error(CodeEnum.FILE_UPLOAD_FAIL.getCode(), "初始广告gz_ad表存在多余空行");
            }
            if (!subjectNumber.equals(ctGzAdModel.getSubjectNum())) {
                return R.error(CodeEnum.FILE_UPLOAD_FAIL.getCode(), "初始广告gz_ad表题库号应该是：" + subjectNumber);
            }
        }

        /*
        ISO规则表gz_ISO
         */
        sheet = new Sheet(3, 2, CtGzIsoModel.class);
        List<CtGzIsoModel> ctGzIsoModelList;
        try {
            ctGzIsoModelList = readExcel(new ByteArrayInputStream(baos.toByteArray()), sheet);
        } catch (Exception e) {
            return R.error(CodeEnum.FILE_UPLOAD_FAIL.getCode(), "ISO规则表gz_ISO表" + e.getMessage());
        }
        if (ctGzIsoModelList == null || ctGzIsoModelList.size() == 0) {
            return R.error(CodeEnum.FILE_UPLOAD_FAIL.getCode(), "上传失败！ISO规则表gz_ISO表无数据");
        }
        for (CtGzIsoModel ctGzIsoModel : ctGzIsoModelList) {
            if (ctGzIsoModel.getSubjectNumber() == null) {
                return R.error(CodeEnum.FILE_UPLOAD_FAIL.getCode(), "ISO规则表gz_ISO表存在多余空行");
            }
            if (!subjectNumber.equals(ctGzIsoModel.getSubjectNumber())) {
                return R.error(CodeEnum.FILE_UPLOAD_FAIL.getCode(), "ISO规则表gz_ISO表题库号应该是：" + subjectNumber);
            }
        }

        /*
        市场订单gz_order
         */
        sheet = new Sheet(4, 1, CtGzOrderModel.class);
        List<CtGzOrderModel> ctGzOrderModelList;
        try {
            ctGzOrderModelList = readExcel(new ByteArrayInputStream(baos.toByteArray()), sheet);
        } catch (Exception e) {
            return R.error(CodeEnum.FILE_UPLOAD_FAIL.getCode(), "市场订单gz_order表" + e.getMessage());
        }
        if (ctGzOrderModelList == null || ctGzOrderModelList.size() == 0) {
            return R.error(CodeEnum.FILE_UPLOAD_FAIL.getCode(), "上传失败！市场订单gz_order表无数据");
        }
        for (CtGzOrderModel ctGzOrderModel : ctGzOrderModelList) {
            if (ctGzOrderModel.getSubjectNumber() == null) {
                return R.error(CodeEnum.FILE_UPLOAD_FAIL.getCode(), "市场订单gz_order表存在多余空行");
            }
            if (StringUtils.isBlank(ctGzOrderModel.getCiId())) {
                ctGzOrderModel.setCiId(null);
            }
            if (!subjectNumber.equals(ctGzOrderModel.getSubjectNumber())) {
                return R.error(CodeEnum.FILE_UPLOAD_FAIL.getCode(), "市场订单gz_order表题库号应该是：" + subjectNumber);
            }
        }

        /*
        市场规则表gz_market
         */
        sheet = new Sheet(5, 2, CtGzMarketModel.class);
        List<CtGzMarketModel> ctGzMarketModelList;
        try {
            ctGzMarketModelList = readExcel(new ByteArrayInputStream(baos.toByteArray()), sheet);
        } catch (Exception e) {
            return R.error(CodeEnum.FILE_UPLOAD_FAIL.getCode(), "市场规则表gz_market表" + e.getMessage());
        }
        if (ctGzMarketModelList == null || ctGzMarketModelList.size() == 0) {
            return R.error(CodeEnum.FILE_UPLOAD_FAIL.getCode(), "上传失败！市场规则表gz_market表无数据");
        }
        for (CtGzMarketModel ctGzMarketModel : ctGzMarketModelList) {
            if (ctGzMarketModel.getSubjectNumber() == null) {
                return R.error(CodeEnum.FILE_UPLOAD_FAIL.getCode(), "市场规则表gz_market表存在多余空行");
            }
            if (!subjectNumber.equals(ctGzMarketModel.getSubjectNumber())) {
                return R.error(CodeEnum.FILE_UPLOAD_FAIL.getCode(), "市场规则表gz_market表题库号应该是：" + subjectNumber);
            }
        }

        /*
        原料规则表gz_material
         */
        sheet = new Sheet(6, 2, CtGzMaterialModel.class);
        List<CtGzMaterialModel> ctGzMaterialModelList;
        try {
            ctGzMaterialModelList = readExcel(new ByteArrayInputStream(baos.toByteArray()), sheet);
        } catch (Exception e) {
            return R.error(CodeEnum.FILE_UPLOAD_FAIL.getCode(), "原料规则表gz_material表" + e.getMessage());
        }
        if (ctGzMaterialModelList == null || ctGzMaterialModelList.size() == 0) {
            return R.error(CodeEnum.FILE_UPLOAD_FAIL.getCode(), "上传失败！原料规则表gz_material表无数据");
        }
        for (CtGzMaterialModel ctGzMaterialModel : ctGzMaterialModelList) {
            if (ctGzMaterialModel.getSubjectNumber() == null) {
                return R.error(CodeEnum.FILE_UPLOAD_FAIL.getCode(), "原料规则表gz_material表存在多余空行");
            }
            if (!subjectNumber.equals(ctGzMaterialModel.getSubjectNumber())) {
                return R.error(CodeEnum.FILE_UPLOAD_FAIL.getCode(), "原料规则表gz_material表题库号应该是：" + subjectNumber);
            }
        }

        /*
        产品规则表gz_product
         */
        sheet = new Sheet(7, 2, CtGzProductModel.class);
        List<CtGzProductModel> ctGzProductModelList;
        try {
            ctGzProductModelList = readExcel(new ByteArrayInputStream(baos.toByteArray()), sheet);
        } catch (Exception e) {
            return R.error(CodeEnum.FILE_UPLOAD_FAIL.getCode(), "产品规则表gz_product表" + e.getMessage());
        }
        if (ctGzProductModelList == null || ctGzProductModelList.size() == 0) {
            return R.error(CodeEnum.FILE_UPLOAD_FAIL.getCode(), "上传失败！产品规则表gz_product表无数据");
        }
        for (CtGzProductModel ctGzProductModel : ctGzProductModelList) {
            if (ctGzProductModel.getSubjectNumber() == null) {
                return R.error(CodeEnum.FILE_UPLOAD_FAIL.getCode(), "产品规则表gz_product表存在多余空行");
            }
            if (!subjectNumber.equals(ctGzProductModel.getSubjectNumber())) {
                return R.error(CodeEnum.FILE_UPLOAD_FAIL.getCode(), "产品规则表gz_product表题库号应该是：" + subjectNumber);
            }
        }

        /*
        生产规则表gz_producing
         */
        sheet = new Sheet(8, 2, CtGzProducingModel.class);
        List<CtGzProducingModel> ctGzProducingModelList;
        try {
            ctGzProducingModelList = readExcel(new ByteArrayInputStream(baos.toByteArray()), sheet);
        } catch (Exception e) {
            return R.error(CodeEnum.FILE_UPLOAD_FAIL.getCode(), "生产规则表gz_producing表" + e.getMessage());
        }
        if (ctGzProducingModelList == null || ctGzProducingModelList.size() == 0) {
            return R.error(CodeEnum.FILE_UPLOAD_FAIL.getCode(), "上传失败！生产规则表gz_producing表无数据");
        }
        for (CtGzProducingModel ctGzProducingModel : ctGzProducingModelList) {
            if (ctGzProducingModel.getSubjectNumber() == null) {
                return R.error(CodeEnum.FILE_UPLOAD_FAIL.getCode(), "生产规则表gz_producing表存在多余空行");
            }
            if (!subjectNumber.equals(ctGzProducingModel.getSubjectNumber())) {
                return R.error(CodeEnum.FILE_UPLOAD_FAIL.getCode(), "生产规则表gz_producing表题库号应该是：" + subjectNumber);
            }
        }

        /*
        生产线规则表gz_product_line
         */
        sheet = new Sheet(9, 2, CtGzProductLineModel.class);
        List<CtGzProductLineModel> ctGzProductLineModelList;
        try {
            ctGzProductLineModelList = readExcel(new ByteArrayInputStream(baos.toByteArray()), sheet);
        } catch (Exception e) {
            return R.error(CodeEnum.FILE_UPLOAD_FAIL.getCode(), "生产线规则表gz_product_line表" + e.getMessage());
        }
        if (ctGzProductLineModelList == null || ctGzProductLineModelList.size() == 0) {
            return R.error(CodeEnum.FILE_UPLOAD_FAIL.getCode(), "上传失败！生产线规则表gz_product_line表无数据");
        }
        for (CtGzProductLineModel ctGzProductLineModel : ctGzProductLineModelList) {
            if (ctGzProductLineModel.getSubjectNum() == null) {
                return R.error(CodeEnum.FILE_UPLOAD_FAIL.getCode(), "生产线规则表gz_product_line表存在多余空行");
            }
            if (!subjectNumber.equals(ctGzProductLineModel.getSubjectNum())) {
                return R.error(CodeEnum.FILE_UPLOAD_FAIL.getCode(), "生产线规则表gz_product_line表题库号应该是：" + subjectNumber);
            }
        }

        /*
        厂房gz_workshop
         */
        sheet = new Sheet(10, 2, CtGzWorkshopModel.class);
        List<CtGzWorkshopModel> ctGzWorkshopModelList;
        try {
            ctGzWorkshopModelList = readExcel(new ByteArrayInputStream(baos.toByteArray()), sheet);
        } catch (Exception e) {
            return R.error(CodeEnum.FILE_UPLOAD_FAIL.getCode(), "厂房gz_workshop表" + e.getMessage());
        }
        if (ctGzWorkshopModelList == null || ctGzWorkshopModelList.size() == 0) {
            return R.error(CodeEnum.FILE_UPLOAD_FAIL.getCode(), "上传失败！厂房gz_workshop表无数据");
        }
        for (CtGzWorkshopModel ctGzWorkshopModel : ctGzWorkshopModelList) {
            if (ctGzWorkshopModel.getSubjectNum() == null) {
                return R.error(CodeEnum.FILE_UPLOAD_FAIL.getCode(), "厂房gz_workshop表存在多余空行");
            }
            if (!subjectNumber.equals(ctGzWorkshopModel.getSubjectNum())) {
                return R.error(CodeEnum.FILE_UPLOAD_FAIL.getCode(), "厂房gz_workshop表题库号应该是：" + subjectNumber);
            }
        }

        if (ctSubject.getUpload() == 1) {
            /*
            先删除之前上传过，但是现在不需要上传的,再存库
             */
            gzExcelMapper.removeRestCs(ctGzCsModelList);
            gzExcelMapper.removeRestAd(ctGzAdModelList);
            gzExcelMapper.removeRestIso(ctGzIsoModelList);
            gzExcelMapper.removeRestOrder(ctGzOrderModelList);
            gzExcelMapper.removeRestMarket(ctGzMarketModelList);
            gzExcelMapper.removeRestMaterial(ctGzMaterialModelList);
            gzExcelMapper.removeRestProduct(ctGzProductModelList);
            gzExcelMapper.removeRestProducing(ctGzProducingModelList);
            gzExcelMapper.removeRestProductLine(ctGzProductLineModelList);
            gzExcelMapper.removeRestWorkshop(ctGzWorkshopModelList);
        }
        /*
        存库
         */
        gzExcelMapper.insertCs(ctGzCsModelList);
        gzExcelMapper.insertAd(ctGzAdModelList);
        gzExcelMapper.insertIso(ctGzIsoModelList);
        gzExcelMapper.insertOrder(ctGzOrderModelList);
        gzExcelMapper.insertMarket(ctGzMarketModelList);
        gzExcelMapper.insertMaterial(ctGzMaterialModelList);
        gzExcelMapper.insertProduct(ctGzProductModelList);
        gzExcelMapper.insertProducing(ctGzProducingModelList);
        gzExcelMapper.insertProductLine(ctGzProductLineModelList);
        gzExcelMapper.insertWorkshop(ctGzWorkshopModelList);

        /*
        更新此题库已经上传规则表
         */
        if (ctSubject.getUpload() == 0) {
            ctSubjectMapper.updateUpload(subjectNumber);
        }

        return R.success();
    }

    public R getRuleBySubjectNumber(Integer subjectNumber) {
        Map<String, Object> gzList = new HashMap<>(12);
        List<CtGzCs> ctGzCsList = ctGzCsMapper.selectList(new QueryWrapper<CtGzCs>().eq("subject_number", subjectNumber));
        List<CtGzIso> ctGzIsoList = ctGzIsoMapper.selectList(new QueryWrapper<CtGzIso>().eq("subject_number", subjectNumber));
        List<CtGzMarket> ctGzMarketList = ctGzMarketMapper.selectList(new QueryWrapper<CtGzMarket>().eq("subject_number", subjectNumber));
        List<CtGzMaterial> ctGzMaterialList = ctGzMaterialMapper.selectList(new QueryWrapper<CtGzMaterial>().eq("subject_number", subjectNumber));
        List<CtGzProduct> ctGzProductList = ctGzProductMapper.selectList(new QueryWrapper<CtGzProduct>().eq("subject_number", subjectNumber));
        List<CtGzProducing> ctGzProducingList = ctGzProducingMapper.selectList(new QueryWrapper<CtGzProducing>().eq("subject_number", subjectNumber));
        List<CtGzProductLine> ctGzProductLineList = ctGzProductLineMapper.selectList(new QueryWrapper<CtGzProductLine>().eq("subject_num", subjectNumber));
        List<CtGzWorkshop> ctGzWorkshopList = ctGzWorkshopMapper.selectList(new QueryWrapper<CtGzWorkshop>().eq("subject_num", subjectNumber));

        gzList.put("ctGzCsList", ctGzCsList);
        gzList.put("ctGzIsoList", ctGzIsoList);
        gzList.put("ctGzMarketList", ctGzMarketList);
        gzList.put("ctGzMaterialList", ctGzMaterialList);
        gzList.put("ctGzProductList", ctGzProductList);
        gzList.put("ctGzProducingList", ctGzProducingList);
        gzList.put("ctGzProductLineList", ctGzProductLineList);
        gzList.put("ctGzWorkshopList", ctGzWorkshopList);

        return R.success(gzList);

    }
}
