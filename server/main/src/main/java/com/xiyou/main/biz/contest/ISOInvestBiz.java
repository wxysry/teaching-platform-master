package com.xiyou.main.biz.contest;

import com.xiyou.common.enums.CodeEnum;
import com.xiyou.common.utils.R;
import com.xiyou.main.dao.contest.CtCashflowMapper;
import com.xiyou.main.dao.contest.CtYfIsoMapper;
import com.xiyou.main.entity.contest.CtCashflow;
import com.xiyou.main.params.contest.ISOInvestParam;
import com.xiyou.main.service.contest.CtCashflowService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

/**
 * @program: multi-module
 * @description:
 * @author: tangcan
 * @create: 2019-09-01 12:43
 **/
@Service
public class ISOInvestBiz {
    @Autowired
    private CtYfIsoMapper ctYfIsoMapper;
    @Autowired
    private CtCashflowService ctCashflowService;
    @Autowired
    private CtCashflowMapper cashflowMapper;

    public R list(Integer contestId, Integer studentId) {
        List<Map<String, Object>> list = ctYfIsoMapper.getIsoInvestList(studentId, contestId);
        return R.success().put("isoList", list);
    }

    @Transactional
    public R commit(ISOInvestParam isoInvestParam) {
        if (isoInvestParam.getIsoIds() == null || isoInvestParam.getIsoIds().size() == 0) {
            return R.error(CodeEnum.PARAM_ERROR.getCode(), "请选择ISO投资");
        }
        // 现金
        Integer cash = cashflowMapper.getCash(isoInvestParam.getStudentId(), isoInvestParam.getContestId());
        if (cash == null) {
            cash = 0;
        }
        // 投资费用总和
        Integer investSum = ctYfIsoMapper.getIsoInvestSum(isoInvestParam.getIsoIds());
        // 当现金<sum（勾选的投资费用总和），则报错【现金不足】
        if (cash < investSum) {
            return R.error(CodeEnum.OTHER_ERROR.getCode(), "现金不足");
        }
        // 刷新cashflow，ID、ISO开拓、0、sum（勾选的投资费用总和），现金+增加-减少、扣减ISO开拓&sum（勾选的投资费用总和）、当前时间
        ctCashflowService.save(new CtCashflow()
                .setStudentId(isoInvestParam.getStudentId())
                .setContestId(isoInvestParam.getContestId())
                .setCAction("ISO开拓")
                .setCIn(0)
                .setCOut(investSum)
                .setCSurplus(cash - investSum)
                .setCComment("扣减ISO开拓" + investSum + "W")
                .setCDate(isoInvestParam.getDate()));
        // 刷新yf_ISO,勾选记录now_date +1，Remain_Date -1
        ctYfIsoMapper.updateISOInvest(isoInvestParam.getStudentId(), isoInvestParam.getContestId(), isoInvestParam.getIsoIds());
        return R.success();
    }
}
