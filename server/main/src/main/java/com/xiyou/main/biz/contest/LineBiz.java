package com.xiyou.main.biz.contest;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.xiyou.common.enums.CodeEnum;
import com.xiyou.common.utils.CommonUtil;
import com.xiyou.common.utils.R;
import com.xiyou.main.dao.contest.CtKcProductMapper;
import com.xiyou.main.dao.contest.CtLineMapper;
import com.xiyou.main.entity.contest.*;
import com.xiyou.main.enums.CashFlowActionEnum;
import com.xiyou.main.enums.ErrorEnum;
import com.xiyou.main.params.contest.OnlineConfirm;
import com.xiyou.main.service.contest.*;
import com.xiyou.main.vo.contest.OnlineLine;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.toList;


/**
 * @author dingyoumeng
 * @since 2019/07/22
 */
@Service
public class LineBiz {

    @Autowired
    private CtLineService lineService;
    @Autowired
    private CtLineMapper ctLineMapper;
    @Autowired
    private CtCashflowService ctCashflowService;
    @Autowired
    private CtGzProductLineService ctGzProductLineService;
    @Autowired
    private CtWorkshopService ctWorkshopService;
    @Autowired
    private CtGzProductService gzProductService;
    @Autowired
    private CtKcMaterialService ctKcMaterialService;
    @Autowired
    private CtGzProductService ctGzProductService;
    @Autowired
    private CtKcProductMapper ctKcProductMapper;

    @Transactional
    public R add(CtLine line) {
        // 查出该厂房
        CtWorkshop ctWorkshop = ctWorkshopService.getById(line.getWorkshopId());
        if (ctWorkshop == null) {
            return R.error(CodeEnum.OTHER_ERROR.getCode(), "请选择厂房");
        }
        if (ctWorkshop.getWSurplusCapacity() <= 0) {
            return R.error(CodeEnum.OTHER_ERROR.getCode(), "该厂房已满");
        }
        Integer cash = ctCashflowService.getCash(line.getStudentId(), line.getContestId());
        Integer cplid = line.getPlCplid();
        CtGzProductLine ctGzProductLine = ctGzProductLineService.getById(cplid);
        CtGzProduct ctGzProduct = ctGzProductService.getById(line.getPlCpid());
        int remain = cash - ctGzProductLine.getCplBuyFee();
        if (remain < 0) {
            return ErrorEnum.CASH_NOT_ENOUGH.getR();
        }
        line.setPlInvest(ctGzProductLine.getCplBuyFee());
        Integer finishDate = ctGzProductLine.getCplInstallDate();

        if (finishDate <= 0) {
            line.setPlFinishDate(line.getPlAddTime());
        } else {
            line.setPlFinishDate(null);
        }
        if (finishDate <= 0) {
            line.setPlRemainDate(null);
        } else {
            line.setPlRemainDate(finishDate - 1);
        }
        line.setPlDepTotal(0);
        line.setPlProductAddDate(null);
        line.setPlProductingDate(null);
        line.setPlTransferDate(null);
        line.setPlTransferAddDate(null);
        ctLineMapper.add(line);

        //插入消费记录
        CtCashflow ctCashflow = new CtCashflow();
        ctCashflow.setCDate(line.getPlAddTime())
                .setCSurplus(remain)
                .setCIn(0)
                .setCOut(ctGzProductLine.getCplBuyFee())
                .setCAction(CashFlowActionEnum.BUY_PRODUCT_LINE.getAction())
                .setContestId(line.getContestId())
                .setStudentId(line.getStudentId())
                .setCComment("新建生产线：" + ctGzProductLine.getCplName() + (ctGzProduct == null ? "" : ctGzProduct.getCpName()));
        ctCashflowService.save(ctCashflow);

        //工厂容量减一
        ctWorkshopService.tallyDown(line.getWorkshopId());
        return R.success();
    }

    public R listOnline(Integer userId, Integer contestId, Integer date) {
        List<OnlineLine> list = lineService.listOnline(userId, contestId, date);
        Map<String, Object> resultMap = new HashMap<>(16);
        resultMap.put("list", list);
        return R.success(resultMap);
    }

    @Transactional
    public R building(Integer userId, OnlineConfirm onlineConfirm) {
        List<Integer> lineIds = onlineConfirm.getLineIds();
        if (lineIds == null || lineIds.size() == 0) {
            return R.success();
        }
        Integer cash = ctCashflowService.getCash(userId, onlineConfirm.getContestId());
        List<CtGzProductLine> productLines = lineService.getLineInfo(lineIds);
        int sum = productLines.stream()
                .mapToInt(CtGzProductLine::getCplBuyFee)
                .sum();
        int remain = cash - sum;
        if (remain < 0) {
            return ErrorEnum.CASH_NOT_ENOUGH.getR();
        }
        CtCashflow ctCashflow = new CtCashflow();
        ctCashflow.setStudentId(userId)
                .setContestId(onlineConfirm.getContestId())
                .setCAction(CashFlowActionEnum.PRODUCT_LINE_BUILDING.getAction())
                .setCComment("在建生产线" + lineIds.toString())
                .setCIn(0)
                .setCOut(sum)
                .setCDate(onlineConfirm.getDate())
                .setCSurplus(remain);
        ctCashflowService.save(ctCashflow);
        List<CtLine> list = new ArrayList<>(lineService.listByIds(lineIds));
        list.sort(Comparator.comparingInt(CtLine::getPlCplid));
        productLines.sort(Comparator.comparingInt(CtGzProductLine::getId));
        List<CtLine> lines = new ArrayList<>();
        for (int i = 0; i < list.size(); i++) {
            CtLine line = new CtLine();
            CtLine l = list.get(i);
            CtGzProductLine pl = productLines.get(i);
            line.setLineId(l.getLineId())
                    .setPlInvest(l.getPlInvest() + pl.getCplBuyFee())
                    .setPlRemainDate(l.getPlRemainDate() - 1);
            lines.add(line);
        }
        lineService.updateBatchById(lines);
        return R.success();
    }

    public R listTransfer(Integer userId, Integer contestId) {
        List<OnlineLine> list = lineService.listTransfer(userId, contestId);
        // 计算净值
        list.forEach(p -> p.setPlInvest(p.getPlInvest() - p.getPlDepTotal()));
        Map<String, Object> resultMap = new HashMap<>(CommonUtil.getListInitCap(list.size()));
        resultMap.put("list", list);
        return R.success(resultMap);
    }

    public R lineTransfer(Integer userId, OnlineConfirm confirm) {
        int newCpid = confirm.getCpId();
        Integer cash = ctCashflowService.getCash(userId, confirm.getContestId());
        List<Integer> lineIds = confirm.getLineIds();

        List<CtLine> list = new ArrayList<>(lineService.listByIds(lineIds));
        list.removeIf(l -> l.getPlCpid() == newCpid);
        if (list.size() == 0) {
            return R.success();
        }

        List<CtGzProductLine> productLines = lineService.getLineInfo(lineIds);

        int sum = productLines.stream()
                .mapToInt(CtGzProductLine::getCplTransferFee)
                .sum();
        int remain = cash - sum;
        if (remain < 0) {
            return ErrorEnum.CASH_NOT_ENOUGH.getR();
        }
        //插入现金交易记录
        CtCashflow ctCashflow = new CtCashflow();
        CtGzProduct ctGzProduct = ctGzProductService.getById(newCpid);
        ctCashflow.setStudentId(userId)
                .setContestId(confirm.getContestId())
                .setCAction(CashFlowActionEnum.PRODUCT_LINE_TRANSFER.getAction())
                .setCComment("生产线" + lineIds.toString() + "转产为" + (ctGzProduct == null ? "" : ctGzProduct.getCpName()))
                .setCIn(0)
                .setCOut(sum)
                .setCDate(confirm.getDate())
                .setCSurplus(remain);
        ctCashflowService.save(ctCashflow);


        List<CtLine> lines = new ArrayList<>();
        list.sort(Comparator.comparingInt(CtLine::getPlCplid));
        productLines.sort(Comparator.comparingInt(CtGzProductLine::getId));
        for (int i = 0; i < list.size(); i++) {
            CtLine line = new CtLine();
            CtLine l = list.get(i);
            CtGzProductLine pl = productLines.get(i);
            int transferDate = pl.getCplTransferDate();
            line.setLineId(l.getLineId())
                    .setPlTransferAddDate(confirm.getDate())
                    .setPlCpid(newCpid)
                    .setPlTransferDate(transferDate == 0 ? null : (transferDate - 1));
            lines.add(line);
        }
        ctLineMapper.updateBatch(lines);
        return R.success();
    }

    public R lineSell(Integer userId, OnlineConfirm confirm) {
        if (confirm.getLineIds() == null || confirm.getLineIds().isEmpty()) {
            return R.error(CodeEnum.PARAM_MISS.getCode(), "请选择生产线");
        }
        Integer cash = ctCashflowService.getCash(userId, confirm.getContestId());
        List<Integer> lineIds = confirm.getLineIds();
        List<CtLine> lineList = new ArrayList<>(lineService.listByIds(lineIds));
        List<CtGzProductLine> productLines = lineService.getLineInfo(lineIds);
        // 残值总值
        int depFeeSum = productLines.stream().mapToInt(CtGzProductLine::getCplDepFee).sum();
        // 原值总值
        int investSum = lineList.stream()
                .mapToInt(CtLine::getPlInvest)
                .sum();
        // 累计折旧
        int depTotalSum = lineList.stream()
                .mapToInt(CtLine::getPlDepTotal)
                .sum();
        if (cash + depFeeSum < 0) {
            return ErrorEnum.CASH_NOT_ENOUGH.getR();
        }
        // 插入现金交易记录
        // 流入=原值-累计折旧
        // 流出=原值-累计折旧-残值
        Integer cIn = investSum - depTotalSum;
        Integer cOut = investSum - depTotalSum - depFeeSum;
        CtCashflow ctCashflow = new CtCashflow();
        ctCashflow.setStudentId(userId)
                .setContestId(confirm.getContestId())
                .setCAction("变卖生产线")
                .setCOut(cOut)
                .setCIn(cIn)
                .setCComment("变卖生产线" + lineIds.toString() + ",增加现金" + investSum + "W,减少现金" + depTotalSum + "W")
                .setCDate(confirm.getDate())
                .setCSurplus(cash + cIn - cOut);
        ctCashflowService.save(ctCashflow);
        for (CtLine l : lineList) {
            ctWorkshopService.tallyUp(l.getWorkshopId());
        }
        lineService.removeByIds(lineIds);
        return R.success();
    }

    //查出处于空闲的生产线
    public R listNoProducting(Integer userId, Integer contestId) {
        List<OnlineLine> list = lineService.listNoProducting(userId, contestId);
        Map<String, Object> resultMap = new HashMap<>(16);
        resultMap.put("list", list);
        return R.success(resultMap);
    }

    @Transactional
    public R producting(Integer userId, OnlineConfirm confirm) {
        Integer cash = ctCashflowService.getCash(userId, confirm.getContestId());
        List<Integer> lineIds = confirm.getLineIds();
        List<CtLine> lineList = new ArrayList<>(lineService.listByIds(lineIds));
        List<Integer> gzPids = lineList.stream().map(CtLine::getPlCpid).collect(toList());
        List<CtGzProduct> products = new ArrayList<>(gzProductService.listByIds(gzPids));
        // lineId 映射 cpid
        Map<Integer, Integer> cpIdMap = lineList.stream().collect(Collectors.toMap(CtLine::getLineId, CtLine::getPlCpid, (k1, k2) -> k1));
        // cpid 映射 DevelopFee
        Map<Integer, Integer> developFeeMap = products.stream().collect(Collectors.toMap(CtGzProduct::getId, CtGzProduct::getCpDevelopFee, (k1, k2) -> k1));
        // 防止重复的只算一次
        int depSum = 0;
        for (Integer lineId : lineIds) {
            depSum += developFeeMap.get(cpIdMap.get(lineId));
        }

        int remain = cash - depSum;
        if (remain < 0) {
            return ErrorEnum.CASH_NOT_ENOUGH.getR();
        }
        //判断原材料是否够
        QueryWrapper<CtKcMaterial> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("student_id", userId);
        queryWrapper.eq("contest_id", confirm.getContestId());
        List<CtKcMaterial> materials = ctKcMaterialService.list(queryWrapper);
        List<CtKcMaterial> materialsNeed = ctKcMaterialService.listNeed(confirm.getContestId(), lineIds);
        HashMap<Integer, Integer> materialNeedMap = new HashMap<>();
        for (CtKcMaterial m : materialsNeed) {
            materialNeedMap.put(m.getImCmId(), m.getImNum());
        }
        for (CtKcMaterial m : materials) {
            int num = m.getImNum();
            Integer imNum = materialNeedMap.get(m.getImCmId());
            int need = (imNum == null ? 0 : imNum);
            if (need > num) {
                return ErrorEnum.MATERIAL_NOT_ENOUGH.getR();
            }
            m.setImNum(num - need);
        }

        //插入现金交易记录
        CtCashflow ctCashflow = new CtCashflow();
        ctCashflow.setStudentId(userId)
                .setContestId(confirm.getContestId())
                .setCAction(CashFlowActionEnum.PRODUCT_PRODUCING.getAction())
                .setCOut(depSum)
                .setCIn(0)
                .setCComment("生产" + lineIds.toString())
                .setCDate(confirm.getDate())
                .setCSurplus(remain);
        ctCashflowService.save(ctCashflow);
        //更新原材料库存
        ctKcMaterialService.updateBatchById(materials);
        List<Integer> cplids = lineList.stream().map(CtLine::getPlCplid).collect(toList());
        List<CtGzProductLine> ctGzProductLineList = ctGzProductLineService.getListByCplids(cplids);
        Map<Integer, Integer> produceDateMap = ctGzProductLineList.stream().collect(Collectors.toMap(CtGzProductLine::getId, CtGzProductLine::getCplProduceDate, (k1, k2) -> k1));
        for (CtLine ctLine : lineList) {
            Integer produceDate = produceDateMap.get(ctLine.getPlCplid());
            ctLine.setPlProductAddDate(confirm.getDate());
            if (produceDate != null && produceDate > 0) {
                ctLine.setPlProductingDate(produceDate - 1);
            } else {
                // 生产周期为0 ，则剩余生产时间置空
                ctLine.setPlProductingDate(null);
                // 产品入库
                ctKcProductMapper.updateNum(confirm.getContestId(), userId, ctLine.getPlCpid(), 1);
            }
        }
        //更新生产线表
        lineService.updateBatchById(lineList);
        return R.success();
    }

    public R info(Integer studentId, Integer contestId) {
        // 所属厂房
        List<Map<String, Object>> workshopList = ctWorkshopService.baseInfoList(studentId, contestId);
        // 生产线类型
        List<CtGzProductLine> productLineList = ctGzProductLineService.getProductTypeList(contestId);
        // 生产产品
        List<CtGzProduct> productList = ctGzProductService.getList(contestId);
        Map<String, Object> returnMap = new HashMap<>();
        returnMap.put("workshopList", workshopList);
        returnMap.put("productLineList", productLineList);
        returnMap.put("productList", productList);
        return R.success(returnMap);
    }

    public R proInfo(Integer contestId) {
        // 生产产品
        List<CtGzProduct> productList = ctGzProductService.getList(contestId);
        Map<String, Object> returnMap = new HashMap<>();
        returnMap.put("productList", productList);
        return R.success(returnMap);
    }
}
