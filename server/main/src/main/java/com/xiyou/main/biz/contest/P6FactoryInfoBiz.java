package com.xiyou.main.biz.contest;

import com.xiyou.common.utils.R;
import com.xiyou.main.dao.contest.CtWorkshopMapper;
import com.xiyou.main.entity.contest.CtLine;
import com.xiyou.main.vo.contest.CashFolowEntity;
import com.xiyou.main.vo.contest.CtWorkshopEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author: zhengxiaodong
 * @description: 厂房信息
 * @since: 2019-07-23 15:34:47
 **/
@Service
public class P6FactoryInfoBiz {

    @Autowired
    CtWorkshopMapper ctWorkshopMapper;

    /**
     * @Description: workshop信息
     * @Author:zhengxiaodong
     * @Param: [cashFolowEntity]
     * @return: com.xiyou.common.utils.R
     */
    public R getWorkshopInfo(CashFolowEntity cashFolowEntity) {

        List<CtWorkshopEntity> workShopList = ctWorkshopMapper.getWorkShopInfo(cashFolowEntity);

        // 当PL_Producing_Date(剩余生产时间) 、PL_Transfer_Date(转产剩余时间) 、PL_Remain_Date(在建剩余时间) 为空，则显示空闲
        // 当PL_Transfer_Date(转产剩余时间)  不为空则显示转产
        // 当PL_Producing_Date(剩余生产时间) 不为空显示在产
        // 当PL_Remain_Date(在建剩余时间) 不为空则显示在建
        for (CtWorkshopEntity workshop : workShopList) {
            for (CtLine ctLine : workshop.getLineList()) {
                if (ctLine.getPlProductingDate() == null && ctLine.getPlTransferDate() == null && ctLine.getPlRemainDate() == null) {
                    ctLine.setStatus("空闲");
                } else if (ctLine.getPlTransferDate() != null) {
                    ctLine.setStatus("转产");
                } else if (ctLine.getPlProductingDate() != null) {
                    ctLine.setStatus("在产");
                } else if (ctLine.getPlRemainDate() != null) {
                    ctLine.setStatus("在建");
                }
            }
        }
        return R.success().put("list", workShopList);
    }


}
