package com.xiyou.main.biz.contest;

import com.xiyou.common.enums.CodeEnum;
import com.xiyou.common.utils.R;
import com.xiyou.main.dao.contest.CtDdMaterialMapper;
import com.xiyou.main.dao.contest.CtGzMaterialMapper;
import com.xiyou.main.entity.contest.CtCashflow;
import com.xiyou.main.entity.contest.CtDdMaterial;
import com.xiyou.main.params.contest.MaterialParam;
import com.xiyou.main.service.contest.CtCashflowService;
import com.xiyou.main.service.contest.CtDdMaterialService;
import com.xiyou.main.service.contest.CtGzMaterialService;
import com.xiyou.main.service.contest.CtKcMaterialService;
import com.xiyou.main.vo.contest.CashFolowEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @author: zhengxiaodong
 * @description: P11-P12原材料更新与订购
 * @since: 2019-07-25 14:31:49
 **/
@Service
public class RawMaterialUpdateBiz {

    @Autowired
    CtDdMaterialService ctDdMaterialService;
    @Autowired
    CtCashflowService ctCashflowService;
    @Autowired
    CtKcMaterialService ctKcMaterialService;
    @Autowired
    CtGzMaterialService ctGzMaterialService;
    @Autowired
    CtGzMaterialMapper ctGzMaterialMapper;
    @Autowired
    private CtDdMaterialMapper ctDdMaterialMapper;

    /**
     * @Description: P11->更新原料-需要金额
     * @Author:zhengxiaodong
     * @Param: [cashFolowEntity]
     * @Return: com.xiyou.common.utils.R
     * @Date: 2019/7/25
     */
    public R getNeedPayMoney(CashFolowEntity cashFolowEntity) {

        Integer money = ctDdMaterialService.getStudentPayMoney(cashFolowEntity);
        return R.success().put("data", money);
    }

    /**
     * @Description: 确认更新原材料
     * @Author:zhengxiaodong
     * @Param: [cashFolowEntity]
     * @Return: com.xiyou.common.utils.R
     * @Date: 2019/7/25
     */
    @Transactional
    public R confirmUpdateMaterial(CashFolowEntity cashFolowEntity) {

        Integer cash = ctCashflowService.getCash(cashFolowEntity.getStudentId(), cashFolowEntity.getContestId());
        Integer money = ctDdMaterialService.getStudentPayMoney(cashFolowEntity);
        // 如现金流量表.现金<sum(原料表.原料采购剩余时间为0的数量*原料规则表.费用）则报错，现金不足
        if (cash == null || cash < money) {
            return R.error(CodeEnum.OTHER_ERROR.getCode(), "现金不足");
        }
        cashFolowEntity.setPayCashMoney(money);

        // dd_material中剩余时间为0的原料编号对应的原料名称、数量
        List<CtDdMaterial> ddMaterialList = ctDdMaterialMapper.getRemainDateISZeroNumb(cashFolowEntity.getContestId(), cashFolowEntity.getStudentId());
        StringBuilder builder = new StringBuilder();
        for (CtDdMaterial ctDdMaterial : ddMaterialList) {
            if (builder.length() > 0) {
                builder.append(",");
            }
            builder.append(ctDdMaterial.getCmName()).append(' ').append(ctDdMaterial.getOmNum()).append("个");
        }

        CtCashflow ctCashflow = new CtCashflow();
        ctCashflow.setStudentId(cashFolowEntity.getStudentId()).setContestId(cashFolowEntity.getContestId());
        ctCashflow.setCAction("原材料入库");
        ctCashflow.setCIn(0);
        ctCashflow.setCOut(money);
        // 现金+资金增加-资金减少
        ctCashflow.setCSurplus(cash - money);
        ctCashflow.setCComment("原材料入库,付现金" + money + "W,入库" + builder.toString());
        ctCashflow.setCDate(cashFolowEntity.getCurrentTime());
        // cashflow新增记录原材料入库
        ctCashflowService.save(ctCashflow);

        // 原材料库存kc_material更新记录，
        ctKcMaterialService.updateKcMaterial(cashFolowEntity);

        // 更新dd_material记录
        ctDdMaterialService.updateStudentDdMaterialListInfo(cashFolowEntity);

        return R.success();
    }

    /**
     * @Description: 订购原料-原料列表
     * @Author:zhengxiaodong
     * @Param: [cashFolowEntity]
     * @Return: com.xiyou.common.utils.R
     * @Date: 2019/7/26
     */
    public R getBuyMaterialList(CashFolowEntity cashFolowEntity) {
        return R.success().put("list", ctDdMaterialMapper.getListBySubjectNumber(cashFolowEntity.getContestId()));
    }

    /**
     * @param materialParam
     * @Description: 订购原料->确认
     * 点击确认后，刷新原料采购表dd_material，把相应数量更新到相应剩余时间（运货期为2，则更新到1，运货期为1，则更新到0）数量
     * @Author:zhengxiaodong
     * @Param: [cashFolowEntity]
     * @Return: com.xiyou.common.utils.R
     * @Date: 2019/7/26
     */
    @Transactional
    public R confirmBuyMaterialList(MaterialParam materialParam) {
        // 判断是否全为0
        boolean ok = false;
        StringBuilder builder = new StringBuilder();
        for (CtDdMaterial material : materialParam.getMaterialList()) {
            if (material.getOmNum() == null) {
                material.setOmNum(0);
            }
            if (material.getOmNum() > 0) {
                ok = true;
            }
            if (builder.length() > 0) {
                builder.append(",");
            }
            builder.append(material.getCmName()).append(' ').append(material.getOmNum()).append("个");
            material.setStudentId(materialParam.getStudentId()).setContestId(materialParam.getContestId());
            // 刷新原料采购表dd_material，把相应数量更新到相应剩余时间（运货期为2，则更新到1，运货期为1，则更新到0）数量
            material.setRemainDate(material.getRemainDate() - 1);
            material.setPurchaseDate(materialParam.getCurrentTime());
        }
        if (!ok) {
            // 都是0，直接返回
            return R.success();
        }
        Integer surplus = ctCashflowService.getCash(materialParam.getStudentId(), materialParam.getContestId());
        CtCashflow ctCashflow = new CtCashflow();
        ctCashflow.setStudentId(materialParam.getStudentId()).setContestId(materialParam.getContestId());
        ctCashflow.setCAction("订购原材料");
        ctCashflow.setCIn(0);
        ctCashflow.setCOut(0);
        // 现金+资金增加-资金减少
        ctCashflow.setCSurplus(surplus);
        ctCashflow.setCComment("订购原材料" + builder.toString());
        ctCashflow.setCDate(materialParam.getCurrentTime());
        // cashflow新增记录原材料入库
        ctCashflowService.save(ctCashflow);

        ctDdMaterialService.updateMaterial(materialParam.getMaterialList());
        return R.success();
    }
}
