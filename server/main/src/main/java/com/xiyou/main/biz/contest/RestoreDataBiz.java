package com.xiyou.main.biz.contest;

import com.xiyou.common.utils.R;
import com.xiyou.main.dao.contest.CtSubjectMapper;
import com.xiyou.main.entity.contest.ContestStudent;
import com.xiyou.main.service.contest.ContestStudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @program: multi-module
 * @description:
 * @author: tangcan
 * @create: 2019-08-31 21:07
 **/
@Service
public class RestoreDataBiz {
    @Autowired
    private CtSubjectMapper ctSubjectMapper;
    @Autowired
    private ContestStudentService contestStudentService;

    /**
     * @Author: tangcan
     * @Description: 数据还原
     * @Param: [contestId, studentId]
     * @date: 2019/8/31
     */
    @Transactional
    public R restore(Integer contestId, Integer studentId) {
        // 先删除现有数据
        ctSubjectMapper.removeMainTableData(contestId, studentId);
        //清除季度数据
        ctSubjectMapper.removeSeasonLikeTableData(contestId,studentId);
        // 再从备份表里把数据插入到主表中
        ctSubjectMapper.insertLikeTableDataToMainTable(contestId, studentId);
        //把年度备份数据放入季度备份表
        ctSubjectMapper.insertLikeTableDataToReasonTable(contestId,studentId);
        // 如果本年已更新报表错误年份统计字段，则删除本年份
        ContestStudent contestStudent = contestStudentService.get(contestId, studentId);
        if (contestStudent != null) {
            String errorReportYear = contestStudent.getErrorReportYear();
            if (errorReportYear.length() > 0) {
                // 经营时间进度,年份+季度+进度
                Integer date = contestStudent.getDate();
                if (date != null) {
                    int year = date / 100;
                    if ((date % 10) >= 5) {
                        // 本年未真正开始，可还原到上一年
                        year--;
                    }
                    String yearStr = String.valueOf(year);
                    if (errorReportYear.contains(yearStr)) {
                        if (errorReportYear.charAt(0) == yearStr.charAt(0)) {
                            // 第一个前面无逗号
                            contestStudent.setErrorReportYear(contestStudent.getErrorReportYear().replaceAll(yearStr, ""));
                        } else {
                            // 其余前面的逗号一起删除
                            contestStudent.setErrorReportYear(contestStudent.getErrorReportYear().replaceAll("," + yearStr, ""));
                        }
                        contestStudentService.updateErrorReportYear(contestStudent);
                    }
                }
            }
        }
        return R.success();
    }

    @Transactional
    public R restoreSeason(Integer contestId, Integer studentId) {
        List<Integer> dataSum = ctSubjectMapper.seasonDataSum(contestId, studentId);
        long sum = dataSum.stream().mapToInt(s -> s).summaryStatistics().getSum();
        if (sum == 0L) {
            return R.success("当前无数据可还原");
        } else {
            //移除主表数据
            ctSubjectMapper.removeMainTableData(contestId, studentId);
            // 再从备份表里把数据插入到主表中
            ctSubjectMapper.insertSeasonLikeTableToMainTable(contestId, studentId);
            return R.success();
        }
    }
    /**
     * @Author: tangcan
     * @Description: 备份数据
     * @Param: [contestId, studentId]
     * @date: 2019/8/31
     */
    @Async
    public void backupData(Integer contestId, Integer studentId) {
        // 如果备份表里有数据，先删除备份表里的数据
        ctSubjectMapper.removeLikeTableData(contestId, studentId);
        // 将主表中的数据插入到备份表中
        ctSubjectMapper.insertMainTableToLikeTableData(contestId, studentId);
    }

    @Async
    public void backupSeasonData(Integer contestId, Integer studentId) {
        // 如果备份表里有数据，先删除备份表里的数据
        ctSubjectMapper.removeSeasonLikeTableData(contestId, studentId);
        // 将主表中的数据插入到备份表中
        ctSubjectMapper.insertMainTableToSeasonLikeTableData(contestId, studentId);
    }

    /**
     * @Author: tangcan
     * @Description: 异步清除备份数据
     * @Param: [contestId, studentId]
     * @date: 2019/9/1
     */
    @Async
    public void removeLikeTableData(Integer contestId, Integer studentId) {
        ctSubjectMapper.removeLikeTableData(contestId, studentId);
    }

    /**
     * 清楚季度缓存数据
     * @param contestId
     * @param studentId
     */
    @Async
    public void removeSeasonLikeTableData(Integer contestId, Integer studentId) {
        ctSubjectMapper.removeSeasonLikeTableData(contestId, studentId);
    }

    /*
    删除模拟选单和已选订单
     */
    public void removeOrder(Integer studentId, Integer contestId) {
        ctSubjectMapper.removeOrder(contestId, studentId);
    }

    /*
    将模拟选单和已选订单的备份数据加入模拟选单和已选订单
     */
    public void backupOrder(Integer studentId, Integer contestId) {
        ctSubjectMapper.backupOrder(contestId, studentId);
    }
}
