package com.xiyou.main.biz.contest;

import com.xiyou.common.utils.R;
import com.xiyou.main.dao.contest.*;
import com.xiyou.main.vo.contest.CashFolowEntity;
import com.xiyou.main.vo.contest.FinanceReturnEntity;
import com.xiyou.main.vo.contest.MarketReturnEntity;
import com.xiyou.main.vo.contest.ProductMaterialEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author: zhengxiaodong
 * @description: 财务信息&综合财务信息&研发认证信息
 * @since: 2019-07-22 16:01:29
 **/
@Service
public class StudentFinanceBiz {

    @Autowired
    CtCashflowMapper ctCashflowMapper;
    @Autowired
    CtFeeMapper ctFeeMapper;
    @Autowired
    CtBankLoanMapper ctBankLoanMapper;
    @Autowired
    CtXzOrderMapper ctXzOrderMapper;
    @Autowired
    CtYfMarketMapper ctYfMarketMapper;
    @Autowired
    CtYfIsoMapper ctYfIsoMapper;
    @Autowired
    CtYfProductMapper ctYfProductMapper;
    @Autowired
    CtKcProductMapper ctKcProductMapper;
    @Autowired
    CtKcMaterialMapper ctKcMaterialMapper;
    @Autowired
    CtDdMaterialMapper ctDdMaterialMapper;

    /**
     * @Description: 财务信息&综合财务信息
     * @Author:zhengxiaodong
     * @Param: [cashFolowEntity]
     * @return: com.xiyou.common.utils.R
     */
    public R getStudentFinanceInfo(CashFolowEntity cashFolowEntity) {

        Map<String, Object> returnMap = new HashMap<>();

        FinanceReturnEntity returnEntity = new FinanceReturnEntity();

        // 当前现金
        returnEntity.setCurrentMoney(ctCashflowMapper.getCash(cashFolowEntity.getStudentId(), cashFolowEntity.getContestId()));
        // 应收账款
        returnEntity.setReceivableMoney(ctFeeMapper.getStudentReceivableMoney(cashFolowEntity));
        returnMap.put("receivableMoneyList", ctFeeMapper.getStudentReceivableMoneyList(cashFolowEntity));
        // 长贷总额
        returnEntity.setLongTermLoansMoney(ctBankLoanMapper.getStudentLongAndShortTermLoansMoney(cashFolowEntity, 1));
        returnMap.put("longTermLoansMoneyList", ctBankLoanMapper.getStudentLongAndShortTermLoansMoneyList(cashFolowEntity, 1));
        // 短贷总额
        returnEntity.setShortTermLoanMoney(ctBankLoanMapper.getStudentLongAndShortTermLoansMoney(cashFolowEntity, 2));
        returnMap.put("shortTermLoanMoneyList", ctBankLoanMapper.getStudentLongAndShortTermLoansMoneyList(cashFolowEntity, 2));
        // 贴息
        returnEntity.setDiscountMoney(ctCashflowMapper.getStudentDiscountMoney(cashFolowEntity, "贴现"));
        // 利息
        returnEntity.setInterest(ctCashflowMapper.getStudentDiscountMoney(cashFolowEntity, "贷款利息"));
        // 销售收入
        returnEntity.setSalesProfit(ctXzOrderMapper.getStudentSalesProfit(cashFolowEntity));

        // 维修费[维修费、转产费、租金、管理费、广告费]
        returnEntity.setRepairCost(ctCashflowMapper.getRepairCostToAdCost(cashFolowEntity, "维修费"));
        // 转产费
        returnEntity.setTransferCost(ctCashflowMapper.getRepairCostToAdCost(cashFolowEntity, "转产费"));
        // 租金
        returnEntity.setRentCost(ctCashflowMapper.getRepairCostToAdCost(cashFolowEntity, "租用厂房"));
        // 管理费
        returnEntity.setManageFee(ctCashflowMapper.getRepairCostToAdCost(cashFolowEntity, "管理费"));
        // 广告费
        returnEntity.setAdCost(ctCashflowMapper.getRepairCostToAdCost(cashFolowEntity, "广告费"));
        // 损失
        returnEntity.setLossCost(ctCashflowMapper.getStudentLossCost(cashFolowEntity));
        // 直接成本
        returnEntity.setDirectCost(ctXzOrderMapper.getStudentProductDevCost(cashFolowEntity));

        // ISO认证[ISO认证、直接成本、市场开拓]
        returnEntity.setISOCost(ctCashflowMapper.getStudentISOTomarketDevCost(cashFolowEntity, "ISO开拓"));
        // 产品研发
        returnEntity.setProductDevCost(ctCashflowMapper.getStudentISOTomarketDevCost(cashFolowEntity, "产品研发"));
        // 市场开拓
        returnEntity.setMarketDevCost(ctCashflowMapper.getStudentISOTomarketDevCost(cashFolowEntity, "市场开拓"));

        returnMap.put("data", returnEntity);
        return R.success(returnMap);
    }

    /**
     * @Description: 研发认证信息
     * @Author:zhengxiaodong
     * @Param: []
     * @return: com.xiyou.common.utils.R
     */
    public R getResearchAndDevelopeInfo(Integer studentId, Integer contestId) {

        MarketReturnEntity returnEntity = new MarketReturnEntity();

        // 市场准入
        returnEntity.setMarket(ctYfMarketMapper.getMarketList(studentId, contestId));
        // 生产资格
        returnEntity.setProduct(ctYfProductMapper.getProductList(studentId, contestId));
        // ISO认证
        returnEntity.setIso(ctYfIsoMapper.getIsoList(studentId, contestId));

        // 本地 To 国际
        MarketReturnEntity proportion1 = ctYfMarketMapper.getYFMarketProportion(1, studentId, contestId);
        returnEntity.setLocal((proportion1 == null ? 0 : proportion1.getNowDate()) + "/" + (proportion1 == null ? 0 : proportion1.getTotalDate()));
        returnEntity.setLocalEndDate(proportion1 == null ? null : proportion1.getFinishDate());

        MarketReturnEntity proportion2 = ctYfMarketMapper.getYFMarketProportion(2, studentId, contestId);
        returnEntity.setArea((proportion2 == null ? 0 : proportion2.getNowDate()) + "/" + (proportion2 == null ? 0 : proportion2.getTotalDate()));
        returnEntity.setAreaEndDate(proportion2 == null ? null : proportion2.getFinishDate());

        MarketReturnEntity proportion3 = ctYfMarketMapper.getYFMarketProportion(3, studentId, contestId);
        returnEntity.setHome((proportion3 == null ? 0 : proportion3.getNowDate()) + "/" + (proportion3 == null ? 0 : proportion3.getTotalDate()));
        returnEntity.setHomeEndDate(proportion3 == null ? null : proportion3.getFinishDate());

        MarketReturnEntity proportion4 = ctYfMarketMapper.getYFMarketProportion(4, studentId, contestId);
        returnEntity.setAsia((proportion4 == null ? 0 : proportion4.getNowDate()) + "/" + (proportion4 == null ? 0 : proportion4.getTotalDate()));
        returnEntity.setAsiaEndDate(proportion4 == null ? null : proportion4.getFinishDate());

        MarketReturnEntity proportion5 = ctYfMarketMapper.getYFMarketProportion(5, studentId, contestId);
        returnEntity.setInternational((proportion5 == null ? 0 : proportion5.getNowDate()) + "/" + (proportion5 == null ? 0 : proportion5.getTotalDate()));
        returnEntity.setInternationalEndDate(proportion5 == null ? null : proportion5.getFinishDate());

        // P1 To P5
        MarketReturnEntity p1 = ctYfProductMapper.getYFProductProportion(1, studentId, contestId);
        returnEntity.setP1((p1 == null ? 0 : p1.getNowDate()) + "/" + (p1 == null ? 0 : p1.getTotalDate()));
        returnEntity.setP1EndDate(p1 == null ? null : p1.getFinishDate());

        MarketReturnEntity p2 = ctYfProductMapper.getYFProductProportion(2, studentId, contestId);
        returnEntity.setP2((p2 == null ? 0 : p2.getNowDate()) + "/" + (p2 == null ? 0 : p2.getTotalDate()));
        returnEntity.setP2EndDate(p2 == null ? null : p2.getFinishDate());

        MarketReturnEntity p3 = ctYfProductMapper.getYFProductProportion(3, studentId, contestId);
        returnEntity.setP3((p3 == null ? 0 : p3.getNowDate()) + "/" + (p3 == null ? 0 : p3.getTotalDate()));
        returnEntity.setP3EndDate(p3 == null ? null : p3.getFinishDate());

        MarketReturnEntity p4 = ctYfProductMapper.getYFProductProportion(4, studentId, contestId);
        returnEntity.setP4((p4 == null ? 0 : p4.getNowDate()) + "/" + (p4 == null ? 0 : p4.getTotalDate()));
        returnEntity.setP4EndDate(p4 == null ? null : p4.getFinishDate());

        MarketReturnEntity p5 = ctYfProductMapper.getYFProductProportion(5, studentId, contestId);
        returnEntity.setP5((p5 == null ? 0 : p5.getNowDate()) + "/" + (p5 == null ? 0 : p5.getTotalDate()));
        returnEntity.setP5EndDate(p5 == null ? null : p5.getFinishDate());

        // ISO认证
        MarketReturnEntity yp1 = ctYfIsoMapper.getYFISOProportion(1, studentId, contestId);
        returnEntity.setIso9000((yp1 == null ? 0 : yp1.getNowDate()) + "/" + (yp1 == null ? null : yp1.getTotalDate()));
        returnEntity.setIso9000EndDate(yp1 == null ? null : yp1.getFinishDate());

        MarketReturnEntity yp2 = ctYfIsoMapper.getYFISOProportion(2, studentId, contestId);
        returnEntity.setIso14000((yp2 == null ? 0 : yp2.getNowDate()) + "/" + (yp2 == null ? null : yp2.getTotalDate()));
        returnEntity.setIso14000EndDate(yp2 == null ? null : yp2.getFinishDate());

        return R.success().put("data", returnEntity);
    }

    /**
     * @Description: 库存采购信息
     * @Author:zhengxiaodong
     * @Param: []
     * @return: com.xiyou.common.utils.R
     */
    public R stockPurchaseInfo(Integer studentId, Integer contestId) {

        ProductMaterialEntity materialEntity = new ProductMaterialEntity();

        materialEntity.setKP1(ctKcProductMapper.getKCProductNumb(1, studentId, contestId));
        materialEntity.setKP2(ctKcProductMapper.getKCProductNumb(2, studentId, contestId));
        materialEntity.setKP3(ctKcProductMapper.getKCProductNumb(3, studentId, contestId));
        materialEntity.setKP4(ctKcProductMapper.getKCProductNumb(4, studentId, contestId));
        materialEntity.setKP5(ctKcProductMapper.getKCProductNumb(5, studentId, contestId));

        materialEntity.setKR1(ctKcMaterialMapper.getKcMaterial(1, studentId, contestId));
        materialEntity.setKR2(ctKcMaterialMapper.getKcMaterial(2, studentId, contestId));
        materialEntity.setKR3(ctKcMaterialMapper.getKcMaterial(3, studentId, contestId));
        materialEntity.setKR4(ctKcMaterialMapper.getKcMaterial(4, studentId, contestId));
        materialEntity.setKR5(ctKcMaterialMapper.getKcMaterial(5, studentId, contestId));

        materialEntity.setBR1(ctDdMaterialMapper.getddMaterialBlackCar(1, 0, studentId, contestId));
        materialEntity.setBR2(ctDdMaterialMapper.getddMaterialBlackCar(2, 0, studentId, contestId));
        materialEntity.setBR3(ctDdMaterialMapper.getddMaterialBlackCar(3, 0, studentId, contestId));
        materialEntity.setBR4(ctDdMaterialMapper.getddMaterialBlackCar(4, 0, studentId, contestId));
        materialEntity.setBR5(ctDdMaterialMapper.getddMaterialBlackCar(5, 0, studentId, contestId));

        materialEntity.setGR1(ctDdMaterialMapper.getddMaterialBlackCar(1, 1, studentId, contestId));
        materialEntity.setGR2(ctDdMaterialMapper.getddMaterialBlackCar(2, 1, studentId, contestId));
        materialEntity.setGR3(ctDdMaterialMapper.getddMaterialBlackCar(3, 1, studentId, contestId));
        materialEntity.setGR4(ctDdMaterialMapper.getddMaterialBlackCar(4, 1, studentId, contestId));
        materialEntity.setGR5(ctDdMaterialMapper.getddMaterialBlackCar(5, 1, studentId, contestId));

        return R.success().put("data", materialEntity);
    }


}
