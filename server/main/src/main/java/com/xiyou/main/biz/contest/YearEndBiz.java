package com.xiyou.main.biz.contest;

import com.xiyou.common.enums.CodeEnum;
import com.xiyou.common.utils.R;
import com.xiyou.main.entity.contest.*;
import com.xiyou.main.service.contest.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.*;

/**
 * @program: multi-module
 * @description:
 * @author: tangcan
 * @create: 2019-07-22 16:08
 **/
@Service
public class YearEndBiz {
    @Autowired
    private CtCashflowService ctCashflowService;
    @Autowired
    private CtChargesService ctChargesService;
    @Autowired
    private CtGzCsService ctGzCsService;
    @Autowired
    private CtWorkshopService ctWorkshopService;
    @Autowired
    private CtLineService ctLineService;
    @Autowired
    private CtXzOrderService ctXzOrderService;
    @Autowired
    private CtYfProductService ctYfProductService;
    @Autowired
    private CtYfMarketService ctYfMarketService;
    @Autowired
    private CtYfIsoService ctYfIsoService;
    @Autowired
    private CtProfitChartService ctProfitChartService;
    @Autowired
    private CtBalanceService ctBalanceService;
    @Autowired
    private CtFeeService ctFeeService;
    @Autowired
    private CtKcProductService ctKcProductService;
    @Autowired
    private CtKcMaterialService ctKcMaterialService;
    @Autowired
    private CtBankLoanService ctBankLoanService;
    @Autowired
    private CtTTimeService ctTTimeService;
    @Autowired
    private RestoreDataBiz restoreDataBiz;

    @Transactional
    public R confirm(Integer studentId, Integer contestId, Integer date) {
        // 当前现金
        int surplus = ctCashflowService.getCash(studentId, contestId);
        // gz_cs的管理费
        CtGzCs ctGzCs = ctGzCsService.getByContestId(contestId);
        int overhaul = (ctGzCs == null ? 0 : ctGzCs.getOverhaul());
        // 违约金比例
        double punishRate = (ctGzCs == null ? 0.0 : (double) ctGzCs.getPunish() / 100.0);
        // sum(（workshop中W_Pay_Date =当前时间-10）*该厂房对应租金)
        Map<String, Object> queryMap = new HashMap<>();
        queryMap.put("studentId", studentId);
        queryMap.put("contestId", contestId);
        queryMap.put("date", date - 10);
        queryMap.put("wStatus", 1);
        Integer rentFee = ctWorkshopService.queryWorkshopRentMoney(queryMap);

        if (rentFee == null) {
            rentFee = 0;
        }
        // sum[(当前line中PL_Remain_Date为null)*生产线所对应的维护费]
        int maintenanceFee = ctLineService.getMaintenanceFee(new CtLine().setStudentId(studentId).setContestId(contestId).setPlRemainDate(null));
        // xz_order.总价（当交单时间为空、年份=当前年份）
        List<CtXzOrder> orderList = ctXzOrderService.list(new CtXzOrder().setStudentId(studentId)
                .setContestId(contestId).setDate(date / 10).setCommitDate(0));
        // sum(四舍五入[xz_order.总价（当交单时间为空、年份=当前年份）*gz_cs.违约金比例]）
        int punish = orderList.stream().mapToInt(p -> (int) Math.round(p.getTotalPrice()*punishRate)).sum();
        // 现金余额
        int cashRest = surplus - overhaul - rentFee - maintenanceFee - punish;
        if (cashRest < 0) {
            return R.error(CodeEnum.OTHER_ERROR.getCode(), "现金不足");
        }
        // Cashflow新增一条记录ID、支付管理费、0、gz_cs中的管理费、现金+增加-减少、支付管理费、当前时间
        ctCashflowService.save(new CtCashflow().setStudentId(studentId)
                .setContestId(contestId)
                .setCAction("支付管理费")
                .setCIn(0)
                .setCOut(overhaul)
                .setCSurplus(surplus - overhaul)
                .setCComment("支付管理费" + overhaul + "W")
                .setCDate(date));
        // cashflow新增一条记录ID、租用厂房、0、该厂房对应租金、现金+增加-减少、支付厂房租金、当前时间
        ctCashflowService.save(new CtCashflow().setStudentId(studentId)
                .setContestId(contestId)
                .setCAction("租用厂房")
                .setCIn(0)
                .setCOut(rentFee)
                .setCSurplus(surplus - overhaul - rentFee)
                .setCComment("支付厂房租金" + rentFee + "W")
                .setCDate(date));

        // 更新最后付租时间
        ctWorkshopService.updateWpayDate(queryMap);

        // cashflow新增一条记录ID、维护费、0、sum[(当前line中PL_Remain_Date为null)*生产线所对应的维护费】、现金+增加-减少、支付维护费、当前时间
        ctCashflowService.save(new CtCashflow().setStudentId(studentId)
                .setContestId(contestId)
                .setCAction("维修费")
                .setCIn(0)
                .setCOut(maintenanceFee)
                .setCSurplus(surplus - overhaul - rentFee - maintenanceFee)
                .setCComment("支付维修费" + maintenanceFee + "W")
                .setCDate(date));
        // Cashflow新一条记录ID、违约金、0、sum(四舍五入【xz_order.总价（当交单时间为空、年份=当前年份）*gz_cs.违约金比例】】-、现金+增加-减少、支付违约金、当前时间
        ctCashflowService.save(new CtCashflow().setStudentId(studentId)
                .setContestId(contestId)
                .setCAction("违约金")
                .setCIn(0)
                .setCOut(punish)
                .setCSurplus(surplus - overhaul - rentFee - maintenanceFee - punish)
                .setCComment("支付违约金" + punish + "W")
                .setCDate(date));

        // Yf_product更新，If剩余研发时间=0，然后剩余研发时间 is null，研发完成时间=当前时间
        ctYfProductService.update(new CtYfProduct().setStudentId(studentId).setContestId(contestId).setDpFinishDate(date));
        // Yf_remark更新，If剩余研发时间=0，然后剩余研发时间 is null，研发完成时间=当前时间
        ctYfMarketService.update(new CtYfMarket().setStudentId(studentId).setContestId(contestId).setDmFinishDate(date));
        // Yf_ISO更新，If剩余研发时间=0，然后剩余研发时间 is null，研发完成时间=当前时间
        ctYfIsoService.update(new CtYfIso().setStudentId(studentId).setContestId(contestId).setDiFinishDate(date));


        // 费用表charges。当年结束后，自动插入一条记录到charges
        CtCharges charges = new CtCharges().setStudentId(studentId).setContestId(contestId).setCDate(date / 10);
        ctChargesService.addBySys(charges);
        // 更新合计字段
        ctChargesService.updateInformation(charges.getChargesId());

        /*
        添加记录到利润表profit_chart
         */
        CtProfitChart profitChart = new CtProfitChart().setStudentId(studentId).setContestId(contestId).setBsIsxt(1).setPcDate(date / 10);
        // 销售额 = Sum(已选定单xz_order.总价) 交单时间=本年
        // 成本 = Sum已选定单xz_order.数量*对应产品成本)交单时间=本年
        Map<String, BigDecimal> salesAndDirectCostMap = ctXzOrderService.getSalesAndDirectCost(studentId, contestId, date / 10);
        BigDecimal sales = salesAndDirectCostMap.get("sales");
        BigDecimal directCost = salesAndDirectCostMap.get("directCost");
        profitChart.setPcSales(sales.intValue());
        profitChart.setPcDirectCost(directCost.intValue());

        // 毛利=销售额-成本
        profitChart.setPcGoodsProfit(profitChart.getPcSales() - profitChart.getPcDirectCost());
        // 管理费用=Charges今年系统填写的总和数据
        profitChart.setPcTotal(ctChargesService.getInformation(studentId, contestId, date / 10, 1));

        // 折旧前利润=毛利-管理费用
        profitChart.setPcProfitBeforeDep(profitChart.getPcGoodsProfit() - profitChart.getPcTotal());
        // 折旧 IF【累计折旧<（（规则中折旧时间-1）*折旧费）&（当前时间-建成时间）>=10】Sum(折旧) ELSE 0
        profitChart.setPcDep(ctLineService.getDepTotal(studentId, contestId, date));
        // 财务费用前利润=折旧前利润-折旧
        profitChart.setPcProfitBeforeInterests(profitChart.getPcProfitBeforeDep() - profitChart.getPcDep());
        // 财务费用=Sum(cashflow.流出)当操作类型 in （归还长期贷款利息，归还短期贷款利息，贴息=贴现+厂房贴现）
        List<String> actionList = Arrays.asList("归还长期贷款利息", "归还短期贷款利息", "贴现", "厂房贴现");
        List<CtCashflow> cashflowList = ctCashflowService.list(studentId, contestId, date / 10, actionList);
        profitChart.setPcFinanceFee(cashflowList.stream().mapToInt(CtCashflow::getCOut).sum());
        // 税前利润=财务费用前利润-财务费用
        profitChart.setPcProfitBeforeTax(profitChart.getPcProfitBeforeInterests() - profitChart.getPcFinanceFee());
        // 税前利润
        int profitBeforeTax = profitChart.getPcProfitBeforeTax();
        // 所得税: 当【税前利润+上一年资产负债表所有者权益>max(之前年份所有者权益）】= round{【税前利润+上一年所有者权益-max[最近一个所得税>0的年份的权益，第0年权益]】*所得税率/100}
        // 所得税率比例
        double incomeTaxRate = ctGzCs == null ? 0.0 : (double) ctGzCs.getIncomeTax() / 100.0;
        // 上一年资产负债表所有者权益
        CtBalance lastYearBalance = ctBalanceService.get(new CtBalance().setStudentId(studentId).setContestId(contestId).setBsYear(date / 10 - 1));
        int lastYearTotalEquity = lastYearBalance == null ? 0 : lastYearBalance.getBsTotalEquity();

        // max(之前年份所有者权益）
        int maxTotalEquityPreYear = ctBalanceService.maxTotalEquityPreYear(studentId, contestId, date / 10);

        if ((profitBeforeTax + lastYearTotalEquity) > maxTotalEquityPreYear) {
            // 第0年权益
            CtBalance zeroYearBalance = ctBalanceService.get(new CtBalance().setStudentId(studentId).setContestId(contestId).setBsYear(0));
            int zeroYearTotalEquity = zeroYearBalance == null ? 0 : zeroYearBalance.getBsTotalEquity();

            // 最近一个所得税>0的年份的权益
            Integer equity = ctBalanceService.getRecentYEarHaveEquity(studentId, contestId, date / 10);
            int recentYEarHaveEquity = (equity == null ? 0 : equity);

            int tax = (int) Math.round(incomeTaxRate * (double) (profitBeforeTax + lastYearTotalEquity - Math.max(recentYEarHaveEquity, zeroYearTotalEquity)));
            profitChart.setPcTax(tax);
        } else {
            profitChart.setPcTax(0);
        }

        profitChart.setPcAnnualNetProfit(profitBeforeTax - profitChart.getPcTax());

        ctProfitChartService.save(profitChart);

        // Line表中更新，当【累计折旧<（（规则中折旧时间-1）*折旧费）&（当前时间-建成时间）>=10】那么累计折旧=累计折旧+规则中对应的折旧费，否则不更新
        // 先更新折旧费，再更新生产线的累计折旧
        ctLineService.updateDepFee(studentId, contestId, date);

        /*
        资产负债表
         */
        CtBalance balance = new CtBalance()
                .setStudentId(studentId)
                .setContestId(contestId)
                .setBsIsxt(1)
                .setBsYear(date / 10);

        // 现金 = Cashflow最新现金
        balance.setBsCash(cashRest);

        // 应收款 = Fee表合计
        int fee = ctFeeService.getFeeSum(new CtFee().setStudentId(studentId).setContestId(contestId));
        balance.setBsReceivable(fee);

        // 在制品 = Line表剩余生产时间不为空的sum(生产产品*所对应成本）
        int productInProcess = ctLineService.getProductInProcess(new CtLine().setStudentId(studentId).setContestId(contestId));
        balance.setBsProductInProcess(productInProcess);

        // 产成品 = Kc_product表sum(库存数*所对应成本)
        int product = ctKcProductService.getProductSum(studentId, contestId);
        balance.setBsProduct(product);

        // 原材料=Kc_material表sum（库存数*所对应成本）
        int material = ctKcMaterialService.getMaterialSum(studentId, contestId);
        balance.setBsMaterial(material);

        // 流动资产合计 = 现金+应收款+在制品+产成品+原材料
        balance.setBsTotalCurrentAsset(balance.getBsCash() + balance.getBsReceivable() + balance.getBsProductInProcess() + balance.getBsProduct() + balance.getBsMaterial());

        // 厂房=Workshop表sum（购买状态的厂房价值合计）
        int workshopFee = ctWorkshopService.getWorkshopFeeSum(studentId, contestId);
        balance.setBsWorkshop(workshopFee);

        // 生产线=Line表PL_Finish_Date不为空的sum（原值-折旧）
        List<CtLine> lineList = ctLineService.list(studentId, contestId);

        balance.setBsEquipment(lineList.stream().filter(p -> p.getPlFinishDate() != null).mapToInt(p -> p.getPlInvest() - p.getPlDepTotal()).sum());
        // 在建
        balance.setBsProjectOnConstruction(lineList.stream().filter(p -> p.getPlFinishDate() == null).mapToInt(CtLine::getPlInvest).sum());

        // 固定资产合计
        balance.setBsTotalFixedAsset(balance.getBsWorkshop() + balance.getBsEquipment() + balance.getBsProjectOnConstruction());

        // 资产合计
        balance.setBsTotalAsset(balance.getBsTotalCurrentAsset() + balance.getBsTotalFixedAsset());

        // 长期贷款:Bank_loan表类型为1的金额合计
        // 短期贷款:Bank_loan表类型为2的金额合计
        List<CtBankLoan> bankLoanList = ctBankLoanService.list(studentId, contestId);
        balance.setBsLongLoan(bankLoanList.stream().filter(p -> p.getBlType() == 1).mapToInt(CtBankLoan::getBlFee).sum());
        balance.setBsShortLoan(bankLoanList.stream().filter(p -> p.getBlType() == 2).mapToInt(CtBankLoan::getBlFee).sum());

        // 应交税费 = 利润表中的PC_TAX
        List<CtProfitChart> profitCharts = ctProfitChartService.list(new CtProfitChart().setStudentId(studentId).setContestId(contestId).setPcDate(date / 10));
        balance.setBsTax(profitCharts.stream().mapToInt(CtProfitChart::getPcTax).sum());

        // 负债合计 = 长期贷款+短期贷款+应交税费
        balance.setBsTotalLiability(balance.getBsLongLoan() + balance.getBsShortLoan() + balance.getBsTax());

        // 股东资本 = 第0年股东资本
        CtBalance ctBalance = ctBalanceService.get(new CtBalance().setStudentId(studentId).setContestId(contestId).setBsYear(0));
        balance.setBsEquity(ctBalance == null ? 0 : ctBalance.getBsEquity());

        // 利润留存 = 上一年所有者权益-股东资本
        balance.setBsRetainedEarning(lastYearTotalEquity - balance.getBsEquity());

        // 年度净利 = 利润表当年净利润
        CtProfitChart ctProfitChart = ctProfitChartService.getSys(new CtProfitChart().setStudentId(studentId).setContestId(contestId).setPcDate(date / 10));
        balance.setBsAnnualNetProfit(ctProfitChart == null ? 0 : ctProfitChart.getPcAnnualNetProfit());

        // 所有者权益 = 股东资本+利润留存+年度净利
        balance.setBsTotalEquity(balance.getBsEquity() + balance.getBsRetainedEarning() + balance.getBsAnnualNetProfit());

        // 负债所有者权益合计 = 所有者权益+负债合计
        balance.setBsTotal(balance.getBsTotalEquity() + balance.getBsTotalLiability());

        ctBalanceService.save(balance);

        /*
        time表时间=当前时间+6
         */
        ctTTimeService.addDate(studentId, contestId, 6);

        // 所有者权益为负后，自动结束比赛
        if (balance.getBsTotalEquity() < 0) {
            return R.error(7000, "当前所有者权益为负，你已破产，模拟竞赛结束");
        }
        //备份季度数据
        restoreDataBiz.backupSeasonData(contestId,studentId);
        return R.success();
    }
}
