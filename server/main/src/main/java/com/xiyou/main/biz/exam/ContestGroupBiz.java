package com.xiyou.main.biz.exam;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.xiyou.common.enums.CodeEnum;
import com.xiyou.common.utils.R;
import com.xiyou.main.entity.contest.ContestGroup;
import com.xiyou.main.service.contest.ContestGroupService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @program: multi-module
 * @description:
 * @author: tangcan
 * @create: 2019-07-09 12:22
 **/
@Service
public class ContestGroupBiz {
    @Autowired
    private ContestGroupService contestGroupService;

    public R list() {
        Map<String, Object> returnMap = new HashMap<>();
        returnMap.put("list", contestGroupService.list());
        return R.success(returnMap);
    }

    public R add(ContestGroup contestGroup) {
        QueryWrapper<ContestGroup> wrapper = new QueryWrapper<>();
        wrapper.eq("group_name", contestGroup.getGroupName());
        if (contestGroupService.getOne(wrapper) != null) {
            return R.error(CodeEnum.PARAM_ERROR.getCode(), "该分组已存在");
        }
        contestGroupService.insert(contestGroup);
        ContestGroup group = contestGroupService.getOne(wrapper);
        Map<String, Object> returnMap = new HashMap<>();
        returnMap.put("id", group.getId());
        return R.success(returnMap);
    }

    public R update(ContestGroup contestGroup) {
        if (contestGroupService.checkNameExist(contestGroup)) {
            return R.error(CodeEnum.PARAM_ERROR.getCode(), "该分组已存在");
        }
        contestGroupService.updateById(contestGroup);
        return R.success();
    }

    public R delete(Integer[] ids) {
        contestGroupService.removeByIds(Arrays.asList(ids));
        return R.success();
    }

    public R listByteacher(Integer teacherId) {
        List<ContestGroup> list = contestGroupService.getListByTeacher(teacherId);
        return R.success().put("list", list);
    }
}
