package com.xiyou.main.biz.exam;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.xiyou.common.enums.CodeEnum;
import com.xiyou.common.exception.CustomException;
import com.xiyou.common.office.utils.EasyPOIUtil;
import com.xiyou.common.utils.CommonUtil;
import com.xiyou.common.utils.R;
import com.xiyou.main.async.exam.AutoJudeAsyncService;
import com.xiyou.main.entity.exam.Exam;
import com.xiyou.main.entity.exam.ExamRecord;
import com.xiyou.main.entity.exam.Paper;
import com.xiyou.main.params.exam.ExamPaperParam;
import com.xiyou.main.params.exam.ExamStudentParam;
import com.xiyou.main.service.exam.ExamProblemService;
import com.xiyou.main.service.exam.ExamRecordService;
import com.xiyou.main.service.exam.ExamService;
import com.xiyou.main.service.exam.PaperService;
import com.xiyou.main.vo.exam.ExamAnalysis;
import com.xiyou.main.vo.exam.ExamPaper;
import com.xiyou.main.vo.exam.ExamScore;
import com.xiyou.main.vo.exam.ExamStudent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @program: multi-module
 * @description:
 * @author: tangcan
 * @create: 2019-07-10 13:57
 **/
@Service
public class ExamBiz {
    @Autowired
    private ExamService examService;
    @Autowired
    private AutoJudeAsyncService autoJudeAsyncService;
    @Autowired
    private ExamRecordService examRecordService;
    @Autowired
    private ExamProblemService examProblemService;
    @Autowired
    private PaperService paperService;

    public R paperList(Integer studentId, ExamPaperParam examPaperParam) {
        examPaperParam.setStudentId(studentId);
        Page<ExamPaper> examPaperPage = examService.getPage(examPaperParam);
        LocalDateTime now = LocalDateTime.now();
        examPaperPage.getRecords().forEach(p -> {
            // 试卷状态paper：0表示未开始，1表示考试中，2表示已结束
            // 考试状态exam： 0表示未交卷，1表示已交卷
            int paper = 0, exam;
            if (now.isBefore(p.getOpenTimeStart())) {
                // 未开始
                paper = 0;
            } else if (now.isAfter(p.getOpenTimeStart()) && now.isBefore(p.getOpenTimeEnd())) {
                // 考试中
                paper = 1;
            } else if (now.isAfter(p.getOpenTimeEnd())) {
                // 已结束
                paper = 2;
            }
            if (p.getCorrectCompleteTime() == null) {
                p.setGrade(null);
            }
            if (p.getIsHandIn() != null && p.getIsHandIn() == 1) {
                if (p.getCorrectCompleteTime() != null) {
                    // 阅卷完成
                    exam = 2;
                } else {
                    // 阅卷中
                    exam = 1;
                }
            } else {
                // 未交卷
                exam = 0;
            }
            p.setPaper(paper);
            p.setExam(exam);
        });
        Map<String, Object> returnMap = new HashMap<>();
        returnMap.put("list", examPaperPage.getRecords());
        returnMap.put("total", examPaperPage.getTotal());
        return R.success(returnMap);
    }

    /**
     * 检查学生是否能考试
     */
    public ExamPaper checkStudentPermission(Integer studentId, Integer examId) {
        ExamPaper examPaper = examService.getByExamId(examId);
        if (examPaper == null) {
            throw new CustomException(CodeEnum.NO_PERMISSION);
        }
        if (studentId != null && !examPaper.getStudentId().equals(studentId)) {
            throw new CustomException(CodeEnum.NO_PERMISSION);
        }
        LocalDateTime now = LocalDateTime.now();
        if (now.isBefore(examPaper.getOpenTimeStart())) {
            // 未开放
            throw new CustomException(CodeEnum.OTHER_ERROR.getCode(), "未到考试时间");
        }
        if (now.isAfter(examPaper.getOpenTimeEnd())) {
            // 已结束
            throw new CustomException(CodeEnum.OTHER_ERROR.getCode(), "考试已结束");
        }
        if (examPaper.getIsHandIn() != null && examPaper.getIsHandIn() == 1) {
            throw new CustomException(CodeEnum.OTHER_ERROR.getCode(), "你已交卷");
        }
        return examPaper;
    }

    /**
     * 检查教师是否能查看学生试卷
     */
    public ExamPaper checkTeacherPermission(Integer teacherId, Integer examId) {
        ExamPaper examPaper = examService.getByExamId(examId);
        if (examPaper == null || !examPaper.getTeacherId().equals(teacherId)) {
            throw new CustomException(CodeEnum.NO_PERMISSION);
        }
        if (examPaper.getIsHandIn() == null || examPaper.getIsHandIn() == 0) {
            throw new CustomException(CodeEnum.OTHER_ERROR.getCode(), "学生未交卷");
        }
        return examPaper;
    }

    /**
     * 检查考试时间限制
     */
    public ExamRecord checkTime(Integer examId) {
        ExamRecord examRecord = examRecordService.getCurrent(examId);
        if (examRecord == null) {
            throw new CustomException(CodeEnum.OTHER_ERROR.getCode(), "请先点击开始考试");
        }
        // if (examRecord.getEndTime().isBefore(LocalDateTime.now())) {
        //     throw new CustomException(CodeEnum.OTHER_ERROR.getCode(), "请先点击开始考试");
        // }
        return examRecord;
    }

    public R start(Integer studentId, Integer examId) {
        // 检查是否能开始
        ExamPaper examPaper = checkStudentPermission(studentId, examId);
        LocalDateTime now = LocalDateTime.now();
        // 查看考试记录，计算已经使用时间
        List<ExamRecord> examRecordList = examRecordService.getByExamId(examId);
        long spendTime = 0;

        if (examRecordList != null && examRecordList.size() > 0) {
            // 计算总共使用的时间
            for (ExamRecord examRecord : examRecordList) {
                Duration duration;
                if (now.isBefore(examRecord.getEndTime())) {
                    // 为避免考生意外退出再次进入考试时，
                    // 理论结束时间没有修改，系统判定考试时间用完的情况，
                    // 更新最后一次结束时间大于现在的记录
                    duration = Duration.between(examRecord.getStartTime(), now);
                    examRecord.setEndTime(now);
                    examRecordService.updateById(examRecord);
                } else {
                    duration = Duration.between(examRecord.getStartTime(), examRecord.getEndTime());
                }
                spendTime += duration.getSeconds();
            }
        }
        // 获取剩余考试时间(按秒计算)
        long restSecondTime = Math.max(0, examPaper.getExamTime() * 60 - spendTime);
        // 无剩余时间
        if (restSecondTime <= 0) {
            // 考试时间用完，强制交卷
            examService.update(new Exam().setId(examId).setIsHandIn(1));
            // 异步自动判题
            autoJudeAsyncService.autoJudge(examId);
            return R.error(CodeEnum.OTHER_ERROR.getCode(), "答题时间已用完");
        }

        // 本次考试开始时间
        // 本次考试理论结束时间（中途退出则更新结束时间为退出时间）
        LocalDateTime endTime = now.plusSeconds(restSecondTime);
        // 添加考试记录
        ExamRecord examRecord = new ExamRecord().setExamId(examId)
                .setStartTime(now)
                .setEndTime(endTime);
        examRecordService.save(examRecord);
        // 返回本次考试可用时间
        Map<String, Object> returnMap = new HashMap<>();
        returnMap.put("restTime", restSecondTime);
        return R.success(returnMap);
    }

    public R interrupt(Integer studentId, Integer examId) {
        checkStudentPermission(studentId, examId);
        // 判断时间
        ExamRecord examRecord = checkTime(examId);
        // 如果已经截止，直接返回
        if (LocalDateTime.now().isAfter(examRecord.getEndTime())) {
            return R.success();
        }
        // 更新结束时间
        examRecord.setEndTime(LocalDateTime.now());
        examRecordService.updateById(examRecord);
        return R.success();
    }

    public R get(Integer studentId, Integer examId) {
        ExamPaper examPaper = examService.getByExamId(examId);
        if (examPaper == null || !studentId.equals(examPaper.getStudentId())) {
            return R.error(CodeEnum.NO_PERMISSION);
        }
        Map<String, Object> returnMap = new HashMap<>();
        returnMap.put("info", examPaper);
        return R.success(returnMap);
    }

    public R handIn(Integer studentId, Integer examId) {
        checkStudentPermission(studentId, examId);
        // 检查考试时间限制
        ExamRecord examRecord = checkTime(examId);
        // 提交试卷
        examService.updateById(new Exam().setId(examId).setIsHandIn(1));
        // 更新考试结束时间
        examRecord.setEndTime(LocalDateTime.now());
        examRecordService.updateById(examRecord);

        // 异步自动判题
        autoJudeAsyncService.autoJudge(examId);
        return R.success();
    }

    public R studentList(Integer teacherId, ExamStudentParam examStudentParam) {
        /*
        阅卷时请求该考卷的学生列表，测试判断是否需要自动提交和自动判题，判断条件：
        首先将考试时间用完但未提交、考试开放时间结束但未提交的学生自动提交
         */
        Paper paper = paperService.getByPaperIdAndTeacherId(examStudentParam.getPaperId(), teacherId);
        if (paper == null) {
            Map<String, Object> returnMap = new HashMap<>();
            returnMap.put("list", new ArrayList<>());
            returnMap.put("total", 0);
            return R.success(returnMap);
        }
        List<ExamRecord> examRecordList = examRecordService.listByPaperId(examStudentParam.getPaperId());
        if (examRecordList.size() > 0) {
            // 按照examId分组
            Map<Integer, List<ExamRecord>> examRecordMap = examRecordList.stream().collect(Collectors.groupingBy(ExamRecord::getExamId));
            List<Integer> examIdList = new ArrayList<>(CommonUtil.getListInitCap(examRecordMap.size()));
            if (paper.getOpenTimeEnd().isBefore(LocalDateTime.now())) {
                // 开放时间已结束
                examIdList.addAll(examRecordMap.keySet());
            } else {
                examRecordMap.forEach((K, V) -> {
                    // 计算耗时
                    long spendTime = 0;
                    for (ExamRecord examRecord : V) {
                        Duration duration = Duration.between(examRecord.getStartTime(), examRecord.getEndTime());
                        spendTime += duration.getSeconds();
                    }
                    if (paper.getExamTime() * 60 <= spendTime) {
                        // 答题时间用完
                        examIdList.add(K);
                    }
                });
            }
            if (examIdList.size() > 0) {
                // 设置自动提交
                examService.handInByExamIds(examIdList);
                /*
                系统对客观题进行自动判题
                */
                examProblemService.autoJudgeByExamIds(examIdList);
            }
        }

        /*
        查询该考卷的学生
         */
        examStudentParam.setTeacherId(teacherId);
        Page<ExamStudent> examStudentPage = examService.getStudentList(examStudentParam);
        examStudentPage.getRecords().forEach(p -> {
            if (p.getCorrectCompleteTime() == null) {
                // 未批卷完成
                p.setStatus(0);
            } else {
                // 已批卷完成
                p.setStatus(1);
            }
            if (p.getIsHandIn() == null || p.getIsHandIn() == 0) {
                // 未交卷不让看成绩
                p.setGrade(null);
            }
        });
        Map<String, Object> returnMap = new HashMap<>();
        returnMap.put("list", examStudentPage.getRecords());
        returnMap.put("total", examStudentPage.getTotal());
        return R.success(returnMap);
    }

    public R analysis(Integer paperId) {
        Paper paper = paperService.getById(paperId);
        if (paper == null) {
            return R.error(CodeEnum.NO_PERMISSION);
        }
        int studentNum = examService.getHandInStudentNum(paperId);
        List<ExamAnalysis> examAnalysisList = examProblemService.analysis(paperId);
        if (studentNum > 0) {
            examAnalysisList.forEach(p -> {
                // 保留两位小数
                if (p.getStudentScoreSum() != null) {
                    p.setAvgScore((double) Math.round(p.getStudentScoreSum() * 100 / studentNum) / 100);
                }
            });
        }
        Map<String, Object> returnMap = new HashMap<>();
        returnMap.put("studentNum", studentNum);
        returnMap.put("list", examAnalysisList);
        return R.success(returnMap);
    }

    public void downloadAnalysis(Integer paperId, HttpServletResponse response) {
        Paper paper = paperService.getById(paperId);
        if (paper == null) {
            return;
        }
        int studentNum = examService.getHandInStudentNum(paperId);
        List<ExamAnalysis> examAnalysisList = examProblemService.analysis(paperId);
        Map<String, Object> paramMap = new HashMap<>();
        List<Map<String, Object>> mapList = new ArrayList<>(CommonUtil.getListInitCap(examAnalysisList.size()));
        int num = 0;
        for (ExamAnalysis examAnalysis : examAnalysisList) {
            Map<String, Object> map = new HashMap<>();
            map.put("id", ++num);
            map.put("title", examAnalysis.getTitle());
            map.put("intro", examAnalysis.getIntro());
            map.put("type", examAnalysis.getTypeName());
            map.put("group", examAnalysis.getGroupName());
            map.put("score", examAnalysis.getScore());
            map.put("scoreAvg", studentNum == 0 ? 0 : ((double) Math.round(examAnalysis.getStudentScoreSum() * 100 / studentNum) / 100));
            mapList.add(map);
        }
        paramMap.put("maplist", mapList);
        String paperName = "《" + paper.getTitle() + "》";
        paramMap.put("paperName", paperName);
        try {
            EasyPOIUtil.exportMultiSheetExcelByMap(response, paperName + "试题分析表" + ".xlsx", CommonUtil.getClassesPath() + "templates/paper_analysis.xlsx", paramMap, false);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void downloadScore(Integer paperId, HttpServletResponse response) {
        Paper paper = paperService.getById(paperId);
        if (paper == null) {
            return;
        }
        List<ExamScore> examScoreList = examProblemService.getScoreList(paperId);
        Map<String, Object> paramMap = new HashMap<>();
        List<Map<String, Object>> mapList = new ArrayList<>(CommonUtil.getListInitCap(examScoreList.size()));
        int num = 0;
        Map<String, Object> map;
        for (ExamScore examScore : examScoreList) {
            map = new HashMap<>();
            map.put("id", ++num);
            map.put("name", examScore.getName());
            map.put("account", examScore.getAccount());
            map.put("school", examScore.getSchool());
            map.put("score", examScore.getScore() == null ? "-" : examScore.getScore());
            mapList.add(map);
        }
        paramMap.put("maplist", mapList);
        String paperName = "《" + paper.getTitle() + "》";
        paramMap.put("title", paperName);
        try {
            EasyPOIUtil.exportMultiSheetExcelByMap(response, paperName + "考试成绩" + ".xlsx", CommonUtil.getClassesPath() + "templates/exam_score.xlsx", paramMap, false);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
