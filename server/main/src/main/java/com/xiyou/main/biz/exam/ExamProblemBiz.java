package com.xiyou.main.biz.exam;

import com.google.common.base.Joiner;
import com.xiyou.common.enums.CodeEnum;
import com.xiyou.common.utils.CommonUtil;
import com.xiyou.common.utils.R;
import com.xiyou.main.entity.exam.Exam;
import com.xiyou.main.entity.exam.ExamProblem;
import com.xiyou.main.entity.exam.Paper;
import com.xiyou.main.entity.exam.Resources;
import com.xiyou.main.params.exam.HandInAnswerParam;
import com.xiyou.main.service.exam.ExamProblemService;
import com.xiyou.main.service.exam.ExamService;
import com.xiyou.main.service.exam.PaperService;
import com.xiyou.main.service.exam.ResourcesService;
import com.xiyou.main.vo.exam.ExamPaper;
import com.xiyou.main.vo.exam.ExamProblemInfo;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @program: multi-module
 * @description:
 * @author: tangcan
 * @create: 2019-07-11 16:20
 **/
@Service
public class ExamProblemBiz {
    @Autowired
    private ExamProblemService examProblemService;
    @Autowired
    private ResourcesService resourcesService;
    @Autowired
    private ExamBiz examBiz;
    @Autowired
    private ExamService examService;
    @Autowired
    private PaperService paperService;

    public R listByStudent(Integer studentId, Integer examId) {
        // 检查是否能操作
        examBiz.checkStudentPermission(studentId, examId);
        // 获取所有的试题
        List<ExamProblemInfo> problemInfoList = examProblemService.getByExamId(examId);

        // 设置资源和图片
        setResources(problemInfoList);

        // 去除标准答案，解析，评分要点
        problemInfoList.forEach(p -> {
            if (p.getTypeId() == 10) {
                // 交单情况表需要提取叫单号:总共三列，提取第一列
                if (p.getAnswer() != null) {
                    String[] answer = p.getAnswer().split(",");
                    int len = answer.length;
                    List<String> orderNum = new ArrayList<>(Arrays.asList(answer).subList(0, len / 3));
                    p.setOrderNum(Joiner.on(",").join(orderNum));
                }
            }
            p.setAnswer(null)
                    .setAnalysis(null)
                    .setScorePoint(null);
        });
        Map<String, Object> returnMap = new HashMap<>();
        returnMap.put("list", problemInfoList);
        returnMap.put("total", problemInfoList.size());
        return R.success(returnMap);
    }

    /*
    设置考试题目的附件和图片
     */
    public void setResources(List<ExamProblemInfo> problemInfoList) {
        // 获取附件和图片信息
        List<String> fileNameList = new ArrayList<>();
        for (ExamProblemInfo problem : problemInfoList) {
            if (StringUtils.isNotBlank(problem.getAttachment())) {
                String[] files = problem.getAttachment().split("\\|");
                fileNameList.addAll(Arrays.asList(files));
            }
            if (StringUtils.isNotBlank(problem.getPic())) {
                String[] files = problem.getPic().split("\\|");
                fileNameList.addAll(Arrays.asList(files));
            }
        }
        fileNameList = fileNameList.stream().distinct().collect(Collectors.toList());

        // 获取所有资源
        List<Resources> resourcesList = resourcesService.getByFileNameList(fileNameList);

        // 映射fileName->Resources，方便查询
        Map<String, Resources> resourcesMap = resourcesList.stream().collect(Collectors.toMap(Resources::getFileName, p -> p, (k1, k2) -> k1));

        for (ExamProblemInfo problemInfo : problemInfoList) {
            // 附件
            if (StringUtils.isNotBlank(problemInfo.getAttachment())) {
                String[] files = problemInfo.getAttachment().split("\\|");
                List<Resources> attachmentList = new ArrayList<>();
                for (String file : files) {
                    if (file != null && resourcesMap.get(file) != null) {
                        attachmentList.add(resourcesMap.get(file));
                    }
                }
                problemInfo.setAttachmentList(attachmentList);
            } else {
                problemInfo.setAttachmentList(Collections.emptyList());
            }
            // 图片
            if (StringUtils.isNotBlank(problemInfo.getPic())) {
                String[] files = problemInfo.getPic().split("\\|");
                List<Resources> picList = new ArrayList<>();
                for (String file : files) {
                    if (file != null && resourcesMap.get(file) != null) {
                        picList.add(resourcesMap.get(file));
                    }
                }
                problemInfo.setPicList(picList);
            } else {
                problemInfo.setPicList(Collections.emptyList());
            }
        }
    }

    public R listByTeacher(Integer teacherId, Integer examId) {
        // 检查是否能操作
        examBiz.checkTeacherPermission(teacherId, examId);
        // 获取所有的试题
        List<ExamProblemInfo> problemInfoList = examProblemService.getByExamId(examId);

        // 设置资源和图片
        setResources(problemInfoList);

        Map<String, Object> returnMap = new HashMap<>();
        returnMap.put("list", problemInfoList);
        returnMap.put("total", problemInfoList.size());
        return R.success(returnMap);
    }

    public R save(Integer studentId, HandInAnswerParam answerParam) {
        ExamPaper examPaper = examBiz.checkStudentPermission(studentId, answerParam.getExamId());
        examBiz.checkTime(answerParam.getExamId());
        if (!studentId.equals(examPaper.getStudentId())) {
            return R.error(CodeEnum.NO_PERMISSION);
        }
        List<ExamProblem> examProblemList = new ArrayList<>(CommonUtil.getListInitCap(answerParam.getAnswerList().size()));
        for (HandInAnswerParam.Answer answer : answerParam.getAnswerList()) {
            if (StringUtils.isNotBlank(answer.getAnswer())) {
                examProblemList.add(new ExamProblem()
                        .setExamId(answerParam.getExamId())
                        .setProblemId(answer.getProblemId())
                        .setStudentAnswer(answer.getAnswer()));
            }
        }
        examProblemService.updateBatch(examProblemList);
        return R.success();
    }

    public R correct(Integer teacherId, Integer examId, Integer problemId, Double studentScore) {
        ExamPaper examPaper = examBiz.checkTeacherPermission(teacherId, examId);
        if (!examPaper.getTeacherId().equals(teacherId)) {
            return R.error(CodeEnum.NO_PERMISSION);
        }
        if (examPaper.getCorrectCompleteTime() != null) {
            return R.error(CodeEnum.OTHER_ERROR.getCode(), "已批卷完成");
        }
        ExamProblem examProblem = examProblemService.getByExamIdAndProblemId(examId, problemId);
        if (examProblem == null) {
            return R.error(CodeEnum.PARAM_ERROR.getCode(), "题目不存在");
        }
        if (studentScore > examProblem.getScore()) {
            return R.error(CodeEnum.PARAM_ERROR.getCode(), "超过本题限定分数");
        }
        if (examProblemService.updateById(new ExamProblem().setId(examProblem.getId()).setStudentScore(studentScore))) {
            return R.success();
        }
        return R.error(CodeEnum.OTHER_ERROR);
    }

    public R completeCorrect(Integer teacherId, Integer examId) {
        ExamPaper examPaper = examBiz.checkTeacherPermission(teacherId, examId);
        if (examPaper.getCorrectCompleteTime() != null) {
            return R.error(CodeEnum.OTHER_ERROR.getCode(), "你已经于" + examPaper.getCorrectCompleteTime() + "完成批改");
        }
        examService.updateById(new Exam().setId(examId).setCorrectCompleteTime(LocalDateTime.now()));
        return R.success();
    }

    public R studentView(Integer studentId, Integer examId) {
        Exam exam = examService.getById(examId);
        if (exam == null || !exam.getStudentId().equals(studentId)) {
            return R.error(CodeEnum.PARAM_ERROR);
        }
        Paper paper = paperService.getById(exam.getPaperId());
        if (paper == null || paper.getOpenTimeEnd().isAfter(LocalDateTime.now())) {
            return R.error(CodeEnum.PARAM_ERROR);
        }
        // 获取所有的试题
        List<ExamProblemInfo> problemInfoList = examProblemService.getByExamId(examId);

        // 设置资源和图片
        setResources(problemInfoList);

        Map<String, Object> returnMap = new HashMap<>();
        returnMap.put("openAnswer", paper.getOpenAnswer());
        returnMap.put("showAllProblem", paper.getShowAllProblem());
        returnMap.put("list", problemInfoList);
        returnMap.put("total", problemInfoList.size());
        return R.success(returnMap);
    }
}
