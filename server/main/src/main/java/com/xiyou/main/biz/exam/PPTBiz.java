package com.xiyou.main.biz.exam;

import com.xiyou.common.office.utils.FileUtil;
import com.xiyou.common.office.utils.PPTUtil;
import com.xiyou.common.utils.CommonUtil;
import com.xiyou.main.entity.exam.PptCourse;
import com.xiyou.main.service.exam.PptCourseService;
import org.apache.commons.lang3.StringUtils;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;

/**
 * @author dingyoumeng
 * @since 2019/07/11
 */
@Service
public class PPTBiz {

    @Autowired
    PptCourseService pptCourseService;
    @Autowired
    Environment environment;

    /**
     * ppt转png的异步方法
     *
     * @param pptSourceName ppt文件名
     * @param pdfName       输出PDF文件名
     * @param targetPath    输出png的路径
     * @param course
     * @return
     */
    @Async
    public void pptToPNGs(String pptSourceName, String pdfName, String targetPath, PptCourse course) {
        int pageCount = PPTUtil.pptToPictures(pptSourceName, pdfName, targetPath);
        course.setPicNum(pageCount);
        if (StringUtils.isBlank(course.getCover())) {
            // 获取第一张图片作为封面
            File firstImageFile = new File(targetPath + "1.png");
            //
            String coverPath = CommonUtil.getCatalinaHome() + environment.getProperty("teaching-platform.ppt-cover-place");
            String coverName = CommonUtil.getUniqueFileName(course.getFileName()) + ".png";
            File coverFile = new File(coverPath + "/" + coverName);
            try {
                // 图片文件复制
                Files.copy(firstImageFile.toPath(), coverFile.toPath());
                // 压缩
                FileUtil.compressImage(coverFile.getPath(), 40);
                course.setCover(coverName);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        pptCourseService.save(course);
    }
}
