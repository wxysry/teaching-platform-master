package com.xiyou.main.biz.exam;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.xiyou.common.enums.CodeEnum;
import com.xiyou.common.office.utils.FileUtil;
import com.xiyou.common.utils.CommonUtil;
import com.xiyou.common.utils.R;
import com.xiyou.main.entity.exam.*;
import com.xiyou.main.params.exam.PaperParam;
import com.xiyou.main.params.exam.PaperProblemParam;
import com.xiyou.main.params.exam.PaperStudentParam;
import com.xiyou.main.service.exam.*;
import com.xiyou.main.vo.exam.ExamProblemInfo;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @program: multi-module
 * @description:
 * @author: tangcan
 * @create: 2019-07-09 14:05
 **/
@Service
public class PaperBiz {
    private Logger log = LoggerFactory.getLogger(this.getClass());
    @Autowired
    private Environment environment;
    @Autowired
    private PaperService paperService;
    @Autowired
    private SysUserService sysUserService;
    @Autowired
    private ExamService examService;
    @Autowired
    private PaperProblemService paperProblemService;
    @Autowired
    private ExamProblemService examProblemService;
    @Autowired
    private ExamRecordService examRecordService;
    @Autowired
    private PaperStudentService paperStudentService;
    @Autowired
    private ExamProblemBiz examProblemBiz;

    public R uploadCover(MultipartFile file) {
        Map<String, Object> returnMap = new HashMap<>();
        returnMap.put("cover", FileUtil.upload(file, CommonUtil.getCatalinaHome() + environment.getProperty("teaching-platform.paper-cover-place"), true));
        return R.success(returnMap);
    }

    public void show(HttpServletResponse response, String pic) {
        try {
            FileUtil.exportPic(response, CommonUtil.getCatalinaHome() + environment.getProperty("teaching-platform.paper-cover-place") + pic);
        } catch (IOException e) {
            log.info(e.getMessage());
        }
    }

    public R addOrUpdate(Integer teacherId, Paper paper) {
        paper.setTeacherId(teacherId);
        if (paper.getIsPublish() == null) {
            paper.setIsPublish(0);
        }
        if (StringUtils.isBlank(paper.getCover())) {
            paper.setCover(null);
        }
        if (paper.getProblemNum() == null) {
            paper.setProblemNum(0);
        }
        if (paper.getTotalScore() == null) {
            paper.setTotalScore(0.00);
        }
        if (paper.getOpenAnswer() == null) {
            paper.setOpenAnswer(1);
        }
        if (paper.getShowAllProblem() == null) {
            paper.setShowAllProblem(1);
        }
        if (paper.getId() == null) {
            paperService.insert(paper);
        } else {
            if (!paperService.updateById(paper)) {
                return R.error(CodeEnum.PARAM_ERROR.getCode(), "试卷不存在");
            }
            examService.updateExamTime(paper.getId(), paper.getExamTime());
        }
        Map<String, Object> returnMap = new HashMap<>();
        returnMap.put("paperId", paper.getId());
        return R.success(returnMap);
    }

    public R publish(Integer teacherId, Integer paperId) {
        List<Integer> studentIdList = paperStudentService.getStudentIdsByPaperId(paperId);
        Paper paper = paperService.getById(paperId);
        if (paper == null || !paper.getTeacherId().equals(teacherId)) {
            return R.error(CodeEnum.OTHER_ERROR);
        }
        // 是否发布给所有人
        if (paper.getPublishAll() != null && paper.getPublishAll() == 1) {
            // 查出该教师的所有学生
            List<SysUser> studentList = sysUserService.getStudentList(teacherId);
            if (studentList.size() == 0) {
                return R.error(CodeEnum.OTHER_ERROR.getCode(), "暂无学生，无法发布试卷");
            }
            studentIdList = studentList.stream().map(SysUser::getId).collect(Collectors.toList());
        }
        if (studentIdList == null || studentIdList.size() == 0) {
            return R.error(CodeEnum.OTHER_ERROR.getCode(), "请选择学生");
        }

        if (paper.getIsPublish() == 1) {
            return R.error(CodeEnum.OTHER_ERROR.getCode(), "发布失败，试卷已发布");
        }
        // 试卷试题
        List<PaperProblem> paperProblemList = paperProblemService.get(new PaperProblem().setPaperId(paper.getId()));
        // 计算总分
        Double totalScore = paperProblemList.stream().mapToDouble(PaperProblem::getScore).sum();
        // 更新发布的状态
        paperService.update(new Paper()
                .setId(paperId)
                .setIsPublish(1)
                .setTotalScore(totalScore)
                .setStudentNum(studentIdList.size()));

        // 向学生发布试卷
        List<Exam> examList = new ArrayList<>(CommonUtil.getListInitCap(studentIdList.size()));
        for (Integer studentId : studentIdList) {
            examList.add(new Exam()
                    .setPaperId(paperId)
                    .setStudentId(studentId)
                    .setTeacherId(teacherId)
                    .setExamTime(paper.getExamTime())
                    .setIsHandIn(0)
                    .setProblemNum(paper.getProblemNum())
            );
        }
        examService.insertBatch(examList);

        // 添加本试卷每个学生的题目
        List<ExamProblem> examProblemList = new ArrayList<>(CommonUtil.getListInitCap(paperProblemList.size() * examList.size()));
        for (Exam exam : examList) {
            for (PaperProblem paperProblem : paperProblemList) {
                examProblemList.add(new ExamProblem()
                        .setExamId(exam.getId())
                        .setPaperId(paperId)
                        .setProblemId(paperProblem.getProblemId())
                        .setScore(paperProblem.getScore()));
            }
        }
        examProblemService.insertBatch(examProblemList);
        return R.success();
    }

    public R delete(Integer teacherId, Integer paperId) {
        Paper paper = paperService.getById(paperId);
        if (paper == null || !paper.getTeacherId().equals(teacherId)) {
            return R.error(CodeEnum.NO_PERMISSION);
        }
        // 删除paper和paper_problem数据
        paperService.removeById(paperId);
        paperProblemService.removeByPaperId(paperId);
        paperStudentService.removeByPaperId(paperId);
        if (paper.getIsPublish() == 1) {
            // 删除exam和exam_problem以及exam_record里面的数据
            List<Exam> examList = examService.getByPaperId(paperId);
            List<Integer> examIdList = examList.stream().map(Exam::getId).collect(Collectors.toList());
            examProblemService.removeByPaperId(paperId);
            examRecordService.removeByExamIds(examIdList);
        }

        return R.success();
    }

    public R list(Integer teacherId, PaperParam paperParam) {
        paperParam.setTeacherId(teacherId);
        Page<Paper> paperPage = paperService.getPage(paperParam);
        LocalDateTime now = LocalDateTime.now();
        paperPage.getRecords().forEach(p -> {
            // 设置试卷状态，0表示未发布，1表示未开始，2表示考试中，3表示已结束
            int status;

            if (p.getIsPublish() != null && p.getIsPublish() == 0) {
                // 未发布
                status = 0;
            } else if (now.isBefore(p.getOpenTimeStart())) {
                // 未开始
                status = 1;
            } else if (now.isAfter(p.getOpenTimeStart()) && now.isBefore(p.getOpenTimeEnd())) {
                // 考试中
                status = 2;
            } else if (now.isAfter(p.getOpenTimeEnd())) {
                // 已结束
                status = 3;
            } else {
                status = 0;
            }
            p.setStatus(status);
        });
        Map<String, Object> returnMap = new HashMap<>();
        returnMap.put("list", paperPage.getRecords());
        returnMap.put("total", paperPage.getTotal());
        return R.success(returnMap);
    }

    public R addProblem(Integer teacherId, PaperProblemParam problemParam) {
        Paper paper = paperService.getById(problemParam.getPaperId());
        if (paper == null) {
            return R.error(CodeEnum.PARAM_ERROR.getCode(), "试卷不存在");
        }
        if (!paper.getTeacherId().equals(teacherId)) {
            return R.error(CodeEnum.NO_PERMISSION);
        }
        if (paper.getIsPublish() == 1) {
            return R.error(CodeEnum.OTHER_ERROR.getCode(), "试卷已发布，不能编辑试题");
        }
        List<PaperProblem> paperProblemList = problemParam.getProblems();
        // 参数设置和校验
        double totalScore = 0;
        for (PaperProblem paperProblem : paperProblemList) {
            paperProblem.setPaperId(problemParam.getPaperId());
            totalScore += paperProblem.getScore();
        }
        if (totalScore > 999.99) {
            return R.error(CodeEnum.PARAM_ERROR.getCode(), "试卷总分只能在1000以内");
        }
        paper.setTotalScore(totalScore);
        paperService.update(new Paper().setId(paper.getId()).setTotalScore(totalScore).setProblemNum(paperProblemList.size()));
        // 先删除之前的题目，再添加
        paperProblemService.deleteAllProblem(paper.getId());
        // 加入新试题
        int success = paperProblemService.insertBatch(paperProblemList);
        return R.success("成功添加 " + success + " 个试题到：" + paper.getTitle());
    }

    public R addStudent(Integer teacherId, PaperStudentParam studentParam) {
        Paper paper = paperService.getById(studentParam.getPaperId());
        if (paper == null || !paper.getTeacherId().equals(teacherId)) {
            return R.error(CodeEnum.NO_PERMISSION);
        }
        if (studentParam.getPublishAll() != null && studentParam.getPublishAll() == 1) {
            // 全部发布
            paperService.updateById(new Paper().setId(studentParam.getPaperId()).setPublishAll(1));
        } else {
            // 指定人发布
            paperService.updateById(new Paper().setId(studentParam.getPaperId()).setPublishAll(0));
            List<PaperStudent> paperStudentList = new ArrayList<>();
            studentParam.getStudentList().forEach(p -> {
                paperStudentList.add(new PaperStudent().setPaperId(studentParam.getPaperId()).setStudentId(p));
            });
            // 先删除所有
            paperStudentService.removeByPaperId(studentParam.getPaperId());
            // 再插入
            paperStudentService.insertBatch(paperStudentList);
        }
        return R.success();
    }

    public R getPaper(Integer teacherId, Integer paperId) {
        Paper paper = paperService.getById(paperId);
        if (paper == null || !paper.getTeacherId().equals(teacherId)) {
            return R.error(CodeEnum.PARAM_ERROR.getCode(), "试卷不存在");
        }
        Map<String, Object> returnMap = new HashMap<>();
        returnMap.put("isPublish", paper.getIsPublish());
        returnMap.put("info", paper);
        return R.success(returnMap);
    }

    public R getProblem(Integer paperId) {
        // 获取所有的试题
        List<ExamProblemInfo> problemInfoList = paperProblemService.listByPaperId(paperId);

        // 设置资源和图片
        examProblemBiz.setResources(problemInfoList);
        Map<String, Object> returnMap = new HashMap<>();
        returnMap.put("problemList", problemInfoList);
        returnMap.put("total", problemInfoList.size());
        return R.success(returnMap);
    }

    public R getStudent(Integer paperId) {
        Paper paper = paperService.getById(paperId);
        if (paper == null) {
            return R.error(CodeEnum.PARAM_ERROR);
        }
        Integer publishAll = paper.getPublishAll();
        List<Integer> studentList = paperStudentService.getStudentIdsByPaperId(paperId);
        Map<String, Object> returnMap = new HashMap<>();
        returnMap.put("publishAll", publishAll);
        returnMap.put("studentList", studentList);
        return R.success(returnMap);
    }

    public R preview(Integer teacherId, Integer paperId) {
        Paper paper = paperService.getById(paperId);
        if (paper == null || !paper.getTeacherId().equals(teacherId)) {
            return R.error(CodeEnum.NO_PERMISSION);
        }
        Map<String, Object> returnMap = new HashMap<>();
        // 返回考试时间（转换为秒）
        returnMap.put("restTime", paper.getExamTime() * 60);
        return R.success(returnMap);
    }
}
