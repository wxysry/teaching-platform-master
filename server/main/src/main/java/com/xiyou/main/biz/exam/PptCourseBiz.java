package com.xiyou.main.biz.exam;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.xiyou.common.enums.CodeEnum;
import com.xiyou.common.office.utils.FileUtil;
import com.xiyou.common.utils.CommonUtil;
import com.xiyou.common.utils.R;
import com.xiyou.main.entity.exam.PptCourse;
import com.xiyou.main.entity.exam.PptGroup;
import com.xiyou.main.entity.exam.VideoGroup;
import com.xiyou.main.enums.ErrorEnum;
import com.xiyou.main.params.exam.CourseQueryParam;
import com.xiyou.main.service.exam.PptCourseService;
import com.xiyou.main.service.exam.PptGroupService;
import com.xiyou.main.service.exam.UserPptGroupService;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author dingyoumeng
 * @since 2019/07/08
 */
@Service
public class PptCourseBiz {
    private Logger log = LoggerFactory.getLogger(this.getClass());
    @Autowired
    Environment environment;
    @Autowired
    private PptGroupService groupService;
    @Autowired
    private PptCourseService pptCourseService;
    @Autowired
    private UserPptGroupService userPptGroupService;
    @Autowired
    private PptGroupService pptGroupService;
    @Autowired
    PPTBiz pptBiz;

    public R addGroup(PptGroup group) {
        QueryWrapper<PptGroup> wrapper = new QueryWrapper<>();
        wrapper.eq("group_name", group.getGroupName());
        if (pptGroupService.getOne(wrapper) != null) {
            return R.error(CodeEnum.PARAM_ERROR.getCode(), "分组已存在");
        }
        groupService.insert(group);
        PptGroup pptGroup = pptGroupService.getOne(wrapper);
        Map<String, Object> returnMap = new HashMap<>();
        returnMap.put("id", pptGroup.getId());
        return R.success(returnMap);
    }

    public R listGroup() {
        List<PptGroup> list = groupService.list();
        Map<String, Object> resultMap = new HashMap<>(16);
        resultMap.put("list", list);
        return R.success(resultMap);
    }

    public R updateGroup(PptGroup group) {
        QueryWrapper<PptGroup> wrapper = new QueryWrapper<>();
        wrapper.eq("group_name", group.getGroupName());
        PptGroup pptGroup = groupService.getOne(wrapper);
        if (pptGroup != null && !pptGroup.getId().equals(group.getId())) {
            return ErrorEnum.COURSE_GROUP_EXIST.getR();
        }
        groupService.updateById(group);
        return R.success();
    }

    public R deleteGroup(String... ids) {
        groupService.removeByIds(Arrays.asList(ids));
        pptCourseService.removeByGroupIds(Arrays.asList(ids));
        userPptGroupService.removeByGroupIds(Arrays.asList(ids));
        return R.success();
    }

    public R getGroupById(int groupId) {
        Map<String, Object> resultMap = new HashMap<>(16);
        PptGroup group = groupService.getById(groupId);
        resultMap.put("group", group);
        return R.success(resultMap);
    }

    public R addCourse(PptCourse course) {
        pptCourseService.save(course);
        return R.success();
    }

    public R deleteCourse(Integer[] ids) {
        pptCourseService.removeByIds(Arrays.asList(ids));
        return R.success();
    }

    public R updateCourse(PptCourse course) {
        pptCourseService.updateById(course);
        return R.success();
    }

    public R listCourse(CourseQueryParam param) {
        Page page = param.getIPage();
        QueryWrapper<PptCourse> wrapper = new QueryWrapper<>();
        if (param.getGroupId() != null && param.getGroupId() != 0) {
            wrapper.eq("group_id", param.getGroupId());
        }
        if (StringUtils.isNotBlank(param.getValue())) {
            wrapper.like("ppt_name", param.getValue());
        }
        wrapper.orderByDesc("id");
        List<PptCourse> list = pptCourseService.page(page, wrapper).getRecords();
        List<PptGroup> groupList = pptGroupService.list();
        Map<Integer, String> groupNameMap = groupList.stream().collect(Collectors.toMap(PptGroup::getId, PptGroup::getGroupName, (k1, k2) -> k1));
        // 文件地址
        String filePrefix = CommonUtil.getCatalinaHome() + environment.getProperty("teaching-platform.ppt-png-place");
        filePrefix = filePrefix.substring(filePrefix.indexOf("/teaching_platform_file"));
        for (PptCourse p : list) {
            p.setGroupName(groupNameMap.get(p.getGroupId()));
            p.setFolderName(filePrefix + p.getFolderName() + "/");
        }
        Map<String, Object> resultMap = new HashMap<>(16);
        resultMap.put("total", pptCourseService.count(wrapper));
        resultMap.put("list", list);
        return R.success(resultMap);
    }

    public R uploadCover(MultipartFile file) {
        Map<String, Object> returnMap = new HashMap<>();
        returnMap.put("cover", FileUtil.upload(file, CommonUtil.getCatalinaHome() + environment.getProperty("teaching-platform.ppt-cover-place"), true));
        return R.success(returnMap);
    }


    public R uploadPpt(MultipartFile file, PptCourse course) {
        Map<String, Object> returnMap = new HashMap<>();
        String sourcePath = CommonUtil.getCatalinaHome() + environment.getProperty("teaching-platform.ppt-course-place");
        String pptSourceName = sourcePath + FileUtil.upload(file, sourcePath);
        File f = new File(pptSourceName);
        String fileName = f.getName();
        String folder = CommonUtil.getUniqueFileName(fileName);
        String pdfPath = CommonUtil.getCatalinaHome() + environment.getProperty("teaching-platform.ppt-pdf-place");
        String pngPath = CommonUtil.getCatalinaHome() + environment.getProperty("teaching-platform.ppt-png-place");
        String pdfName = pdfPath + "/" + folder + ".pdf";
        course.setFileName(fileName);
        course.setFolderName(folder);
        pptBiz.pptToPNGs(pptSourceName, pdfName, pngPath + "/" + folder + "/", course);
        return R.success(returnMap);
    }

    public void showCover(HttpServletResponse response, String cover) {
        if (StringUtils.isBlank(cover)) {
            return;
        }
        try {
            FileUtil.exportPic(response, CommonUtil.getCatalinaHome() + environment.getProperty("teaching-platform.ppt-cover-place") + cover);
        } catch (IOException e) {
            log.info(e.getMessage());
        }
    }

    public void showPptById(HttpServletResponse response, String folderPath, int id) {
        if (StringUtils.isBlank(folderPath)) {
            return;
        }
        try {
            FileUtil.exportPic(response, CommonUtil.getCatalinaHome() + environment.getProperty("teaching-platform.ppt-png-place") + folderPath + "/" + id + ".png");
        } catch (IOException e) {
            log.info(e.getMessage());
        }
    }

    public R listByTeacher(Integer userId, CourseQueryParam param) {
        param.setTeacherId(userId);
        Page<PptCourse> coursePage = pptCourseService.listByUser(param);
        // 文件地址
        String filePrefix = CommonUtil.getCatalinaHome() + environment.getProperty("teaching-platform.ppt-png-place");
        filePrefix = filePrefix.substring(filePrefix.indexOf("/teaching_platform_file"));
        for (PptCourse p : coursePage.getRecords()) {
            p.setFolderName(filePrefix + p.getFolderName() + "/");
        }

        Map<String, Object> resultMap = new HashMap<>(16);
        resultMap.put("list", coursePage.getRecords());
        resultMap.put("total", coursePage.getTotal());
        return R.success(resultMap);
    }

    public R listGroupByTeacher(Integer userId) {
        List<VideoGroup> list = groupService.listByTeacher(userId);
        Map<String, Object> resultMap = new HashMap<>(16);
        resultMap.put("list", list);
        return R.success(resultMap);
    }
}
