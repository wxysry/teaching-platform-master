package com.xiyou.main.biz.exam;

import cn.afterturn.easypoi.excel.entity.result.ExcelImportResult;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.xiyou.common.enums.CodeEnum;
import com.xiyou.common.exception.CustomException;
import com.xiyou.common.office.utils.EasyPOIUtil;
import com.xiyou.common.utils.CommonUtil;
import com.xiyou.common.utils.R;
import com.xiyou.main.async.exam.ProblemAsyncService;
import com.xiyou.main.entity.exam.*;
import com.xiyou.main.params.exam.ProblemInfo;
import com.xiyou.main.params.exam.ProblemParam;
import com.xiyou.main.pojo.exam.ImportProblem;
import com.xiyou.main.service.exam.*;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.util.*;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.stream.Collectors;

/**
 * @program: multi-module
 * @description:
 * @author: tangcan
 * @create: 2019-07-06 21:13
 **/
@Service
public class ProblemBiz {
    @Autowired
    private ProblemService problemService;
    @Autowired
    private ProblemTypeService problemTypeService;
    @Autowired
    private ProblemGroupService problemGroupService;
    @Autowired
    private ProblemOptionService problemOptionService;
    @Autowired
    private ProblemAsyncService problemAsyncService;
    @Autowired
    private ResourcesService resourcesService;

    public R upload(Integer userId, MultipartFile file) {
        // 获取导入的数据并校验
        ExcelImportResult<ImportProblem> result;
        // 表格标题的行数
        int titleRows = 0;
        // 表格头的行数
        int headRows = 1;
        try {
            result = EasyPOIUtil.importAndVerfiy(file, titleRows, headRows, ImportProblem.class);
        } catch (Exception e) {
            return R.error(CodeEnum.FILE_UPLOAD_FAIL.getCode(), e.getMessage());
        }
        Map<String, Object> returnMap = new HashMap<>();
        if (result.isVerfiyFail()) {
            returnMap.put("error", EasyPOIUtil.getFailMsgList(result.getFailList(), titleRows, headRows));
            return R.error(CodeEnum.FILE_UPLOAD_FAIL, returnMap);
        }

        int total = result.getList().size();
        List<ImportProblem> successList = result.getList();
        int listCap = CommonUtil.getListInitCap(total);

        // 查出分组信息，和试题类型
        List<ProblemGroup> groupList = problemGroupService.list();
        List<ProblemType> typeList = problemTypeService.list();

        // 统计各题型的数量
        Map<String, Integer> typeNumMap = new LinkedHashMap<>(CommonUtil.getMapInitCap(total));

        // 用map映射分组，便于查找,不同的一级分组下，二级分组名称可能相同，因此需要fatherId和groupName一起作为key值
        Map<String, ProblemGroup> groupMap = new HashMap<>(CommonUtil.getMapInitCap(groupList.size()));
        for (ProblemGroup group : groupList) {
            groupMap.put(group.getGroupName() + ":" + group.getFatherId(), group);
        }

        Map<Integer, ProblemType> typeMap = typeList.stream()
                .collect(Collectors.toMap(ProblemType::getTypeCode, a -> a, (k1, k2) -> k1));

        // 提取题目、选项，并检查题型和分组信息正确性
        List<Problem> problemList = new ArrayList<>(listCap);
        List<ProblemOption> optionList = new ArrayList<>(listCap);
        List<String> errorList = new ArrayList<>();

        for (ImportProblem importProblem : successList) {
            if (errorList.size() >= 10) {
                errorList.add("...");
                break;
            }
            Integer typeCode = Integer.valueOf(importProblem.getProblemTypeCode());
            ProblemType type = typeMap.get(typeCode);
            ProblemGroup group = groupMap.get(importProblem.getGroupName() + ":" + 0);
            ProblemGroup subGroup = null;
            if (group != null) {
                subGroup = groupMap.get(importProblem.getSubgroupName() + ":" + group.getId());
            }
            // 检查数据正确性
            StringBuilder error = new StringBuilder();
            if (type == null) {
                error.append("题型编号不正确;");
            }
            // 判断一级分组和二级分组的正确性
            if (group == null || subGroup == null) {
                error.append("分组不正确;");
            }

            if (error.length() > 0) {
                errorList.add("第" + (titleRows + headRows + importProblem.getRowNum()) + "行的数据：" + error.toString());
                continue;
            }

            // 统计各题型的数量
            Integer cnt = typeNumMap.get(type.getTypeName());
            if (cnt == null) {
                cnt = 1;
            } else {
                cnt++;
            }
            typeNumMap.put(type.getTypeName(), cnt);

            // 是否是选择题
            int isChoice = isChoice(type.getTypeName(), typeCode);

            // 答案(先判断答案，选择题答案可能会出现格式错误的情况)
            String answer;
            if (isChoice > 0) {
                answer = getChoiceAnswer(importProblem.getAnswer());
                if (answer == null) {
                    error.append("答案不符合规定;");
                }
            } else {
                answer = importProblem.getAnswer();
            }

            // 试题
            Problem problem = new Problem().setTypeId(type.getId())
                    .setGroupId(group.getId())
                    .setSubgroupId(subGroup.getId())
                    .setIsChoice(isChoice)
                    .setTitle(importProblem.getProblemTitle())
                    .setPic(importProblem.getPic())
                    .setAnswer(answer)
                    .setAnalysis(importProblem.getAnalysis())
                    .setScorePoint(importProblem.getScorePoint())
                    .setAttachment(importProblem.getAttachment())
                    .setIntro(importProblem.getIntro())
                    .setCreateUserId(userId)
                    .setRowNum(importProblem.getRowNum());
            if (problem.getTypeId() >= 5) {
                problem.setAnswer(this.answerAnalysis(answer));
            }
            problemList.add(problem);

            // 选项 A->0 B->1 C->2 D->3 E->4 F->5
            if (problem.getIsChoice() > 0) {
                addOption(optionList, importProblem.getOptionA(), 0, null, importProblem.getRowNum());
                addOption(optionList, importProblem.getOptionB(), 1, null, importProblem.getRowNum());
                addOption(optionList, importProblem.getOptionC(), 2, null, importProblem.getRowNum());
                addOption(optionList, importProblem.getOptionD(), 3, null, importProblem.getRowNum());
                addOption(optionList, importProblem.getOptionE(), 4, null, importProblem.getRowNum());
                addOption(optionList, importProblem.getOptionF(), 5, null, importProblem.getRowNum());
                if (optionList.size() < 4) {
                    error.append("选项至少4项;");
                }
            }
            if (error.length() > 0) {
                errorList.add("第" + (titleRows + headRows + importProblem.getRowNum()) + "行的数据：" + error.toString());
            }
        }

        if (errorList.size() > 0) {
            returnMap.put("error", errorList);
            return R.error(CodeEnum.FILE_UPLOAD_FAIL, returnMap);
        }

        // 插入试题，并返回主键id
        problemService.insertBatch(problemList);

        // rowNum->problemId
        Map<Integer, Integer> problemIdMap = problemList.stream()
                .collect(Collectors.toMap(Problem::getRowNum, Problem::getId, (k1, k2) -> k1));

        for (ProblemOption option : optionList) {
            option.setProblemId(problemIdMap.get(option.getRowNum()));
        }

        // 插入选项
        problemOptionService.insertBatch(optionList);

        StringBuilder builder = new StringBuilder();
        builder.append("成功上传: ");
        typeNumMap.forEach((k, v) -> {
            builder.append(v).append("个").append(k).append(',');
        });
        return R.success(builder.toString());
    }

    // 判断是否是选择题
    public int isChoice(String typeName, Integer typeCode) {
        if ("单选题".equals(typeName) || typeCode == 1) {
            return 1;
        }
        if ("多选题".equals(typeName) || typeCode == 2) {
            return 2;
        }
        return 0;
    }

    // 添加选项
    private void addOption(List<ProblemOption> optionList, String content, Integer number, Integer problemId, Integer rowNum) {
        if (StringUtils.isBlank(content)) {
            return;
        }
        ProblemOption option = new ProblemOption().setOption(content)
                .setNumber(number);
        if (problemId != null) {
            option.setProblemId(problemId);
        }
        if (rowNum != null) {
            option.setRowNum(rowNum);
        }
        optionList.add(option);
    }

    // 选择题的答案ABC换成0,1,2的形式，序号从小到大
    public String getChoiceAnswer(String answer) {
        int optionNum = 6;
        if (StringUtils.isNotBlank(answer) &&
                StringUtils.isAllUpperCase(answer) &&
                answer.length() <= optionNum) {
            int lenth = answer.length();
            StringBuilder builder = new StringBuilder();
            // 获取所有答案，对应位置标志为1
            int[] num = {0, 0, 0, 0, 0, 0, 0};
            for (int i = 0; i < lenth; i++) {
                int x = answer.charAt(i) - 'A';
                if (x >= optionNum || x < 0) {
                    return null;
                }
                num[x] = 1;
            }
            for (int i = 0; i < optionNum; i++) {
                if (num[i] == 1) {
                    if (builder.length() == 0) {
                        builder.append(i);
                    } else {
                        builder.append(',').append(i);
                    }
                }
            }
            return builder.toString();
        }
        return null;
    }

    public R add(Integer userId, ProblemInfo problemInfo) {
        ProblemType type = problemTypeService.getById(problemInfo.getTypeId());
        // 选择题
        int isChoice = isChoice(type.getTypeName(), type.getTypeCode());
        // 先判断answer和选项是否符合规定
        String answer = null;
        if (isChoice == 1) {
            // 判断选项
            if (problemInfo.getOptionList() == null || problemInfo.getOptionList().size() == 0) {
                return R.error(CodeEnum.PARAM_ERROR.getCode(), "请填写选项");
            }
            answer = getChoiceAnswer(problemInfo.getAnswer());
            if (answer == null) {
                return R.error(CodeEnum.PARAM_ERROR.getCode(), "答案不符合规定");
            }
        }
        // 试题
        Problem problem = new Problem().setTypeId(problemInfo.getTypeId())
                .setGroupId(problemInfo.getGroupId())
                .setSubgroupId(problemInfo.getSubgroupId())
                .setIsChoice(isChoice)
                .setTitle(problemInfo.getTitle())
                .setPic(problemInfo.getPic())
                .setAnalysis(problemInfo.getAnalysis())
                .setScorePoint(problemInfo.getScorePoint())
                .setAttachment(problemInfo.getAttachment())
                .setIntro(problemInfo.getIntro())
                .setCreateUserId(userId);
        if (isChoice > 0) {
            problem.setAnswer(answer);
        } else {
            problem.setAnswer(problemInfo.getAnswer());
        }
        if (problem.getTypeId() >= 5) {
            problem.setAnswer(this.answerAnalysis(problemInfo.getAnswer()));
        }
        // 试题存库
        problemService.save(problem);

        // 选择题
        if (problem.getIsChoice() > 0) {
            List<ProblemOption> optionList = new ArrayList<>();
            List<String> options = problemInfo.getOptionList();
            for (int i = 0; i < options.size(); i++) {
                addOption(optionList, options.get(i), i, problem.getId(), null);
            }
            problemOptionService.insertBatch(optionList);
        }
        return R.success("成功新增 1 个" + type.getTypeName());
    }

    public R update(ProblemInfo problemInfo) {
        ProblemType type = problemTypeService.getById(problemInfo.getTypeId());
        // 是否是选择题
        int isChoice = isChoice(type.getTypeName(), type.getTypeCode());
        // 先判断answer和选项是否符合规定
        String answer = null;
        if (isChoice > 0) {
            // 判断选项
            if (problemInfo.getOptionList() == null || problemInfo.getOptionList().size() == 0) {
                return R.error(CodeEnum.PARAM_ERROR.getCode(), "请填写选项");
            }
            answer = getChoiceAnswer(problemInfo.getAnswer());
            if (answer == null) {
                return R.error(CodeEnum.PARAM_ERROR.getCode(), "答案不符合规定");
            }
        } else {
            answer = problemInfo.getAnswer();
        }
        // 试题
        Problem problem = new Problem().setId(problemInfo.getId())
                .setTypeId(problemInfo.getTypeId())
                .setGroupId(problemInfo.getGroupId())
                .setSubgroupId(problemInfo.getSubgroupId())
                .setIsChoice(isChoice)
                .setTitle(problemInfo.getTitle())
                .setPic(problemInfo.getPic())
                .setAnswer(answer)
                .setAnalysis(problemInfo.getAnalysis())
                .setScorePoint(problemInfo.getScorePoint())
                .setIsChoice(isChoice)
                .setAttachment(problemInfo.getAttachment())
                .setIntro(problemInfo.getIntro());
        if (problem.getTypeId() >= 5) {
            problem.setAnswer(this.answerAnalysis(answer));
        }
        // 试题存库
        if (!problemService.update(problem)) {
            return R.error(CodeEnum.PARAM_ERROR.getCode(), "试题不存在");
        }

        // 选择题
        if (problem.getIsChoice() > 0) {
            List<ProblemOption> optionList = new ArrayList<>();
            List<String> options = problemInfo.getOptionList();
            for (int i = 0; i < options.size(); i++) {
                addOption(optionList, options.get(i), i, problem.getId(), null);
            }
            // 删除之前的
            List<Integer> problemIdList = new ArrayList<>();
            problemIdList.add(problem.getId());
            problemOptionService.removeByProblemIds(problemIdList);
            // 插入
            problemAsyncService.saveProblemOption(optionList);
        }

        return R.success("更新成功");
    }

    public R adminGet(Integer id) {
        Problem problem = problemService.selectById(id);
        if (problem == null) {
            return R.error(CodeEnum.PARAM_ERROR.getCode(), "试题不存在");
        }

        return getProblemInfo(problem, false);
    }

    public R getProblemInfo(Problem problem, boolean isTeacher) {
        ProblemInfo problemInfo = new ProblemInfo();
        problemInfo.setGroupName(problem.getGroupName() + "/" + problem.getSubgroupName());
        List<Integer> groupIds = new ArrayList<>();
        groupIds.add(problem.getGroupId());
        groupIds.add(problem.getSubgroupId());
        // 试题
        problemInfo.setTypeId(problem.getTypeId())
                .setTypeName(problem.getTypeName())
                .setIsChoice(problem.getIsChoice())
                .setTitle(problem.getTitle())
                .setGroupId(problem.getGroupId())
                .setSubgroupId(problem.getSubgroupId())
                .setGroupIds(groupIds)
                .setPic(problem.getPic())
                .setAnswer(problem.getAnswer())
                .setAnalysis(problem.getAnalysis())
                .setScorePoint(problem.getScorePoint())
                .setAttachment(problem.getAttachment())
                .setIntro(problem.getIntro())
                .setCreateUserName(problem.getCreateUserName())
                .setCreateTime(problem.getCreateTime());
        // 解答题处理答案
        if (problemInfo.getTypeId() >= 5 && !isTeacher) {
            problemInfo.setAnswer(this.answerRestore(problem.getAnswer()));
        }
        // 附件列表
        if (StringUtils.isNotBlank(problem.getAttachment())) {
            String[] files = problem.getAttachment().split("\\|");
            problemInfo.setAttachmentList(resourcesService.getList(Arrays.asList(files), 0));
        } else {
            problemInfo.setAttachmentList(Collections.EMPTY_LIST);
        }

        // 图片资源
        if (StringUtils.isNotBlank(problem.getPic())) {
            String[] files = problem.getPic().split("\\|");
            problemInfo.setPicList(resourcesService.getList(Arrays.asList(files), 1));
        } else {
            problemInfo.setPicList(Collections.EMPTY_LIST);
        }

        // 选项
        if (problem.getIsChoice() > 0) {
            List<ProblemOption> optionList = problemOptionService.getList(problem.getId());
            problemInfo.setOptionList(optionList.stream().map(ProblemOption::getOption).collect(Collectors.toList()));
            problemInfo.setAnswer(formatChoiceAnswer(problem.getAnswer()));
        }

        Map<String, Object> returnMap = new HashMap<>();
        returnMap.put("data", problemInfo);
        return R.success(returnMap);
    }


    public R teacherGet(Integer teacherId, Integer problemId) {
        Problem problem = problemService.getByTeacher(teacherId, problemId);
        if (problem == null) {
            return R.error(CodeEnum.PARAM_ERROR.getCode(), "试题不存在");
        }
        return getProblemInfo(problem, true);
    }

    public R listByAdmin(ProblemParam problemParam) {
        // 管理员查询全部(除非有条件限定)
        Page<ProblemInfo> problemPage = problemService.getList(null, problemParam);
        return getProblemList(problemPage);
    }

    public R listByTeacher(Integer userId, ProblemParam problemParam) {
        // 教师根据分组权限查询
        Page<ProblemInfo> problemPage = problemService.getList(userId, problemParam);
        return getProblemList(problemPage);
    }

    private R getProblemList(Page<ProblemInfo> problemPage) {
        // 试题
        List<ProblemInfo> problemList = problemPage.getRecords();
        // 获取所有的problemId
        List<Integer> problemIdList = problemList.stream().map(ProblemInfo::getId).collect(Collectors.toList());

        // 选项
        Future<Map<Integer, List<ProblemOption>>> optionFuture = problemAsyncService.getOptionMap(problemIdList);

        // 附件和图片
        Future<Map<String, Resources>> resourcesFuture = problemAsyncService.getResourcesMap(problemList);

        while (true) {
            if (optionFuture.isDone() && resourcesFuture.isDone()) {
                break;
            }
        }
        Map<Integer, List<ProblemOption>> optionMap;
        Map<String, Resources> resourcesMap;
        try {
            optionMap = optionFuture.get();
            resourcesMap = resourcesFuture.get();
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
            return R.error(CodeEnum.OTHER_ERROR);
        }


        // 试题信息
        for (ProblemInfo problemInfo : problemList) {
            // 选择题
            if (problemInfo.getIsChoice() > 0) {
                List<ProblemOption> options = optionMap.get(problemInfo.getId());
                if (options != null) {
                    problemInfo.setOptionList(options.stream().map(ProblemOption::getOption).collect(Collectors.toList()));
                }
                // 重新格式化答案
                problemInfo.setAnswer(formatChoiceAnswer(problemInfo.getAnswer()));
            }

            // 附件
            if (StringUtils.isNotBlank(problemInfo.getAttachment())) {
                String[] files = problemInfo.getAttachment().split("\\|");
                List<Resources> attachmentList = new ArrayList<>();
                for (String file : files) {
                    if (file != null) {
                        attachmentList.add(resourcesMap.get(file));
                    }
                }
                problemInfo.setAttachmentList(attachmentList);
            } else {
                problemInfo.setAttachmentList(Collections.emptyList());
            }

            // 图片
            if (problemInfo.getPic() != null && StringUtils.isNotBlank(problemInfo.getPic())) {
                String[] files = problemInfo.getPic().split("\\|");
                List<Resources> picList = new ArrayList<>();
                for (String file : files) {
                    if (file != null) {
                        picList.add(resourcesMap.get(file));
                    }
                }
                problemInfo.setPicList(picList);
            } else {
                problemInfo.setPicList(Collections.emptyList());
            }
        }

        Map<String, Object> returnMap = new HashMap<>();
        returnMap.put("total", problemPage.getTotal());
        returnMap.put("list", problemList);
        return R.success(returnMap);
    }

    public String formatChoiceAnswer(String answer) {
        if (StringUtils.isBlank(answer)) {
            return "";
        }
        String[] str = answer.split(",");
        StringBuilder newAnswer = new StringBuilder();
        for (String s : str) {
            newAnswer.append((char) ('A' + Integer.valueOf(s)));
        }
        return newAnswer.toString();
    }

    public R delete(Integer[] ids) {
        List<Integer> problemIdList = Arrays.asList(ids);
        // 删除试题
        problemService.removeByIds(problemIdList);
        // 删除选项
        problemOptionService.removeByProblemIds(problemIdList);
        return R.success();
    }

    /*
    答案解析：2,3,[4],3 解析成2,3,,,,,3([4]表示有连续4个空答案)
     */
    public String answerAnalysis(String answer) {
        if (StringUtils.isBlank(answer)) {
            return answer;
        }
        String[] ans = answer.split("\\|");
        StringBuilder builder = new StringBuilder();
        for (String str : ans) {
            if (builder.length() > 0) {
                builder.append('|');
            }
            builder.append(doAnswerAnalysis(str));
        }
        return builder.toString();
    }

    private String doAnswerAnalysis(String answer) {
        if (StringUtils.isBlank(answer)) {
            return answer;
        }
        StringBuilder builder = new StringBuilder();
        int len = answer.length();
        for (int i = 0; i < len; i++) {
            char c1 = answer.charAt(i);
            StringBuilder str = new StringBuilder();
            if (c1 == '[') {
                int j = i + 1;
                while (j < len) {
                    char c2 = answer.charAt(j);
                    if (c2 == ']') {
                        break;
                    }
                    str.append(c2);
                    j++;
                }
                if (str.length() > 0) {
                    if (!StringUtils.isNumeric(str.toString())) {
                        throw new CustomException(CodeEnum.PARAM_ERROR.getCode(), "答案格式错误:[]内出现非数字");
                    }
                    // 如果[]里有数字
                    int num = Integer.valueOf(str.toString());
                    for (int k = 0; k < num - 1; k++) {
                        builder.append(',');
                    }
                }
                i = j;
            } else {
                builder.append(c1);
            }
        }
        return builder.toString();
    }

    /*
    答案还原：2,3,,,,,3 解析成2,3,[4],3([4]表示有连续4个空答案)
     */
    public String answerRestore(String answer) {
        if (StringUtils.isBlank(answer)) {
            return answer;
        }
        String[] ans = answer.split("\\|");
        StringBuilder builder = new StringBuilder();
        for (String str : ans) {
            if (builder.length() > 0) {
                builder.append('|');
            }
            builder.append(doAnswerRestore(str));
        }
        return builder.toString();
    }

    private String doAnswerRestore(String answer) {
        if (StringUtils.isBlank(answer)) {
            return answer;
        }
        // 特判答案就一个逗号的情况
        if (",".equals(answer)) {
            return "[2]";
        }

        StringBuilder builder = new StringBuilder();
        int len = answer.length();
        // 判断 ,2,4,5 的情况
        if (answer.charAt(0) == ',' && (len > 1 && answer.charAt(1) != ',')) {
            builder.append("[1]");
        }

        for (int i = 0; i < len; ) {
            char c1 = answer.charAt(i);
            if (i < len - 1) {
                char c2 = answer.charAt(i + 1);
                if (c1 == ',' && c2 == ',') {
                    // 出现连续的逗号
                    int num = 2, j;
                    for (j = i + 2; (j < len) && (answer.charAt(j) == ','); j++) {
                        num++;
                    }
                    j--;
                    // 判断i和j
                    if (i == 0 && j >= len - 1) {
                        // 全是连续逗号
                        builder.append('[').append(num + 1).append(']');
                    } else if (i == 0 && j < len - 1) {
                        // 连续逗号开头
                        builder.append('[').append(num).append(']').append(',');
                    } else if (i > 0 && j >= len - 1) {
                        // 连续逗号结束
                        builder.append(',').append('[').append(num).append(']');
                    } else {
                        // 连续逗号出现在中间
                        builder.append(',').append('[').append(num - 1).append(']').append(',');
                    }
                    i = j + 1;
                } else {
                    builder.append(c1);
                    i++;
                }
            } else {
                builder.append(c1);
                i++;
            }
        }

        // 判断 2,4,5, 的情况
        if (answer.charAt(len - 1) == ',' && (len > 1 && answer.charAt(len - 2) != ',')) {
            builder.append("[1]");
        }
        return builder.toString();
    }
}
