package com.xiyou.main.biz.exam;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.xiyou.common.enums.CodeEnum;
import com.xiyou.common.utils.R;
import com.xiyou.main.entity.exam.ProblemGroup;
import com.xiyou.main.params.exam.GroupParam;
import com.xiyou.main.service.exam.ProblemGroupService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @program: multi-module
 * @description:
 * @author: tangcan
 * @create: 2019-07-08 16:28
 **/
@Service
public class ProblemGroupBiz {
    @Autowired
    private ProblemGroupService problemGroupService;

    public R listByAdmin(Integer fatherId) {
        List<ProblemGroup> groupList = problemGroupService.getList(fatherId);
        Map<String, Object> returnMap = new HashMap<>();
        returnMap.put("list", groupList);
        return R.success(returnMap);
    }

    public R listByTeacher(Integer userId, Integer fatherId) {
        Map<String, Object> returnMap = new HashMap<>();
        List<ProblemGroup> groupList;
        if (fatherId == null) {
            // 获取一级分组
            groupList = problemGroupService.getGroupListByUserId(userId);
        } else {
            groupList = problemGroupService.getSubGroupList(userId, fatherId, null);
        }
        returnMap.put("list", groupList);
        return R.success(returnMap);
    }

    public R allListByTeacher(Integer userId) {
        List<ProblemGroup> groupList = problemGroupService.getAllList(userId);
        Map<String, Object> returnMap = new HashMap<>();
        returnMap.put("list", groupList);
        return R.success(returnMap);
    }

    public R allListByAdmin() {
        List<ProblemGroup> groupList = problemGroupService.getAllList(null);
        Map<String, Object> returnMap = new HashMap<>();
        returnMap.put("list", groupList);
        return R.success(returnMap);
    }

    public R add(GroupParam groupParam) {
        if (groupParam.getFatherId() == null) {
            groupParam.setFatherId(0);
        }
        ProblemGroup problemGroup = new ProblemGroup()
                .setFatherId(groupParam.getFatherId())
                .setGroupName(groupParam.getGroupName());
        QueryWrapper<ProblemGroup> wrapper = new QueryWrapper<>();
        wrapper.eq("father_id", groupParam.getFatherId()).eq("group_name", groupParam.getGroupName());
        if (problemGroupService.getOne(wrapper) != null) {
            return R.error(CodeEnum.PARAM_ERROR.getCode(), "分组已存在");
        }
        problemGroupService.insert(problemGroup);

        ProblemGroup group = problemGroupService.getOne(wrapper);
        Map<String, Object> returnMap = new HashMap<>();
        returnMap.put("id", group.getId());
        return R.success(returnMap);
    }

    public R update(GroupParam groupParam) {
        ProblemGroup group = new ProblemGroup().setId(groupParam.getId())
                .setGroupName(groupParam.getGroupName());
        if (problemGroupService.checkNameExist(group)) {
            return R.error(CodeEnum.PARAM_ERROR.getCode(), "分组已存在");
        }
        problemGroupService.updateById(group);
        return R.success();
    }

    public R delete(Integer[] ids) {
        List<Integer> idList = Arrays.asList(ids);
        problemGroupService.removeByIds(idList);
        // 删除二级分组
        problemGroupService.deleteSubGroup(idList);
        return R.success();
    }
}
