package com.xiyou.main.biz.exam;

import com.xiyou.common.utils.R;
import com.xiyou.main.service.exam.ProblemTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

/**
 * @program: multi-module
 * @description:
 * @author: tangcan
 * @create: 2019-07-08 17:59
 **/
@Service
public class ProblemTypeBiz {
    @Autowired
    private ProblemTypeService problemTypeService;

    public R list() {
        Map<String, Object> returnMap = new HashMap<>();
        returnMap.put("list", problemTypeService.getList());
        return R.success(returnMap);
    }
}
