package com.xiyou.main.biz.exam;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.xiyou.common.enums.CodeEnum;
import com.xiyou.common.office.utils.FileUploadUtil;
import com.xiyou.common.office.utils.FileUtil;
import com.xiyou.common.utils.CommonUtil;
import com.xiyou.common.utils.R;
import com.xiyou.main.entity.exam.Resources;
import com.xiyou.main.params.exam.ResourcesParam;
import com.xiyou.main.service.exam.ResourcesService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.time.LocalTime;
import java.util.*;

/**
 * @program: multi-module
 * @description:
 * @author: tangcan
 * @create: 2019-07-10 19:57
 **/
@Service
public class ResourcesBiz {
    private Logger log = LoggerFactory.getLogger(this.getClass());
    @Autowired
    Environment environment;
    @Autowired
    private ResourcesService resourcesService;

    public R addAttachment(Integer userId, MultipartFile[] files, String intro) {
        if (files == null || files.length == 0) {
            return R.error(CodeEnum.FILE_UPLOAD_FAIL.getCode(), "请选择文件");
        }
        List<Resources> resourcesList = new ArrayList<>(CommonUtil.getListInitCap(files.length));
        StringBuilder builder = new StringBuilder();
        for (MultipartFile file : files) {
            String originalName = file.getOriginalFilename();
            // 自定义文件名
            String fileName = "FJ-" + getUniqueFileName();
            String realFileName = FileUtil.upload(file, CommonUtil.getCatalinaHome() + environment.getProperty("teaching-platform.resources-attachment"), fileName);
            if (builder.length() == 0) {
                builder.append(realFileName);
            } else {
                builder.append('|').append(realFileName);
            }
            resourcesList.add(new Resources().setOriginalName(originalName)
                    .setFileName(realFileName)
                    .setUploadUserId(userId)
                    .setType(0)
                    .setIntro(intro));
        }
        resourcesService.insertBatch(resourcesList);
        Map<String, Object> returnMap = new HashMap<>();
        returnMap.put("files", builder.toString());
        return R.success(returnMap);
    }

    private String getUniqueFileName() {
        return UUID.randomUUID().toString().replaceAll("-", "").substring(0, 10)
                + LocalTime.now().toString()
                .replaceAll(":", "")
                .replaceAll("\\.", "");
    }

    public R addPic(Integer userId, MultipartFile[] files, String intro) {
        if (files == null || files.length == 0) {
            return R.error(CodeEnum.FILE_UPLOAD_FAIL.getCode(), "请选择图片");
        }
        List<Resources> resourcesList = new ArrayList<>(CommonUtil.getListInitCap(files.length));
        StringBuilder builder = new StringBuilder();
        for (MultipartFile file : files) {
            String originalName = file.getOriginalFilename();
            // 自定义文件名
            String fileName = "TP-" + getUniqueFileName();
            String realFileName = FileUtil.upload(file, CommonUtil.getCatalinaHome() + environment.getProperty("teaching-platform.resources-pic"), fileName);
            if (builder.length() == 0) {
                builder.append(realFileName);
            } else {
                builder.append('|').append(realFileName);
            }
            resourcesList.add(new Resources().setOriginalName(originalName)
                    .setFileName(realFileName)
                    .setUploadUserId(userId)
                    .setType(1)
                    .setIntro(intro));
        }
        resourcesService.insertBatch(resourcesList);
        Map<String, Object> returnMap = new HashMap<>();
        returnMap.put("files", builder.toString());
        return R.success(returnMap);
    }

    public R attachmentList(ResourcesParam resourcesParam) {
        resourcesParam.setType(0);
        Page<Resources> resourcesPage = resourcesService.getPage(resourcesParam);
        Map<String, Object> returnMap = new HashMap<>();
        returnMap.put("list", resourcesPage.getRecords());
        returnMap.put("total", resourcesPage.getTotal());
        return R.success(returnMap);
    }

    public R picList(ResourcesParam resourcesParam) {
        resourcesParam.setType(1);
        Page<Resources> resourcesPage = resourcesService.getPage(resourcesParam);
        Map<String, Object> returnMap = new HashMap<>();
        returnMap.put("list", resourcesPage.getRecords());
        returnMap.put("total", resourcesPage.getTotal());
        return R.success(returnMap);
    }

    public void downloadAttachment(HttpServletResponse response, String attachment, String originalName) {
        try {
            FileUtil.exportFile(response, CommonUtil.getCatalinaHome() + environment.getProperty("teaching-platform.resources-attachment") + attachment, originalName);
        } catch (IOException e) {
            log.info(e.getMessage());
        }
    }

    public void downloadPic(HttpServletResponse response, String pic) {
        try {
            FileUtil.exportPic(response, CommonUtil.getCatalinaHome() + environment.getProperty("teaching-platform.resources-pic") + pic);
        } catch (IOException e) {
            log.info(e.getMessage());
        }
    }
}
