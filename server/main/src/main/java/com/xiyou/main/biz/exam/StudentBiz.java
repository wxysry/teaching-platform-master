package com.xiyou.main.biz.exam;

import cn.afterturn.easypoi.excel.entity.result.ExcelImportResult;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.xiyou.common.enums.CodeEnum;
import com.xiyou.common.redis.utils.RedisCache;
import com.xiyou.common.redis.utils.RedisKey;
import com.xiyou.common.shiro.jwt.JWTToken;
import com.xiyou.common.shiro.jwt.JWTUtil;
import com.xiyou.common.shiro.service.JWTTokenService;
import com.xiyou.common.utils.CommonUtil;
import com.xiyou.common.office.utils.EasyPOIUtil;
import com.xiyou.common.shiro.utils.MD5Util;
import com.xiyou.common.utils.R;
import com.xiyou.main.constants.RoleConstant;
import com.xiyou.main.entity.exam.SysUser;
import com.xiyou.main.entity.exam.TeachClass;
import com.xiyou.main.params.exam.UserParam;
import com.xiyou.main.pojo.exam.ImportStudent;
import com.xiyou.main.service.exam.SysRoleService;
import com.xiyou.main.service.exam.SysUserService;
import com.xiyou.main.service.exam.TeachClassService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.util.*;
import java.util.stream.Collectors;

/**
 * @program: multi-module
 * @description: 业务逻辑层
 * @author: tangcan
 * @create: 2019-06-25 12:28
 **/
@Service
@Transactional
public class StudentBiz {
    @Autowired
    private SysUserService sysUserService;
    @Autowired
    private SysRoleService sysRoleService;
    @Autowired
    private SysUserBiz sysUserBiz;
    @Autowired
    private TeachClassService teachClassService;
    @Autowired
    private Environment environment;

    public R add(Integer teacherId, SysUser student) {
        // 检查剩余可开设学生账号数
        if (getRestMemberNum(teacherId) <= 0) {
            return R.error(CodeEnum.OTHER_ERROR.getCode(), "可开设学生账号数已满");
        }

        // 检查邮箱和手机号是否被占用
        sysUserBiz.checkUserInsert(student);

        // 设置创建人
        student.setCreateUserId(teacherId);

        // 设置用户角色为学生
        student.setRoleId(sysRoleService.get(RoleConstant.STUDENT).getId());

        student.setMemberNum(0);

        // 存库
        if (sysUserService.insert(student)) {
            return R.success();
        }
        return R.error(CodeEnum.OTHER_ERROR.getCode(), "账号已存在");
    }


    @Autowired
    private JWTTokenService jwtTokenService;
    @Autowired
    private RedisCache redisCache;

    public R update(SysUser student) {
        // 检查邮箱和手机号
        sysUserBiz.checkUserUpdate(student);
        // 学校不能修改
        student.setSchoolName(null);
        if (sysUserService.updateById(student)) {
            //判断是否输入了密码
            if(StringUtils.isNotEmpty(student.getPassword())){
                //修改了密码的情况下，所有人都需要重新登录

                Integer userId = student.getId();
                Set<Object> tokenSet = redisCache.sGet(RedisKey.get("user_token", String.valueOf(userId)));
                for (Object o : tokenSet) {
                    JWTToken jwtToken = jwtTokenService.get(o.toString());
                    jwtTokenService.remove(jwtToken);
                }
                redisCache.delete(RedisKey.get("user_token", String.valueOf(userId)));
            }
            return R.success();
        }
        return R.error(CodeEnum.OTHER_ERROR);
    }

    public R upload(Integer teacherId, MultipartFile file) {
        // 获取导入的数据并校验
        ExcelImportResult<ImportStudent> result;
        // 表格标题的行数
        int titleRows = 1;
        // 表格头的行数
        int headRows = 1;
        try {
            result = EasyPOIUtil.importAndVerfiy(file, titleRows, headRows, ImportStudent.class);
        } catch (Exception e) {
            e.printStackTrace();
            return R.error(CodeEnum.FILE_UPLOAD_FAIL.getCode(), "上传失败：" + e.getMessage());
        }
        // 判断剩余可开设的学生账号数
        int correct = result.getList().size();
        int restMemberNum = getRestMemberNum(teacherId);
        // 判断是否有校验不成功的数据
        if (result.isVerfiyFail()) {
            // 返回校验错误信息
            Map<String, Object> errorMap = new HashMap<>();
            errorMap.put("error", EasyPOIUtil.getFailMsgList(result.getFailList(), titleRows, headRows));
            return R.error(CodeEnum.FILE_UPLOAD_FAIL, errorMap);
        }
        if (correct > restMemberNum) {
            return R.error(CodeEnum.FILE_UPLOAD_FAIL.getCode(),
                    "上传失败：本次上传 " + correct + " 个学生账号" +
                            ",但是你最多还能开设 " + restMemberNum + " 个学生账号");
        }
        // 2019.10.24新增：每个教师的学生都不一样，因此账号不能重复
        List<String> accountList = result.getList().stream().map(ImportStudent::getAccount).collect(Collectors.toList());
        List<SysUser> repeatList = sysUserService.getListByAccountList(accountList);
        if (!repeatList.isEmpty()) {
            List<String> errorAccounts = repeatList.stream().map(SysUser::getAccount).collect(Collectors.toList());
            return R.error(CodeEnum.OTHER_ERROR, "上传失败! 账号：" + errorAccounts + " 已存在！");
        }
//        // 取出所有email存入Map
//        // Map<K,V> K为 V为rowNum 方便提示错误
//        Map<String, Integer> emailMap = new HashMap<>(CommonUtil.getMapInitCap(correct));
//        // Map<K,V> K为email V为account 可能某用户在数据库中，因此邮箱使用过也正常，所以要判断
//        Map<String, String> accountMap = new HashMap<>(CommonUtil.getMapInitCap(correct));
//        for (ImportStudent student : result.getList()) {
//            emailMap.put(student.getEmail(), student.getRowNum());
//            accountMap.put(student.getEmail(), student.getAccount());
//        }


//        // 校验上传的学生邮箱是否重复
//        if (emailMap.size() < correct) {
//            return R.error(CodeEnum.FILE_UPLOAD_FAIL.getCode(), "学生邮箱不能重复");
//        }

//        // 查出是否存在已注册使用的邮箱
//        List<SysUser> sysUserList = sysUserService.getByEmailList(new ArrayList<>(emailMap.keySet()));
//        if (sysUserList.size() > 0) {
//            // 将邮箱被注册的账号所在行记录下来
//            StringBuilder builder = new StringBuilder();
//            for (SysUser sysUser : sysUserList) {
//                Integer rowNum = emailMap.get(sysUser.getEmail());
//                String account = accountMap.get(sysUser.getEmail());
//                // 如果是自己的邮箱，那么就不是被别人注册
//                if (sysUser.getAccount().equals(account)) {
//                    continue;
//                }
//                if (rowNum != null) {
//                    rowNum += titleRows + headRows;
//                    if (builder.length() == 0) {
//                        builder.append(rowNum);
//                    } else {
//                        builder.append(",").append(rowNum);
//                    }
//                }
//            }
//            if (builder.length() > 0) {
//                Map<String, Object> map = new HashMap<>();
//                map.put("error", "行号为" + builder.toString() + "的邮箱已被注册");
//                return R.error(CodeEnum.FILE_UPLOAD_FAIL, map);
//            }
//        }
        /*
        设置其他字段
         */
        // 角色id
        int roleId = sysRoleService.get(RoleConstant.STUDENT).getId();

        List<SysUser> userList = new ArrayList<>(CommonUtil.getListInitCap(correct));
        for (ImportStudent student : result.getList()) {
            SysUser user = new SysUser();
            user.setRoleId(roleId)
                    .setAccount(student.getAccount())
                    .setPassword(MD5Util.encrypt(student.getAccount(), student.getPassword()))
                    .setName(student.getName())
                    .setEmail(student.getEmail())
                    .setSchoolName(student.getSchoolName())
                    .setCreateUserId(teacherId)
                    .setMemberNum(0);
            userList.add(user);
        }
        // 新用户存库，存在就忽略(不会返回主键id，需要重新查询)
        int success = sysUserService.insertBatch(userList);

        /*
        所有学生加入该教师的教学班
         */
        List<TeachClass> teachClassList = new ArrayList<>(CommonUtil.getListInitCap(correct));
        // 批量插入没有返回id的学生
        List<String> needIdUserAccountList = new ArrayList<>(CommonUtil.getListInitCap(correct - success));
        for (SysUser sysUser : userList) {
            needIdUserAccountList.add(sysUser.getAccount());
        }
        // 如果有已存在的用户
        if (needIdUserAccountList.size() > 0) {
//            // 查出所有id ,email 映射到 id
//            List<SysUser> list = sysUserService.getByEmailList(needIdUserEmailList);

            // 查出所有id ,account 映射到 id
            List<SysUser> list = sysUserService.getByAccountList(needIdUserAccountList);

            for (SysUser sysUser : list) {
                TeachClass teachClass = new TeachClass();
                teachClass.setTeacherId(teacherId).setStudentId(sysUser.getId());
                teachClassList.add(teachClass);
            }
        }
        // 所有学生加入该教师的教学班
        teachClassService.insertBatch(teachClassList);

        // String msg = "总共上传 " + correct + " 个学生账号，其中 " + (correct - success) + " 个账号已存在，本次新增 " + success + " 个账号";
        String msg = "本次新增 " + correct + " 名学生";
        return R.success(msg);
    }

    /**
     * @Author: tangcan
     * @Description: 剩余可开设学生账号数
     * @Param: [teacherId]
     * @date: 2019/6/26
     */
    private int getRestMemberNum(Integer teacherId) {
        // 该教师可开设学生账号数
        SysUser teacher = sysUserService.getById(teacherId);
        int memberNum = teacher == null ? 0 : teacher.getMemberNum();

        // 该教师已开设学生账号数
        int hadOpenMemberNum = teachClassService.count(new TeachClass().setTeacherId(teacherId));

        return Math.max(0, memberNum - hadOpenMemberNum);
    }

    public R list(Integer teacherId, UserParam userParam) {
        userParam.setRoleId(RoleConstant.STUDENT_ROLE_ID);
        userParam.setUserId(teacherId);
        if (userParam.getForbid() == null) {
            userParam.setForbid(0);
        }
        Map<String, Object> returnMap = new HashMap<>();
        Page<SysUser> userPage = sysUserService.getStudentPage(userParam);
        returnMap.put("list", userPage.getRecords());
        returnMap.put("total", userPage.getTotal());
        return R.success(returnMap);
    }

    public R delete(Integer[] ids) {
        sysUserService.removeByIds(Arrays.asList(ids));
        //移除学生权限
        for (Integer id : ids) {
            teachClassService.remove(new LambdaQueryWrapper<TeachClass>()
                    .eq(TeachClass::getStudentId,id)
            );
        }

        return R.success();
    }

    public R listAll(Integer teacherId) {
        Map<String, Object> returnMap = new HashMap<>();
        returnMap.put("list", sysUserService.getStudentList(teacherId));
        return R.success(returnMap);
    }
}
