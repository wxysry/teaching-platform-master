package com.xiyou.main.biz.exam;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.xiyou.common.captcha.service.CaptchaService;
import com.xiyou.common.email.service.MailService;
import com.xiyou.common.enums.CodeEnum;
import com.xiyou.common.exception.CustomException;
import com.xiyou.common.redis.utils.RedisCache;
import com.xiyou.common.redis.utils.RedisKey;
import com.xiyou.common.shiro.jwt.JWTToken;
import com.xiyou.common.shiro.jwt.JWTUtil;
import com.xiyou.common.shiro.service.JWTTokenService;
import com.xiyou.common.shiro.utils.MD5Util;
import com.xiyou.common.utils.R;
import com.xiyou.main.entity.exam.SysUser;
import com.xiyou.main.entity.exam.TeachClass;
import com.xiyou.main.params.exam.LoginParam;
import com.xiyou.main.params.exam.ResetPwdParam;
import com.xiyou.main.service.exam.SysUserService;
import com.xiyou.main.service.exam.TeachClassService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;
import java.util.concurrent.TimeUnit;

/**
 * @program: multi-module
 * @description: 业务逻辑层
 * @author: tangcan
 * @create: 2019-06-24 10:46
 **/
@Service
@Transactional
public class SysUserBiz {
    @Autowired
    private CaptchaService captchaService;
    @Autowired
    private JWTTokenService jwtTokenService;
    @Autowired
    private SysUserService sysUserService;
    @Autowired
    private MailService mailService;
    @Autowired
    private RedisCache redisCache;

    @Autowired
    private TeachClassService teachClassService;

    public R login(LoginParam loginParam) {
        // 验证码校验
        if (!captchaService.checkCode(loginParam.getCaptchaToken(), loginParam.getCode())) {
            throw new CustomException(CodeEnum.CODE_ERROR);
        }

        // 查出用户
        SysUser sysUser = sysUserService.get(new SysUser().setAccount(loginParam.getAccount()));
        // 验证账号
        if (sysUser == null) {
            throw new CustomException(CodeEnum.USER_OR_PWD_ERROR);
        }
        // 验证密码
        if (!MD5Util.verifyPwd(loginParam.getAccount(), loginParam.getPassword(), sysUser.getPassword())) {
            throw new CustomException(CodeEnum.USER_OR_PWD_ERROR);
        }
        // 验证是否被禁用
        if (sysUser.getForbid() == 1) {
            throw new CustomException(CodeEnum.USER_FORBID);
        }

        // 生成token
        JWTToken jwtToken = new JWTToken();
        jwtToken.setToken(JWTUtil.sign(sysUser.getAccount(), sysUser.getId(), sysUser.getPassword()));

        // 设置过期时间
        jwtToken.setActiveTime(new Date(System.currentTimeMillis() + JWTUtil.ACCESS_TIME_SECOND * 1000));

        // 缓存token
        jwtToken.setAccount(sysUser.getAccount())
                .setUserId(sysUser.getId())
                .setName(sysUser.getName());

        jwtTokenService.save(jwtToken);

        // 设置返回字段
        Map<String, Object> returnMap = new HashMap<>();
        // 设置token
        returnMap.put("token", jwtToken.getToken());

        return R.success(returnMap);
    }

    public R getRoles(Integer userId, String account, String name) {
        // 缓存基本信息的key
        String redisKey = RedisKey.get("sys_user_info", "user_id", String.valueOf(userId));
        Map<String, Object> map = redisCache.hmGetMap(redisKey);
        if (map == null) {
            map = new HashMap<>();
            SysUser user = sysUserService.getAllInfo(new SysUser().setId(userId));
            List<String> roles = new ArrayList<>();
            roles.add(user.getRoleCode());
            map.put("roles", roles);
            map.put("school", user.getSchoolName());
            map.put("account", account);
            map.put("name", name);
            map.put("userId", user.getId());
            // 缓存
            redisCache.hmSetMap(redisKey, map);
            // 设置过期时间
            redisCache.setExpire(redisKey, JWTUtil.ACCESS_TIME_SECOND);
        }

        return R.success(map);
    }

    public R getAllInfo(Integer userId) {
        Map<String, Object> returnMap = new HashMap<>();
        returnMap.put("info", sysUserService.getAllInfo(new SysUser().setId(userId)));
        return R.success(returnMap);
    }

    public R delete(Integer userId) {
        if (sysUserService.removeById(userId)) {
            //移除学生权限
            teachClassService.remove(new LambdaQueryWrapper<TeachClass>()
                    .eq(TeachClass::getStudentId,userId)
            );
            return R.success();
        }
        return R.error(CodeEnum.OTHER_ERROR);
    }

    public R forbid(Integer userId, Integer forbid) {
        if (sysUserService.setForbid(userId, forbid)) {
            return R.success();
        }
        return R.error(CodeEnum.OTHER_ERROR);
    }

    public R resetPassword(ResetPwdParam param) {
        SysUser sysUser = sysUserService.get(new SysUser()
                .setAccount(param.getAccount())
                .setEmail(param.getEmail()));
        if (sysUser == null) {
            return R.error(CodeEnum.PARAM_ERROR.getCode(), "账号或邮箱错误");
        }
        if (!mailService.checkCode(param.getEmail(), param.getCode())) {
            return R.error(CodeEnum.CODE_ERROR);
        }
        sysUser.setPassword(MD5Util.encrypt(param.getAccount(), param.getPassword()));
        sysUserService.updatePwd(sysUser.getId(), sysUser.getPassword());
        return null;
    }

    /**
     * @Author: tangcan
     * @Description: 检查添加用户时的字段
     * @Param: [sysUser]
     * @date: 2019/6/26
     */
    public void checkUserInsert(SysUser sysUser) {
        // 加密密码
        sysUser.setPassword(MD5Util.encrypt(sysUser.getAccount(), sysUser.getPassword()));

        // 判断邮箱是否已用
        if (sysUserService.get(new SysUser().setEmail(sysUser.getEmail())) != null) {
            throw new CustomException(CodeEnum.OTHER_ERROR.getCode(), "该邮箱已被使用");
        }

        // 判断手机号是否已用
        if (StringUtils.isBlank(sysUser.getPhone())) {
            sysUser.setPhone(null);
        }
    }

    /**
     * @Author: tangcan
     * @Description: 检查更新用户时的字段
     * @Param: [sysUser]
     * @date: 2019/6/26
     */
    public void checkUserUpdate(SysUser sysUser) {

        if (StringUtils.isNotBlank(sysUser.getPassword())) {
            sysUser.setPassword(MD5Util.encrypt(sysUser.getAccount(), sysUser.getPassword()));
        }else{
            sysUser.setPassword(null);
        }

        // 账号、密码、角色不更新
        sysUser.setAccount(null)
                .setRoleId(null);

        // 防止更新不必要的字段
        sysUser.setCreateUserId(null)
                .setForbid(null);
        //如果邮箱不为空，还需要判断邮箱是否被别人使用避免重复
        if(StringUtils.isNotEmpty(sysUser.getEmail())){
            SysUser user = sysUserService.get(new SysUser().setEmail(sysUser.getEmail()));
            if (user != null && !sysUser.getId().equals(user.getId())) {
                throw new CustomException(CodeEnum.OTHER_ERROR.getCode(), "该邮箱已被使用");
            }
        }
    }

    public R update(Integer userId, SysUser user) {
        if (user.getId() == null) {
            user.setId(userId);
        }
        // 检查邮箱和手机号
        this.checkUserUpdate(user);
        // 更新用户信息
        if (sysUserService.updateById(user)) {
            return R.success();
        }
        return R.error(CodeEnum.OTHER_ERROR);
    }
}
