package com.xiyou.main.biz.exam;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.xiyou.common.enums.CodeEnum;
import com.xiyou.common.utils.R;
import com.xiyou.main.async.exam.UserGroupAsyncService;
import com.xiyou.main.constants.RoleConstant;
import com.xiyou.main.entity.contest.UserContestGroup;
import com.xiyou.main.entity.exam.*;
import com.xiyou.main.entity.gbcontest.UserGbContestGroup;
import com.xiyou.main.entity.newcontest.UserNewContestGroup;
import com.xiyou.main.entity.ngbcontest.UserNGbContestGroup;
import com.xiyou.main.params.exam.GroupUpdateParam;
import com.xiyou.main.params.exam.UserParam;
import com.xiyou.main.service.exam.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.stream.Collectors;

/**
 * @program: multi-module
 * @description: 业务逻辑层
 * @author: tangcan
 * @create: 2019-06-25 12:28
 **/
@Service
@Transactional
public class TeacherBiz {
    @Autowired
    private SysRoleService sysRoleService;
    @Autowired
    private SysUserService sysUserService;
    @Autowired
    private UserProblemGroupService userProblemGroupService;
    @Autowired
    private SysUserBiz sysUserBiz;
    @Autowired
    private UserPptGroupService userPptGroupService;
    @Autowired
    private UserVideoGroupService userVideoGroupService;
    @Autowired
    private UserGroupAsyncService userGroupAsyncService;
    @Autowired
    private UserContestGroupService userContestGroupService;

    @Autowired
    private UserNewContestGroupService userNewContestGroupService;

    @Autowired
    private UserGbContestGroupService userGbContestGroupService;

    @Autowired
    private UserNGbContestGroupService userNGbContestGroupService;

    public R add(Integer adminId, SysUser sysUser) {

        // 检验邮箱和手机号是否被占用
        sysUserBiz.checkUserInsert(sysUser);

        // 设置创建人
        sysUser.setCreateUserId(adminId);

        // 设置用户角色为教师
        sysUser.setRoleId(sysRoleService.get(RoleConstant.TEACHER).getId());

        if (sysUser.getMemberNum() == null) {
            sysUser.setMemberNum(0);
        }
        if (sysUser.getContestNum() == null) {
            sysUser.setContestNum(1);
        }

        // 保存用户
        if (sysUserService.insert(sysUser)) {
            // // 保存可查看的考题分组
            // userProblemGroupService.save(sysUser.getId(), sysUser.getProblemGroups());
            //
            // // 保存可查看的视频教程的分组
            // userVideoGroupService.save(sysUser.getId(), sysUser.getVideoGroups());
            // userPptGroupService.save(sysUser.getId(), sysUser.getPptGroups());
            return R.success();
        }
        return R.error(CodeEnum.OTHER_ERROR.getCode(), "账号已存在");
    }


    public R update(Integer userId, SysUser teacher) {
        // 检查邮箱和手机号
        sysUserBiz.checkUserUpdate(teacher);
        // 是否是自己修改
        if (teacher.getId().equals(userId)) {
            // 学校不能修改
            teacher.setSchoolName(null);
            // 人数不能修改
            teacher.setMemberNum(null);
        }
        // 更新用户信息
        if (sysUserService.updateById(teacher)) {
            return R.success();
        }
        return R.error(CodeEnum.OTHER_ERROR);
    }

    public R list(UserParam userParam) {
        userParam.setRoleId(RoleConstant.TEACHER_ROLE_ID);
        Map<String, Object> returnMap = new HashMap<>();
        Page<SysUser> userPage = sysUserService.getTeacherPage(userParam);
        List<SysUser> userList = userPage.getRecords();
        List<Integer> userIdList = userList.stream().map(SysUser::getId).collect(Collectors.toList());
        // 异步请求，并处理数据
        Future<Map<Integer, List<UserProblemGroup>>> problem = userGroupAsyncService.getProblemGroupByUserIdList(userIdList);
        Future<Map<Integer, List<UserVideoGroup>>> video = userGroupAsyncService.getVideoGroupByUserIdList(userIdList);
        Future<Map<Integer, List<UserPptGroup>>> ppt = userGroupAsyncService.getPptGroupByUserIdList(userIdList);
        Future<Map<Integer, List<UserContestGroup>>> contest = userGroupAsyncService.getContestGroupByUserIdList(userIdList);
        //新平台模拟
        Future<Map<Integer, List<UserNewContestGroup>>> newContest = userGroupAsyncService.getNewContestGroupByUserIdList(userIdList);
        //组件对抗
        Future<Map<Integer, List<UserGbContestGroup>>> gbContest = userGroupAsyncService.getGbContestGroupByUserIdList(userIdList);        //组件对抗
        //组件对抗(新)
        Future<Map<Integer, List<UserNGbContestGroup>>> ngbContest = userGroupAsyncService.getNGbContestGroupByUserIdList(userIdList);

        while (true) {
            // 所有异步人物完成才进行下面的封装操作
            if (problem.isDone() && video.isDone() && ppt.isDone() && contest.isDone()&& newContest.isDone()&& gbContest.isDone() && ngbContest.isDone()) {
                break;
            }
        }
        // 设置各分组信息
        Map<Integer, List<UserProblemGroup>> problemMap;
        Map<Integer, List<UserVideoGroup>> videoMap;
        Map<Integer, List<UserPptGroup>> pptMap;
        Map<Integer, List<UserContestGroup>> contestMap;
        Map<Integer, List<UserNewContestGroup>> newContestMap;
        Map<Integer, List<UserGbContestGroup>> gbContestMap;
        Map<Integer, List<UserNGbContestGroup>> ngbContestMap;
        try {
            problemMap = problem.get();
            videoMap = video.get();
            pptMap = ppt.get();
            contestMap = contest.get();
            newContestMap = newContest.get();
            gbContestMap = gbContest.get();
            ngbContestMap = ngbContest.get();
        } catch (InterruptedException | ExecutionException e) {
            return R.error(CodeEnum.OTHER_ERROR);
        }
        for (SysUser user : userList) {
            // 试题
            List<UserProblemGroup> problemGroupList = problemMap.get(user.getId());
            if (problemGroupList == null) {
                problemGroupList = new ArrayList<>();
            }
            user.setProblemGroups(problemGroupList
                    .stream()
                    .map(UserProblemGroup::getGroupId).collect(Collectors.toList()));
            // 视频
            List<UserVideoGroup> videoGroupList = videoMap.get(user.getId());
            if (videoGroupList == null) {
                videoGroupList = new ArrayList<>();
            }
            user.setVideoGroups(videoGroupList
                    .stream()
                    .map(UserVideoGroup::getGroupId).collect(Collectors.toList()));

            // ppt
            List<UserPptGroup> pptGroupList = pptMap.get(user.getId());
            if (pptGroupList == null) {
                pptGroupList = new ArrayList<>();
            }
            user.setPptGroups(pptGroupList
                    .stream()
                    .map(UserPptGroup::getGroupId).collect(Collectors.toList()));

            // 竞赛
            List<UserContestGroup> contestGroupList = contestMap.get(user.getId());
            if (contestGroupList == null) {
                contestGroupList = new ArrayList<>();
            }
            user.setContestGroups(contestGroupList
                    .stream()
                    .map(UserContestGroup::getGroupId).collect(Collectors.toList()));

            // 新平台模拟
            List<UserNewContestGroup> contestNewGroupList = newContestMap.get(user.getId());
            if (contestNewGroupList == null) {
                contestNewGroupList = new ArrayList<>();
            }
            user.setNewContestGroups(contestNewGroupList
                    .stream()
                    .map(UserNewContestGroup::getGroupId).collect(Collectors.toList()));

            // 组间对抗
            List<UserGbContestGroup> contestGbGroupList = gbContestMap.get(user.getId());
            if (contestGbGroupList == null) {
                contestGbGroupList = new ArrayList<>();
            }
            user.setGbContestGroups(contestGbGroupList
                    .stream()
                    .map(UserGbContestGroup::getGroupId).collect(Collectors.toList()));

            // 组间对抗(新)
            List<UserNGbContestGroup> contestNGbGroupList = ngbContestMap.get(user.getId());
            if (contestNGbGroupList == null) {
                contestNGbGroupList = new ArrayList<>();
            }
            user.setNgbContestGroups(contestNGbGroupList
                    .stream()
                    .map(UserNGbContestGroup::getGroupId).collect(Collectors.toList()));
        }
        returnMap.put("list", userList);
        returnMap.put("total", userPage.getTotal());
        return R.success(returnMap);
    }

    public R updateGroup(GroupUpdateParam groupUpdateParam) {
        if (groupUpdateParam.getType() == 0) {
            // 题库分组
            userProblemGroupService.save(groupUpdateParam.getUserId(), groupUpdateParam.getGroups());
        } else if (groupUpdateParam.getType() == 1) {
            // 视频
            userVideoGroupService.save(groupUpdateParam.getUserId(), groupUpdateParam.getGroups());
        } else if (groupUpdateParam.getType() == 2) {
            // PPT
            userPptGroupService.save(groupUpdateParam.getUserId(), groupUpdateParam.getGroups());
        } else if (groupUpdateParam.getType() == 3) {
            // 竞赛模拟
            userContestGroupService.save(groupUpdateParam.getUserId(), groupUpdateParam.getGroups());
        }else if (groupUpdateParam.getType() == 4) {
            // 新平台模拟
            userNewContestGroupService.save(groupUpdateParam.getUserId(), groupUpdateParam.getGroups());
        }else if (groupUpdateParam.getType() == 5) {
            // 组间对抗
            userGbContestGroupService.save(groupUpdateParam.getUserId(), groupUpdateParam.getGroups());
        }
        else if (groupUpdateParam.getType() == 6) {
            // 组间对抗(新)
            userNGbContestGroupService.save(groupUpdateParam.getUserId(), groupUpdateParam.getGroups());
        }
        return R.success();
    }
}
