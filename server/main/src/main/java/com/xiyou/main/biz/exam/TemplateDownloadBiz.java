package com.xiyou.main.biz.exam;

import com.xiyou.common.office.utils.EasyPOIUtil;
import com.xiyou.common.office.utils.FileUtil;
import com.xiyou.common.office.utils.WordUtil;
import com.xiyou.common.utils.CommonUtil;
import com.xiyou.main.entity.exam.ProblemGroup;
import com.xiyou.main.entity.exam.ProblemType;
import com.xiyou.main.service.exam.ProblemGroupService;
import com.xiyou.main.service.exam.ProblemTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @program: multi-module
 * @description:
 * @author: tangcan
 * @create: 2019-07-09 14:23
 **/
@Service
public class TemplateDownloadBiz {
    @Autowired
    private ProblemTypeService problemTypeService;
    @Autowired
    private ProblemGroupService problemGroupService;

    public void problem(HttpServletResponse response) {
        // 模板说明字段填充
        List<ProblemType> typeList = problemTypeService.list();
        List<ProblemGroup> problemGroupList = problemGroupService.getAllList(null);
        // 题型
        StringBuilder typeBuilder = new StringBuilder();
        typeList.forEach(p -> {
            if (typeBuilder.length() > 0) {
                typeBuilder.append(',');
            }
            typeBuilder.append(p.getTypeName()).append('[').append(p.getTypeCode()).append(']');
        });

        // 分组
        StringBuilder groupBuilder = new StringBuilder();
        problemGroupList.forEach(p -> {
            groupBuilder.append(p.getGroupName()).append('[');
            if (p.getSubgroups() != null) {
                int i = 0;
                for (ProblemGroup s : p.getSubgroups()) {
                    if (i > 0) {
                        groupBuilder.append(',');
                    }
                    groupBuilder.append(s.getGroupName());
                    i = 1;
                }
            }
            groupBuilder.append(']').append("\r\n");
        });

        Map<String, Object> paramMap = new HashMap<>();
        paramMap.put("typeInfo", typeBuilder.toString());
        paramMap.put("groupInfo", groupBuilder.toString());

        String classesPath = CommonUtil.getClassesPath();
        String fileName = "import_problem_explain.docx";
        String templateFilePath = classesPath + "templates/" + fileName;
        String wordPath = classesPath + "temp/";

        String savePath = WordUtil.exportWordTemplateToFile(templateFilePath, wordPath, fileName, paramMap);

        // 文件和对应的命名
        List<File> fileList = new ArrayList<>();
        List<String> fileNameList = new ArrayList<>();
        fileList.add(new File(classesPath + "templates/import_problem.xlsx"));
        fileNameList.add("试题导入模板.xlsx");
        fileList.add(new File(savePath));
        fileNameList.add("试题导入说明.docx");

        try {
            FileUtil.toZipAndDownload(response, fileList, fileNameList, "试题导入说明及模板.zip");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void student(HttpServletResponse response) {
        String path = CommonUtil.getClassesPath() + "templates/import_student.xlsx";
        try {
            EasyPOIUtil.exportExcelByPath(response, path, "学生账号开设表.xlsx");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void gzExcel(HttpServletResponse response) {
        String path = CommonUtil.getClassesPath() + "templates/gz.xlsx";
        try {
            EasyPOIUtil.exportExcelByPath(response, path, "规则表.xlsx");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    /**
     * 下载新平台模拟规则模板
     * @param response
     */
    public void gzNewExcel(HttpServletResponse response) {
        String path = CommonUtil.getClassesPath() + "templates/newGz.xlsx";
        try {
            EasyPOIUtil.exportExcelByPath(response, path, "新平台模拟规则表.xlsx");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    /**
     * 下载组件对抗规则模板
     * @param response
     */
    public void gzGbExcel(HttpServletResponse response) {
        String path = CommonUtil.getClassesPath() + "templates/gbGz.xlsx";
        try {
            EasyPOIUtil.exportExcelByPath(response, path, "组间对抗规则表.xlsx");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * 下载组件对抗(新)规则模板
     * @param response
     */
    public void gzNGbExcel(HttpServletResponse response) {
        String path = CommonUtil.getClassesPath() + "templates/ngbGz.xlsx";
        try {
            EasyPOIUtil.exportExcelByPath(response, path, "组间对抗规则表.xlsx");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
