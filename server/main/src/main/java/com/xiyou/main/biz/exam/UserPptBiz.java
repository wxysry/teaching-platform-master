package com.xiyou.main.biz.exam;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.xiyou.common.utils.CommonUtil;
import com.xiyou.common.utils.R;
import com.xiyou.main.entity.exam.*;
import com.xiyou.main.params.exam.CourseQueryParam;
import com.xiyou.main.service.exam.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @program: multi-module
 * @description:
 * @author: tangcan
 * @create: 2019-10-28 16:41
 **/
@Service
public class UserPptBiz {
    @Autowired
    private PptCourseService courseService;
    @Autowired
    Environment environment;
    @Autowired
    private PptGroupService groupService;
    @Autowired
    private UserPptService userPptService;

    public R group(Integer studentId) {
        List<PptGroup> list = groupService.listByStudent(studentId);
        Map<String, Object> resultMap = new HashMap<>(16);
        resultMap.put("list", list);
        return R.success(resultMap);
    }

    public R list(CourseQueryParam param) {
        Page<PptCourse> pptCoursePage = courseService.listByStudent(param);
        // 文件地址
        String filePrefix = CommonUtil.getCatalinaHome() + environment.getProperty("teaching-platform.ppt-png-place");
        filePrefix = filePrefix.substring(filePrefix.indexOf("/teaching_platform_file"));
        for (PptCourse p : pptCoursePage.getRecords()) {
            p.setFolderName(filePrefix + p.getFolderName() + "/");
        }

        Map<String, Object> resultMap = new HashMap<>(16);
        resultMap.put("list", pptCoursePage.getRecords());
        resultMap.put("total", pptCoursePage.getTotal());
        return R.success(resultMap);
    }

    public R add(UserPpt userPpt) {
        if (userPpt.getStudentIdList() == null || userPpt.getStudentIdList().isEmpty()) {
            return R.success();
        }
        // 先删除之前的
        userPptService.removeByPptId(userPpt.getPptId());
        // 添加新的
        userPptService.insertBatch(userPpt.getPptId(), userPpt.getStudentIdList());
        return R.success();
    }

    public R studentList(Integer pptId) {
        List<SysUser> list = userPptService.getStudents(pptId);
        return R.success().put("list", list);
    }
}
