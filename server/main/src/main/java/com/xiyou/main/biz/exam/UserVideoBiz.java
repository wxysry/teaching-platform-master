package com.xiyou.main.biz.exam;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.xiyou.common.utils.CommonUtil;
import com.xiyou.common.utils.R;
import com.xiyou.main.entity.exam.SysUser;
import com.xiyou.main.entity.exam.UserVideo;
import com.xiyou.main.entity.exam.VideoCourse;
import com.xiyou.main.entity.exam.VideoGroup;
import com.xiyou.main.params.exam.CourseQueryParam;
import com.xiyou.main.service.exam.UserVideoService;
import com.xiyou.main.service.exam.VideoCourseService;
import com.xiyou.main.service.exam.VideoGroupService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @program: multi-module
 * @description:
 * @author: tangcan
 * @create: 2019-10-28 15:56
 **/
@Service
public class UserVideoBiz {
    @Autowired
    private VideoCourseService courseService;
    @Autowired
    Environment environment;
    @Autowired
    private VideoGroupService groupService;
    @Autowired
    private UserVideoService userVideoService;

    public R group(Integer studentId) {
        List<VideoGroup> list = groupService.listByStudent(studentId);
        Map<String, Object> resultMap = new HashMap<>(16);
        resultMap.put("list", list);
        return R.success(resultMap);
    }

    public R list(CourseQueryParam param) {
        Page<VideoCourse> videoCoursePage = courseService.listByStudent(param);
        Map<String, Object> resultMap = new HashMap<>(16);
        // 文件地址
        String filePrefix = CommonUtil.getCatalinaHome() + environment.getProperty("teaching-platform.video-course-place");
        filePrefix = filePrefix != null ? filePrefix.substring(filePrefix.indexOf("/teaching_platform_file")) : "";
        for (VideoCourse videoCourse : videoCoursePage.getRecords()) {
            videoCourse.setFileName(filePrefix + videoCourse.getFileName());
        }
        resultMap.put("list", videoCoursePage.getRecords());
        resultMap.put("total", videoCoursePage.getTotal());
        return R.success(resultMap);
    }

    public R add(UserVideo userVideo) {
        if (userVideo.getStudentIdList() == null || userVideo.getStudentIdList().isEmpty()) {
            return R.success();
        }
        // 先删除之前的
        userVideoService.removeByVideoId(userVideo.getVideoId());
        // 添加新的
        userVideoService.insertBatch(userVideo.getVideoId(), userVideo.getStudentIdList());
        return R.success();
    }

    public R studentList(Integer videoId) {
        List<SysUser> list = userVideoService.getStudents(videoId);
        return R.success().put("list", list);
    }
}
