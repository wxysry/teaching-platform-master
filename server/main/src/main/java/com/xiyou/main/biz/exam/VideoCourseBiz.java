package com.xiyou.main.biz.exam;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.xiyou.common.enums.CodeEnum;
import com.xiyou.common.office.utils.FileUtil;
import com.xiyou.common.utils.CommonUtil;
import com.xiyou.common.utils.R;
import com.xiyou.main.entity.exam.VideoCourse;
import com.xiyou.main.entity.exam.VideoGroup;
import com.xiyou.main.enums.ErrorEnum;
import com.xiyou.main.params.exam.CourseQueryParam;
import com.xiyou.main.service.exam.UserVideoGroupService;
import com.xiyou.main.service.exam.VideoCourseService;
import com.xiyou.main.service.exam.VideoGroupService;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author dingyoumeng
 * @since 2019/07/08
 */
@Service
public class VideoCourseBiz {
    private Logger log = LoggerFactory.getLogger(this.getClass());
    @Autowired
    Environment environment;
    @Autowired
    private VideoGroupService groupService;
    @Autowired
    private VideoCourseService courseService;
    @Autowired
    private UserVideoGroupService userVideoGroupService;
    @Autowired
    private VideoGroupService videoGroupService;

    public R addGroup(VideoGroup group) {
        QueryWrapper<VideoGroup> wrapper = new QueryWrapper<>();
        wrapper.eq("group_name", group.getGroupName());
        if (videoGroupService.getOne(wrapper) != null) {
            return R.error(CodeEnum.PARAM_ERROR.getCode(), "分组已存在");
        }
        groupService.insert(group);
        VideoGroup videoGroup = videoGroupService.getOne(wrapper);
        Map<String, Object> returnMap = new HashMap<>();
        returnMap.put("id", videoGroup.getId());
        return R.success(returnMap);
    }

    public R listGroup() {
        List<VideoGroup> list = groupService.list();
        Map<String, Object> resultMap = new HashMap<>(16);
        resultMap.put("list", list);
        return R.success(resultMap);
    }

    public R updateGroup(VideoGroup group) {
        QueryWrapper<VideoGroup> wrapper = new QueryWrapper<>();
        wrapper.eq("group_name", group.getGroupName());
        VideoGroup videoGroup = groupService.getOne(wrapper);
        if (videoGroup != null && !videoGroup.getId().equals(group.getId())) {
            return ErrorEnum.COURSE_GROUP_EXIST.getR();
        }
        groupService.updateById(group);
        return R.success();
    }

    public R deleteGroup(String... ids) {
        groupService.removeByIds(Arrays.asList(ids));
        courseService.removeByGroupIds(Arrays.asList(ids));
        return R.success();
    }

    public R getGroupById(int groupId) {
        Map<String, Object> resultMap = new HashMap<>(16);
        VideoGroup group = groupService.getById(groupId);
        resultMap.put("group", group);
        return R.success(resultMap);
    }


    public R deleteCourse(Integer... ids) {
        courseService.removeByIds(Arrays.asList(ids));
        courseService.removeByGroupIds(Arrays.asList(ids));
        userVideoGroupService.removeByGroupIds(Arrays.asList(ids));
        return R.success();
    }

    public R updateCourse(VideoCourse course) {
        courseService.updateById(course);
        return R.success();
    }

    public R listCourse(CourseQueryParam param) {
        Page page = param.getIPage();
        QueryWrapper<VideoCourse> wrapper = new QueryWrapper<>();
        if (param.getGroupId() != null && param.getGroupId() != 0) {
            wrapper.eq("group_id", param.getGroupId());
        }
        if (StringUtils.isNotBlank(param.getValue())) {
            wrapper.like("course_name", param.getValue());
        }
        wrapper.orderByDesc("id");
        List<VideoCourse> list = courseService.page(page, wrapper).getRecords();
        List<VideoGroup> groupList = videoGroupService.list();
        Map<Integer, String> groupNameMap = groupList.stream().collect(Collectors.toMap(VideoGroup::getId, VideoGroup::getGroupName, (k1, k2) -> k1));
        // 文件地址
        String filePrefix = CommonUtil.getCatalinaHome() + environment.getProperty("teaching-platform.video-course-place");
        filePrefix = filePrefix != null ? filePrefix.substring(filePrefix.indexOf("/teaching_platform_file")) : "";
        for (VideoCourse videoCourse : list) {
            videoCourse.setGroupName(groupNameMap.get(videoCourse.getGroupId()));
            videoCourse.setFileName(filePrefix + videoCourse.getFileName());
        }
        Map<String, Object> resultMap = new HashMap<>();
        resultMap.put("total", courseService.count(wrapper));
        resultMap.put("list", list);
        return R.success(resultMap);
    }

    public R uploadPic(MultipartFile file) {
        Map<String, Object> returnMap = new HashMap<>();
        returnMap.put("cover", FileUtil.upload(file, CommonUtil.getCatalinaHome() + environment.getProperty("teaching-platform.video-cover-place"), true));
        return R.success(returnMap);
    }

    public R uploadVideo(MultipartFile file) {
        Map<String, Object> returnMap = new HashMap<>();
        returnMap.put("fileName", FileUtil.upload(file, CommonUtil.getCatalinaHome() + environment.getProperty("teaching-platform.video-course-place")));
        return R.success(returnMap);
    }

    public void showCover(HttpServletResponse response, String cover) {
        if (StringUtils.isBlank(cover)) {
            return;
        }
        try {
            FileUtil.exportPic(response, CommonUtil.getCatalinaHome() + environment.getProperty("teaching-platform.video-cover-place") + cover);
        } catch (IOException e) {
            log.info(e.getMessage());
        }
    }

    public void showVideo(HttpServletResponse response, String video) {
        if (StringUtils.isBlank(video)) {
            return;
        }
        try {
            FileUtil.exportVideo(response, CommonUtil.getCatalinaHome() + environment.getProperty("teaching-platform.video-course-place") + video);
        } catch (IOException e) {
            log.info(e.getMessage());
        }
    }


    public R listByTeacher(Integer userId, CourseQueryParam param) {
        param.setTeacherId(userId);
        Page<VideoCourse> videoCoursePage = courseService.listByUser(param);
        Map<String, Object> resultMap = new HashMap<>(16);
        // 文件地址
        String filePrefix = CommonUtil.getCatalinaHome() + environment.getProperty("teaching-platform.video-course-place");
        filePrefix = filePrefix.substring(filePrefix.indexOf("/teaching_platform_file"));
        for (VideoCourse videoCourse : videoCoursePage.getRecords()) {
            videoCourse.setFileName(filePrefix + videoCourse.getFileName());
        }
        resultMap.put("list", videoCoursePage.getRecords());
        resultMap.put("total", videoCoursePage.getTotal());
        return R.success(resultMap);
    }

    public R listGroupByTeacher(Integer userId) {
        List<VideoGroup> list = groupService.listByTeacher(userId);
        Map<String, Object> resultMap = new HashMap<>(16);
        resultMap.put("list", list);
        return R.success(resultMap);
    }

    public R addCourse(Integer adminId, MultipartFile file, Integer groupId, String cover, String videoName, String intro) {
        VideoCourse videoCourse = new VideoCourse().setGroupId(groupId)
                .setCover(cover)
                .setVideoName(videoName)
                .setIntro(intro)
                .setUploadUserId(adminId);
        videoCourse.setFileName(FileUtil.upload(file, CommonUtil.getCatalinaHome() + environment.getProperty("teaching-platform.video-course-place")));
        courseService.save(videoCourse);
        return R.success();
    }

}
