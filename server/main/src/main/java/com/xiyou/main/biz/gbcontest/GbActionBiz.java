package com.xiyou.main.biz.gbcontest;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.xiyou.common.enums.CodeEnum;
import com.xiyou.common.exception.CustomException;
import com.xiyou.common.utils.R;
import com.xiyou.main.dao.gbcontest.*;
import com.xiyou.main.entity.gbcontest.*;
import com.xiyou.main.entity.gbcontest.GbRePaymentInfo;
import com.xiyou.main.enums.CashFlowActionEnum;
import com.xiyou.main.enums.ErrorEnum;
import com.xiyou.main.params.gbcontest.*;
import com.xiyou.main.service.gbcontest.GbCtCashflowService;
import com.xiyou.main.service.gbcontest.GbCtFeeService;
import com.xiyou.main.service.gbcontest.*;
import com.xiyou.main.service.gbcontest.GbCtBalanceService;
import com.xiyou.main.service.gbcontest.GbCtBankLoanService;
import com.xiyou.main.service.gbcontest.GbCtChargesService;
import com.xiyou.main.service.gbcontest.GbCtGzMaterialService;
import com.xiyou.main.service.gbcontest.GbCtGzProductService;
import com.xiyou.main.service.gbcontest.GbCtKcMaterialService;
import com.xiyou.main.service.gbcontest.GbCtKcProductService;
import com.xiyou.main.service.gbcontest.GbCtLineService;
import com.xiyou.main.service.gbcontest.GbCtProfitChartService;
import com.xiyou.main.service.gbcontest.GbCtXzOrderService;
import com.xiyou.main.service.gbcontest.GbCtYfMarketService;
import com.xiyou.main.service.gbcontest.GbCtYfProductService;
import com.xiyou.main.utils.DateUtils;
import com.xiyou.main.vo.gbcontest.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.HttpServletResponse;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @author dingyoumeng
 * @since 2019/07/25
 */
@Service
@Transactional(rollbackFor = Exception.class)
@Slf4j
public class GbActionBiz {
    @Autowired
    private GbCtGzMaterialMapper gbCtGzMaterialMapper;
    @Autowired
    private GbCtFeeService ctFeeService;
    @Autowired
    GbCtCashflowService ctCashflowService;
    @Autowired
    GbCtXzOrderService ctXzOrderService;
    @Autowired
    GbCtKcProductService ctKcProductService;
    @Autowired
    GbCtGzProductService ctGzProductService;
    @Autowired
    GbCtYfProductService ctYfProductService;
    @Autowired
    GbCtGzCsService ctGzCsService;
    @Autowired
    GbCtKcMaterialService ctKcMaterialService;
    @Autowired
    GbCtGzMaterialService ctGzMaterialService;
    @Autowired
    GbCtYfMarketService ctYfMarketService;
    @Autowired
    GbCtXzOrderMapper ctXzOrderMapper;
    @Autowired
    GbCtFeeMapper ctFeeMapper;
    @Autowired
    private GbCtGzProductMapper ctGzProductMapper;
    @Autowired
    private GbCtGzIsoMapper ctGzIsoMapper;
    @Autowired
    private GbRestoreDataBiz gbRestoreDataBiz;
    @Autowired
    private GbCtGzMarketMapper ctGzMarketMapper;
    @Autowired
    private GbCtYfProductMapper ctYfProductMapper;
    @Autowired
    private GbCtCorporateMapper ctCorporateMapper;
    @Autowired
    private GbApplyForLoanBiz applyForLoanBiz;
    @Autowired
    private GbCtWorkerMapper ctWorkerMapper;
    @Autowired
    private GbCtLineMapper ctLineMapper;
    @Autowired
    private GbCtYfMarketMapper ctYfMarketMapper;
    @Autowired
    private GbCtYfIsoMapper ctYfIsoMapper;
    @Autowired
    private GbCtYfNumMapper ctYfNumMapper;
    @Autowired
    private GbCtGzDiscountMapper gbCtGzDiscountMapper;
    @Autowired
    private GbCtLineService gbCtLineService;
    @Autowired
    private GbCtChargesService ctChargesService;
    @Autowired
    private GbCtProfitChartService ctProfitChartService;
    @Autowired
    private GbCtBalanceService ctBalanceService;
    @Autowired
    private GbCtFinancialTargetService gbCtFinancialTargetService;
    @Autowired
    private GbCtBankLoanService ctBankLoanService;
    @Autowired
    private GbCtCorporateMapper gbCtCorporateMapper;
    @Autowired
    private GbCtDdMaterialMapper gbCtDdMaterialMapper;
    @Autowired
    private GbApplyForLoanBiz gbApplyForLoanBiz;
    @Autowired
    private GbContestService gbContestService;
    @Autowired
    private GbContestStudentMapper gbContestStudentMapper;
    @Autowired
    private GbCtCapitalMapper gbCtCapitalMapper;
    @Autowired
    private GbContestStudentService gbContestStudentService;
    @Autowired
    private GbDownloadBiz gbDownloadBiz;





    /**
     * 减分数
     */
    public void minusScoreAndLog(Integer studentId,Integer contestId,String action){
        int score = ctCorporateMapper.getScore(studentId, contestId);
        score = score -1;
        GbCtCorporate ctCorporate = new GbCtCorporate();
        ctCorporate.setStudentId(studentId)
                .setContestId(contestId)
                .setScore(score)
                .setAction(action);
        ctCorporateMapper.insert(ctCorporate);
    }

//    public R checkReceivables(Integer userId, Integer contestId) {
//        QueryWrapper<GbCtFee> queryWrapper = new QueryWrapper<>();
//        queryWrapper.eq("student_id", userId);
//        queryWrapper.eq("contest_id", contestId);
//        queryWrapper.eq("r_remain_date", 1);
//        GbCtFee ctFee = ctFeeService.getOne(queryWrapper);
//        Map<String, Object> resultMap = new HashMap<>(16);
//        resultMap.put("fee", ctFee);
//        return R.success(resultMap);
//    }

    /**
     * 应收款确认
     * @param userId
     * @param contestId
     * @param feeId
     * @param currentTime
     * @return
     */
    public R confirmReceivables(Integer userId, Integer contestId,Integer feeId,Integer currentTime) {
        GbCtFee ctFee = ctFeeService.getById(feeId);
        if (ctFee == null) {
            return R.success();
        }
        Integer cash = ctCashflowService.getCash(userId, contestId);
        int remain = cash + ctFee.getRFee();
        //插入消费记录
        GbCtCashflow ctCashflow = new GbCtCashflow();
        ctCashflow.setCDate(currentTime)
                .setCSurplus(remain)
                .setCIn(ctFee.getRFee())
                .setCOut(0)
                .setCAction(CashFlowActionEnum.UPDATE_RECEIVABLE.getAction())
                .setContestId(contestId)
                .setStudentId(userId)
                .setCComment("更新应收款" + ctFee.getRFee() + "元");
        ctCashflowService.save(ctCashflow);
        ctFeeService.removeById(feeId);

        //计算四张报表,判断破产情况
        isBankruptcy(userId,contestId,currentTime);
        return R.success();
    }

    /**
     * 应付款确认
     * @param userId
     * @param contestId
     * @param feeId
     * @param currentTime
     * @return
     */
    public R confirmPay(Integer userId, Integer contestId,Integer feeId,Integer currentTime) {
        GbCtFee ctFee = ctFeeService.getById(feeId);
        if (ctFee == null) {
            return R.success();
        }
        Integer cash = ctCashflowService.getCash(userId, contestId);
        int remain = cash - ctFee.getRFee();
        if(remain < 0){
            return ErrorEnum.CASH_NOT_ENOUGH.getR();
        }
        //插入消费记录
        GbCtCashflow ctCashflow = new GbCtCashflow();
        ctCashflow.setCDate(currentTime)
                .setCSurplus(remain)
                .setCIn(0)
                .setCOut(ctFee.getRFee())
                .setCAction(CashFlowActionEnum.UPDATE_PAY.getAction())
                .setContestId(contestId)
                .setStudentId(userId)
                .setCComment("更新应付款" + ctFee.getRFee() + "W");
        ctCashflowService.save(ctCashflow);
        ctFeeService.removeById(feeId);
        return R.success();
    }

    /**
     * 查看所有订单
     * @param userId
     * @param contestId
     * @param date
     * @return
     */
    public R listOrder(Integer userId, Integer contestId, Integer date) {
        Map<String, Object> queryMap = new HashMap<>();
        queryMap.put("studentId", userId);
        queryMap.put("contestId", contestId);
//        queryMap.put("year", date / 10);
//        queryMap.put("date", date);
        //交单时间为空
//        queryMap.put("commitDate", 0);
        List<GbXzOrderVo> list = ctXzOrderService.listDeliveryOrder(queryMap);
        List<GbCtGzIso> ctGzIsoList = ctGzIsoMapper.list(contestId);
        Map<Integer, String> isoNameMap = ctGzIsoList.stream().collect(Collectors.toMap(GbCtGzIso::getCiId, GbCtGzIso::getCiName, (k1, k2) -> k1));
        list.forEach(p -> {
            //设置ISO
            if (p.getIsoId() == null) {
                p.setCiName("-");
            } else if (p.getIsoId() == 3) {
                p.setCiName(isoNameMap.get(1) + " / " + isoNameMap.get(2));
            } else {
                p.setCiName(isoNameMap.get(p.getIsoId()));
            }
            //设置状态  违约/已交货/未交货

            String status = "";
            if (p.getCommitDate() != null && p.getCommitDate() > 0) {
                status = "已交货";
            } else if ((date / 10 == p.getDate() && date % 10 > p.getDeliveryDate()) || ((date / 10) > p.getDate())) {
                status = "已违约";
            }
            else{
                status = "可交货";
            }
            p.setStatus(status);
        });
        Map<String, Object> resultMap = new HashMap<>(16);
        resultMap.put("list", list);
        return R.success(resultMap);
    }

    /***
     * 交货
     * @param userId
     * @param contestId
     * @param orderId
     * @param date
     * @return
     */
    public R deliveryOrder(Integer userId, Integer contestId, Integer orderId, Integer date) {

        GbCtXzOrder order = ctXzOrderService.getById(orderId);//获取订单
        if (order == null) {
            return R.error(CodeEnum.BUSINESS_WRONG.getCode(), "无此订单");
        }
        GbCtGzProduct ctGzProduct = ctGzProductMapper.get(contestId, order.getCpId());//获取规则
        if (ctGzProduct == null) {
            return R.error(CodeEnum.BUSINESS_WRONG.getCode(), "无此产品");
        }
        List<GbCtKcProduct> listKc = ctKcProductMapper.getAllKcProductAsc(contestId, userId,ctGzProduct.getId());//ASC产品库存
        int kCNum = listKc.size();
        if (kCNum < order.getNum()) {
            return R.error(CodeEnum.BUSINESS_WRONG.getCode(), "库存不足");
        }
        order.setCommitDate(date);
        ctXzOrderService.updateById(order);

        //计算成本 删除产品的库存
        int cost = 0;//成本
        int submitNum = order.getNum();
        for (int i = 0; i < listKc.size(); i++) {
            if(i < submitNum){
                GbCtKcProduct gbCtKcProduct = listKc.get(i);
                int realCost = gbCtKcProduct.getRealCost();
                cost = cost + realCost;
                ctKcProductMapper.deleteById(gbCtKcProduct);
            }
        }

        order.setCommitDate(date).setTotalCost(cost);
        ctXzOrderService.updateById(order);


        int paymentDate = order.getPaymentDate();//账期
        //产生应收账单 此处不生成现金流量表 现金流量表在季末生成
            int payDate = DateUtils.addYearAndSeasonTime(date,paymentDate);//收款日期
            GbCtFee ctFee = new GbCtFee();
            ctFee.setStudentId(userId)
                    .setContestId(contestId)
                    .setBorrower("经销商")
                    .setLender("销售部")
                    .setRFee(order.getTotalPrice())
                    .setRemarks("订单收款")
                    .setRRemainDate(paymentDate)
                    .setPaymentDate(payDate)
                    .setType(GbCtFee.TO_RECEIVABLE);
            ctFeeService.save(ctFee);
//        }


        //计算四张报表,判断破产情况
        isBankruptcy(userId,contestId,date);

        return R.success();
    }

    /**
     * 研发产品列表
     * @param userId
     * @param contestId
     * @return
     */
    public R yfProductList(Integer userId, Integer contestId) {
        List<GbYfProductVO> list = ctGzProductService.listYfProduct(userId, contestId);
        Map<String, Object> resultMap = new HashMap<>(16);
        resultMap.put("list", list);
        return R.success(resultMap);
    }

    /**
     * 研发产品
     * @param userId
     * @param yfParam
     * @return
     */
    public R yfProduct(Integer userId, GbYfParam yfParam) {
        Integer cpId = yfParam.getCpId();
        GbCtGzProduct gbCtGzProduct = ctGzProductService.getById(cpId);
        int depFeeSum = gbCtGzProduct.getCpProcessingFee();
        int cash = ctCashflowService.getCash(userId, yfParam.getContestId());
        int remain = cash - depFeeSum;
        if (remain < 0) {
            return ErrorEnum.CASH_NOT_ENOUGH.getR();
        }
        GbCtCashflow ctCashflow = new GbCtCashflow();
        ctCashflow.setCDate(yfParam.getDate())
                .setCSurplus(remain)
                .setCIn(0)
                .setCOut(depFeeSum)
                .setCAction(CashFlowActionEnum.DEVELOP_PRODUCT.getAction())
                .setContestId(yfParam.getContestId())
                .setStudentId(userId)
                .setCComment("扣减产品"+gbCtGzProduct.getCpName()+"研发" + depFeeSum + "元");
        ctCashflowService.save(ctCashflow);

        QueryWrapper<GbCtYfProduct> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("student_id", userId);
        queryWrapper.eq("contest_id", yfParam.getContestId());
        queryWrapper.eq("dp_cp_id", gbCtGzProduct.getId());
        GbCtYfProduct p = ctYfProductService.getOne(queryWrapper);
        if (GbCtYfProduct.CP_YF_ING.equals(p.getDpState()) || GbCtYfProduct.CP_YF_FINISH.equals(p.getDpState())) {
            return R.error(CodeEnum.OTHER_ERROR.getCode(), "无需重复研发");
        }
        Integer remainDate = gbCtGzProduct.getCpDevelopDate();
        Integer finishDate = DateUtils.addYearAndSeasonTime(yfParam.getDate(), remainDate);
        if (remainDate != null && remainDate > 0) {
            p.setDpStartDate(yfParam.getDate());
            p.setDpFinishDate(finishDate);
            p.setDpState(GbCtYfProduct.CP_YF_ING);
        } else {        //研发周期为0的情况下，直接研发成功
            p.setDpStartDate(yfParam.getDate());//申请时间
            p.setDpFinishDate(yfParam.getDate());
            p.setDpState(GbCtYfProduct.CP_YF_FINISH);
        }
        ctYfProductService.updateById(p);

        //计算四张报表,判断破产情况
        isBankruptcy(userId,yfParam.getContestId(),yfParam.getDate());

        return R.success();
    }

    /**
     * 季末操作
     * @param studentId
     * @param contestId
     * @param date
     */
    public void seasonEndAction(Integer studentId, Integer contestId, Integer date){
        //1.自动缴纳未缴管理费 ，扣社会责任
        //2.自动缴纳未缴纳贷款本金利息 ，扣社会责任
        //3.自动缴纳应付款未付，扣社会责任
        //4.自动订单违约扣款，扣社会责任
        //规则
        GbCtGzCs ctGzCs = ctGzCsService.getByContestId(contestId);

        //费用管理-缴纳未缴纳的管理费用,并且扣社会责任得分
        GbRePaymentInfo overhaul = applyForLoanBiz.getOverhaul(contestId, studentId, date);
        if(overhaul != null){
            applyForLoanBiz.payOverhaul(contestId,studentId ,date);
            minusScoreAndLog(studentId,contestId,"未缴管理费");
        }

        //费用管理-缴纳利息,并且扣社会责任得分  2.费用管理-缴纳本金,并且扣社会责任得分
        List<GbRePaymentInfo> bankLoan = applyForLoanBiz.getBankLoan(date, contestId, studentId);
        //获取现金
        for (GbRePaymentInfo info : bankLoan) {
            Integer cash = ctCashflowService.getCash(studentId, contestId);
            applyForLoanBiz.payCurrSeasonAddCashFlow(contestId,studentId,date,info.getAmount().intValue(),info.getPayment(),info.getBlAddTime(),cash);
            //扣社会责任分
            minusScoreAndLog(studentId,contestId,"贷款利息或本金未偿还");
        }

        //应收货强制收货并产生账单和扣分
        List<GbCtDdMaterial> materialOrderRec = gbCtDdMaterialMapper.getMaterialOrderRec(studentId, contestId, date);
        for (GbCtDdMaterial dd : materialOrderRec) {
            gbApplyForLoanBiz.materialToInventory(dd.getId(), date);
            //扣社会责任分
            ctCorporateMapper.minusScore(studentId,contestId);
        }

        //应付款强制付款，并且扣社会责任得分
        QueryWrapper<GbCtFee> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("student_id", studentId);
        queryWrapper.eq("contest_id", contestId);
        queryWrapper.eq("type", GbCtFee.TO_PAYMENT);
        queryWrapper.le("payment_date",date);
        queryWrapper.orderByAsc("payment_date");
        List<GbCtFee> list = ctFeeMapper.selectList(queryWrapper);
        for (GbCtFee f : list) {
            Integer cash = ctCashflowService.getCash(studentId, contestId);
            //应付款付款
            paymentAndCashFlow(f,studentId,contestId,date,cash);
            //扣社会责任分
            ctCorporateMapper.minusScore(studentId,contestId);
        }


        // 订单交货 -违约罚款并且扣社会责任得分
        //订单年份等于今年  当前季度 = 交货期， 且交货时间为空,则视为违约，需计算违约金
        List<GbCtXzOrder> orderList = ctXzOrderService.list(new GbCtXzOrder().setStudentId(studentId)
                .setContestId(contestId).setDate(date / 10).setCommitDate(0).setDeliveryDate(date % 10));

        for (GbCtXzOrder order : orderList) {
            //扣社会责任分
            ctCorporateMapper.minusScore(studentId,contestId);
        }
        // 违约金比例
        double punishRate = (ctGzCs == null ? 0.0 : (double) ctGzCs.getPunish() / 100.0);
        int punish = orderList.stream().mapToInt(p -> (int) Math.round(p.getTotalPrice()*punishRate)).sum();
        if(punish>0){
            Integer surplus = ctCashflowService.getCash(studentId, contestId);
            // Cashflow新一条记录ID、违约金、0、sum(四舍五入【xz_order.总价（当交单时间为空、年份=当前年份）*gz_cs.违约金比例】】-、现金+增加-减少、支付违约金、当前时间
            ctCashflowService.save(new GbCtCashflow().setStudentId(studentId)
                    .setContestId(contestId)
                    .setCAction("违约金")
                    .setCIn(0)
                    .setCOut(punish)
                    .setCSurplus(surplus - punish)
                    .setCComment("支付违约金" + punish + "元")
                    .setCDate(date));
        }
    }

    public void updateStatus(Integer studentId, Integer contestId, Integer finishDate){

        log.error("contestId:{},studentId:{}当季开始更新状态-开始,时间:{}",contestId,studentId,finishDate);

        //下个季节的时间
        // 更新工人状态，上个季度招聘-下个季度正式入职
        ctWorkerMapper.updateWorkerToOnboarding(contestId,studentId, DateUtils.addYearAndSeasonTime(finishDate,-1));
        // 更新生产线 建成状态  建造中-> 空闲
        ctLineMapper.updateLineStatusBuildToSpace(contestId,studentId,finishDate,GbCtLine.ON_BUILD,GbCtLine.ON_SPACE);
        // 更新生产线 转产状态  转产中-> 空闲
        ctLineMapper.updateLineStatusTransferToSpace(contestId,studentId,finishDate, GbCtLine.ON_TRANSFER,GbCtLine.ON_SPACE);
        // 更新生产线 生产状态  生产中->空闲 ，产品数量++
        ctLineMapper.updateLineStatusProduceToSpace(contestId,studentId,finishDate,GbCtLine.ON_PRODUCE,GbCtLine.ON_SPACE);

        //修改产品入库状态
        ctKcProductMapper.updateProductToFinish(contestId,studentId,finishDate);

        // 更新产品研发状态
        ctYfProductMapper.updateStateToFinish(studentId,contestId,finishDate);
        // 更新市场研发状态
        ctYfMarketMapper.updateStateToFinish(studentId,contestId,finishDate);
        // 更新ISO研发状态
        ctYfIsoMapper.updateStateToFinish(studentId,contestId,finishDate);
        // 更新数字化研发状态
        ctYfNumMapper.updateStateToFinish(studentId,contestId,finishDate);

        log.error("contestId:{},studentId:{}当季开始更新状态-结束,时间:{}",contestId,studentId,finishDate);
    }


    /**
     * 扣除维修费
     * @param studentId
     * @param contestId
     * @param date
     */
    public void payMaintenanceFee(Integer studentId, Integer contestId, Integer date){
        Integer surplus = ctCashflowService.getCash(studentId, contestId);
        int maintenanceFee = gbCtLineService.getMaintenanceFee(studentId,contestId,date);
        // cashflow新增一条记录ID、维护费、0、sum[(当前line中PL_Remain_Date为null)*生产线所对应的维护费】、现金+增加-减少、支付维护费、当前时间
        if(maintenanceFee >0) {
            ctCashflowService.save(new GbCtCashflow().setStudentId(studentId)
                    .setContestId(contestId)
                    .setCAction("维修费")
                    .setCIn(0)
                    .setCOut(maintenanceFee)
                    .setCSurplus(surplus - maintenanceFee)
                    .setCComment("支付维修费" + maintenanceFee + "元")
                    .setCDate(date));
        }
    }





    /**
     * 下个季节需要做的事
     * @param studentId
     * @param contestId
     * @param nextSeason
     * @return
     */
    public R nextSeasonAction(Integer studentId, Integer contestId, Integer nextSeason){
        log.error("当季开始-开始,学生:{},contestId:{}",studentId,contestId);
        //更新学生的季度
        Integer date = nextSeason * 10 + 1;

        //判断是否重复更新数据
        GbContestStudent gbContestStudent = gbContestStudentMapper.selectOne(new LambdaQueryWrapper<GbContestStudent>()
                .eq(GbContestStudent::getContestId,contestId)
                .eq(GbContestStudent::getStudentId,studentId)
        );

        //如果时间未发生变化,则说明重写更新数据
        if((gbContestStudent.getDate()/10) == nextSeason){
            throw new CustomException(CodeEnum.OTHER_ERROR.getCode(), "已跳转到下一季度,请勿重复操作");
        }


        gbContestStudentMapper.saveProgress(studentId,contestId,date,"{\"fillReportForm\":true,\"adLaunch\":true,\"attendOrderMeeting\":false}");
        log.error("当季开始-开始,学生:{},contestId:{},date:{}",studentId,contestId,date);

        int year = nextSeason/10;
         int season = nextSeason%10;
        //判断当季属于哪个季度，每年一季度要扣税
        if((year!=1)&&(season==1)){
            //支付所得税
            // 上一年报表计算的所得税
            List<GbCtProfitChart> profitChartList = ctProfitChartService.list(new GbCtProfitChart().setStudentId(studentId).setContestId(contestId).setPcDate((nextSeason / 10) - 1));
            int lastYearTax = profitChartList.stream().mapToInt(GbCtProfitChart::getPcTax).sum();

            int cash = ctCashflowService.getCash(studentId, contestId);
            ctCashflowService.save(new GbCtCashflow().setStudentId(studentId)
                    .setContestId(contestId)
                    .setCAction("支付所得税")
                    .setCIn(0)
                    .setCOut(lastYearTax)
                    .setCSurplus(cash - lastYearTax)
                    .setCComment("支付所得税" + lastYearTax + "元")
                    .setCDate(nextSeason));
        }

        //维修费
        payMaintenanceFee(studentId,contestId, nextSeason);
        //折旧计提
        // 更新累计折旧
        gbCtLineService.updateDepFee(studentId, contestId, nextSeason);


        //折旧费计入统计报表
        //1. 先生成四张报表，用于保存折旧费，后面会被覆盖
        calBsTotalEquity(studentId,contestId,nextSeason);
        // 计算本季度的折旧费  + 本年前几个季度的折旧费合计 ，将折旧费存到 profit表的折旧费字段。
        //2.先获取本季度新产生的折旧费
        Integer thisSeasonDep = gbCtLineService.getDepTotal(studentId, contestId, nextSeason);
        //3.获取本年上几个季度的折旧费
        //获取本年已计算的折旧费
        GbCtProfitChart  gbCtProfitChartTemp= ctProfitChartService.getOne(new QueryWrapper<GbCtProfitChart>()
                .eq("student_id",studentId)
                .eq("contest_id",contestId)
                .eq("pc_date",nextSeason/10)
                .eq("bs_isxt",1)//系统自动生成
        );
        //4  更新折旧费字段的值
        gbCtProfitChartTemp.setPcDep(gbCtProfitChartTemp.getPcDep()+thisSeasonDep);
        ctProfitChartService.updateById(gbCtProfitChartTemp);



        //更新产线/产品/研发(产品、市场、数字化)状态
        updateStatus(studentId,contestId, nextSeason);

        //计算报表,判断是否破产
        isBankruptcy(studentId,contestId,nextSeason);

        //备份当季数据时间节点+1作为下季的季初数据
        gbRestoreDataBiz.backupData(contestId,studentId,nextSeason);
        return R.success();
    }


    /**
     * 当季结束
     * @param studentId
     * @param contestId
     * @param date
     * @return
     */
    public  R endingSeason(Integer studentId, Integer contestId, int date) {

        GbContestStudent gbContestStudent= gbContestStudentService.getOne(new LambdaQueryWrapper<GbContestStudent>()
                .eq(GbContestStudent::getStudentId,studentId)
                .eq(GbContestStudent::getContestId,contestId)
                );
        //校验一下当前后台数据是否已经发生了变化
        int oldDate = gbContestStudent.getDate()/10;
        if(oldDate!=date){
            throw new CustomException(CodeEnum.OTHER_ERROR.getCode(), "操作失败，请重试");
        }


        //1.自动缴纳未缴管理费 ，扣社会责任
        //2.自动缴纳未缴纳贷款本金利息 ，扣社会责任
        //3.自动缴纳应付款未付，扣社会责任
        //4.自动订单违约扣款，扣社会责任
        seasonEndAction(studentId,contestId, date);

        //计算报表,判断是否破产
        isBankruptcy(studentId,contestId,date);


        //判断是当年结束还是当季结束
        int season =date %10;
        //当季结束.直接进入下一个季度
        if(season!=4){
            //保存当季结束时间
            gbContestStudentService.insertStudentEndTime(contestId,studentId,date);

            //判断该用户是否是处于统一模式下
            if(studentIsSyn(studentId,contestId)){
                //当年结束更新学生的填表状态
                int updateRow = gbContestStudentMapper.update(new GbContestStudent()
                                .setDate(date*10+6)
                                .setProgress("{\"fillReportForm\":true,\"adLaunch\":false,\"attendOrderMeeting\":false}"),
                        new LambdaQueryWrapper<GbContestStudent>()
                                .eq(GbContestStudent::getContestId,contestId)
                                .eq(GbContestStudent::getStudentId,studentId)
                                .eq(GbContestStudent::getDate,gbContestStudent.getDate())
                );
                if(updateRow!=1){
                    throw new CustomException(CodeEnum.OTHER_ERROR.getCode(), "操作失败，请重试");
                }
                return R.success();
            }
            //非统一模式下，直接进入下季度。
            else{
                //获取下个季度的时间(折旧计提，维修费，更新产品状态)
                Integer nextSeason = DateUtils.addYearAndSeasonTime(date,1);
                return nextSeasonAction(studentId,contestId,nextSeason);
            }
        }
        //到达填写报表阶段
        else if(season==4){
            //当年结束更新学生的填表状态
           int updateRow = gbContestStudentMapper.update(new GbContestStudent()
                            .setDate(date*10+5)
                            .setProgress("{\"fillReportForm\":true,\"adLaunch\":false,\"attendOrderMeeting\":false}"),
                    new LambdaQueryWrapper<GbContestStudent>()
                            .eq(GbContestStudent::getContestId,contestId)
                            .eq(GbContestStudent::getStudentId,studentId)
                            .eq(GbContestStudent::getDate,gbContestStudent.getDate())
            );
           if(updateRow!=1){
               throw new CustomException(CodeEnum.OTHER_ERROR.getCode(), "操作失败，请重试");
           }
            return  R.success();
        }else{
            throw  new CustomException(777,"季度错误");
        }
    }

    /**
     * 判断学生是否处于同一模式下
     * @return
     */
    public boolean studentIsSyn(int studentId,int contestId){
        //获取考试信息
        GbContest gbContest = gbContestService.getOne(new LambdaQueryWrapper<GbContest>().eq(GbContest::getContestId,contestId));
        if(1==gbContest.getIsSyn()){
            //教师时间
           int teachDate = gbContest.getTeacherDate();
           int year = teachDate/10;

           //获取学生的基本信息
           GbContestStudent gbContestStudent = gbContestStudentService.getOne(new LambdaQueryWrapper<GbContestStudent>()
                    .eq(GbContestStudent::getContestId,contestId)
                    .eq(GbContestStudent::getStudentId,studentId)
            );
            //学生的时间线
            int studentDate = gbContestStudent.getDate()/10;
            //学生最近一次还原的年份
            int lastRestoreYear =gbContestStudent.getLastRestoreTime()==null?0: gbContestStudent.getLastRestoreTime()/10;
            //满足三个条件，统一模式 时间线与大部队一致，当年无还原，则视为统一模式
            return (studentDate==teachDate)&&(lastRestoreYear!=year);
        }else{
            return  false;
        }
    }


    /**
     * 生成报表
     * @param studentId
     * @param contestId
     * @param date
     * @return
     */
    public Integer calBsTotalEquity(Integer studentId, Integer contestId, Integer date){
        if((date/10)==0){
            throw  new RuntimeException("数据异常");
        }

        //获取本年已计算的折旧费
        GbCtProfitChart  gbCtProfitChartTemp= ctProfitChartService.getOne(new QueryWrapper<GbCtProfitChart>()
                .eq("student_id",studentId)
                .eq("contest_id",contestId)
                .eq("pc_date",date/10)
                .eq("bs_isxt",1)//系统自动生成
        );
        Integer depTotal = 0;
        if(gbCtProfitChartTemp!=null){
            depTotal = gbCtProfitChartTemp.getPcDep();
        }


        // gz_cs的管理费
        GbCtGzCs ctGzCs = ctGzCsService.getByContestId(contestId);
        //先删除本年的这四张报表
        ctChargesService.remove(new QueryWrapper<GbCtCharges>()
                .eq("student_id",studentId)
                .eq("contest_id",contestId)
                .eq("c_date",date/10)
        );
        ctProfitChartService.remove(new QueryWrapper<GbCtProfitChart>()
                .eq("student_id",studentId)
                .eq("contest_id",contestId)
                .eq("pc_date",date/10)
        );
        ctBalanceService.remove(new QueryWrapper<GbCtBalance>()
                .eq("student_id",studentId)
                .eq("contest_id",contestId)
                .eq("bs_year",date/10)
        );
        gbCtFinancialTargetService.remove(new QueryWrapper<GbCtFinancialTarget>()
                .eq("student_id",studentId)
                .eq("contest_id",contestId)
                .eq("ft_year",date/10)
        );


        //重新生成四张报表
        // 费用表charges。当年结束后，自动插入一条记录到charges
        GbCtCharges charges = new GbCtCharges().setStudentId(studentId).setContestId(contestId).setCDate(date / 10);
        //生成费用表数据并计算合计值
        ctChargesService.addBySys(charges);
        ctChargesService.updateTotal(charges.getChargesId());


        /*
        添加记录到利润表profit_chart
         */
        GbCtProfitChart profitChart = new GbCtProfitChart().setStudentId(studentId).setContestId(contestId).setBsIsxt(1).setPcDate(date / 10).setIsSubmit("Y");
        // 销售额 = Sum(已选定单xz_order.总价) 交单时间=本年
        // 成本 =  Sum已选定单xz_order.数量*对应产品成本)交单时间=本年
        Map<String, BigDecimal> salesAndDirectCostMap = ctXzOrderService.getSalesAndDirectCost(studentId, contestId, date / 10);
        BigDecimal sales = salesAndDirectCostMap.get("sales");
        BigDecimal directCost = salesAndDirectCostMap.get("directCost");
        profitChart.setPcSales(sales.intValue());
        profitChart.setPcDirectCost(directCost.intValue());

        // 毛利=销售额-成本
        profitChart.setPcGoodsProfit(profitChart.getPcSales() - profitChart.getPcDirectCost());
        // 管理费用=Charges今年系统填写的总和数据
        profitChart.setPcTotal(ctChargesService.getTotal(studentId, contestId, date / 10, 1));
        // 折旧前利润=毛利-管理费用
        profitChart.setPcProfitBeforeDep(profitChart.getPcGoodsProfit() - profitChart.getPcTotal());

        //计算折旧费
        profitChart.setPcDep(depTotal);
        // 财务费用前利润=折旧前利润-折旧
        profitChart.setPcProfitBeforeInterests(profitChart.getPcProfitBeforeDep() - profitChart.getPcDep());


        // 财务费用=Sum(cashflow.流出)当操作类型 in （"本息同还-利息", "每季付息-利息", "贴现"）
        List<String> actionList = Arrays.asList("本息同还-利息", "每季付息-利息", "贴现");
        List<GbCtCashflow> cashflowList = ctCashflowService.list(studentId, contestId, date / 10, actionList);
        profitChart.setPcFinanceFee(cashflowList.stream().mapToInt(GbCtCashflow::getCOut).sum());

        //营业外收支:  出售库存  出售原材料， 变卖生产线， 违约金
        List<GbCtCashflow> yywCashflowList = ctCashflowService.list(studentId, contestId, date / 10, Arrays.asList("出售库存", "出售原材料", "变卖生产线","违约金"));
        profitChart.setPcNonOperating(yywCashflowList.stream().mapToInt(x->0-x.getCOut()).sum());
        // 税前利润=财务费用前利润-财务费用
        profitChart.setPcProfitBeforeTax(profitChart.getPcProfitBeforeInterests() - profitChart.getPcFinanceFee() + profitChart.getPcNonOperating()) ;
        // 税前利润
        int profitBeforeTax = profitChart.getPcProfitBeforeTax();

        // 所得税: 当【税前利润+上一年资产负债表所有者权益>max(之前年份所有者权益）】= round{【税前利润+上一年所有者权益-max[最近一个所得税>0的年份的权益，第0年权益]】*所得税率/100}
        // 所得税率比例
        double incomeTaxRate = ctGzCs == null ? 0.0 : (double) ctGzCs.getIncomeTax() / 100.0;
        // 上一年资产负债表所有者权益
        GbCtBalance lastYearBalance = ctBalanceService.get(new GbCtBalance().setStudentId(studentId).setContestId(contestId).setBsYear(date / 10 - 1));

        //上一年所有者权益
        int lastYearTotalEquity = lastYearBalance == null ? 0 : lastYearBalance.getBsTotalEquity();
        //上一年股东资本
        int lastYearBsEquity = lastYearBalance == null ? 0 : lastYearBalance.getBsEquity();


        // max(之前年份所有者权益）
        int maxTotalEquityPreYear = ctBalanceService.maxTotalEquityPreYear(studentId, contestId, date / 10);

        if ((profitBeforeTax + lastYearTotalEquity) > maxTotalEquityPreYear) {
            // 第0年权益
            GbCtBalance zeroYearBalance = ctBalanceService.get(new GbCtBalance().setStudentId(studentId).setContestId(contestId).setBsYear(0));
            int zeroYearTotalEquity = zeroYearBalance == null ? 0 : zeroYearBalance.getBsTotalEquity();

            // 最近一个所得税>0的年份的权益
            Integer equity = ctBalanceService.getRecentYEarHaveEquity(studentId, contestId, date / 10);
            int recentYEarHaveEquity = (equity == null ? 0 : equity);

            int tax = (int) Math.round(incomeTaxRate * (double) (profitBeforeTax + lastYearTotalEquity - Math.max(recentYEarHaveEquity, zeroYearTotalEquity)));
            profitChart.setPcTax(tax);
        } else {
            profitChart.setPcTax(0);
        }
        profitChart.setPcAnnualNetProfit(profitBeforeTax - profitChart.getPcTax());
        ctProfitChartService.save(profitChart);

        //剩余现金
        Integer cashRest = ctCashflowService.getCash(studentId, contestId);
        /*
        资产负债表
         */
        GbCtBalance balance = new GbCtBalance()
                .setStudentId(studentId)
                .setContestId(contestId)
                .setBsIsxt(1)
                .setBsYear(date / 10)
                .setIsSubmit("Y");
        // 现金 = Cashflow最新现金
        balance.setBsCash(cashRest);


        // 应收款 = Fee表合计
        int fee = ctFeeService.getFeeSum(new GbCtFee().setStudentId(studentId).setContestId(contestId).setType(GbCtFee.TO_RECEIVABLE));
        balance.setBsReceivable(fee);


        // 在制品 = Line表剩余生产时间不为空的sum(生产产品所对应成本）
        int productInProcess = gbCtLineService.getProductInProcess(new GbCtLine().setStudentId(studentId).setContestId(contestId));
        balance.setBsProductInProcess(productInProcess);


        // 产成品 = Kc_product表sum(库存产品-所对应成本)
        int product = ctKcProductService.getProductSum(studentId, contestId);
        balance.setBsProduct(product);

        // 原材料=Kc_material表sum（库存数*所对应成本）
        int material = ctKcMaterialService.getMaterialSum(studentId, contestId);
        balance.setBsMaterial(material);

        // 流动资产合计 = 现金+应收款+在制品+产成品+原材料
        balance.setBsTotalCurrentAsset(balance.getBsCash() + balance.getBsReceivable() + balance.getBsProductInProcess() + balance.getBsProduct() + balance.getBsMaterial());


        // 生产线=Line表PL_Finish_Date不为空的sum（原值-折旧）  已建成的生产线总价
        List<GbCtLine> lineList = gbCtLineService.list(studentId, contestId);
        balance.setBsEquipment(lineList.stream().filter(p ->  !"在建".equals(p.getStatus())).mapToInt(p -> p.getPlInvest() - p.getPlDepTotal()).sum());

        // 在建
        balance.setBsProjectOnConstruction(lineList.stream().filter(p -> "在建".equals(p.getStatus())).mapToInt(GbCtLine::getPlInvest).sum());

        // 固定资产合计
        balance.setBsTotalFixedAsset(balance.getBsEquipment() + balance.getBsProjectOnConstruction());

        // 资产合计
        balance.setBsTotalAsset(balance.getBsTotalCurrentAsset() + balance.getBsTotalFixedAsset());


        // 长期贷款:Bank_loan表类型为1的金额合计
        // 短期贷款:Bank_loan表类型为2的金额合计
        List<GbCtBankLoan> bankLoanList = ctBankLoanService.list(studentId, contestId);
        balance.setBsLongLoan(bankLoanList.stream().filter(p -> p.getBlType() == 1).mapToInt(GbCtBankLoan::getBlFee).sum());

        balance.setBsShortLoan(bankLoanList.stream().filter(p -> p.getBlType() == 2).mapToInt(GbCtBankLoan::getBlFee).sum());

        //原材料的应付款也计入短期负债
        QueryWrapper<GbCtFee> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("student_id", studentId);
        queryWrapper.eq("contest_id", contestId);
        queryWrapper.eq("type", GbCtFee.TO_PAYMENT);
        List<GbCtFee> list = ctFeeMapper.selectList(queryWrapper);
        Integer marketShouldPay = list.stream().mapToInt(GbCtFee::getRFee).sum();
        balance.setBsShortLoan(balance.getBsShortLoan()+marketShouldPay);


        // 应交税费 = 利润表中的PC_TAX
        GbCtProfitChart ctProfitChart = ctProfitChartService.getSys(new GbCtProfitChart().setStudentId(studentId).setContestId(contestId).setPcDate(date / 10));
        balance.setBsTax(ctProfitChart.getPcTax());

        //其他应付款   0 工人工资
        balance.setBsOtherPay(0);

        // 负债合计 = 长期贷款+短期贷款+应交税费
        balance.setBsTotalLiability(balance.getBsLongLoan() + balance.getBsShortLoan() + balance.getBsTax());

        // 股东资本 = 第0年股东资本
        GbCtBalance ctBalance = ctBalanceService.get(new GbCtBalance().setStudentId(studentId).setContestId(contestId).setBsYear(0));
        balance.setBsEquity(ctBalance == null ? 0 : ctBalance.getBsEquity());

        // 利润留存 = 上一年所有者权益-上一年的股东资本
        balance.setBsRetainedEarning(lastYearTotalEquity - lastYearBsEquity);

        // 年度净利 = 利润表当年净利润
        balance.setBsAnnualNetProfit(ctProfitChart == null ? 0 : ctProfitChart.getPcAnnualNetProfit());

        // 所有者权益 = 股东资本+利润留存+年度净利
        balance.setBsTotalEquity(balance.getBsEquity() + balance.getBsRetainedEarning() + balance.getBsAnnualNetProfit());

        // 负债所有者权益合计 = 所有者权益+负债合计
        balance.setBsTotal(balance.getBsTotalEquity() + balance.getBsTotalLiability());

        ctBalanceService.save(balance);

        //财务指标
        GbCtFinancialTarget gbCtFinancialTarget = new GbCtFinancialTarget();
        gbCtFinancialTarget.setStudentId(studentId)
                .setContestId(contestId)
                .setFtYear(date / 10)
                .setFtIsxt(1).setIsSubmit("Y");


        //本年短期贷款+其他应付款+应交税金
        BigDecimal sum1 = new BigDecimal(balance.getBsShortLoan()+balance.getBsOtherPay()+balance.getBsTax());

        if(sum1.compareTo(BigDecimal.ZERO) != 0){
            //流动比率 :本年流动资产合计/（本年短期贷款+其他应付款+应交税金）
            BigDecimal ftCurrentRate =  new BigDecimal(balance.getBsTotalCurrentAsset())
                    .divide(sum1,2, BigDecimal.ROUND_HALF_UP);
            gbCtFinancialTarget.setFtCurrentRate(ftCurrentRate);
            //速动比率: (本年现金+本年应收款)/（本年短期贷款+其他应付款+应交税金）
            BigDecimal ftQuickRate =  new BigDecimal(balance.getBsCash()+balance.getBsReceivable())
                    .divide(new BigDecimal(balance.getBsShortLoan()+balance.getBsOtherPay()+balance.getBsTax()),2, BigDecimal.ROUND_HALF_UP);
            gbCtFinancialTarget.setFtQuickRate(ftQuickRate);
        }else{
            //流动比率 :本年流动资产合计/（本年短期贷款+其他应付款+应交税金）
            gbCtFinancialTarget.setFtCurrentRate(BigDecimal.ZERO);
            //速动比率: (本年现金+本年应收款)/（本年短期贷款+其他应付款+应交税金）
            gbCtFinancialTarget.setFtQuickRate(BigDecimal.ZERO);
        }


        //资产负债率：本年负债合计/本年资产合计
        if(BigDecimal.ZERO.compareTo(new BigDecimal(balance.getBsTotalAsset())) != 0){
            BigDecimal ftDebtRate = new BigDecimal(balance.getBsTotalLiability())
                    .divide(new BigDecimal(balance.getBsTotalAsset()),2, BigDecimal.ROUND_HALF_UP);
            gbCtFinancialTarget.setFtDebtRate(ftDebtRate);
        }else{
            gbCtFinancialTarget.setFtDebtRate(BigDecimal.ZERO);
        }



        //产权比率： 本年负债合计/本年所有者权益
        BigDecimal equity = new BigDecimal(balance.getBsTotalEquity());
        if(equity.compareTo(BigDecimal.ZERO)!=0){
            BigDecimal ftEquityRate = new BigDecimal(balance.getBsTotalLiability())
                    .divide(equity,2, BigDecimal.ROUND_HALF_UP);
            gbCtFinancialTarget.setFtEquityRate(ftEquityRate);
        }else{
            gbCtFinancialTarget.setFtEquityRate(BigDecimal.ZERO);
        }



        //营业净利润率: 本年折旧前利润/本年销售收入
        if(new BigDecimal(ctProfitChart.getPcSales()).compareTo(BigDecimal.ZERO)!=0){
            BigDecimal ftNetProfitRate = new BigDecimal(ctProfitChart.getPcProfitBeforeDep())
                    .divide(new BigDecimal(ctProfitChart.getPcSales()),2, BigDecimal.ROUND_HALF_UP);
            gbCtFinancialTarget.setFtNetProfitRate(ftNetProfitRate);
        }else{
            gbCtFinancialTarget.setFtNetProfitRate(BigDecimal.ZERO);
        }

        //成本费用率: （本年营业外收支+本年折旧前利润）/(本年直接成本+管理费用+财务费用)
         BigDecimal costExpense = new BigDecimal(ctProfitChart.getPcDirectCost()+ctProfitChart.getPcTotal()+ctProfitChart.getPcFinanceFee());
        if(costExpense.compareTo(BigDecimal.ZERO)!=0){
            BigDecimal ftCostExpenseRate = new BigDecimal(ctProfitChart.getPcNonOperating()+ctProfitChart.getPcProfitBeforeDep())
                    .divide(costExpense,2, BigDecimal.ROUND_HALF_UP);
            gbCtFinancialTarget.setFtCostExpenseRate(ftCostExpenseRate);
        }else{
            gbCtFinancialTarget.setFtCostExpenseRate(BigDecimal.ZERO);
        }




        //资产报酬率: 2*本年支付利息前利润 /（本年资产总计+去年资产总计）
        //本年支付利息前利润
        Integer lxqlr = ctProfitChart.getPcProfitBeforeInterests();
        BigDecimal zczj=new BigDecimal(balance.getBsTotalAsset()+(lastYearBalance!=null?lastYearBalance.getBsTotalAsset():0));
        if(BigDecimal.ZERO.compareTo(zczj)!=0){
            BigDecimal ftReturnAssetsRate = new BigDecimal(2)
                    .multiply(new BigDecimal(lxqlr))
                    .divide( zczj,2, BigDecimal.ROUND_HALF_UP);
            gbCtFinancialTarget.setFtReturnAssetsRate(ftReturnAssetsRate);
        }else{
            gbCtFinancialTarget.setFtReturnAssetsRate(BigDecimal.ZERO);
        }



        //净资产收益率: 2*净利润/（去年所有者权益+今年所有者权益）
        BigDecimal returnEquity = new BigDecimal(balance.getBsTotalEquity()+(lastYearBalance!=null?lastYearBalance.getBsTotalEquity():0));
        if(returnEquity.compareTo(BigDecimal.ZERO)!=0){
            BigDecimal ftReturnEquityRate =  new BigDecimal(2)
                    .multiply(new BigDecimal(balance.getBsAnnualNetProfit()))
                    .divide(returnEquity,2, BigDecimal.ROUND_HALF_UP);
            gbCtFinancialTarget.setFtReturnEquityRate(ftReturnEquityRate);
        }else{
            gbCtFinancialTarget.setFtReturnEquityRate(BigDecimal.ZERO);
        }


        //营业收入增长率:  今年销售收入/去年销售收入 -1
        //去年利润表
        GbCtProfitChart lastYearProfitChar = ctProfitChartService.getSys(new GbCtProfitChart().setStudentId(studentId).setContestId(contestId).setPcDate((date/10) - 1));
        if(lastYearProfitChar!=null && new BigDecimal(lastYearProfitChar.getPcSales()).compareTo(BigDecimal.ZERO)!=0){
            BigDecimal ftRevenueGrowthRate = new BigDecimal(ctProfitChart.getPcSales())
                    .divide(new BigDecimal(lastYearProfitChar.getPcSales()),100, BigDecimal.ROUND_HALF_UP)
                    .subtract(new BigDecimal(1))
                    .setScale(2, BigDecimal.ROUND_HALF_UP)
                    ;
            gbCtFinancialTarget.setFtRevenueGrowthRate(ftRevenueGrowthRate);
        }else{
            gbCtFinancialTarget.setFtRevenueGrowthRate(BigDecimal.ZERO);
        }

        //资本保值增值率:  今年所有者权益/去年所有者权益
        BigDecimal lastBsTotalEquity = new BigDecimal(lastYearBalance!=null?lastYearBalance.getBsTotalEquity():0);
        if(lastBsTotalEquity.compareTo(BigDecimal.ZERO)!=0){
            BigDecimal ftAppreciationRate = new BigDecimal(balance.getBsTotalEquity())
                    .divide(lastBsTotalEquity,2, BigDecimal.ROUND_HALF_UP);
            gbCtFinancialTarget.setFtAppreciationRate(ftAppreciationRate);
        }else{
            gbCtFinancialTarget.setFtAppreciationRate(BigDecimal.ZERO);
        }


        //总资产增长率:   今年资产总计/去年资产总计 -1
        BigDecimal lastBsTotalAsset= new BigDecimal(lastYearBalance!=null?lastYearBalance.getBsTotalAsset():0);
        if(lastBsTotalAsset.compareTo(BigDecimal.ZERO)!=0){
            BigDecimal ftTotalGrowthRate = new BigDecimal(balance.getBsTotalAsset())
                    .divide(lastBsTotalAsset,100, BigDecimal.ROUND_HALF_UP)
                    .subtract(new BigDecimal(1))
                    .setScale(2, BigDecimal.ROUND_HALF_UP)
                    ;
            gbCtFinancialTarget.setFtTotalGrowthRate(ftTotalGrowthRate);
        }else{
            gbCtFinancialTarget.setFtTotalGrowthRate(BigDecimal.ZERO);
        }



        //库存周转率      2*今年直接成本/（去年原材料+去年在制品+去年库存+今年原材料+今年在制品+今年库存）
        BigDecimal inventorySum;
        if(lastYearBalance!=null){
            inventorySum = new BigDecimal(balance.getBsProductInProcess()+balance.getBsProduct()+balance.getBsMaterial()
                    +lastYearBalance.getBsProductInProcess()+lastYearBalance.getBsProduct()+lastYearBalance.getBsMaterial());
        }else{
            inventorySum = new BigDecimal(balance.getBsProductInProcess()+balance.getBsProduct()+balance.getBsMaterial());
        }
        if(inventorySum.compareTo(BigDecimal.ZERO)!=0){
            BigDecimal ftInventoryRate = new BigDecimal(2)
                    .multiply(new BigDecimal(ctProfitChart.getPcDirectCost()))
                    .divide(inventorySum,2, BigDecimal.ROUND_HALF_UP);
            gbCtFinancialTarget.setFtInventoryRate(ftInventoryRate);
        }else{
            gbCtFinancialTarget.setFtInventoryRate(BigDecimal.ZERO);

        }

        //库存周转天数    365/库存周转率 (先四舍五入两位小数)
        if(gbCtFinancialTarget.getFtInventoryRate().compareTo(BigDecimal.ZERO)!=0){
            BigDecimal ftInventoryDays = new BigDecimal(365)
                    .divide(gbCtFinancialTarget.getFtInventoryRate()
                            ,2, BigDecimal.ROUND_HALF_UP);
            gbCtFinancialTarget.setFtInventoryDays(ftInventoryDays);
        }else{
            gbCtFinancialTarget.setFtInventoryDays(BigDecimal.ZERO);

        }


        //应收账款周转率  2 * 今年销售收入/（去年应收账款+今年应收账款）
        BigDecimal receivable = new BigDecimal(lastYearBalance!=null?lastYearBalance.getBsReceivable():0)
                .add(new BigDecimal(balance.getBsReceivable()));
        if(receivable.compareTo(BigDecimal.ZERO)!=0){
            BigDecimal ftReceivableRate = new BigDecimal(2)
                    .multiply(new BigDecimal(ctProfitChart.getPcSales()))
                    .divide(receivable,2, BigDecimal.ROUND_HALF_UP);
            gbCtFinancialTarget.setFtReceivableRate(ftReceivableRate);

        }else{
            gbCtFinancialTarget.setFtReceivableRate(BigDecimal.ZERO);

        }

        //应收账款周转天数 365/应收账款周转率
        if(gbCtFinancialTarget.getFtReceivableRate().compareTo(BigDecimal.ZERO)!=0){
            BigDecimal ftReceivableDays = new BigDecimal(365)
                    .divide(gbCtFinancialTarget.getFtReceivableRate()
                            ,2, BigDecimal.ROUND_HALF_UP);
            gbCtFinancialTarget.setFtReceivableDays(ftReceivableDays);
        }else{
            gbCtFinancialTarget.setFtReceivableDays(BigDecimal.ZERO);
        }


        //现金周转期     存货周转天数+应收账款周转天数
        BigDecimal ftCashPeriod = gbCtFinancialTarget.getFtInventoryDays()
                .add(gbCtFinancialTarget.getFtReceivableDays())
                .setScale(2, BigDecimal.ROUND_HALF_UP);;
        gbCtFinancialTarget.setFtCashPeriod(ftCashPeriod);

        gbCtFinancialTargetService.save(gbCtFinancialTarget);
        return balance.getBsTotalEquity();
    }

    /**
     * 显示账单（应付、应收）
     * @param userId
     * @param contestId
     * @param type
     * @return
     */
    public R showFee(Integer userId, Integer contestId, Integer type, Integer currentTime) {
        QueryWrapper<GbCtFee> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("student_id", userId);
        queryWrapper.eq("contest_id", contestId);
        queryWrapper.eq("type", type);
        queryWrapper.orderByAsc("r_remain_date");
        List<GbCtFee> list = ctFeeMapper.selectList(queryWrapper);


        //计算贴现利率
        list.forEach(item->{
            //贴现率
            //计算剩余时间
            Integer remainDate = DateUtils.calSeasonInterval(currentTime,item.getPaymentDate());
            if(remainDate>0){
                List<GbCtGzDiscount> gbCtGzDiscounts = gbCtGzDiscountMapper.getGzDiscountByStudentIdAndContestId(contestId,remainDate);
                if( gbCtGzDiscounts!=null && gbCtGzDiscounts.size()>0){
                    GbCtGzDiscount gbCtGzDiscount = gbCtGzDiscounts.get(0);
                    item.setRate(gbCtGzDiscount.getDiscountInterest());
                }else{
                    item.setRate(new BigDecimal(0));
                }
            }else{
                item.setRate(new BigDecimal(0));
            }
        });

        Map<String, Object> resultMap = new HashMap<>(16);
        resultMap.put("list", list);
        return R.success(resultMap);
    }

    /**
     * 贴现
     * @param param
     * @param userId
     * @return
     */
    public R discount(GbDiscountParam param, Integer userId) {

        GbCtFee f = ctFeeService.getById(param.getId());
        if(param.getFee() > f.getRFee()){
            return ErrorEnum.DISCOUNT_ERROR.getR();
        }
        int cash = ctCashflowService.getCash(userId, param.getContestId());
        //计算剩余时间
        Integer remainDate = DateUtils.calSeasonInterval(param.getDate(),f.getPaymentDate());
        List<GbCtGzDiscount> gbCtGzDiscounts = gbCtGzDiscountMapper.getGzDiscountByStudentIdAndContestId(param.getContestId(),remainDate);
        if( gbCtGzDiscounts!=null && gbCtGzDiscounts.size()>0){
            GbCtGzDiscount gbCtGzDiscount = gbCtGzDiscounts.get(0);
            //贴息
            int  out = new BigDecimal(param.getFee()).multiply(gbCtGzDiscount.getDiscountInterest()).divide(new BigDecimal(100))
                    .setScale(0, RoundingMode.CEILING)
                    .intValue();
            int remain = cash + param.getFee() - out;
            //现金流量表
            GbCtCashflow ctCashflow = new GbCtCashflow();
            ctCashflow.setCDate(param.getDate())
                    .setCSurplus(remain)
                    .setCIn(param.getFee())
                    .setCOut(out)
                    .setCAction(CashFlowActionEnum.DISCOUNT.getAction())
                    .setContestId(param.getContestId())
                    .setStudentId(userId)
                    .setCComment("应收日期:"+(f.getPaymentDate()/10)+"年"+(f.getPaymentDate()%10)+"季"+",贴现"+param.getFee()+"元");
            ctCashflowService.save(ctCashflow);
            //更新应收款数据
            int remianAmount = f.getRFee() - param.getFee();
            if(remianAmount>0){
                f.setRFee(f.getRFee() - param.getFee());
                ctFeeService.updateById(f);
            }else{
                ctFeeService.removeById(f);
            }
        }

        //计算报表,判断是否破产
        isBankruptcy(userId,param.getContestId(),param.getDate());

        return R.success();
    }

    /**
     * 付款
     * @return
     */
    public R payment(Integer userId,Integer contestId,Integer feeId,Integer currentTime){
        GbCtFee f = ctFeeService.getById(feeId);
        int cash = ctCashflowService.getCash(userId, contestId);
        if(cash<f.getRFee()){
            return ErrorEnum.CASH_NOT_ENOUGH.getR();
        }
        paymentAndCashFlow(f,userId,contestId,currentTime,cash);

        //计算报表,判断是否破产
        isBankruptcy(userId,contestId,currentTime);

        return R.success();
    }


    //应付款并产生现金流量表
    public void paymentAndCashFlow(GbCtFee f ,Integer userId,Integer contestId ,Integer currentTime,Integer cash){
        cash = cash - f.getRFee();
        //现金流量表
        GbCtCashflow ctCashflow = new GbCtCashflow();
        ctCashflow.setCDate(currentTime)
                .setCSurplus(cash)
                .setCIn(0)
                .setCOut(f.getRFee())
                .setCAction(CashFlowActionEnum.PAY_MATERIAL.getAction())
                .setContestId(contestId)
                .setStudentId(userId)
                .setCComment(f.getRemarks()+f.getRFee()+"元");
        ctCashflowService.save(ctCashflow);
        //删除应付款数据
        ctFeeService.removeById(f.getFeeId());
    }



    /**
     * 紧急采购原材料
     * @param userId
     * @param param
     * @return
     */
    public R buyMaterial(Integer userId, GbUrgentBuyParam param) {
        //判断现金是否足够
        int cash = ctCashflowService.getCash(userId, param.getContestId());
        // 紧急采购成本
        int cost = (param.getUrgentPrice() == null ? 0:param.getUrgentPrice()) * (param.getNum()==null? 0:param.getNum());
        if (cost == 0) {
            return R.success();
        }
        int remain = cash - cost;
        if (remain < 0) {
            return ErrorEnum.CASH_NOT_ENOUGH.getR();
        }
        GbCtGzMaterial gbCtGzMaterial = gbCtGzMaterialMapper.selectById(param.getId());
        if(null == gbCtGzMaterial){
            return ErrorEnum.MATERIAL_NOT_EXIST.getR();
        }

        //记录现金流量表
        GbCtCashflow ctCashflow = new GbCtCashflow();
        ctCashflow.setCDate(param.getDate())
                .setCSurplus(remain)
                .setCIn(0)
                .setCOut(cost)
                .setCAction(CashFlowActionEnum.URGENT_MATERIAL_COST.getAction())
                .setContestId(param.getContestId())
                .setStudentId(userId)
                .setCComment("原材料紧急采购成本" + cost + "元");
        ctCashflowService.save(ctCashflow);


        //采购入库更新库存
        GbCtKcMaterial gbCtKcMaterial = new GbCtKcMaterial();
        gbCtKcMaterial.setStudentId(userId)
                .setContestId(param.getContestId())
                .setImNum(param.getNum())
                .setMaterialPrice(param.getUrgentPrice())
                .setInInventoryDate(param.getDate())
                .setImCmId(param.getId())
                .setMaterialName(gbCtGzMaterial.getCnName());
        ctKcMaterialService.save(gbCtKcMaterial);

        //计算报表,判断是否破产
        isBankruptcy(userId,param.getContestId(),param.getDate());


        return R.success();
    }

    /**
     * 紧急采购产品
     * @param userId
     * @param param
     * @return
     */
    @Autowired
    private GbCtGzProductMapper gbCtGzProductMapper;

    public R buyProduct(Integer userId, GbUrgentBuyParam param) {
        //判断现金是否足够
        int cash = ctCashflowService.getCash(userId, param.getContestId());
        // 紧急采购成本
        int cost = (param.getUrgentPrice() == null ? 0:param.getUrgentPrice()) * (param.getNum()==null? 0:param.getNum());
        if (cost == 0) {
            return R.success();
        }
        int remain = cash - cost;
        if (remain < 0) {
            return ErrorEnum.CASH_NOT_ENOUGH.getR();
        }
        GbCtGzProduct gbCtGzProduct = gbCtGzProductMapper.selectById(param.getId());
        if(null == gbCtGzProduct){
            return ErrorEnum.PRODUCT_NOT_EXIST.getR();
        }

        //记录现金流量表
        GbCtCashflow ctCashflow = new GbCtCashflow();
        ctCashflow.setCDate(param.getDate())
                .setCSurplus(remain)
                .setCIn(0)
                .setCOut(cost)
                .setCAction(CashFlowActionEnum.URGENT_PRODUCT_COST.getAction())
                .setContestId(param.getContestId())
                .setStudentId(userId)
                .setCComment("产成品紧急采购成本" + cost + "元");
        ctCashflowService.save(ctCashflow);


        //采购入库更新库存
        List<GbCtKcProduct> list = new ArrayList<>();
        for (int i = 0; i < param.getNum(); i++) {
            GbCtKcProduct gbCtKcProduct = new GbCtKcProduct();
            gbCtKcProduct.setStudentId(userId)
                    .setContestId(param.getContestId())
                    .setIpNum(1)
                    .setRealCost(param.getUrgentPrice())
                    .setIsInventory(GbCtKcProduct.IS_IN)
                    .setInventoryDate(param.getDate())
                    .setIpCpId(param.getId());
            list.add(gbCtKcProduct);
        }
        ctKcProductService.saveOrUpdateBatch(list);

        //计算报表,判断是否破产
        isBankruptcy(userId,param.getContestId(),param.getDate());

        return R.success();
    }

    /**
     * 判断是否破产
     * @param studentId
     * @param contestId
     * @param currentTime
     */
    public void isBankruptcy(Integer studentId,Integer contestId,Integer currentTime){
        //获取当前的现金流量表余额
        int cash = ctCashflowService.getCash(studentId,contestId);
        //实时计算四张报表
        //计算四张报表
        Integer bsTotalEquity = calBsTotalEquity(studentId,contestId,currentTime);

        //获取之前的破产状态
        GbContestStudent gbContestStudent = gbContestStudentMapper.selectOne(new LambdaQueryWrapper<GbContestStudent>()
                .eq(GbContestStudent::getContestId,contestId)
                .eq(GbContestStudent::getStudentId,studentId)
        );
        //判断当前是否处于破产状态
        Integer isPc=gbContestStudent.getIsBankruptcy();

        // 所有者权益为负后，自动结束比赛
        if (bsTotalEquity < 0 || cash < 0) {
            //如果之前没有破产
            if(isPc==0){
                gbContestStudentMapper.updateById(new GbContestStudent()
                        .setId(gbContestStudent.getId())
                        .setIsBankruptcy(1)
                );
            }
        }
        //没有破产
        else{
            //如果之前已经破产，则将其激活
            if(isPc==1){
                gbContestStudentMapper.updateById(new GbContestStudent()
                        .setId(gbContestStudent.getId())
                        .setIsBankruptcy(0)
                );
            }
        }
    }


    public R listSellKc(Integer contestId, Integer userId) {
//        List<MaterialVo> kcMaterialList = ctKcMaterialService.listSellKc(contestId, userId);
        List<GbProductVo> kcProductList = ctKcProductService.listSellKc(contestId, userId);
        Map<String, Object> resultMap = new HashMap<>();
//        resultMap.put("kcMaterialList", kcMaterialList);
        resultMap.put("kcProductList", kcProductList);
        return R.success(resultMap);
    }

    @Autowired
    private GbCtKcMaterialMapper gbCtKcMaterialMapper;

    public R sellMaterial(Integer userId, GbSellMaterialParam param) {

        //获取
        GbCtGzCs ctGzCs = ctGzCsService.getByContestId(param.getContestId());
        List<GbCtKcMaterial> materials = gbCtKcMaterialMapper.listByPriceAndDateAndCmId(param.getContestId(), userId,param.getImCmId(), param.getMaterialPrice(), param.getInInventoryDate());
        int kcSum = materials.stream().mapToInt(GbCtKcMaterial::getImNum).sum();
        if(param.getImNum() > kcSum){
            return ErrorEnum.MATERIAL_NOT_ENOUGH.getR();
        }

        int sellSum = param.getImNum();
        int sellTotal = 0;//出售总价
        int directCost = 0;// 本来的本钱
        for (GbCtKcMaterial material : materials) {
            Integer imNum = material.getImNum();
            if(sellSum < imNum ){//更新数量，结束
                imNum = imNum - sellSum;
                material.setImNum(imNum);
                sellTotal += sellSum * material.getMaterialPrice()*ctGzCs.getInventoryDiscountMaterial()/100;
                directCost += sellSum * material.getMaterialPrice();
                gbCtKcMaterialMapper.updateById(material);
                break;
            }else {//删除
                sellSum = sellSum - material.getImNum();
                sellTotal += material.getImNum() * material.getMaterialPrice()*ctGzCs.getInventoryDiscountMaterial()/100;
                directCost += material.getImNum() * material.getMaterialPrice();
                gbCtKcMaterialMapper.deleteById(material);
                if(sellSum == 0){
                    break;
                }
            }
        }

        int loss = directCost - sellTotal;
        int cash = ctCashflowService.getCash(userId, param.getContestId());

        //现金流量表
        GbCtCashflow ctCashflow = new GbCtCashflow();
        ctCashflow.setCDate(param.getDate())
                .setCSurplus(cash + sellTotal)
                .setCIn(directCost)
                .setCOut(loss)
                .setCAction(CashFlowActionEnum.SELL_MATERIAL.getAction())
                .setContestId(param.getContestId())
                .setStudentId(userId)
                .setCComment("出售原材料");
        ctCashflowService.save(ctCashflow);

        //计算报表,判断是否破产
        isBankruptcy(userId,param.getContestId(),param.getDate());

        return R.success();
    }

    @Autowired
    private GbCtKcProductMapper ctKcProductMapper;

    public R sellProduct(Integer userId, GbSellProductParam param) {

        //获取
        GbCtGzCs ctGzCs = ctGzCsService.getByContestId(param.getContestId());

        List<GbCtKcProduct> kcProductList = ctKcProductMapper.getKcProductByPriceAndInDate(param.getContestId(),userId,param.getIpCpId(),param.getInventoryDate(),param.getRealCost());
        if(param.getNum() > kcProductList.size()){
            return ErrorEnum.PRODUCT_NOT_ENOUGH.getR();
        }

        //出售总价
        int sellTotal = param.getNum() * param.getRealCost() * ctGzCs.getInventoryDiscountProduct() / 100;

        // 本来的本钱
        int directCost = param.getNum() * param.getRealCost();

        int loss = directCost - sellTotal;
        int cash = ctCashflowService.getCash(userId, param.getContestId());

        //现金流量表
        GbCtCashflow ctCashflow = new GbCtCashflow();
        ctCashflow.setCDate(param.getDate())
                .setCSurplus(cash + sellTotal)
                .setCIn(directCost)
                .setCOut(loss)
                .setCAction(CashFlowActionEnum.SELL_PRODUCT.getAction())
                .setContestId(param.getContestId())
                .setStudentId(userId)
                .setCComment("出售产品");
        ctCashflowService.save(ctCashflow);

        for (int i = 0; i < kcProductList.size(); i++) {
            if(i >= param.getNum()){
                break;
            }
            GbCtKcProduct gbCtKcProduct = kcProductList.get(i);
            ctKcProductService.removeById(gbCtKcProduct);
        }


        //计算报表,判断是否破产
        isBankruptcy(userId,param.getContestId(),param.getDate());

        return R.success();
    }



    public R xzorder(Integer studentId, Integer contestId, Integer date) {
        List<GbCtXzOrder> list = ctXzOrderMapper.getList(contestId, studentId);
        list.forEach(p -> {
            String status = "-";
            if (p.getCommitDate() != null && p.getCommitDate() > 0) {
                status = "已交货";
            } else if ((date / 10 == p.getDate() && date % 10 > p.getDeliveryDate()) || ((date / 10) > p.getDate())) {
                status = "已违约";
            } else {
                status = "未到期";
            }
            p.setStatus(status);
        });
        Map<String, Object> resultMap = new HashMap<>(16);
        resultMap.put("list", list);
        return R.success(resultMap);
    }

    public R marketList(Integer userId, Integer contestId) {
        List<GbMarketVo> list = ctYfMarketService.listYfMarket(userId, contestId);
        Map<String, Object> resultMap = new HashMap<>(16);
        resultMap.put("list", list);
        return R.success(resultMap);
    }

    public R marketYf(Integer userId, GbMarketYfParam param) {
        List<GbMarketVo> list = ctYfMarketService.listYfMarket(userId, param.getContestId());
        Map<Integer, Integer> map = new HashMap<>(16);
        for (GbMarketVo m : list) {
            map.put(m.getMarketId(), m.getCmDevelopFee());
        }
        int cash = ctCashflowService.getCash(userId, param.getContestId());
        GbCtYfMarket yf = ctYfMarketService.getById(param.getMarketId());
        GbCtGzMarket gz = ctGzMarketMapper.selectById(yf.getDmCmId());

        if (GbCtYfMarket.SC_YF_ING.equals(yf.getDmState()) || GbCtYfMarket.SC_YF_FINISH.equals(yf.getDmState())) {
            return R.error(CodeEnum.OTHER_ERROR.getCode(), "无需重复研发");
        }
        int sum = gz.getCmDevelopFee();
        int remain = cash - sum;
        if (remain < 0) {
            return ErrorEnum.CASH_NOT_ENOUGH.getR();
        }
        GbCtCashflow ctCashflow = new GbCtCashflow();
        ctCashflow.setCDate(param.getDate())
                .setCSurplus(remain)
                .setCIn(0)
                .setCOut(sum)
                .setCAction(CashFlowActionEnum.DEVELOP_MARKET.getAction())
                .setContestId(param.getContestId())
                .setStudentId(userId)
                .setCComment("扣减市场"+gz.getCmName()+"开拓" + sum + "元");
        ctCashflowService.save(ctCashflow);

        //更新市场
        Integer cmDevelopDate = gz.getCmDevelopDate();
        Integer fininshDate = DateUtils.addYearAndSeasonTime(param.getDate(), cmDevelopDate);
        if(cmDevelopDate!= null && cmDevelopDate>0){
            yf.setDmState(GbCtYfMarket.SC_YF_ING);
        }else {
            yf.setDmState(GbCtYfMarket.SC_YF_FINISH);
        }
        yf.setDmStartDate(param.getDate());//申请时间
        yf.setDmFinishDate(fininshDate);//结束时间
        ctYfMarketService.updateById(yf);

        //计算报表,判断是否破产
        isBankruptcy(userId,param.getContestId(),param.getDate());

        return R.success();
    }

    /**
     * 获取社会责任分
     */
    public R  getCorporateScore(Integer userId, Integer contestId){
        int score = gbCtCorporateMapper.getScore(userId, contestId);
        return R.success().put("data",score);
    }

    /**
     * 注入资本
     * @return
     */
    public R addCapital(GbCapitalParam param){
        JSONArray array = new JSONArray();
        for (GbCapitalParam.MaterialIdAndMaterialNum obj : param.getMaterialIdAndMaterialNumList()) {
            JSONObject js = new JSONObject();
            js.put("materialId",obj.getMaterialId());
            js.put("materialNum",obj.getMaterialNum());
            js.put("materialName",obj.getCnName());
            array.add(js);
        }
        GbContestStudent gbContestStudent = gbContestStudentService.get(param.getContestId(), param.getStudentId());
        int caDate = gbContestStudent.getDate() / 10;
        GbCtCapital ctCapital = new GbCtCapital();
        ctCapital.setContestId(param.getContestId())
                .setStudentId(param.getStudentId())
                .setAddCapital(param.getAddCapital())
                .setCaDate(caDate)
                .setMaterialInfo(array.toString());
        gbCtCapitalMapper.insert(ctCapital);

        int cash = ctCashflowService.getCash(param.getStudentId(), param.getContestId());
        //入库存
        Integer materialCash=0;
        for (GbCapitalParam.MaterialIdAndMaterialNum obj : param.getMaterialIdAndMaterialNumList()) {
            GbCtGzMaterial gbCtGzMaterial = gbCtGzMaterialMapper.selectById(obj.getMaterialId());
            GbCtKcMaterial kc = new GbCtKcMaterial();
            kc.setStudentId(param.getStudentId())//学生id
                    .setContestId(param.getContestId())//竞赛id
                    .setImCmId(obj.getMaterialId())//原料商店产品id
                    .setImNum(obj.getMaterialNum())//数量
                    .setMaterialName(gbCtGzMaterial.getCnName())//原料名称
                    .setInInventoryDate(caDate)//入库时间
                    .setMaterialPrice(gbCtGzMaterial.getCmBuyFee());//价格
            gbCtKcMaterialMapper.insert(kc);
            //注入原材料，同步增加股东资本
            materialCash+= obj.getMaterialNum()*gbCtGzMaterial.getCmBuyFee();
        }

        if(param.getAddCapital() != 0 ){
            // 改变现金流量表
            GbCtCashflow ctCashflow = new GbCtCashflow();
            ctCashflow.setCDate(caDate)
                    .setCSurplus(cash + param.getAddCapital())
                    .setCIn(param.getAddCapital())
                    .setCOut(0)
                    .setCAction(CashFlowActionEnum.ADD_CAPITAL.getAction())
                    .setContestId(param.getContestId())
                    .setStudentId(param.getStudentId())
                    .setCComment("注入资本" + param.getAddCapital() + "元");
            ctCashflowService.save(ctCashflow);

            //修改第0年股东资本
            // 股东资本 = 第0年股东资本
            GbCtBalance ctBalance = ctBalanceService.get(new GbCtBalance().setStudentId(param.getStudentId()).setContestId(param.getContestId()).setBsYear(0));
            //原股东资本 + 注资金额 + 注入原材料(转化成对应金额)
            Integer newInitCash = ctBalance.getBsEquity()+param.getAddCapital()+materialCash;
            ctBalance
                    .setBsCash(newInitCash)
                    .setBsTotalCurrentAsset(newInitCash)
                    .setBsTotalAsset(newInitCash)
                    .setBsEquity(newInitCash)
                    .setBsTotalEquity(newInitCash)
                    .setBsTotal(newInitCash);
            ctBalanceService.updateById(ctBalance);

            //重新计算报表并判断是否破产
            isBankruptcy(param.getStudentId(), param.getContestId(),caDate);
        }


        return R.success();

    }


    public R getOtherInfo(HttpServletResponse response, Integer userId, Integer otherStudentId, Integer contestId, Integer date, Integer amount) {
        //扣取现金
        int cash = ctCashflowService.getCash(userId, contestId);
        int remain = cash - amount;
        if (remain < 0) {
            return ErrorEnum.CASH_NOT_ENOUGH.getR();
        }
        GbCtCashflow ctCashflow = new GbCtCashflow();
        ctCashflow.setCDate(date)
                .setCSurplus(remain)
                .setCIn(0)
                .setCOut(amount)
                .setCAction(CashFlowActionEnum.INFO_OTHER.getAction())
                .setContestId(contestId)
                .setStudentId(userId)
                .setCComment("扣取数据咨询费" + amount + "元");
        ctCashflowService.save(ctCashflow);
        //下载
        gbDownloadBiz.downloadResult(response,contestId,otherStudentId,"数据咨询");
        return R.success();
    }
}
