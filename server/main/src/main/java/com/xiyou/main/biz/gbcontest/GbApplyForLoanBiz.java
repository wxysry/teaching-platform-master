package com.xiyou.main.biz.gbcontest;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.xiyou.common.enums.CodeEnum;
import com.xiyou.common.utils.R;
import com.xiyou.main.dao.gbcontest.*;
import com.xiyou.main.entity.gbcontest.*;
import com.xiyou.main.entity.gbcontest.GbCtBankLoan;
import com.xiyou.main.entity.gbcontest.GbCtCashflow;
import com.xiyou.main.entity.gbcontest.GbCtGzLoan;
import com.xiyou.main.entity.newcontest.*;
import com.xiyou.main.enums.CashFlowActionEnum;
import com.xiyou.main.enums.ErrorEnum;
import com.xiyou.main.service.gbcontest.GbCtBalanceService;
import com.xiyou.main.service.gbcontest.GbCtBankLoanService;
import com.xiyou.main.service.gbcontest.GbCtCashflowService;
import com.xiyou.main.utils.DateUtils;
import com.xiyou.main.vo.gbcontest.GbCashFolowEntity;
import com.xiyou.main.vo.gbcontest.GbMaterialVo;
import com.xiyou.main.entity.gbcontest.GbRePaymentInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import java.math.BigDecimal;
import java.util.*;

/**
 *
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class GbApplyForLoanBiz {

    @Autowired
    GbCtCashflowService gbCtCashflowService;
    @Autowired
    GbCtBankLoanService gbCtBankLoanService;
    @Autowired
    GbCtBalanceService gbCtBalanceService;
    @Autowired
    GbCtBankLoanMapper gbCtBankLoanMapper;
    @Autowired
    GbCtGzLoanMapper gbCtGzLoanMapper;
    @Autowired
    GbCtGzCsMapper gbCtGzCsMapper;
    @Autowired
    GbActionBiz gbActionBiz;
    @Autowired
    private GbCtWorkerMapper gbCtWorkerMapper;
    @Autowired
    private GbRePaymentInfoMapper gbRePaymentInfoMapper;

    /**
     * 申请贷款-获取最大贷款额度&需贷款年限
     *
     * @param cashFolowEntity
     * @return
     */
    public R getMaxLoanAmountInfo(GbCashFolowEntity cashFolowEntity) {
        return R.success().put("data", gbCtBalanceService.getAllMaxLoanAmount(cashFolowEntity));
    }

    /**
     * 申请贷款-确认
     */
    public R applyForLoanCommit( GbCashFolowEntity cashFolowEntity) {
        //获取融资规则
        GbCtGzLoan ctGzLoan = gbCtGzLoanMapper.selectById(cashFolowEntity.getGzLoanId());
        Integer loanTime = ctGzLoan.getLoanTime();
        if (loanTime == null) {
            return R.error(CodeEnum.OTHER_ERROR.getCode(), "该规则中没有贷款时间");
        }
        Integer repaymentDate = DateUtils.addYearAndSeasonTime(cashFolowEntity.getCurrentTime() , loanTime);//贷款时间
        Integer loanAmount = cashFolowEntity.getLoanAmount();

        Integer maxLoanAmount = gbCtBalanceService.getMaxLoanAmount(cashFolowEntity);
        if (loanAmount == null  || loanAmount > maxLoanAmount) {
            return ErrorEnum.LOAN_ERROR.getR();
        }

        //先判断当前季度是否已经有该贷款记录
        GbCtBankLoan ctBankLoan= gbCtBankLoanService.getOne(new LambdaQueryWrapper<GbCtBankLoan>()
                .eq(GbCtBankLoan::getContestId,cashFolowEntity.getContestId())
                .eq(GbCtBankLoan::getStudentId,cashFolowEntity.getStudentId())
                .eq(GbCtBankLoan::getBlName,ctGzLoan.getLoanName())
                .eq(GbCtBankLoan::getBlAddTime,cashFolowEntity.getCurrentTime())
                .last("limit 1")
        );
        if(ctBankLoan==null){
            //每期利息
            BigDecimal interest = BigDecimal.valueOf(loanAmount).multiply(ctGzLoan.getLoanRate()).divide(BigDecimal.valueOf(100),0, BigDecimal.ROUND_UP);
            BigDecimal allInterest = new BigDecimal(0);
            if("2".equals(ctGzLoan.getLoanRepayment())){
                allInterest = interest.multiply(new BigDecimal(loanTime));
            }else {
                allInterest = interest;
            }

            //插入贷款记录
            GbCtBankLoan loan = new GbCtBankLoan();
            loan.setContestId(cashFolowEntity.getContestId())
                    .setStudentId(cashFolowEntity.getStudentId())
                    .setBlType((ctGzLoan.getLoanTime()!=null && ctGzLoan.getLoanTime()> 4 )? NewCtBankLoan.LONG_LOAN:NewCtBankLoan.SHORT_LOAN)//贷款方式
                    .setBlName(ctGzLoan.getLoanName())//贷款名称
                    .setBlAddTime(cashFolowEntity.getCurrentTime())//贷款时间
                    .setBlFee(loanAmount)//贷款金额
                    .setRepaymentType(ctGzLoan.getLoanRepayment())//还款方式
                    .setBlRepaymentDate(repaymentDate)//还款时间
                    .setRate(ctGzLoan.getLoanRate())//利率
                    .setInterest(allInterest)//利息
                    .setBlRemainTime(ctGzLoan.getLoanTime());//贷款时长
            gbCtBankLoanService.save(loan);

            //生成还款信息
            if("2".equals(ctGzLoan.getLoanRepayment())) {
                for (int i = 1; i <= ctGzLoan.getLoanTime(); i++) {
                    Integer payTime = DateUtils.addYearAndSeasonTime(cashFolowEntity.getCurrentTime(), i);
                    GbRePaymentInfo info = new GbRePaymentInfo();
                    info.setAmount(interest)
                            .setBlAddTime(loan.getBlAddTime())
                            .setPayment(CashFlowActionEnum.INTEREST_I.getAction())//每季付息-利息
                            .setContestId(cashFolowEntity.getContestId())
                            .setStudentId(cashFolowEntity.getStudentId())
                            .setLoanId(loan.getBlId())
                            .setPayTime(payTime)//缴费时间
                            .setDescription("利息");//说明
                    gbRePaymentInfoMapper.insert(info);
                    if (i == ctGzLoan.getLoanTime()) {
                        GbRePaymentInfo infoB = new GbRePaymentInfo();
                        infoB.setAmount(new BigDecimal(loanAmount))
                                .setBlAddTime(loan.getBlAddTime())
                                .setPayment(CashFlowActionEnum.INTEREST_R.getAction())//每季付息-本金
                                .setContestId(cashFolowEntity.getContestId())
                                .setStudentId(cashFolowEntity.getStudentId())
                                .setLoanId(loan.getBlId())
                                .setPayTime(payTime)//缴费时间
                                .setDescription("本金");//说明本息同还
                        gbRePaymentInfoMapper.insert(infoB);
                    }
                }
            }else {
                Integer payTime = DateUtils.addYearAndSeasonTime(cashFolowEntity.getCurrentTime(), ctGzLoan.getLoanTime());
                GbRePaymentInfo info1 = new GbRePaymentInfo();
                info1.setAmount(interest)
                        .setBlAddTime(loan.getBlAddTime())
                        .setPayment(CashFlowActionEnum.REPAYMENT_INTEREST_I.getAction())//本息同还-利息
                        .setContestId(cashFolowEntity.getContestId())
                        .setStudentId(cashFolowEntity.getStudentId())
                        .setLoanId(loan.getBlId())
                        .setPayTime(payTime)//缴费时间
                        .setDescription("利息");//说明
                gbRePaymentInfoMapper.insert(info1);

                GbRePaymentInfo info2 = new GbRePaymentInfo();
                info2.setAmount(new BigDecimal(loanAmount))
                        .setBlAddTime(loan.getBlAddTime())
                        .setPayment(CashFlowActionEnum.REPAYMENT_INTEREST_R.getAction())//本息同还-本金
                        .setContestId(cashFolowEntity.getContestId())
                        .setStudentId(cashFolowEntity.getStudentId())
                        .setLoanId(loan.getBlId())
                        .setPayTime(payTime)//缴费时间
                        .setDescription("本金");//说明
                gbRePaymentInfoMapper.insert(info2);
            }

        }
        //本季已贷款同种类型的款项
        else{
            //获取总的贷款金额
            Integer allLoanAmount = cashFolowEntity.getLoanAmount()+ctBankLoan.getBlFee();

            //每期利息
            BigDecimal interest = BigDecimal.valueOf(allLoanAmount)
                    .multiply(ctGzLoan.getLoanRate())
                    .divide(BigDecimal.valueOf(100),0, BigDecimal.ROUND_UP);
            BigDecimal allInterest = new BigDecimal(0);
            if("2".equals(ctGzLoan.getLoanRepayment())){
                allInterest = interest.multiply(new BigDecimal(loanTime));
            }else {
                allInterest = interest;
            }

            //更新贷款记录表
            ctBankLoan.setBlFee(allLoanAmount)//贷款金额
                    .setInterest(allInterest);//利息
            gbCtBankLoanService.updateById(ctBankLoan);

            //生成还款信息
            //2代表每季付息-最后本息偿还
            if("2".equals(ctGzLoan.getLoanRepayment())) {
                for (int i = 1; i <= ctGzLoan.getLoanTime(); i++) {
                    Integer payTime = DateUtils.addYearAndSeasonTime(cashFolowEntity.getCurrentTime(), i);
                    GbRePaymentInfo info = gbRePaymentInfoMapper.selectOne(new LambdaQueryWrapper<GbRePaymentInfo>()
                            .eq(GbRePaymentInfo::getContestId,cashFolowEntity.getContestId())
                            .eq(GbRePaymentInfo::getStudentId,cashFolowEntity.getStudentId())
                            .eq(GbRePaymentInfo::getBlAddTime,ctBankLoan.getBlAddTime())
                            .eq(GbRePaymentInfo::getPayment,CashFlowActionEnum.INTEREST_I.getAction()) //每季付息-利息
                            .eq(GbRePaymentInfo::getLoanId,ctBankLoan.getBlId())
                            .eq(GbRePaymentInfo::getPayTime,payTime)
                            .last("limit 1")
                    );

                    info.setAmount(interest);
                    gbRePaymentInfoMapper.updateById(info);
//

                    //最后一季本息同还
                    if (i == ctGzLoan.getLoanTime()) {
                        GbRePaymentInfo infoB = gbRePaymentInfoMapper.selectOne(new LambdaQueryWrapper<GbRePaymentInfo>()
                                .eq(GbRePaymentInfo::getContestId,cashFolowEntity.getContestId())
                                .eq(GbRePaymentInfo::getStudentId,cashFolowEntity.getStudentId())
                                .eq(GbRePaymentInfo::getBlAddTime,ctBankLoan.getBlAddTime())
                                .eq(GbRePaymentInfo::getPayment,CashFlowActionEnum.INTEREST_R.getAction()) //每季付息-本金
                                .eq(GbRePaymentInfo::getLoanId,ctBankLoan.getBlId())
                                .eq(GbRePaymentInfo::getPayTime,payTime)
                                .last("limit 1")
                        );
                        infoB.setAmount(new BigDecimal(allLoanAmount));
                        gbRePaymentInfoMapper.updateById(infoB);
                    }
                }
            }
            //代表本息同还
            else {
                Integer payTime = DateUtils.addYearAndSeasonTime(cashFolowEntity.getCurrentTime(), ctGzLoan.getLoanTime());
                GbRePaymentInfo info1 = gbRePaymentInfoMapper.selectOne(new LambdaQueryWrapper<GbRePaymentInfo>()
                        .eq(GbRePaymentInfo::getContestId,cashFolowEntity.getContestId())
                        .eq(GbRePaymentInfo::getStudentId,cashFolowEntity.getStudentId())
                        .eq(GbRePaymentInfo::getBlAddTime,ctBankLoan.getBlAddTime())
                        .eq(GbRePaymentInfo::getPayment,CashFlowActionEnum.REPAYMENT_INTEREST_I.getAction()) //本息同还-利息
                        .eq(GbRePaymentInfo::getLoanId,ctBankLoan.getBlId())
                        .eq(GbRePaymentInfo::getPayTime,payTime)
                        .last("limit 1")
                );
                info1.setAmount(interest);
                gbRePaymentInfoMapper.updateById(info1);



                GbRePaymentInfo info2 =  gbRePaymentInfoMapper.selectOne(new LambdaQueryWrapper<GbRePaymentInfo>()
                        .eq(GbRePaymentInfo::getContestId,cashFolowEntity.getContestId())
                        .eq(GbRePaymentInfo::getStudentId,cashFolowEntity.getStudentId())
                        .eq(GbRePaymentInfo::getBlAddTime,ctBankLoan.getBlAddTime())
                        .eq(GbRePaymentInfo::getPayment,CashFlowActionEnum.REPAYMENT_INTEREST_R.getAction()) //本息同还-本金
                        .eq(GbRePaymentInfo::getLoanId,ctBankLoan.getBlId())
                        .eq(GbRePaymentInfo::getPayTime,payTime)
                        .last("limit 1")
                );
                info2.setAmount(new BigDecimal(allLoanAmount));
                gbRePaymentInfoMapper.updateById(info2);
            }
        }



        //插入现金交易记录
        Integer cash = gbCtCashflowService.getCash(cashFolowEntity.getStudentId(), cashFolowEntity.getContestId());
        GbCtCashflow ctCashflow = new GbCtCashflow();
        ctCashflow.setStudentId(cashFolowEntity.getStudentId())
                .setContestId(cashFolowEntity.getContestId())
                .setCIn(loanAmount)
                .setCOut(0)
                .setCDate(cashFolowEntity.getCurrentTime())
                .setCSurplus(cash + loanAmount)
                .setCComment("贷款" + loanAmount + "元")
                .setCAction((ctGzLoan.getLoanTime()!=null && ctGzLoan.getLoanTime()> 4)? CashFlowActionEnum.LONG_LOAN.getAction():CashFlowActionEnum.SHORT_LOAN.getAction());
        gbCtCashflowService.save(ctCashflow);
        //计算报表
        Integer bsTotalEquity = gbActionBiz.calBsTotalEquity(cashFolowEntity.getStudentId(),cashFolowEntity.getContestId(),cashFolowEntity.getCurrentTime());
        if (bsTotalEquity < 0) {
            return R.error(7000, "当前所有者权益为负，你已破产，模拟竞赛结束");
        }

        return R.success();
    }

    /**
     * 获取融资（贷款）信息
     * @param contestId 竞赛ID
     * @param studentId 学生ID
     * @return
     */
    public R getLoanBankInfo(Integer contestId,Integer studentId){
        List<GbCtBankLoan> list = gbCtBankLoanMapper.getList(contestId, studentId);
        return R.success().put("list",list);
    }

    /**
     * 获取融资（贷款）规则
     * @param contestId 竞赛ID
     * @return
     */
    public R getLoanGz(Integer contestId){
        List<GbCtGzLoan> gbCtGzLoans = gbCtGzLoanMapper.listCtGzLoan(contestId);
        return R.success().put("data",gbCtGzLoans);
    }


    /**
     * 生成管理费信息（缴费了的就返回null）
     * @param contestId
     * @param studentId
     * @param currentTime
     * @return
     */
    public GbRePaymentInfo getOverhaul(Integer contestId,Integer studentId ,Integer currentTime ){
        GbRePaymentInfo overhaulInfo = gbRePaymentInfoMapper.getOverhaul(contestId, studentId, currentTime);
        if(overhaulInfo == null){
            GbCtGzCs ctGz = gbCtGzCsMapper.getByContestId(contestId);
            int overhaul = ctGz.getOverhaul();//获取管理费
            GbRePaymentInfo info = new GbRePaymentInfo();
            info.setAmount(new BigDecimal(overhaul))
                    .setStudentId(studentId)
                    .setContestId(contestId)
                    .setPayment("管理费")
                    .setBlAddTime(currentTime)
                    .setDescription("每季"+overhaul+"元");
            gbRePaymentInfoMapper.insert(info);
            return info;
        }else if(overhaulInfo.getPayTime() == null || overhaulInfo.getPayTime() == 0){
            return overhaulInfo;
        }
        return null;
    }

    /**
     * 生成管理费信息（缴费了的就返回null）
     * @param contestId
     * @param studentId
     * @param currentTime
     * @return
     */
    public void payOverhaul(Integer contestId,Integer studentId ,Integer currentTime){
        GbRePaymentInfo gbRePaymentInfo = getOverhaul(contestId, studentId, currentTime);
        if(gbRePaymentInfo == null){
            return ;
        }
        Integer overhaul = gbRePaymentInfo.getAmount().intValue();
        //获取现金
        Integer cash = gbCtCashflowService.getCash(studentId, contestId);
        GbCtCashflow gbCtCashflow = new GbCtCashflow();
        gbCtCashflow.setStudentId(studentId);
        gbCtCashflow.setContestId(contestId);
        gbCtCashflow.setCDate(currentTime);
        gbCtCashflow.setCSurplus(cash - overhaul);
        gbCtCashflow.setCComment("支付管理费:"+overhaul+"元");
        gbCtCashflow.setCIn(0);
        gbCtCashflow.setCOut(overhaul);
        gbCtCashflow.setCAction(CashFlowActionEnum.PAY_OVERHAUL.getAction());
        gbCtCashflowService.save(gbCtCashflow);
        //更新管理费表
        gbRePaymentInfo.setPayTime(currentTime);
        gbRePaymentInfoMapper.updateById(gbRePaymentInfo);
    }




    @Autowired
    private GbCtCashflowMapper gbCtCashflowMapper;
    @Autowired
    private GbContestMapper gbContestMapper;
    /**
     *
     * @param currentTime
     * @param contestId
     * @param studentId
     * @return
     */
    public R startCurrSeason(Integer currentTime,Integer contestId ,Integer studentId){

        //获取管理费
        List<GbRePaymentInfo> listData = new ArrayList<>();
        GbRePaymentInfo overhaul = getOverhaul(contestId, studentId, currentTime);
        if(overhaul != null){
            listData.add(overhaul);
        }
        //需要缴纳利息和本金
        List<GbRePaymentInfo> bankLoans = getBankLoan(currentTime, contestId, studentId);
        listData.addAll(bankLoans);

        //获取贷款利息
        return R.success().put("data",listData);
    }


    /**
     * 获取利息和贷款
     * @param currentTime
     * @param contestId
     * @param studentId
     * @return
     */
    public List<GbRePaymentInfo> getBankLoan(Integer currentTime, Integer contestId , Integer studentId){
        //获取当季需要偿还的贷款利息和本金
        List<GbRePaymentInfo> listBankInfo = gbRePaymentInfoMapper.getListByTime(contestId, studentId, currentTime);
        return listBankInfo;
    }
    /**
     * 支付当季利息或本金
     * @param contestId
     * @param studentId
     * @param currentTime
     * @param amount
     * @param payment
     * @param blAddTime
     * @param cash
     */
    public void payCurrSeasonAddCashFlow(Integer contestId,Integer studentId ,Integer currentTime,Integer amount,String payment,Integer blAddTime,Integer cash){
        cash = cash - amount;
        if(CashFlowActionEnum.INTEREST_R.getAction().equals(payment) || CashFlowActionEnum.REPAYMENT_INTEREST_R.getAction().equals(payment)){
            List<GbRePaymentInfo> gbRePaymentInfos = gbRePaymentInfoMapper.selectByAmountAndBlAddTimeAndPayment(contestId, studentId, currentTime, payment, blAddTime);
            for (GbRePaymentInfo gbRePaymentInfo : gbRePaymentInfos) {
                gbCtBankLoanMapper.deleteById(gbRePaymentInfo.getLoanId());
            }
        }
        //生成流量表
        GbCtCashflow gbCtCashflow = new GbCtCashflow();
        gbCtCashflow.setStudentId(studentId);
        gbCtCashflow.setContestId(contestId);
        gbCtCashflow.setCDate(currentTime);
        gbCtCashflow.setCSurplus(cash);
        gbCtCashflow.setCComment("偿还本金或利息"+amount+"元");
        gbCtCashflow.setCIn(0);
        gbCtCashflow.setCOut(amount);
        gbCtCashflow.setCAction(payment);
        gbCtCashflowService.save(gbCtCashflow);
        //删除
        gbRePaymentInfoMapper.deleteByAmountAndBlAddTimeAndPayment(contestId,studentId,currentTime,payment,blAddTime);

    }

    /**
     * 缴费
     * @param currentTime
     * @param contestId
     * @param studentId
     * @param
     * @return
     */
    public R payCurrSeason(Integer contestId,Integer studentId,Integer currentTime,String payment,Integer blAddTime,Integer amount){
        Integer cash = gbCtCashflowService.getCash(studentId, contestId);
        if(cash<amount){
            return ErrorEnum.CASH_NOT_ENOUGH.getR();
        }

        if("管理费".equals(payment)){
            payOverhaul(contestId,studentId,currentTime);
        }else {
            payCurrSeasonAddCashFlow(contestId,studentId,currentTime,amount,payment,blAddTime,cash);
        }

        //计算报表,判断是否破产
        gbActionBiz.isBankruptcy(studentId,contestId,currentTime);

        return R.success();
    }

    /**
     * 显示工人列表
     */
    @Autowired
    private GbCtGzWorkerRecruitMapper gbCtGzWorkerRecruitMapper;
    public R getWorkerList(Integer contestId,Integer studentId,Integer currentTime){
        //目前已招聘的工人
        List<GbCtWorker> workerList = gbCtWorkerMapper.getGbCtWorkerListByContestIdAndCurrTime(contestId, studentId, currentTime);
        Map<String,GbCtWorker> workerMap = new HashMap<>();
        for (GbCtWorker gbCtWorker : workerList) {
            String workerName = gbCtWorker.getWorkerName();
            workerMap.put(workerName,gbCtWorker);
        }

        //市场上要显示的数据
        List<GbCtWorker> list = new ArrayList<>();
        //获取竞赛表
        GbContest gbContest = gbContestMapper.getObjByContestId(contestId);
        //获取规则表数据
        List<GbCtGzWorkerRecruit> gbCtGzWorkerRecruitList = gbCtGzWorkerRecruitMapper.getListBySubjectNum(gbContest.getSubjectNumber());
        if(gbCtGzWorkerRecruitList != null && gbCtGzWorkerRecruitList.size()>0){
            for (GbCtGzWorkerRecruit gbCtGzWorkerRecruit : gbCtGzWorkerRecruitList) {
                for (int i = 1 ;i <=gbCtGzWorkerRecruit.getQtyPerQtr() ; i++){
                    String name = gbCtGzWorkerRecruit.getRecruitName()+i+"("+currentTime+")";

                    if(!workerMap.containsKey(name)){
                        GbCtWorker gbCtWorker = new GbCtWorker();
                        gbCtWorker.setContestId(contestId)
                                .setWorkerName(gbCtGzWorkerRecruit.getRecruitName()+i+"("+currentTime+")")
                                .setWorkerNum(i)
                                .setRecruitName(gbCtGzWorkerRecruit.getRecruitName())
                                .setRecruitNum(gbCtGzWorkerRecruit.getRecruitNum())
                                .setMultBonus(gbCtGzWorkerRecruit.getMultBonus())
                                .setInitSal(gbCtGzWorkerRecruit.getInitSalExpect())
                                .setStartWorkTime(currentTime)
                                .setPiece(gbCtGzWorkerRecruit.getPiece())
                                .setIsWork(0);
                        list.add(gbCtWorker);
                    }

                }
            }
        }
        return R.success().put("data",list);
    }


    /**
     * 发送offer
     * @param gbCtWorker
     * @param studentId
     * @return
     */
    public R sendOffer(GbCtWorker gbCtWorker,Integer studentId){
        gbCtWorker.setStudentId(studentId)
                   .setIsWork(GbCtWorker.ON_BUSI);//招聘



        //判断该员工是否已经招聘
        List<GbCtWorker> gbCtWorkerList = gbCtWorkerMapper.selectList(
                new LambdaQueryWrapper<GbCtWorker>()
                        .eq(GbCtWorker::getWorkerName,gbCtWorker.getWorkerName())
                        .eq(GbCtWorker::getStudentId,studentId)
                        .eq(GbCtWorker::getContestId,gbCtWorker.getContestId())
        );
        if((gbCtWorkerList!=null) && (gbCtWorkerList.size()>0)){
            return ErrorEnum.GRYZP.getR();
        }


        gbCtWorkerMapper.insert(gbCtWorker);
        return R.success();
    }


    /**
     * 获取原料商店
     * @param contestId
     * @return
     */
    @Autowired
    private GbCtGzMaterialMapper gbCtGzMaterialMapper;
    public R getGzMaterialList(Integer contestId, Integer studentId,Integer currentTime){
        //规则表数量
        List<GbCtGzMaterial> gzs = gbCtGzMaterialMapper.list(contestId);
        //原料订单sum聚合  获取对应原材料本季度所有人下单的数量
        List<GbCtDdMaterial> dds = gbCtDdMaterialMapper.getMaterialOrderEachSum(contestId, studentId,currentTime);
        Map<Integer,GbCtDdMaterial> ddsMap = new HashMap<>();
        for (GbCtDdMaterial dd : dds) {
            ddsMap.put(dd.getOmCmid(),dd);
        }
        for (GbCtGzMaterial gz : gzs) {
            if(ddsMap.containsKey(gz.getId())){
                //减去订单数量
                int i = gz.getNum() - ddsMap.get(gz.getId()).getNum();
                gz.setNum(i);
            }
        }
        return R.success().put("data",gzs);
    }

    @Autowired
    private GbCtFeeMapper gbCtFeeMapper;
    /**
     * 原料商店确认下单
     * @param gbMaterialVo
     * @param studentId
     * @return
     */
    public R getGzMaterialShopConfirm(GbMaterialVo gbMaterialVo , Integer studentId){

        //收货期
        Integer remainDate = DateUtils.addYearAndSeasonTime(gbMaterialVo.getCurrentTime(), gbMaterialVo.getCmLeadDate());
        //新增订单
        GbCtDdMaterial gbCtDdMaterial = new GbCtDdMaterial();
        gbCtDdMaterial
                .setContestId(gbMaterialVo.getContestId())
                .setStudentId(studentId)
                .setOmCmid(gbMaterialVo.getCmId())
                .setCmName(gbMaterialVo.getCnName())
                .setPurchaseDate(gbMaterialVo.getCurrentTime())
                .setRemainDate(remainDate)
                .setIsInventory(0)
                .setPayDate(gbMaterialVo.getCmPayDate())
                .setIsPay(1)
                .setPrice(gbMaterialVo.getCmBuyFee())
                .setNum(gbMaterialVo.getNum());
        gbCtDdMaterialMapper.insert(gbCtDdMaterial);

        return R.success();
    }
    /**
     * 获取原料订单
     * @param contestId
     * @return
     */
    @Autowired
    private GbCtDdMaterialMapper gbCtDdMaterialMapper;
    public R getMaterialOrderList(Integer contestId, Integer studentId){
        List<GbCtDdMaterial> list = gbCtDdMaterialMapper.list(contestId,studentId);
        return R.success().put("data",list);
    }
    /**
     * 获取原料库存
     * @param contestId
     * @return
     */
    @Autowired
    private GbCtKcMaterialMapper gbCtKcMaterialMapper;
    public R getMaterialInventoryList(Integer contestId, Integer studentId){
        List<GbCtKcMaterial> list = gbCtKcMaterialMapper.listKc(contestId, studentId);
        return R.success().put("data",list);
    }

    
    /**
     * 获取产品库存 按入库时间，价格，产品分组展示
     * @param contestId
     * @return
     */
    @Autowired
    private GbCtKcProductMapper gbCtKcProductMapper;
    public R getProductInventory(Integer contestId, Integer studentId){
        List<GbCtKcProduct> list = gbCtKcProductMapper.listKcGroupByPriceAndInventory(contestId, studentId,GbCtKcProduct.IS_IN);
        return R.success().put("data",list);
    }

    /**
     * 收货
     * @param id
     * @param currentTime
     * @return
     */
    public R materialToInventory(Integer id, Integer currentTime){
        GbCtDdMaterial dd = gbCtDdMaterialMapper.selectById(id);
        if(dd.getIsInventory() == 1){
            return R.error(CodeEnum.IS_INVENTORY,"已经收货！");
        }
        if(currentTime < dd.getRemainDate()){
            return R.error(CodeEnum.NOT_REMAIN_DATE,"收货期未到！");
        }
        //更新订单入库状态/付款账期
        dd.setIsInventory(GbCtDdMaterial.IS_INVENTORY);
        gbCtDdMaterialMapper.updateById(dd);
        //付款账期
        int remainDate = dd.getRemainDate();
        int paymentDate = DateUtils.addYearAndSeasonTime(currentTime,dd.getPayDate());
        int rFee = dd.getNum() * dd.getPrice();
        GbCtFee fee = new GbCtFee();
        fee.setStudentId(dd.getStudentId())
                .setContestId(dd.getContestId())
                .setBorrower("1")
                .setLender("供应商")
                .setRFee(rFee)
                .setRemarks("原材料"+dd.getCmName()+"付款")
                .setRRemainDate(remainDate)
                .setPaymentDate(paymentDate)
                .setType(GbCtFee.TO_PAYMENT);
        gbCtFeeMapper.insert(fee);
        //产生入库库存
        GbCtKcMaterial kc = new GbCtKcMaterial();
        kc.setStudentId(dd.getStudentId())//学生id
                .setContestId(dd.getContestId())//竞赛id
                .setImCmId(dd.getOmCmid())//原料商店产品id
                .setImNum(dd.getNum())//数量
                .setMaterialName(dd.getCmName())//原料名称
                .setInInventoryDate(currentTime)//入库时间
                .setMaterialPrice(dd.getPrice());//价格
        gbCtKcMaterialMapper.insert(kc);

        //计算报表,判断是否破产
        gbActionBiz.isBankruptcy(dd.getStudentId(),dd.getContestId(),currentTime);

        return R.success();
    }

    /**
     *获取在职工人信息
     * @param contestId
     * @param studentId
     * @return
     */
    public R getWorkerOnTheJobList(Integer contestId,Integer studentId ){
        List<GbCtWorker> workerList = gbCtWorkerMapper.getAllWorker(contestId, studentId);
        return R.success().put("data",workerList);
    }

    /**
     *获取班次规则
     * @param contestId
     * @param studentId
     * @return
     */
    @Autowired
    private GbCtGzClassesMapper gbCtGzClassesMapper;
    public R getClassList(Integer contestId){
        List<GbCtGzClasses> classes = gbCtGzClassesMapper.getClassesByContestId(contestId);
        return R.success().put("data",classes);
    }


    @Autowired
    private GbCtLineMapper gbCtLineMapper;
    public R getLineGroupInfo(Integer studentId,Integer contestId,Integer currentTime){
        List<GbCtLine> listLine = gbCtLineMapper.getLineListOrderByFinishTime(studentId, contestId, currentTime);
        return R.success().put("list",listLine);
    }
}
