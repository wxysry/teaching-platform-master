package com.xiyou.main.biz.gbcontest;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.xiyou.common.enums.CodeEnum;
import com.xiyou.common.utils.R;
import com.xiyou.main.dao.gbcontest.*;
import com.xiyou.main.entity.gbcontest.*;
import com.xiyou.main.service.gbcontest.GbContestService;
import com.xiyou.main.service.gbcontest.GbCtBalanceService;
import com.xiyou.main.service.gbcontest.GbCtCashflowService;
import com.xiyou.main.service.gbcontest.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @program: multi-module
 * @description:
 * @author: wangxingyu
 * @create: 2023-06-11
 **/
@Service
@Transactional(rollbackFor = Exception.class)
@Slf4j
public class GbBusinessBiz {

    @Autowired
    private GbActionBiz gbActionBiz;

    @Autowired
    private GbContestService gbContestService;
    @Autowired
    private GbContestStudentService gbContestStudentService;
    @Autowired
    private GbContestStudentMapper gbContestStudentMapper;
    @Autowired
    private GbCtSubjectMapper gbCtSubjectMapper;

    @Autowired
    private GbCtGzIsoMapper gbCtGzIsoMapper;
    @Autowired
    private GbCtGzProductMapper gbCtGzProductMapper;
    @Autowired
    private GbCtYfProductMapper gbCtYfProductMapper;
    @Autowired
    private GbCtGzMarketMapper gbCtGzMarketMapper;
    @Autowired
    private GbCtGzOrderMapper gbCtGzOrderMapper;
    @Autowired
    private GbCtGzNumMapper gbCtGzNumMapper;
    @Autowired
    private GbCtYfNumMapper gbCtYfNumMapper;
    @Autowired
    private GbCtCorporateMapper gbCtCorporateMapper;
    @Autowired
    private GbCtBalanceService gbCtBalanceService;
    @Autowired
    private GbCtCashflowService gbCtCashflowService;
    @Autowired
    @Lazy
    private GbRestoreDataBiz gbRestoreDataBiz;
    @Autowired
    private GbCtYfIsoMapper gbCtYfIsoMapper;
    @Autowired
    private GbCtYfMarketMapper gbCtYfMarketMapper;


    public R start(Integer studentId, Integer contestId) {

        GbContestStudent gbContestStudent = gbContestStudentService.get(contestId, studentId);
        if (gbContestStudent == null) {
            return R.error(CodeEnum.OTHER_ERROR.getCode(), "竞赛不存在");
        }
        if (gbContestStudent.getStart() == 1) {
            return R.error(CodeEnum.OTHER_ERROR.getCode(), "已开始经营");
        }
        if (gbContestStudent.getFinishTime() != null) {
            return R.error(CodeEnum.OTHER_ERROR.getCode(), "已完成该竞赛模拟");
        }
        // 初始权益作为初始资金
        GbContest gbContest = gbContestService.getById(contestId);

        Integer cash = gbContest == null ? 0 : gbContest.getEquity();

        // balance新增一条记录
        // 学生ID、考试ID、1、0、规则表该套题中的cash、0、0、0、0、规则表该套题中的cash、0、0、0、0、规则表该套题中的cash、0、0、0、0、规则表该套题中的cash、0、0、规则表该套题中的cash、规则表该套题中的cash
        GbCtBalance gbCtBalance = new GbCtBalance()
                .setStudentId(studentId)
                .setContestId(contestId)
                .setBsIsxt(1)
                .setBsYear(0)
                .setBsCash(cash)
                .setBsReceivable(0)
                .setBsProductInProcess(0)
                .setBsProduct(0)
                .setBsMaterial(0)
                .setBsTotalCurrentAsset(cash)
                .setBsEquipment(0)
                .setBsProjectOnConstruction(0)
                .setBsTotalFixedAsset(0)
                .setBsTotalAsset(cash)
                .setBsLongLoan(0)
                .setBsShortLoan(0)
                .setBsOtherPay(0)
                .setBsTax(0)
                .setBsTotalLiability(0)
                .setBsEquity(cash)
                .setBsRetainedEarning(0)
                .setBsAnnualNetProfit(0)
                .setBsTotalEquity(cash)
                .setBsTotal(cash)
                .setIsSubmit("Y");
        gbCtBalanceService.save(gbCtBalance);

        // Cashflow新增一条记录，具体为：
        // 学生ID、考试ID、顺序号、开始经营、规则表该套题中的cash、0、规则表该套题中的cash、公司成立、10
        GbCtCashflow cashflow = new GbCtCashflow()
                .setStudentId(studentId)
                .setContestId(contestId)
                .setCAction("开始经营")
                .setCIn(cash)
                .setCOut(0)
                .setCSurplus(cash)
                .setCComment("公司成立")
                .setCDate(10);
        gbCtCashflowService.save(cashflow);

        // ISO研发情况新增记录，具体为：
        // 该套规则中含有的所有ISO编号各生成一条记录，ISO编号=ISO编号，总研发时间=研发周期，已研发时间=0，剩余研发时间=总研发周期，完成时间 is null
        List<GbCtGzIso> ctGzIsoList = gbCtGzIsoMapper.list(contestId);
        List<GbCtYfIso> ctYfIsoList = new ArrayList<>();
        for (GbCtGzIso gbCtGzIso : ctGzIsoList) {
            ctYfIsoList.add(new GbCtYfIso()
                    .setStudentId(studentId)
                    .setContestId(contestId)
                    .setDiCiid(gbCtGzIso.getId())
                    .setDiTotalDate(gbCtGzIso.getCiDevelopDate())
                    .setDiNowDate(0)
                    .setDiRemainDate(gbCtGzIso.getCiDevelopDate())
                    .setDiFinishDate(null)
                    .setDiState(GbCtYfIso.ISO_YF_NONE)
            )
            ;
        }
        if (ctYfIsoList.size() > 0) {
            gbCtYfIsoMapper.insertBatch(ctYfIsoList);
        }
        // 市场研发、产品研发同ISO
        List<GbCtGzMarket> ctGzMarketList = gbCtGzMarketMapper.list(contestId);
        List<GbCtYfMarket> ctYfMarketList = new ArrayList<>();
        for (GbCtGzMarket ctGzMarket : ctGzMarketList) {
            ctYfMarketList.add(new GbCtYfMarket()
                    .setStudentId(studentId)
                    .setContestId(contestId)
                    .setDmCmId(ctGzMarket.getId())
                    .setDmTotalDate(ctGzMarket.getCmDevelopDate())
                    .setDmNowDate(0)
                    .setDmRemainDate(ctGzMarket.getCmDevelopDate())
                    .setDmFinishDate(null)
                    .setDmState(GbCtYfMarket.SC_YF_NONE)
            );
        }
        if (ctYfMarketList.size() > 0) {
            gbCtYfMarketMapper.insertBatch(ctYfMarketList);
        }

        List<GbCtGzProduct> ctGzProductList = gbCtGzProductMapper.getList(contestId);
        List<GbCtYfProduct> ctYfProductList = new ArrayList<>();
        for (GbCtGzProduct ctGzProduct : ctGzProductList) {
            ctYfProductList.add(new GbCtYfProduct()
                    .setStudentId(studentId)
                    .setContestId(contestId)
                    .setDpCpId(ctGzProduct.getId())
                    .setDpTotalDate(ctGzProduct.getCpDevelopDate())
                    .setDpNowDate(0)
                    .setDpRemainDate(ctGzProduct.getCpDevelopDate())
                    .setDpFinishDate(null)
                    .setDpState(GbCtYfProduct.CP_YF_NONE)
            );
        }
        if (ctYfProductList.size() > 0) {
            gbCtYfProductMapper.insertBatch(ctYfProductList);
        }
        //数字化研发
        List<GbCtGzNum> gbCtGzNums = gbCtGzNumMapper.getList(contestId);
        for (GbCtGzNum gbCtGzNum : gbCtGzNums) {
            GbCtYfNum gbCtYfNum = new GbCtYfNum().
                    setStudentId(studentId)
                    .setContestId(contestId)
                    .setNumId(gbCtGzNum.getId())
                    .setConsumeMoney(gbCtGzNum.getConsumeMoney())
                    .setState(GbCtYfNum.NUM_YF_NONE);
            gbCtYfNumMapper.insert(gbCtYfNum);
        }

        //初始化社会责任分
        GbCtCorporate ctCorporate = new GbCtCorporate();
        ctCorporate.setContestId(contestId)
                .setScore(GbCtCorporate.DEFAULT_SCORE)
                .setStudentId(studentId);
        gbCtCorporateMapper.insert(ctCorporate);

        // 更新开始状态
        gbContestStudentMapper.updateStart(contestId, studentId);


        //生成初始报表
        gbActionBiz.isBankruptcy(studentId,contestId,11);

        //完成以上操作后，除了规则表，其余备份到新的表。
        gbRestoreDataBiz.backupData(contestId, studentId,11);
        return R.success();
    }
    /**
     *
     */
    public R saveProgress(Integer studentId, Integer contestId, Integer date, String progress) {
        gbContestStudentMapper.saveProgress(studentId, contestId, date, progress);
        return R.success();
    }

    public R getProgress(Integer studentId, Integer contestId) {
        GbContestStudent gbContestStudent = gbContestStudentMapper.get(contestId, studentId);
        if (gbContestStudent == null) {
            return R.error(CodeEnum.PARAM_ERROR.getCode(), "竞赛不存在");
        }
        if (gbContestStudent.getFinishTime() != null) {
            return R.error(CodeEnum.OTHER_ERROR.getCode(), "已完成该竞赛模拟");
        }
        GbContest gbContest = gbContestService.getById(contestId);
        Map<String, Object> returnMap = new HashMap<>();
//        returnMap.put("restore", gbContest.getRestore());
//        returnMap.put("restoreSeason", gbContest.getRestoreSeason());
//        returnMap.put("restart", gbContest.getRestart());
        returnMap.put("title", gbContest.getTitle());
        returnMap.put("progress", gbContestStudent.getProgress());
        returnMap.put("date", gbContestStudent.getDate());
        returnMap.put("data",gbContestStudent);
        return R.success(returnMap);
    }


    public R end(Integer studentId, Integer contestId) {
        GbContestStudent gbContestStudent = gbContestStudentMapper.get(contestId, studentId);
        if (gbContestStudent == null) {
            return R.error(CodeEnum.OTHER_ERROR);
        }
        if (gbContestStudent.getFinishTime() != null) {
            return R.error(CodeEnum.OTHER_ERROR.getCode(), "已完成该竞赛模拟");
        }
        gbContestStudent.setFinishTime(LocalDateTime.now());
        gbContestStudentService.updateById(gbContestStudent);

//        // 清除本年度备份的数据
//        gbRestoreDataBiz.removeLikeTableDataAll(contestId, studentId);

        return R.success();
    }

    /**
     * 学生点击继续按钮
     * @param userId
     * @param contestId
     * @return
     */
    public R nextStatus(Integer userId, Integer contestId) {
        log.error("点击继续按钮,contestId:{},studentId:{}",contestId,userId);
        GbContestStudent gbContestStudent = gbContestStudentService.getOne(new LambdaQueryWrapper<GbContestStudent>()
                .eq(GbContestStudent::getContestId,contestId)
                .eq(GbContestStudent::getStudentId,userId)
        );
        //获取学生当前的status
        if(gbContestStudent!=null){
            int date = gbContestStudent.getDate();
            int status = date%10;
            //只有
            if(status<=3){
                gbContestStudent.setDate(date+1);
                log.error("点击继续按钮,contestId:{},studentId:{},date:{}",contestId,userId,gbContestStudent.getDate());
                gbContestStudentService.update(new GbContestStudent(),new UpdateWrapper<GbContestStudent>()
                        .eq("id",gbContestStudent.getId())
                        .eq("date",date)
                        .set("date",date+1)
                );
            }
        }
        return R.success();
    }

}
