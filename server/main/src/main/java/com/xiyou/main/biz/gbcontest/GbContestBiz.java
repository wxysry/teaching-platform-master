package com.xiyou.main.biz.gbcontest;

import com.alibaba.excel.support.ExcelTypeEnum;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.xiyou.common.enums.CodeEnum;
import com.xiyou.common.exception.CustomException;
import com.xiyou.common.office.utils.EasyPOIUtil;
import com.xiyou.common.office.utils.FileUtil;
import com.xiyou.common.redis.utils.RedisCache;
import com.xiyou.common.utils.CommonUtil;
import com.xiyou.common.utils.R;
import com.xiyou.main.dao.exam.ResourcesMapper;
import com.xiyou.main.dao.gbcontest.*;
import com.xiyou.main.entity.exam.Resources;
import com.xiyou.main.entity.exam.SysUser;
import com.xiyou.main.entity.gbcontest.*;
import com.xiyou.main.enums.CashFlowActionEnum;
import com.xiyou.main.enums.ErrorEnum;
import com.xiyou.main.enums.RedisEnum;
import com.xiyou.main.params.contest.ContestParam;
import com.xiyou.main.params.contest.ContestStudentParam;
import com.xiyou.main.params.gbcontest.GbCompanyParam;
import com.xiyou.main.params.gbcontest.GbDigitalizeParam;
import com.xiyou.main.schedule.ScheduleTaskService;
import com.xiyou.main.service.exam.SysUserService;
import com.xiyou.main.service.exam.TeachClassService;
import com.xiyou.main.service.gbcontest.GbContestService;
import com.xiyou.main.service.gbcontest.GbCtCashflowService;
import com.xiyou.main.service.gbcontest.*;
import com.xiyou.main.utils.DateUtils;
import com.xiyou.main.vo.contest.ContestScore;
import com.xiyou.main.vo.gbcontest.GbMarketForecast;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @program: multi-module
 * @description:
 * @author: tangcan
 * @create: 2019-08-25 10:39
 **/
@Service
@Transactional(rollbackFor = Exception.class)
@Slf4j
public class GbContestBiz {
    @Autowired
    private GbContestService gbContestService;
    @Autowired
    private TeachClassService teachClassService;
    @Autowired
    private GbContestStudentService gbContestStudentService;
    @Autowired
    private GbCtSubjectMapper gbCtSubjectMapper;
    @Autowired
    private ResourcesMapper resourcesMapper;
    @Autowired
    private SysUserService sysUserService;
    @Autowired
    private GbCtGzCsService gbCtGzCsService;
    @Autowired
    private GbCtGzMarketMapper gbCtGzMarketMapper;
    @Autowired
    private GbCtGzProductMapper gbCtGzProductMapper;
    @Autowired
    private GbCtGzMaterialMapper gbCtGzMaterialMapper;
    @Autowired
    private GbCtGzProducingMapper gbCtGzProducingMapper;
    @Autowired
    private GbCtKcProductMapper gbCtKcProductMapper;
    @Autowired
    private GbCtLineMapper gbCtLineMapper;
    @Autowired
    private GbCtGzClassesMapper gbCtGzClassesMapper;
    @Autowired
    private GbCtFeeMapper gbCtFeeMapper;
    @Autowired
    private GbCtCashflowService gbCtCashflowService;
    @Autowired
    private GbCtWorkerMapper gbCtWorkerMapper;
    @Autowired
    private GbCtKcMaterialMapper gbCtKcMaterialMapper;
    @Autowired
    private GbContestMapper gbContestMapper;
    @Autowired
    private GbContestStudentMapper gbContestStudentMapper;
    @Autowired
    private ScheduleTaskService scheduleTaskService;
    @Autowired
    private RedisCache redisCache;
    @Autowired
    private GbRestoreDataBiz gbRestoreDataBiz;
    @Autowired
    private GbActionBiz gbActionBiz;
    @Autowired
    private GbCtProfitChartMapper gbCtProfitChartMapper;
    @Autowired
    private GbCtChargesMapper gbCtChargesMapper;
    @Autowired
    private GbCtBalanceMapper gbCtBalanceMapper;
    @Autowired
    private GbCtAnnouncementMapper gbCtAnnouncementMapper;
    @Autowired
    private GbDownloadBiz gbDownloadBiz;
    @Autowired
    private GbFillReportBiz gbFillReportBiz;
    @Autowired
    private GbCtChargesService gbCtChargesService;
    @Autowired
    private GbCtDdMaterialMapper gbCtDdMaterialMapper;
    @Autowired
    private GbCtYfIsoMapper gbCtYfIsoMapper;
    @Autowired
    private GbCtYfProductMapper gbCtYfProductMapper;
    @Autowired
    private GbCtYfMarketMapper gbCtYfMarketMapper;
    @Autowired
    private GbCtBankLoanMapper gbCtBankLoanMapper;
    @Autowired
    private GbCtXzOrderMapper gbCtXzOrderMapper;
    @Autowired
    private GbCtCashflowMapper gbCtCashflowMapper;
    @Autowired
    private GbTradeFairBiz gbTradeFairBiz;
    @Autowired
    private GbCtMnAdMapper gbCtMnAdMapper;

    @Autowired
    Environment environment;

    public R listByTeacher(ContestParam contestParam) {
        Page<GbContest> page = gbContestService.getPage(contestParam);
        for (GbContest gbContest : page.getRecords()) {
            // 竞赛状态
            int status;
            LocalDateTime now = LocalDateTime.now();
            if (gbContest.getIsPublish() == 0) {
                status = 0;
            } else if(gbContest.getIsPublish() == 1){
                status = 1;
            } else {
                status = 2;
            }
            gbContest.setStatus(status);
        }
        Map<String, Object> returnMap = new HashMap<>();
        returnMap.put("total", page.getTotal());
        returnMap.put("list", page.getRecords());
        return R.success(returnMap);
    }

    public R listByAdmin(ContestParam contestParam) {
        Page<GbContest> page = gbContestService.getPageAdmin(contestParam);
        for (GbContest gbContest : page.getRecords()) {
            // 竞赛状态
            int status;
            LocalDateTime now = LocalDateTime.now();
            if (gbContest.getIsPublish() == 0) {
                status = 0;
            } else if(gbContest.getIsPublish() == 1){
                status = 1;
            } else {
                status = 2;
            }
            gbContest.setStatus(status);
        }
        Map<String, Object> returnMap = new HashMap<>();
        returnMap.put("total", page.getTotal());
        returnMap.put("list", page.getRecords());
        return R.success(returnMap);
    }

    public R listByStudent(ContestParam contestParam) {
        Page<GbContest> page = gbContestService.getStudentContestPage(contestParam);
        for (GbContest contest : page.getRecords()) {
            // 竞赛状态
            int status;
            LocalDateTime now = LocalDateTime.now();
            if(contest.getFinishTime()==null){
                //已开始
                status = 1;
            }else{
                //已结束
                status = 2;
            }
            contest.setStatus(status);
        }
        Map<String, Object> returnMap = new HashMap<>();
        returnMap.put("total", page.getTotal());
        returnMap.put("list", page.getRecords());
        return R.success(returnMap);
    }

    public R get(Integer contestId) {
        Map<String, Object> returnMap = new HashMap<>();
        GbContest gbContest = gbContestService.getById(contestId);
        if (gbContest == null) {
            return R.error(CodeEnum.OTHER_ERROR.getCode(), "无此竞赛");
        }
        GbCtSubject gbCtSubject = gbCtSubjectMapper.getBySubjectNumber(gbContest.getSubjectNumber());
        if (gbCtSubject != null) {
            gbContest.setGroupId(gbCtSubject.getGroupId());
            gbContest.setStudentList(gbContestStudentService.getStudentIdList(contestId));
        }
        int  remainTime = (int)redisCache.getExpire(RedisEnum.HHDJS+":" + contestId);
        if(remainTime <= 0){
            remainTime = gbContest.getRemainTime();
        }
        gbContest.setRemainTime(remainTime);
        returnMap.put("data", gbContest);

        return R.success(returnMap);
    }

    public R add(GbContest gbContest) {
        List<Integer> studentIdList;
        if (gbContest.getPublishAll() != null && gbContest.getPublishAll() == 1) {
            studentIdList = teachClassService.getStudentIdList(gbContest.getTeacherId());
        } else {
            studentIdList = gbContest.getStudentList();
        }
        if (studentIdList != null) {
            studentIdList = studentIdList.stream().distinct().collect(Collectors.toList());
        }
        gbContest.setStudentNum(studentIdList == null ? 0 : studentIdList.size());
        gbContest.setTeacherDate(11);
        List<GbCtGzMarket> markets = gbCtGzMarketMapper.selectList(new QueryWrapper<GbCtGzMarket>().eq("subject_number", gbContest.getSubjectNumber()));
        StringBuilder marketSort = new StringBuilder();
        for (int i = 0; i < markets.size(); i++) {
            marketSort.append(markets.get(i).getCmName());
            if(i<markets.size()-1){
                marketSort.append("-->");
            }
        }
        gbContest.setMarketSort(marketSort.toString());//市场排序
        List<GbCtGzProduct> products = gbCtGzProductMapper.selectList(new QueryWrapper<GbCtGzProduct>().eq("subject_number", gbContest.getSubjectNumber()));
        StringBuilder productSort = new StringBuilder();
        for (int i = 0; i < products.size(); i++) {
            productSort.append(products.get(i).getCpName());
            if(i<products.size()-1){
                productSort.append("-->");
            }
        }
        gbContest.setProductSort(productSort.toString());//产品排序
        gbContest.setCountDown("[10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10]");
        gbContest.setRemainTime(10*60);
        gbContest.setIsSuspend(1);
        gbContestService.save(gbContest);
        if (studentIdList != null && studentIdList.size() > 0) {
            gbContestStudentService.insertBatch(gbContest.getContestId(), studentIdList);
        }


        return R.success().put("contestId", gbContest.getContestId());
    }


    public R update(GbContest gbContest) {
        if (gbContest.getContestId() == null) {
            return R.error(CodeEnum.PARAM_MISS.getCode(), "请选择竞赛");
        }
        GbContest gbContest1 = gbContestService.getById(gbContest.getContestId());
        if (gbContest1 == null || !gbContest1.getTeacherId().equals(gbContest.getTeacherId())) {
            return R.error(CodeEnum.PARAM_ERROR.getCode(), "无此竞赛");
        }
        // 未开始才能修改学生
        boolean start = (gbContest.getIsPublish() == 1);
        if (!start) {
            List<Integer> studentIdList;
            if (gbContest.getPublishAll() != null && gbContest.getPublishAll() == 1) {
                studentIdList = teachClassService.getStudentIdList(gbContest.getTeacherId());
            } else {
                studentIdList = gbContest.getStudentList();
            }
            if (studentIdList != null) {
                studentIdList = studentIdList.stream().distinct().collect(Collectors.toList());
            }
            gbContest.setStudentNum(studentIdList == null ? 0 : studentIdList.size());

            // 先删掉原来的学生
            gbContestStudentService.remove(gbContest.getContestId());
            if (studentIdList != null && studentIdList.size() > 0) {
                // 再插入
                gbContestStudentService.insertBatch(gbContest.getContestId(), studentIdList);
            }
        }
        if (start) {
            // 开始了就不再更改初始权益
            gbContest.setEquity(null);
        }
        gbContestService.updateById(gbContest);
        return R.success();
    }


    public R delete(Integer contestId) {
        //根据ID删除gb_contest
        gbContestService.removeById(contestId);
        QueryWrapper<GbContestStudent> wrapper = new QueryWrapper<>();
        wrapper.eq("contest_id", contestId);
        //删除学生
        gbContestStudentService.remove(wrapper);
        //删除当前比赛相关数据
        gbContestMapper.deleteData(contestId);
        //删除备份数据
        gbContestMapper.deleteLikeData(contestId);

        return R.success();
    }


    public R publish(Integer teacherId, Integer contestId) {
        SysUser user = sysUserService.getById(teacherId);
        // 只能同时发布一个竞赛
        if (gbContestService.getUnEndContest(teacherId) >= user.getContestNum()) {
            return R.error(CodeEnum.OTHER_ERROR.getCode(), "只能同时发布 " + user.getContestNum() + " 套竞赛模拟");
        }
        GbContest gbContest = gbContestService.getById(contestId);
        if (gbContest == null || !gbContest.getTeacherId().equals(teacherId)) {
            return R.error(CodeEnum.PARAM_ERROR.getCode(), "竞赛不存在");
        }
        if (gbContest.getIsPublish() == 1) {
            return R.error(CodeEnum.OTHER_ERROR.getCode(), "竞赛已发布！");
        }
        if (gbContest.getStudentNum() == 0) {
            return R.error(CodeEnum.OTHER_ERROR.getCode(), "暂无考生，不能发布！");
        }
        gbContestService.publish(teacherId, contestId);
        return R.success();
    }

    public R studentList(ContestStudentParam contestStudentParam) {
        Page<GbContestStudent> page = gbContestStudentService.getPage(contestStudentParam);
        Map<String, Object> returnMap = new HashMap<>();
        returnMap.put("total", page.getTotal());
        returnMap.put("list", page.getRecords());
        return R.success(returnMap);
    }


    public R getRuleAttachment(Integer contestId) {
        Resources resources = gbCtSubjectMapper.getRuleAttachment(contestId);
        if (resources == null) {
            return R.error(CodeEnum.OTHER_ERROR.getCode(), "暂无规则预测详单");
        }
        return R.success().put("info", resources);
    }

    public R studentScore(Integer id, Double score) {
        gbContestStudentService.updateScore(id, score);
        return R.success();
    }

    public R getEquity(Integer subjectNumber) {
        // 规则表该套题中的cash
        GbCtGzCs ctGzCs = gbCtGzCsService.getBySubjectNumber(subjectNumber);
        Integer cash = ctGzCs == null ? 0 : ctGzCs.getCash();
        return R.success().put("equity", cash);
    }

    public void downloadScore(Integer contestId, HttpServletResponse response) {
        GbContest gbContest = gbContestService.getById(contestId);
        if (gbContest == null) {
            return;
        }
        List<ContestScore> contestScoreList = gbContestStudentService.getScoreList(contestId);
        Map<String, Object> paramMap = new HashMap<>();
        List<Map<String, Object>> mapList = new ArrayList<>(CommonUtil.getListInitCap(contestScoreList.size()));
        int num = 0;
        Map<String, Object> map;
        for (ContestScore contestScore : contestScoreList) {
            map = new HashMap<>();
            map.put("id", ++num);
            map.put("name", contestScore.getName());
            map.put("account", contestScore.getAccount());
            map.put("school", contestScore.getSchool());
            map.put("equity", contestScore.getEquity() == null ? "-" : (contestScore.getEquity() + "W"));
            map.put("score", contestScore.getScore() == null ? "-" : contestScore.getScore());
            mapList.add(map);
        }
        paramMap.put("maplist", mapList);
        String paperName = "《" + gbContest.getTitle() + "》";
        paramMap.put("title", paperName);
        try {
            EasyPOIUtil.exportMultiSheetExcelByMap(response, paperName + "竞赛成绩" + ".xlsx", CommonUtil.getClassesPath() + "templates/contest_score.xlsx", paramMap, false);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    /**
     * 数字化
     * @param userId
     * @param param
     * @return
     */
    public R digitalizeStart(Integer userId,  GbDigitalizeParam param){
        List<GbDigitalizeParam.LineIdAndCpId> LineIdCpIds = param.getList();
        for (GbDigitalizeParam.LineIdAndCpId lineIdAndCpId : LineIdCpIds) {
            GbCtLine ctLine = gbCtLineMapper.selectById(lineIdAndCpId.getLineId());
            String status = ctLine.getStatus();
            if(!GbCtLine.ON_SPACE.equals(status)){
                throw new CustomException(CodeEnum.OTHER_ERROR.getCode(), "操作失败，请重试");
            }
        }

        //获取当前学生的操作时间
        Integer studentDate = gbContestStudentService.getStudentDate(param.getContestId(),userId);
        param.setCurrentTime(studentDate/10);

        //获取工人
        List<GbCtWorker> workers = gbCtWorkerMapper.getGbCtWorkerListOnTheJob(param.getContestId(), userId,GbCtWorker.ON_SPACE);
        //获取班次
        GbCtGzClasses classes = gbCtGzClassesMapper.selectById(param.getClassesId());
        int gj = 0 ,pt = 0;//人数
        int gjXl = 0, ptXl = 0 ;//效率
        List<GbCtWorker> workerGj = new ArrayList<>();//高级工人
        List<GbCtWorker> workerPt = new ArrayList<>();//普通工人
        for (GbCtWorker worker : workers) {
            if("手工工人".equals(worker.getRecruitName())) {
                pt++;
                ptXl = worker.getMultBonus();
                workerPt.add(worker);
            }
            if("高级工人".equals(worker.getRecruitName())) {
                gj++;
                gjXl = worker.getMultBonus();
                workerGj.add(worker);
            }
        }

        //获取原料库存信息
        List<GbCtKcMaterial> materials = gbCtKcMaterialMapper.list(param.getContestId(), userId);

        //材料规则id和材料编号的键值对
        List<GbCtGzMaterial> materialGzs = gbCtGzMaterialMapper.list(param.getContestId());
        Map<Integer,Integer> idAndCmIdMap = new HashMap<>();
        for (GbCtGzMaterial gbCtGzMaterial : materialGzs) {
            idAndCmIdMap.put(gbCtGzMaterial.getId(),gbCtGzMaterial.getCmId());
        }
        //获取原料规则
        Map<Integer,GbCtGzMaterial> materialGzMaps = new HashMap<>();
        for (GbCtGzMaterial materialGz : materialGzs) {
            materialGzMaps.put(materialGz.getCmId(),materialGz);
        }
        //把材料库存分类
        Map<Integer,List<GbCtKcMaterial>> kcMaterialMap = new HashMap<>();
        for (GbCtGzMaterial materialGz : materialGzs) {
            List<GbCtKcMaterial> list = new ArrayList<>();
            for (GbCtKcMaterial material : materials) {
                Integer imNum = idAndCmIdMap.get(material.getImCmId());
                if(imNum.equals(materialGz.getCmId())){
                    list.add(material);
                }
            }
            kcMaterialMap.put(materialGz.getCmId(),list);
        }

        List<GbDigitalizeParam.LineIdAndCpId> LineIdAndCpIds = param.getList();
        for (GbDigitalizeParam.LineIdAndCpId lineIdAndCpId : LineIdAndCpIds) {
            GbCtLine ctLine = gbCtLineMapper.selectById(lineIdAndCpId.getLineId());
            //生产线规则
            GbCtGzProductLine gbCtGzProductLine = gbCtLineMapper.getGzLineByLineId(lineIdAndCpId.getLineId());
            //产品规则
            GbCtGzProduct gbCtGzProduct = gbCtGzProductMapper.selectById(lineIdAndCpId.getCpId());
            //获取当前产品所需的材料及数量
            List<GbCtGzProducing> gbCtGzProducings = gbCtGzProducingMapper.getListByCpId(lineIdAndCpId.getCpId());
            Integer cplSeniorWorker = gbCtGzProductLine.getCplSeniorWorker();//高级工人数
            Integer cplOrdinaryWorker = gbCtGzProductLine.getCplOrdinaryWorker();//普通工人数
            Integer productFinishTime = DateUtils.addYearAndSeasonTime(param.getCurrentTime(),gbCtGzProductLine.getCplProduceDate()  == null?0:gbCtGzProductLine.getCplProduceDate());
            //查询出当前产线的工人
            List<GbCtWorker> workerOnLines = gbCtWorkerMapper.getAllWorkerListByLineId(ctLine.getLineId());
            int gjOn = 0 ,ptOn = 0;//人数
            for (GbCtWorker worker : workerOnLines) {
                if("手工工人".equals(worker.getRecruitName())) {
                    ptOn++;
                }
                if("高级工人".equals(worker.getRecruitName())) {
                    gjOn++;
                }
            }
            int cplSeniorWorkerNeed =  cplSeniorWorker - gjOn;
            int cplOrdinaryWorkerNeed =  cplOrdinaryWorker - ptOn;
            if(((gj - cplSeniorWorkerNeed) >= 0) && ((pt - cplOrdinaryWorkerNeed) >= 0 )){
                //产量
                int production = gbCtGzProductLine.getCplProduction() * (100 + cplOrdinaryWorker * ptXl / 4 + cplSeniorWorker * gjXl) * classes.getOutputMulti()/100;
                //获取生产所需加工费
                int developFee = gbCtGzProduct.getCpDevelopFee() * production;
                Integer cash = gbCtCashflowService.getCash(userId, param.getContestId());
                int remain = cash - developFee;
                if (remain < 0) {
                    return ErrorEnum.CASH_NOT_ENOUGH.getR();
                }
                gj = gj - cplSeniorWorkerNeed;
                pt = pt - cplOrdinaryWorkerNeed;
                //工人分配
                for (int i = workerGj.size() - 1; i >= gj; i--) {
                    GbCtWorker gbCtWorker = workerGj.get(i);
                    gbCtWorker.setIsWork(GbCtWorker.ON_WORK)
                            .setLineId(ctLine.getLineId());
                    gbCtWorkerMapper.updateById(gbCtWorker);
                    workerGj.remove(gbCtWorker);
                }
                for (int i = workerPt.size() - 1; i >= pt; i--) {
                    GbCtWorker gbCtWorker = workerPt.get(i);
                    gbCtWorker.setIsWork(GbCtWorker.ON_WORK)
                            .setLineId(ctLine.getLineId());
                    gbCtWorkerMapper.updateById(gbCtWorker);
                    workerPt.remove(gbCtWorker);
                }




                //开始生产产品
                for (int i = 0; i < production; i++) {
                    int cashNeedOne = 0; //计算产品的成本
                    for (GbCtGzProducing gbCtGzProducing : gbCtGzProducings) {
                        int needNum = gbCtGzProducing.getCpNum();
                        if(needNum == 0){ continue;}

                        List<GbCtKcMaterial> materialList = kcMaterialMap.get(gbCtGzProducing.getCpMid());
                        for (GbCtKcMaterial material : materialList) {
//                            if(material.getImNum() == 0 ){
//                                continue;
//                            }
                            if(material.getImNum() - needNum > 0){
                                //计算成本，更新材料剩余数量
                                cashNeedOne = cashNeedOne + material.getMaterialPrice() * needNum;
                                material.setImNum(material.getImNum() - needNum);
                                needNum = 0;
                                gbCtKcMaterialMapper.updateById(material);
                                break;
                            }else{
                                //计算金额，删除数据（改为更新为0），结束当前循环
                                cashNeedOne = cashNeedOne + material.getMaterialPrice() * material.getImNum();
                                needNum = needNum - material.getImNum();
                                material.setImNum(0);
                                gbCtKcMaterialMapper.deleteById(material);
                            }
                        }
                        if(needNum != 0){//库存不足  直接原价购买
                            GbCtGzMaterial gbCtGzMaterial = materialGzMaps.get(gbCtGzProducing.getCpMid());
                            int fee = needNum * gbCtGzMaterial.getCmBuyFee();
                            cashNeedOne += fee;
                            //产生应付款账期
                            GbCtFee ctFee = new GbCtFee();
                            int paymentDate = DateUtils.addYearAndSeasonTime(param.getCurrentTime(),gbCtGzMaterial.getCmPayDate());
                            ctFee.setRFee(fee)
                                    .setType(GbCtFee.TO_PAYMENT)
                                    .setContestId(param.getContestId())
                                    .setStudentId(userId)
                                    .setBorrower("1")
                                    .setLender("供应商")
                                    .setPaymentDate(paymentDate)
                                    .setRRemainDate(gbCtGzMaterial.getCmPayDate())
                                    .setRemarks("原材料付款（智能生产）");
                            gbCtFeeMapper.insert(ctFee);

                        }
                    }
                    cashNeedOne += gbCtGzProduct.getCpDevelopFee();//加工费
                    //生成库存数据
                    GbCtKcProduct gbCtKcProduct = new GbCtKcProduct();
                    gbCtKcProduct.setContestId(param.getContestId())
                            .setStudentId(userId)
                            .setIpCpId(lineIdAndCpId.getCpId())
                            .setIpNum(1)
                            .setLineId(lineIdAndCpId.getLineId())
                            .setRealCost(cashNeedOne);
                    // 产品入库
                    if(gbCtGzProduct.getCpDevelopDate() != null &&  gbCtGzProduct.getCpDevelopDate()>0){
                        gbCtKcProduct.setIsInventory(GbCtKcProduct.NOT_IN);
                    }else {
                        gbCtKcProduct.setIsInventory(GbCtKcProduct.IS_IN);
                    }
                    gbCtKcProduct.setInventoryDate(productFinishTime);//入库时间
                    gbCtKcProductMapper.insert(gbCtKcProduct);
                }
                //插入现金交易记录
                GbCtCashflow ctCashflow = new GbCtCashflow();
                ctCashflow.setStudentId(userId)
                        .setContestId(param.getContestId())
                        .setCAction(CashFlowActionEnum.PRODUCT_PRODUCING.getAction())
                        .setCOut(developFee)
                        .setCIn(0)
                        .setCComment("生产线" + lineIdAndCpId.getLineId().toString()+"生产产品"+gbCtGzProduct.getCpName())
                        .setCDate(param.getCurrentTime())
                        .setCSurplus(remain);
                gbCtCashflowService.save(ctCashflow);
                //更新生产线
                if(gbCtGzProduct.getCpDevelopDate() != null &&  gbCtGzProduct.getCpDevelopDate()>0){
                    ctLine.setStatus(GbCtLine.ON_PRODUCE);
                }else {
                    ctLine.setStatus(GbCtLine.ON_SPACE);
                }
                ctLine.setPlProductAddDate(param.getCurrentTime())
                        .setPlProductingDate(productFinishTime)
                        .setRealProduction(production)
                        .setPlCpid(lineIdAndCpId.getCpId())
                        .setClassesId(param.getClassesId());
                gbCtLineMapper.updateById(ctLine);
            }
        }
        return R.success();
    }

    /**
     * 暂停竞赛
     * @param contestId
     * @return
     */
    public R pause(Integer contestId){
        // 持久化剩余时间
        long expire = redisCache.getExpire(RedisEnum.HHDJS+":" + contestId);
        //小于0说明redis已过期
        if(expire<0){
            expire=0;
        }
        GbContest gbContest = gbContestService.getById(contestId);
        gbContest.setRemainTime((int)expire);
        gbContestService.updateById(gbContest);
        // 清除任务
        this.stopContestScheduleTask(contestId);
        // 更新暂停状态为暂停
        gbContestMapper.pauseContest(contestId);
        return R.success();
    }

    /**
     * 恢复竞赛
     * @param contestId
     * @return
     */
    public R recover(Integer contestId){
        //添加调度任务
        GbContest gbContest = gbContestService.getById(contestId);
        Integer remainTime = gbContest.getRemainTime();

        //判断是否还有剩余时间,如果是没有剩余时间的情况下，点击恢复，则无需计时
        if(remainTime>0){
            this.addContestScheduleTask(contestId,remainTime);
        }
        //更新暂停状态
        gbContestMapper.recoverContest(contestId);
        return R.success();
    }


    /**
     * 上一回合
     * @param contestId
     * @return
     */
    public R lastRound(Integer contestId,Integer currentTime){
        Integer lastRoundTime = DateUtils.addYearAndSeasonTime(currentTime,-1);
//        //如果不是统一模式 只增加教师时间
//        if(!gbContestService.isSyn(contestId)){
//            GbContest contest = gbContestService.getById(contestId);
//            contest.setTeacherDate(lastRoundTime);
//            gbContestService.updateById(contest);
//            return R.success();
//        }

        // 暂停比赛
        this.pause(contestId);
        // 还原数据
        if(currentTime<=11){
            return ErrorEnum.NO_LAST_ROUND.getR();
        }
        // 全部还原学生数据
        gbRestoreDataBiz.restoreAll(contestId,currentTime,lastRoundTime);

        // 重新计时
        int seasonTime = getSeasonTime(contestId, lastRoundTime);

        //计算最后一次订货会的时间，取gb_ct_xz_order里面最大的年份 和 季度
         GbCtXzOrder gbCtXzOrder = gbCtXzOrderMapper.selectOne(new LambdaQueryWrapper<GbCtXzOrder>()
                .eq(GbCtXzOrder::getContestId,contestId)
                .orderByDesc(GbCtXzOrder::getDate)
                .orderByDesc(GbCtXzOrder::getQuarterly)
                .last("limit 1")
        );
        Integer lastFairOrderDate= 10;
         if(gbCtXzOrder!=null){
             lastFairOrderDate=gbCtXzOrder.getDate()*10+gbCtXzOrder.getQuarterly();
         }

        //更新gb_contest  剩余时间/暂停状态/教师当前时间
        //最后一次订货会时间
        gbContestMapper.updateById(new GbContest()
                .setContestId(contestId)
                .setRemainTime(seasonTime)
                //不暂停
                .setIsSuspend(0)
                .setTeacherDate(lastRoundTime)
                //设置最后一次订货会时间
                .setLastFairOrderDate(lastFairOrderDate)
                //订货会状态为0
                .setFairOrderState(0)
        );
         //倒计时开始
        this.addContestScheduleTask(contestId,seasonTime);
        return R.success();
    }



    /**
     * 下一回合
     * @param teacherId
     * @param contestId
     * @param currentTime
     * @return
     */
    public R nextRound(Integer teacherId,Integer contestId,Integer currentTime){
        log.error("点击下一回合,开始,contestId:{},currentTime:{}",contestId,currentTime);
        Integer nextSeason = DateUtils.addYearAndSeasonTime(currentTime,1);
        int season = currentTime%10;
        int year = currentTime/10;


        //获取符合进行下一回合条件的学生，两个条件：1.时间线与大部队一致 2.当年无还原
        List<GbContestStudent> studentList= gbContestStudentService.list(new LambdaQueryWrapper<GbContestStudent>()
                .eq(GbContestStudent::getContestId,contestId));
        List<GbContestStudent> nextRoundStudentList = studentList.stream().filter(item->{
            //学生的时间线
            int studentDate = item.getDate()/10;
            //学生最近一次还原的年份
            int lastRestoreYear = item.getLastRestoreTime()==null?0:item.getLastRestoreTime()/10;
            log.error("下一回合-学生:{},studentDate:{},最近一次还原的年份:{}",item.getStudentId(),item.getDate(),lastRestoreYear);
            log.error("下一回合-当前学生是否进入下一回合:{}",(studentDate==currentTime)&&(year!=lastRestoreYear));
            return (studentDate==currentTime)&&(year!=lastRestoreYear);
        }).collect(Collectors.toList());


        //跳至下一回合
        for (GbContestStudent gbContestStudent : nextRoundStudentList) {
            int status = gbContestStudent.getDate()%10;

            //小于等于4说明未进行当季/当年结束的操作，系统自动进行当季结束操作
            if(status <= 4){
                // 当季结束/当季结束   保存结束时间-并不会备份数据
                gbActionBiz.endingSeason(gbContestStudent.getStudentId(),contestId,currentTime);
            }

            //判断是否是某年的第四个季度。该季度需要填表
            if(season==4){
                // 判断当年报表是否填写，没有填写的话，自动填写，值全部填0.并标记报表错误年份
                GbCtCharges gbCtCharges = gbCtChargesService.getOne(new GbCtCharges()
                        .setStudentId(gbContestStudent.getStudentId()).setCDate(currentTime / 10).setContestId(contestId).setBsIsxt(0));

                //为空则需要自动填写报表
                if(null == gbCtCharges){
                    gbFillReportBiz.fillDefault(gbContestStudent.getStudentId(),currentTime/10,contestId);
                }
            }

            //判断是否结束,如果未结束，则开始到下一个季度
            if(currentTime<64){
                //开始下一个季度
                gbActionBiz.nextSeasonAction(gbContestStudent.getStudentId(),contestId,nextSeason);
            }else{
                gbContestStudent.setFinishTime(LocalDateTime.now());
                gbContestStudentService.updateById(gbContestStudent);
                log.error("比赛已结束,contestId",contestId);
            }
        }


        // 取消调度
        this.stopContestScheduleTask(contestId);

        if(currentTime<64){
            // 重新计算下一回合时间
            int seasonTime = getSeasonTime(contestId, nextSeason);
            //更新gb_contest  剩余时间/暂停状态/教师当前时间
            gbContestMapper.updateById(new GbContest()
                    .setContestId(contestId)
                    .setRemainTime(seasonTime)
                    .setIsSuspend(0)
                    .setTeacherDate(nextSeason)
                    //更新订货会状态
                    .setFairOrderState(0)
            );
            //执行调度
            this.addContestScheduleTask(contestId,seasonTime);
        }else{
            gbContestMapper.updateById(new GbContest()
                    .setContestId(contestId)
                    .setRemainTime(0)
                    .setIsSuspend(0)
                    .setTeacherDate(nextSeason)
            );
        }

        log.error("点击下一回合,结束,contestId:{},currentTime:{}",contestId,currentTime);
        return R.success();
    }



    /**
     * 还原本季
     * @param contestId
     * @param currentTime
     * @return
     */
    public R restoreCurrentRound(Integer contestId,Integer currentTime){

        // 暂停比赛
        this.pause(contestId);
        // 全部还原学生数据
        gbRestoreDataBiz.restoreAll(contestId,currentTime,currentTime);
        // 重新计时
        int seasonTime = getSeasonTime(contestId, currentTime);
        //计算最后一次订货会的时间，取gb_ct_xz_order里面最大的年份 和 季度
        GbCtXzOrder gbCtXzOrder = gbCtXzOrderMapper.selectOne(new LambdaQueryWrapper<GbCtXzOrder>()
                .orderByDesc(GbCtXzOrder::getDate)
                .orderByDesc(GbCtXzOrder::getQuarterly)
                .last("limit 1")
        );
        Integer lastFairOrderDate= 10;
        if(gbCtXzOrder!=null){
            lastFairOrderDate=gbCtXzOrder.getDate()*10+gbCtXzOrder.getQuarterly();
        }
        //更新gb_contest  剩余时间/暂停状态/教师当前时间
        //最后一次订货会时间
        gbContestMapper.updateById(new GbContest()
                .setContestId(contestId)
                .setRemainTime(seasonTime)
                //不暂停
                .setIsSuspend(0)
                .setTeacherDate(currentTime)
                //设置最后一次订货会时间
                .setLastFairOrderDate(lastFairOrderDate)
                //更新订货会状态
                .setFairOrderState(0)
        );
        //倒计时开始
        this.addContestScheduleTask(contestId,seasonTime);
        return R.success();
    }










    /**
     * 添加竞赛计时调度
     * @param contestId
     * @param seasonTime
     */
    public void addContestScheduleTask(Integer contestId,Integer seasonTime){
        scheduleTaskService.add(RedisEnum.HHDJS+":" + contestId, new Runnable() {
            @Override
            public void run() {
                // 更新暂停状态为暂停
                //时间到了需要暂停比赛
                gbContestMapper.pauseContest(contestId);
                gbContestMapper.updateById(new GbContest().setContestId(contestId).setRemainTime(0));
            }
        },seasonTime);
    }

    /**
     * 停止竞赛计时调度
     * @param contestId
     */
    public void stopContestScheduleTask(Integer contestId){
        scheduleTaskService.stop(RedisEnum.HHDJS+":" + contestId);
        GbContest contest = gbContestService.getById(contestId);
        int fairOrderState = contest.getFairOrderState();
        //如果定会会正在选单，暂停选单
        if(fairOrderState == 3){
            gbTradeFairBiz.pauseChoose(contestId,4);
        }
    }

    /**
     *设置各季度时间
     * @param contestId
     * @param countDown
     * @return
     */
    public R setSeasonTime(Integer contestId,String countDown){
        GbContest gbContest = gbContestService.getById(contestId);
        gbContest.setCountDown(countDown);
        gbContestService.updateById(gbContest);
        return R.success();
    }

    /**
     * 公共信息
     * @param contestId
     * @param year
     * @return
     */
    public R getPublicInfo(Integer contestId,Integer year){
        List<GbCtBalance> gbCtBalanceList = gbCtBalanceMapper.getListByContestId(contestId,year);//资产负债表
        List<GbCtCharges> gbCtChargesList = gbCtChargesMapper.getListByContestId(contestId, year);//综合费用表
        List<GbCtProfitChart> gbCtProfitChartsList = gbCtProfitChartMapper.getListByContestId(contestId, year);//利润表
        //塞入结束时间
        for (GbCtBalance gbCtBalance : gbCtBalanceList) {

            GbContestStudent gbContestStudent = gbContestStudentMapper.selectOne(new LambdaQueryWrapper<GbContestStudent>()
                    .eq(GbContestStudent::getContestId,contestId)
                    .eq(GbContestStudent::getStudentId,gbCtBalance.getStudentId())
                    .last("limit 1")
            );
            String everySeasonEndTime = gbContestStudent.getEverySeasonEndTime();
            if((everySeasonEndTime==null)||("".equals(everySeasonEndTime))){
                everySeasonEndTime="[]";
            }
//          String endTime = gbContestStudentService.getEndTime(contestId, gbCtBalance.getStudentId(), year*10+4);
            gbCtBalance.setEndTime1(getDateFinshTime(everySeasonEndTime,year*10+1));
            gbCtBalance.setEndTime2(getDateFinshTime(everySeasonEndTime,year*10+2));
            gbCtBalance.setEndTime3(getDateFinshTime(everySeasonEndTime,year*10+3));
            gbCtBalance.setEndTime(getDateFinshTime(everySeasonEndTime,year*10+4));
        }
        return Objects.requireNonNull(Objects.requireNonNull(R.success().
                put("gbCtBalanceList", gbCtBalanceList)).
                put("gbCtChargesList", gbCtChargesList)).
                put("gbCtProfitChartsList",gbCtProfitChartsList);
    }


    /**
     * 根据时间获取结束时间
     * @param everySeasonEndTime
     * @param dateTime
     * @return
     */
    public String getDateFinshTime(String everySeasonEndTime,int dateTime){

        JSONArray jsonArray = JSONArray.parseArray(everySeasonEndTime);

        String targetTime = null;
        for (int i = 0; i < jsonArray.size(); i++) {
            JSONObject jsonObject = jsonArray.getJSONObject(i);
            int date = jsonObject.getIntValue("date");
            String time = jsonObject.getString("time");
            if (date == dateTime) {
                targetTime = time;
                break;
            }
        }
        //如果没有对应的结束时间，则为空
        if(targetTime==null){
            return "";
        }else{
            return targetTime;
        }
    }

    /**
     * 企业信息
     * @param
     * @param
     * @return
     */
    public R getCompanyInfo(GbCompanyParam gbCompanyParam){
        Integer studentId = gbCompanyParam.getStudentId();
        Integer contestId = gbCompanyParam.getContestId();
        List<?> list = new ArrayList<>();
        String type = gbCompanyParam.getType();
        switch (type) {
            case "原料订购":
                list = gbCtDdMaterialMapper.list(contestId, studentId);
                break;
            case "产品库存":
                list = gbCtKcProductMapper.listKc(contestId, studentId);
                break;
            case "原料库存":
                list = gbCtKcMaterialMapper.listKcGroupNumber(contestId, studentId);
                break;
            case "应付款":
                list = gbCtFeeMapper.getPayList(studentId, contestId);
                break;
            case "应收款":
                list = gbCtFeeMapper.getRecList(studentId, contestId);
                break;
            case "贷款信息":
                list = gbCtBankLoanMapper.getList(contestId, studentId);
                break;
            case "市场开拓":
                list = gbCtYfMarketMapper.getList(contestId, studentId);
                break;
            case "产品研发":
                list = gbCtYfProductMapper.getList(contestId, studentId);
                break;
            case "ISO认证":
                list = gbCtYfIsoMapper.getList(contestId, studentId);
                break;
            case "生产线信息":
                list = gbCtLineMapper.getList(contestId, studentId);
                break;
            case "订单信息":
                list = gbCtXzOrderMapper.getList(contestId, studentId);
                break;
            case "现金流量表":
                list = gbCtCashflowMapper.getList(contestId, studentId);
                break;
            default:
                return ErrorEnum.CHOOSE_TYEP_WRONG.getR();
        }
        return R.success().put("list",list);
    }

    /**
     * 获取季度时间
     * @param contestId
     * @param date
     * @return
     */
    public  int getSeasonTime(Integer contestId,Integer date){
        int index = (date/10-1)*4 + date%10 -1;
        GbContest gbContest = gbContestService.getById(contestId);
        String countDown = gbContest.getCountDown();
        JSONArray jsonArray = JSONArray.parseArray(countDown);
        return (Integer) jsonArray.get(index)*60;
    }

    /**
     * 公告下发
     * @param gbCtAnnouncement
     * @return
     */
    public R addAnnouncement(GbCtAnnouncement gbCtAnnouncement) throws IOException {
        //获取学生表中时间最大的值
        Integer maxDate = gbContestStudentService.getMaxDate(gbCtAnnouncement.getContestId());
        //获取最大的结束时间
        Integer maxFinishDate = gbContestStudentService.getMaxFinishDate(gbCtAnnouncement.getContestId());
        //获取广告投放的最大时间
        Integer maxAdDate = gbContestStudentService.getMaxAdDate(gbCtAnnouncement.getContestId());


        //报表时间取当前进度最达到的学生的时间
        gbCtAnnouncement.setAnDate(maxDate);

        String tempFilePath = CommonUtil.getCatalinaHome() + environment.getProperty("teaching-platform.temp-excel");


        //广告
        if("1".equals(gbCtAnnouncement.getAdvertising())){
            String uniqueFileName = CommonUtil.getUniqueFileName(gbCtAnnouncement.getContestId()+maxAdDate+"所有广告");
            uniqueFileName = uniqueFileName+"--"+DateUtils.dateToCN(maxAdDate)+"广告下发";
            File excelFile = FileUtil.createExcelFile(tempFilePath, uniqueFileName);
            OutputStream out = new FileOutputStream(excelFile,true);
            gbDownloadBiz.adAll(out,gbCtAnnouncement.getContestId(),maxAdDate);
            out.close();
            gbCtAnnouncement.setAdvertising(excelFile.getName());
        }
        //财务报表
        if("1".equals(gbCtAnnouncement.getFinancial())){
            String uniqueFileName = CommonUtil.getUniqueFileName(gbCtAnnouncement.getContestId()+maxFinishDate+"所有企业报表");
            uniqueFileName = uniqueFileName+"--第"+maxFinishDate+"年企业报表";
            File excelFile = FileUtil.createExcelFile(tempFilePath, uniqueFileName);
            OutputStream out = new FileOutputStream(excelFile,true);
            gbDownloadBiz.allFinancial(out,gbCtAnnouncement.getContestId(),maxFinishDate);
            out.close();
            gbCtAnnouncement.setFinancial(excelFile.getName());
        }
        //一键下载
        if("1".equals(gbCtAnnouncement.getAllData())){
            String uniqueFileName = CommonUtil.getUniqueFileName(gbCtAnnouncement.getContestId()+gbCtAnnouncement.getAnDate()+"所有竞赛数据");
            uniqueFileName = uniqueFileName+"--"+DateUtils.dateToCN(maxDate)+"一键下载";
            File zipFile = FileUtil.createZipFile(tempFilePath, uniqueFileName);
            List<Integer> studentIdList = gbContestStudentService.getStudentIdList(gbCtAnnouncement.getContestId());
            Map<Integer, String> studentIdAndAccountMap = gbContestStudentService.getStudentIdAndAccountMap(gbCtAnnouncement.getContestId());
            //临时文件夹
            String uniqueDir = tempFilePath + CommonUtil.getUniqueFileName(gbCtAnnouncement.getContestId()+maxDate+"") + "/";
            List<String> filesNames = new ArrayList<>();
            List<File> files = new ArrayList<>();
            for (Integer studentId : studentIdList) {
                String fileName = studentIdAndAccountMap.get(studentId)+ ExcelTypeEnum.XLSX.getValue();;
                gbDownloadBiz.saveResultNoCashFlowToFile(gbCtAnnouncement.getContestId(), studentId,fileName,uniqueDir);
                filesNames.add(fileName);
                File temp = new File(uniqueDir+fileName);
                files.add(temp);
            }
            FileUtil.saveFileToZip(files,zipFile,filesNames);
            files.forEach(File::delete);
            gbCtAnnouncement.setAllData(zipFile.getName());
        }
        gbCtAnnouncement.setCreateTime(LocalDateTime.now());
        gbCtAnnouncementMapper.insert(gbCtAnnouncement);
        return R.success();
    }


    /**
     * 获取公告信息
     * @param contestId
     * @return
     */
    public R getAnnouncement(Integer contestId) {
        QueryWrapper<GbCtAnnouncement> qw = new QueryWrapper<>();
        qw.eq("contest_id",contestId);
        List<GbCtAnnouncement> gbCtAnnouncements = gbCtAnnouncementMapper.selectList(qw);
        return R.success().put("list",gbCtAnnouncements);
    }

    /**
     * 生成复盘数据
     * @param contestId
     * @return
     */
    public void generateSubject(HttpServletResponse response,Integer contestId,Integer subjectNumber) {
        gbDownloadBiz.downAutoSubjectModelFile(response,contestId,subjectNumber);
    }


    /***
     * 获取可还原时间
     * @param contestId
     * @param studentId
     * @return
     */
    public R getRestoreTime(Integer contestId, Integer studentId) {
        List<Integer> restoreTime = gbContestStudentMapper.getRestoreTime(contestId, studentId);
        GbContest contest = gbContestService.getById(contestId);
        //获取最后一次的 选单记录
        GbCtXzOrder gbCtXzOrder= gbCtXzOrderMapper.selectOne(new LambdaQueryWrapper<GbCtXzOrder>()
                .eq(GbCtXzOrder::getContestId,contestId)
                .orderByDesc(GbCtXzOrder::getDate)
                .orderByDesc(GbCtXzOrder::getQuarterly)
                .last("limit 1")
        );

        //计算最后一次订货会的时间
        Integer lastFairOrderDate = 11;
        if(gbCtXzOrder!=null){
            lastFairOrderDate = gbCtXzOrder.getDate()*10+gbCtXzOrder.getQuarterly();
        }
        for (int i = restoreTime.size() - 1; i >= 0; i--) {
            if(restoreTime.get(i) < lastFairOrderDate){
                restoreTime.remove(i);
            }
        }
        return R.success().put("list",restoreTime);
    }


    /**
     * 获取市场预测
     * @param contestId
     * @return
     */
    public R marketForecast(Integer contestId) {
        List<GbMarketForecast> table1= gbContestMapper.marketForecast1(contestId);
        List<GbMarketForecast> table2= gbContestMapper.marketForecast2(contestId);
        List<GbMarketForecast> table3= gbContestMapper.marketForecast3(contestId);
        return R.success()
                .put("table1",table1)
                .put("table2",table2)
                .put("table3",table3)
                ;
    }
}
