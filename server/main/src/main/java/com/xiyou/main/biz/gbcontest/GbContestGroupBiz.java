package com.xiyou.main.biz.gbcontest;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.xiyou.common.enums.CodeEnum;
import com.xiyou.common.utils.R;
import com.xiyou.main.entity.gbcontest.GbContestGroup;
import com.xiyou.main.service.gbcontest.GbContestGroupService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @program: multi-module
 * @description:
 * @author: tangcan
 * @create: 2019-07-09 12:22
 **/
@Service
@Transactional(rollbackFor = Exception.class)
public class GbContestGroupBiz {
    @Autowired
    private GbContestGroupService gbContestGroupService;

    public R list() {
        Map<String, Object> returnMap = new HashMap<>();
        returnMap.put("list", gbContestGroupService.list());
        return R.success(returnMap);
    }

    public R add(GbContestGroup contestGroup) {
        QueryWrapper<GbContestGroup> wrapper = new QueryWrapper<>();
        wrapper.eq("group_name", contestGroup.getGroupName());
        if (gbContestGroupService.getOne(wrapper) != null) {
            return R.error(CodeEnum.PARAM_ERROR.getCode(), "该分组已存在");
        }
        gbContestGroupService.insert(contestGroup);
        GbContestGroup group = gbContestGroupService.getOne(wrapper);
        Map<String, Object> returnMap = new HashMap<>();
        returnMap.put("id", group.getId());
        return R.success(returnMap);
    }

    public R update(GbContestGroup contestGroup) {
        if (gbContestGroupService.checkNameExist(contestGroup)) {
            return R.error(CodeEnum.PARAM_ERROR.getCode(), "该分组已存在");
        }
        gbContestGroupService.updateById(contestGroup);
        return R.success();
    }

    public R delete(Integer[] ids) {
        gbContestGroupService.removeByIds(Arrays.asList(ids));
        return R.success();
    }

    public R listByteacher(Integer teacherId) {
        List<GbContestGroup> list = gbContestGroupService.getListByTeacher(teacherId);
        return R.success().put("list", list);
    }
}
