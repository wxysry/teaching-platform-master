package com.xiyou.main.biz.gbcontest;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.xiyou.common.enums.CodeEnum;
import com.xiyou.common.utils.R;
import com.xiyou.main.entity.exam.SysUser;
import com.xiyou.main.entity.gbcontest.GbCtCashflow;
import com.xiyou.main.entity.gbcontest.GbCtMnAd;
import com.xiyou.main.enums.ErrorEnum;
import com.xiyou.main.service.exam.SysUserService;
import com.xiyou.main.service.gbcontest.GbCtCashflowService;
import com.xiyou.main.service.gbcontest.GbCtMnAdService;
import com.xiyou.main.service.gbcontest.GbCtYfMarketService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @program: multi-module
 * @description:
 * @author: tangcan
 * @create: 2019-07-25 15:03
 **/
@Service
@Slf4j
@Transactional(rollbackFor = Exception.class)
public class GbDeliverAdsBiz {
    @Autowired
    private GbCtYfMarketService gbCtYfMarketService;
    @Autowired
    private GbCtMnAdService gbCtMnAdService;
    @Autowired
    private GbCtCashflowService gbCtCashflowService;

    @Autowired
    private SysUserService sysUserService;

    @Autowired
    private GbActionBiz gbActionBiz;


    //已研发完成的市场列表
    public R finishYfMarket(Integer studentId, Integer contestId) {
        List<String> hadYfFinishMarketList = gbCtYfMarketService.getHadYfFinishMarkets(studentId, contestId);
        Map<String, Object> returnMap = new HashMap<>();
        returnMap.put("marketList", hadYfFinishMarketList);
        return R.success(returnMap);
    }



    public R deliverAds(Integer studentId, GbCtMnAd mnAd) {
        if (mnAd.getStudentId() == null) {
            mnAd.setStudentId(studentId);
        }
        log.error("广告投放:"+ JSONObject.toJSONString(mnAd));
        int now = mnAd.getYear();
        //年
        if (mnAd.getYear() > 10) {
            mnAd.setYear(mnAd.getYear() / 10);
        }

        log.error("广告投放,studentId："+studentId+",contestId:"+mnAd.getContestId()+",年"+mnAd.getYear()+"季"+mnAd.getQuarterly());

        // 所有填写金额之和
        double cashSum = mnAd.calSum();
        // 现金
        int cash = gbCtCashflowService.getCash(mnAd.getStudentId(), mnAd.getContestId());


        //判断是否提交
        boolean isSubmit = "Y".equals(mnAd.getIsSubmit());
        // 如果大于，则报错【现金不足】
        if(isSubmit) {
            if (cashSum > cash ) {
                return R.error(CodeEnum.OTHER_ERROR.getCode(), "现金不足");
            }
        }

        // 设置组号为学生姓名,记录写入mn_ad
        SysUser sysUser = sysUserService.getById(studentId);
        mnAd.setGroupNum(sysUser.getAccount());
        mnAd.setCreateTime(LocalDateTime.now());


        List<GbCtMnAd> gbCtMnAdList = gbCtMnAdService.list(new LambdaQueryWrapper<GbCtMnAd>()
                .eq(GbCtMnAd::getContestId,mnAd.getContestId())
                .eq(GbCtMnAd::getStudentId,studentId)
                .eq(GbCtMnAd::getYear,mnAd.getYear())
                .eq(GbCtMnAd::getQuarterly,mnAd.getQuarterly())
                .eq(GbCtMnAd::getGroupNum,mnAd.getGroupNum())
                //本年本季已投放广告
                .eq(GbCtMnAd::getIsSubmit,"Y")
        );
        if(gbCtMnAdList!=null && gbCtMnAdList.size()>0){
            //本季度已投放广告不可重复投放
            return ErrorEnum.YTF_GG.getR();
        }

        //如果系统中已有则保存，否则走更新逻辑
        if(StringUtils.isEmpty(mnAd.getId())){
            gbCtMnAdService.save(mnAd);
        }else{
            gbCtMnAdService.updateById(mnAd);
        }
        log.error("广告投放扣减现金流量表1,studentId："+studentId+",contestId:"+mnAd.getContestId()+",cashSum:"+cashSum+"isSubmit:"+isSubmit);
        //已提交则，生成现金流量表
        if(isSubmit) {
            log.error("广告投放扣减现金流量表2,studentId："+studentId+",contestId:"+mnAd.getContestId()+",cashSum:"+cashSum);
            // Cashflow表写入ID，支付广告费，0，所有填写金额之和，现金+流入-流出，投放广告费，当前时间
            gbCtCashflowService.save(new GbCtCashflow().setStudentId(studentId)
                    .setContestId(mnAd.getContestId())
                    .setCAction("支付广告费")
                    .setCIn(0)
                    .setCOut((int) cashSum)
                    .setCSurplus(cash - (int) cashSum)
                    .setCComment("投放广告费" + (int) cashSum + "元")
                    .setCDate(now));
            //计算报表,判断是否破产
            gbActionBiz.isBankruptcy(studentId, mnAd.getContestId(), now);
            log.error("广告投放扣减现金流量表3,studentId："+studentId+",contestId:"+mnAd.getContestId()+",cashSum:"+cashSum);
            return R.success();
        }
        return R.success();
    }



    /**
     * 获取当前学生本季度投放的广告
     * @param contestId
     * @param userId
     * @param currentTime
     * @return
     */
    public R getFillAd(Integer contestId, Integer userId, Integer currentTime) {
        SysUser sysUser = sysUserService.getById(userId);
        GbCtMnAd ctMnAd = gbCtMnAdService.getOne(new LambdaQueryWrapper<GbCtMnAd>()
                .eq(GbCtMnAd::getStudentId,userId)
                .eq(GbCtMnAd::getContestId,contestId)
                .eq(GbCtMnAd::getYear,currentTime/10)
                .eq(GbCtMnAd::getQuarterly,currentTime%10)
                .eq(GbCtMnAd::getGroupNum, sysUser.getAccount()) //组号
        );
        return R.success().put("data",ctMnAd);
    }
}
