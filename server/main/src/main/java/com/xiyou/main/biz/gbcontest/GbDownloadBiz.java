package com.xiyou.main.biz.gbcontest;

import com.alibaba.excel.ExcelWriter;
import com.alibaba.excel.metadata.*;
import com.alibaba.excel.support.ExcelTypeEnum;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.xiyou.common.office.utils.EasyPOIUtil;
import com.xiyou.common.office.utils.FileUtil;
import com.xiyou.common.utils.CommonUtil;
import com.xiyou.main.async.gbcontest.GbContestResultAsyncService;
import com.xiyou.main.dao.gbcontest.*;
import com.xiyou.main.entity.gbcontest.*;
import com.xiyou.main.service.gbcontest.GbContestStudentService;
import com.xiyou.main.service.gbcontest.GbCtMnAdService;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.util.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.net.URLEncoder;
import java.util.*;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.stream.Collectors;

/**
 * @program: multi-module
 * @description:
 * @author: tangcan
 * @create: 2019-08-28 14:23
 **/
@Service
@Transactional(rollbackFor = Exception.class)
public class GbDownloadBiz {

    @Autowired
    private GbCtMnAdService gbCtMnAdService;
    @Autowired
    private GbContestStudentMapper contestStudentMapper;
    @Autowired
    private GbContestResultAsyncService contestResultAsyncService;
    @Autowired
    private GbCtMnAdMapper gbCtMnAdMapper;
    @Autowired
    private GbCtBalanceMapper gbCtBalanceMapper;
    @Autowired
    private GbCtProfitChartMapper gbCtProfitChartMapper;
    @Autowired
    private GbCtChargesMapper gbCtChargesMapper;
    @Autowired
    private GbContestStudentService gbContestStudentService;
    @Autowired
    private GbCtGzCsMapper gbCtGzCsMapper;
    @Autowired
    private GbCtGzAdMapper gbCtGzAdMapper;
    @Autowired
    private GbCtGzIsoMapper gbCtGzIsoMapper;
    @Autowired
    private GbCtGzOrderMapper gbCtGzOrderMapper;
    @Autowired
    private GbCtGzMarketMapper gbCtGzMarketMapper;
    @Autowired
    private GbCtGzMaterialMapper gbCtGzMaterialMapper;
    @Autowired
    private GbCtGzProductMapper gbCtGzProductMapper;
    @Autowired
    private GbCtGzProducingMapper gbCtGzProducingMapper;
    @Autowired
    private GbCtGzProductLineMapper gbCtGzProductLineMapper;
    @Autowired
    private GbCtGzProductDesignMapper gbCtGzProductDesignMapper;
    @Autowired
    private GbCtGzWorkerRecruitMapper gbCtGzWorkerRecruitMapper;
    @Autowired
    private GbCtGzWorkerTrainMapper gbCtGzWorkerTrainMapper;
    @Autowired
    private GbCtGzLoanMapper gbCtGzLoanMapper;
    @Autowired
    private GbCtGzDiscountMapper gbCtGzDiscountMapper;
    @Autowired
    private GbCtGzClassesMapper gbCtGzClassesMapper;
    @Autowired
    private GbCtGzWorkerIncentiveMapper gbCtGzWorkerIncentiveMapper;
    @Autowired
    private GbCtGzNumMapper gbCtGzNumMapper;
    @Autowired
    private GbContestMapper gbContestMapper;
    @Autowired
    private GbCtMnChooseMapper gbCtMnChooseMapper;
    @Autowired
    Environment environment;

    /**
     * 下载其他组广告
     * @param response
     * @param studentId
     * @param contestId
     * @param date
     */
    public void ad(HttpServletResponse response, Integer studentId, Integer contestId, Integer date) {
        String name = "第" + date / 10 + "年"+date%10+"季广告投放";
        try {
            response.setHeader("Content-Disposition", "attachment;filename=" + URLEncoder.encode(name + ".xlsx", "utf-8"));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        ExcelWriter writer = null;
        try {
            writer = new ExcelWriter(response.getOutputStream(), ExcelTypeEnum.XLSX);
        } catch (IOException e) {
            e.printStackTrace();
        }
        assert writer != null;

        /*
        获取广告投放情况
         */
        List<GbCtMnAd> ctMnAdList = gbCtMnAdService.getAdList(contestId, studentId, date / 10,date % 10);
        if (ctMnAdList.size() == 0) {
            ctMnAdList.add(new GbCtMnAd());
        }

        // 设置每列宽度
        Map<Integer, Integer> columnWidth = new HashMap<>();
        for (int i = 0; i < 6; i++) {
            columnWidth.put(i, 4000);
        }

        Sheet sheet = new Sheet(1, 0);
        sheet.setColumnWidthMap(columnWidth);
        sheet.setSheetName(name);
        sheet.setStartRow(1);   // 前面空一行

        /*
        表格式
         */
        TableStyle tableStyle = new TableStyle();
        Font font = new Font();
        font.setFontName("Arial");
        font.setFontHeightInPoints((short) 10);
        tableStyle.setTableHeadFont(font);
        tableStyle.setTableHeadBackGroundColor(IndexedColors.GREY_25_PERCENT);
        tableStyle.setTableContentFont(font);
        tableStyle.setTableContentBackGroundColor(IndexedColors.WHITE);

        // 表格数
        int i = 0;
        String[] strs = {"产品", "本地", "区域", "国内", "亚洲", "国际"};
        for (GbCtMnAd ctMnAd : ctMnAdList) {
            List<List<String>> head = new ArrayList<>();
            String title = ctMnAd.getGroupNum() + "广告投放情况";
            for (int j = 0; j < 6; j++) {
                List<String> headCoulumn = new ArrayList<>();
                // 第一行的头（名称一样则自动合并成一个单元格）
                headCoulumn.add(title);
                // 第二行的头
                headCoulumn.add(strs[j]);
                head.add(headCoulumn);
            }

            List<List<String>> data = new ArrayList<>();
            data.add(Arrays.asList("P1",
                    this.convertToString(ctMnAd.getLocalP1()),
                    this.convertToString(ctMnAd.getRegionalP1()),
                    this.convertToString(ctMnAd.getNationalP1()),
                    this.convertToString(ctMnAd.getAsianP1()),
                    this.convertToString(ctMnAd.getInternationalP1())));

            data.add(Arrays.asList("P2",
                    this.convertToString(ctMnAd.getLocalP2()),
                    this.convertToString(ctMnAd.getRegionalP2()),
                    this.convertToString(ctMnAd.getNationalP2()),
                    this.convertToString(ctMnAd.getAsianP2()),
                    this.convertToString(ctMnAd.getInternationalP2())));

            data.add(Arrays.asList("P3",
                    this.convertToString(ctMnAd.getLocalP3()),
                    this.convertToString(ctMnAd.getRegionalP3()),
                    this.convertToString(ctMnAd.getNationalP3()),
                    this.convertToString(ctMnAd.getAsianP3()),
                    this.convertToString(ctMnAd.getInternationalP3())));

            data.add(Arrays.asList("P4",
                    this.convertToString(ctMnAd.getLocalP4()),
                    this.convertToString(ctMnAd.getRegionalP4()),
                    this.convertToString(ctMnAd.getNationalP4()),
                    this.convertToString(ctMnAd.getAsianP4()),
                    this.convertToString(ctMnAd.getInternationalP4())));

            data.add(Arrays.asList("P5",
                    this.convertToString(ctMnAd.getLocalP5()),
                    this.convertToString(ctMnAd.getRegionalP5()),
                    this.convertToString(ctMnAd.getNationalP5()),
                    this.convertToString(ctMnAd.getAsianP5()),
                    this.convertToString(ctMnAd.getInternationalP5())));

            Table table1 = new Table(++i);
            table1.setHead(head);
            table1.setTableStyle(tableStyle);
            writer.write0(data, sheet, table1);
        }
        writer.finish();
    }

    /***
     * 字符转换
     * @param localP1
     * @return
     */
    private String convertToString(Double localP1) {
        return localP1 == null ? "0" : String.valueOf((int) Math.floor(localP1));
    }

    /***
     * 下载学生竞赛结果
     * @param response
     * @param contestId
     * @param studentId
     */
    public void downloadResult(HttpServletResponse response, Integer contestId, Integer studentId,String fileName) {
        GbContestStudent contestStudent = contestStudentMapper.get(contestId, studentId);
        if (contestStudent == null) {
            return;
        }

        Integer date = contestStudent.getDate() == null ? null : (contestStudent.getDate() / 10);

        // 库存信息
        Map<String, Object> kcxx;
        Future<Map<String, Object>> kcxxFuture = contestResultAsyncService.getKCXX(contestId, studentId);

        // 银行贷款
        Map<String, Object> yhdk;
        Future<Map<String, Object>> yhdkFuture = contestResultAsyncService.getYHDK(contestId, studentId, date);

        // 研发认证
        Map<String, Object> yfrz;
        Future<Map<String, Object>> yfrzFuture = contestResultAsyncService.getYFRZ(contestId, studentId);

        // 厂房与生产线
        Map<String, Object> cfyscx;
        Future<Map<String, Object>> cfyscxFuture = contestResultAsyncService.getCFYSCX(contestId, studentId);

        // 订单信息
        Map<String, Object> ddxx;
        Future<Map<String, Object>> ddxxFuture = contestResultAsyncService.getDDXX(contestId, studentId, date);

        // 现金流量表
        Map<String, Object> xjllb;
        Future<Map<String, Object>> xjllbFuture = contestResultAsyncService.getXJLLB(contestId, studentId);

        // 企业财务报表
        Map<String, Object> qycwbb;
        Future<Map<String, Object>> qycwbbFuture = contestResultAsyncService.getQYCWBB(contestId, studentId);

        // 四年广告投放
//        Map<String, Object> ggtf;
//        Future<Map<String, Object>> ggtfFuture = contestResultAsyncService.getGGTF(contestId, studentId);

        // 所有异步任务执行完成
        while (true) {
            if (kcxxFuture.isDone() &&
                    yhdkFuture.isDone() &&
                    yfrzFuture.isDone() &&
                    cfyscxFuture.isDone() &&
                    ddxxFuture.isDone() &&
                    xjllbFuture.isDone() &&
                    qycwbbFuture.isDone()
//                    && ggtfFuture.isDone()
            ) {
                break;
            }
        }

        // 获取异步任务返回的数据
        try {
            kcxx = kcxxFuture.get();
            yhdk = yhdkFuture.get();
            cfyscx = cfyscxFuture.get();
            ddxx = ddxxFuture.get();
            yfrz = yfrzFuture.get();
            xjllb = xjllbFuture.get();
            qycwbb = qycwbbFuture.get();
//            ggtf = ggtfFuture.get();
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
            return;
        }

        /*
        数据封装
         */
        Map<String, Object> param = new HashMap<>();
        param.putAll(kcxx);
        param.putAll(yhdk);
        param.putAll(yfrz);
        param.putAll(cfyscx);
        param.putAll(ddxx);
        param.putAll(xjllb);
        param.putAll(qycwbb);
//        param.putAll(ggtf);

        String filePath = CommonUtil.getClassesPath() + "templates/gb_contest_result.xlsx";
        Map<Integer, String> studentIdAndAccountMap = gbContestStudentService.getStudentIdAndAccountMap(contestId);
        if(fileName == null){
            fileName = "竞赛数据-" + studentIdAndAccountMap.get(studentId) + ".xlsx";
        }else {
            fileName += "-" + studentIdAndAccountMap.get(studentId) + ".xlsx";
        }
        try {
            EasyPOIUtil.exportMultiSheetExcelByMap(response, fileName + ".xlsx", filePath, param, true);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    /***
     * 企业报表
     * @param outputStream
     * @param contestId
     * @param year
     */
    public void allFinancial(OutputStream outputStream,Integer contestId, Integer year) {
        ExcelWriter writer = new ExcelWriter(outputStream, ExcelTypeEnum.XLSX);
        Map<Integer, String> studentIdAndAccountMap = gbContestStudentService.getStudentIdAndAccountMap(contestId);
        /*
        获取综合费用表
         */

        {
            List<GbCtCharges> gbCtChargesList = gbCtChargesMapper.getListByContestId(contestId, year);//综合费用表
            if (gbCtChargesList.size() == 0) {
                gbCtChargesList.add(new GbCtCharges());
            }
            // 设置每列宽度
            Map<Integer, Integer> columnWidth = new HashMap<>();
            for (int i = 0; i <= gbCtChargesList.size(); i++) {
                columnWidth.put(i, 4000);
            }

            Sheet sheet = new Sheet(1, 0);
            sheet.setColumnWidthMap(columnWidth);
            sheet.setSheetName("综合费用表");
            sheet.setStartRow(1);   // 前面空一行

            /*
            表格式
             */
            TableStyle tableStyle = new TableStyle();
            Font font = new Font();
            font.setFontName("Arial");
            font.setFontHeightInPoints((short) 10);
            tableStyle.setTableHeadFont(font);
            tableStyle.setTableHeadBackGroundColor(IndexedColors.GREY_25_PERCENT);
            tableStyle.setTableContentFont(font);
            tableStyle.setTableContentBackGroundColor(IndexedColors.WHITE);

            Table table1 = new Table(1);
            String title = "综合费用表";
            List<List<String>> head = new ArrayList<>();
            List<String> headCoulumn = new ArrayList<>();
            headCoulumn.add(title);
            headCoulumn.add("组号");
            head.add(headCoulumn);
            for (GbCtCharges gbCtCharges : gbCtChargesList) {
                List<String> headCoulumn1 = new ArrayList<>();
                headCoulumn1.add(title);
                headCoulumn1.add(studentIdAndAccountMap.get(gbCtCharges.getStudentId()));
                head.add(headCoulumn1);
            }
            table1.setHead(head);
            table1.setTableStyle(tableStyle);

            List<List<String>> data = new ArrayList<>();
            List<String> data1 = new ArrayList<>();
            data.add(data1);
            data1.add("管理费");
            data1.addAll(gbCtChargesList.stream().map(k->convertObjToString(k.getCOverhaul())).collect(Collectors.toList()));
            List<String> data2 = new ArrayList<>();
            data.add(data2);
            data2.add("广告费");
            data2.addAll(gbCtChargesList.stream().map(k->convertObjToString(k.getCAd())).collect(Collectors.toList()));
            List<String> data3 = new ArrayList<>();
            data.add(data3);
            data3.add("产线维修费");
            data3.addAll(gbCtChargesList.stream().map(k->convertObjToString(k.getCMaintenance())).collect(Collectors.toList()));
            List<String> data4 = new ArrayList<>();
            data.add(data4);
            data4.add("转产费");
            data4.addAll(gbCtChargesList.stream().map(k->convertObjToString(k.getCTransfer())).collect(Collectors.toList()));
            List<String> data5 = new ArrayList<>();
            data.add(data5);
            data5.add("市场开拓费");
            data5.addAll(gbCtChargesList.stream().map(k->convertObjToString(k.getCDevelopMarket())).collect(Collectors.toList()));
            List<String> data6 = new ArrayList<>();
            data.add(data6);
            data6.add("产品资质申请");
            data6.addAll(gbCtChargesList.stream().map(k->convertObjToString(k.getCDevelopProduct())).collect(Collectors.toList()));
            List<String> data7 = new ArrayList<>();
            data.add(data7);
            data7.add("ISO认证申请");
            data7.addAll(gbCtChargesList.stream().map(k->convertObjToString(k.getCDevelopIso())).collect(Collectors.toList()));
            List<String> data8 = new ArrayList<>();
            data.add(data8);
            data8.add("信息费");
            data8.addAll(gbCtChargesList.stream().map(k->convertObjToString(k.getCInformation())).collect(Collectors.toList()));
            List<String> data9 = new ArrayList<>();
            data.add(data9);
            data9.add("数字化研发费");
            data9.addAll(gbCtChargesList.stream().map(k->convertObjToString(k.getCDigitalization())).collect(Collectors.toList()));
            List<String> data10 = new ArrayList<>();
            data.add(data10);
            data10.add("合计");
            data10.addAll(gbCtChargesList.stream().map(k->convertObjToString(k.getCTotal())).collect(Collectors.toList()));

            writer.write0(data, sheet, table1);
        }
        /*
        获取利润表
         */
        {
            List<GbCtProfitChart> gbCtProfitChartsList = gbCtProfitChartMapper.getListByContestId(contestId, year);//利润表
            if (gbCtProfitChartsList.size() == 0) {
                gbCtProfitChartsList.add(new GbCtProfitChart());
            }
            // 设置每列宽度
            Map<Integer, Integer> columnWidth = new HashMap<>();
            for (int i = 0; i < gbCtProfitChartsList.size(); i++) {
                columnWidth.put(i, 4000);
            }

            Sheet sheet = new Sheet(2, 0);
            sheet.setColumnWidthMap(columnWidth);
            sheet.setSheetName("利润表");
            sheet.setStartRow(1);   // 前面空一行

            /*
            表格式
             */
            TableStyle tableStyle = new TableStyle();
            Font font = new Font();
            font.setFontName("Arial");
            font.setFontHeightInPoints((short) 10);
            tableStyle.setTableHeadFont(font);
            tableStyle.setTableHeadBackGroundColor(IndexedColors.GREY_25_PERCENT);
            tableStyle.setTableContentFont(font);
            tableStyle.setTableContentBackGroundColor(IndexedColors.WHITE);

            Table table1 = new Table(1);
            String title = "利润表";
            List<List<String>> head = new ArrayList<>();
            List<String> headCoulumn = new ArrayList<>();
            headCoulumn.add(title);
            headCoulumn.add("组号");
            head.add(headCoulumn);
            for (GbCtProfitChart gbCtProfitChart : gbCtProfitChartsList) {
                List<String> headCoulumn1 = new ArrayList<>();
                headCoulumn1.add(title);
                headCoulumn1.add(studentIdAndAccountMap.get(gbCtProfitChart.getStudentId()));
                head.add(headCoulumn1);
            }
            table1.setHead(head);
            table1.setTableStyle(tableStyle);

            List<List<String>> data = new ArrayList<>();
            List<String> data1 = new ArrayList<>();
            data.add(data1);
            data1.add("销售收入");
            data1.addAll(gbCtProfitChartsList.stream().map(k->convertObjToString(k.getPcSales())).collect(Collectors.toList()));
            List<String> data2 = new ArrayList<>();
            data.add(data2);
            data2.add("直接成本");
            data2.addAll(gbCtProfitChartsList.stream().map(k->convertObjToString(k.getPcDirectCost())).collect(Collectors.toList()));
            List<String> data3 = new ArrayList<>();
            data.add(data3);
            data3.add("毛利");
            data3.addAll(gbCtProfitChartsList.stream().map(k->convertObjToString(k.getPcGoodsProfit())).collect(Collectors.toList()));
            List<String> data4 = new ArrayList<>();
            data.add(data4);
            data4.add("综合管理费用");
            data4.addAll(gbCtProfitChartsList.stream().map(k->convertObjToString(k.getPcTotal())).collect(Collectors.toList()));
            List<String> data5 = new ArrayList<>();
            data.add(data5);
            data5.add("折旧前利润");
            data5.addAll(gbCtProfitChartsList.stream().map(k->convertObjToString(k.getPcProfitBeforeDep())).collect(Collectors.toList()));
            List<String> data6 = new ArrayList<>();
            data.add(data6);
            data6.add("折旧");
            data6.addAll(gbCtProfitChartsList.stream().map(k->convertObjToString(k.getPcDep())).collect(Collectors.toList()));
            List<String> data7 = new ArrayList<>();
            data.add(data7);
            data7.add("支付利息前利润");
            data7.addAll(gbCtProfitChartsList.stream().map(k->convertObjToString(k.getPcProfitBeforeInterests())).collect(Collectors.toList()));
            List<String> data8 = new ArrayList<>();
            data.add(data8);
            data8.add("财务费用");
            data8.addAll(gbCtProfitChartsList.stream().map(k->convertObjToString(k.getPcFinanceFee())).collect(Collectors.toList()));
            List<String> data9 = new ArrayList<>();
            data.add(data9);
            data9.add("营业外收支");
            data9.addAll(gbCtProfitChartsList.stream().map(k->convertObjToString(k.getPcNonOperating())).collect(Collectors.toList()));
            List<String> data10 = new ArrayList<>();
            data.add(data10);
            data10.add("税前利润");
            data10.addAll(gbCtProfitChartsList.stream().map(k->convertObjToString(k.getPcProfitBeforeTax())).collect(Collectors.toList()));
            List<String> data11 = new ArrayList<>();
            data.add(data11);
            data11.add("所得税");
            data11.addAll(gbCtProfitChartsList.stream().map(k->convertObjToString(k.getPcTax())).collect(Collectors.toList()));
            List<String> data12 = new ArrayList<>();
            data.add(data12);
            data12.add("净利润");
            data12.addAll(gbCtProfitChartsList.stream().map(k->convertObjToString(k.getPcAnnualNetProfit())).collect(Collectors.toList()));


            writer.write0(data, sheet, table1);
        }
        /*
        获取资产负债表
         */

        {
            List<GbCtBalance> gbCtBalanceList = gbCtBalanceMapper.getListByContestId(contestId,year);//资产负债表
            if (gbCtBalanceList.size() == 0) {
                gbCtBalanceList.add(new GbCtBalance());
            }
            // 设置每列宽度
            Map<Integer, Integer> columnWidth = new HashMap<>();
            for (int i = 0; i < gbCtBalanceList.size(); i++) {
                columnWidth.put(i, 4000);
            }

            Sheet sheet = new Sheet(3, 0);
            sheet.setColumnWidthMap(columnWidth);
            sheet.setSheetName("资产负债表");
            sheet.setStartRow(1);   // 前面空一行

            /*
            表格式
             */
            TableStyle tableStyle = new TableStyle();
            Font font = new Font();
            font.setFontName("Arial");
            font.setFontHeightInPoints((short) 10);
            tableStyle.setTableHeadFont(font);
            tableStyle.setTableHeadBackGroundColor(IndexedColors.GREY_25_PERCENT);
            tableStyle.setTableContentFont(font);
            tableStyle.setTableContentBackGroundColor(IndexedColors.WHITE);


            Table table1 = new Table(1);
            String title = "资产负债表";
            List<List<String>> head = new ArrayList<>();
            List<String> headCoulumn = new ArrayList<>();
            headCoulumn.add(title);
            headCoulumn.add("组号");
            head.add(headCoulumn);
            for (GbCtBalance gbCtBalance : gbCtBalanceList) {
                List<String> headCoulumn1 = new ArrayList<>();
                headCoulumn1.add(title);
                headCoulumn1.add(studentIdAndAccountMap.get(gbCtBalance.getStudentId()));
                head.add(headCoulumn1);
            }
            table1.setHead(head);
            table1.setTableStyle(tableStyle);

            List<List<String>> data = new ArrayList<>();
            List<String> data1 = new ArrayList<>();
            data.add(data1);
            data1.add("现金");
            data1.addAll(gbCtBalanceList.stream().map(k->convertObjToString(convertObjToString(k.getBsCash()))).collect(Collectors.toList()));
            List<String> data2 = new ArrayList<>();
            data.add(data2);
            data2.add("应收款");
            data2.addAll(gbCtBalanceList.stream().map(k->convertObjToString(convertObjToString(k.getBsReceivable()))).collect(Collectors.toList()));
            List<String> data3 = new ArrayList<>();
            data.add(data3);
            data3.add("在制品");
            data3.addAll(gbCtBalanceList.stream().map(k->convertObjToString(k.getBsProductInProcess())).collect(Collectors.toList()));
            List<String> data4 = new ArrayList<>();
            data.add(data4);
            data4.add("产成品");
            data4.addAll(gbCtBalanceList.stream().map(k->convertObjToString(k.getBsProduct())).collect(Collectors.toList()));
            List<String> data5 = new ArrayList<>();
            data.add(data5);
            data5.add("原材料");
            data5.addAll(gbCtBalanceList.stream().map(k->convertObjToString(k.getBsMaterial())).collect(Collectors.toList()));
            List<String> data6 = new ArrayList<>();
            data.add(data6);
            data6.add("流动资产合计");
            data6.addAll(gbCtBalanceList.stream().map(k->convertObjToString(k.getBsTotalCurrentAsset())).collect(Collectors.toList()));
            List<String> data7 = new ArrayList<>();
            data.add(data7);
            data7.add("机器和设备");
            data7.addAll(gbCtBalanceList.stream().map(k->convertObjToString(k.getBsEquipment())).collect(Collectors.toList()));
            List<String> data8 = new ArrayList<>();
            data.add(data8);
            data8.add("在建工程");
            data8.addAll(gbCtBalanceList.stream().map(k->convertObjToString(k.getBsProjectOnConstruction())).collect(Collectors.toList()));
            List<String> data9 = new ArrayList<>();
            data.add(data9);
            data9.add("固定资产合计");
            data9.addAll(gbCtBalanceList.stream().map(k->convertObjToString(k.getBsTotalFixedAsset())).collect(Collectors.toList()));
            List<String> data10 = new ArrayList<>();
            data.add(data10);
            data10.add("资产合计");
            data10.addAll(gbCtBalanceList.stream().map(k->convertObjToString(k.getBsTotalAsset())).collect(Collectors.toList()));
            List<String> data11 = new ArrayList<>();
            data.add(data11);
            data11.add("长期贷款");
            data11.addAll(gbCtBalanceList.stream().map(k->convertObjToString(k.getBsLongLoan())).collect(Collectors.toList()));
            List<String> data12 = new ArrayList<>();
            data.add(data12);
            data12.add("短期贷款");
            data12.addAll(gbCtBalanceList.stream().map(k->convertObjToString(k.getBsShortLoan())).collect(Collectors.toList()));
            List<String> data13 = new ArrayList<>();
            data.add(data13);
            data13.add("其他应付款");
            data13.addAll(gbCtBalanceList.stream().map(k->convertObjToString(k.getBsOtherPay())).collect(Collectors.toList()));
            List<String> data14 = new ArrayList<>();
            data.add(data14);
            data14.add("应交税费");
            data14.addAll(gbCtBalanceList.stream().map(k->convertObjToString(k.getBsTax())).collect(Collectors.toList()));
            List<String> data15 = new ArrayList<>();
            data.add(data15);
            data15.add("负债合计");
            data15.addAll(gbCtBalanceList.stream().map(k->convertObjToString(k.getBsTotalLiability())).collect(Collectors.toList()));
            List<String> data16 = new ArrayList<>();
            data.add(data16);
            data16.add("股东资本");
            data16.addAll(gbCtBalanceList.stream().map(k->convertObjToString(k.getBsEquity())).collect(Collectors.toList()));
            List<String> data17 = new ArrayList<>();
            data.add(data17);
            data17.add("利润留存");
            data17.addAll(gbCtBalanceList.stream().map(k->convertObjToString(k.getBsRetainedEarning())).collect(Collectors.toList()));
            List<String> data18 = new ArrayList<>();
            data.add(data18);
            data18.add("年度净利");
            data18.addAll(gbCtBalanceList.stream().map(k->convertObjToString(k.getBsAnnualNetProfit())).collect(Collectors.toList()));
            List<String> data19 = new ArrayList<>();
            data.add(data19);
            data19.add("所有者权益");
            data19.addAll(gbCtBalanceList.stream().map(k->convertObjToString(k.getBsTotalEquity())).collect(Collectors.toList()));
            List<String> data20 = new ArrayList<>();
            data.add(data20);
            data20.add("负债所有者权益合计");
            data20.addAll(gbCtBalanceList.stream().map(k->convertObjToString(k.getBsTotal())).collect(Collectors.toList()));

            table1.setHead(head);
            table1.setTableStyle(tableStyle);
            writer.write0(data, sheet, table1);
        }
        writer.finish();
    }

    /**
     * 获取广告投放情况保存到服务器
     * @param outputStream
     * @param contestId
     * @param date
     */
    public void adAll(OutputStream outputStream,Integer contestId, Integer date) {
        String name = "第" + date / 10 + "年"+date%10+"季广告投放";
        ExcelWriter writer = null;
        writer = new ExcelWriter(outputStream, ExcelTypeEnum.XLSX);

        /*
        获取广告投放情况
         */
        List<GbCtMnAd> ctMnAdList = gbCtMnAdMapper.getAdListByContestId(contestId,date / 10,date % 10);
        Map<Integer,GbCtMnAd> dataMap = new HashMap<>();
        for (GbCtMnAd gbCtMnAd : ctMnAdList) {
            dataMap.put(gbCtMnAd.getStudentId(),gbCtMnAd);
        }
        List<Integer> studentIdList = gbContestStudentService.getStudentIdList(contestId);
        Map<Integer, String> studentIdAndAccountMap = gbContestStudentService.getStudentIdAndAccountMap(contestId);
        // 设置每列宽度
        Map<Integer, Integer> columnWidth = new HashMap<>();
        for (int i = 0; i < 6; i++) {
            columnWidth.put(i, 4000);
        }

        Sheet sheet = new Sheet(1, 0);
        sheet.setColumnWidthMap(columnWidth);
        sheet.setSheetName(name);
        sheet.setStartRow(0);   // 前面空一行

        /*
        表格式
         */
        TableStyle tableStyle = new TableStyle();
        Font font = new Font();
        font.setFontName("Arial");
        font.setFontHeightInPoints((short) 10);
        tableStyle.setTableHeadFont(font);
        tableStyle.setTableHeadBackGroundColor(IndexedColors.GREY_25_PERCENT);
        tableStyle.setTableContentFont(font);
        tableStyle.setTableContentBackGroundColor(IndexedColors.WHITE);

        // 表格数
        int i = 0;
        String[] strs = {"产品", "本地", "区域", "国内", "亚洲", "国际"};
        for (Integer studentId : studentIdList) {
            GbCtMnAd ctMnAd = dataMap.get(studentId);
            if(ctMnAd == null){
                ctMnAd = new GbCtMnAd();
            }
            List<List<String>> head = new ArrayList<>();
            String title = studentIdAndAccountMap.get(studentId) + "广告投放情况";
            for (int j = 0; j < 6; j++) {
                List<String> headCoulumn = new ArrayList<>();
                // 第一行的头（名称一样则自动合并成一个单元格）
                headCoulumn.add(title);
                // 第二行的头
                headCoulumn.add(strs[j]);
                head.add(headCoulumn);
            }

            List<List<String>> data = new ArrayList<>();
            data.add(Arrays.asList("P1",
                    this.convertToString(ctMnAd.getLocalP1()),
                    this.convertToString(ctMnAd.getRegionalP1()),
                    this.convertToString(ctMnAd.getNationalP1()),
                    this.convertToString(ctMnAd.getAsianP1()),
                    this.convertToString(ctMnAd.getInternationalP1())));

            data.add(Arrays.asList("P2",
                    this.convertToString(ctMnAd.getLocalP2()),
                    this.convertToString(ctMnAd.getRegionalP2()),
                    this.convertToString(ctMnAd.getNationalP2()),
                    this.convertToString(ctMnAd.getAsianP2()),
                    this.convertToString(ctMnAd.getInternationalP2())));

            data.add(Arrays.asList("P3",
                    this.convertToString(ctMnAd.getLocalP3()),
                    this.convertToString(ctMnAd.getRegionalP3()),
                    this.convertToString(ctMnAd.getNationalP3()),
                    this.convertToString(ctMnAd.getAsianP3()),
                    this.convertToString(ctMnAd.getInternationalP3())));

            data.add(Arrays.asList("P4",
                    this.convertToString(ctMnAd.getLocalP4()),
                    this.convertToString(ctMnAd.getRegionalP4()),
                    this.convertToString(ctMnAd.getNationalP4()),
                    this.convertToString(ctMnAd.getAsianP4()),
                    this.convertToString(ctMnAd.getInternationalP4())));

            data.add(Arrays.asList("P5",
                    this.convertToString(ctMnAd.getLocalP5()),
                    this.convertToString(ctMnAd.getRegionalP5()),
                    this.convertToString(ctMnAd.getNationalP5()),
                    this.convertToString(ctMnAd.getAsianP5()),
                    this.convertToString(ctMnAd.getInternationalP5())));

            Table table1 = new Table(++i);
            table1.setHead(head);
            table1.setTableStyle(tableStyle);
            writer.write0(data, sheet, table1);
        }
        writer.finish();
    }

    /**
     * 根据文件名下载信息
     * @param response
     * @param fileName
     * @throws IOException
     */
    public void downloadFile(HttpServletResponse response,String fileName) throws IOException {
        String tempFilePath = CommonUtil.getCatalinaHome() + environment.getProperty("teaching-platform.temp-excel");
        int lastIndex = fileName.lastIndexOf("--");
        String newFileName = fileName.substring(lastIndex + 2);
        EasyPOIUtil.exportFileByPath(response, tempFilePath+fileName,newFileName);
    }


    /**
     * 根据模板保存竞赛数据文件
     * @param contestId
     * @param studentId
     * @return
     */
    public void saveResultToFile(Integer contestId, Integer studentId,String fileName,String saveFilePath) {
        GbContestStudent contestStudent = contestStudentMapper.get(contestId, studentId);
        if (contestStudent == null) {
            return;
        }

        Integer date = contestStudent.getDate() == null ? null : (contestStudent.getDate() / 10);
//        if(!maxDate.equals(date)){
//            return;
//        }
        // 库存信息
        Map<String, Object> kcxx;
        Future<Map<String, Object>> kcxxFuture = contestResultAsyncService.getKCXX(contestId, studentId);

        // 银行贷款
        Map<String, Object> yhdk;
        Future<Map<String, Object>> yhdkFuture = contestResultAsyncService.getYHDK(contestId, studentId, date);

        // 研发认证
        Map<String, Object> yfrz;
        Future<Map<String, Object>> yfrzFuture = contestResultAsyncService.getYFRZ(contestId, studentId);

        // 厂房与生产线
        Map<String, Object> cfyscx;
        Future<Map<String, Object>> cfyscxFuture = contestResultAsyncService.getCFYSCX(contestId, studentId);

        // 订单信息
        Map<String, Object> ddxx;
        Future<Map<String, Object>> ddxxFuture = contestResultAsyncService.getDDXX(contestId, studentId, date);

        // 现金流量表
        Map<String, Object> xjllb;
        Future<Map<String, Object>> xjllbFuture = contestResultAsyncService.getXJLLB(contestId, studentId);

        // 企业财务报表
        Map<String, Object> qycwbb;
        Future<Map<String, Object>> qycwbbFuture = contestResultAsyncService.getQYCWBB(contestId, studentId);

        // 四年广告投放
        Map<String, Object> ggtf;
        Future<Map<String, Object>> ggtfFuture = contestResultAsyncService.getGGTF(contestId, studentId);

        // 所有异步任务执行完成
        while (true) {
            if (kcxxFuture.isDone() &&
                    yhdkFuture.isDone() &&
                    yfrzFuture.isDone() &&
                    cfyscxFuture.isDone() &&
                    ddxxFuture.isDone() &&
                    xjllbFuture.isDone() &&
                    qycwbbFuture.isDone() &&
                    ggtfFuture.isDone()) {
                break;
            }
        }

        // 获取异步任务返回的数据
        try {
            kcxx = kcxxFuture.get();
            yhdk = yhdkFuture.get();
            cfyscx = cfyscxFuture.get();
            ddxx = ddxxFuture.get();
            yfrz = yfrzFuture.get();
            xjllb = xjllbFuture.get();
            qycwbb = qycwbbFuture.get();
            ggtf = ggtfFuture.get();
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
            return;
        }

        /*
        数据封装
         */
        Map<String, Object> param = new HashMap<>();
        param.putAll(kcxx);
        param.putAll(yhdk);
        param.putAll(yfrz);
        param.putAll(cfyscx);
        param.putAll(ddxx);
        param.putAll(xjllb);
        param.putAll(qycwbb);
        param.putAll(ggtf);


//        String uniqueFileName = CommonUtil.getUniqueFileName(studentId+"竞赛数据");
        String templateFilePath = CommonUtil.getClassesPath() + "templates/gb_contest_result.xlsx";
        try {
            EasyPOIUtil.saveMultiSheetExcelByMap( saveFilePath ,fileName ,templateFilePath,param,true);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * 根据模板保存竞赛数据文件
     * @param contestId
     * @param studentId
     * @return
     */
    public void saveResultNoCashFlowToFile(Integer contestId, Integer studentId,String fileName,String saveFilePath) {
        GbContestStudent contestStudent = contestStudentMapper.get(contestId, studentId);
        if (contestStudent == null) {
            return;
        }

        Integer date = contestStudent.getDate() == null ? null : (contestStudent.getDate() / 10);
//        if(!maxDate.equals(date)){
//            return;
//        }
        // 库存信息
        Map<String, Object> kcxx;
        Future<Map<String, Object>> kcxxFuture = contestResultAsyncService.getKCXX(contestId, studentId);

        // 银行贷款
        Map<String, Object> yhdk;
        Future<Map<String, Object>> yhdkFuture = contestResultAsyncService.getYHDK(contestId, studentId, date);

        // 研发认证
        Map<String, Object> yfrz;
        Future<Map<String, Object>> yfrzFuture = contestResultAsyncService.getYFRZ(contestId, studentId);

        // 厂房与生产线
        Map<String, Object> cfyscx;
        Future<Map<String, Object>> cfyscxFuture = contestResultAsyncService.getCFYSCX(contestId, studentId);

        // 订单信息
        Map<String, Object> ddxx;
        Future<Map<String, Object>> ddxxFuture = contestResultAsyncService.getDDXX(contestId, studentId, date);

//        // 现金流量表
//        Map<String, Object> xjllb;
//        Future<Map<String, Object>> xjllbFuture = contestResultAsyncService.getXJLLB(contestId, studentId);

        // 企业财务报表
        Map<String, Object> qycwbb;
        Future<Map<String, Object>> qycwbbFuture = contestResultAsyncService.getQYCWBB(contestId, studentId);

        // 四年广告投放
        Map<String, Object> ggtf;
        Future<Map<String, Object>> ggtfFuture = contestResultAsyncService.getGGTF(contestId, studentId);

        // 所有异步任务执行完成
        while (true) {
            if (kcxxFuture.isDone() &&
                    yhdkFuture.isDone() &&
                    yfrzFuture.isDone() &&
                    cfyscxFuture.isDone() &&
                    ddxxFuture.isDone() &&
//                    xjllbFuture.isDone() &&
                    qycwbbFuture.isDone() &&
                    ggtfFuture.isDone()) {
                break;
            }
        }

        // 获取异步任务返回的数据
        try {
            kcxx = kcxxFuture.get();
            yhdk = yhdkFuture.get();
            cfyscx = cfyscxFuture.get();
            ddxx = ddxxFuture.get();
            yfrz = yfrzFuture.get();
//            xjllb = xjllbFuture.get();
            qycwbb = qycwbbFuture.get();
            ggtf = ggtfFuture.get();
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
            return;
        }

        /*
        数据封装
         */
        Map<String, Object> param = new HashMap<>();
        param.putAll(kcxx);
        param.putAll(yhdk);
        param.putAll(yfrz);
        param.putAll(cfyscx);
        param.putAll(ddxx);
//        param.putAll(xjllb);
        param.putAll(qycwbb);
        param.putAll(ggtf);


//        String uniqueFileName = CommonUtil.getUniqueFileName(studentId+"竞赛数据");
        String templateFilePath = CommonUtil.getClassesPath() + "templates/gb_contest_result_no_cash_flow.xlsx";
        try {
            EasyPOIUtil.saveMultiSheetExcelByMap( saveFilePath ,fileName ,templateFilePath,param,true);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    /**
     * 一键导出
     * @param response
     * @param contestId
     * @throws IOException
     */
    public void downAllFile(HttpServletResponse response,Integer contestId) throws IOException {
        Integer maxDate = gbContestStudentService.getMaxDate(contestId);
        String tempFilePath = CommonUtil.getCatalinaHome() + environment.getProperty("teaching-platform.temp-excel");
        String uniqueDir = CommonUtil.getUniqueFileName(contestId+maxDate+"");
        tempFilePath = tempFilePath+uniqueDir+"/";
        List<Integer> studentIdList = gbContestStudentService.getStudentIdList(contestId);
        Map<Integer, String> studentIdAndAccountMap = gbContestStudentService.getStudentIdAndAccountMap(contestId);
        List<String> filesNames = new ArrayList<>();
        List<File> files = new ArrayList<>();
        //每一个学生的所有竞赛数据
        for (Integer studentId : studentIdList) {
            String fileName = studentIdAndAccountMap.get(studentId) + ExcelTypeEnum.XLSX.getValue();
            this.saveResultToFile(contestId, studentId, fileName, tempFilePath);
            filesNames.add(fileName);
            File file = new File(tempFilePath+fileName);
            files.add(file);
        }
        //多年的广告投放和报表填写数据
        for (int year = 1; year <= maxDate/10; year++) {
            String fileName = "第"+year+"年";
            File excelFile = FileUtil.createExcelFile(tempFilePath,fileName);
            files.add(excelFile);
            filesNames.add(excelFile.getName());
            this.addAndFinancial(excelFile,contestId,year);
        }

        FileUtil.toZipAndDownload(response,files,filesNames,"第"+maxDate/10+"年"+maxDate%10+"季一键导出.zip");
        files.forEach(File::delete);
    }


    /**
     * 广告和财务报表信息
     * @param excelFile
     * @param contestId
     * @param year
     * @throws FileNotFoundException
     */
    public void addAndFinancial(File excelFile ,Integer contestId,Integer year) throws FileNotFoundException {
        //学生和账号的map
        Map<Integer, String> studentMap = gbContestStudentService.getStudentIdAndAccountMap(contestId);

        FileOutputStream fos = new FileOutputStream(excelFile);
        ExcelWriter writer = new ExcelWriter(fos, ExcelTypeEnum.XLSX);
        //广告投放数据
        {
            /*
            表格式
             */
            TableStyle tableStyle = new TableStyle();
            Font font = new Font();
            font.setFontName("Arial");
            font.setFontHeightInPoints((short) 10);
            tableStyle.setTableHeadFont(font);
            tableStyle.setTableHeadBackGroundColor(IndexedColors.GREY_25_PERCENT);
            tableStyle.setTableContentFont(font);
            tableStyle.setTableContentBackGroundColor(IndexedColors.WHITE);
            // 设置每列宽度
            Map<Integer, Integer> columnWidth = new HashMap<>();
            for (int i = 0; i < 24; i++) {
                columnWidth.put(i, 4000);
            }
            String name = "第" + year + "年广告投放";
            Sheet sheet = new Sheet(1, 0);
            sheet.setColumnWidthMap(columnWidth);
            sheet.setSheetName(name);
            sheet.setStartRow(1);   // 前面空一行
            List<GbCtMnAd> ctMnAdList = gbCtMnAdMapper.getAdListAndAccountByYear(contestId,year);
            //整理数据
            Map<Integer,Map<Integer,GbCtMnAd>> mnAdMap = new HashMap<>();
            for (GbCtMnAd gbCtMnAd : ctMnAdList) {
                Map<Integer, GbCtMnAd> studentIdGbCtMnAdMap = mnAdMap.get(gbCtMnAd.getStudentId());
                if(studentIdGbCtMnAdMap == null){
                    studentIdGbCtMnAdMap = new HashMap<>();
                    mnAdMap.put(gbCtMnAd.getStudentId(),studentIdGbCtMnAdMap);
                    studentIdGbCtMnAdMap.put(gbCtMnAd.getQuarterly(),gbCtMnAd);
                }else {
                    studentIdGbCtMnAdMap.put(gbCtMnAd.getQuarterly(),gbCtMnAd);
                }
            }
            int i = 0;
            for (Integer studentId : studentMap.keySet()) {
                Map<Integer, GbCtMnAd> integerGbCtMnAdMap = mnAdMap.get(studentId);
                if(integerGbCtMnAdMap == null){
                    integerGbCtMnAdMap = new HashMap<>();
                }
                List<List<String>> data = new ArrayList<>();
                List<String> P1 = new ArrayList<>();
                List<String> P2 = new ArrayList<>();
                List<String> P3 = new ArrayList<>();
                List<String> P4 = new ArrayList<>();
                List<String> P5 = new ArrayList<>();
                List<List<String>> head = new ArrayList<>();
                String[] strs = {"产品", "本地", "区域", "国内", "亚洲", "国际"};
                for (int season = 1; season <= 4; season++) {
                    String headTitle = studentMap.get(studentId)+"组广告投放情况";
                    String title = "第" + year + "年" + "第" + season + "季广告投放";
                    //获取当季数据
                    for (int j = 0; j < 6; j++) {
                        List<String> headCoulumn = new ArrayList<>();
                        // 第一行的头（名称一样则自动合并成一个单元格）
                        headCoulumn.add(headTitle);
                        headCoulumn.add(title);
                        // 第二行的头
                        headCoulumn.add(strs[j]);
                        head.add(headCoulumn);
                    }
                    GbCtMnAd ctMnAd = integerGbCtMnAdMap.get(season);
                    if(ctMnAd == null){
                        ctMnAd = new GbCtMnAd();
                    }
                    P1.add("P1");
                    P1.add(this.convertToString(ctMnAd.getLocalP1()));
                    P1.add(this.convertToString(ctMnAd.getRegionalP1()));
                    P1.add(this.convertToString(ctMnAd.getNationalP1()));
                    P1.add(this.convertToString(ctMnAd.getAsianP1()));
                    P1.add(this.convertToString(ctMnAd.getInternationalP1()));

                    P2.add("P2");
                    P2.add(this.convertToString(ctMnAd.getLocalP2()));
                    P2.add(this.convertToString(ctMnAd.getRegionalP2()));
                    P2.add(this.convertToString(ctMnAd.getNationalP2()));
                    P2.add(this.convertToString(ctMnAd.getAsianP2()));
                    P2.add(this.convertToString(ctMnAd.getInternationalP2()));

                    P3.add("P3");
                    P3.add(this.convertToString(ctMnAd.getLocalP3()));
                    P3.add(this.convertToString(ctMnAd.getRegionalP3()));
                    P3.add(this.convertToString(ctMnAd.getNationalP3()));
                    P3.add(this.convertToString(ctMnAd.getAsianP3()));
                    P3.add(this.convertToString(ctMnAd.getInternationalP3()));

                    P4.add("P4");
                    P4.add(this.convertToString(ctMnAd.getLocalP4()));
                    P4.add(this.convertToString(ctMnAd.getRegionalP4()));
                    P4.add(this.convertToString(ctMnAd.getNationalP4()));
                    P4.add(this.convertToString(ctMnAd.getAsianP4()));
                    P4.add(this.convertToString(ctMnAd.getInternationalP4()));

                    P5.add("P5");
                    P5.add(this.convertToString(ctMnAd.getLocalP5()));
                    P5.add(this.convertToString(ctMnAd.getRegionalP5()));
                    P5.add(this.convertToString(ctMnAd.getNationalP5()));
                    P5.add(this.convertToString(ctMnAd.getAsianP5()));
                    P5.add(this.convertToString(ctMnAd.getInternationalP5()));
                }
                data.add(P1);
                data.add(P2);
                data.add(P3);
                data.add(P4);
                data.add(P5);
                Table table1 = new Table(++i);
                table1.setHead(head);
                table1.setTableStyle(tableStyle);
                writer.write0(data, sheet, table1);
            }
        }
        /*
        获取综合费用表
         */
        {
            List<GbCtCharges> gbCtChargesList = gbCtChargesMapper.getListByContestId(contestId, year);//综合费用表
            if (gbCtChargesList.size() == 0) {
                gbCtChargesList.add(new GbCtCharges());
            }
            // 设置每列宽度
            Map<Integer, Integer> columnWidth = new HashMap<>();
            for (int i = 0; i <= gbCtChargesList.size(); i++) {
                columnWidth.put(i, 4000);
            }

            Sheet sheet = new Sheet(2, 0);
            sheet.setColumnWidthMap(columnWidth);
            sheet.setSheetName("综合费用表");
            sheet.setStartRow(1);   // 前面空一行

            /*
            表格式
             */
            TableStyle tableStyle = new TableStyle();
            Font font = new Font();
            font.setFontName("Arial");
            font.setFontHeightInPoints((short) 10);
            tableStyle.setTableHeadFont(font);
            tableStyle.setTableHeadBackGroundColor(IndexedColors.GREY_25_PERCENT);
            tableStyle.setTableContentFont(font);
            tableStyle.setTableContentBackGroundColor(IndexedColors.WHITE);

            Table table1 = new Table(1);
            String title = "综合费用表";
            List<List<String>> head = new ArrayList<>();
            List<String> headCoulumn = new ArrayList<>();
            headCoulumn.add(title);
            headCoulumn.add("组号");
            head.add(headCoulumn);
            for (GbCtCharges gbCtCharges : gbCtChargesList) {
                List<String> headCoulumn1 = new ArrayList<>();
                headCoulumn1.add(title);
                headCoulumn1.add(studentMap.get(gbCtCharges.getStudentId()));
                head.add(headCoulumn1);
            }
            table1.setHead(head);
            table1.setTableStyle(tableStyle);

            List<List<String>> data = new ArrayList<>();
            List<String> data1 = new ArrayList<>();
            data.add(data1);
            data1.add("管理费");
            data1.addAll(gbCtChargesList.stream().map(k->convertObjToString(k.getCOverhaul())).collect(Collectors.toList()));
            List<String> data2 = new ArrayList<>();
            data.add(data2);
            data2.add("广告费");
            data2.addAll(gbCtChargesList.stream().map(k->convertObjToString(k.getCAd())).collect(Collectors.toList()));
            List<String> data3 = new ArrayList<>();
            data.add(data3);
            data3.add("产线维修费");
            data3.addAll(gbCtChargesList.stream().map(k->convertObjToString(k.getCMaintenance())).collect(Collectors.toList()));
            List<String> data4 = new ArrayList<>();
            data.add(data4);
            data4.add("转产费");
            data4.addAll(gbCtChargesList.stream().map(k->convertObjToString(k.getCTransfer())).collect(Collectors.toList()));
            List<String> data5 = new ArrayList<>();
            data.add(data5);
            data5.add("市场开拓费");
            data5.addAll(gbCtChargesList.stream().map(k->convertObjToString(k.getCDevelopMarket())).collect(Collectors.toList()));
            List<String> data6 = new ArrayList<>();
            data.add(data6);
            data6.add("产品资质申请");
            data6.addAll(gbCtChargesList.stream().map(k->convertObjToString(k.getCDevelopProduct())).collect(Collectors.toList()));
            List<String> data7 = new ArrayList<>();
            data.add(data7);
            data7.add("ISO认证申请");
            data7.addAll(gbCtChargesList.stream().map(k->convertObjToString(k.getCDevelopIso())).collect(Collectors.toList()));
            List<String> data8 = new ArrayList<>();
            data.add(data8);
            data8.add("信息费");
            data8.addAll(gbCtChargesList.stream().map(k->convertObjToString(k.getCInformation())).collect(Collectors.toList()));
            List<String> data9 = new ArrayList<>();
            data.add(data9);
            data9.add("数字化研发费");
            data9.addAll(gbCtChargesList.stream().map(k->convertObjToString(k.getCDigitalization())).collect(Collectors.toList()));
            List<String> data10 = new ArrayList<>();
            data.add(data10);
            data10.add("合计");
            data10.addAll(gbCtChargesList.stream().map(k->convertObjToString(k.getCTotal())).collect(Collectors.toList()));

            writer.write0(data, sheet, table1);
        }
        /*
        获取利润表
         */
        {
            List<GbCtProfitChart> gbCtProfitChartsList = gbCtProfitChartMapper.getListByContestId(contestId, year);//利润表
            if (gbCtProfitChartsList.size() == 0) {
                gbCtProfitChartsList.add(new GbCtProfitChart());
            }
            // 设置每列宽度
            Map<Integer, Integer> columnWidth = new HashMap<>();
            for (int i = 0; i < gbCtProfitChartsList.size(); i++) {
                columnWidth.put(i, 4000);
            }

            Sheet sheet = new Sheet(3, 0);
            sheet.setColumnWidthMap(columnWidth);
            sheet.setSheetName("利润表");
            sheet.setStartRow(1);   // 前面空一行

            /*
            表格式
             */
            TableStyle tableStyle = new TableStyle();
            Font font = new Font();
            font.setFontName("Arial");
            font.setFontHeightInPoints((short) 10);
            tableStyle.setTableHeadFont(font);
            tableStyle.setTableHeadBackGroundColor(IndexedColors.GREY_25_PERCENT);
            tableStyle.setTableContentFont(font);
            tableStyle.setTableContentBackGroundColor(IndexedColors.WHITE);

            Table table1 = new Table(1);
            String title = "利润表";
            List<List<String>> head = new ArrayList<>();
            List<String> headCoulumn = new ArrayList<>();
            headCoulumn.add("组号");
            head.add(headCoulumn);
            for (GbCtProfitChart gbCtProfitChart : gbCtProfitChartsList) {
                List<String> headCoulumn1 = new ArrayList<>();
                headCoulumn1.add(title);
                headCoulumn1.add(studentMap.get(gbCtProfitChart.getStudentId()));
                head.add(headCoulumn1);
            }
            table1.setHead(head);
            table1.setTableStyle(tableStyle);

            List<List<String>> data = new ArrayList<>();
            List<String> data1 = new ArrayList<>();
            data.add(data1);
            data1.add("销售收入");
            data1.addAll(gbCtProfitChartsList.stream().map(k->convertObjToString(k.getPcSales())).collect(Collectors.toList()));
            List<String> data2 = new ArrayList<>();
            data.add(data2);
            data2.add("直接成本");
            data2.addAll(gbCtProfitChartsList.stream().map(k->convertObjToString(k.getPcDirectCost())).collect(Collectors.toList()));
            List<String> data3 = new ArrayList<>();
            data.add(data3);
            data3.add("毛利");
            data3.addAll(gbCtProfitChartsList.stream().map(k->convertObjToString(k.getPcGoodsProfit())).collect(Collectors.toList()));
            List<String> data4 = new ArrayList<>();
            data.add(data4);
            data4.add("综合管理费用");
            data4.addAll(gbCtProfitChartsList.stream().map(k->convertObjToString(k.getPcTotal())).collect(Collectors.toList()));
            List<String> data5 = new ArrayList<>();
            data.add(data5);
            data5.add("折旧前利润");
            data5.addAll(gbCtProfitChartsList.stream().map(k->convertObjToString(k.getPcProfitBeforeDep())).collect(Collectors.toList()));
            List<String> data6 = new ArrayList<>();
            data.add(data6);
            data6.add("折旧");
            data6.addAll(gbCtProfitChartsList.stream().map(k->convertObjToString(k.getPcDep())).collect(Collectors.toList()));
            List<String> data7 = new ArrayList<>();
            data.add(data7);
            data7.add("支付利息前利润");
            data7.addAll(gbCtProfitChartsList.stream().map(k->convertObjToString(k.getPcProfitBeforeInterests())).collect(Collectors.toList()));
            List<String> data8 = new ArrayList<>();
            data.add(data8);
            data8.add("财务费用");
            data8.addAll(gbCtProfitChartsList.stream().map(k->convertObjToString(k.getPcFinanceFee())).collect(Collectors.toList()));
            List<String> data9 = new ArrayList<>();
            data.add(data9);
            data9.add("营业外收支");
            data9.addAll(gbCtProfitChartsList.stream().map(k->convertObjToString(k.getPcNonOperating())).collect(Collectors.toList()));
            List<String> data10 = new ArrayList<>();
            data.add(data10);
            data10.add("税前利润");
            data10.addAll(gbCtProfitChartsList.stream().map(k->convertObjToString(k.getPcProfitBeforeTax())).collect(Collectors.toList()));
            List<String> data11 = new ArrayList<>();
            data.add(data11);
            data11.add("所得税");
            data11.addAll(gbCtProfitChartsList.stream().map(k->convertObjToString(k.getPcTax())).collect(Collectors.toList()));
            List<String> data12 = new ArrayList<>();
            data.add(data12);
            data12.add("净利润");
            data12.addAll(gbCtProfitChartsList.stream().map(k->convertObjToString(k.getPcAnnualNetProfit())).collect(Collectors.toList()));


            writer.write0(data, sheet, table1);
        }
        /*
        获取资产负债表
         */
        {
            List<GbCtBalance> gbCtBalanceList = gbCtBalanceMapper.getListByContestId(contestId,year);//资产负债表
            if (gbCtBalanceList.size() == 0) {
                gbCtBalanceList.add(new GbCtBalance());
            }
            // 设置每列宽度
            Map<Integer, Integer> columnWidth = new HashMap<>();
            for (int i = 0; i < gbCtBalanceList.size(); i++) {
                columnWidth.put(i, 4000);
            }

            Sheet sheet = new Sheet(4, 0);
            sheet.setColumnWidthMap(columnWidth);
            sheet.setSheetName("资产负债表");
            sheet.setStartRow(1);   // 前面空一行

            /*
            表格式
             */
            TableStyle tableStyle = new TableStyle();
            Font font = new Font();
            font.setFontName("Arial");
            font.setFontHeightInPoints((short) 10);
            tableStyle.setTableHeadFont(font);
            tableStyle.setTableHeadBackGroundColor(IndexedColors.GREY_25_PERCENT);
            tableStyle.setTableContentFont(font);
            tableStyle.setTableContentBackGroundColor(IndexedColors.WHITE);


            Table table1 = new Table(1);
            String title = "资产负债表";
            List<List<String>> head = new ArrayList<>();
            List<String> headCoulumn = new ArrayList<>();
            headCoulumn.add("组号");
            head.add(headCoulumn);
            for (GbCtBalance gbCtBalance : gbCtBalanceList) {
                List<String> headCoulumn1 = new ArrayList<>();
                headCoulumn1.add(title);
                headCoulumn1.add(studentMap.get(gbCtBalance.getStudentId()));
                head.add(headCoulumn1);
            }
            table1.setHead(head);
            table1.setTableStyle(tableStyle);

            List<List<String>> data = new ArrayList<>();
            List<String> data1 = new ArrayList<>();
            data.add(data1);
            data1.add("现金");
            data1.addAll(gbCtBalanceList.stream().map(k->convertObjToString(convertObjToString(k.getBsCash()))).collect(Collectors.toList()));
            List<String> data2 = new ArrayList<>();
            data.add(data2);
            data2.add("应收款");
            data2.addAll(gbCtBalanceList.stream().map(k->convertObjToString(convertObjToString(k.getBsReceivable()))).collect(Collectors.toList()));
            List<String> data3 = new ArrayList<>();
            data.add(data3);
            data3.add("在制品");
            data3.addAll(gbCtBalanceList.stream().map(k->convertObjToString(k.getBsProductInProcess())).collect(Collectors.toList()));
            List<String> data4 = new ArrayList<>();
            data.add(data4);
            data4.add("产成品");
            data4.addAll(gbCtBalanceList.stream().map(k->convertObjToString(k.getBsProduct())).collect(Collectors.toList()));
            List<String> data5 = new ArrayList<>();
            data.add(data5);
            data5.add("原材料");
            data5.addAll(gbCtBalanceList.stream().map(k->convertObjToString(k.getBsMaterial())).collect(Collectors.toList()));
            List<String> data6 = new ArrayList<>();
            data.add(data6);
            data6.add("流动资产合计");
            data6.addAll(gbCtBalanceList.stream().map(k->convertObjToString(k.getBsTotalCurrentAsset())).collect(Collectors.toList()));
            List<String> data7 = new ArrayList<>();
            data.add(data7);
            data7.add("机器和设备");
            data7.addAll(gbCtBalanceList.stream().map(k->convertObjToString(k.getBsEquipment())).collect(Collectors.toList()));
            List<String> data8 = new ArrayList<>();
            data.add(data8);
            data8.add("在建工程");
            data8.addAll(gbCtBalanceList.stream().map(k->convertObjToString(k.getBsProjectOnConstruction())).collect(Collectors.toList()));
            List<String> data9 = new ArrayList<>();
            data.add(data9);
            data9.add("固定资产合计");
            data9.addAll(gbCtBalanceList.stream().map(k->convertObjToString(k.getBsTotalFixedAsset())).collect(Collectors.toList()));
            List<String> data10 = new ArrayList<>();
            data.add(data10);
            data10.add("资产合计");
            data10.addAll(gbCtBalanceList.stream().map(k->convertObjToString(k.getBsTotalAsset())).collect(Collectors.toList()));
            List<String> data11 = new ArrayList<>();
            data.add(data11);
            data11.add("长期贷款");
            data11.addAll(gbCtBalanceList.stream().map(k->convertObjToString(k.getBsTotalAsset())).collect(Collectors.toList()));
            List<String> data12 = new ArrayList<>();
            data.add(data12);
            data12.add("短期贷款");
            data12.addAll(gbCtBalanceList.stream().map(k->convertObjToString(k.getBsShortLoan())).collect(Collectors.toList()));
            List<String> data13 = new ArrayList<>();
            data.add(data13);
            data13.add("其他应付款");
            data13.addAll(gbCtBalanceList.stream().map(k->convertObjToString(k.getBsOtherPay())).collect(Collectors.toList()));
            List<String> data14 = new ArrayList<>();
            data.add(data14);
            data14.add("应交税费");
            data14.addAll(gbCtBalanceList.stream().map(k->convertObjToString(k.getBsTax())).collect(Collectors.toList()));
            List<String> data15 = new ArrayList<>();
            data.add(data15);
            data15.add("负债合计");
            data15.addAll(gbCtBalanceList.stream().map(k->convertObjToString(k.getBsTotalLiability())).collect(Collectors.toList()));
            List<String> data16 = new ArrayList<>();
            data.add(data16);
            data16.add("股东资本");
            data16.addAll(gbCtBalanceList.stream().map(k->convertObjToString(k.getBsEquity())).collect(Collectors.toList()));
            List<String> data17 = new ArrayList<>();
            data.add(data17);
            data17.add("利润留存");
            data17.addAll(gbCtBalanceList.stream().map(k->convertObjToString(k.getBsRetainedEarning())).collect(Collectors.toList()));
            List<String> data18 = new ArrayList<>();
            data.add(data18);
            data18.add("年度净利");
            data18.addAll(gbCtBalanceList.stream().map(k->convertObjToString(k.getBsAnnualNetProfit())).collect(Collectors.toList()));
            List<String> data19 = new ArrayList<>();
            data.add(data19);
            data19.add("所有者权益");
            data19.addAll(gbCtBalanceList.stream().map(k->convertObjToString(k.getBsTotalEquity())).collect(Collectors.toList()));
            List<String> data20 = new ArrayList<>();
            data.add(data20);
            data20.add("负债所有者权益合计");
            data20.addAll(gbCtBalanceList.stream().map(k->convertObjToString(k.getBsTotal())).collect(Collectors.toList()));

            table1.setHead(head);
            table1.setTableStyle(tableStyle);
            writer.write0(data, sheet, table1);
        }
        writer.finish();
        IOUtils.closeQuietly(fos);
    }


    /**
     * 生成复盘数据
     * @param response
     * @param contestId
     * @param newSubjectNumber
     */
    public void downAutoSubjectModelFile(HttpServletResponse response, Integer contestId, Integer newSubjectNumber) {
        Map<String,Object> resultMap = new HashMap<>();
        GbContest contest = gbContestMapper.selectById(contestId);
        Integer subjectNumber = contest.getSubjectNumber();
        // 规则cs
        List<Map<String, Object>> cs = new ArrayList<>();
        List<GbCtGzCs> gbCtGzCsList = gbCtGzCsMapper.selectList(new QueryWrapper<GbCtGzCs>().eq("subject_number", subjectNumber));
        for (GbCtGzCs ctGzCs : gbCtGzCsList) {
            Map<String, Object> map = new HashMap<>();
            map.put("subjectNumber",newSubjectNumber);
            map.put("punish",ctGzCs.getPunish());
            map.put("inventoryDiscountProduct",ctGzCs.getInventoryDiscountProduct());
            map.put("inventoryDiscountMaterial",ctGzCs.getInventoryDiscountMaterial());
            map.put("loanCeiling",ctGzCs.getLoanCeiling());
            map.put("cash",ctGzCs.getCash());
            map.put("overhaul",ctGzCs.getOverhaul());
            map.put("min",ctGzCs.getMin());
            map.put("tolerance",ctGzCs.getTolerance());
            map.put("information",ctGzCs.getInformation());
            map.put("emergenceMultipleMaterial",ctGzCs.getEmergenceMultipleMaterial());
            map.put("emergenceMultipleProduct",ctGzCs.getEmergenceMultipleProduct());
            map.put("incomeTax",ctGzCs.getIncomeTax());
            map.put("orderTimeout",ctGzCs.getOrderTimeout());
            map.put("orderFixTime",ctGzCs.getOrderFixTime());
            map.put("auctionTimeout",ctGzCs.getOrderTimeout());
            map.put("auctionThread",ctGzCs.getAuctionThread());
            map.put("maxLine",ctGzCs.getMaxLine());
            cs.add(map);
        }
        resultMap.put("cs",cs);
        //广告
        List<Map<String, Object>> ad = new ArrayList<>();
        List<GbCtMnAd> gbCtMnAds = gbCtMnAdMapper.selectList(new QueryWrapper<GbCtMnAd>().eq("contest_id", contestId));
        for (GbCtMnAd gbCtMnAd : gbCtMnAds) {
            Map<String, Object> map = new HashMap<>();
            map.put("subjectNumber",newSubjectNumber);
            map.put("year",gbCtMnAd.getYear());
            map.put("quarterly",gbCtMnAd.getQuarterly());
            if(gbCtMnAd.getGroupNum() != null && StringUtils.isNotEmpty(gbCtMnAd.getGroupNum())){
                map.put("groupNum","人机"+gbCtMnAd.getGroupNum());
            }
            map.put("localP1",gbCtMnAd.getLocalP1());
            map.put("localP2",gbCtMnAd.getLocalP2());
            map.put("localP3",gbCtMnAd.getLocalP3());
            map.put("localP4",gbCtMnAd.getLocalP4());
            map.put("localP5",gbCtMnAd.getLocalP5());
            map.put("regionalP1",gbCtMnAd.getRegionalP1());
            map.put("regionalP2",gbCtMnAd.getRegionalP2());
            map.put("regionalP3",gbCtMnAd.getRegionalP3());
            map.put("regionalP4",gbCtMnAd.getRegionalP4());
            map.put("regionalP5",gbCtMnAd.getRegionalP5());
            map.put("nationalP1",gbCtMnAd.getNationalP1());
            map.put("nationalP2",gbCtMnAd.getNationalP2());
            map.put("nationalP3",gbCtMnAd.getNationalP3());
            map.put("nationalP4",gbCtMnAd.getNationalP4());
            map.put("nationalP5",gbCtMnAd.getNationalP5());
            map.put("asianP1",gbCtMnAd.getAsianP1());
            map.put("asianP2",gbCtMnAd.getAsianP2());
            map.put("asianP3",gbCtMnAd.getAsianP3());
            map.put("asianP4",gbCtMnAd.getAsianP4());
            map.put("asianP5",gbCtMnAd.getAsianP5());
            map.put("internationalP1",gbCtMnAd.getInternationalP1());
            map.put("internationalP2",gbCtMnAd.getInternationalP2());
            map.put("internationalP3",gbCtMnAd.getInternationalP3());
            map.put("internationalP4",gbCtMnAd.getInternationalP4());
            map.put("internationalP5",gbCtMnAd.getInternationalP5());
            ad.add(map);
        }
        resultMap.put("ad",ad);
        //ISO
        List<Map<String, Object>> iso = new ArrayList<>();
        List<GbCtGzIso> gbCtGzIsoList = gbCtGzIsoMapper.selectList(new QueryWrapper<GbCtGzIso>().eq("subject_number", subjectNumber));
        for (GbCtGzIso gbCtGzIso : gbCtGzIsoList) {
            Map<String, Object> map = new HashMap<>();
            map.put("subjectNumber",newSubjectNumber);
            map.put("ciId",gbCtGzIso.getCiId());
            map.put("ciName",gbCtGzIso.getCiName());
            map.put("ciDevelopFee",gbCtGzIso.getCiDevelopFee());
            map.put("ciDevelopDate",gbCtGzIso.getCiDevelopDate());
            iso.add(map);
        }
        resultMap.put("iso",iso);
        //order
        List<Map<String, Object>> order = new ArrayList<>();
        List<GbCtMnChoose> gbCtMnChooses = gbCtMnChooseMapper.selectList(new QueryWrapper<GbCtMnChoose>().eq("contest_id", contestId));
        for (GbCtMnChoose gbCtMnChoose : gbCtMnChooses) {
            Map<String, Object> map = new HashMap<>();
            map.put("subjectNumber",newSubjectNumber);
            map.put("coId",gbCtMnChoose.getCoId());
            map.put("date",gbCtMnChoose.getDate());
            map.put("quarterly",gbCtMnChoose.getQuarterly());
            map.put("cmId",gbCtMnChoose.getCmId());
            map.put("cpId",gbCtMnChoose.getCpId());
            map.put("num",gbCtMnChoose.getNum());
            map.put("totalPrice",gbCtMnChoose.getTotalPrice());
            map.put("deliveryDate",gbCtMnChoose.getDeliveryDate());
            map.put("paymentDate",gbCtMnChoose.getPaymentDay());
            map.put("ciId",gbCtMnChoose.getCiId());
            if(gbCtMnChoose.getXzRound() != null && StringUtils.isNotEmpty(gbCtMnChoose.getXzRound().toString())){
                map.put("xzRound",+gbCtMnChoose.getXzRound());
            }
            if(gbCtMnChoose.getXzGroup() != null && StringUtils.isNotEmpty(gbCtMnChoose.getXzGroup().toString())){
                map.put("xzGroup","人机"+gbCtMnChoose.getXzGroup());
            }
            order.add(map);
        }
        resultMap.put("order",order);
        //market
        List<Map<String, Object>> market = new ArrayList<>();
        List<GbCtGzMarket> markets = gbCtGzMarketMapper.selectList(new QueryWrapper<GbCtGzMarket>().eq("subject_number", subjectNumber));
        for (GbCtGzMarket gbCtGzMarket : markets) {
            Map<String, Object> map = new HashMap<>();
            map.put("subjectNumber",newSubjectNumber);
            map.put("cmId",gbCtGzMarket.getCmId());
            map.put("cmName",gbCtGzMarket.getCmName());
            map.put("cmDevelopFee",gbCtGzMarket.getCmDevelopFee());
            map.put("cmDevelopDate",gbCtGzMarket.getCmDevelopDate());
            market.add(map);
        }
        resultMap.put("market",market);
        //material
        List<Map<String, Object>> material = new ArrayList<>();
        List<GbCtGzMaterial> materials = gbCtGzMaterialMapper.selectList(new QueryWrapper<GbCtGzMaterial>().eq("subject_number", subjectNumber));
        for (GbCtGzMaterial gbCtGzMaterial : materials) {
            Map<String, Object> map = new HashMap<>();
            map.put("subjectNumber",newSubjectNumber);
            map.put("cmId",gbCtGzMaterial.getCmId());
            map.put("cnName",gbCtGzMaterial.getCnName());
            map.put("cmLeadDate",gbCtGzMaterial.getCmLeadDate());
            map.put("cmBuyFee",gbCtGzMaterial.getCmBuyFee());
            map.put("cmPayDate",gbCtGzMaterial.getCmPayDate());
            map.put("num",gbCtGzMaterial.getNum());
            material.add(map);
        }
        resultMap.put("material",material);
        //product
        List<Map<String, Object>> product = new ArrayList<>();
        List<GbCtGzProduct> products = gbCtGzProductMapper.selectList(new QueryWrapper<GbCtGzProduct>().eq("subject_number", subjectNumber));
        for (GbCtGzProduct gbCtGzProduct : products) {
            Map<String, Object> map = new HashMap<>();
            map.put("subjectNumber",newSubjectNumber);
            map.put("cpId",gbCtGzProduct.getCpId());
            map.put("cpName",gbCtGzProduct.getCpName());
            map.put("cpProcessingFee",gbCtGzProduct.getCpProcessingFee());
            map.put("cpDevelopDate",gbCtGzProduct.getCpDevelopDate());
            map.put("cpDevelopFee",gbCtGzProduct.getCpDevelopFee());
            map.put("cpDirectCost",gbCtGzProduct.getCpDirectCost());
            product.add(map);
        }
        resultMap.put("product",product);
        //producing
        List<Map<String, Object>> producing = new ArrayList<>();
        List<GbCtGzProducing> producings = gbCtGzProducingMapper.selectList(new QueryWrapper<GbCtGzProducing>().eq("subject_number", subjectNumber));
        for (GbCtGzProducing gbCtGzProducing : producings) {
            Map<String, Object> map = new HashMap<>();
            map.put("subjectNumber",newSubjectNumber);
            map.put("cpId",gbCtGzProducing.getCpId());
            map.put("cpName",gbCtGzProducing.getCpName());
            map.put("cpMid",gbCtGzProducing.getCpMid());
            map.put("cpNum",gbCtGzProducing.getCpNum());
            producing.add(map);
        }
        resultMap.put("producing",producing);
        //productLine
        List<Map<String, Object>> productLine = new ArrayList<>();
        List<GbCtGzProductLine> productLines = gbCtGzProductLineMapper.selectList(new QueryWrapper<GbCtGzProductLine>().eq("subject_num", subjectNumber));
        for (GbCtGzProductLine line : productLines) {
            Map<String, Object> map = new HashMap<>();
            map.put("subjectNumber",newSubjectNumber);
            map.put("cplId",line.getCplId());
            map.put("cplName",line.getCplName());
            map.put("cplBuyFee",line.getCplBuyFee());
            map.put("cplInstallDate",line.getCplInstallDate());
            map.put("cplProduceDate",line.getCplProduceDate());
            map.put("cplTransferDate",line.getCplTransferDate());
            map.put("cplTransferFee",line.getCplTransferFee());
            map.put("cplMaintenanceFee",line.getCplMaintenanceFee());
            map.put("cplScarpFee",line.getCplScarpFee());
            map.put("cplDepDate",line.getCplDepDate());
            map.put("cplDepFee",line.getCplDepFee());
            map.put("cplSeniorWorker",line.getCplSeniorWorker());
            map.put("cplOrdinaryWorker",line.getCplOrdinaryWorker());
            map.put("cplProduction",line.getCplProduction());
            productLine.add(map);
        }
        resultMap.put("productLine",productLine);

        //productDesign
        List<Map<String, Object>> productDesign = new ArrayList<>();
        List<GbCtGzProductDesign> productDesigns = gbCtGzProductDesignMapper.selectList(new QueryWrapper<GbCtGzProductDesign>().eq("subject_num", subjectNumber));
        for (GbCtGzProductDesign design : productDesigns) {
            Map<String, Object> map = new HashMap<>();
            map.put("subjectNumber",newSubjectNumber);
            map.put("featureName",design.getFeatureName());
            map.put("designNum",design.getDesignNum());
            map.put("costMarkup",design.getCostMarkup());
            map.put("designCost",design.getDesignCost());
            map.put("unitCostUpgrade",design.getUnitCostUpgrade());
            map.put("initialValue",design.getInitialValue());
            map.put("upperLimit",design.getUpperLimit());
            productDesign.add(map);
        }
        resultMap.put("productDesign",productDesign);

        //workerRecruit
        List<Map<String, Object>> workerRecruit = new ArrayList<>();
        List<GbCtGzWorkerRecruit> workerRecruits = gbCtGzWorkerRecruitMapper.selectList(new QueryWrapper<GbCtGzWorkerRecruit>().eq("subject_num", subjectNumber));
        for (GbCtGzWorkerRecruit recruit : workerRecruits) {
            Map<String, Object> map = new HashMap<>();
            map.put("subjectNumber",newSubjectNumber);
            map.put("recruitName",recruit.getRecruitName());
            map.put("recruitNum",recruit.getRecruitNum());
            map.put("initSalExpect",recruit.getInitSalExpect());
            map.put("piece",recruit.getPiece());
            map.put("qtyPerQtr",recruit.getQtyPerQtr());
            map.put("multBonus",recruit.getMultBonus());
            workerRecruit.add(map);
        }
        resultMap.put("workerRecruit",workerRecruit);

        //workerTrain
        List<Map<String, Object>> workerTrain = new ArrayList<>();
        List<GbCtGzWorkerTrain> workerTrains = gbCtGzWorkerTrainMapper.selectList(new QueryWrapper<GbCtGzWorkerTrain>().eq("subject_num", subjectNumber));
        for (GbCtGzWorkerTrain train : workerTrains) {
            Map<String, Object> map = new HashMap<>();
            map.put("subjectNumber",newSubjectNumber);
            map.put("trainingName",train.getTrainingName());
            map.put("cashCost",train.getCashCost());
            map.put("timeCostQuarter",train.getTimeCostQuarter());
            map.put("originalPosition",train.getOriginalPosition());
            map.put("trainedPosition",train.getTrainedPosition());
            map.put("salaryIncreasePercent",train.getSalaryIncreasePercent());
            workerTrain.add(map);
        }
        resultMap.put("workerTrain",workerTrain);

        //loan
        List<Map<String, Object>> loan = new ArrayList<>();
        List<GbCtGzLoan> loans = gbCtGzLoanMapper.selectList(new QueryWrapper<GbCtGzLoan>().eq("subject_num", subjectNumber));
        for (GbCtGzLoan gbCtGzLoan : loans) {
            Map<String, Object> map = new HashMap<>();
            map.put("subjectNumber",newSubjectNumber);
            map.put("loanName",gbCtGzLoan.getLoanName());
            map.put("loanNum",gbCtGzLoan.getLoanNum());
            map.put("loanMax",gbCtGzLoan.getLoanMax());
            map.put("loanTime",gbCtGzLoan.getLoanTime());
            map.put("loanRepayment",gbCtGzLoan.getLoanRepayment());
            map.put("loanRate",gbCtGzLoan.getLoanRate());
            loan.add(map);
        }
        resultMap.put("loan",loan);

        //discount
        List<Map<String, Object>> discount = new ArrayList<>();
        List<GbCtGzDiscount> discounts = gbCtGzDiscountMapper.selectList(new QueryWrapper<GbCtGzDiscount>().eq("subject_num", subjectNumber));
        for (GbCtGzDiscount gbCtGzDiscount : discounts) {
            Map<String, Object> map = new HashMap<>();
            map.put("subjectNumber",newSubjectNumber);
            map.put("discountName",gbCtGzDiscount.getDiscountName());
            map.put("discountNum",gbCtGzDiscount.getDiscountNum());
            map.put("discountTime",gbCtGzDiscount.getDiscountTime());
            map.put("discountInterest",gbCtGzDiscount.getDiscountInterest());
            discount.add(map);
        }
        resultMap.put("discount",discount);

        //classe
        List<Map<String, Object>> classe = new ArrayList<>();
        List<GbCtGzClasses> classes = gbCtGzClassesMapper.selectList(new QueryWrapper<GbCtGzClasses>().eq("subject_num", subjectNumber));
        for (GbCtGzClasses aClass : classes) {
            Map<String, Object> map = new HashMap<>();
            map.put("subjectNumber",newSubjectNumber);
            map.put("classesName",aClass.getClassesName());
            map.put("classesNum",aClass.getClassesNum());
            map.put("outputMulti",aClass.getOutputMulti());
            map.put("efficiencyLoss",aClass.getEfficiencyLoss());
            classe.add(map);
        }
        resultMap.put("classe",classe);

        //workerIncentive
        List<Map<String, Object>> workerIncentive = new ArrayList<>();
        List<GbCtGzWorkerIncentive> workerIncentives = gbCtGzWorkerIncentiveMapper.selectList(new QueryWrapper<GbCtGzWorkerIncentive>().eq("subject_num", subjectNumber));
        for (GbCtGzWorkerIncentive incentive : workerIncentives) {
            Map<String, Object> map = new HashMap<>();
            map.put("subjectNumber",newSubjectNumber);
            map.put("incentiveName",incentive.getIncentiveName());
            map.put("incentiveNum",incentive.getIncentiveNum());
            map.put("efficiencyPercent",incentive.getEfficiencyPercent());
            workerIncentive.add(map);
        }
        resultMap.put("workerIncentive",workerIncentive);

        //num
        List<Map<String, Object>> num = new ArrayList<>();
        List<GbCtGzNum> numList = gbCtGzNumMapper.selectList(new QueryWrapper<GbCtGzNum>().eq("subject_num", subjectNumber));
        for (GbCtGzNum gbCtGzNum : numList) {
            Map<String, Object> map = new HashMap<>();
            map.put("subjectNumber",newSubjectNumber);
            map.put("postNum",gbCtGzNum.getPostNum());
            map.put("consumeMoney",gbCtGzNum.getConsumeMoney());
            map.put("timeCostQuarter",gbCtGzNum.getTimeCostQuarter());
            num.add(map);
        }
        resultMap.put("num",num);

        String filePath = CommonUtil.getClassesPath() + "templates/gb_contest_to_new_gz.xlsx";
        String fileName = "新平台模拟单人版";
        try {
            EasyPOIUtil.exportMultiSheetExcelByMap(response, fileName + ".xlsx", filePath, resultMap, true);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    /**
     * 下载所有企业报表
     */
    public void downAllFinancial(HttpServletResponse response, Integer contestId, Integer year) throws IOException {
        String name = "第"+year+"年各组企业报表";
        try {
            response.setHeader("Content-Disposition", "attachment;filename=" + URLEncoder.encode(name + ".xlsx", "utf-8"));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        allFinancial(response.getOutputStream(),contestId,year);
    }

    public String convertObjToString(Object obj){
        if(obj != null){
            return obj.toString();
        }
        return "";
    }

}
