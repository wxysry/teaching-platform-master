package com.xiyou.main.biz.gbcontest;

import com.alibaba.excel.metadata.Sheet;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.xiyou.common.enums.CodeEnum;
import com.xiyou.common.exception.CustomException;
import com.xiyou.common.utils.R;
import com.xiyou.main.biz.exam.ResourcesBiz;
import com.xiyou.main.dao.gbcontest.*;
import com.xiyou.main.entity.gbcontest.GbCtSubject;
import com.xiyou.main.entity.gbcontest.*;
import com.xiyou.main.pojo.gbcontest.GbCtGzClassesModel;
import com.xiyou.main.pojo.gbcontest.GbCtGzCsModel;
import com.xiyou.main.pojo.gbcontest.GbCtGzDiscountModel;
import com.xiyou.main.pojo.gbcontest.GbCtGzIsoModel;
import com.xiyou.main.pojo.gbcontest.GbCtGzLoanModel;
import com.xiyou.main.pojo.gbcontest.GbCtGzMarketModel;
import com.xiyou.main.pojo.gbcontest.GbCtGzMaterialModel;
import com.xiyou.main.pojo.gbcontest.GbCtGzNumModel;
import com.xiyou.main.pojo.gbcontest.GbCtGzOrderModel;
import com.xiyou.main.pojo.gbcontest.GbCtGzProducingModel;
import com.xiyou.main.pojo.gbcontest.GbCtGzProductDesignModel;
import com.xiyou.main.pojo.gbcontest.GbCtGzProductLineModel;
import com.xiyou.main.pojo.gbcontest.GbCtGzProductModel;
import com.xiyou.main.pojo.gbcontest.GbCtGzWorkerIncentiveModel;
import com.xiyou.main.pojo.gbcontest.GbCtGzWorkerRecruitModel;
import com.xiyou.main.pojo.gbcontest.GbCtGzWorkerTrainModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.xiyou.common.office.utils.EasyExcelUtil.readExcel;

/**
 * @program: multi-module
 * @description:
 * @author: tangcan
 * @create: 2019-08-27 18:18
 **/
@Service
@Transactional(rollbackFor = Exception.class)
public class GbGzBiz {
    private Logger log = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private GbCtSubjectMapper gbCtSubjectMapper;

    @Autowired
    private GbGzExcelMapper gbGzExcelMapper;

    @Autowired
    private GbCtGzCsMapper gbCtGzCsMapper;
    @Autowired
    private GbCtGzAdMapper gbCtGzAdMapper;
    @Autowired
    private GbCtGzIsoMapper gbCtGzIsoMapper;
    @Autowired
    private GbCtGzOrderMapper gbCtGzOrderMapper;
    @Autowired
    private GbCtGzMarketMapper gbCtGzMarketMapper;
    @Autowired
    private GbCtGzMaterialMapper gbCtGzMaterialMapper;
    @Autowired
    private GbCtGzProductMapper gbCtGzProductMapper;
    @Autowired
    private GbCtGzProducingMapper gbCtGzProducingMapper;
    @Autowired
    private GbCtGzProductLineMapper gbCtGzProductLineMapper;
    @Autowired
    private GbCtGzProductDesignMapper gbCtGzProductDesignMapper;
    @Autowired
    private GbCtGzWorkerRecruitMapper gbCtGzWorkerRecruitMapper;
    @Autowired
    private GbCtGzWorkerTrainMapper gbCtGzWorkerTrainMapper;
    @Autowired
    private GbCtGzLoanMapper gbCtGzLoanMapper;
    @Autowired
    private GbCtGzDiscountMapper gbCtGzDiscountMapper;
    @Autowired
    private GbCtGzClassesMapper gbCtGzClassesMapper;
    @Autowired
    private GbCtGzWorkerIncentiveMapper gbCtGzWorkerIncentiveMapper;
    @Autowired
    private GbCtGzNumMapper gbCtGzNumMapper;
    @Autowired
    private ResourcesBiz resourcesBiz;


    @Transactional(rollbackFor = Exception.class)
    public R upload(Integer userId,MultipartFile multipartFile, Integer subjectNumber) {
        GbCtSubject gbCtSubject = gbCtSubjectMapper.getBySubjectNumber(subjectNumber);
        if (gbCtSubject == null) {
            return R.error(CodeEnum.OTHER_ERROR.getCode(), "请选择要上传的题库");
        }



        InputStream inputStream;
        /*
        由于不能直接重复使用inputstream
        因此先做如下处理
         */
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        try {
            inputStream = multipartFile.getInputStream();
            byte[] buffer = new byte[1024];
            int len;
            while ((len = inputStream.read(buffer)) > -1) {
                baos.write(buffer, 0, len);
            }
            baos.flush();
        } catch (IOException e) {
            log.error(e.getMessage());
            return R.error(CodeEnum.FILE_UPLOAD_FAIL.getCode(), e.getMessage());
        }


        // 先将获取的文件存储到资源服务器，然后获取文件名，存入 GbCtSubject的ruleAttachment字段
        MultipartFile[] files = new MultipartFile[1];
        files[0] = multipartFile;
        R r = resourcesBiz.addAttachment(userId, files, "规则预测详单");
        String ruleAttachment = r.get("files").toString();
        //如果返回的规则附件为空,则报异常
        if(StringUtils.isEmpty(ruleAttachment)){
            log.error("返回规则附件为空,返回的R为:{}", JSONObject.toJSONString(r));
            throw new CustomException(CodeEnum.FILE_UPLOAD_FAIL);
        }
        gbCtSubject.setRuleAttachment(ruleAttachment);
        gbCtSubjectMapper.updateById(gbCtSubject);

        Sheet sheet;
        /*
        规则gz_cs
         */
        sheet = new Sheet(1, 2, GbCtGzCsModel.class);
        List<GbCtGzCsModel> gbCtGzCsModelList;
        try {
            gbCtGzCsModelList = readExcel(new ByteArrayInputStream(baos.toByteArray()), sheet);
        } catch (Exception e) {
            return R.error(CodeEnum.FILE_UPLOAD_FAIL.getCode(), "规则gz_cs表" + e.getMessage());
        }
        if (gbCtGzCsModelList == null || gbCtGzCsModelList.size() == 0) {
            return R.error(CodeEnum.FILE_UPLOAD_FAIL.getCode(), "上传失败！规则gz_cs表无数据");
        }
        for (GbCtGzCsModel gbCtGzCsModel : gbCtGzCsModelList) {
            if (gbCtGzCsModel.getSubjectNumber() == null) {
                return R.error(CodeEnum.FILE_UPLOAD_FAIL.getCode(), "规则gz_cs表存在多余空行");
            }
//            if (!subjectNumber.equals(gbCtGzCsModel.getSubjectNumber())) {
//                return R.error(CodeEnum.FILE_UPLOAD_FAIL.getCode(), "规则gz_cs表题库号应该是：" + subjectNumber);
//            }
            gbCtGzCsModel.setSubjectNumber(subjectNumber);
        }



        /*
        ISO规则表gz_ISO
         */
        sheet = new Sheet(2, 2, GbCtGzIsoModel.class);
        List<GbCtGzIsoModel> gbCtGzIsoModelList;
        try {
            gbCtGzIsoModelList = readExcel(new ByteArrayInputStream(baos.toByteArray()), sheet);
        } catch (Exception e) {
            return R.error(CodeEnum.FILE_UPLOAD_FAIL.getCode(), "ISO规则表gz_ISO表" + e.getMessage());
        }
        if (gbCtGzIsoModelList == null || gbCtGzIsoModelList.size() == 0) {
            return R.error(CodeEnum.FILE_UPLOAD_FAIL.getCode(), "上传失败！ISO规则表gz_ISO表无数据");
        }
        for (GbCtGzIsoModel gbCtGzIsoModel : gbCtGzIsoModelList) {
            if (gbCtGzIsoModel.getSubjectNumber() == null) {
                return R.error(CodeEnum.FILE_UPLOAD_FAIL.getCode(), "ISO规则表gz_ISO表存在多余空行");
            }
//            if (!subjectNumber.equals(gbCtGzIsoModel.getSubjectNumber())) {
//                return R.error(CodeEnum.FILE_UPLOAD_FAIL.getCode(), "ISO规则表gz_ISO表题库号应该是：" + subjectNumber);
//            }
            gbCtGzIsoModel.setSubjectNumber(subjectNumber);
        }

        /*
        市场订单gz_order
         */
        sheet = new Sheet(3, 1, GbCtGzOrderModel.class);
        List<GbCtGzOrderModel> gbCtGzOrderModelList;
        try {
            gbCtGzOrderModelList = readExcel(new ByteArrayInputStream(baos.toByteArray()), sheet);
        } catch (Exception e) {
            return R.error(CodeEnum.FILE_UPLOAD_FAIL.getCode(), "市场订单gz_order表" + e.getMessage());
        }
        if (gbCtGzOrderModelList == null || gbCtGzOrderModelList.size() == 0) {
            return R.error(CodeEnum.FILE_UPLOAD_FAIL.getCode(), "上传失败！市场订单gz_order表无数据");
        }
        for (GbCtGzOrderModel gbCtGzOrderModel : gbCtGzOrderModelList) {
            if (gbCtGzOrderModel.getSubjectNumber() == null) {
                return R.error(CodeEnum.FILE_UPLOAD_FAIL.getCode(), "市场订单gz_order表存在多余空行");
            }
            if (ObjectUtils.isEmpty(gbCtGzOrderModel.getCiId())) {
                gbCtGzOrderModel.setCiId(null);
            }
//            if (!subjectNumber.equals(gbCtGzOrderModel.getSubjectNumber())) {
//                return R.error(CodeEnum.FILE_UPLOAD_FAIL.getCode(), "市场订单gz_order表题库号应该是：" + subjectNumber);
//            }
            gbCtGzOrderModel.setSubjectNumber(subjectNumber);
        }

        /*
        市场规则表gz_market
         */
        sheet = new Sheet(4, 2, com.xiyou.main.pojo.gbcontest.GbCtGzMarketModel.class);
        List<GbCtGzMarketModel> gbCtGzMarketModelList;
        try {
            gbCtGzMarketModelList = readExcel(new ByteArrayInputStream(baos.toByteArray()), sheet);
        } catch (Exception e) {
            return R.error(CodeEnum.FILE_UPLOAD_FAIL.getCode(), "市场规则表gz_market表" + e.getMessage());
        }
        if (gbCtGzMarketModelList == null || gbCtGzMarketModelList.size() == 0) {
            return R.error(CodeEnum.FILE_UPLOAD_FAIL.getCode(), "上传失败！市场规则表gz_market表无数据");
        }
        for (GbCtGzMarketModel gbCtGzMarketModel : gbCtGzMarketModelList) {
            if (gbCtGzMarketModel.getSubjectNumber() == null) {
                return R.error(CodeEnum.FILE_UPLOAD_FAIL.getCode(), "市场规则表gz_market表存在多余空行");
            }
//            if (!subjectNumber.equals(gbCtGzMarketModel.getSubjectNumber())) {
//                return R.error(CodeEnum.FILE_UPLOAD_FAIL.getCode(), "市场规则表gz_market表题库号应该是：" + subjectNumber);
//            }
            gbCtGzMarketModel.setSubjectNumber(subjectNumber);
        }

        /*
        原料规则表gz_material
         */
        sheet = new Sheet(5, 2, GbCtGzMaterialModel.class);
        List<GbCtGzMaterialModel> gbCtGzMaterialModelList;
        try {
            gbCtGzMaterialModelList = readExcel(new ByteArrayInputStream(baos.toByteArray()), sheet);
        } catch (Exception e) {
            return R.error(CodeEnum.FILE_UPLOAD_FAIL.getCode(), "原料规则表gz_material表" + e.getMessage());
        }
        if (gbCtGzMaterialModelList == null || gbCtGzMaterialModelList.size() == 0) {
            return R.error(CodeEnum.FILE_UPLOAD_FAIL.getCode(), "上传失败！原料规则表gz_material表无数据");
        }
        for (GbCtGzMaterialModel gbCtGzMaterialModel : gbCtGzMaterialModelList) {
            if (gbCtGzMaterialModel.getSubjectNumber() == null) {
                return R.error(CodeEnum.FILE_UPLOAD_FAIL.getCode(), "原料规则表gz_material表存在多余空行");
            }
//            if (!subjectNumber.equals(gbCtGzMaterialModel.getSubjectNumber())) {
//                return R.error(CodeEnum.FILE_UPLOAD_FAIL.getCode(), "原料规则表gz_material表题库号应该是：" + subjectNumber);
//            }
            gbCtGzMaterialModel.setSubjectNumber(subjectNumber);
        }

        /*
        产品规则表gz_product
         */
        sheet = new Sheet(6, 2, GbCtGzProductModel.class);
        List<GbCtGzProductModel> gbCtGzProductModelList;
        try {
            gbCtGzProductModelList = readExcel(new ByteArrayInputStream(baos.toByteArray()), sheet);
        } catch (Exception e) {
            return R.error(CodeEnum.FILE_UPLOAD_FAIL.getCode(), "产品规则表gz_product表" + e.getMessage());
        }
        if (gbCtGzProductModelList == null || gbCtGzProductModelList.size() == 0) {
            return R.error(CodeEnum.FILE_UPLOAD_FAIL.getCode(), "上传失败！产品规则表gz_product表无数据");
        }
        for (GbCtGzProductModel gbCtGzProductModel : gbCtGzProductModelList) {
            if (gbCtGzProductModel.getSubjectNumber() == null) {
                return R.error(CodeEnum.FILE_UPLOAD_FAIL.getCode(), "产品规则表gz_product表存在多余空行");
            }
//            if (!subjectNumber.equals(gbCtGzProductModel.getSubjectNumber())) {
//                return R.error(CodeEnum.FILE_UPLOAD_FAIL.getCode(), "产品规则表gz_product表题库号应该是：" + subjectNumber);
//            }
            gbCtGzProductModel.setSubjectNumber(subjectNumber);
        }


        /*
        生产规则表gz_producing
         */
        sheet = new Sheet(7, 2, GbCtGzProducingModel.class);
        List<GbCtGzProducingModel> gbCtGzProducingModelList;
        try {
            gbCtGzProducingModelList = readExcel(new ByteArrayInputStream(baos.toByteArray()), sheet);
        } catch (Exception e) {
            return R.error(CodeEnum.FILE_UPLOAD_FAIL.getCode(), "生产规则表gz_producing表" + e.getMessage());
        }
        if (gbCtGzProducingModelList == null || gbCtGzProducingModelList.size() == 0) {
            return R.error(CodeEnum.FILE_UPLOAD_FAIL.getCode(), "上传失败！生产规则表gz_producing表无数据");
        }
        for (GbCtGzProducingModel gbCtGzProducingModel : gbCtGzProducingModelList) {
            if (gbCtGzProducingModel.getSubjectNumber() == null) {
                return R.error(CodeEnum.FILE_UPLOAD_FAIL.getCode(), "生产规则表gz_producing表存在多余空行");
            }
//            if (!subjectNumber.equals(gbCtGzProducingModel.getSubjectNumber())) {
//                return R.error(CodeEnum.FILE_UPLOAD_FAIL.getCode(), "生产规则表gz_producing表题库号应该是：" + subjectNumber);
//            }
            gbCtGzProducingModel.setSubjectNumber(subjectNumber);
        }

        /*
        生产线规则表gz_product_line
         */
        sheet = new Sheet(8, 2, GbCtGzProductLineModel.class);
        List<GbCtGzProductLineModel> gbCtGzProductLineModelList;
        try {
            gbCtGzProductLineModelList = readExcel(new ByteArrayInputStream(baos.toByteArray()), sheet);
        } catch (Exception e) {
            return R.error(CodeEnum.FILE_UPLOAD_FAIL.getCode(), "生产线规则表gz_product_line表" + e.getMessage());
        }
        if (gbCtGzProductLineModelList == null || gbCtGzProductLineModelList.size() == 0) {
            return R.error(CodeEnum.FILE_UPLOAD_FAIL.getCode(), "上传失败！生产线规则表gz_product_line表无数据");
        }
        for (GbCtGzProductLineModel gbCtGzProductLineModel : gbCtGzProductLineModelList) {
            if (gbCtGzProductLineModel.getSubjectNum() == null) {
                return R.error(CodeEnum.FILE_UPLOAD_FAIL.getCode(), "生产线规则表gz_product_line表存在多余空行");
            }
//            if (!subjectNumber.equals(gbCtGzProductLineModel.getSubjectNum())) {
//                return R.error(CodeEnum.FILE_UPLOAD_FAIL.getCode(), "生产线规则表gz_product_line表题库号应该是：" + subjectNumber);
//            }
            gbCtGzProductLineModel.setSubjectNum(subjectNumber);
        }




        /*
        产品设计gz_product_design（新增）
         */
        sheet = new Sheet(9, 2, GbCtGzProductDesignModel.class);
        List<GbCtGzProductDesignModel> gbCtGzProductDesignModelList;
        try {
            gbCtGzProductDesignModelList = readExcel(new ByteArrayInputStream(baos.toByteArray()), sheet);
        } catch (Exception e) {
            return R.error(CodeEnum.FILE_UPLOAD_FAIL.getCode(), "产品设计规则表gz_product_design表" + e.getMessage());
        }
        if (gbCtGzProductDesignModelList == null || gbCtGzProductDesignModelList.size() == 0) {
            return R.error(CodeEnum.FILE_UPLOAD_FAIL.getCode(), "上传失败！产品设计规则表gz_product_design表无数据");
        }
        for (GbCtGzProductDesignModel gbCtGzProductDesignModel : gbCtGzProductDesignModelList) {
            if (gbCtGzProductDesignModel.getSubjectNum() == null) {
                return R.error(CodeEnum.FILE_UPLOAD_FAIL.getCode(), "产品设计规则表gz_product_design表存在多余空行");
            }
//            if (!subjectNumber.equals(gbCtGzProductDesignModel.getSubjectNum())) {
//                return R.error(CodeEnum.FILE_UPLOAD_FAIL.getCode(), "产品设计规则表gz_product_design表题库号应该是：" + subjectNumber);
//            }
            gbCtGzProductDesignModel.setSubjectNum(subjectNumber);
        }

        /*
        工人招聘gz_worker_recruit（新增）
         */
        sheet = new Sheet(10, 2, GbCtGzWorkerRecruitModel.class);
        List<GbCtGzWorkerRecruitModel> gbCtGzWorkerRecruitModelList;
        try {
            gbCtGzWorkerRecruitModelList = readExcel(new ByteArrayInputStream(baos.toByteArray()), sheet);
        } catch (Exception e) {
            return R.error(CodeEnum.FILE_UPLOAD_FAIL.getCode(), "工人招聘规则表gz_worker_recruit表" + e.getMessage());
        }
        if (gbCtGzWorkerRecruitModelList == null || gbCtGzWorkerRecruitModelList.size() == 0) {
            return R.error(CodeEnum.FILE_UPLOAD_FAIL.getCode(), "上传失败！工人招聘规则表gz_worker_recruit表无数据");
        }
        for (GbCtGzWorkerRecruitModel gbCtGzWorkerRecruitModel : gbCtGzWorkerRecruitModelList) {
            if (gbCtGzWorkerRecruitModel.getSubjectNum() == null) {
                return R.error(CodeEnum.FILE_UPLOAD_FAIL.getCode(), "工人招聘规则表gz_worker_recruit表存在多余空行");
            }
//            if (!subjectNumber.equals(gbCtGzWorkerRecruitModel.getSubjectNum())) {
//                return R.error(CodeEnum.FILE_UPLOAD_FAIL.getCode(), "工人招聘规则表gz_worker_recruit表题库号应该是：" + subjectNumber);
//            }
            gbCtGzWorkerRecruitModel.setSubjectNum(subjectNumber);
        }



        /*
        工人培训gz_worker_train(新增)
         */
        sheet = new Sheet(11, 2, GbCtGzWorkerTrainModel.class);
        List<GbCtGzWorkerTrainModel> gbCtGzWorkerTrainModelList;
        try {
            gbCtGzWorkerTrainModelList = readExcel(new ByteArrayInputStream(baos.toByteArray()), sheet);
        } catch (Exception e) {
            return R.error(CodeEnum.FILE_UPLOAD_FAIL.getCode(), "工人培训规则表gz_worker_train表" + e.getMessage());
        }
        if (gbCtGzWorkerTrainModelList == null || gbCtGzWorkerTrainModelList.size() == 0) {
            return R.error(CodeEnum.FILE_UPLOAD_FAIL.getCode(), "上传失败！工人培训规则表gz_worker_train表无数据");
        }
        for (GbCtGzWorkerTrainModel gbCtGzWorkerTrainModel : gbCtGzWorkerTrainModelList) {
            if (gbCtGzWorkerTrainModel.getSubjectNum() == null) {
                return R.error(CodeEnum.FILE_UPLOAD_FAIL.getCode(), "工人培训规则表gz_worker_train表存在多余空行");
            }
//            if (!subjectNumber.equals(gbCtGzWorkerTrainModel.getSubjectNum())) {
//                return R.error(CodeEnum.FILE_UPLOAD_FAIL.getCode(), "工人培训规则表gz_worker_train表题库号应该是：" + subjectNumber);
//            }
            gbCtGzWorkerTrainModel.setSubjectNum(subjectNumber);
        }



        /*
        贷款规则gz_loan(新增)
         */
        sheet = new Sheet(12, 2, GbCtGzLoanModel.class);
        List<GbCtGzLoanModel> gbCtGzLoanModelList;
        try {
            gbCtGzLoanModelList = readExcel(new ByteArrayInputStream(baos.toByteArray()), sheet);
        } catch (Exception e) {
            return R.error(CodeEnum.FILE_UPLOAD_FAIL.getCode(), "贷款规则表gz_loann表" + e.getMessage());
        }
        if (gbCtGzLoanModelList == null || gbCtGzLoanModelList.size() == 0) {
            return R.error(CodeEnum.FILE_UPLOAD_FAIL.getCode(), "上传失败！贷款规则表gz_loan表无数据");
        }
        for (GbCtGzLoanModel gbCtGzLoanModel : gbCtGzLoanModelList) {
            if (gbCtGzLoanModel.getSubjectNum() == null) {
                return R.error(CodeEnum.FILE_UPLOAD_FAIL.getCode(), "贷款规则表gz_loan表存在多余空行");
            }
//            if (!subjectNumber.equals(gbCtGzLoanModel.getSubjectNum())) {
//                return R.error(CodeEnum.FILE_UPLOAD_FAIL.getCode(), "贷款规则表gz_loan表题库号应该是：" + subjectNumber);
//            }
            gbCtGzLoanModel.setSubjectNum(subjectNumber);
        }


        /*
        贴现规则gz_discount（新增）
         */
        sheet = new Sheet(13, 2, GbCtGzDiscountModel.class);
        List<GbCtGzDiscountModel> gbCtGzDiscountModelList;
        try {
            gbCtGzDiscountModelList = readExcel(new ByteArrayInputStream(baos.toByteArray()), sheet);
        } catch (Exception e) {
            return R.error(CodeEnum.FILE_UPLOAD_FAIL.getCode(), "贴现规则表gz_discount表" + e.getMessage());
        }
        if (gbCtGzDiscountModelList == null || gbCtGzDiscountModelList.size() == 0) {
            return R.error(CodeEnum.FILE_UPLOAD_FAIL.getCode(), "上传失败！贴现规则表gz_discount表无数据");
        }
        for (GbCtGzDiscountModel gbCtGzDiscountModel : gbCtGzDiscountModelList) {
            if (gbCtGzDiscountModel.getSubjectNum() == null) {
                return R.error(CodeEnum.FILE_UPLOAD_FAIL.getCode(), "贴现规则表gz_discount表存在多余空行");
            }
//            if (!subjectNumber.equals(gbCtGzDiscountModel.getSubjectNum())) {
//                return R.error(CodeEnum.FILE_UPLOAD_FAIL.getCode(), "贴现规则表gz_discount表题库号应该是：" + subjectNumber);
//            }
            gbCtGzDiscountModel.setSubjectNum(subjectNumber);
        }



        /*
        班次规则gz_classes(新增)
         */
        sheet = new Sheet(14, 2, GbCtGzClassesModel.class);
        List<GbCtGzClassesModel> gbCtGzClassesModelList;
        try {
            gbCtGzClassesModelList = readExcel(new ByteArrayInputStream(baos.toByteArray()), sheet);
        } catch (Exception e) {
            return R.error(CodeEnum.FILE_UPLOAD_FAIL.getCode(), "班次规则表gz_classes表" + e.getMessage());
        }
        if (gbCtGzClassesModelList == null || gbCtGzClassesModelList.size() == 0) {
            return R.error(CodeEnum.FILE_UPLOAD_FAIL.getCode(), "上传失败！班次规则表gz_classes表无数据");
        }
        for (GbCtGzClassesModel gbCtGzClassesModel : gbCtGzClassesModelList) {
            if (gbCtGzClassesModel.getSubjectNum() == null) {
                return R.error(CodeEnum.FILE_UPLOAD_FAIL.getCode(), "班次规则表gz_classes表存在多余空行");
            }
//            if (!subjectNumber.equals(gbCtGzClassesModel.getSubjectNum())) {
//                return R.error(CodeEnum.FILE_UPLOAD_FAIL.getCode(), "班次规则表gz_classes表题库号应该是：" + subjectNumber);
//            }
            gbCtGzClassesModel.setSubjectNum(subjectNumber);
        }


        /*
        员工激励gz_worker_incentive(新增)
         */
        sheet = new Sheet(15, 2, GbCtGzWorkerIncentiveModel.class);
        List<GbCtGzWorkerIncentiveModel> gbCtGzWorkerIncentiveModelList;
        try {
            gbCtGzWorkerIncentiveModelList = readExcel(new ByteArrayInputStream(baos.toByteArray()), sheet);
        } catch (Exception e) {
            return R.error(CodeEnum.FILE_UPLOAD_FAIL.getCode(), "员工激励规则表gz_worker_incentive表" + e.getMessage());
        }
        if (gbCtGzWorkerIncentiveModelList == null || gbCtGzWorkerIncentiveModelList.size() == 0) {
            return R.error(CodeEnum.FILE_UPLOAD_FAIL.getCode(), "上传失败！员工激励规则表gz_worker_incentive表无数据");
        }
        for (GbCtGzWorkerIncentiveModel gbCtGzWorkerIncentiveModel : gbCtGzWorkerIncentiveModelList) {
            if (gbCtGzWorkerIncentiveModel.getSubjectNum() == null) {
                return R.error(CodeEnum.FILE_UPLOAD_FAIL.getCode(), "员工激励规则表gz_worker_incentive表存在多余空行");
            }
//            if (!subjectNumber.equals(gbCtGzWorkerIncentiveModel.getSubjectNum())) {
//                return R.error(CodeEnum.FILE_UPLOAD_FAIL.getCode(), "员工激励规则表gz_worker_incentive表题库号应该是：" + subjectNumber);
//            }
            gbCtGzWorkerIncentiveModel.setSubjectNum(subjectNumber);
        }


        /*
        数字化岗位gz_num(新增)
         */
        sheet = new Sheet(16, 2, GbCtGzNumModel.class);
        List<GbCtGzNumModel> gbCtGzNumModelList;
        try {
            gbCtGzNumModelList = readExcel(new ByteArrayInputStream(baos.toByteArray()), sheet);
        } catch (Exception e) {
            return R.error(CodeEnum.FILE_UPLOAD_FAIL.getCode(), "数字化岗位规则表gz_num表" + e.getMessage());
        }
        if (gbCtGzNumModelList == null || gbCtGzNumModelList.size() == 0) {
            return R.error(CodeEnum.FILE_UPLOAD_FAIL.getCode(), "上传失败！数字化岗位规则表gz_num表无数据");
        }
        for (GbCtGzNumModel gbCtGzNumModel : gbCtGzNumModelList) {
            if (gbCtGzNumModel.getSubjectNum() == null) {
                return R.error(CodeEnum.FILE_UPLOAD_FAIL.getCode(), "数字化岗位规则表gz_num表存在多余空行");
            }
//            if (!subjectNumber.equals(gbCtGzNumModel.getSubjectNum())) {
//                return R.error(CodeEnum.FILE_UPLOAD_FAIL.getCode(), "数字化岗位规则表gz_num表题库号应该是：" + subjectNumber);
//            }
            gbCtGzNumModel.setSubjectNum(subjectNumber);
        }



        if (gbCtSubject.getUpload() == 1) {
            /*
            先删除之前上传过，但是现在不需要上传的,再存库
             */
            gbGzExcelMapper.removeRestCs(gbCtGzCsModelList);
//            gbGzExcelMapper.removeRestAd(gbCtGzAdModelList);
            gbGzExcelMapper.removeRestIso(gbCtGzIsoModelList);
            gbGzExcelMapper.removeRestOrder(gbCtGzOrderModelList);
            gbGzExcelMapper.removeRestMarket(gbCtGzMarketModelList);
            gbGzExcelMapper.removeRestMaterial(gbCtGzMaterialModelList);
            gbGzExcelMapper.removeRestProduct(gbCtGzProductModelList);
            gbGzExcelMapper.removeRestProducing(gbCtGzProducingModelList);
            gbGzExcelMapper.removeRestProductLine(gbCtGzProductLineModelList);

            gbGzExcelMapper.removeRestProductDesign(gbCtGzProductDesignModelList);
            gbGzExcelMapper.removeRestWorkerRecruit(gbCtGzWorkerRecruitModelList);
            gbGzExcelMapper.removeRestWorkerTrain(gbCtGzWorkerTrainModelList);
            gbGzExcelMapper.removeRestLoan(gbCtGzLoanModelList);
            gbGzExcelMapper.removeRestDiscount(gbCtGzDiscountModelList);
            gbGzExcelMapper.removeRestClasses(gbCtGzClassesModelList);
            gbGzExcelMapper.removeRestWorkerIncentive(gbCtGzWorkerIncentiveModelList);
            gbGzExcelMapper.removeRestNum(gbCtGzNumModelList);

        }
        /*
        存库
         */
        gbGzExcelMapper.insertCs(gbCtGzCsModelList);
//        gbGzExcelMapper.insertAd(gbCtGzAdModelList);
        gbGzExcelMapper.insertIso(gbCtGzIsoModelList);
        gbGzExcelMapper.insertOrder(gbCtGzOrderModelList);
        gbGzExcelMapper.insertMarket(gbCtGzMarketModelList);
        gbGzExcelMapper.insertMaterial(gbCtGzMaterialModelList);
        gbGzExcelMapper.insertProduct(gbCtGzProductModelList);
        gbGzExcelMapper.insertProducing(gbCtGzProducingModelList);
        gbGzExcelMapper.insertProductLine(gbCtGzProductLineModelList);
        gbGzExcelMapper.insertProductDesign(gbCtGzProductDesignModelList);
        gbGzExcelMapper.insertWorkerRecruit(gbCtGzWorkerRecruitModelList);
        gbGzExcelMapper.insertWorkerTrain(gbCtGzWorkerTrainModelList);
        gbGzExcelMapper.insertLoan(gbCtGzLoanModelList);
        gbGzExcelMapper.insertDiscount(gbCtGzDiscountModelList);
        gbGzExcelMapper.insertClasses(gbCtGzClassesModelList);
        gbGzExcelMapper.insertWorkerIncentive(gbCtGzWorkerIncentiveModelList);
        gbGzExcelMapper.insertNum(gbCtGzNumModelList);


        /*
        更新此题库已经上传规则表
         */
        if (gbCtSubject.getUpload() == 0) {
            gbCtSubjectMapper.updateUpload(subjectNumber);
        }

        return R.success();
    }


    public R getRuleBySubjectNumber(Integer subjectNumber) {
        Map<String, Object> gzList = getRuleMapBySubjectNumber(subjectNumber);
        return R.success(gzList);
    }

    public Map<String, Object> getRuleMapBySubjectNumber(Integer subjectNumber){
        Map<String, Object> gzList = new HashMap<>();
        List<GbCtGzCs> gbCtGzCsList = gbCtGzCsMapper.selectList(new QueryWrapper<GbCtGzCs>().eq("subject_number", subjectNumber));
//        List<GbCtGzAd> gbCtGzAdList = gbCtGzAdMapper.selectList(new QueryWrapper<GbCtGzAd>().eq("subject_num", subjectNumber));
        List<GbCtGzIso> gbCtGzIsoList = gbCtGzIsoMapper.selectList(new QueryWrapper<GbCtGzIso>().eq("subject_number", subjectNumber));
        List<GbCtGzOrder> gbCtGzOrderList = gbCtGzOrderMapper.selectList(new QueryWrapper<GbCtGzOrder>().eq("subject_number", subjectNumber));
        List<GbCtGzMarket> gbCtGzMarketList = gbCtGzMarketMapper.selectList(new QueryWrapper<GbCtGzMarket>().eq("subject_number", subjectNumber));
        List<GbCtGzMaterial> gbCtGzMaterialList = gbCtGzMaterialMapper.selectList(new QueryWrapper<GbCtGzMaterial>().eq("subject_number", subjectNumber));
        List<GbCtGzProduct> gbCtGzProductList = gbCtGzProductMapper.selectList(new QueryWrapper<GbCtGzProduct>().eq("subject_number", subjectNumber));
        List<GbCtGzProducing> gbCtGzProducingList = gbCtGzProducingMapper.selectList(new QueryWrapper<GbCtGzProducing>().eq("subject_number", subjectNumber));
        List<GbCtGzProductLine> gbCtGzProductLineList = gbCtGzProductLineMapper.selectList(new QueryWrapper<GbCtGzProductLine>().eq("subject_num", subjectNumber));


        List<GbCtGzProductDesign> gbCtGzProductDesignList = gbCtGzProductDesignMapper.selectList(new QueryWrapper<GbCtGzProductDesign>().eq("subject_num", subjectNumber));
        List<GbCtGzWorkerRecruit> gbCtGzWorkerRecruitList = gbCtGzWorkerRecruitMapper.selectList(new QueryWrapper<GbCtGzWorkerRecruit>().eq("subject_num", subjectNumber));
        List<GbCtGzWorkerTrain> gbCtGzWorkerTrainList = gbCtGzWorkerTrainMapper.selectList(new QueryWrapper<GbCtGzWorkerTrain>().eq("subject_num", subjectNumber));
        List<GbCtGzLoan> gbCtGzLoanList = gbCtGzLoanMapper.selectList(new QueryWrapper<GbCtGzLoan>().eq("subject_num", subjectNumber));
        List<GbCtGzDiscount> gbCtGzDiscountList = gbCtGzDiscountMapper.selectList(new QueryWrapper<GbCtGzDiscount>().eq("subject_num", subjectNumber));
        List<GbCtGzClasses> gbCtGzClassesList = gbCtGzClassesMapper.selectList(new QueryWrapper<GbCtGzClasses>().eq("subject_num", subjectNumber));
        List<GbCtGzWorkerIncentive> gbCtGzWorkerIncentiveList = gbCtGzWorkerIncentiveMapper.selectList(new QueryWrapper<GbCtGzWorkerIncentive>().eq("subject_num", subjectNumber));
        List<GbCtGzNum> gbCtGzNumList = gbCtGzNumMapper.selectList(new QueryWrapper<GbCtGzNum>().eq("subject_num", subjectNumber));

        gzList.put("gbCtGzCsList", gbCtGzCsList);
//        gzList.put("gbCtGzAdList", gbCtGzAdList);
        gzList.put("gbCtGzIsoList", gbCtGzIsoList);
        gzList.put("gbCtGzOrderList", gbCtGzOrderList);
        gzList.put("gbCtGzMarketList", gbCtGzMarketList);
        gzList.put("gbCtGzMaterialList", gbCtGzMaterialList);
        gzList.put("gbCtGzProductList", gbCtGzProductList);
        gzList.put("gbCtGzProducingList", gbCtGzProducingList);
        gzList.put("gbCtGzProductLineList", gbCtGzProductLineList);

        gzList.put("gbCtGzProductDesignList", gbCtGzProductDesignList);
        gzList.put("gbCtGzWorkerRecruitList", gbCtGzWorkerRecruitList);
        gzList.put("gbCtGzWorkerTrainList", gbCtGzWorkerTrainList);
        gzList.put("gbCtGzLoanList", gbCtGzLoanList);
        gzList.put("gbCtGzDiscountList", gbCtGzDiscountList);
        gzList.put("gbCtGzClassesList", gbCtGzClassesList);
        gzList.put("gbCtGzWorkerIncentiveList", gbCtGzWorkerIncentiveList);
        gzList.put("gbCtGzNumList", gbCtGzNumList);
        return gzList;
    }



}
