package com.xiyou.main.biz.gbcontest;

import com.xiyou.common.enums.CodeEnum;
import com.xiyou.common.utils.R;
import com.xiyou.main.biz.gbcontest.GbActionBiz;
import com.xiyou.main.dao.gbcontest.GbCtCashflowMapper;
import com.xiyou.main.dao.gbcontest.GbCtGzIsoMapper;
import com.xiyou.main.dao.gbcontest.GbCtYfIsoMapper;
import com.xiyou.main.entity.gbcontest.GbCtCashflow;
import com.xiyou.main.entity.gbcontest.GbCtYfIso;
import com.xiyou.main.entity.gbcontest.GbCtGzIso;
import com.xiyou.main.entity.gbcontest.GbCtYfIso;
import com.xiyou.main.params.gbcontest.GbISOInvestParam;
import com.xiyou.main.service.gbcontest.GbCtCashflowService;
import com.xiyou.main.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

/**
 * @program: multi-module
 * @description:
 * @author: tangcan
 * @create: 2019-09-01 12:43
 **/
@Service
@Transactional(rollbackFor = Exception.class)
public class GbISOInvestBiz {
    @Autowired
    private GbCtYfIsoMapper ctYfIsoMapper;
    @Autowired
    private GbCtCashflowService ctCashflowService;
    @Autowired
    private GbCtCashflowMapper cashflowMapper;
    @Autowired
    private GbCtGzIsoMapper gbCtGzIsoMapper;
    @Autowired
    private GbActionBiz gbActionBiz;

    public R list(Integer contestId, Integer studentId) {
        List<Map<String, Object>> list = ctYfIsoMapper.getIsoInvestList(studentId, contestId);
        return R.success().put("isoList", list);
    }

    public R commit(GbISOInvestParam isoInvestParam) {
        if (isoInvestParam.getIsoId() == null) {
            return R.error(CodeEnum.PARAM_ERROR.getCode(), "请选择ISO投资");
        }
        // 现金
        Integer cash = cashflowMapper.getCash(isoInvestParam.getStudentId(), isoInvestParam.getContestId());
        if (cash == null) {
            cash = 0;
        }

        GbCtYfIso gbCtYfIso = ctYfIsoMapper.selectById(isoInvestParam.getIsoId());
        if (GbCtYfIso.ISO_YF_ING.equals(gbCtYfIso.getDiState()) || GbCtYfIso.ISO_YF_FINISH.equals(gbCtYfIso.getDiState())) {
            return R.error(CodeEnum.OTHER_ERROR.getCode(), "无需重复研发");
        }

        GbCtGzIso gbCtGzIso = gbCtGzIsoMapper.selectById(gbCtYfIso.getDiCiid());
        Integer investSum = gbCtGzIso.getCiDevelopFee();
        // 当现金<sum（勾选的投资费用总和），则报错【现金不足】
        if (cash < investSum) {
            return R.error(CodeEnum.OTHER_ERROR.getCode(), "现金不足");
        }
        // 刷新cashflow，ID、ISO开拓、0、sum（勾选的投资费用总和），现金+增加-减少、扣减ISO开拓&sum（勾选的投资费用总和）、当前时间
        ctCashflowService.save(new GbCtCashflow()
                .setStudentId(isoInvestParam.getStudentId())
                .setContestId(isoInvestParam.getContestId())
                .setCAction("ISO开拓")
                .setCIn(0)
                .setCOut(investSum)
                .setCSurplus(cash - investSum)
                .setCComment("扣减ISO"+gbCtGzIso.getCiName()+"开拓" + investSum + "元")
                .setCDate(isoInvestParam.getDate()));
        // 刷新yf_ISO,勾选记录now_date +1，Remain_Date -1
        Integer ciDevelopDate = gbCtGzIso.getCiDevelopDate();
        Integer finishDate = DateUtils.addYearAndSeasonTime(isoInvestParam.getDate(),ciDevelopDate);
        if(ciDevelopDate != null && ciDevelopDate>0 ){
            gbCtYfIso.setDiState(GbCtYfIso.ISO_YF_ING);
        }else {
            gbCtYfIso.setDiState(GbCtYfIso.ISO_YF_FINISH);
        }
        gbCtYfIso.setDiStartDate(isoInvestParam.getDate())
                .setDiFinishDate(finishDate);
        ctYfIsoMapper.updateById(gbCtYfIso);

        //计算报表,判断是否破产
        gbActionBiz.isBankruptcy(isoInvestParam.getStudentId(),isoInvestParam.getContestId(),isoInvestParam.getDate());

        return R.success();
    }
}
