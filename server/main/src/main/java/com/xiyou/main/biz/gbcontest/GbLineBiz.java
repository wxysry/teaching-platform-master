package com.xiyou.main.biz.gbcontest;

import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.xiyou.common.enums.CodeEnum;
import com.xiyou.common.utils.R;
import com.xiyou.main.dao.gbcontest.*;
import com.xiyou.main.entity.gbcontest.*;
import com.xiyou.main.enums.CashFlowActionEnum;
import com.xiyou.main.enums.ErrorEnum;
import com.xiyou.main.params.gbcontest.GbLineParam;
import com.xiyou.main.service.gbcontest.*;
import com.xiyou.main.utils.DateUtils;
import com.xiyou.main.vo.gbcontest.GbCtKcMaterialNumVo;
import com.xiyou.main.vo.gbcontest.GbOnlineLine;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;


/**
 * @author dingyoumeng
 * @since 2019/07/22
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class GbLineBiz {

    @Autowired
    private GbCtLineService lineService;
    @Autowired
    private GbCtLineMapper gbCtLineMapper;
    @Autowired
    private GbCtCashflowService ctCashflowService;
    @Autowired
    private GbCtGzProductLineService ctGzProductLineService;
    @Autowired
    private GbCtKcMaterialService ctKcMaterialService;
    @Autowired
    private GbCtGzProductService ctGzProductService;
    @Autowired
    private GbCtKcProductMapper ctKcProductMapper;
    @Autowired
    private GbCtGzProductLineMapper gbCtGzProductLineMapper;
    @Autowired
    private GbCtGzProducingMapper gbCtGzProducingMapper;
    @Autowired
    private GbCtKcMaterialMapper gbCtKcMaterialMapper;
    @Autowired
    private GbCtGzProductMapper gbCtGzProductMapper;
    @Autowired
    private GbCtGzMaterialMapper gbCtGzMaterialMapper;
    @Autowired
    private GbCtWorkerMapper gbCtWorkerMapper;
    @Autowired
    private GbActionBiz gbActionBiz;
    @Autowired
    private GbCtGzCsMapper gbCtGzCsMapper;

    @Autowired
    private GbContestStudentService gbContestStudentService;


    public R add(GbCtLine line) {
        int countLine = gbCtLineMapper.getCountLine(line.getContestId(), line.getStudentId());
        GbCtGzCs gzCs = gbCtGzCsMapper.getByContestId(line.getContestId());
        if(countLine >=gzCs.getMaxLine()){
            return ErrorEnum.LINE_MULTI.getR();
        }

        //获取现金
        Integer cash = ctCashflowService.getCash(line.getStudentId(), line.getContestId());
        //生产线类型编号
        Integer cplid = line.getPlCplid();

        GbCtGzProductLine ctGzProductLine = ctGzProductLineService.getById(cplid);

        GbCtGzProduct ctGzProduct = ctGzProductService.getById(line.getPlCpid());
        int remain = cash - ctGzProductLine.getCplBuyFee();
        if (remain < 0) {
            return ErrorEnum.CASH_NOT_ENOUGH.getR();
        }
        line.setPlInvest(ctGzProductLine.getCplBuyFee());//原值
        Integer addDate = ctGzProductLine.getCplInstallDate();
        Integer finishDate = DateUtils.addYearAndSeasonTime(line.getPlAddTime(), addDate);
        line.setPlFinishDate(finishDate);
        line.setPlRemainDate(addDate);
        if(addDate == 0){
            line.setStatus(GbCtLine.ON_SPACE);
        }else {
            line.setStatus(GbCtLine.ON_BUILD);//状态
        }
        line.setCplName(ctGzProductLine.getCplName());//产线名称
        line.setPlDepTotal(0);
        line.setPlProductAddDate(null);
        line.setPlProductingDate(null);
        line.setPlTransferDate(null);
        line.setPlTransferAddDate(null);
        gbCtLineMapper.insert(line);

        //插入消费记录
        GbCtCashflow ctCashflow = new GbCtCashflow();
        ctCashflow.setCDate(line.getPlAddTime())
                .setCSurplus(remain)
                .setCIn(0)
                .setCOut(ctGzProductLine.getCplBuyFee())
                .setCAction(CashFlowActionEnum.BUY_PRODUCT_LINE.getAction())
                .setContestId(line.getContestId())
                .setStudentId(line.getStudentId())
                .setCComment("新建生产线：" + ctGzProductLine.getCplName() + (ctGzProduct == null ? "" : ctGzProduct.getCpName()));
        ctCashflowService.save(ctCashflow);


        //计算报表,判断是否破产
        gbActionBiz.isBankruptcy(line.getStudentId(),line.getContestId(),line.getPlAddTime());
        return R.success();
    }


    /**
     * 获取所有生产线
     * @param contestId
     * @param studentId
     * @return
     */
    public R getAllLineList(Integer contestId,Integer studentId) {
        List<GbCtLine> allLineList = gbCtLineMapper.getList(contestId, studentId);
        for (GbCtLine gbCtLine : allLineList) {
            //获取所需高级工人和所需普通工人数
             GbCtGzProductLine gbCtGzProductLine = gbCtGzProductLineMapper.selectById(gbCtLine.getPlCplid());
            gbCtLine.setCplSeniorWorker(gbCtGzProductLine.getCplSeniorWorker());
            gbCtLine.setCplOrdinaryWorker(gbCtGzProductLine.getCplOrdinaryWorker());

            List<GbCtWorker> gj = gbCtWorkerMapper.getGjWorkerListByLineId(gbCtLine.getLineId());
            List<Integer> gjId = new ArrayList<>();
            for (GbCtWorker gbCtWorker : gj) {
                gjId.add(gbCtWorker.getId());
            }
            gbCtLine.setSeniorWorkerList(gjId);
            List<GbCtWorker> pt = gbCtWorkerMapper.getPtWorkerListByLineId(gbCtLine.getLineId());
            List<Integer> ptId = new ArrayList<>();
            for (GbCtWorker gbCtWorker : pt) {
                ptId.add(gbCtWorker.getId());
            }
            gbCtLine.setOrdinaryWorkerList(ptId);
        }


        return R.success().put("list",allLineList);
    }


    public R listOnline(Integer userId, Integer contestId, Integer date) {
        List<GbOnlineLine> list = lineService.listOnline(userId, contestId, date);
        Map<String, Object> resultMap = new HashMap<>(16);
        resultMap.put("list", list);
        return R.success(resultMap);
    }





    /***
     * 转产
     * @param userId
     * @param contestId
     * @param lineId
     * @param cpId
     * @param currentTime
     * @return
     */
    public R lineTransfer(Integer userId,Integer contestId,Integer lineId,Integer cpId,Integer currentTime) {


        Integer cash = ctCashflowService.getCash(userId, contestId);
        GbCtLine line = gbCtLineMapper.selectById(lineId);
        if(line.getPlCpid().equals(cpId)){
            return ErrorEnum.TRANSFER_WORING.getR();
        }
        //获取生产线规则
        GbCtGzProductLine gbCtGzProductLine = gbCtGzProductLineMapper.selectById(line.getPlCplid());

        int remain = cash - gbCtGzProductLine.getCplTransferFee();
        if (remain < 0) {
            return ErrorEnum.CASH_NOT_ENOUGH.getR();
        }
        //插入现金交易记录
        GbCtCashflow ctCashflow = new GbCtCashflow();
        GbCtGzProduct ctGzProduct = ctGzProductService.getById(cpId);
        ctCashflow.setStudentId(userId)
                .setContestId(contestId)
                .setCAction(CashFlowActionEnum.PRODUCT_LINE_TRANSFER.getAction())
                .setCComment("生产线" + line.getLineId().toString() + "转产为" + (ctGzProduct == null ? "" : ctGzProduct.getCpName()))
                .setCIn(0)
                .setCOut(gbCtGzProductLine.getCplTransferFee())//转产费用
                .setCDate(currentTime)
                .setCSurplus(remain);
        ctCashflowService.save(ctCashflow);

        Integer finishDate = DateUtils.addYearAndSeasonTime(currentTime, gbCtGzProductLine.getCplTransferDate());
        line.setPlTransferAddDate(currentTime)//转产开始时间
            .setPlCpid(cpId)//转产结束时间
            .setPlTransferDate(finishDate);//转产结束时间
        if(gbCtGzProductLine.getCplTransferDate() == null || gbCtGzProductLine.getCplTransferDate() == 0){
            line.setStatus(GbCtLine.ON_SPACE);
        }else {
            line.setStatus(GbCtLine.ON_TRANSFER);
        }
        gbCtLineMapper.updateById(line);
        return R.success();
    }

    /**
     * 出售
     * @param userId
     * @param line
     * @return
     */
    public R lineSell(Integer userId, GbCtLine line) {
        if (line.getLineId() == null) {
            return R.error(CodeEnum.PARAM_MISS.getCode(), "请选择生产线");
        }
        Integer cash = ctCashflowService.getCash(userId, line.getContestId());
        //获取生产线规则
        GbCtGzProductLine gbCtGzProductLine = gbCtGzProductLineMapper.selectById(line.getPlCplid());
        gbCtWorkerMapper.updateWorkerListByLineIdToSpace(line.getLineId());

        // 插入现金交易记录
        // 流入=原值-累计折旧
        // 流出=原值-累计折旧-残值
        Integer cIn = line.getPlInvest() - line.getPlDepTotal();
        int cOut = line.getPlInvest() - line.getPlDepTotal() - gbCtGzProductLine.getCplDepFee();
        GbCtCashflow ctCashflow = new GbCtCashflow();
        ctCashflow.setStudentId(userId)
                .setContestId(line.getContestId())
                .setCAction(CashFlowActionEnum.SELL_PRODUCT_LINE.getAction())
                .setCOut(cOut)
                .setCIn(cIn)
                .setCComment("变卖生产线" + line.getLineId() + ",增加现金" + line.getPlInvest() + "元,减少现金" + line.getPlDepTotal() + "元")
                .setCDate(line.getActionTime())//当前时间
                .setCSurplus(cash + cIn - cOut);
        ctCashflowService.save(ctCashflow);
        lineService.removeById(line.getLineId());

        //计算报表,判断是否破产
        gbActionBiz.isBankruptcy(userId,line.getContestId(),line.getActionTime());

        return R.success();
    }





    /**
     * 生产产品
     * @param userId
     * @param params
     * @return
     * @throws Exception
     */
    public R producting(Integer userId, GbCtLine params) throws Exception {
        //获取当前学生的操作时间
        Integer studentDate = gbContestStudentService.getStudentDate(params.getContestId(),userId);
        params.setActionTime(studentDate/10);


        Integer cash = ctCashflowService.getCash(userId, params.getContestId());
        GbCtLine line = gbCtLineMapper.selectById(params.getLineId());
        //获取当前产线生产对应的单个产品及所需的材料
        List<GbCtGzProducing> gbCtGzProducings = gbCtGzProducingMapper.getListByCpId(line.getPlCpid());

        //判断库存是否足够生产
        List<GbCtKcMaterialNumVo> kcNums = gbCtKcMaterialMapper.listKcNum(line.getContestId(), line.getStudentId());
        Map<Integer,Integer> kcNumMap = new HashMap<>();
        for (GbCtKcMaterialNumVo kcNum : kcNums) {
            kcNumMap.put(kcNum.getMaterialNum(),kcNum.getNum());
        }
        for (GbCtGzProducing gbCtGzProducing : gbCtGzProducings) {
            int need  = line.getRealProduction() * gbCtGzProducing.getCpNum();
            Integer kc = kcNumMap.get(gbCtGzProducing.getCpMid());
            if(kc==null || kc < need){
                return ErrorEnum.MATERIAL_NOT_ENOUGH.getR();
            }
        }

        //获取生产所需加工费
        int developFee = 0;
        GbCtGzProduct gbCtGzProduct = gbCtGzProductMapper.selectById(params.getPlCpid());
        int fee = gbCtGzProduct.getCpDevelopFee() * line.getRealProduction();
        developFee += developFee + fee;
        int remain = cash - developFee;
        if (remain < 0) {
            return ErrorEnum.CASH_NOT_ENOUGH.getR();
        }

        //获取当前生产线规则信息
        GbCtGzProductLine gzLine = gbCtGzProductLineMapper.selectById(line.getPlCplid());
        
        //获取材料库存信息（按入库时间排序）
        List<GbCtKcMaterial> materials = gbCtKcMaterialMapper.list(line.getContestId(), line.getStudentId());
        
        //材料规则id和编号的键值对
        List<GbCtGzMaterial> gzMaterial = gbCtGzMaterialMapper.list(line.getContestId());
        Map<Integer,Integer> idAndCmIdMap = new HashMap<>();
        for (GbCtGzMaterial gbCtGzMaterial : gzMaterial) {
            idAndCmIdMap.put(gbCtGzMaterial.getId(),gbCtGzMaterial.getCmId());
        }
        
        //把材料库存分类
        Map<Integer,List<GbCtKcMaterial>> kcMaterialMap = new HashMap<>();
        for (GbCtGzProducing gbCtGzProducing : gbCtGzProducings) {
            List<GbCtKcMaterial> list = new ArrayList<>();
            for (GbCtKcMaterial material : materials) {
                Integer imNum = idAndCmIdMap.get(material.getImCmId());
                if(imNum.equals(gbCtGzProducing.getCpMid())){
                    list.add(material);
                }
            }
            kcMaterialMap.put(gbCtGzProducing.getCpMid(),list);
        }

        //生产的产品个数进行循环 进行生产
        for (int i = 0; i < line.getRealProduction(); i++) {
            int cashNeed = 0;
            for (GbCtGzProducing gbCtGzProducing : gbCtGzProducings) {
                int needNum = gbCtGzProducing.getCpNum();
                if(needNum == 0){ continue;}

                List<GbCtKcMaterial> materialList = kcMaterialMap.get(gbCtGzProducing.getCpMid());
                for (GbCtKcMaterial material : materialList) {
                    if(material.getImNum() == 0 ){continue;}
                    if(material.getImNum() - needNum > 0){
                        //计算金额，更新材料剩余数量
                        cashNeed += material.getMaterialPrice() * needNum;
                        material.setImNum(material.getImNum() - needNum);
                        break;
                    }else{
                        //计算金额，删除数据（改为更新为0），结束当前循环
                        cashNeed += material.getMaterialPrice() * material.getImNum();
                        needNum = needNum - material.getImNum();
                        material.setImNum(0);
                    }
                }
            }
            cashNeed += gbCtGzProduct.getCpDevelopFee();//加工费
            //生成库存数据
            GbCtKcProduct gbCtKcProduct = new GbCtKcProduct();
            gbCtKcProduct.setContestId(line.getContestId())
                    .setStudentId(line.getStudentId())
                    .setIpCpId(line.getPlCpid())
                    .setIpNum(1)
                    .setLineId(line.getLineId())
                    .setRealCost(cashNeed);

            Integer produceTime = gzLine.getCplProduceDate();
            //判断生产周期
            if(produceTime !=null && produceTime>0){
                Integer produceFinishTime = DateUtils.addYearAndSeasonTime(params.getActionTime(),produceTime);
                line.setPlProductingDate(produceFinishTime);//生产结束时间
                gbCtKcProduct.setIsInventory(GbCtKcProduct.NOT_IN);// 产品未入库
                gbCtKcProduct.setInventoryDate(produceFinishTime);//入库时间
            }else {
                line.setPlProductingDate(line.getActionTime());//生产结束时间
                gbCtKcProduct.setIsInventory(GbCtKcProduct.IS_IN);// 产品入库
                gbCtKcProduct.setInventoryDate(line.getActionTime());//入库时间
            }
            ctKcProductMapper.insert(gbCtKcProduct);
        }

        //更新所有记录
        kcMaterialMap.forEach((k,v)->ctKcMaterialService.updateBatchById(v));

        //插入现金交易记录
        GbCtCashflow ctCashflow = new GbCtCashflow();
        ctCashflow.setStudentId(userId)
                .setContestId(line.getContestId())
                .setCAction(CashFlowActionEnum.PRODUCT_PRODUCING.getAction())
                .setCOut(developFee)
                .setCIn(0)
                .setCComment("生产线" + line.getLineId().toString()+"生产产品"+gbCtGzProduct.getCpName())
                .setCDate(params.getActionTime())
                .setCSurplus(remain);
        ctCashflowService.save(ctCashflow);

        //更新生产线表
        Integer produceTimeDate = DateUtils.addYearAndSeasonTime(params.getActionTime(),gzLine.getCplProduceDate());

        line.setPlProductAddDate(params.getActionTime());//设置开产时间
        line.setPlProductingDate(produceTimeDate);//设置生产结束时间
        line.setStatus(GbCtLine.ON_PRODUCE);
        lineService.updateById(line);

        //计算报表,判断是否破产
        gbActionBiz.isBankruptcy(userId,line.getContestId(),params.getActionTime());

        return R.success();
    }

    /**
     * 设备规则、线型、产品获取
     * @param studentId
     * @param contestId
     * @return
     */
    public R info(Integer studentId, Integer contestId) {

        // 生产线类型
        List<GbCtGzProductLine> productLineList = ctGzProductLineService.getProductLineTypeList(contestId);
        // 生产产品
        List<GbCtGzProduct> productList = ctGzProductService.getList(contestId);
        Map<String, Object> returnMap = new HashMap<>();
        returnMap.put("productLineList", productLineList);
        returnMap.put("productList", productList);
        return R.success(returnMap);
    }

    /**
     * 产品信息获取
     * @param contestId
     * @return
     */
    public R proInfo(Integer contestId) {
        // 生产产品
        List<GbCtGzProduct> productList = ctGzProductService.getList(contestId);
        Map<String, Object> returnMap = new HashMap<>();
        returnMap.put("productList", productList);
        return R.success(returnMap);
    }


    /**
     * 产线保存工人
     * @param studentId
     * @param param
     * @return
     */
    public R saveWorkerToLine(Integer studentId, GbLineParam param){
        List<Integer> workerIds1 = param.getOrdinaryWorkerList();
        List<Integer> workerIds2 = param.getSeniorWorkerList();
        List<Integer> workerIds = new ArrayList<>();
        workerIds.addAll(workerIds1);
        workerIds.addAll(workerIds2);
        List<GbCtWorker> workers = new ArrayList<>();
        if(workerIds.size() > 0){
            workers = gbCtWorkerMapper.selectBatchIds(workerIds);
            for (GbCtWorker worker : workers) {
                //在其他产线
                if(worker.getLineId()!= null && !Objects.equals(worker.getLineId(), param.getLineId())){
                    return ErrorEnum.WORKER_WORKING.getR();
                }
            }
        }
        //删除原来保存的所有id
        List<GbCtWorker> allWorker = gbCtWorkerMapper.getAllWorkerListByLineId(param.getLineId());
        for (GbCtWorker gbCtWorker : allWorker) {
            gbCtWorkerMapper.update(gbCtWorker,new UpdateWrapper<GbCtWorker>().lambda()
                    .set(GbCtWorker::getLineId,null)
                    .set(GbCtWorker::getIsWork,GbCtWorker.ON_SPACE)
                    .eq(GbCtWorker::getId,gbCtWorker.getId())
            );
        }

        //重新保存id
        for (GbCtWorker worker : workers) {
            worker.setLineId(param.getLineId())
                    .setIsWork(GbCtWorker.ON_WORK);
            gbCtWorkerMapper.updateById(worker);
        }

        GbCtLine gbCtLine = gbCtLineMapper.selectById(param.getLineId());
        gbCtLine.setClassesId(param.getClassesId())//班次规则
                .setRealProduction(param.getRealProduction());//实际产量
        gbCtLineMapper.updateById(gbCtLine);

        return R.success();
    }
}
