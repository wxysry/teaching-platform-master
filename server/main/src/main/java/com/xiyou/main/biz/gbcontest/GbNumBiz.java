package com.xiyou.main.biz.gbcontest;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.xiyou.common.enums.CodeEnum;
import com.xiyou.common.utils.R;
import com.xiyou.main.dao.gbcontest.GbCtCashflowMapper;
import com.xiyou.main.dao.gbcontest.GbCtGzNumMapper;
import com.xiyou.main.dao.gbcontest.GbCtYfNumMapper;
import com.xiyou.main.entity.gbcontest.GbCtCashflow;
import com.xiyou.main.entity.gbcontest.GbCtGzNum;
import com.xiyou.main.entity.gbcontest.GbCtYfNum;
import com.xiyou.main.params.gbcontest.GbNumParam;
import com.xiyou.main.service.gbcontest.GbCtCashflowService;
import com.xiyou.main.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @program: multi-module
 * @description:
 * @author: tangcan
 * @create: 2019-09-01 12:43
 **/
@Service
@Transactional(rollbackFor = Exception.class)
public class GbNumBiz {
    @Autowired
    private GbCtCashflowService ctCashflowService;
    @Autowired
    private GbCtCashflowMapper cashflowMapper;
    @Autowired
    private GbCtGzNumMapper gbCtGzNumMapper;
    @Autowired
    private GbCtYfNumMapper gbCtYfNumMapper;

    @Autowired
    private GbActionBiz gbActionBiz;

    public R list(Integer contestId, Integer studentId) {
        QueryWrapper<GbCtYfNum> qw = new QueryWrapper<>();
        qw.eq("contest_id",contestId);
        qw.eq("student_id",studentId);
        qw.orderByAsc("id");
        List<GbCtYfNum> list = gbCtYfNumMapper.selectList(qw);
        return R.success().put("list", list);
    }



    public R commit(GbNumParam param) {
        if (param.getId() == null) {
            return R.error(CodeEnum.PARAM_ERROR.getCode(), "请选择数字化研发");
        }
        // 现金
        Integer cash = cashflowMapper.getCash(param.getStudentId(), param.getContestId());
        if (cash == null) {
            cash = 0;
        }

        GbCtYfNum gbCtYfNum = gbCtYfNumMapper.selectById(param.getId());
        if (GbCtYfNum.NUM_YF_ING.equals(gbCtYfNum.getState()) || GbCtYfNum.NUM_YF_FINISH.equals(gbCtYfNum.getState())) {
            return R.error(CodeEnum.OTHER_ERROR.getCode(), "无需重复研发");
        }

        GbCtGzNum gbCtGzNum = gbCtGzNumMapper.selectById(gbCtYfNum.getNumId());
        Integer investSum = gbCtGzNum.getConsumeMoney();
        // 当现金<sum（勾选的投资费用总和），则报错【现金不足】
        if (cash < investSum) {
            return R.error(CodeEnum.OTHER_ERROR.getCode(), "现金不足");
        }
        // 刷新cashflow，ID、ISO开拓、0、sum（勾选的投资费用总和），现金+增加-减少、扣减ISO开拓&sum（勾选的投资费用总和）、当前时间
        ctCashflowService.save(new GbCtCashflow()
                .setStudentId(gbCtYfNum.getStudentId())
                .setContestId(gbCtYfNum.getContestId())
                .setCAction("数字化")
                .setCIn(0)
                .setCOut(investSum)
                .setCSurplus(cash - investSum)
                .setCComment("扣减数字化开拓" + investSum + "元")
                .setCDate(param.getDate()));
        // 刷新yf_ISO,勾选记录now_date +1，Remain_Date -1
        Integer ciDevelopDate = gbCtGzNum.getTimeCostQuarter();
        Integer finishDate = DateUtils.addYearAndSeasonTime(param.getDate(),ciDevelopDate);
        if(ciDevelopDate != null && ciDevelopDate>0 ){
            gbCtYfNum.setState(GbCtYfNum.NUM_YF_ING);
        }else {
            gbCtYfNum.setState(GbCtYfNum.NUM_YF_FINISH);
        }
        gbCtYfNum.setStartDate(param.getDate())
                .setFinishDate(finishDate);
        gbCtYfNumMapper.updateById(gbCtYfNum);

        //破产判断,生成报表
        gbActionBiz.isBankruptcy(param.getStudentId(),param.getContestId(),param.getDate());

        return R.success();
    }
}
