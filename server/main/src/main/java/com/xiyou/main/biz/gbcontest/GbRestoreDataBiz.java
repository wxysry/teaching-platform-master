package com.xiyou.main.biz.gbcontest;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.xiyou.common.utils.R;
import com.xiyou.main.dao.gbcontest.GbCtSubjectMapper;
import com.xiyou.main.entity.gbcontest.GbContestStudent;
import com.xiyou.main.enums.ErrorEnum;
import com.xiyou.main.service.gbcontest.GbContestStudentService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @program: multi-module
 * @description:
 * @author wangxingyu
 * @since 2023-06-11
 **/
@Slf4j
@Service
@Transactional(rollbackFor = Exception.class)
public class GbRestoreDataBiz {

    @Autowired
    private GbCtSubjectMapper gbCtSubjectMapper;
    @Autowired
    private GbContestStudentService gbContestStudentService;


//    /**
//     * 判断是否最后一场订货会之前
//     * @param currentTime
//     * @param restoreTime
//     * @return
//     */
//    public boolean checkIsBeforeTradeFair(Integer contestId,Integer currentTime,Integer restoreTime){
//        // 如果这个时间节点在最近一个订货会之前 不允许还原
//        GbContest contest = gbContestService.getById(contestId);
//        return restoreTime < contest.getLastFairOrderDate();
//    }




    /**
     * 统一模式还原所有学生
     * @param contestId
     * @param currentTime
     * @return
     */
    public void restoreAll(Integer contestId,Integer currentTime,Integer restoreTime){
        //查询出所有学生还原
        List<Integer> studentIdList = gbContestStudentService.getStudentIdList(contestId);

        //大部队所在的年份
        int year = currentTime/10;
        List<GbContestStudent> studentList= gbContestStudentService.list(new LambdaQueryWrapper<GbContestStudent>()
                .eq(GbContestStudent::getContestId,contestId));
        List<GbContestStudent> RestoreStudentList = studentList.stream().filter(item->{
            //学生的时间线
            int studentDate = item.getDate()/10;
            //学生最近一次还原的年份
            int lastRestoreYear = item.getLastRestoreTime()==null?0:item.getLastRestoreTime()/10;
            return (studentDate==currentTime)&&(year!=lastRestoreYear);
        }).collect(Collectors.toList());

        //还原学生
        for (GbContestStudent student : RestoreStudentList) {
            restoreDataByTime(contestId,student.getStudentId(),currentTime,restoreTime);

            //清空最后一次还原时间
            gbContestStudentService.update(new GbContestStudent(), new UpdateWrapper<GbContestStudent>()
                    .eq("contest_id", contestId)
                    .eq("student_id", student.getStudentId())
                    .set("last_restore_time",null)
            );
        }
        //删除系统公告数据和重置选单信息
        restoreSystemNoticeAndChooseByTime(contestId,currentTime,restoreTime);

    }


    /**
     * @Author: tangcan
     * @Description: 备份数据
     * @Param: [contestId, studentId]
     * @date: 2019/8/31
     */
    public void backupData(Integer contestId, Integer studentId,Integer currentTime) {
        // 备份数据
        // 1.先删除备份表中之前备份的数据
        gbCtSubjectMapper.removeLikeTableDataByTime(contestId, studentId,currentTime);

        // 2.更新原表的 back_up_date 字段的值
        gbCtSubjectMapper.updateTableBackUpDate(contestId, studentId,currentTime);

        // 3.将主表的数据插入到备份表
        gbCtSubjectMapper.insertMainTableToLikeTableData(contestId, studentId);
    }



    /**
     * 非统一模式还原学生
     * @param contestId
     * @param studentId
     * @param restoreTime
     * @return
     */
    public R restoreOneData(Integer contestId, Integer studentId,Integer restoreTime){
        GbContestStudent studentContest = gbContestStudentService.getOne(new QueryWrapper<GbContestStudent>().
                eq("contest_id", contestId).eq("student_id", studentId));
        int currentTime = studentContest.getDate()/10;
        log.error("【还原单个学生】：contestId:"+contestId+",studentId:"+studentId+",从"+currentTime+"还原到"+restoreTime);
        //将学生还原到对应的时间节点
        restoreDataByTime(contestId,studentId,currentTime,restoreTime);

        //记录该学生最近一次还原到的时间
        gbContestStudentService.update(new GbContestStudent().setLastRestoreTime(restoreTime),
                new QueryWrapper<GbContestStudent>().eq("contest_id", contestId).eq("student_id", studentId));

        return R.success();
    }

    /**
     * 按时间节点还原数据
     * @param contestId
     * @param studentId
     * @param
     * @return
     */
    public R restoreDataByTime(Integer contestId, Integer studentId,Integer currentTime,Integer restoreTime){

        if(restoreTime > currentTime){
            return ErrorEnum.RESTORE_TIME_ERROR.getR();
        }
        // 1.先删除现有数据
        gbCtSubjectMapper.removeMainTableData(contestId, studentId);

        //2.删除这个时间节点的后备份表里的数据
        gbCtSubjectMapper.removeLikeTableDataAfterRestoreTime(contestId,studentId,restoreTime);

        //3.再从备份表里把数据插入到竞赛表
        gbCtSubjectMapper.insertLikeTableDataToMainTable(contestId, studentId,restoreTime);

        return R.success();
    }




    public R restoreSystemNoticeAndChooseByTime(Integer contestId,Integer currentTime,Integer restoreTime){
        if(restoreTime > currentTime){
            return ErrorEnum.RESTORE_TIME_ERROR.getR();
        }
        // 删除系统公告数据和重置选单信息
        gbCtSubjectMapper.removeSystemNoticeAfterRestoreTime(contestId,restoreTime);
        gbCtSubjectMapper.resetChooseAfterRestoreTime(contestId,restoreTime/10,restoreTime%10);
        return R.success();
    }




    /**
     *  重置当季的选单信息
     * @param contestId
     */
    public void resetOrderChoose(Integer contestId,Integer year,Integer quarterly) {
        gbCtSubjectMapper.resetOrderChoose(contestId,year,quarterly);
    }



    /**
     * 将模拟选单和已选订单的备份数据加入模拟选单和已选订单
     * @param contestId
     * @param date
     */
    public void backupOrder(Integer contestId,Integer date) {
        gbCtSubjectMapper.backupOrder(contestId,date);
    }
}
