package com.xiyou.main.biz.gbcontest;

import com.xiyou.common.utils.R;
import com.xiyou.main.dao.gbcontest.*;
import com.xiyou.main.dao.gbcontest.*;
import com.xiyou.main.vo.gbcontest.GbCashFolowEntity;
import com.xiyou.main.vo.gbcontest.GbFinanceReturnEntity;
import com.xiyou.main.vo.gbcontest.GbMarketReturnEntity;
import com.xiyou.main.vo.gbcontest.GbProductMaterialEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.Map;

/**
 * @author wangxingyu
 * @since 2023-06-11
 * @description: 财务信息&综合财务信息&研发认证信息
 **/
@Service
@Transactional(rollbackFor = Exception.class)
public class GbStudentFinanceBiz {

    @Autowired
    GbCtCashflowMapper ctCashflowMapper;
    @Autowired
    GbCtFeeMapper ctFeeMapper;
    @Autowired
    GbCtBankLoanMapper ctBankLoanMapper;
    @Autowired
    GbCtXzOrderMapper ctXzOrderMapper;
    @Autowired
    GbCtYfMarketMapper ctYfMarketMapper;
    @Autowired
    GbCtYfIsoMapper ctYfIsoMapper;
    @Autowired
    GbCtYfProductMapper ctYfProductMapper;
    @Autowired
    GbCtKcProductMapper ctKcProductMapper;
    @Autowired
    GbCtKcMaterialMapper ctKcMaterialMapper;
    @Autowired
    GbCtDdMaterialMapper ctDdMaterialMapper;

    /**
     * @Description: 财务信息&综合财务信息
     * @Author:zhengxiaodong
     * @Param: [cashFolowEntity]
     * @return: com.xiyou.common.utils.R
     */
    public R getStudentFinanceInfo(GbCashFolowEntity cashFolowEntity) {

        Map<String, Object> returnMap = new HashMap<>();

        GbFinanceReturnEntity returnEntity = new GbFinanceReturnEntity();

        // 当前现金
        returnEntity.setCurrentMoney(ctCashflowMapper.getCash(cashFolowEntity.getStudentId(), cashFolowEntity.getContestId()));
        // 应收账款
        returnEntity.setReceivableMoney(ctFeeMapper.getStudentReceivableMoney(cashFolowEntity));
        returnMap.put("receivableMoneyList", ctFeeMapper.getStudentReceivableMoneyList(cashFolowEntity));
        // 长贷总额
        returnEntity.setLongTermLoansMoney(ctBankLoanMapper.getStudentLongAndShortTermLoansMoney(cashFolowEntity, 1));
        returnMap.put("longTermLoansMoneyList", ctBankLoanMapper.getStudentLongAndShortTermLoansMoneyList(cashFolowEntity, 1));
        // 短贷总额
        returnEntity.setShortTermLoanMoney(ctBankLoanMapper.getStudentLongAndShortTermLoansMoney(cashFolowEntity, 2));
        returnMap.put("shortTermLoanMoneyList", ctBankLoanMapper.getStudentLongAndShortTermLoansMoneyList(cashFolowEntity, 2));
        // 贴息
        returnEntity.setDiscountMoney(ctCashflowMapper.getStudentDiscountMoney(cashFolowEntity, "贴现"));
        // 利息      本息同还-利息  每季付息-利息
        returnEntity.setInterest(ctCashflowMapper.getStudentDiscountMoney(cashFolowEntity, "利息"));
        // 销售收入
        returnEntity.setSalesProfit(ctXzOrderMapper.getStudentSalesProfit(cashFolowEntity));

        // 维修费[维修费、转产费、租金、管理费、广告费]
        returnEntity.setRepairCost(ctCashflowMapper.getRepairCostToAdCost(cashFolowEntity, "维修费"));
        // 转产费
        returnEntity.setTransferCost(ctCashflowMapper.getRepairCostToAdCost(cashFolowEntity, "转产"));
        // 管理费
        returnEntity.setManageFee(ctCashflowMapper.getRepairCostToAdCost(cashFolowEntity, "管理费"));

        // 信息费
        returnEntity.setInformationCost(ctCashflowMapper.getRepairCostToAdCost(cashFolowEntity, "信息费"));
        // 广告费
        returnEntity.setAdCost(ctCashflowMapper.getRepairCostToAdCost(cashFolowEntity, "广告费"));


        // 订单成本
        returnEntity.setDirectCost(ctXzOrderMapper.getStudentProductDevCost(cashFolowEntity));

        // ISO认证[ISO认证、直接成本、市场开拓]
        returnEntity.setISOCost(ctCashflowMapper.getStudentISOTomarketDevCost(cashFolowEntity, "ISO开拓"));
        // 产品研发
        returnEntity.setProductDevCost(ctCashflowMapper.getStudentISOTomarketDevCost(cashFolowEntity, "产品研发"));
        // 市场开拓
        returnEntity.setMarketDevCost(ctCashflowMapper.getStudentISOTomarketDevCost(cashFolowEntity, "市场开拓"));
        //数字化研发
        returnEntity.setDigitalDevCost(ctCashflowMapper.getStudentISOTomarketDevCost(cashFolowEntity,"数字化"));

        returnMap.put("data", returnEntity);
        return R.success(returnMap);
    }

    /**
     * @Description: 研发认证信息
     * @Author:zhengxiaodong
     * @Param: []
     * @return: com.xiyou.common.utils.R
     */
    public R getResearchAndDevelopeInfo(Integer studentId, Integer contestId) {

        GbMarketReturnEntity returnEntity = new GbMarketReturnEntity();

        // 市场准入
        returnEntity.setMarket(ctYfMarketMapper.getList(contestId,studentId));
        // 生产资格
        returnEntity.setProduct(ctYfProductMapper.getList(contestId,studentId));
        // ISO认证
        returnEntity.setIso(ctYfIsoMapper.getList(contestId,studentId));

        return R.success().put("data", returnEntity);
    }

    /**
     * @Description: 库存采购信息
     * @Author:zhengxiaodong
     * @Param: []
     * @return: com.xiyou.common.utils.R
     */
    public R stockPurchaseInfo(Integer studentId, Integer contestId,Integer currentTime) {

        GbProductMaterialEntity materialEntity = new GbProductMaterialEntity();

        materialEntity.setKP(ctKcProductMapper.getKCProductNumb(studentId, contestId));


        materialEntity.setKR(ctKcMaterialMapper.getKcMaterial(studentId, contestId));


        materialEntity.setBR(ctDdMaterialMapper.getddMaterialBlackCar(studentId, contestId,currentTime));


        materialEntity.setGR(ctDdMaterialMapper.getddMaterialGreyCar(studentId, contestId,currentTime));


        return R.success().put("data", materialEntity);
    }


}
