package com.xiyou.main.biz.gbcontest;


import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.xiyou.common.enums.CodeEnum;
import com.xiyou.common.utils.R;
import com.xiyou.main.dao.gbcontest.*;
import com.xiyou.main.entity.gbcontest.*;
import com.xiyou.main.enums.ErrorEnum;
import com.xiyou.main.params.gbcontest.GbCtMnChooseParam;
import com.xiyou.main.service.gbcontest.GbContestService;
import com.xiyou.main.service.gbcontest.GbCtMnChooseService;
import com.xiyou.main.vo.gbcontest.GbCtMnAdVo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import java.util.*;
import java.util.stream.Collectors;

/**
 * @program: multi-module
 * @description:
 * @author: tangcan
 * @create: 2019-07-27 16:36
 **/
@Service
@Slf4j
public class GbTradeFairBiz {
    @Autowired
    private GbCtMnAdMapper gbCtMnAdMapper;
    @Autowired
    private GbCtMnChooseMapper gbCtMnChooseMapper;
    @Autowired
    private GbCtMnChooseService gbCtMnChooseService;
    @Autowired
    private GbCtXzOrderMapper gbCtXzOrderMapper;
    @Autowired
    private GbContestService contestService;
    @Autowired
    private GbContestStudentMapper gbContestStudentMapper;
    @Autowired
    private GbTradeFairBizTranscation tradeFairBizTranscation;




    /**
     * 开启投放广告
     * @return
     */
    public R startAd(Integer contestId,Integer date){
        log.error("订货会-开启投放广告,contestId:{},date:{}",contestId,date);
        GbContest contest = contestService.getById(contestId);
        Integer sysFairOrderState = contest.getFairOrderState();
        if(sysFairOrderState != 0){
            return ErrorEnum.FAIR_ORDER_STATE_ERROR.getR();
        }else {
            contest.setFairOrderState(1);
            contest.setLastFairOrderDate(date);
            contestService.updateById(contest);
        };
        log.error("订货会-开启投放广告结束,contestId:{},date:{}",contestId,date);
        return R.success();
    }

    /**
     * 进入查看广告阶段
     * @return
     */
    public R showAd(Integer contestId){
        log.error("订货会-进入查看广告阶段,contestId:{},date:{}",contestId);
        GbContest contest = contestService.getById(contestId);
        Integer sysFairOrderState = contest.getFairOrderState();
        if(sysFairOrderState != 1){
            return ErrorEnum.FAIR_ORDER_STATE_ERROR.getR();
        }else {
            contest.setFairOrderState(2);
            contestService.updateById(contest);
        };
        log.error("订货会-进入查看广告阶段结束,contestId:{},date:{}",contestId);
        return R.success();
    }

    /**
     * 开启订货会
     * @param contestId
     */
    public R startTrade(Integer contestId,Integer date,Integer fairOrderState){
        switch (fairOrderState){
            case 1:
                return startAd(contestId,date);
            case 2:
                //初始化
                return showAd(contestId);
            case 3:
                return startChoose(contestId,date);
            default:
                return ErrorEnum.FAIR_ORDER_STATE_ERROR.getR();
        }
    }


    public  R pauseOrContinue(Integer contestId ,Integer fairOrderState){
        //继续
        if(fairOrderState == 3){
            return continueChoose(contestId,fairOrderState);
        }
        //暂停
        if(fairOrderState == 4){
            return pauseChoose(contestId,fairOrderState);
        }
        return ErrorEnum.FAIR_ORDER_STATE_ERROR.getR();
    }


    /**
     * 暂停选单
     * @param contestId
     * @return
     */
    public synchronized  R pauseChoose(Integer contestId,Integer fairOrderState){
        tradeFairBizTranscation.pauseChoose(contestId,fairOrderState);
        return R.success();
    }

    /**
     * 继续选单
     * @param contestId
     * @return
     */
    public synchronized R continueChoose(Integer contestId,Integer fairOrderState){
        tradeFairBizTranscation.continueChoose(contestId,fairOrderState);
        return R.success();
    }


    /**
     * 开始选单
     * @param
     */
    public synchronized R startChoose(Integer contestId,Integer date){
        //开始选单
        tradeFairBizTranscation.startChoose(contestId,date);
        return R.success();
    }


    /**
     * 选单
     * @param
     * @return
     */
    public synchronized  R  chooseOrder(Integer contestId,Integer studentId,String market,Integer chooseId) {
        tradeFairBizTranscation.chooseOrder(contestId,studentId,market,chooseId);
        return R.success();
    }


    /**
     * 放弃选单
     * @param contestId
     * @param studentId
     * @param market
     * @param isTimeOut
     * @return
     */
    public synchronized R  giveUpOrder(Integer contestId,Integer studentId,String market,boolean isTimeOut) {
        tradeFairBizTranscation.giveUpOrder(contestId,studentId,market,isTimeOut);
        return R.success();
    }



    /**
     * 还原订单
     * @param contestId
     * @param date
     * @return
     */
    public synchronized R restoreOrder(Integer contestId, Integer date) {
        tradeFairBizTranscation.restoreOrder(contestId,date);
        return R.success();
    }



    /**
     * 学生获取广告信息和选单列表
     * @param
     * @return
     */
    public R getUserAndOrder(Integer contestId,Integer studentId,String market) {
        return tradeFairBizTranscation.getUserAndOrder(contestId,studentId,market);
    }



    /**
     * 临时添加订单
     * @param
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    public R addOrder(Integer contestId,Integer studentId,Integer chooseId) {
        GbCtMnChoose ctMnChoose = gbCtMnChooseService.getById(chooseId);
        if (ctMnChoose == null || !ctMnChoose.getChooseId().equals(chooseId)) {
            return R.error(CodeEnum.PARAM_ERROR.getCode(), "该选单不存在");
        }
        GbCtXzOrder xzOrder = gbCtXzOrderMapper.selectOne(
                new QueryWrapper<GbCtXzOrder>().eq("contest_id",contestId)
                        .eq("student_id",studentId)
                        .eq("co_id",ctMnChoose.getCoId())
        );
        if(xzOrder != null){
            return R.error(CodeEnum.PARAM_ERROR.getCode(), "该选单学生已经选过");
        }
        GbCtXzOrder ctXzOrder = new GbCtXzOrder()
                .setCoId(ctMnChoose.getCoId())
                .setStudentId(studentId)
                .setContestId(contestId)
                .setDate(ctMnChoose.getDate())
                .setQuarterly(ctMnChoose.getQuarterly())
                .setCmId(ctMnChoose.getCmId())
                .setCpId(ctMnChoose.getCpId())
                .setNum(ctMnChoose.getNum())
                .setTotalPrice(ctMnChoose.getTotalPrice())
                .setDeliveryDate(ctMnChoose.getDeliveryDate())
                .setPaymentDate(ctMnChoose.getPaymentDay())
                .setIsoId(ctMnChoose.getCiId())
                .setCommitDate(0);
        gbCtXzOrderMapper.insert(ctXzOrder);
        return R.success();
    }



    /**
     * 获取选单
     * @param
     * @param
     * @return
     */
    public R getMnOrder(GbCtMnChooseParam gbCtMnChooseParam){
        for (Integer date : gbCtMnChooseParam.getDates()) {
            List<GbCtMnChooseParam.yearSeason> list = new ArrayList<>();
            GbCtMnChooseParam.yearSeason yearSeason = new GbCtMnChooseParam.yearSeason();
            yearSeason.setYear(date/10);
            yearSeason.setQuarterly(date%10);
            list.add(yearSeason);
            gbCtMnChooseParam.setYearSeasons(list);
        }
        Page<GbCtMnChoose> page = new Page<>(gbCtMnChooseParam.getPage(), gbCtMnChooseParam.getLimit());
        gbCtMnChooseMapper.getPage(page, gbCtMnChooseParam);
        Map<String, Object> returnMap = new HashMap<>();
        returnMap.put("total", page.getTotal());
        returnMap.put("list", page.getRecords());
        return R.success(returnMap);
    }




    /**
     * 获取当季所有广告
     * @param contestId
     * @param date
     * @return
     */
    public R adList(Integer contestId, Integer date){
        List<GbCtMnAdVo> adList = gbCtMnAdMapper.getAdListAndAccountByContestId(contestId, date / 10, date % 10);
        return R.success().put("list",adList);
    }



    /**
     * 获取当季订单
     * @param contestId
     * @param date
     * @return
     */
    public R getChooseOrder(Integer contestId, Integer date) {
        List<GbCtMnChoose> ctMnChooseList = gbCtMnChooseMapper.getOrderBySeason(contestId,date/10,date%10);
        return R.success().put("list",ctMnChooseList);
    }


    /**
     * 获取可选订单得到时间
     * @param contestId
     * @return
     */
    public R getOrderDate(Integer contestId) {
          //先获取所有的可选订单
          List<GbCtMnChoose> gbCtMnChooseList= gbCtMnChooseMapper.getOrderDate(contestId);
          //过滤掉小于或等于最后一次选单时间的
          GbContest gbContest= contestService.getOne(new LambdaQueryWrapper<GbContest>().eq(GbContest::getContestId,contestId));
          int lastFairOrderDate = 10;
          if(!StringUtils.isEmpty(gbContest.getLastFairOrderDate())){
              lastFairOrderDate = gbContest.getLastFairOrderDate();
          }
          //获取时间线最大的人的时间
          GbContestStudent maxGbContestStudent = gbContestStudentMapper.selectOne(new LambdaQueryWrapper<GbContestStudent>()
                  .eq(GbContestStudent::getContestId,contestId)
                  .orderByDesc(GbContestStudent::getDate)
                  .last("limit 1")
          );

        int finalLastFairOrderDate = lastFairOrderDate;
        gbCtMnChooseList = gbCtMnChooseList.stream().filter(item->(item.getDate()*10 + item.getQuarterly())> finalLastFairOrderDate).collect(Collectors.toList());
          if(maxGbContestStudent!=null){
              int maxDate = maxGbContestStudent.getDate()/10;
              gbCtMnChooseList.stream().forEach(
                      item->{
                          if((item.getDate()*10 + item.getQuarterly())!=maxDate){
                              item.setDisabled(true);
                          }
                      }
              );
          }
          return R.success().put("list",gbCtMnChooseList);
    }
}
