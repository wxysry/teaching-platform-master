package com.xiyou.main.biz.gbcontest;

import com.xiyou.common.utils.R;
import com.xiyou.main.entity.gbcontest.GbCtCashflow;
import com.xiyou.main.entity.gbcontest.GbCtProfitChart;
import com.xiyou.main.service.gbcontest.GbCtCashflowService;
import com.xiyou.main.service.gbcontest.GbCtLineService;
import com.xiyou.main.service.gbcontest.GbCtProfitChartService;
import com.xiyou.main.vo.gbcontest.GbCashFolowEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @program: multi-module
 * @description:
 * @author: tangcan
 * @create: 2019-07-22 16:08
 **/
@Service
@Transactional(rollbackFor = Exception.class)
public class GbYearEndBiz {


    @Autowired
    private GbActionBiz gbActionBiz;



    /**
     * 确认当年结束
     */
    public R confirm(Integer studentId, Integer contestId, Integer date) {
        return gbActionBiz.endingSeason(studentId,contestId,date);
    }


    /**
     * 当年结束后->继续下个季度
     * @param cashFolowEntity
     * @return
     */
    public R currentQuarterNext(GbCashFolowEntity cashFolowEntity) {
        Integer studentId = cashFolowEntity.getStudentId();
        Integer contestId = cashFolowEntity.getContestId();
        Integer currentTime = cashFolowEntity.getCurrentTime();
        gbActionBiz.nextSeasonAction(studentId,contestId,currentTime);
        return R.success();
    }
}
