package com.xiyou.main.biz.newcontest;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.xiyou.common.enums.CodeEnum;
import com.xiyou.common.utils.R;
import com.xiyou.main.dao.newcontest.*;
import com.xiyou.main.entity.contest.CtProfitChart;
import com.xiyou.main.entity.gbcontest.GbCtGzDiscount;
import com.xiyou.main.entity.gbcontest.GbCtProfitChart;
import com.xiyou.main.entity.newcontest.*;
import com.xiyou.main.enums.CashFlowActionEnum;
import com.xiyou.main.enums.ErrorEnum;
import com.xiyou.main.params.newcontest.*;
import com.xiyou.main.service.newcontest.*;
import com.xiyou.main.utils.DateUtils;
import com.xiyou.main.vo.newcontest.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @author dingyoumeng
 * @since 2019/07/25
 */
@Service
@Transactional
public class NewActionBiz {
    @Autowired
    private NewCtGzMaterialMapper newCtGzMaterialMapper;
    @Autowired
    private NewCtFeeService ctFeeService;
    @Autowired
    NewCtCashflowService ctCashflowService;
    @Autowired
    NewCtXzOrderService ctXzOrderService;
    @Autowired
    NewCtKcProductService ctKcProductService;
    @Autowired
    NewCtGzProductService ctGzProductService;
    @Autowired
    NewCtYfProductService ctYfProductService;
    @Autowired
    NewCtGzCsService ctGzCsService;
    @Autowired
    NewCtKcMaterialService ctKcMaterialService;
    @Autowired
    NewCtGzMaterialService ctGzMaterialService;
    @Autowired
    NewCtYfMarketService ctYfMarketService;
    @Autowired
    NewCtXzOrderMapper ctXzOrderMapper;
    @Autowired
    NewCtFeeMapper ctFeeMapper;
    @Autowired
    private NewCtGzProductMapper ctGzProductMapper;
    @Autowired
    private NewCtGzIsoMapper ctGzIsoMapper;
    @Autowired
    private NewRestoreDataBiz restoreDataBiz;
    @Autowired
    private NewCtGzMarketMapper ctGzMarketMapper;
    @Autowired
    private NewCtYfProductMapper ctYfProductMapper;
    @Autowired
    private NewCtCorporateMapper ctCorporateMapper;
    @Autowired
    private NewApplyForLoanBiz applyForLoanBiz;
    @Autowired
    private NewCtWorkerMapper ctWorkerMapper;
    @Autowired
    private NewCtLineMapper ctLineMapper;
    @Autowired
    private NewCtYfMarketMapper ctYfMarketMapper;
    @Autowired
    private NewCtYfIsoMapper ctYfIsoMapper;
    @Autowired
    private NewCtYfNumMapper ctYfNumMapper;
    @Autowired
    private NewCtGzDiscountMapper newCtGzDiscountMapper;
    @Autowired
    private NewCtLineService newCtLineService;
    @Autowired
    private NewCtChargesService ctChargesService;
    @Autowired
    private NewCtProfitChartService ctProfitChartService;
    @Autowired
    private NewCtBalanceService ctBalanceService;
    @Autowired
    private NewCtFinancialTargetService newCtFinancialTargetService;
    @Autowired
    private NewCtBankLoanService ctBankLoanService;
    @Autowired
    private NewCtCorporateMapper newCtCorporateMapper;
    @Autowired
    private NewCtDdMaterialMapper newCtDdMaterialMapper;
    @Autowired
    private NewApplyForLoanBiz newApplyForLoanBiz;
    @Autowired
    private NewCtKcMaterialMapper newCtKcMaterialMapper;
    @Autowired
    private NewCtKcProductMapper ctKcProductMapper;
    @Autowired
    private NewContestStudentService newContestStudentService;



    public R checkReceivables(Integer userId, Integer contestId) {
        QueryWrapper<NewCtFee> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("student_id", userId);
        queryWrapper.eq("contest_id", contestId);
        queryWrapper.eq("r_remain_date", 1);
        NewCtFee ctFee = ctFeeService.getOne(queryWrapper);
        Map<String, Object> resultMap = new HashMap<>(16);
        resultMap.put("fee", ctFee);
        return R.success(resultMap);
    }

    /**
     * 应收款确认
     * @param userId
     * @param contestId
     * @param feeId
     * @param currentTime
     * @return
     */
    @Transactional
    public R confirmReceivables(Integer userId, Integer contestId,Integer feeId,Integer currentTime) {
        NewCtFee ctFee = ctFeeService.getById(feeId);
        if (ctFee == null) {
            return R.success();
        }
        Integer cash = ctCashflowService.getCash(userId, contestId);
        int remain = cash + ctFee.getRFee();
        //插入消费记录
        NewCtCashflow ctCashflow = new NewCtCashflow();
        ctCashflow.setCDate(currentTime)
                .setCSurplus(remain)
                .setCIn(ctFee.getRFee())
                .setCOut(0)
                .setCAction(CashFlowActionEnum.UPDATE_RECEIVABLE.getAction())
                .setContestId(contestId)
                .setStudentId(userId)
                .setCComment("更新应收款" + ctFee.getRFee() + "元");
        ctCashflowService.save(ctCashflow);
        ctFeeService.removeById(feeId);

        //计算四张报表
        Integer bsTotalEquity = calBsTotalEquity(userId,contestId,currentTime);
        // 所有者权益为负后，自动结束比赛
        if (bsTotalEquity < 0) {
            return R.error(7000, "当前所有者权益为负，你已破产，模拟竞赛结束");
        }
        return R.success();
    }

    /**
     * 应付款确认
     * @param userId
     * @param contestId
     * @param feeId
     * @param currentTime
     * @return
     */
    @Transactional
    public R confirmPay(Integer userId, Integer contestId,Integer feeId,Integer currentTime) {
        NewCtFee ctFee = ctFeeService.getById(feeId);
        if (ctFee == null) {
            return R.success();
        }
        Integer cash = ctCashflowService.getCash(userId, contestId);
        int remain = cash - ctFee.getRFee();
        if(remain < 0){
            return ErrorEnum.CASH_NOT_ENOUGH.getR();
        }
        //插入消费记录
        NewCtCashflow ctCashflow = new NewCtCashflow();
        ctCashflow.setCDate(currentTime)
                .setCSurplus(remain)
                .setCIn(0)
                .setCOut(ctFee.getRFee())
                .setCAction(CashFlowActionEnum.UPDATE_PAY.getAction())
                .setContestId(contestId)
                .setStudentId(userId)
                .setCComment("更新应付款" + ctFee.getRFee() + "W");
        ctCashflowService.save(ctCashflow);
        ctFeeService.removeById(feeId);
        return R.success();
    }

    /**
     * 查看所有订单
     * @param userId
     * @param contestId
     * @param date
     * @return
     */
    public R listOrder(Integer userId, Integer contestId, Integer date) {
        Map<String, Object> queryMap = new HashMap<>();
        queryMap.put("studentId", userId);
        queryMap.put("contestId", contestId);
//        queryMap.put("year", date / 10);
        //交单时间为空
//        queryMap.put("commitDate", 0);
        List<NewXzOrderVo> list = ctXzOrderService.listDeliveryOrder(queryMap);

        List<NewCtGzIso> ctGzIsoList = ctGzIsoMapper.list(contestId);
        Map<Integer, String> isoNameMap = ctGzIsoList.stream().collect(Collectors.toMap(NewCtGzIso::getCiId, NewCtGzIso::getCiName, (k1, k2) -> k1));
        list.forEach(p -> {
            //设置ISO
            if (p.getIsoId() == null) {
                p.setCiName("-");
            } else if (p.getIsoId() == 3) {
                p.setCiName(isoNameMap.get(1) + " / " + isoNameMap.get(2));
            } else {
                p.setCiName(isoNameMap.get(p.getIsoId()));
            }
            //设置状态  违约/已交货/未交货

            String status = "";
            if (p.getCommitDate() != null && p.getCommitDate() > 0) {
                status = "已交货";
            } else if ((date / 10 == p.getDate() && date % 10 > p.getDeliveryDate()) || ((date / 10) > p.getDate())) {
                status = "已违约";
            }
            else{
                status = "可交货";
            }
            p.setStatus(status);
        });

        Map<String, Object> resultMap = new HashMap<>(16);
        resultMap.put("list", list);
        return R.success(resultMap);
    }

    /***
     * 交货
     * @param userId
     * @param contestId
     * @param orderId
     * @param date
     * @return
     */
    @Transactional
    public R deliveryOrder(Integer userId, Integer contestId, Integer orderId, Integer date) {

        NewCtXzOrder order = ctXzOrderService.getById(orderId);//获取订单
        if (order == null) {
            return R.error(CodeEnum.BUSINESS_WRONG.getCode(), "无此订单");
        }
        NewCtGzProduct ctGzProduct = ctGzProductMapper.get(contestId, order.getCpId());//获取规则
        if (ctGzProduct == null) {
            return R.error(CodeEnum.BUSINESS_WRONG.getCode(), "无此产品");
        }
        List<NewCtKcProduct> listKc = ctKcProductMapper.getAllKcProductAsc(contestId, userId,ctGzProduct.getId());//ASC产品库存
        int kCNum = listKc.size();
        if (kCNum < order.getNum()) {
            return R.error(CodeEnum.BUSINESS_WRONG.getCode(), "库存不足");
        }


        //计算成本 删除产品的库存
        int cost = 0;//成本
        int submitNum = order.getNum();
        for (int i = 0; i < listKc.size(); i++) {
            if(i < submitNum){
                NewCtKcProduct newCtKcProduct = listKc.get(i);
                int realCost = newCtKcProduct.getRealCost();
                cost = cost + realCost;
                ctKcProductMapper.deleteById(newCtKcProduct);
            }
        }
        order.setCommitDate(date).setTotalCost(cost);
        ctXzOrderService.updateById(order);

        int paymentDate = order.getPaymentDate();//账期
        //产生应收账单 此处不生成现金流量表 现金流量表在季末生成
        int payDate = DateUtils.addYearAndSeasonTime(date,paymentDate);//收款日期
        NewCtFee ctFee = new NewCtFee();
        ctFee.setStudentId(userId)
                .setContestId(contestId)
                .setBorrower("经销商")
                .setLender("1")
                .setRFee(order.getTotalPrice())
                .setRemarks("订单收款")
                .setRRemainDate(paymentDate)
                .setPaymentDate(payDate)
                .setType(NewCtFee.TO_RECEIVABLE);
        ctFeeService.save(ctFee);


        //计算四张报表
        Integer bsTotalEquity = calBsTotalEquity(userId,contestId,date);
        // 所有者权益为负后，自动结束比赛
        if (bsTotalEquity < 0) {
            return R.error(7000, "当前所有者权益为负，你已破产，模拟竞赛结束");
        }
        return R.success();
    }

    /**
     * 研发产品列表
     * @param userId
     * @param contestId
     * @return
     */
    public R yfProductList(Integer userId, Integer contestId) {
        List<NewYfProductVO> list = ctGzProductService.listYfProduct(userId, contestId);
        Map<String, Object> resultMap = new HashMap<>(16);
        resultMap.put("list", list);
        return R.success(resultMap);
    }

    /**
     * 研发产品
     * @param userId
     * @param yfParam
     * @return
     */
    @Transactional
    public R yfProduct(Integer userId, NewYfParam yfParam) {
        Integer cpId = yfParam.getCpId();
        NewCtGzProduct newCtGzProduct = ctGzProductService.getById(cpId);
        int depFeeSum = newCtGzProduct.getCpProcessingFee();
        int cash = ctCashflowService.getCash(userId, yfParam.getContestId());
        int remain = cash - depFeeSum;
        if (remain < 0) {
            return ErrorEnum.CASH_NOT_ENOUGH.getR();
        }
        NewCtCashflow ctCashflow = new NewCtCashflow();
        ctCashflow.setCDate(yfParam.getDate())
                .setCSurplus(remain)
                .setCIn(0)
                .setCOut(depFeeSum)
                .setCAction(CashFlowActionEnum.DEVELOP_PRODUCT.getAction())
                .setContestId(yfParam.getContestId())
                .setStudentId(userId)
                .setCComment("扣减产品"+newCtGzProduct.getCpName()+"研发" + depFeeSum + "元");
        ctCashflowService.save(ctCashflow);

        QueryWrapper<NewCtYfProduct> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("student_id", userId);
        queryWrapper.eq("contest_id", yfParam.getContestId());
        queryWrapper.eq("dp_cp_id", newCtGzProduct.getId());
        NewCtYfProduct p = ctYfProductService.getOne(queryWrapper);
        if (NewCtYfProduct.CP_YF_ING.equals(p.getDpState()) || NewCtYfProduct.CP_YF_FINISH.equals(p.getDpState())) {
            return R.error(CodeEnum.OTHER_ERROR.getCode(), "无需重复研发");
        }
        Integer remainDate = newCtGzProduct.getCpDevelopDate();
        Integer finishDate = DateUtils.addYearAndSeasonTime(yfParam.getDate(), remainDate);
        if (remainDate != null && remainDate > 0) {
            p.setDpStartDate(yfParam.getDate());
            p.setDpFinishDate(finishDate);
            p.setDpState(NewCtYfProduct.CP_YF_ING);
        } else {        //研发周期为0的情况下，直接研发成功
            p.setDpStartDate(yfParam.getDate());//申请时间
            p.setDpFinishDate(yfParam.getDate());
            p.setDpState(NewCtYfProduct.CP_YF_FINISH);
        }
        ctYfProductService.updateById(p);

        //计算四张报表
        Integer bsTotalEquity = calBsTotalEquity(userId,yfParam.getContestId(),yfParam.getDate());
        // 所有者权益为负后，自动结束比赛
        if (bsTotalEquity < 0) {
            return R.error(7000, "当前所有者权益为负，你已破产，模拟竞赛结束");
        }

        return R.success();
    }

    /**
     * 减分数
     */
    public void minusScoreAndLog(Integer studentId,Integer contestId,String action){
        int score = ctCorporateMapper.getScore(studentId, contestId);
        score = score -1;
        NewCtCorporate ctCorporate = new NewCtCorporate();
        ctCorporate.setStudentId(studentId)
                .setContestId(contestId)
                .setScore(score)
                .setAction(action);
        ctCorporateMapper.insert(ctCorporate);
    }
    /**
     * 季末操作
     * @param studentId
     * @param contestId
     * @param date
     */
    public void seasonEndAction(Integer studentId, Integer contestId, Integer date){
        //1.自动缴纳未缴管理费 ，扣社会责任
        //2.自动缴纳未缴纳贷款本金利息 ，扣社会责任
        //3.自动缴纳应付款未付，扣社会责任
        //4.自动订单违约扣款，扣社会责任
        //规则
        NewCtGzCs ctGzCs = ctGzCsService.getByContestId(contestId);

        //费用管理-缴纳未缴纳的管理费用,并且扣社会责任得分
        NewRePaymentInfo overhaul = applyForLoanBiz.getOverhaul(contestId, studentId, date);
        if(overhaul != null){
            applyForLoanBiz.payOverhaul(contestId,studentId ,date);
            minusScoreAndLog(studentId,contestId,"未缴管理费");
        }

        //费用管理-缴纳利息,并且扣社会责任得分  2.费用管理-缴纳本金,并且扣社会责任得分
        List<NewRePaymentInfo> bankLoan = applyForLoanBiz.getBankLoan(date, contestId, studentId);
        //获取现金
        for (NewRePaymentInfo info : bankLoan) {
            Integer cash = ctCashflowService.getCash(studentId, contestId);
            applyForLoanBiz.payCurrSeasonAddCashFlow(contestId,studentId,date,info.getAmount().intValue(),info.getPayment(),info.getBlAddTime(),cash);
            //扣社会责任分
            minusScoreAndLog(studentId,contestId,"贷款利息或本金未偿还");
        }

        //应收货强制收货并产生账单和扣分
        List<NewCtDdMaterial> materialOrderRec = newCtDdMaterialMapper.getMaterialOrderRec(studentId, contestId, date);
        for (NewCtDdMaterial dd : materialOrderRec) {
            newApplyForLoanBiz.materialToInventory(dd.getId(), date);
            //扣社会责任分
            minusScoreAndLog(studentId,contestId,"未收货强制收货");
        }

        //应付款强制付款，并且扣社会责任得分
        QueryWrapper<NewCtFee> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("student_id", studentId);
        queryWrapper.eq("contest_id", contestId);
        queryWrapper.eq("type", NewCtFee.TO_PAYMENT);
        queryWrapper.le("payment_date",date);
        queryWrapper.orderByAsc("payment_date");
        List<NewCtFee> list = ctFeeMapper.selectList(queryWrapper);
        for (NewCtFee f : list) {
            Integer cash = ctCashflowService.getCash(studentId, contestId);
            //应付款付款
            paymentAndCashFlow(f,studentId,contestId,date,cash);
            //扣社会责任分
            minusScoreAndLog(studentId,contestId,"未及时付款");
        }


        // 订单交货 -违约罚款并且扣社会责任得分
        //订单年份等于今年  当前季度 = 交货期， 且交货时间为空,则视为违约，需计算违约金
        List<NewCtXzOrder> orderList = ctXzOrderService.list(new NewCtXzOrder().setStudentId(studentId)
                .setContestId(contestId).setDate(date / 10).setCommitDate(0).setDeliveryDate(date % 10));

        for (NewCtXzOrder order : orderList) {
            //扣社会责任分
            minusScoreAndLog(studentId,contestId,"未按期交货");
        }
        // 违约金比例
        double punishRate = (ctGzCs == null ? 0.0 : (double) ctGzCs.getPunish() / 100.0);
        int punish = orderList.stream().mapToInt(p -> (int) Math.round(p.getTotalPrice()*punishRate)).sum();
        if(punish>0){
            Integer surplus = ctCashflowService.getCash(studentId, contestId);
            // Cashflow新一条记录ID、违约金、0、sum(四舍五入【xz_order.总价（当交单时间为空、年份=当前年份）*gz_cs.违约金比例】】-、现金+增加-减少、支付违约金、当前时间
            ctCashflowService.save(new NewCtCashflow().setStudentId(studentId)
                    .setContestId(contestId)
                    .setCAction("违约金")
                    .setCIn(0)
                    .setCOut(punish)
                    .setCSurplus(surplus - punish)
                    .setCComment("支付违约金" + punish + "元")
                    .setCDate(date));

        }
    }


    public void updateStatus(Integer studentId, Integer contestId, Integer finishDate){
        //下个季节的时间
        // 更新工人状态，上个季度招聘-下个季度正式入职
        ctWorkerMapper.updateWorkerToOnboarding(contestId,studentId,DateUtils.addYearAndSeasonTime(finishDate,-1));
        // 更新生产线 建成状态  建造中-> 空闲
        ctLineMapper.updateLineStatusBuildToSpace(contestId,studentId,finishDate,NewCtLine.ON_BUILD,NewCtLine.ON_SPACE);
        // 更新生产线 转产状态  转产中-> 空闲
        ctLineMapper.updateLineStatusTransferToSpace(contestId,studentId,finishDate,NewCtLine.ON_TRANSFER,NewCtLine.ON_SPACE);
        // 更新生产线 生产状态  生产中->空闲 ，产品数量++
        ctLineMapper.updateLineStatusProduceToSpace(contestId,studentId,finishDate,NewCtLine.ON_PRODUCE,NewCtLine.ON_SPACE);
        //修改产品入库状态
        ctKcProductMapper.updateProductToFinish(contestId,studentId,finishDate);
        // 更新产品研发状态
        ctYfProductMapper.updateStateToFinish(studentId,contestId,finishDate);
        // 更新市场研发状态
        ctYfMarketMapper.updateStateToFinish(studentId,contestId,finishDate);
        // 更新ISO研发状态
        ctYfIsoMapper.updateStateToFinish(studentId,contestId,finishDate);
        // 更新数字化研发状态
        ctYfNumMapper.updateStateToFinish(studentId,contestId,finishDate);
    }


    /**
     * 扣除维修费
     * @param studentId
     * @param contestId
     * @param date
     */
    public void payMaintenanceFee(Integer studentId, Integer contestId, Integer date){
        Integer surplus = ctCashflowService.getCash(studentId, contestId);
        int maintenanceFee = newCtLineService.getMaintenanceFee(studentId,contestId,date);
        // cashflow新增一条记录ID、维护费、0、sum[(当前line中PL_Remain_Date为null)*生产线所对应的维护费】、现金+增加-减少、支付维护费、当前时间
        if(maintenanceFee >0) {
            ctCashflowService.save(new NewCtCashflow().setStudentId(studentId)
                    .setContestId(contestId)
                    .setCAction("维修费")
                    .setCIn(0)
                    .setCOut(maintenanceFee)
                    .setCSurplus(surplus - maintenanceFee)
                    .setCComment("支付维修费" + maintenanceFee + "元")
                    .setCDate(date));
        }
    }

    @Transactional
    public R endingSeason(Integer studentId, Integer contestId, Integer date) {
        //1.自动缴纳未缴管理费 ，扣社会责任
        //2.自动缴纳未缴纳贷款本金利息 ，扣社会责任
        //3.自动缴纳应付款未付，扣社会责任
        //4.自动订单违约扣款，扣社会责任
        seasonEndAction(studentId,contestId, date);


        // 获取所有人权益
        Integer bsTotalEquity1 = calBsTotalEquity(studentId,contestId,date);
        // 获取现金流量表余额
        Integer cash1 = ctCashflowService.getCash(studentId, contestId);

        // 所有者权益为负后，自动结束比赛
        if (bsTotalEquity1 < 0) {
            return R.error(7000, "当前所有者权益为负，你已破产，模拟竞赛结束");
        }
        if (cash1 < 0) {
            return R.error(7000, "现金余额为负，你已破产，模拟竞赛结束");
        }

        //获取下个季度的时间
        Integer nextSeason = DateUtils.addYearAndSeasonTime(date,1);


        //下季度当季开始
        nextSeasonAction(studentId,contestId,nextSeason);


        // 获取所有人权益
        Integer bsTotalEquity2 = calBsTotalEquity(studentId,contestId,nextSeason);
        // 获取现金流量表余额
        Integer cash2 = ctCashflowService.getCash(studentId, contestId);

        // 所有者权益为负后，自动结束比赛
        if (bsTotalEquity2 < 0) {
            return R.error(7000, "当前所有者权益为负，你已破产，模拟竞赛结束");
        }
        if (cash2 < 0) {
            return R.error(7000, "现金余额为负，你已破产，模拟竞赛结束");
        }


        //  更新学生表
        //  progress: this.buttonVisible,
        //  contestId: this.contestId,
        //  date: this.year * 100 + this.season * 10 + this.status
        NewContestStudent newContestStudent = new NewContestStudent();
        newContestStudent.setContestId(contestId);
        newContestStudent.setStudentId(studentId);
        newContestStudent.setDate(nextSeason*10+1);
        newContestStudent.setProgress("{\"fillReportForm\":true,\"adLaunch\":true,\"attendOrderMeeting\":false}");
        newContestStudentService.update(newContestStudent,
                new LambdaQueryWrapper<NewContestStudent>()
                .eq(NewContestStudent::getContestId,contestId)
                .eq(NewContestStudent::getStudentId,studentId)
        );

        //备份当季数据
        restoreDataBiz.backupSeasonData(contestId, studentId);

        return R.success();
    }




    /**
     * 当季开始时进行的动作  扣税（每年第一季），维修费，折旧计提，更新产品线状态
     * @param studentId
     * @param contestId
     * @param nextSeason
     */
    public void nextSeasonAction(Integer studentId, Integer contestId, Integer nextSeason){

        int year = nextSeason/10;
        int season = nextSeason%10;
        //判断当季属于哪个季度，每年一季度要扣税
        if((year!=1)&&(season==1)){
            //支付所得税
            // 上一年报表计算的所得税
            List<NewCtProfitChart> profitChartList = ctProfitChartService.list(new NewCtProfitChart().setStudentId(studentId).setContestId(contestId).setPcDate((nextSeason / 10) - 1));
            int lastYearTax = profitChartList.stream().mapToInt(NewCtProfitChart::getPcTax).sum();

            int cash = ctCashflowService.getCash(studentId, contestId);
            ctCashflowService.save(new NewCtCashflow().setStudentId(studentId)
                    .setContestId(contestId)
                    .setCAction("支付所得税")
                    .setCIn(0)
                    .setCOut(lastYearTax)
                    .setCSurplus(cash - lastYearTax)
                    .setCComment("支付所得税" + lastYearTax + "元")
                    .setCDate(nextSeason));
        }


        //维修费
        payMaintenanceFee(studentId,contestId,nextSeason);

        //折旧计提
        newCtLineService.updateDepFee(studentId, contestId, nextSeason);


        //折旧费计入统计报表
        //1. 先生成四张报表，用于保存折旧费，后面会被覆盖
        calBsTotalEquity(studentId,contestId,nextSeason);
        // 计算本季度的折旧费  + 本年前几个季度的折旧费合计 ，将折旧费存到 profit表的折旧费字段。
        //2.先获取本季度新产生的折旧费
        Integer thisSeasonDep = newCtLineService.getDepTotal(studentId, contestId, nextSeason);
        //3.获取本年上几个季度的折旧费
        //获取本年已计算的折旧费
        NewCtProfitChart newCtProfitChartTemp= ctProfitChartService.getOne(new QueryWrapper<NewCtProfitChart>()
                .eq("student_id",studentId)
                .eq("contest_id",contestId)
                .eq("pc_date",nextSeason/10)
                .eq("bs_isxt",1)//系统自动生成
        );
        //4  更新折旧费字段的值
        newCtProfitChartTemp.setPcDep(newCtProfitChartTemp.getPcDep()+thisSeasonDep);
        ctProfitChartService.updateById(newCtProfitChartTemp);



        //更新产线/产品/研发(产品、市场、数字化)状态
        updateStatus(studentId,contestId,nextSeason);
    }



    /**
     * 生成报表
     * @param studentId
     * @param contestId
     * @param date
     * @return
     */
    public Integer calBsTotalEquity(Integer studentId, Integer contestId, Integer date){
        if((date/10)==0){
            throw  new RuntimeException("数据异常");
        }

        //获取本年已计算的折旧费
        NewCtProfitChart  newCtProfitChartTemp= ctProfitChartService.getOne(new QueryWrapper<NewCtProfitChart>()
                .eq("student_id",studentId)
                .eq("contest_id",contestId)
                .eq("pc_date",date/10)
                .eq("bs_isxt",1)//系统自动生成
        );
        Integer depTotal = 0;
        if(newCtProfitChartTemp!=null){
            depTotal = newCtProfitChartTemp.getPcDep();
        }



        // gz_cs的管理费
        NewCtGzCs ctGzCs = ctGzCsService.getByContestId(contestId);
        //先删除本年的这四张报表
        ctChargesService.remove(new QueryWrapper<NewCtCharges>()
                .eq("student_id",studentId)
                .eq("contest_id",contestId)
                .eq("c_date",date/10)
        );
        ctProfitChartService.remove(new QueryWrapper<NewCtProfitChart>()
                .eq("student_id",studentId)
                .eq("contest_id",contestId)
                .eq("pc_date",date/10)
        );
        ctBalanceService.remove(new QueryWrapper<NewCtBalance>()
                .eq("student_id",studentId)
                .eq("contest_id",contestId)
                .eq("bs_year",date/10)
        );
        newCtFinancialTargetService.remove(new QueryWrapper<NewCtFinancialTarget>()
                .eq("student_id",studentId)
                .eq("contest_id",contestId)
                .eq("ft_year",date/10)
        );

        //重新生成四张报表
        // 费用表charges。当年结束后，自动插入一条记录到charges
        NewCtCharges charges = new NewCtCharges().setStudentId(studentId).setContestId(contestId).setCDate(date / 10);
        //生成费用表数据并计算合计值
        ctChargesService.addBySys(charges);
        ctChargesService.updateTotal(charges.getChargesId());



        /*
        添加记录到利润表profit_chart
         */
        NewCtProfitChart profitChart = new NewCtProfitChart().setStudentId(studentId).setContestId(contestId).setBsIsxt(1).setPcDate(date / 10).setIsSubmit("Y");
        // 销售额 = Sum(已选定单xz_order.总价) 交单时间=本年
        // 成本 =  Sum已选定单xz_order.数量*对应产品成本)交单时间=本年
        Map<String, BigDecimal> salesAndDirectCostMap = ctXzOrderService.getSalesAndDirectCost(studentId, contestId, date / 10);
        BigDecimal sales = salesAndDirectCostMap.get("sales");
        BigDecimal directCost = salesAndDirectCostMap.get("directCost");
        profitChart.setPcSales(sales.intValue());
        profitChart.setPcDirectCost(directCost.intValue());

        // 毛利=销售额-成本
        profitChart.setPcGoodsProfit(profitChart.getPcSales() - profitChart.getPcDirectCost());
        // 管理费用=Charges今年系统填写的总和数据
        profitChart.setPcTotal(ctChargesService.getTotal(studentId, contestId, date / 10, 1));
        // 折旧前利润=毛利-管理费用
        profitChart.setPcProfitBeforeDep(profitChart.getPcGoodsProfit() - profitChart.getPcTotal());

        //计算折旧费
        profitChart.setPcDep(depTotal);
        // 财务费用前利润=折旧前利润-折旧
        profitChart.setPcProfitBeforeInterests(profitChart.getPcProfitBeforeDep() - profitChart.getPcDep());


        // 财务费用=Sum(cashflow.流出)当操作类型 in （"本息同还-利息", "每季付息-利息", "贴现"）
        List<String> actionList = Arrays.asList("本息同还-利息", "每季付息-利息", "贴现");
        List<NewCtCashflow> cashflowList = ctCashflowService.list(studentId, contestId, date / 10, actionList);
        profitChart.setPcFinanceFee(cashflowList.stream().mapToInt(NewCtCashflow::getCOut).sum());

        //营业外收支:  出售库存  出售原材料， 变卖生产线， 违约金
        List<NewCtCashflow> yywCashflowList = ctCashflowService.list(studentId, contestId, date / 10, Arrays.asList("出售库存", "出售原材料", "变卖生产线","违约金"));
        profitChart.setPcNonOperating(yywCashflowList.stream().mapToInt(x->0-x.getCOut()).sum());
        // 税前利润=财务费用前利润-财务费用
        profitChart.setPcProfitBeforeTax(profitChart.getPcProfitBeforeInterests() - profitChart.getPcFinanceFee() + profitChart.getPcNonOperating()) ;
        // 税前利润
        int profitBeforeTax = profitChart.getPcProfitBeforeTax();

        // 所得税: 当【税前利润+上一年资产负债表所有者权益>max(之前年份所有者权益）】= round{【税前利润+上一年所有者权益-max[最近一个所得税>0的年份的权益，第0年权益]】*所得税率/100}
        // 所得税率比例
        double incomeTaxRate = ctGzCs == null ? 0.0 : (double) ctGzCs.getIncomeTax() / 100.0;
        // 上一年资产负债表所有者权益
        NewCtBalance lastYearBalance = ctBalanceService.get(new NewCtBalance().setStudentId(studentId).setContestId(contestId).setBsYear(date / 10 - 1));
        int lastYearTotalEquity = lastYearBalance == null ? 0 : lastYearBalance.getBsTotalEquity();

        // max(之前年份所有者权益）
        int maxTotalEquityPreYear = ctBalanceService.maxTotalEquityPreYear(studentId, contestId, date / 10);

        if ((profitBeforeTax + lastYearTotalEquity) > maxTotalEquityPreYear) {
            // 第0年权益
            NewCtBalance zeroYearBalance = ctBalanceService.get(new NewCtBalance().setStudentId(studentId).setContestId(contestId).setBsYear(0));
            int zeroYearTotalEquity = zeroYearBalance == null ? 0 : zeroYearBalance.getBsTotalEquity();

            // 最近一个所得税>0的年份的权益
            Integer equity = ctBalanceService.getRecentYEarHaveEquity(studentId, contestId, date / 10);
            int recentYEarHaveEquity = (equity == null ? 0 : equity);

            int tax = (int) Math.round(incomeTaxRate * (double) (profitBeforeTax + lastYearTotalEquity - Math.max(recentYEarHaveEquity, zeroYearTotalEquity)));
            profitChart.setPcTax(tax);
        } else {
            profitChart.setPcTax(0);
        }
        profitChart.setPcAnnualNetProfit(profitBeforeTax - profitChart.getPcTax());
        ctProfitChartService.save(profitChart);

        //剩余现金
        Integer cashRest = ctCashflowService.getCash(studentId, contestId);
        /*
        资产负债表
         */
        NewCtBalance balance = new NewCtBalance()
                .setStudentId(studentId)
                .setContestId(contestId)
                .setBsIsxt(1)
                .setBsYear(date / 10)
                .setIsSubmit("Y");
        // 现金 = Cashflow最新现金
        balance.setBsCash(cashRest);


        // 应收款 = Fee表合计
        int fee = ctFeeService.getFeeSum(new NewCtFee().setStudentId(studentId).setContestId(contestId).setType(NewCtFee.TO_RECEIVABLE));
        balance.setBsReceivable(fee);


        // 在制品 = Line表剩余生产时间不为空的sum(生产产品所对应成本）
        int productInProcess = newCtLineService.getProductInProcess(new NewCtLine().setStudentId(studentId).setContestId(contestId));
        balance.setBsProductInProcess(productInProcess);


        // 产成品 = Kc_product表sum(库存产品-所对应成本)
        int product = ctKcProductService.getProductSum(studentId, contestId);
        balance.setBsProduct(product);

        // 原材料=Kc_material表sum（库存数*所对应成本）
        int material = ctKcMaterialService.getMaterialSum(studentId, contestId);
        balance.setBsMaterial(material);

        // 流动资产合计 = 现金+应收款+在制品+产成品+原材料
        balance.setBsTotalCurrentAsset(balance.getBsCash() + balance.getBsReceivable() + balance.getBsProductInProcess() + balance.getBsProduct() + balance.getBsMaterial());


        // 生产线=Line表PL_Finish_Date不为空的sum（原值-折旧）  已建成的生产线总价
        List<NewCtLine> lineList = newCtLineService.list(studentId, contestId);
        balance.setBsEquipment(lineList.stream().filter(p ->  !"在建".equals(p.getStatus())).mapToInt(p -> p.getPlInvest() - p.getPlDepTotal()).sum());

        // 在建
        balance.setBsProjectOnConstruction(lineList.stream().filter(p -> "在建".equals(p.getStatus())).mapToInt(NewCtLine::getPlInvest).sum());

        // 固定资产合计
        balance.setBsTotalFixedAsset(balance.getBsEquipment() + balance.getBsProjectOnConstruction());

        // 资产合计
        balance.setBsTotalAsset(balance.getBsTotalCurrentAsset() + balance.getBsTotalFixedAsset());


        // 长期贷款:Bank_loan表类型为1的金额合计
        // 短期贷款:Bank_loan表类型为2的金额合计
        List<NewCtBankLoan> bankLoanList = ctBankLoanService.list(studentId, contestId);
        balance.setBsLongLoan(bankLoanList.stream().filter(p -> p.getBlType() == 1).mapToInt(NewCtBankLoan::getBlFee).sum());

        balance.setBsShortLoan(bankLoanList.stream().filter(p -> p.getBlType() == 2).mapToInt(NewCtBankLoan::getBlFee).sum());

        //原材料的应付款也计入短期负债
        QueryWrapper<NewCtFee> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("student_id", studentId);
        queryWrapper.eq("contest_id", contestId);
        queryWrapper.eq("type", NewCtFee.TO_PAYMENT);
        List<NewCtFee> list = ctFeeMapper.selectList(queryWrapper);
        Integer marketShouldPay = list.stream().mapToInt(NewCtFee::getRFee).sum();
        balance.setBsShortLoan(balance.getBsShortLoan()+marketShouldPay);


        // 应交税费 = 利润表中的PC_TAX
        NewCtProfitChart ctProfitChart = ctProfitChartService.getSys(new NewCtProfitChart().setStudentId(studentId).setContestId(contestId).setPcDate(date / 10));
        balance.setBsTax(ctProfitChart.getPcTax());

        //其他应付款   0 工人工资
        balance.setBsOtherPay(0);

        // 负债合计 = 长期贷款+短期贷款+应交税费
        balance.setBsTotalLiability(balance.getBsLongLoan() + balance.getBsShortLoan() + balance.getBsTax());

        // 股东资本 = 第0年股东资本
        NewCtBalance ctBalance = ctBalanceService.get(new NewCtBalance().setStudentId(studentId).setContestId(contestId).setBsYear(0));
        balance.setBsEquity(ctBalance == null ? 0 : ctBalance.getBsEquity());

        // 利润留存 = 上一年所有者权益-股东资本
        balance.setBsRetainedEarning(lastYearTotalEquity - balance.getBsEquity());

        // 年度净利 = 利润表当年净利润
        balance.setBsAnnualNetProfit(ctProfitChart == null ? 0 : ctProfitChart.getPcAnnualNetProfit());

        // 所有者权益 = 股东资本+利润留存+年度净利
        balance.setBsTotalEquity(balance.getBsEquity() + balance.getBsRetainedEarning() + balance.getBsAnnualNetProfit());

        // 负债所有者权益合计 = 所有者权益+负债合计
        balance.setBsTotal(balance.getBsTotalEquity() + balance.getBsTotalLiability());

        ctBalanceService.save(balance);

        //财务指标
        NewCtFinancialTarget newCtFinancialTarget = new NewCtFinancialTarget();
        newCtFinancialTarget.setStudentId(studentId)
                .setContestId(contestId)
                .setFtYear(date / 10)
                .setFtIsxt(1).setIsSubmit("Y");


        //本年短期贷款+其他应付款+应交税金
        BigDecimal sum1 = new BigDecimal(balance.getBsShortLoan()+balance.getBsOtherPay()+balance.getBsTax());

        if(sum1.compareTo(BigDecimal.ZERO) != 0){
            //流动比率 :本年流动资产合计/（本年短期贷款+其他应付款+应交税金）
            BigDecimal ftCurrentRate =  new BigDecimal(balance.getBsTotalCurrentAsset())
                    .divide(sum1,2, BigDecimal.ROUND_HALF_UP);
            newCtFinancialTarget.setFtCurrentRate(ftCurrentRate);
            //速动比率: (本年现金+本年应收款)/（本年短期贷款+其他应付款+应交税金）
            BigDecimal ftQuickRate =  new BigDecimal(balance.getBsCash()+balance.getBsReceivable())
                    .divide(new BigDecimal(balance.getBsShortLoan()+balance.getBsOtherPay()+balance.getBsTax()),2, BigDecimal.ROUND_HALF_UP);
            newCtFinancialTarget.setFtQuickRate(ftQuickRate);
        }else{
            //流动比率 :本年流动资产合计/（本年短期贷款+其他应付款+应交税金）
            newCtFinancialTarget.setFtCurrentRate(BigDecimal.ZERO);
            //速动比率: (本年现金+本年应收款)/（本年短期贷款+其他应付款+应交税金）
            newCtFinancialTarget.setFtQuickRate(BigDecimal.ZERO);
        }


        //资产负债率：本年负债合计/本年资产合计
        if(BigDecimal.ZERO.compareTo(new BigDecimal(balance.getBsTotalAsset())) != 0){
            BigDecimal ftDebtRate = new BigDecimal(balance.getBsTotalLiability())
                    .divide(new BigDecimal(balance.getBsTotalAsset()),2, BigDecimal.ROUND_HALF_UP);
            newCtFinancialTarget.setFtDebtRate(ftDebtRate);
        }else{
            newCtFinancialTarget.setFtDebtRate(BigDecimal.ZERO);
        }


        //产权比率： 本年负债合计/本年所有者权益
        BigDecimal equity = new BigDecimal(balance.getBsTotalEquity());
        if(equity.compareTo(BigDecimal.ZERO)!=0){
            BigDecimal ftEquityRate = new BigDecimal(balance.getBsTotalLiability())
                    .divide(equity,2, BigDecimal.ROUND_HALF_UP);
            newCtFinancialTarget.setFtEquityRate(ftEquityRate);
        }else{
            newCtFinancialTarget.setFtEquityRate(BigDecimal.ZERO);
        }



        //营业净利润率: 本年折旧前利润/本年销售收入
        if(new BigDecimal(ctProfitChart.getPcSales()).compareTo(BigDecimal.ZERO)!=0){
            BigDecimal ftNetProfitRate = new BigDecimal(ctProfitChart.getPcProfitBeforeDep())
                    .divide(new BigDecimal(ctProfitChart.getPcSales()),2, BigDecimal.ROUND_HALF_UP);
            newCtFinancialTarget.setFtNetProfitRate(ftNetProfitRate);
        }else{
            newCtFinancialTarget.setFtNetProfitRate(BigDecimal.ZERO);
        }

        //成本费用率: （本年营业外收支+本年折旧前利润）/(本年直接成本+管理费用+财务费用)
         BigDecimal costExpense = new BigDecimal(ctProfitChart.getPcDirectCost()+ctProfitChart.getPcTotal()+ctProfitChart.getPcFinanceFee());
        if(costExpense.compareTo(BigDecimal.ZERO)!=0){
            BigDecimal ftCostExpenseRate = new BigDecimal(ctProfitChart.getPcNonOperating()+ctProfitChart.getPcProfitBeforeDep())
                    .divide(costExpense,2, BigDecimal.ROUND_HALF_UP);
            newCtFinancialTarget.setFtCostExpenseRate(ftCostExpenseRate);
        }else{
            newCtFinancialTarget.setFtCostExpenseRate(BigDecimal.ZERO);
        }


        Integer lxqlr = ctProfitChart.getPcProfitBeforeInterests();
        BigDecimal zczj=new BigDecimal(balance.getBsTotalAsset()+(lastYearBalance!=null?lastYearBalance.getBsTotalAsset():0));
        if(BigDecimal.ZERO.compareTo(zczj)!=0){
            BigDecimal ftReturnAssetsRate = new BigDecimal(2)
                    .multiply(new BigDecimal(lxqlr))
                    .divide( zczj,2, BigDecimal.ROUND_HALF_UP);
            newCtFinancialTarget.setFtReturnAssetsRate(ftReturnAssetsRate);
        }else{
            newCtFinancialTarget.setFtReturnAssetsRate(BigDecimal.ZERO);
        }


        //净资产收益率: 2*净利润/（去年所有者权益+今年所有者权益）
        BigDecimal returnEquity = new BigDecimal(balance.getBsTotalEquity()+(lastYearBalance!=null?lastYearBalance.getBsTotalEquity():0));
        if(returnEquity.compareTo(BigDecimal.ZERO)!=0){
            BigDecimal ftReturnEquityRate =  new BigDecimal(2)
                    .multiply(new BigDecimal(balance.getBsAnnualNetProfit()))
                    .divide(returnEquity,2, BigDecimal.ROUND_HALF_UP);
            newCtFinancialTarget.setFtReturnEquityRate(ftReturnEquityRate);
        }else{
            newCtFinancialTarget.setFtReturnEquityRate(BigDecimal.ZERO);
        }


        //营业收入增长率:  今年销售收入/去年销售收入 -1
        //去年利润表
        NewCtProfitChart lastYearProfitChar = ctProfitChartService.getSys(new NewCtProfitChart().setStudentId(studentId).setContestId(contestId).setPcDate((date/10) - 1));
        if(lastYearProfitChar!=null && new BigDecimal(lastYearProfitChar.getPcSales()).compareTo(BigDecimal.ZERO)!=0){
            BigDecimal ftRevenueGrowthRate = new BigDecimal(ctProfitChart.getPcSales())
                    .divide(new BigDecimal(lastYearProfitChar.getPcSales()),100, BigDecimal.ROUND_HALF_UP)
                    .subtract(new BigDecimal(1))
                    .setScale(2, BigDecimal.ROUND_HALF_UP);
            newCtFinancialTarget.setFtRevenueGrowthRate(ftRevenueGrowthRate);
        }else{
            newCtFinancialTarget.setFtRevenueGrowthRate(BigDecimal.ZERO);
        }

        //资本保值增值率:  今年所有者权益/去年所有者权益
        BigDecimal lastBsTotalEquity = new BigDecimal(lastYearBalance!=null?lastYearBalance.getBsTotalEquity():0);
        if(lastBsTotalEquity.compareTo(BigDecimal.ZERO)!=0){
            BigDecimal ftAppreciationRate = new BigDecimal(balance.getBsTotalEquity())
                    .divide(lastBsTotalEquity,2, BigDecimal.ROUND_HALF_UP);
            newCtFinancialTarget.setFtAppreciationRate(ftAppreciationRate);
        }else{
            newCtFinancialTarget.setFtAppreciationRate(BigDecimal.ZERO);
        }


        //总资产增长率:   今年资产总计/去年资产总计 -1
        BigDecimal lastBsTotalAsset= new BigDecimal(lastYearBalance!=null?lastYearBalance.getBsTotalAsset():0);
        if(lastBsTotalAsset.compareTo(BigDecimal.ZERO)!=0){
            BigDecimal ftTotalGrowthRate = new BigDecimal(balance.getBsTotalAsset())
                    .divide(lastBsTotalAsset,100, BigDecimal.ROUND_HALF_UP)
                    .subtract(new BigDecimal(1))
                    .setScale(2, BigDecimal.ROUND_HALF_UP);
            newCtFinancialTarget.setFtTotalGrowthRate(ftTotalGrowthRate);
        }else{
            newCtFinancialTarget.setFtTotalGrowthRate(BigDecimal.ZERO);
        }



        //库存周转率      2*今年直接成本/（去年原材料+去年在制品+去年库存+今年原材料+今年在制品+今年库存）
        BigDecimal inventorySum;
        if(lastYearBalance!=null){
            inventorySum = new BigDecimal(balance.getBsProductInProcess()+balance.getBsProduct()+balance.getBsMaterial()
                    +lastYearBalance.getBsProductInProcess()+lastYearBalance.getBsProduct()+lastYearBalance.getBsMaterial());
        }else{
            inventorySum = new BigDecimal(balance.getBsProductInProcess()+balance.getBsProduct()+balance.getBsMaterial());
        }
        if(inventorySum.compareTo(BigDecimal.ZERO)!=0){
            BigDecimal ftInventoryRate = new BigDecimal(2)
                    .multiply(new BigDecimal(ctProfitChart.getPcDirectCost()))
                    .divide(inventorySum,2, BigDecimal.ROUND_HALF_UP);
            newCtFinancialTarget.setFtInventoryRate(ftInventoryRate);
        }else{
            newCtFinancialTarget.setFtInventoryRate(BigDecimal.ZERO);

        }

        //库存周转天数    365/库存周转率 (先四舍五入两位小数)
        if(newCtFinancialTarget.getFtInventoryRate().compareTo(BigDecimal.ZERO)!=0){
            BigDecimal ftInventoryDays = new BigDecimal(365)
                    .divide(newCtFinancialTarget.getFtInventoryRate()
                            ,2, BigDecimal.ROUND_HALF_UP);
            newCtFinancialTarget.setFtInventoryDays(ftInventoryDays);
        }else{
            newCtFinancialTarget.setFtInventoryDays(BigDecimal.ZERO);

        }


        //应收账款周转率  2 * 今年销售收入/（去年应收账款+今年应收账款）
        BigDecimal receivable = new BigDecimal(lastYearBalance!=null?lastYearBalance.getBsReceivable():0)
                .add(new BigDecimal(balance.getBsReceivable()));
        if(receivable.compareTo(BigDecimal.ZERO)!=0){
            BigDecimal ftReceivableRate = new BigDecimal(2)
                    .multiply(new BigDecimal(ctProfitChart.getPcSales()))
                    .divide(receivable,2, BigDecimal.ROUND_HALF_UP);
            newCtFinancialTarget.setFtReceivableRate(ftReceivableRate);

        }else{
            newCtFinancialTarget.setFtReceivableRate(BigDecimal.ZERO);

        }

        //应收账款周转天数 365/应收账款周转率
        if(newCtFinancialTarget.getFtReceivableRate().compareTo(BigDecimal.ZERO)!=0){
            BigDecimal ftReceivableDays = new BigDecimal(365)
                    .divide(newCtFinancialTarget.getFtReceivableRate()
                            ,2, BigDecimal.ROUND_HALF_UP);
            newCtFinancialTarget.setFtReceivableDays(ftReceivableDays);
        }else{
            newCtFinancialTarget.setFtReceivableDays(BigDecimal.ZERO);
        }


        //现金周转期     存货周转天数+应收账款周转天数
        BigDecimal ftCashPeriod = newCtFinancialTarget.getFtInventoryDays()
                .add(newCtFinancialTarget.getFtReceivableDays())
                .setScale(2, BigDecimal.ROUND_HALF_UP);;
        newCtFinancialTarget.setFtCashPeriod(ftCashPeriod);

        newCtFinancialTargetService.save(newCtFinancialTarget);
        return balance.getBsTotalEquity();
    }

    /**
     * 显示账单（应付、应收）
     * @param userId
     * @param contestId
     * @param type
     * @return
     */
    public R showFee(Integer userId, Integer contestId,Integer type,Integer currentTime) {
        QueryWrapper<NewCtFee> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("student_id", userId);
        queryWrapper.eq("contest_id", contestId);
        queryWrapper.eq("type", type);
        queryWrapper.orderByAsc("r_remain_date");
        List<NewCtFee> list = ctFeeMapper.selectList(queryWrapper);


        //计算贴现利率
        list.forEach(item->{
            //贴现率
            //计算剩余时间
            Integer remainDate = DateUtils.calSeasonInterval(currentTime,item.getPaymentDate());
            if(remainDate>0){
                List<NewCtGzDiscount> newCtGzDiscounts = newCtGzDiscountMapper.getGzDiscountByStudentIdAndContestId(contestId,remainDate);
                if( newCtGzDiscounts!=null && newCtGzDiscounts.size()>0){
                    NewCtGzDiscount gbCtGzDiscount = newCtGzDiscounts.get(0);
                    item.setRate(gbCtGzDiscount.getDiscountInterest());
                }else{
                    item.setRate(new BigDecimal(0));
                }
            }else{
                item.setRate(new BigDecimal(0));
            }
        });

        Map<String, Object> resultMap = new HashMap<>(16);
        resultMap.put("list", list);
        return R.success(resultMap);
    }

    /**
     * 贴现
     * @param param
     * @param userId
     * @return
     */
    @Transactional
    public R discount(NewDiscountParam param, Integer userId) {

        NewCtFee f = ctFeeService.getById(param.getId());
        if(param.getFee() > f.getRFee()){
            return ErrorEnum.DISCOUNT_ERROR.getR();
        }
        Integer cash = ctCashflowService.getCash(userId, param.getContestId());
        //计算剩余时间
        Integer remainDate = DateUtils.calSeasonInterval(param.getDate(),f.getPaymentDate());
        List<NewCtGzDiscount> newCtGzDiscounts = newCtGzDiscountMapper.getGzDiscountByStudentIdAndContestId(param.getContestId(),remainDate);
        if( newCtGzDiscounts!=null && newCtGzDiscounts.size()>0){
            NewCtGzDiscount newCtGzDiscount = newCtGzDiscounts.get(0);
            //贴息
            int  out = new BigDecimal(param.getFee()).multiply(newCtGzDiscount.getDiscountInterest()).divide(new BigDecimal(100))
                    .setScale(0, RoundingMode.CEILING)
                    .intValue();
            int remain = cash + param.getFee() - out;
            //现金流量表
            NewCtCashflow ctCashflow = new NewCtCashflow();
            ctCashflow.setCDate(param.getDate())
                    .setCSurplus(remain)
                    .setCIn(param.getFee())
                    .setCOut(out)
                    .setCAction(CashFlowActionEnum.DISCOUNT.getAction())
                    .setContestId(param.getContestId())
                    .setStudentId(userId)
                    .setCComment("应收日期:"+(f.getPaymentDate()/10)+"年"+(f.getPaymentDate()%10)+"季"+",贴现"+param.getFee()+"元");
            ctCashflowService.save(ctCashflow);
            //更新应收款数据
            int remianAmount = f.getRFee() - param.getFee();
            if(remianAmount>0){
                f.setRFee(f.getRFee() - param.getFee());
                ctFeeService.updateById(f);
            }else{
                ctFeeService.removeById(f);
            }
        }

        //计算四张报表
        Integer bsTotalEquity = calBsTotalEquity(userId,param.getContestId(),param.getDate());
        // 所有者权益为负后，自动结束比赛
        if (bsTotalEquity < 0) {
            return R.error(7000, "当前所有者权益为负，你已破产，模拟竞赛结束");
        }
        return R.success();
    }

    /**
     * 付款
     * @return
     */
    @Transactional
    public R payment(Integer userId,Integer contestId,Integer feeId,Integer currentTime){
        NewCtFee f = ctFeeService.getById(feeId);
        int cash = ctCashflowService.getCash(userId, contestId);
        if(cash<f.getRFee()){
            return ErrorEnum.CASH_NOT_ENOUGH.getR();
        }
        paymentAndCashFlow(f,userId,contestId,currentTime,cash);

        //计算四张报表
        Integer bsTotalEquity = calBsTotalEquity(userId,contestId,currentTime);
        // 所有者权益为负后，自动结束比赛
        if (bsTotalEquity < 0) {
            return R.error(7000, "当前所有者权益为负，你已破产，模拟竞赛结束");
        }
        return R.success();
    }


    //应付款并产生现金流量表
    public void paymentAndCashFlow(NewCtFee f ,Integer userId,Integer contestId ,Integer currentTime,Integer cash){
        cash = cash - f.getRFee();
        //现金流量表
        NewCtCashflow ctCashflow = new NewCtCashflow();
        ctCashflow.setCDate(currentTime)
                .setCSurplus(cash)
                .setCIn(0)
                .setCOut(f.getRFee())
                .setCAction(CashFlowActionEnum.PAY_MATERIAL.getAction())
                .setContestId(contestId)
                .setStudentId(userId)
                .setCComment(f.getRemarks()+f.getRFee()+"元");
        ctCashflowService.save(ctCashflow);
        //删除应付款数据
        ctFeeService.removeById(f.getFeeId());
    }



    /**
     * 紧急采购原材料
     * @param userId
     * @param param
     * @return
     */
    @Transactional
    public R buyMaterial(Integer userId, NewUrgentBuyParam param) {
        //判断现金是否足够
        int cash = ctCashflowService.getCash(userId, param.getContestId());
        // 紧急采购成本
        int cost = (param.getUrgentPrice() == null ? 0:param.getUrgentPrice()) * (param.getNum()==null? 0:param.getNum());
        if (cost == 0) {
            return R.success();
        }
        int remain = cash - cost;
        if (remain < 0) {
            return ErrorEnum.CASH_NOT_ENOUGH.getR();
        }
        NewCtGzMaterial newCtGzMaterial = newCtGzMaterialMapper.selectById(param.getId());
        if(null == newCtGzMaterial){
            return ErrorEnum.MATERIAL_NOT_EXIST.getR();
        }

        //记录现金流量表
        NewCtCashflow ctCashflow = new NewCtCashflow();
        ctCashflow.setCDate(param.getDate())
                .setCSurplus(remain)
                .setCIn(0)
                .setCOut(cost)
                .setCAction(CashFlowActionEnum.URGENT_MATERIAL_COST.getAction())
                .setContestId(param.getContestId())
                .setStudentId(userId)
                .setCComment("原材料紧急采购"+newCtGzMaterial.getCnName()+",数量:"+param.getNum()+",成本" + cost + "元");
        ctCashflowService.save(ctCashflow);


        //采购入库更新库存
        NewCtKcMaterial newCtKcMaterial = new NewCtKcMaterial();
        newCtKcMaterial.setStudentId(userId)
                .setContestId(param.getContestId())
                .setImNum(param.getNum())
                .setMaterialPrice(param.getUrgentPrice())
                .setInInventoryDate(param.getDate())
                .setImCmId(param.getId())
                .setMaterialName(newCtGzMaterial.getCnName());
        ctKcMaterialService.save(newCtKcMaterial);

        //计算四张报表
        Integer bsTotalEquity = calBsTotalEquity(userId,param.getContestId(),param.getDate());
        // 所有者权益为负后，自动结束比赛
        if (bsTotalEquity < 0) {
            return R.error(7000, "当前所有者权益为负，你已破产，模拟竞赛结束");
        }
        return R.success();
    }

    /**
     * 紧急采购产品
     * @param userId
     * @param param
     * @return
     */
    @Autowired
    private NewCtGzProductMapper newCtGzProductMapper;
    @Transactional
    public R buyProduct(Integer userId, NewUrgentBuyParam param) {
        //判断现金是否足够
        int cash = ctCashflowService.getCash(userId, param.getContestId());
        // 紧急采购成本
        int cost = (param.getUrgentPrice() == null ? 0:param.getUrgentPrice()) * (param.getNum()==null? 0:param.getNum());
        if (cost == 0) {
            return R.success();
        }
        int remain = cash - cost;
        if (remain < 0) {
            return ErrorEnum.CASH_NOT_ENOUGH.getR();
        }
        NewCtGzProduct newCtGzProduct = newCtGzProductMapper.selectById(param.getId());
        if(null == newCtGzProduct){
            return ErrorEnum.PRODUCT_NOT_EXIST.getR();
        }

        //记录现金流量表
        NewCtCashflow ctCashflow = new NewCtCashflow();
        ctCashflow.setCDate(param.getDate())
                .setCSurplus(remain)
                .setCIn(0)
                .setCOut(cost)
                .setCAction(CashFlowActionEnum.URGENT_PRODUCT_COST.getAction())
                .setContestId(param.getContestId())
                .setStudentId(userId)
                .setCComment("产成品紧急采购"+newCtGzProduct.getCpName()+",数量:"+param.getNum()+"成本" + cost + "元");
        ctCashflowService.save(ctCashflow);


        //采购入库更新库存
        List<NewCtKcProduct> list = new ArrayList<>();
        for (int i = 0; i < param.getNum(); i++) {
            NewCtKcProduct newCtKcProduct = new NewCtKcProduct();
            newCtKcProduct.setStudentId(userId)
                    .setContestId(param.getContestId())
                    .setIpNum(1)
                    .setRealCost(param.getUrgentPrice())
                    .setIsInventory(NewCtKcProduct.IS_IN)
                    .setInventoryDate(param.getDate())
                    .setIpCpId(param.getId());
            list.add(newCtKcProduct);
        }
        ctKcProductService.saveOrUpdateBatch(list);

        //计算四张报表
        Integer bsTotalEquity = calBsTotalEquity(userId,param.getContestId(),param.getDate());
        // 所有者权益为负后，自动结束比赛
        if (bsTotalEquity < 0) {
            return R.error(7000, "当前所有者权益为负，你已破产，模拟竞赛结束");
        }
        return R.success();
    }


    @Transactional
    public R sellMaterial(Integer userId, NewSellMaterialParam param) {

        //获取
        NewCtGzCs ctGzCs = ctGzCsService.getByContestId(param.getContestId());
        List<NewCtKcMaterial> materials = newCtKcMaterialMapper.listByPriceAndDateAndCmId(param.getContestId(), userId,param.getImCmId(), param.getMaterialPrice(), param.getInInventoryDate());
        int kcSum = materials.stream().mapToInt(NewCtKcMaterial::getImNum).sum();
        if(param.getImNum() > kcSum){
            return ErrorEnum.MATERIAL_NOT_ENOUGH.getR();
        }

        int sellSum = param.getImNum();
        int sellTotal = 0;//出售总价
        int directCost = 0;// 本来的本钱
        for (NewCtKcMaterial material : materials) {
            Integer imNum = material.getImNum();
            if(sellSum < imNum ){//更新数量，结束
                imNum = imNum - sellSum;
                material.setImNum(imNum);
                sellTotal += sellSum * material.getMaterialPrice()*ctGzCs.getInventoryDiscountMaterial()/100;
                directCost += sellSum * material.getMaterialPrice();
                newCtKcMaterialMapper.updateById(material);
                break;
            }else {//删除
                sellSum = sellSum - material.getImNum();
                sellTotal += material.getImNum() * material.getMaterialPrice()*ctGzCs.getInventoryDiscountMaterial()/100;
                directCost += material.getImNum() * material.getMaterialPrice();
                newCtKcMaterialMapper.deleteById(material);
                if(sellSum == 0){
                    break;
                }
            }
        }

        int loss = directCost - sellTotal;
        int cash = ctCashflowService.getCash(userId, param.getContestId());

        //现金流量表
        NewCtCashflow ctCashflow = new NewCtCashflow();
        ctCashflow.setCDate(param.getDate())
                .setCSurplus(cash + sellTotal)
                .setCIn(directCost)
                .setCOut(loss)
                .setCAction(CashFlowActionEnum.SELL_MATERIAL.getAction())
                .setContestId(param.getContestId())
                .setStudentId(userId)
                .setCComment("出售原材料");
        ctCashflowService.save(ctCashflow);

        //计算四张报表
        Integer bsTotalEquity = calBsTotalEquity(userId,param.getContestId(),param.getDate());
        // 所有者权益为负后，自动结束比赛
        if (bsTotalEquity < 0) {
            return R.error(7000, "当前所有者权益为负，你已破产，模拟竞赛结束");
        }


        return R.success();
    }


    @Transactional
    public R sellProduct(Integer userId, NewSellProductParam param) {

        //获取
        NewCtGzCs ctGzCs = ctGzCsService.getByContestId(param.getContestId());

        List<NewCtKcProduct> kcProductList = ctKcProductMapper.getKcProductByPriceAndInDate(param.getContestId(),userId,param.getIpCpId(),param.getInventoryDate(),param.getRealCost());
        if(param.getNum() > kcProductList.size()){
            return ErrorEnum.PRODUCT_NOT_ENOUGH.getR();
        }

        //出售总价
        int sellTotal = param.getNum() * param.getRealCost() * ctGzCs.getInventoryDiscountProduct() / 100;

        // 本来的本钱
        int directCost = param.getNum() * param.getRealCost();

        int loss = directCost - sellTotal;
        int cash = ctCashflowService.getCash(userId, param.getContestId());

        //现金流量表
        NewCtCashflow ctCashflow = new NewCtCashflow();
        ctCashflow.setCDate(param.getDate())
                .setCSurplus(cash + sellTotal)
                .setCIn(directCost)
                .setCOut(loss)
                .setCAction(CashFlowActionEnum.SELL_PRODUCT.getAction())
                .setContestId(param.getContestId())
                .setStudentId(userId)
                .setCComment("出售产品");
        ctCashflowService.save(ctCashflow);

        for (int i = 0; i < kcProductList.size(); i++) {
            if(i >= param.getNum()){
                break;
            }
            NewCtKcProduct newCtKcProduct = kcProductList.get(i);
            ctKcProductService.removeById(newCtKcProduct);
        }


        //计算四张报表
        Integer bsTotalEquity = calBsTotalEquity(userId,param.getContestId(),param.getDate());
        // 所有者权益为负后，自动结束比赛
        if (bsTotalEquity < 0) {
            return R.error(7000, "当前所有者权益为负，你已破产，模拟竞赛结束");
        }

        return R.success();
    }



    public R xzorder(Integer studentId, Integer contestId, Integer date) {
        List<NewCtXzOrder> list = ctXzOrderMapper.getList(contestId, studentId);
        list.forEach(p -> {
            String status = "-";
            if (p.getCommitDate() != null && p.getCommitDate() > 0) {
                status = "已交货";
            } else if ((date / 10 == p.getDate() && date % 10 > p.getDeliveryDate()) || ((date / 10) > p.getDate())) {
                status = "已违约";
            } else {
                status = "未到期";
            }
            p.setStatus(status);
        });
        Map<String, Object> resultMap = new HashMap<>(16);
        resultMap.put("list", list);
        return R.success(resultMap);
    }

    public R marketList(Integer userId, Integer contestId) {
        List<NewMarketVo> list = ctYfMarketService.listYfMarket(userId, contestId);
        Map<String, Object> resultMap = new HashMap<>(16);
        resultMap.put("list", list);
        return R.success(resultMap);
    }

    @Transactional
    public R marketYf(Integer userId, NewMarketYfParam param) {
        List<NewMarketVo> list = ctYfMarketService.listYfMarket(userId, param.getContestId());
        Map<Integer, Integer> map = new HashMap<>(16);
        for (NewMarketVo m : list) {
            map.put(m.getMarketId(), m.getCmDevelopFee());
        }
        int cash = ctCashflowService.getCash(userId, param.getContestId());
        NewCtYfMarket yf = ctYfMarketService.getById(param.getMarketId());
        NewCtGzMarket gz = ctGzMarketMapper.selectById(yf.getDmCmId());

        if (NewCtYfMarket.SC_YF_ING.equals(yf.getDmState()) || NewCtYfMarket.SC_YF_FINISH.equals(yf.getDmState())) {
            return R.error(CodeEnum.OTHER_ERROR.getCode(), "无需重复研发");
        }
        int sum = gz.getCmDevelopFee();
        int remain = cash - sum;
        if (remain < 0) {
            return ErrorEnum.CASH_NOT_ENOUGH.getR();
        }
        NewCtCashflow ctCashflow = new NewCtCashflow();
        ctCashflow.setCDate(param.getDate())
                .setCSurplus(remain)
                .setCIn(0)
                .setCOut(sum)
                .setCAction(CashFlowActionEnum.DEVELOP_MARKET.getAction())
                .setContestId(param.getContestId())
                .setStudentId(userId)
                .setCComment("扣减市场"+gz.getCmName()+"开拓" + sum + "元");
        ctCashflowService.save(ctCashflow);

        //更新市场
        Integer cmDevelopDate = gz.getCmDevelopDate();
        Integer fininshDate = DateUtils.addYearAndSeasonTime(param.getDate(), cmDevelopDate);
        if(cmDevelopDate!= null && cmDevelopDate>0){
            yf.setDmState(NewCtYfMarket.SC_YF_ING);
        }else {
            yf.setDmState(NewCtYfMarket.SC_YF_FINISH);
        }
        yf.setDmStartDate(param.getDate());//申请时间
        yf.setDmFinishDate(fininshDate);//结束时间
        ctYfMarketService.updateById(yf);

        //计算四张报表
        Integer bsTotalEquity = calBsTotalEquity(userId,param.getContestId(),param.getDate());
        // 所有者权益为负后，自动结束比赛
        if (bsTotalEquity < 0) {
            return R.error(7000, "当前所有者权益为负，你已破产，模拟竞赛结束");
        }
        return R.success();
    }

    /**
     * 获取社会责任分
     */
    public R  getCorporateScore(Integer userId, Integer contestId){
        int score = newCtCorporateMapper.getScore(userId, contestId);
        return R.success().put("data",score);
    }
}
