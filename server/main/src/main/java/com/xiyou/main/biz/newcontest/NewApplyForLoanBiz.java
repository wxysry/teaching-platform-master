package com.xiyou.main.biz.newcontest;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.xiyou.common.enums.CodeEnum;
import com.xiyou.common.utils.R;
import com.xiyou.main.dao.newcontest.*;
import com.xiyou.main.entity.newcontest.*;
import com.xiyou.main.enums.CashFlowActionEnum;
import com.xiyou.main.enums.ErrorEnum;
import com.xiyou.main.service.newcontest.NewCtBalanceService;
import com.xiyou.main.service.newcontest.NewCtBankLoanService;
import com.xiyou.main.service.newcontest.NewCtCashflowService;
import com.xiyou.main.utils.DateUtils;
import com.xiyou.main.vo.newcontest.NewCashFolowEntity;
import com.xiyou.main.vo.newcontest.NewMaterialVo;
import com.xiyou.main.entity.newcontest.NewRePaymentInfo;
import io.swagger.models.auth.In;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import java.math.BigDecimal;
import java.util.*;

/**
 *
 */
@Service
public class NewApplyForLoanBiz {

    @Autowired
    NewCtCashflowService newCtCashflowService;
    @Autowired
    NewCtBankLoanService newCtBankLoanService;
    @Autowired
    NewCtBalanceService newCtBalanceService;
    @Autowired
    NewCtBankLoanMapper newCtBankLoanMapper;
    @Autowired
    NewCtGzLoanMapper newCtGzLoanMapper;
    @Autowired
    NewCtGzCsMapper newCtGzCsMapper;
    @Autowired
    NewActionBiz newActionBiz;
    @Autowired
    private NewCtWorkerMapper newCtWorkerMapper;
    @Autowired
    private NewRePaymentInfoMapper newRePaymentInfoMapper;
    @Autowired
    private NewCtCashflowMapper newCtCashflowMapper;
    @Autowired
    private NewContestMapper newContestMapper;

    /**
     * 申请贷款-获取最大贷款额度&需贷款年限
     *
     * @param cashFolowEntity
     * @return
     */
    public R getMaxLoanAmountInfo(NewCashFolowEntity cashFolowEntity) {
        return R.success().put("data", newCtBalanceService.getAllMaxLoanAmount(cashFolowEntity));
    }

    /**
     * 申请贷款-确认
     */
    @Transactional
    public R applyForLoanCommit( NewCashFolowEntity cashFolowEntity) {
        //获取融资规则
        NewCtGzLoan ctGzLoan = newCtGzLoanMapper.selectById(cashFolowEntity.getGzLoanId());
        Integer loanTime = ctGzLoan.getLoanTime();
        if (loanTime == null) {
                return R.error(CodeEnum.OTHER_ERROR.getCode(), "该规则中没有贷款时间");
        }
        Integer repaymentDate = DateUtils.addYearAndSeasonTime(cashFolowEntity.getCurrentTime() , loanTime);//贷款时间
        Integer loanAmount = cashFolowEntity.getLoanAmount();

        Integer maxLoanAmount = newCtBalanceService.getMaxLoanAmount(cashFolowEntity);
        if (loanAmount == null  || loanAmount > maxLoanAmount) {
            return ErrorEnum.LOAN_ERROR.getR();
        }

        //先判断当前季度是否已经有该贷款记录
        NewCtBankLoan ctBankLoan= newCtBankLoanService.getOne(new LambdaQueryWrapper<NewCtBankLoan>()
                .eq(NewCtBankLoan::getContestId,cashFolowEntity.getContestId())
                .eq(NewCtBankLoan::getStudentId,cashFolowEntity.getStudentId())
                .eq(NewCtBankLoan::getBlName,ctGzLoan.getLoanName())
                .eq(NewCtBankLoan::getBlAddTime,cashFolowEntity.getCurrentTime())
                .last("limit 1")
        );
        if(ctBankLoan==null){
            //每期利息
            BigDecimal interest = BigDecimal.valueOf(loanAmount).multiply(ctGzLoan.getLoanRate()).divide(BigDecimal.valueOf(100),0, BigDecimal.ROUND_UP);
            BigDecimal allInterest = new BigDecimal(0);
            if("2".equals(ctGzLoan.getLoanRepayment())){
                allInterest = interest.multiply(new BigDecimal(loanTime));
            }else {
                allInterest = interest;
            }

            //插入贷款记录
            NewCtBankLoan loan = new NewCtBankLoan();
            loan.setContestId(cashFolowEntity.getContestId())
                    .setStudentId(cashFolowEntity.getStudentId())
                    .setBlType((ctGzLoan.getLoanTime()!=null && ctGzLoan.getLoanTime()> 4 )? NewCtBankLoan.LONG_LOAN:NewCtBankLoan.SHORT_LOAN)//贷款方式
                    .setBlName(ctGzLoan.getLoanName())//贷款名称
                    .setBlAddTime(cashFolowEntity.getCurrentTime())//贷款时间
                    .setBlFee(loanAmount)//贷款金额
                    .setRepaymentType(ctGzLoan.getLoanRepayment())//还款方式
                    .setBlRepaymentDate(repaymentDate)//还款时间
                    .setRate(ctGzLoan.getLoanRate())//利率
                    .setInterest(allInterest)//利息
                    .setBlRemainTime(ctGzLoan.getLoanTime());//贷款时长
            newCtBankLoanService.save(loan);

            //生成还款信息
            if("2".equals(ctGzLoan.getLoanRepayment())) {
                for (int i = 1; i <= ctGzLoan.getLoanTime(); i++) {
                    Integer payTime = DateUtils.addYearAndSeasonTime(cashFolowEntity.getCurrentTime(), i);
                    NewRePaymentInfo info = new NewRePaymentInfo();
                    info.setAmount(interest)
                            .setBlAddTime(loan.getBlAddTime())
                            .setPayment(CashFlowActionEnum.INTEREST_I.getAction())//每季付息-利息
                            .setContestId(cashFolowEntity.getContestId())
                            .setStudentId(cashFolowEntity.getStudentId())
                            .setLoanId(loan.getBlId())
                            .setPayTime(payTime)//缴费时间
                            .setDescription("利息");//说明
                    newRePaymentInfoMapper.insert(info);
                    if (i == ctGzLoan.getLoanTime()) {
                        NewRePaymentInfo infoB = new NewRePaymentInfo();
                        infoB.setAmount(new BigDecimal(loanAmount))
                                .setBlAddTime(loan.getBlAddTime())
                                .setPayment(CashFlowActionEnum.INTEREST_R.getAction())//每季付息-本金
                                .setContestId(cashFolowEntity.getContestId())
                                .setStudentId(cashFolowEntity.getStudentId())
                                .setLoanId(loan.getBlId())
                                .setPayTime(payTime)//缴费时间
                                .setDescription("本金");//说明本息同还
                        newRePaymentInfoMapper.insert(infoB);
                    }
                }
            }else {
                Integer payTime = DateUtils.addYearAndSeasonTime(cashFolowEntity.getCurrentTime(), ctGzLoan.getLoanTime());
                NewRePaymentInfo info1 = new NewRePaymentInfo();
                info1.setAmount(interest)
                        .setBlAddTime(loan.getBlAddTime())
                        .setPayment(CashFlowActionEnum.REPAYMENT_INTEREST_I.getAction())//本息同还-利息
                        .setContestId(cashFolowEntity.getContestId())
                        .setStudentId(cashFolowEntity.getStudentId())
                        .setLoanId(loan.getBlId())
                        .setPayTime(payTime)//缴费时间
                        .setDescription("利息");//说明
                newRePaymentInfoMapper.insert(info1);

                NewRePaymentInfo info2 = new NewRePaymentInfo();
                info2.setAmount(new BigDecimal(loanAmount))
                        .setBlAddTime(loan.getBlAddTime())
                        .setPayment(CashFlowActionEnum.REPAYMENT_INTEREST_R.getAction())//本息同还-本金
                        .setContestId(cashFolowEntity.getContestId())
                        .setStudentId(cashFolowEntity.getStudentId())
                        .setLoanId(loan.getBlId())
                        .setPayTime(payTime)//缴费时间
                        .setDescription("本金");//说明
                newRePaymentInfoMapper.insert(info2);
            }

        }
        //本季已贷款同种类型的款项
        else{
            //获取总的贷款金额
            Integer allLoanAmount = cashFolowEntity.getLoanAmount()+ctBankLoan.getBlFee();

            //每期利息
            BigDecimal interest = BigDecimal.valueOf(allLoanAmount)
                    .multiply(ctGzLoan.getLoanRate())
                    .divide(BigDecimal.valueOf(100),0, BigDecimal.ROUND_UP);
            BigDecimal allInterest = new BigDecimal(0);
            if("2".equals(ctGzLoan.getLoanRepayment())){
                allInterest = interest.multiply(new BigDecimal(loanTime));
            }else {
                allInterest = interest;
            }

            //更新贷款记录表
            ctBankLoan.setBlFee(allLoanAmount)//贷款金额
                    .setInterest(allInterest);//利息
            newCtBankLoanService.updateById(ctBankLoan);

            //生成还款信息
            //2代表每季付息-最后本息偿还
            if("2".equals(ctGzLoan.getLoanRepayment())) {
                for (int i = 1; i <= ctGzLoan.getLoanTime(); i++) {
                    Integer payTime = DateUtils.addYearAndSeasonTime(cashFolowEntity.getCurrentTime(), i);
                    NewRePaymentInfo info = newRePaymentInfoMapper.selectOne(new LambdaQueryWrapper<NewRePaymentInfo>()
                            .eq(NewRePaymentInfo::getContestId,cashFolowEntity.getContestId())
                            .eq(NewRePaymentInfo::getStudentId,cashFolowEntity.getStudentId())
                            .eq(NewRePaymentInfo::getBlAddTime,ctBankLoan.getBlAddTime())
                            .eq(NewRePaymentInfo::getPayment,CashFlowActionEnum.INTEREST_I.getAction()) //每季付息-利息
                            .eq(NewRePaymentInfo::getLoanId,ctBankLoan.getBlId())
                            .eq(NewRePaymentInfo::getPayTime,payTime)
                            .last("limit 1")
                    );

                    info.setAmount(interest);
                    newRePaymentInfoMapper.updateById(info);
//

                    //最后一季本息同还
                    if (i == ctGzLoan.getLoanTime()) {
                        NewRePaymentInfo infoB = newRePaymentInfoMapper.selectOne(new LambdaQueryWrapper<NewRePaymentInfo>()
                                .eq(NewRePaymentInfo::getContestId,cashFolowEntity.getContestId())
                                .eq(NewRePaymentInfo::getStudentId,cashFolowEntity.getStudentId())
                                .eq(NewRePaymentInfo::getBlAddTime,ctBankLoan.getBlAddTime())
                                .eq(NewRePaymentInfo::getPayment,CashFlowActionEnum.INTEREST_R.getAction()) //每季付息-本金
                                .eq(NewRePaymentInfo::getLoanId,ctBankLoan.getBlId())
                                .eq(NewRePaymentInfo::getPayTime,payTime)
                                .last("limit 1")
                        );
                        infoB.setAmount(new BigDecimal(allLoanAmount));
                        newRePaymentInfoMapper.updateById(infoB);
                    }
                }
            }
            //代表本息同还
            else {
                Integer payTime = DateUtils.addYearAndSeasonTime(cashFolowEntity.getCurrentTime(), ctGzLoan.getLoanTime());
                NewRePaymentInfo info1 = newRePaymentInfoMapper.selectOne(new LambdaQueryWrapper<NewRePaymentInfo>()
                        .eq(NewRePaymentInfo::getContestId,cashFolowEntity.getContestId())
                        .eq(NewRePaymentInfo::getStudentId,cashFolowEntity.getStudentId())
                        .eq(NewRePaymentInfo::getBlAddTime,ctBankLoan.getBlAddTime())
                        .eq(NewRePaymentInfo::getPayment,CashFlowActionEnum.REPAYMENT_INTEREST_I.getAction()) //本息同还-利息
                        .eq(NewRePaymentInfo::getLoanId,ctBankLoan.getBlId())
                        .eq(NewRePaymentInfo::getPayTime,payTime)
                        .last("limit 1")
                );
                info1.setAmount(interest);
                newRePaymentInfoMapper.updateById(info1);



                NewRePaymentInfo info2 =  newRePaymentInfoMapper.selectOne(new LambdaQueryWrapper<NewRePaymentInfo>()
                        .eq(NewRePaymentInfo::getContestId,cashFolowEntity.getContestId())
                        .eq(NewRePaymentInfo::getStudentId,cashFolowEntity.getStudentId())
                        .eq(NewRePaymentInfo::getBlAddTime,ctBankLoan.getBlAddTime())
                        .eq(NewRePaymentInfo::getPayment,CashFlowActionEnum.REPAYMENT_INTEREST_R.getAction()) //本息同还-本金
                        .eq(NewRePaymentInfo::getLoanId,ctBankLoan.getBlId())
                        .eq(NewRePaymentInfo::getPayTime,payTime)
                        .last("limit 1")
                );
                info2.setAmount(new BigDecimal(allLoanAmount));
                newRePaymentInfoMapper.updateById(info2);
            }
        }



        //插入现金交易记录
        Integer cash = newCtCashflowService.getCash(cashFolowEntity.getStudentId(), cashFolowEntity.getContestId());
        NewCtCashflow ctCashflow = new NewCtCashflow();
        ctCashflow.setStudentId(cashFolowEntity.getStudentId())
                .setContestId(cashFolowEntity.getContestId())
                .setCIn(loanAmount)
                .setCOut(0)
                .setCDate(cashFolowEntity.getCurrentTime())
                .setCSurplus(cash + loanAmount)
                .setCComment("贷款" + loanAmount + "元")
                .setCAction((ctGzLoan.getLoanTime()!=null && ctGzLoan.getLoanTime()> 4)? CashFlowActionEnum.LONG_LOAN.getAction():CashFlowActionEnum.SHORT_LOAN.getAction());
        newCtCashflowService.save(ctCashflow);
        //计算报表
        Integer bsTotalEquity = newActionBiz.calBsTotalEquity(cashFolowEntity.getStudentId(),cashFolowEntity.getContestId(),cashFolowEntity.getCurrentTime());
        if (bsTotalEquity < 0) {
            return R.error(7000, "当前所有者权益为负，你已破产，模拟竞赛结束");
        }

        return R.success();
    }

    /**
     * 获取融资（贷款）信息
     * @param contestId 竞赛ID
     * @param studentId 学生ID
     * @return
     */
    public R getLoanBankInfo(Integer contestId,Integer studentId){
        List<NewCtBankLoan> list = newCtBankLoanMapper.getList(contestId, studentId);
        return R.success().put("list",list);
    }

    /**
     * 获取融资（贷款）规则
     * @param contestId 竞赛ID
     * @return
     */
    public R getLoanGz(Integer contestId){
        List<NewCtGzLoan> newCtGzLoans = newCtGzLoanMapper.listCtGzLoan(contestId);
        return R.success().put("data",newCtGzLoans);
    }


    /**
     * 生成管理费信息（缴费了的就返回null）
     * @param contestId
     * @param studentId
     * @param currentTime
     * @return
     */
    public NewRePaymentInfo getOverhaul(Integer contestId,Integer studentId ,Integer currentTime ){
        NewRePaymentInfo overhaulInfo = newRePaymentInfoMapper.getOverhaul(contestId, studentId, currentTime);
        if(overhaulInfo == null){
            NewCtGzCs ctGz = newCtGzCsMapper.getByContestId(contestId);
            int overhaul = ctGz.getOverhaul();//获取管理费
            NewRePaymentInfo info = new NewRePaymentInfo();
            info.setAmount(new BigDecimal(overhaul))
                    .setStudentId(studentId)
                    .setContestId(contestId)
                    .setPayment("管理费")
                    .setBlAddTime(currentTime)
                    .setDescription("每季"+overhaul+"元");
            newRePaymentInfoMapper.insert(info);
            return info;
        }else if(overhaulInfo.getPayTime() == null || overhaulInfo.getPayTime() == 0){
            return overhaulInfo;
        }
        return null;
    }

    /**
     * 缴管理费信息（缴费了的就返回null）
     * @param contestId
     * @param studentId
     * @param currentTime
     * @return
     */
    public void payOverhaul(Integer contestId,Integer studentId ,Integer currentTime){
        NewRePaymentInfo newRePaymentInfo = getOverhaul(contestId, studentId, currentTime);
        if(newRePaymentInfo == null){
            return ;
        }
        Integer overhaul = newRePaymentInfo.getAmount().intValue();
        //获取现金
        Integer cash = newCtCashflowService.getCash(studentId, contestId);
        NewCtCashflow newCtCashflow = new NewCtCashflow();
        newCtCashflow.setStudentId(studentId);
        newCtCashflow.setContestId(contestId);
        newCtCashflow.setCDate(currentTime);
        newCtCashflow.setCSurplus(cash - overhaul);
        newCtCashflow.setCComment("支付管理费:"+overhaul+"元");
        newCtCashflow.setCIn(0);
        newCtCashflow.setCOut(overhaul);
        newCtCashflow.setCAction(CashFlowActionEnum.PAY_OVERHAUL.getAction());
        newCtCashflowService.save(newCtCashflow);
        //更新管理费表
        newRePaymentInfo.setPayTime(currentTime);
        newRePaymentInfoMapper.updateById(newRePaymentInfo);
    }





    /**
     *
     * @param currentTime
     * @param contestId
     * @param studentId
     * @return
     */
    public R startCurrSeason(Integer currentTime,Integer contestId ,Integer studentId){

        //获取管理费
        List<NewRePaymentInfo> listData = new ArrayList<>();
        NewRePaymentInfo overhaul = getOverhaul(contestId, studentId, currentTime);
        if(overhaul != null){
            listData.add(overhaul);
        }
        //需要缴纳利息和本金
        List<NewRePaymentInfo> bankLoans = getBankLoan(currentTime, contestId, studentId);
        listData.addAll(bankLoans);

        //获取贷款利息
        return R.success().put("data",listData);
    }


    /**
     * 获取利息和贷款
     * @param currentTime
     * @param contestId
     * @param studentId
     * @return
     */
    public List<NewRePaymentInfo> getBankLoan(Integer currentTime,Integer contestId ,Integer studentId){
        //获取当季需要偿还的贷款利息和本金
        List<NewRePaymentInfo> listBankInfo = newRePaymentInfoMapper.getListByTime(contestId, studentId, currentTime);
        return listBankInfo;
    }

    /**
     * 缴费
     * @param currentTime
     * @param contestId
     * @param studentId
     * @param
     * @return
     */
    public R payCurrSeason(Integer contestId,Integer studentId,Integer currentTime,String payment,Integer blAddTime,Integer amount){
        Integer cash = newCtCashflowService.getCash(studentId, contestId);
        if(cash<amount){
            return ErrorEnum.CASH_NOT_ENOUGH.getR();
        }

        if("管理费".equals(payment)){
            payOverhaul(contestId,studentId,currentTime);
        }else {
            payCurrSeasonAddCashFlow(contestId,studentId,currentTime,amount,payment,blAddTime,cash);
        }
        //计算报表
        Integer bsTotalEquity = newActionBiz.calBsTotalEquity(studentId,contestId,currentTime);
        if (bsTotalEquity < 0) {
            return R.error(7000, "当前所有者权益为负，你已破产，模拟竞赛结束");
        }

        return R.success();
    }


    /**
     * 支付当季利息或本金
     * @param contestId
     * @param studentId
     * @param currentTime
     * @param amount
     * @param payment
     * @param blAddTime
     * @param cash
     */
    public void payCurrSeasonAddCashFlow(Integer contestId,Integer studentId ,Integer currentTime,Integer amount,String payment,Integer blAddTime,Integer cash){
        cash = cash - amount;
        if(CashFlowActionEnum.INTEREST_R.getAction().equals(payment) || CashFlowActionEnum.REPAYMENT_INTEREST_R.getAction().equals(payment)){
            List<NewRePaymentInfo> newRePaymentInfos = newRePaymentInfoMapper.selectByAmountAndBlAddTimeAndPayment(contestId, studentId, currentTime, payment, blAddTime);
            for (NewRePaymentInfo newRePaymentInfo : newRePaymentInfos) {
                newCtBankLoanMapper.deleteById(newRePaymentInfo.getLoanId());
            }
        }
        //生成流量表
        NewCtCashflow newCtCashflow = new NewCtCashflow();
        newCtCashflow.setStudentId(studentId);
        newCtCashflow.setContestId(contestId);
        newCtCashflow.setCDate(currentTime);
        newCtCashflow.setCSurplus(cash);
        newCtCashflow.setCComment("偿还本金或利息"+amount+"元");
        newCtCashflow.setCIn(0);
        newCtCashflow.setCOut(amount);
        newCtCashflow.setCAction(payment);
        newCtCashflowService.save(newCtCashflow);
        //删除
        newRePaymentInfoMapper.deleteByAmountAndBlAddTimeAndPayment(contestId,studentId,currentTime,payment,blAddTime);

    }

    /**
     * 显示工人列表
     */
    @Autowired
    private NewCtGzWorkerRecruitMapper newCtGzWorkerRecruitMapper;
    public R getWorkerList(Integer contestId,Integer studentId,Integer currentTime){
        //目前已招聘的工人
        List<NewCtWorker> workerList = newCtWorkerMapper.getNewCtWorkerListByContestIdAndCurrTime(contestId, studentId, currentTime);
        Map<String,NewCtWorker> workerMap = new HashMap<>();
        for (NewCtWorker newCtWorker : workerList) {
            String workerName = newCtWorker.getWorkerName();
            workerMap.put(workerName,newCtWorker);
        }

        //市场上要显示的数据
        List<NewCtWorker> list = new ArrayList<>();
        //获取竞赛表
        NewContest newContest = newContestMapper.getObjByContestId(contestId);
        //获取规则表数据
        List<NewCtGzWorkerRecruit> newCtGzWorkerRecruitList = newCtGzWorkerRecruitMapper.getListBySubjectNum(newContest.getSubjectNumber());
        if(newCtGzWorkerRecruitList != null && newCtGzWorkerRecruitList.size()>0){
            for (NewCtGzWorkerRecruit newCtGzWorkerRecruit : newCtGzWorkerRecruitList) {
                for (int i = 1 ;i <=newCtGzWorkerRecruit.getQtyPerQtr() ; i++){
                    String name = newCtGzWorkerRecruit.getRecruitName()+i+"("+currentTime+")";

                    if(!workerMap.containsKey(name)){
                        NewCtWorker newCtWorker = new NewCtWorker();
                        newCtWorker.setContestId(contestId)
                                .setWorkerName(newCtGzWorkerRecruit.getRecruitName()+i+"("+currentTime+")")
                                .setWorkerNum(i)
                                .setRecruitName(newCtGzWorkerRecruit.getRecruitName())
                                .setRecruitNum(newCtGzWorkerRecruit.getRecruitNum())
                                .setMultBonus(newCtGzWorkerRecruit.getMultBonus())
                                .setInitSal(newCtGzWorkerRecruit.getInitSalExpect())
                                .setStartWorkTime(currentTime)
                                .setPiece(newCtGzWorkerRecruit.getPiece())
                                .setIsWork(0);
                        list.add(newCtWorker);
                    }

                }
            }
        }
        return R.success().put("data",list);
    }


    /**
     * 发送offer
     * @param newCtWorker
     * @param studentId
     * @return
     */
    public R sendOffer(NewCtWorker newCtWorker,Integer studentId){
        newCtWorker.setStudentId(studentId)
                .setIsWork(NewCtWorker.ON_BUSI);//招聘

        //判断该员工是否已经招聘
        List<NewCtWorker> newCtWorkerList = newCtWorkerMapper.selectList(
                new LambdaQueryWrapper<NewCtWorker>()
                        .eq(NewCtWorker::getWorkerName,newCtWorker.getWorkerName())
                        .eq(NewCtWorker::getStudentId,studentId)
                        .eq(NewCtWorker::getContestId,newCtWorker.getContestId())
        );
        if((newCtWorkerList!=null) && (newCtWorkerList.size()>0)){
            return ErrorEnum.GRYZP.getR();
        }


        newCtWorkerMapper.insert(newCtWorker);
        return R.success();
    }


    /**
     * 获取原料商店
     * @param contestId
     * @return
     */
    @Autowired
    private NewCtGzMaterialMapper newCtGzMaterialMapper;
    public R getGzMaterialList(Integer contestId, Integer studentId,Integer currentTime){
        //规则表数量
        List<NewCtGzMaterial> gzs = newCtGzMaterialMapper.list(contestId);
        //原料订单sum聚合
        List<NewCtDdMaterial> dds = newCtDdMaterialMapper.getMaterialOrderEachSum(contestId, studentId,currentTime);
        Map<Integer,NewCtDdMaterial> ddsMap = new HashMap<>();
        for (NewCtDdMaterial dd : dds) {
            ddsMap.put(dd.getOmCmid(),dd);
        }
        for (NewCtGzMaterial gz : gzs) {
            if(ddsMap.containsKey(gz.getCmId())){
                //减去订单数量
                int i = gz.getNum() - ddsMap.get(gz.getCmId()).getNum();
                gz.setNum(i);
            }
        }
        return R.success().put("data",gzs);
    }

    @Autowired
    private NewCtFeeMapper newCtFeeMapper;
    /**
     * 原料商店确认下单
     * @param newMaterialVo
     * @param studentId
     * @return
     */
    public R getGzMaterialShopConfirm(NewMaterialVo newMaterialVo , Integer studentId){

        //收货期
        Integer remainDate = DateUtils.addYearAndSeasonTime(newMaterialVo.getCurrentTime(), newMaterialVo.getCmLeadDate());
        //新增订单
        NewCtDdMaterial newCtDdMaterial = new NewCtDdMaterial();
        newCtDdMaterial
                .setContestId(newMaterialVo.getContestId())
                .setStudentId(studentId)
                .setOmCmid(newMaterialVo.getCmId())
                .setCmName(newMaterialVo.getCnName())
                .setPurchaseDate(newMaterialVo.getCurrentTime())
                .setRemainDate(remainDate)
                .setIsInventory(0)
                .setPayDate(newMaterialVo.getCmPayDate())
                .setIsPay(1)
                .setPrice(newMaterialVo.getCmBuyFee())
                .setNum(newMaterialVo.getNum());
        newCtDdMaterialMapper.insert(newCtDdMaterial);
        //更新规则数据
        NewCtGzMaterial gzMaterial = newCtGzMaterialMapper.selectById(newMaterialVo.getCmId());
        gzMaterial.setNum(gzMaterial.getNum()-newMaterialVo.getNum());
        newCtGzMaterialMapper.updateById(gzMaterial);
        return R.success();
    }
    /**
     * 获取原料订单
     * @param contestId
     * @return
     */
    @Autowired
    private NewCtDdMaterialMapper newCtDdMaterialMapper;
    public R getMaterialOrderList(Integer contestId, Integer studentId){
        List<NewCtDdMaterial> list = newCtDdMaterialMapper.list(contestId,studentId);
        return R.success().put("data",list);
    }
    /**
     * 获取原料库存
     * @param contestId
     * @return
     */
    @Autowired
    private NewCtKcMaterialMapper newCtKcMaterialMapper;
    public R getMaterialInventoryList(Integer contestId, Integer studentId){
        List<NewCtKcMaterial> list = newCtKcMaterialMapper.listKc(contestId, studentId);
        return R.success().put("data",list);
    }

    
    /**
     * 获取产品库存 按入库时间，价格，产品分组展示
     * @param contestId
     * @return
     */
    @Autowired
    private NewCtKcProductMapper newCtKcProductMapper;
    public R getProductInventory(Integer contestId, Integer studentId){
        List<NewCtKcProduct> list = newCtKcProductMapper.listKcGroupByPriceAndInventory(contestId, studentId,NewCtKcProduct.IS_IN);
        return R.success().put("data",list);
    }

    /**
     * 收货
     * @param id
     * @param currentTime
     * @return
     */
    public R materialToInventory(Integer id, Integer currentTime){
        NewCtDdMaterial dd = newCtDdMaterialMapper.selectById(id);
        if(dd.getIsInventory() == 1){
            return R.error(CodeEnum.IS_INVENTORY,"已经收货！");
        }
        if(currentTime < dd.getRemainDate()){
            return R.error(CodeEnum.NOT_REMAIN_DATE,"收货期未到！");
        }
        //更新订单入库状态/付款账期
        dd.setIsInventory(NewCtDdMaterial.IS_INVENTORY);
        newCtDdMaterialMapper.updateById(dd);
        //付款账期
        int remainDate = dd.getRemainDate();
        int paymentDate = DateUtils.addYearAndSeasonTime(currentTime,dd.getPayDate());
        int rFee = dd.getNum() * dd.getPrice();
        NewCtFee fee = new NewCtFee();
        fee.setStudentId(dd.getStudentId())
                .setContestId(dd.getContestId())
                .setBorrower("1")
                .setLender("供应商")
                .setRFee(rFee)
                .setRemarks("原材料"+dd.getCmName()+"付款")
                .setRRemainDate(remainDate)
                .setPaymentDate(paymentDate)
                .setType(NewCtFee.TO_PAYMENT);
        newCtFeeMapper.insert(fee);
        //产生入库库存
        NewCtKcMaterial kc = new NewCtKcMaterial();
        kc.setStudentId(dd.getStudentId())//学生id
                .setContestId(dd.getContestId())//竞赛id
                .setImCmId(dd.getOmCmid())//原料商店产品id
                .setImNum(dd.getNum())//数量
                .setMaterialName(dd.getCmName())//原料名称
                .setInInventoryDate(currentTime)//入库时间
                .setMaterialPrice(dd.getPrice());//价格
        newCtKcMaterialMapper.insert(kc);

        //计算四张报表
        Integer bsTotalEquity = newActionBiz.calBsTotalEquity(dd.getStudentId(),dd.getContestId(),currentTime);
        // 所有者权益为负后，自动结束比赛
        if (bsTotalEquity < 0) {
            return R.error(7000, "当前所有者权益为负，你已破产，模拟竞赛结束");
        }

        return R.success();
    }

    /**
     *获取在职工人信息
     * @param contestId
     * @param studentId
     * @return
     */
    public R getWorkerOnTheJobList(Integer contestId,Integer studentId ){
        List<NewCtWorker> workerList = newCtWorkerMapper.getAllWorker(contestId, studentId);
        return R.success().put("data",workerList);
    }

    /**
     *获取班次规则
     * @param contestId
     * @param studentId
     * @return
     */
    @Autowired
    private NewCtGzClassesMapper newCtGzClassesMapper;
    public R getClassList(Integer contestId){
        List<NewCtGzClasses> classes = newCtGzClassesMapper.getClassesByContestId(contestId);
        return R.success().put("data",classes);
    }


    @Autowired
    private NewCtLineMapper newCtLineMapper;
    public R getLineGroupInfo(Integer studentId,Integer contestId,Integer currentTime){
        List<NewCtLine> listLine = newCtLineMapper.getLineListOrderByFinishTime(studentId, contestId, currentTime);
        return R.success().put("list",listLine);
    }
}
