package com.xiyou.main.biz.newcontest;

import com.alibaba.fastjson.JSONObject;
import com.xiyou.common.enums.CodeEnum;
import com.xiyou.common.utils.R;
import com.xiyou.main.dao.newcontest.*;
import com.xiyou.main.entity.newcontest.*;
import com.xiyou.main.service.newcontest.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @program: multi-module
 * @description:
 * @author: wangxingyu
 * @create: 2023-06-11
 **/
@Service
@Slf4j
public class NewBusinessBiz {

    @Autowired
    private NewContestService newContestService;
    @Autowired
    private NewContestStudentService newContestStudentService;
    @Autowired
    private NewContestStudentMapper newContestStudentMapper;
    @Autowired
    private NewCtSubjectMapper newCtSubjectMapper;

    @Autowired
    private NewCtGzIsoMapper newCtGzIsoMapper;
    @Autowired
    private NewCtGzProductMapper newCtGzProductMapper;
    @Autowired
    private NewCtYfProductMapper newCtYfProductMapper;
    @Autowired
    private NewCtGzMarketMapper newCtGzMarketMapper;
    @Autowired
    private NewCtGzMaterialMapper newCtGzMaterialMapper;
    @Autowired
    private NewCtGzOrderMapper newCtGzOrderMapper;
    @Autowired
    private NewCtGzAdMapper newCtGzAdMapper;
    @Autowired
    private NewCtGzNumMapper newCtGzNumMapper;
    @Autowired
    private NewCtYfNumMapper newCtYfNumMapper;
    @Autowired
    private NewCtCorporateMapper newCtCorporateMapper;



    @Autowired
    private NewCtBalanceService newCtBalanceService;
    @Autowired
    private NewCtCashflowService newCtCashflowService;

    @Autowired
    private NewCtMnChooseMapper newCtMnChooseMapper;
    @Autowired
    private NewCtMnAdMapper newCtMnAdMapper;
    @Autowired
    private NewRestoreDataBiz newRestoreDataBiz;
    @Autowired
    private NewCtYfIsoMapper newCtYfIsoMapper;
    @Autowired
    private NewCtYfMarketMapper newCtYfMarketMapper;


    @Transactional
    public R start(Integer studentId, Integer contestId) {

        NewContestStudent newContestStudent = newContestStudentService.get(contestId, studentId);
        if (newContestStudent == null) {
            return R.error(CodeEnum.OTHER_ERROR.getCode(), "竞赛不存在");
        }
        if (newContestStudent.getStart() == 1) {
            return R.error(CodeEnum.OTHER_ERROR.getCode(), "已开始经营");
        }
        if (newContestStudent.getFinishTime() != null) {
            return R.error(CodeEnum.OTHER_ERROR.getCode(), "已完成该竞赛模拟");
        }
        // 初始权益作为初始资金
        NewContest newContest = newContestService.getById(contestId);

        Integer cash = newContest == null ? 0 : newContest.getEquity();

        // balance新增一条记录
        // 学生ID、考试ID、1、0、规则表该套题中的cash、0、0、0、0、规则表该套题中的cash、0、0、0、0、规则表该套题中的cash、0、0、0、0、规则表该套题中的cash、0、0、规则表该套题中的cash、规则表该套题中的cash
        NewCtBalance newCtBalance = new NewCtBalance()
                .setStudentId(studentId)
                .setContestId(contestId)
                .setBsIsxt(1)
                .setBsYear(0)
                .setBsCash(cash)
                .setBsReceivable(0)
                .setBsProductInProcess(0)
                .setBsProduct(0)
                .setBsMaterial(0)
                .setBsTotalCurrentAsset(cash)
                .setBsEquipment(0)
                .setBsProjectOnConstruction(0)
                .setBsTotalFixedAsset(0)
                .setBsTotalAsset(cash)
                .setBsLongLoan(0)
                .setBsShortLoan(0)
                .setBsOtherPay(0)
                .setBsTax(0)
                .setBsTotalLiability(0)
                .setBsEquity(cash)
                .setBsRetainedEarning(0)
                .setBsAnnualNetProfit(0)
                .setBsTotalEquity(cash)
                .setBsTotal(cash)
                .setIsSubmit("Y");
        newCtBalanceService.save(newCtBalance);

        // Cashflow新增一条记录，具体为：
        // 学生ID、考试ID、顺序号、开始经营、规则表该套题中的cash、0、规则表该套题中的cash、公司成立、10
        NewCtCashflow cashflow = new NewCtCashflow()
                .setStudentId(studentId)
                .setContestId(contestId)
                .setCAction("开始经营")
                .setCIn(cash)
                .setCOut(0)
                .setCSurplus(cash)
                .setCComment("公司成立")
                .setCDate(10);
        newCtCashflowService.save(cashflow);

        // ISO研发情况新增记录，具体为：
        // 该套规则中含有的所有ISO编号各生成一条记录，ISO编号=ISO编号，总研发时间=研发周期，已研发时间=0，剩余研发时间=总研发周期，完成时间 is null
        List<NewCtGzIso> ctGzIsoList = newCtGzIsoMapper.list(contestId);
        List<NewCtYfIso> ctYfIsoList = new ArrayList<>();
        for (NewCtGzIso newCtGzIso : ctGzIsoList) {
            ctYfIsoList.add(new NewCtYfIso()
                    .setStudentId(studentId)
                    .setContestId(contestId)
                    .setDiCiid(newCtGzIso.getId())
                    .setDiTotalDate(newCtGzIso.getCiDevelopDate())
                    .setDiNowDate(0)
                    .setDiRemainDate(newCtGzIso.getCiDevelopDate())
                    .setDiFinishDate(null)
                    .setDiState(NewCtYfIso.ISO_YF_NONE)
            )
            ;
        }
        if (ctYfIsoList.size() > 0) {
            newCtYfIsoMapper.insertBatch(ctYfIsoList);
        }
        // 市场研发、产品研发同ISO
        List<NewCtGzMarket> ctGzMarketList = newCtGzMarketMapper.list(contestId);
        List<NewCtYfMarket> ctYfMarketList = new ArrayList<>();
        for (NewCtGzMarket ctGzMarket : ctGzMarketList) {
            ctYfMarketList.add(new NewCtYfMarket()
                    .setStudentId(studentId)
                    .setContestId(contestId)
                    .setDmCmId(ctGzMarket.getId())
                    .setDmTotalDate(ctGzMarket.getCmDevelopDate())
                    .setDmNowDate(0)
                    .setDmRemainDate(ctGzMarket.getCmDevelopDate())
                    .setDmFinishDate(null)
                    .setDmState(NewCtYfMarket.SC_YF_NONE)
            );
        }
        if (ctYfMarketList.size() > 0) {
            newCtYfMarketMapper.insertBatch(ctYfMarketList);
        }

        List<NewCtGzProduct> ctGzProductList = newCtGzProductMapper.getList(contestId);
        List<NewCtYfProduct> ctYfProductList = new ArrayList<>();
        for (NewCtGzProduct ctGzProduct : ctGzProductList) {
            ctYfProductList.add(new NewCtYfProduct()
                    .setStudentId(studentId)
                    .setContestId(contestId)
                    .setDpCpId(ctGzProduct.getId())
                    .setDpTotalDate(ctGzProduct.getCpDevelopDate())
                    .setDpNowDate(0)
                    .setDpRemainDate(ctGzProduct.getCpDevelopDate())
                    .setDpFinishDate(null)
                    .setDpState(NewCtYfProduct.CP_YF_NONE)
            );
        }
        if (ctYfProductList.size() > 0) {
            newCtYfProductMapper.insertBatch(ctYfProductList);
        }
        //数字化研发
        List<NewCtGzNum> newCtGzNums = newCtGzNumMapper.getList(contestId);
        for (NewCtGzNum newCtGzNum : newCtGzNums) {
            NewCtYfNum newCtYfNum = new NewCtYfNum().
                    setStudentId(studentId)
                    .setContestId(contestId)
                    .setNumId(newCtGzNum.getId())
                    .setConsumeMoney(newCtGzNum.getConsumeMoney())
                    .setState(NewCtYfNum.NUM_YF_NONE);
            newCtYfNumMapper.insert(newCtYfNum);
        }

        //初始化社会责任分
        NewCtCorporate ctCorporate = new NewCtCorporate();
        ctCorporate.setContestId(contestId)
                    .setScore(NewCtCorporate.DEFAULT_SCORE)
                    .setStudentId(studentId)
                    .setAction("初始社会责任分100")
        ;
        newCtCorporateMapper.insert(ctCorporate);



        // 模拟选单mn_choose新增数据=市场定单gz_order为该套的
        List<NewCtMnChoose> ctMnChooseList = newCtGzOrderMapper.getList(contestId);
        if (ctMnChooseList != null) {
            for (NewCtMnChoose ctMnChoose : ctMnChooseList) {
                ctMnChoose.setStudentId(studentId).setContestId(contestId);
            }
            if (ctMnChooseList.size() > 0) {
                newCtMnChooseMapper.insertBatch(ctMnChooseList);
            }
        }

        // 模拟广告mn_ad新增数据=初始广告gz_ad为该套的
        List<NewCtMnAd> ctMnAdList = newCtGzAdMapper.get(contestId);
        for (NewCtMnAd ctMnAd : ctMnAdList) {
            ctMnAd.setStudentId(studentId).setContestId(contestId).setIsSubmit("Y");
        }
        if (ctMnAdList.size() > 0) {
            newCtMnAdMapper.insertBatch(ctMnAdList);
        }

        // 更新开始状态
        newContestStudentMapper.updateStart(contestId, studentId);

        // 完成以上操作后，除了规则表，其余备份到新的表。
        //备份当年数据
        newRestoreDataBiz.backupData(contestId, studentId);
        //备份当季数据
        newRestoreDataBiz.backupSeasonData(contestId,studentId);

        return R.success();
    }

    public R saveProgress(Integer studentId, Integer contestId, Integer date, String progress) {
        newContestStudentMapper.saveProgress(studentId, contestId, date, progress);
        return R.success();
    }

    public R getProgress(Integer studentId, Integer contestId) {
        NewContestStudent newContestStudent = newContestStudentMapper.get(contestId, studentId);
        if (newContestStudent == null) {
            return R.error(CodeEnum.PARAM_ERROR.getCode(), "竞赛不存在");
        }
        if (newContestStudent.getFinishTime() != null) {
            return R.error(CodeEnum.OTHER_ERROR.getCode(), "已完成该竞赛模拟");
        }
        NewContest newContest = newContestService.getById(contestId);
        if (newContest.getOpenTimeEnd().isBefore(LocalDateTime.now())) {
            return R.error(CodeEnum.OTHER_ERROR.getCode(), "竞赛模拟已结束");
        }
        Map<String, Object> returnMap = new HashMap<>();
        returnMap.put("restore", newContest.getRestore());
        returnMap.put("restoreSeason", newContest.getRestoreSeason());
        returnMap.put("restart", newContest.getRestart());
        returnMap.put("title", newContest.getTitle());
        returnMap.put("progress", newContestStudent.getProgress());
        returnMap.put("date", newContestStudent.getDate());
        return R.success(returnMap);
    }

    @Transactional
    public R end(Integer studentId, Integer contestId) {
        NewContestStudent newContestStudent = newContestStudentMapper.get(contestId, studentId);
        if (newContestStudent == null) {
            return R.error(CodeEnum.OTHER_ERROR);
        }
        if (newContestStudent.getFinishTime() != null) {
            return R.error(CodeEnum.OTHER_ERROR.getCode(), "已完成该竞赛模拟");
        }
        newContestStudent.setFinishTime(LocalDateTime.now());
        newContestStudentService.updateById(newContestStudent);
//        // 清除本年度备份的数据
//        newRestoreDataBiz.removeLikeTableData(contestId, studentId);
//        // 清除季度备份数据
//        newRestoreDataBiz.removeSeasonLikeTableData(contestId,studentId);
        return R.success();
    }

    public R saveXzorder(NewContestStudent newContestStudent) {
        log.error("人机模式,保存选单进度,{}", JSONObject.toJSONString(newContestStudent));
        newContestStudentMapper.saveXzorder(newContestStudent);
        return R.success();
    }

    public R getXzorder(Integer studentId, Integer contestId) {
        NewContestStudent newContestStudent = newContestStudentMapper.get(contestId, studentId);
        NewContest newContest = newContestService.getById(contestId);
        if (newContestStudent == null || newContest == null) {
            return R.error(CodeEnum.PARAM_ERROR.getCode(), "竞赛不存在");
        }
        Map<String, Object> returnMap = new HashMap<>();
        returnMap.put("marketList", newContestStudent.getMarketList());
        returnMap.put("activeName", newContestStudent.getActiveName());
        // 是否可以重新选单
        returnMap.put("restoreOrder", newContest.getRestoreOrder());
        return R.success(returnMap);
    }

    @Transactional
    public R restart(Integer studentId, Integer contestId) {
        NewContest newContest = newContestService.getById(contestId);
        if (newContest == null) {
            return R.error(CodeEnum.OTHER_ERROR.getCode(), "无此竞赛");
        }
        NewContestStudent newContestStudent = newContestStudentService.get(contestId, studentId);
        if (newContestStudent == null) {
            return R.error(CodeEnum.OTHER_ERROR.getCode(), "无此竞赛");
        }

        // 清除经营进度
        newContestStudentMapper.removeProgress(contestId, studentId);

        // 删除经营数据
        newCtSubjectMapper.removeLikeTableData(contestId, studentId);
        newCtSubjectMapper.removeSeasonLikeTableData(contestId,studentId);

        //删除所有的数据，除了new_contest_student
        newCtSubjectMapper.removeMainTableDataNS(contestId, studentId);

        return R.success();
    }
}
