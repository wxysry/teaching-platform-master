package com.xiyou.main.biz.newcontest;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.xiyou.common.enums.CodeEnum;
import com.xiyou.common.exception.CustomException;
import com.xiyou.common.office.utils.EasyPOIUtil;
import com.xiyou.common.utils.CommonUtil;
import com.xiyou.common.utils.R;
import com.xiyou.main.dao.exam.ResourcesMapper;
import com.xiyou.main.dao.newcontest.*;
import com.xiyou.main.entity.exam.Resources;
import com.xiyou.main.entity.exam.SysUser;
import com.xiyou.main.entity.gbcontest.GbCtLine;
import com.xiyou.main.entity.newcontest.*;
import com.xiyou.main.enums.CashFlowActionEnum;
import com.xiyou.main.enums.ErrorEnum;
import com.xiyou.main.params.contest.ContestParam;
import com.xiyou.main.params.contest.ContestStudentParam;
import com.xiyou.main.params.newcontest.DigitalizeParam;
import com.xiyou.main.service.exam.SysUserService;
import com.xiyou.main.service.exam.TeachClassService;
import com.xiyou.main.service.newcontest.*;
import com.xiyou.main.utils.DateUtils;
import com.xiyou.main.vo.contest.ContestScore;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @program: multi-module
 * @description:
 * @author: tangcan
 * @create: 2019-08-25 10:39
 **/
@Service
public class NewContestBiz {
    @Autowired
    private NewContestService newContestService;
    @Autowired
    private TeachClassService teachClassService;
    @Autowired
    private NewContestStudentService newContestStudentService;
    @Autowired
    private NewCtSubjectMapper newCtSubjectMapper;
    @Autowired
    private ResourcesMapper resourcesMapper;
    @Autowired
    private SysUserService sysUserService;
    @Autowired
    private NewCtGzCsService newCtGzCsService;
    @Autowired
    private NewCtWorkerMapper newCtWorkerMapper;
    @Autowired
    private NewCtGzProductMapper newCtGzProductMapper;
    @Autowired
    private NewCtKcMaterialMapper newCtKcMaterialMapper;
    @Autowired
    private NewCtGzMaterialMapper newCtGzMaterialMapper;
    @Autowired
    private NewCtGzProducingMapper newCtGzProducingMapper;
    @Autowired
    private NewCtKcProductMapper newCtKcProductMapper;
    @Autowired
    private NewCtLineMapper newCtLineMapper;
    @Autowired
    private NewCtGzClassesMapper newCtGzClassesMapper;
    @Autowired
    private NewCtFeeMapper newCtFeeMapper;
    @Autowired
    private NewCtCashflowService newCtCashflowService;


    public R listByTeacher(ContestParam contestParam) {
        Page<NewContest> page = newContestService.getPage(contestParam);
        for (NewContest newContest : page.getRecords()) {
            // 竞赛状态
            int status;
            LocalDateTime now = LocalDateTime.now();
            if (newContest.getIsPublish() == 0) {
                status = 0;
            } else if (now.isBefore(newContest.getOpenTimeStart())) {
                // 未开始
                status = 1;
            } else if (now.isAfter(newContest.getOpenTimeEnd())) {
                // 已结束
                status = 3;
            } else {
                // 竞赛中
                status = 2;
            }
            newContest.setStatus(status);
        }
        Map<String, Object> returnMap = new HashMap<>();
        returnMap.put("total", page.getTotal());
        returnMap.put("list", page.getRecords());
        return R.success(returnMap);
    }

    public R listByStudent(ContestParam contestParam) {
        Page<NewContest> page = newContestService.getStudentContestPage(contestParam);
        for (NewContest contest : page.getRecords()) {
            // 竞赛状态
            int status;
            LocalDateTime now = LocalDateTime.now();
            if (now.isBefore(contest.getOpenTimeStart())) {
                // 未开始
                status = 0;
            } else if (now.isAfter(contest.getOpenTimeEnd())) {
                // 已结束
                status = 2;
            } else {
                // 竞赛中
                status = 1;
            }
            contest.setStatus(status);
        }
        Map<String, Object> returnMap = new HashMap<>();
        returnMap.put("total", page.getTotal());
        returnMap.put("list", page.getRecords());
        return R.success(returnMap);
    }

    public R get(Integer contestId) {
        Map<String, Object> returnMap = new HashMap<>();
        NewContest newContest = newContestService.getById(contestId);
        if (newContest == null) {
            return R.error(CodeEnum.OTHER_ERROR.getCode(), "无此竞赛");
        }
        NewCtSubject newCtSubject = newCtSubjectMapper.getBySubjectNumber(newContest.getSubjectNumber());
        if (newCtSubject != null) {
            newContest.setGroupId(newCtSubject.getGroupId());
            newContest.setStudentList(newContestStudentService.getStudentIdList(contestId));
        }
        int start = 0;
        if (newContest.getIsPublish() == 1 && newContest.getOpenTimeStart().isBefore(LocalDateTime.now())) {
            start = 1;
        }
        returnMap.put("start", start);
        returnMap.put("info", newContest);

        return R.success(returnMap);
    }

    public R add(NewContest newContest) {
        List<Integer> studentIdList;
        if (newContest.getPublishAll() != null && newContest.getPublishAll() == 1) {
            studentIdList = teachClassService.getStudentIdList(newContest.getTeacherId());
        } else {
            studentIdList = newContest.getStudentList();
        }
        if (studentIdList != null) {
            studentIdList = studentIdList.stream().distinct().collect(Collectors.toList());
        }
        newContest.setStudentNum(studentIdList == null ? 0 : studentIdList.size());
        newContestService.save(newContest);
        if (studentIdList != null && studentIdList.size() > 0) {
            newContestStudentService.insertBatch(newContest.getContestId(), studentIdList);
        }

        return R.success().put("contestId", newContest.getContestId());
    }

    public R update(NewContest newContest) {
        if (newContest.getContestId() == null) {
            return R.error(CodeEnum.PARAM_MISS.getCode(), "请选择竞赛");
        }
        NewContest newContest1 = newContestService.getById(newContest.getContestId());
        if (newContest1 == null || !newContest1.getTeacherId().equals(newContest.getTeacherId())) {
            return R.error(CodeEnum.PARAM_ERROR.getCode(), "无此竞赛");
        }
        if (newContest1.getIsPublish() == 1 && newContest1.getOpenTimeEnd().isBefore(LocalDateTime.now())) {
            return R.error(CodeEnum.PARAM_ERROR.getCode(), "竞赛模拟已结束，不能更新");
        }
        // 未开始才能修改学生
        boolean start = (newContest.getIsPublish() == 1 && newContest.getOpenTimeStart().isBefore(LocalDateTime.now()));
        if (!start) {
            List<Integer> studentIdList;
            if (newContest.getPublishAll() != null && newContest.getPublishAll() == 1) {
                studentIdList = teachClassService.getStudentIdList(newContest.getTeacherId());
            } else {
                studentIdList = newContest.getStudentList();
            }
            if (studentIdList != null) {
                studentIdList = studentIdList.stream().distinct().collect(Collectors.toList());
            }
            newContest.setStudentNum(studentIdList == null ? 0 : studentIdList.size());

            // 先删掉原来的学生
            newContestStudentService.remove(newContest.getContestId());
            if (studentIdList != null && studentIdList.size() > 0) {
                // 再插入
                newContestStudentService.insertBatch(newContest.getContestId(), studentIdList);
            }
        }
        if (start) {
            // 开始了就不再更改初始权益
            newContest.setEquity(null);
        }
        newContestService.updateById(newContest);
        return R.success();
    }

    public R delete(Integer contestId) {
        newContestService.removeById(contestId);
        QueryWrapper<NewContestStudent> wrapper = new QueryWrapper<>();
        wrapper.eq("contest_id", contestId);
        newContestStudentService.remove(wrapper);
        return R.success();
    }

    public R publish(Integer teacherId, Integer contestId) {
        SysUser user = sysUserService.getById(teacherId);
        // 只能同时发布一个竞赛
        if (newContestService.getUnEndContest(teacherId) >= user.getContestNum()) {
            return R.error(CodeEnum.OTHER_ERROR.getCode(), "只能同时发布 " + user.getContestNum() + " 套竞赛模拟");
        }
        NewContest newContest = newContestService.getById(contestId);
        if (newContest == null || !newContest.getTeacherId().equals(teacherId)) {
            return R.error(CodeEnum.PARAM_ERROR.getCode(), "竞赛不存在");
        }
        if (newContest.getIsPublish() == 1) {
            return R.error(CodeEnum.OTHER_ERROR.getCode(), "竞赛已发布！");
        }
        if (newContest.getStudentNum() == 0) {
            return R.error(CodeEnum.OTHER_ERROR.getCode(), "暂无考生，不能发布！");
        }
        if (newContest.getOpenTimeEnd().isBefore(LocalDateTime.now())) {
            return R.error(CodeEnum.OTHER_ERROR.getCode(), "发布失败！开放时间已过！");
        }
        newContestService.publish(teacherId, contestId);
        return R.success();
    }

    public R studentList(ContestStudentParam contestStudentParam) {
        Page<NewContestStudent> page = newContestStudentService.getPage(contestStudentParam);
        Map<String, Object> returnMap = new HashMap<>();
        returnMap.put("total", page.getTotal());
        returnMap.put("list", page.getRecords());
        return R.success(returnMap);
    }

    public R getSpyAttachment(Integer contestId, Integer year) {
        NewCtSubject newCtSubject = newCtSubjectMapper.getByContestId(contestId);
        if (newCtSubject == null || StringUtils.isBlank(newCtSubject.getSpyAttachment())) {
            return R.error(CodeEnum.OTHER_ERROR.getCode(), "暂无间谍");
        }
        String[] spys = newCtSubject.getSpyAttachment().split("\\|");
        if (spys.length < year || year <= 0) {
            return R.error(CodeEnum.OTHER_ERROR.getCode(), "暂无间谍");
        }
        Resources resources = resourcesMapper.get(spys[year - 1]);
        if (resources == null) {
            return R.error(CodeEnum.OTHER_ERROR.getCode(), "暂无间谍");
        }
        return R.success().put("info", resources);
    }

    public R getRuleAttachment(Integer contestId) {
        Resources resources = newCtSubjectMapper.getRuleAttachment(contestId);
        if (resources == null) {
            return R.error(CodeEnum.OTHER_ERROR.getCode(), "暂无规则预测详单");
        }
        return R.success().put("info", resources);
    }

    public R studentScore(Integer id, Double score) {
        newContestStudentService.updateScore(id, score);
        return R.success();
    }

    public R getEquity(Integer subjectNumber) {
        // 规则表该套题中的cash
        NewCtGzCs ctGzCs = newCtGzCsService.getBySubjectNumber(subjectNumber);
        Integer cash = ctGzCs == null ? 0 : ctGzCs.getCash();
        return R.success().put("equity", cash);
    }

    public void downloadScore(Integer contestId, HttpServletResponse response) {
        NewContest newContest = newContestService.getById(contestId);
        if (newContest == null) {
            return;
        }
        List<ContestScore> contestScoreList = newContestStudentService.getScoreList(contestId);
        Map<String, Object> paramMap = new HashMap<>();
        List<Map<String, Object>> mapList = new ArrayList<>(CommonUtil.getListInitCap(contestScoreList.size()));
        int num = 0;
        Map<String, Object> map;
        for (ContestScore contestScore : contestScoreList) {
            map = new HashMap<>();
            map.put("id", ++num);
            map.put("name", contestScore.getName());
            map.put("account", contestScore.getAccount());
            map.put("school", contestScore.getSchool());
            map.put("equity", contestScore.getEquity() == null ? "-" : (contestScore.getEquity() + "W"));
            map.put("score", contestScore.getScore() == null ? "-" : contestScore.getScore());
            mapList.add(map);
        }
        paramMap.put("maplist", mapList);
        String paperName = "《" + newContest.getTitle() + "》";
        paramMap.put("title", paperName);
        try {
            EasyPOIUtil.exportMultiSheetExcelByMap(response, paperName + "竞赛成绩" + ".xlsx", CommonUtil.getClassesPath() + "templates/contest_score.xlsx", paramMap, false);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    /**
     * 数字化
     * @param userId
     * @param param
     * @return
     */
    @Transactional
    public R digitalizeStart(Integer userId,  DigitalizeParam param){
        List<DigitalizeParam.LineIdAndCpId> LineIdCpIds = param.getList();
        for (DigitalizeParam.LineIdAndCpId lineIdAndCpId : LineIdCpIds) {
            NewCtLine ctLine = newCtLineMapper.selectById(lineIdAndCpId.getLineId());
            String status = ctLine.getStatus();
            if(!GbCtLine.ON_SPACE.equals(status)){
                throw new CustomException(CodeEnum.OTHER_ERROR.getCode(), "操作失败，请重试");
            }
        }


        //获取工人
        List<NewCtWorker> workers = newCtWorkerMapper.getNewCtWorkerListOnTheJob(param.getContestId(), userId,NewCtWorker.ON_SPACE);
        //获取班次
        NewCtGzClasses classes = newCtGzClassesMapper.selectById(param.getClassesId());
        int gj = 0 ,pt = 0;//人数
        int gjXl = 0, ptXl = 0 ;//效率
        List<NewCtWorker> workerGj = new ArrayList<>();//高级工人
        List<NewCtWorker> workerPt = new ArrayList<>();//普通工人
        for (NewCtWorker worker : workers) {
            if("手工工人".equals(worker.getRecruitName())) {
                pt++;
                ptXl = worker.getMultBonus();
                workerPt.add(worker);
            }
            if("高级工人".equals(worker.getRecruitName())) {
                gj++;
                gjXl = worker.getMultBonus();
                workerGj.add(worker);
            }
        }

        //获取原料库存信息
        List<NewCtKcMaterial> materials = newCtKcMaterialMapper.list(param.getContestId(), userId);

        //材料规则id和材料编号的键值对
        List<NewCtGzMaterial> materialGzs = newCtGzMaterialMapper.list(param.getContestId());
        Map<Integer,Integer> idAndCmIdMap = new HashMap<>();
        for (NewCtGzMaterial newCtGzMaterial : materialGzs) {
            idAndCmIdMap.put(newCtGzMaterial.getId(),newCtGzMaterial.getCmId());
        }
        //获取原料规则
        Map<Integer,NewCtGzMaterial> materialGzMaps = new HashMap<>();
        for (NewCtGzMaterial materialGz : materialGzs) {
            materialGzMaps.put(materialGz.getCmId(),materialGz);
        }
        //把材料库存分类
        Map<Integer,List<NewCtKcMaterial>> kcMaterialMap = new HashMap<>();
        for (NewCtGzMaterial materialGz : materialGzs) {
            List<NewCtKcMaterial> list = new ArrayList<>();
            for (NewCtKcMaterial material : materials) {
                Integer imNum = idAndCmIdMap.get(material.getImCmId());
                if(imNum.equals(materialGz.getCmId())){
                    list.add(material);
                }
            }
            kcMaterialMap.put(materialGz.getCmId(),list);
        }

        List<DigitalizeParam.LineIdAndCpId> LineIdAndCpIds = param.getList();
        for (DigitalizeParam.LineIdAndCpId lineIdAndCpId : LineIdAndCpIds) {
            NewCtLine ctLine = newCtLineMapper.selectById(lineIdAndCpId.getLineId());
            //生产线规则
            NewCtGzProductLine newCtGzProductLine = newCtLineMapper.getGzLineByLineId(lineIdAndCpId.getLineId());
            //产品规则
            NewCtGzProduct newCtGzProduct = newCtGzProductMapper.selectById(lineIdAndCpId.getCpId());
            //获取当前产品所需的材料及数量
            List<NewCtGzProducing> newCtGzProducings = newCtGzProducingMapper.getListByCpId(lineIdAndCpId.getCpId());
            Integer cplSeniorWorker = newCtGzProductLine.getCplSeniorWorker();//高级工人数
            Integer cplOrdinaryWorker = newCtGzProductLine.getCplOrdinaryWorker();//普通工人数
            Integer productFinishTime = DateUtils.addYearAndSeasonTime(param.getCurrentTime(),newCtGzProductLine.getCplProduceDate() == null?0:newCtGzProductLine.getCplProduceDate());
            //查询出当前产线的工人
            List<NewCtWorker> workerOnLines = newCtWorkerMapper.getAllWorkerListByLineId(ctLine.getLineId());
            int gjOn = 0 ,ptOn = 0;//人数
            for (NewCtWorker worker : workerOnLines) {
                if("手工工人".equals(worker.getRecruitName())) {
                    ptOn++;
                }
                if("高级工人".equals(worker.getRecruitName())) {
                    gjOn++;
                }
            }
            int cplSeniorWorkerNeed =  cplSeniorWorker - gjOn;
            int cplOrdinaryWorkerNeed =  cplOrdinaryWorker - ptOn;
            if(((gj - cplSeniorWorkerNeed) >= 0) && ((pt - cplOrdinaryWorkerNeed) >= 0 )){
                //产量
                int production = newCtGzProductLine.getCplProduction() * (100 + cplOrdinaryWorker * ptXl / 4 + cplSeniorWorker * gjXl) * classes.getOutputMulti()/100;
                //获取生产所需加工费
                int developFee = newCtGzProduct.getCpDevelopFee() * production;
                Integer cash = newCtCashflowService.getCash(userId, param.getContestId());
                int remain = cash - developFee;
                if (remain < 0) {
                    return ErrorEnum.CASH_NOT_ENOUGH.getR();
                }
                gj = gj - cplSeniorWorkerNeed;
                pt = pt - cplOrdinaryWorkerNeed;
                //工人分配
                for (int i = workerGj.size() - 1; i >= gj; i--) {
                    NewCtWorker newCtWorker = workerGj.get(i);
                    newCtWorker.setIsWork(NewCtWorker.ON_WORK)
                            .setLineId(ctLine.getLineId());
                    newCtWorkerMapper.updateById(newCtWorker);
                    workerGj.remove(newCtWorker);
                }
                for (int i = workerPt.size() - 1; i >= pt; i--) {
                    NewCtWorker newCtWorker = workerPt.get(i);
                    newCtWorker.setIsWork(NewCtWorker.ON_WORK)
                            .setLineId(ctLine.getLineId());
                    newCtWorkerMapper.updateById(newCtWorker);
                    workerPt.remove(newCtWorker);
                }




                //开始生产产品
                for (int i = 0; i < production; i++) {
                    int cashNeedOne = 0; //计算产品的成本
                    for (NewCtGzProducing newCtGzProducing : newCtGzProducings) {
                        int needNum = newCtGzProducing.getCpNum();
                        if(needNum == 0){ continue;}

                        List<NewCtKcMaterial> materialList = kcMaterialMap.get(newCtGzProducing.getCpMid());
                        for (NewCtKcMaterial material : materialList) {
//                            if(material.getImNum() == 0 ){
//                                continue;
//                            }
                            if(material.getImNum() - needNum > 0){
                                //计算成本，更新材料剩余数量
                                cashNeedOne = cashNeedOne + material.getMaterialPrice() * needNum;
                                material.setImNum(material.getImNum() - needNum);
                                needNum = 0;
                                newCtKcMaterialMapper.updateById(material);
                                break;
                            }else{
                                //计算金额，删除数据（改为更新为0），结束当前循环
                                cashNeedOne = cashNeedOne + material.getMaterialPrice() * material.getImNum();
                                needNum = needNum - material.getImNum();
                                material.setImNum(0);
                                newCtKcMaterialMapper.deleteById(material);
                            }
                        }
                        if(needNum != 0){//库存不足  直接原价购买
                            NewCtGzMaterial newCtGzMaterial = materialGzMaps.get(newCtGzProducing.getCpMid());
                            int fee = needNum * newCtGzMaterial.getCmBuyFee();
                            cashNeedOne += fee;
                            //产生应付款账期
                            NewCtFee ctFee = new NewCtFee();
                            int paymentDate = DateUtils.addYearAndSeasonTime(param.getCurrentTime(),newCtGzMaterial.getCmPayDate());
                            ctFee.setRFee(fee)
                                    .setType(NewCtFee.TO_PAYMENT)
                                    .setContestId(param.getContestId())
                                    .setStudentId(userId)
                                    .setBorrower("1")
                                    .setLender("供应商")
                                    .setPaymentDate(paymentDate)
                                    .setRRemainDate(newCtGzMaterial.getCmPayDate())
                                    .setRemarks("原材料付款（智能生产）");
                            newCtFeeMapper.insert(ctFee);

                        }
                    }
                    cashNeedOne += newCtGzProduct.getCpDevelopFee();//加工费
                    //生成库存数据
                    NewCtKcProduct newCtKcProduct = new NewCtKcProduct();
                    newCtKcProduct.setContestId(param.getContestId())
                            .setStudentId(userId)
                            .setIpCpId(lineIdAndCpId.getCpId())
                            .setIpNum(1)
                            .setLineId(lineIdAndCpId.getLineId())
                            .setRealCost(cashNeedOne);
                    // 产品入库
                    if(newCtGzProduct.getCpDevelopDate() != null &&  newCtGzProduct.getCpDevelopDate()>0){
                        newCtKcProduct.setIsInventory(NewCtKcProduct.NOT_IN);
                    }else {
                        newCtKcProduct.setIsInventory(NewCtKcProduct.IS_IN);
                    }
                    newCtKcProduct.setInventoryDate(productFinishTime);//入库时间
                    newCtKcProductMapper.insert(newCtKcProduct);
                }
                //插入现金交易记录
                NewCtCashflow ctCashflow = new NewCtCashflow();
                ctCashflow.setStudentId(userId)
                        .setContestId(param.getContestId())
                        .setCAction(CashFlowActionEnum.PRODUCT_PRODUCING.getAction())
                        .setCOut(developFee)
                        .setCIn(0)
                        .setCComment("生产线" + lineIdAndCpId.getLineId().toString()+"生产产品"+newCtGzProduct.getCpName())
                        .setCDate(param.getCurrentTime())
                        .setCSurplus(remain);
                newCtCashflowService.save(ctCashflow);
                //更新生产线
                if(newCtGzProduct.getCpDevelopDate() != null &&  newCtGzProduct.getCpDevelopDate()>0){
                    ctLine.setStatus(NewCtLine.ON_PRODUCE);
                }else {
                    ctLine.setStatus(NewCtLine.ON_SPACE);
                }
                ctLine.setPlProductAddDate(param.getCurrentTime())
                        .setPlProductingDate(productFinishTime)
                        .setRealProduction(production)
                        .setPlCpid(lineIdAndCpId.getCpId())
                        .setClassesId(param.getClassesId());
                newCtLineMapper.updateById(ctLine);
            }
        }
        return R.success();
    }

}
