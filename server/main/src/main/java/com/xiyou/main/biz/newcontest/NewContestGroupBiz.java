package com.xiyou.main.biz.newcontest;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.xiyou.common.enums.CodeEnum;
import com.xiyou.common.utils.R;
import com.xiyou.main.entity.newcontest.NewContestGroup;
import com.xiyou.main.service.newcontest.NewContestGroupService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @program: multi-module
 * @description:
 * @author: tangcan
 * @create: 2019-07-09 12:22
 **/
@Service
public class NewContestGroupBiz {
    @Autowired
    private NewContestGroupService newContestGroupService;

    public R list() {
        Map<String, Object> returnMap = new HashMap<>();
        returnMap.put("list", newContestGroupService.list());
        return R.success(returnMap);
    }

    public R add(NewContestGroup contestGroup) {
        QueryWrapper<NewContestGroup> wrapper = new QueryWrapper<>();
        wrapper.eq("group_name", contestGroup.getGroupName());
        if (newContestGroupService.getOne(wrapper) != null) {
            return R.error(CodeEnum.PARAM_ERROR.getCode(), "该分组已存在");
        }
        newContestGroupService.insert(contestGroup);
        NewContestGroup group = newContestGroupService.getOne(wrapper);
        Map<String, Object> returnMap = new HashMap<>();
        returnMap.put("id", group.getId());
        return R.success(returnMap);
    }

    public R update(NewContestGroup contestGroup) {
        if (newContestGroupService.checkNameExist(contestGroup)) {
            return R.error(CodeEnum.PARAM_ERROR.getCode(), "该分组已存在");
        }
        newContestGroupService.updateById(contestGroup);
        return R.success();
    }

    public R delete(Integer[] ids) {
        newContestGroupService.removeByIds(Arrays.asList(ids));
        return R.success();
    }

    public R listByteacher(Integer teacherId) {
        List<NewContestGroup> list = newContestGroupService.getListByTeacher(teacherId);
        return R.success().put("list", list);
    }
}
