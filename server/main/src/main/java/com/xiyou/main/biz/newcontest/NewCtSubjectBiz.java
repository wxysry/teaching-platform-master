package com.xiyou.main.biz.newcontest;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.xiyou.common.enums.CodeEnum;
import com.xiyou.common.utils.R;
import com.xiyou.main.dao.newcontest.NewCtSubjectMapper;
import com.xiyou.main.entity.newcontest.NewCtSubject;
import com.xiyou.main.params.contest.SubjectParam;
import com.xiyou.main.service.exam.ResourcesService;
import com.xiyou.main.service.newcontest.NewCtSubjectService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @program: multi-module
 * @description:
 * @author: tangcan
 * @create: 2019-08-25 13:01
 **/
@Service
public class NewCtSubjectBiz {
    @Autowired
    private NewCtSubjectService newCtSubjectService;
    @Autowired
    private NewCtSubjectMapper newCtSubjectMapper;
    @Autowired
    private ResourcesService resourcesService;

    public R listByAdmin(SubjectParam subjectParam) {
        Page<NewCtSubject> page = newCtSubjectService.listByAdmin(subjectParam);
        Map<String, Object> returnMap = new HashMap<>();
        returnMap.put("list", page.getRecords());
        returnMap.put("total", page.getTotal());
        return R.success(returnMap);
    }

    public R listByTeacher(Integer groupId) {
        List<NewCtSubject> list = newCtSubjectService.listByTeacher(groupId);
        return R.success().put("list", list);
    }

    public R add(NewCtSubject newCtSubject) {
        if (newCtSubjectMapper.getBySubjectNumber(newCtSubject.getSubjectNumber()) != null) {
            return R.error(CodeEnum.PARAM_ERROR.getCode(), "该题库号已存在");
        }
        if (newCtSubject.getSpyAttachment() != null) {
            newCtSubject.setSpyAttachment(newCtSubject.getSpyAttachment().replaceAll(" ", ""));
        }
        if (newCtSubject.getRuleAttachment() != null) {
            newCtSubject.setRuleAttachment(newCtSubject.getRuleAttachment().replaceAll(" ", ""));
        }
        if (StringUtils.isNotBlank(newCtSubject.getRuleAttachment())) {
            if (!resourcesService.check(newCtSubject.getRuleAttachment())) {
                return R.error(CodeEnum.PARAM_ERROR.getCode(), "该规则预测详单不存在");
            }
        } else {
            newCtSubject.setRuleAttachment(null);
        }

        newCtSubjectService.save(newCtSubject);
        return R.success();
    }

    public R update(NewCtSubject newCtSubject) {
        if (newCtSubject.getSubjectId() == null) {
            return R.error(CodeEnum.PARAM_ERROR.getCode(), "请选择题库");
        }
        NewCtSubject subject = newCtSubjectMapper.getBySubjectNumber(newCtSubject.getSubjectNumber());
        if (subject != null && !subject.getSubjectId().equals(newCtSubject.getSubjectId())) {
            return R.error(CodeEnum.PARAM_ERROR.getCode(), "该题库号已存在");
        } else if (subject == null) {
            newCtSubject.setUpload(0);
        }
        if (newCtSubject.getSpyAttachment() != null) {
            newCtSubject.setSpyAttachment(newCtSubject.getSpyAttachment().replaceAll(" ", ""));
        }
        if (newCtSubject.getRuleAttachment() != null) {
            newCtSubject.setRuleAttachment(newCtSubject.getRuleAttachment().replaceAll(" ", ""));
        }
        if (StringUtils.isNotBlank(newCtSubject.getRuleAttachment())) {
            if (!resourcesService.check(newCtSubject.getRuleAttachment())) {
                return R.error(CodeEnum.PARAM_ERROR.getCode(), "该规则预测详单不存在");
            }
        } else {
            newCtSubject.setRuleAttachment(null);
        }
        newCtSubjectMapper.update(newCtSubject);
        return R.success();
    }

    public R get(Integer subjectId) {
        Map<String, Object> returnMap = new HashMap<>();
        returnMap.put("info", newCtSubjectMapper.getById(subjectId));
        return R.success(returnMap);
    }

    public R delete(Integer subjectId) {
        NewCtSubject newCtSubject = newCtSubjectMapper.getById(subjectId);
        if (newCtSubject == null) {
            return R.success();
        }
        newCtSubjectService.removeById(subjectId);
        // 删除规则表中的数据
        newCtSubjectMapper.removeGz(newCtSubject.getSubjectNumber());
        return R.success();
    }

    public R cascadeByTeacher(Integer teacherId) {
        return R.success().put("list", newCtSubjectMapper.getTeacherSubject(teacherId));
    }
}
