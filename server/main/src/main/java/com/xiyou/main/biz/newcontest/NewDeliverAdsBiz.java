package com.xiyou.main.biz.newcontest;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.xiyou.common.enums.CodeEnum;
import com.xiyou.common.utils.R;
import com.xiyou.main.entity.exam.SysUser;
import com.xiyou.main.entity.newcontest.NewCtCashflow;
import com.xiyou.main.entity.newcontest.NewCtMnAd;
import com.xiyou.main.enums.ErrorEnum;
import com.xiyou.main.service.exam.SysUserService;
import com.xiyou.main.service.newcontest.NewCtCashflowService;
import com.xiyou.main.service.newcontest.NewCtMnAdService;
import com.xiyou.main.service.newcontest.NewCtYfMarketService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @program: multi-module
 * @description:
 * @author: tangcan
 * @create: 2019-07-25 15:03
 **/
@Service
@Transactional(rollbackFor = Exception.class)
public class NewDeliverAdsBiz {
    @Autowired
    private NewCtYfMarketService newCtYfMarketService;
    @Autowired
    private NewCtMnAdService newCtMnAdService;
    @Autowired
    private NewCtCashflowService newCtCashflowService;

    @Autowired
    private SysUserService sysUserService;

    @Autowired
    private NewActionBiz newActionBiz;


    //已研发完成的市场列表
    public R finishYfMarket(Integer studentId, Integer contestId) {
        List<String> hadYfFinishMarketList = newCtYfMarketService.getHadYfFinishMarkets(studentId, contestId);
        Map<String, Object> returnMap = new HashMap<>();
        returnMap.put("marketList", hadYfFinishMarketList);
        return R.success(returnMap);
    }


    public R deliverAds(Integer studentId, NewCtMnAd mnAd) {
        if (mnAd.getStudentId() == null) {
            mnAd.setStudentId(studentId);
        }
        int now = mnAd.getYear();
        //年
        if (mnAd.getYear() > 10) {
            mnAd.setYear(mnAd.getYear() / 10);
        }

        // 所有填写金额之和
        double cashSum = mnAd.calSum();
        // 现金
        int cash = newCtCashflowService.getCash(mnAd.getStudentId(), mnAd.getContestId());


        // 如果大于，则报错【现金不足】
        if("Y".equals(mnAd.getIsSubmit())){
            if (cashSum > cash ) {
                return R.error(CodeEnum.OTHER_ERROR.getCode(), "现金不足");
            }
        }


        // 设置组号为学生姓名,记录写入mn_ad
        SysUser sysUser = sysUserService.getById(studentId);
        mnAd.setGroupNum(sysUser.getAccount());

        List<NewCtMnAd> newCtMnAdList = newCtMnAdService.list(new LambdaQueryWrapper<NewCtMnAd>()
                .eq(NewCtMnAd::getContestId,mnAd.getContestId())
                .eq(NewCtMnAd::getStudentId,studentId)
                .eq(NewCtMnAd::getYear,mnAd.getYear())
                .eq(NewCtMnAd::getQuarterly,mnAd.getQuarterly())
                .eq(NewCtMnAd::getGroupNum,mnAd.getGroupNum())
                //本年本季已投放广告
                .eq(NewCtMnAd::getIsSubmit,"Y")
        );
        if(newCtMnAdList!=null && newCtMnAdList.size()>0){
            //本季度已投放广告不可重复投放
            return ErrorEnum.YTF_GG.getR();
        }

        //如果系统中已有则保存，否则走更新逻辑
        if(StringUtils.isEmpty(mnAd.getId())){
            newCtMnAdService.save(mnAd);
        }else{
            newCtMnAdService.updateById(mnAd);
        }

        //已提交则，生成现金流量表
        if("Y".equals(mnAd.getIsSubmit())){
            // Cashflow表写入ID，支付广告费，0，所有填写金额之和，现金+流入-流出，投放广告费，当前时间
            newCtCashflowService.save(new NewCtCashflow().setStudentId(studentId)
                    .setContestId(mnAd.getContestId())
                    .setCAction("支付广告费")
                    .setCIn(0)
                    .setCOut((int) cashSum)
                    .setCSurplus(cash - (int) cashSum)
                    .setCComment("投放广告费" + (int) cashSum + "元")
                    .setCDate(now));
            //
            //计算四张报表
            Integer bsTotalEquity = newActionBiz.calBsTotalEquity(studentId,mnAd.getContestId(),now);
            // 所有者权益为负后，自动结束比赛
            if (bsTotalEquity < 0) {
                return R.error(7000, "当前所有者权益为负，你已破产，模拟竞赛结束");
            }
        }

        return R.success();
    }


    /**
     * 获取当前学生本季度投放的广告
     * @param contestId
     * @param userId
     * @param currentTime
     * @return
     */
    public R getFillAd(Integer contestId, Integer userId, Integer currentTime) {

        SysUser sysUser = sysUserService.getById(userId);
        NewCtMnAd ctMnAd = newCtMnAdService.getOne(new LambdaQueryWrapper<NewCtMnAd>()
                .eq(NewCtMnAd::getStudentId,userId)
                .eq(NewCtMnAd::getContestId,contestId)
                .eq(NewCtMnAd::getYear,currentTime/10)
                .eq(NewCtMnAd::getQuarterly,currentTime%10)
                .eq(NewCtMnAd::getGroupNum, sysUser.getAccount()) //组号
        );
        return R.success().put("data",ctMnAd);
    }
}
