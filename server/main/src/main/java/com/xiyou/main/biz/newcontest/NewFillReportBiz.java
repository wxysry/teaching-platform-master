package com.xiyou.main.biz.newcontest;

import com.xiyou.common.utils.R;
import com.xiyou.main.entity.contest.ContestStudent;
import com.xiyou.main.entity.contest.CtBalance;
import com.xiyou.main.entity.contest.CtCharges;
import com.xiyou.main.entity.contest.CtProfitChart;
import com.xiyou.main.entity.gbcontest.*;
import com.xiyou.main.entity.newcontest.*;
import com.xiyou.main.params.newcontest.NewFillReportParam;
import com.xiyou.main.service.contest.ContestStudentService;
import com.xiyou.main.service.newcontest.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import java.util.HashMap;
import java.util.Map;

/**
 * @program: multi-module
 * @description:
 * @author: wangxingyu
 * @create: 2023-06-28 21:42
 **/
@Service
@Transactional(rollbackFor = Exception.class)
public class NewFillReportBiz {

    @Autowired
    private NewCtProfitChartService newCtProfitChartService;

    @Autowired
    private NewCtChargesService newCtChargesService;

    @Autowired
    private NewCtBalanceService newCtBalanceService;

    @Autowired
    private NewCtFinancialTargetService newCtFinancialTargetService;

    @Autowired
    private NewContestStudentService newContestStudentService;


    /**
     * 获取暂存数据
     * @param studentId
     * @param contestId
     * @param year
     * @return
     */
    public R getTemp(Integer studentId, Integer contestId, Integer year) {
        NewCtCharges charges=newCtChargesService.getTemp(studentId, contestId, year);
        NewCtProfitChart profitChart=newCtProfitChartService.getTemp(studentId, contestId, year);
        NewCtBalance balance = newCtBalanceService.getTemp(studentId, contestId, year);
        NewCtFinancialTarget financialTarget= newCtFinancialTargetService.getTemp(studentId,contestId,year);
        Map<String, Object> list = new HashMap<>();
        list.put("charges",charges);
        list.put("profitChart",profitChart);
        list.put("balance",balance);
        list.put("financialTarget",financialTarget);
        return R.success(list);

    }


    /**
     * 获取暂存数据
     * @param studentId
     * @param contestId
     * @param year
     * @return
     */
    public R getAutoFill(Integer studentId, Integer contestId, Integer year) {
        NewCtCharges charges=newCtChargesService.getSys(studentId, contestId, year);
        NewCtProfitChart profitChart=newCtProfitChartService.getSys(new NewCtProfitChart().setStudentId(studentId).setContestId(contestId).setPcDate(year));
        NewCtBalance balance = newCtBalanceService.get(new NewCtBalance().setStudentId(studentId).setContestId(contestId).setBsYear(year));
        NewCtFinancialTarget financialTarget= newCtFinancialTargetService.get(new NewCtFinancialTarget().setStudentId(studentId).setContestId(contestId).setFtYear(year));
        Map<String, Object> list = new HashMap<>();
        list.put("charges",charges);
        list.put("profitChart",profitChart);
        list.put("balance",balance);
        list.put("financialTarget",financialTarget);
        return R.success(list);

    }

    public R batchFill(Integer userId, NewFillReportParam newFillReportParam) {
        Boolean fillCharges = addCharges(userId,newFillReportParam.getCharges());
        Boolean fillProfitChart = addProfitChart(userId, newFillReportParam.getProfitChart());
        Boolean fillBalance = addBalance(userId,newFillReportParam.getBalance());
        Boolean fillFinancialTarget = addFinancialTarget(userId,newFillReportParam.getFinancialTarget());

        NewCtCharges charges=newFillReportParam.getCharges();
        //判断是否暂存
        if("N".equals(charges.getIsSubmit())){
            return R.success("暂存成功");
        }
        if(fillCharges&&fillProfitChart&&fillBalance&&fillFinancialTarget){
            return R.success("报表正确");
        }else{
            int year = charges.getCDate() >= 10 ? (charges.getCDate() / 10) : charges.getCDate();
            NewContestStudent contestStudent = newContestStudentService.get(charges.getContestId(), userId);
            newContestStudentService.updateErrorReportYear(contestStudent, year);
            return R.success("报表错误");
        }
    }


    /**
     * 填写综合费用表
     * @param studentId
     * @param charges
     * @return
     */
    public boolean addCharges(Integer studentId, NewCtCharges charges) {
        if (charges.getStudentId() == null) {
            charges.setStudentId(studentId);
        }
        charges.setBsIsxt(0);
        if (charges.getCDate() > 10) {
            charges.setCDate(charges.getCDate() / 10);
        }

        if(StringUtils.isEmpty(charges.getChargesId())){
            newCtChargesService.save(charges);
        }else{
            newCtChargesService.updateById(charges);
        }

        //判断是暂存还是提交
        if("Y".equals(charges.getIsSubmit())){
            boolean correct = true;
            // 同时判断这三张表当年填入0和1的记录是否相同，如果不同则提示报表错误，否则提示报表正确。
            NewCtCharges sysCharges = newCtChargesService.getSys(studentId, charges.getContestId(), charges.getCDate());
            if (sysCharges == null
                    || !charges.getCOverhaul().equals(sysCharges.getCOverhaul())
                    || !charges.getCAd().equals(sysCharges.getCAd())
                    || !charges.getCMaintenance().equals(sysCharges.getCMaintenance())
                    || !charges.getCTransfer().equals(sysCharges.getCTransfer())
                    || !charges.getCDevelopMarket().equals(sysCharges.getCDevelopMarket())
                    || !charges.getCDevelopIso().equals(sysCharges.getCDevelopIso())
                    || !charges.getCDevelopProduct().equals(sysCharges.getCDevelopProduct())
                    || !charges.getCInformation().equals(sysCharges.getCInformation())
                    || !charges.getCHr().equals(sysCharges.getCHr())
                    || !charges.getCDigitalization().equals(sysCharges.getCDigitalization())
                    || !charges.getCTotal().equals(sysCharges.getCTotal())
            ) {
                correct = false;
            }
            if (correct) {
                return true;
            }
            return false;
        }else{
            return true;
        }
    }


    /**
     * 填写利润表
     * @param studentId
     * @param profitChart
     * @return
     */
    public boolean addProfitChart(Integer studentId, NewCtProfitChart profitChart) {
        if (profitChart.getStudentId() == null) {
            profitChart.setStudentId(studentId);
        }
        profitChart.setBsIsxt(0);
        if (profitChart.getPcDate() > 10) {
            profitChart.setPcDate(profitChart.getPcDate() / 10);
        }
        if(StringUtils.isEmpty(profitChart.getProfitChartId())){
            newCtProfitChartService.save(profitChart);
        }else{
            newCtProfitChartService.updateById(profitChart);
        }

        if("Y".equals(profitChart.getIsSubmit())){
            boolean correct = true;
            // 同时判断这三张表当年填入0和1的记录是否相同，如果不同则提示报表错误，否则提示报表正确。
            NewCtProfitChart sysProfitChart = newCtProfitChartService.getSys(profitChart);
            if (sysProfitChart == null
                    || !profitChart.getPcSales().equals(sysProfitChart.getPcSales())
                    || !profitChart.getPcDirectCost().equals(sysProfitChart.getPcDirectCost())
                    || !profitChart.getPcGoodsProfit().equals(sysProfitChart.getPcGoodsProfit())
                    || !profitChart.getPcTotal().equals(sysProfitChart.getPcTotal())
                    || !profitChart.getPcProfitBeforeDep().equals(sysProfitChart.getPcProfitBeforeDep())
                    || !profitChart.getPcDep().equals(sysProfitChart.getPcDep())
                    || !profitChart.getPcProfitBeforeInterests().equals(sysProfitChart.getPcProfitBeforeInterests())
                    || !profitChart.getPcFinanceFee().equals(sysProfitChart.getPcFinanceFee())
                    || !profitChart.getPcNonOperating().equals(sysProfitChart.getPcNonOperating())
                    || !profitChart.getPcProfitBeforeTax().equals(sysProfitChart.getPcProfitBeforeTax())
                    || !profitChart.getPcTax().equals(sysProfitChart.getPcTax())
                    || !profitChart.getPcAnnualNetProfit().equals(sysProfitChart.getPcAnnualNetProfit())) {
                correct = false;
            }
            if (correct) {
                return true;
            }
            return false;
        }else{
            return true;
        }
    }


    /**
     * 填写资产负债表
     * @param studentId
     * @param balance
     * @return
     */
    public boolean addBalance(Integer studentId, NewCtBalance balance) {
        if (balance.getStudentId() == null) {
            balance.setStudentId(studentId);
        }
        balance.setBsIsxt(0);
        if (balance.getBsYear() > 10) {
            balance.setBsYear(balance.getBsYear() / 10);
        }
        if(StringUtils.isEmpty(balance.getBalanceId())){
            newCtBalanceService.save(balance);
        }else{
            newCtBalanceService.updateById(balance);
        }

        if("Y".equals(balance.getIsSubmit())){
            boolean correct = true;
            // 同时判断这三张表当年填入0和1的记录是否相同，如果不同则提示报表错误，否则提示报表正确。
            NewCtBalance sysBalance = newCtBalanceService.get(balance);
            if (sysBalance == null
                    || !balance.getBsCash().equals(sysBalance.getBsCash())
                    || !balance.getBsReceivable().equals(sysBalance.getBsReceivable())
                    || !balance.getBsProductInProcess().equals(sysBalance.getBsProductInProcess())
                    || !balance.getBsProduct().equals(sysBalance.getBsProduct())
                    || !balance.getBsMaterial().equals(sysBalance.getBsMaterial())
                    || !balance.getBsTotalCurrentAsset().equals(sysBalance.getBsTotalCurrentAsset())
                    || !balance.getBsEquipment().equals(sysBalance.getBsEquipment())
                    || !balance.getBsProjectOnConstruction().equals(sysBalance.getBsProjectOnConstruction())
                    || !balance.getBsTotalFixedAsset().equals(sysBalance.getBsTotalFixedAsset())
                    || !balance.getBsTotalAsset().equals(sysBalance.getBsTotalAsset())
                    || !balance.getBsLongLoan().equals(sysBalance.getBsLongLoan())
                    || !balance.getBsShortLoan().equals(sysBalance.getBsShortLoan())
                    || !balance.getBsOtherPay().equals(sysBalance.getBsOtherPay())
                    || !balance.getBsTax().equals(sysBalance.getBsTax())
                    || !balance.getBsTotalLiability().equals(sysBalance.getBsTotalLiability())
                    || !balance.getBsEquity().equals(sysBalance.getBsEquity())
                    || !balance.getBsRetainedEarning().equals(sysBalance.getBsRetainedEarning())
                    || !balance.getBsAnnualNetProfit().equals(sysBalance.getBsAnnualNetProfit())
                    || !balance.getBsTotalEquity().equals(sysBalance.getBsTotalEquity())
                    || !balance.getBsTotal().equals(sysBalance.getBsTotal())) {
                correct = false;
            }
            if (correct) {
                return true;
            }
            return false;
        }else{
            return true;
        }
    }


    /**
     * 填写财务指标
     * @param studentId
     * @param ctFinancialTarget
     * @return
     */
    public boolean addFinancialTarget(Integer studentId, NewCtFinancialTarget ctFinancialTarget) {
        if (ctFinancialTarget.getStudentId() == null) {
            ctFinancialTarget.setStudentId(studentId);
        }
        ctFinancialTarget.setFtIsxt(0);
        if (ctFinancialTarget.getFtYear() > 10) {
            ctFinancialTarget.setFtYear(ctFinancialTarget.getFtYear() / 10);
        }
        if(StringUtils.isEmpty(ctFinancialTarget.getFtId())){
            newCtFinancialTargetService.save(ctFinancialTarget);
        }else{
            newCtFinancialTargetService.updateById(ctFinancialTarget);
        }

        if("Y".equals(ctFinancialTarget.getIsSubmit())){

            boolean correct = true;
            // 同时判断这三张表当年填入0和1的记录是否相同，如果不同则提示报表错误，否则提示报表正确。
            NewCtFinancialTarget sysTarget = newCtFinancialTargetService.get(ctFinancialTarget);
            if (sysTarget == null
                    || ctFinancialTarget.getFtCurrentRate().compareTo(sysTarget.getFtCurrentRate())!=0
                    || ctFinancialTarget.getFtQuickRate().compareTo(sysTarget.getFtQuickRate())!=0
                    || ctFinancialTarget.getFtDebtRate().compareTo(sysTarget.getFtDebtRate())!=0
                    || ctFinancialTarget.getFtEquityRate().compareTo(sysTarget.getFtEquityRate())!=0
                    || ctFinancialTarget.getFtNetProfitRate().compareTo(sysTarget.getFtNetProfitRate())!=0
                    || ctFinancialTarget.getFtCostExpenseRate().compareTo(sysTarget.getFtCostExpenseRate())!=0
                    || ctFinancialTarget.getFtReturnAssetsRate().compareTo(sysTarget.getFtReturnAssetsRate())!=0
                    || ctFinancialTarget.getFtReturnEquityRate().compareTo(sysTarget.getFtReturnEquityRate())!=0
                    || ctFinancialTarget.getFtRevenueGrowthRate().compareTo(sysTarget.getFtRevenueGrowthRate())!=0
                    || ctFinancialTarget.getFtAppreciationRate().compareTo(sysTarget.getFtAppreciationRate())!=0
                    || ctFinancialTarget.getFtTotalGrowthRate().compareTo(sysTarget.getFtTotalGrowthRate())!=0
                    || ctFinancialTarget.getFtInventoryRate().compareTo(sysTarget.getFtInventoryRate())!=0
                    || ctFinancialTarget.getFtInventoryDays().compareTo(sysTarget.getFtInventoryDays())!=0
                    || ctFinancialTarget.getFtReceivableRate().compareTo(sysTarget.getFtReceivableRate())!=0
                    || ctFinancialTarget.getFtReceivableDays().compareTo(sysTarget.getFtReceivableDays())!=0
                    || ctFinancialTarget.getFtCashPeriod().compareTo(sysTarget.getFtCashPeriod())!=0
            ) {
                correct = false;
            }

            if (correct) {
                return true;
            }
            return false;
        }else{
            return false;
        }
    }


}
