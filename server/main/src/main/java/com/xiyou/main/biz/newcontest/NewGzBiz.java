package com.xiyou.main.biz.newcontest;

import com.alibaba.excel.metadata.Sheet;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.xiyou.common.enums.CodeEnum;
import com.xiyou.common.utils.R;
import com.xiyou.main.dao.newcontest.*;
import com.xiyou.main.entity.newcontest.*;
import com.xiyou.main.pojo.newcontest.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.ObjectUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.*;

import static com.xiyou.common.office.utils.EasyExcelUtil.readExcel;

/**
 * @program: multi-module
 * @description:
 * @author: tangcan
 * @create: 2019-08-27 18:18
 **/
@Service
public class NewGzBiz {
    private Logger log = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private NewCtSubjectMapper newCtSubjectMapper;

    @Autowired
    private NewGzExcelMapper newGzExcelMapper;

    @Autowired
    private NewCtGzCsMapper newCtGzCsMapper;
    @Autowired
    private NewCtGzAdMapper newCtGzAdMapper;
    @Autowired
    private NewCtGzIsoMapper newCtGzIsoMapper;
    @Autowired
    private NewCtGzOrderMapper newCtGzOrderMapper;
    @Autowired
    private NewCtGzMarketMapper newCtGzMarketMapper;
    @Autowired
    private NewCtGzMaterialMapper newCtGzMaterialMapper;
    @Autowired
    private NewCtGzProductMapper newCtGzProductMapper;
    @Autowired
    private NewCtGzProducingMapper newCtGzProducingMapper;
    @Autowired
    private NewCtGzProductLineMapper newCtGzProductLineMapper;
    @Autowired
    private NewCtGzProductDesignMapper newCtGzProductDesignMapper;
    @Autowired
    private NewCtGzWorkerRecruitMapper newCtGzWorkerRecruitMapper;
    @Autowired
    private NewCtGzWorkerTrainMapper newCtGzWorkerTrainMapper;
    @Autowired
    private NewCtGzLoanMapper newCtGzLoanMapper;
    @Autowired
    private NewCtGzDiscountMapper newCtGzDiscountMapper;
    @Autowired
    private NewCtGzClassesMapper newCtGzClassesMapper;
    @Autowired
    private NewCtGzWorkerIncentiveMapper newCtGzWorkerIncentiveMapper;
    @Autowired
    private NewCtGzNumMapper newCtGzNumMapper;



    @Transactional
    public R upload(MultipartFile multipartFile, Integer subjectNumber) {
        NewCtSubject newCtSubject = newCtSubjectMapper.getBySubjectNumber(subjectNumber);
        if (newCtSubject == null) {
            return R.error(CodeEnum.OTHER_ERROR.getCode(), "请选择要上传的题库");
        }
        InputStream inputStream;
        /*
        由于不能直接重复使用inputstream
        因此先做如下处理
         */
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        try {
            inputStream = multipartFile.getInputStream();
            byte[] buffer = new byte[1024];
            int len;
            while ((len = inputStream.read(buffer)) > -1) {
                baos.write(buffer, 0, len);
            }
            baos.flush();
        } catch (IOException e) {
            log.error(e.getMessage());
            return R.error(CodeEnum.FILE_UPLOAD_FAIL.getCode(), e.getMessage());
        }

        Sheet sheet;
        /*
        规则gz_cs
         */
        sheet = new Sheet(1, 2, NewCtGzCsModel.class);
        List<NewCtGzCsModel> newCtGzCsModelList;
        try {
            newCtGzCsModelList = readExcel(new ByteArrayInputStream(baos.toByteArray()), sheet);
        } catch (Exception e) {
            return R.error(CodeEnum.FILE_UPLOAD_FAIL.getCode(), "规则gz_cs表" + e.getMessage());
        }
        if (newCtGzCsModelList == null || newCtGzCsModelList.size() == 0) {
            return R.error(CodeEnum.FILE_UPLOAD_FAIL.getCode(), "上传失败！规则gz_cs表无数据");
        }
        for (NewCtGzCsModel newCtGzCsModel : newCtGzCsModelList) {
            if (newCtGzCsModel.getSubjectNumber() == null) {
                return R.error(CodeEnum.FILE_UPLOAD_FAIL.getCode(), "规则gz_cs表存在多余空行");
            }
//            if (!subjectNumber.equals(newCtGzCsModel.getSubjectNumber())) {
//                return R.error(CodeEnum.FILE_UPLOAD_FAIL.getCode(), "规则gz_cs表题库号应该是：" + subjectNumber);
//            }
            newCtGzCsModel.setSubjectNumber(subjectNumber);
        }

        /*
        初始广告gz_ad
         */
        sheet = new Sheet(2, 1, NewCtGzAdModel.class);
        List<NewCtGzAdModel> newCtGzAdModelList;
        try {
            newCtGzAdModelList = readExcel(new ByteArrayInputStream(baos.toByteArray()), sheet);
        } catch (Exception e) {
            return R.error(CodeEnum.FILE_UPLOAD_FAIL.getCode(), "初始广告gz_ad表" + e.getMessage());
        }
        if (newCtGzAdModelList == null || newCtGzAdModelList.size() == 0) {
            return R.error(CodeEnum.FILE_UPLOAD_FAIL.getCode(), "上传失败！初始广告gz_ad表无数据");
        }
        for (NewCtGzAdModel newCtGzAdModel : newCtGzAdModelList) {
            if (newCtGzAdModel.getSubjectNum() == null) {
                return R.error(CodeEnum.FILE_UPLOAD_FAIL.getCode(), "初始广告gz_ad表存在多余空行");
            }
//            if (!subjectNumber.equals(newCtGzAdModel.getSubjectNum())) {
//                return R.error(CodeEnum.FILE_UPLOAD_FAIL.getCode(), "初始广告gz_ad表题库号应该是：" + subjectNumber);
//            }
            newCtGzAdModel.setSubjectNum(subjectNumber);
        }

        /*
        ISO规则表gz_ISO
         */
        sheet = new Sheet(3, 2, NewCtGzIsoModel.class);
        List<NewCtGzIsoModel> newCtGzIsoModelList;
        try {
            newCtGzIsoModelList = readExcel(new ByteArrayInputStream(baos.toByteArray()), sheet);
        } catch (Exception e) {
            return R.error(CodeEnum.FILE_UPLOAD_FAIL.getCode(), "ISO规则表gz_ISO表" + e.getMessage());
        }
        if (newCtGzIsoModelList == null || newCtGzIsoModelList.size() == 0) {
            return R.error(CodeEnum.FILE_UPLOAD_FAIL.getCode(), "上传失败！ISO规则表gz_ISO表无数据");
        }
        for (NewCtGzIsoModel newCtGzIsoModel : newCtGzIsoModelList) {
            if (newCtGzIsoModel.getSubjectNumber() == null) {
                return R.error(CodeEnum.FILE_UPLOAD_FAIL.getCode(), "ISO规则表gz_ISO表存在多余空行");
            }
//            if (!subjectNumber.equals(newCtGzIsoModel.getSubjectNumber())) {
//                return R.error(CodeEnum.FILE_UPLOAD_FAIL.getCode(), "ISO规则表gz_ISO表题库号应该是：" + subjectNumber);
//            }
            newCtGzIsoModel.setSubjectNumber(subjectNumber);
        }

        /*
        市场订单gz_order
         */
        sheet = new Sheet(4, 1, NewCtGzOrderModel.class);
        List<NewCtGzOrderModel> newCtGzOrderModelList;
        try {
            newCtGzOrderModelList = readExcel(new ByteArrayInputStream(baos.toByteArray()), sheet);
        } catch (Exception e) {
            return R.error(CodeEnum.FILE_UPLOAD_FAIL.getCode(), "市场订单gz_order表" + e.getMessage());
        }
        if (newCtGzOrderModelList == null || newCtGzOrderModelList.size() == 0) {
            return R.error(CodeEnum.FILE_UPLOAD_FAIL.getCode(), "上传失败！市场订单gz_order表无数据");
        }
        for (NewCtGzOrderModel newCtGzOrderModel : newCtGzOrderModelList) {
            if (newCtGzOrderModel.getSubjectNumber() == null) {
                return R.error(CodeEnum.FILE_UPLOAD_FAIL.getCode(), "市场订单gz_order表存在多余空行");
            }
            if (ObjectUtils.isEmpty(newCtGzOrderModel.getCiId())) {
                newCtGzOrderModel.setCiId(null);
            }
//            if (!subjectNumber.equals(newCtGzOrderModel.getSubjectNumber())) {
//                return R.error(CodeEnum.FILE_UPLOAD_FAIL.getCode(), "市场订单gz_order表题库号应该是：" + subjectNumber);
//            }
            newCtGzOrderModel.setSubjectNumber(subjectNumber);
        }

        /*
        市场规则表gz_market
         */
        sheet = new Sheet(5, 2, NewCtGzMarketModel.class);
        List<NewCtGzMarketModel> newCtGzMarketModelList;
        try {
            newCtGzMarketModelList = readExcel(new ByteArrayInputStream(baos.toByteArray()), sheet);
        } catch (Exception e) {
            return R.error(CodeEnum.FILE_UPLOAD_FAIL.getCode(), "市场规则表gz_market表" + e.getMessage());
        }
        if (newCtGzMarketModelList == null || newCtGzMarketModelList.size() == 0) {
            return R.error(CodeEnum.FILE_UPLOAD_FAIL.getCode(), "上传失败！市场规则表gz_market表无数据");
        }
        for (NewCtGzMarketModel newCtGzMarketModel : newCtGzMarketModelList) {
            if (newCtGzMarketModel.getSubjectNumber() == null) {
                return R.error(CodeEnum.FILE_UPLOAD_FAIL.getCode(), "市场规则表gz_market表存在多余空行");
            }
//            if (!subjectNumber.equals(newCtGzMarketModel.getSubjectNumber())) {
//                return R.error(CodeEnum.FILE_UPLOAD_FAIL.getCode(), "市场规则表gz_market表题库号应该是：" + subjectNumber);
//            }
            newCtGzMarketModel.setSubjectNumber(subjectNumber);
        }

        /*
        原料规则表gz_material
         */
        sheet = new Sheet(6, 2, NewCtGzMaterialModel.class);
        List<NewCtGzMaterialModel> newCtGzMaterialModelList;
        try {
            newCtGzMaterialModelList = readExcel(new ByteArrayInputStream(baos.toByteArray()), sheet);
        } catch (Exception e) {
            return R.error(CodeEnum.FILE_UPLOAD_FAIL.getCode(), "原料规则表gz_material表" + e.getMessage());
        }
        if (newCtGzMaterialModelList == null || newCtGzMaterialModelList.size() == 0) {
            return R.error(CodeEnum.FILE_UPLOAD_FAIL.getCode(), "上传失败！原料规则表gz_material表无数据");
        }
        for (NewCtGzMaterialModel newCtGzMaterialModel : newCtGzMaterialModelList) {
            if (newCtGzMaterialModel.getSubjectNumber() == null) {
                return R.error(CodeEnum.FILE_UPLOAD_FAIL.getCode(), "原料规则表gz_material表存在多余空行");
            }
//            if (!subjectNumber.equals(newCtGzMaterialModel.getSubjectNumber())) {
//                return R.error(CodeEnum.FILE_UPLOAD_FAIL.getCode(), "原料规则表gz_material表题库号应该是：" + subjectNumber);
//            }
            newCtGzMaterialModel.setSubjectNumber(subjectNumber);
        }

        /*
        产品规则表gz_product
         */
        sheet = new Sheet(7, 2, NewCtGzProductModel.class);
        List<NewCtGzProductModel> newCtGzProductModelList;
        try {
            newCtGzProductModelList = readExcel(new ByteArrayInputStream(baos.toByteArray()), sheet);
        } catch (Exception e) {
            return R.error(CodeEnum.FILE_UPLOAD_FAIL.getCode(), "产品规则表gz_product表" + e.getMessage());
        }
        if (newCtGzProductModelList == null || newCtGzProductModelList.size() == 0) {
            return R.error(CodeEnum.FILE_UPLOAD_FAIL.getCode(), "上传失败！产品规则表gz_product表无数据");
        }
        for (NewCtGzProductModel newCtGzProductModel : newCtGzProductModelList) {
            if (newCtGzProductModel.getSubjectNumber() == null) {
                return R.error(CodeEnum.FILE_UPLOAD_FAIL.getCode(), "产品规则表gz_product表存在多余空行");
            }
//            if (!subjectNumber.equals(newCtGzProductModel.getSubjectNumber())) {
//                return R.error(CodeEnum.FILE_UPLOAD_FAIL.getCode(), "产品规则表gz_product表题库号应该是：" + subjectNumber);
//            }
            newCtGzProductModel.setSubjectNumber(subjectNumber);
        }


        /*
        生产规则表gz_producing
         */
        sheet = new Sheet(8, 2, NewCtGzProducingModel.class);
        List<NewCtGzProducingModel> newCtGzProducingModelList;
        try {
            newCtGzProducingModelList = readExcel(new ByteArrayInputStream(baos.toByteArray()), sheet);
        } catch (Exception e) {
            return R.error(CodeEnum.FILE_UPLOAD_FAIL.getCode(), "生产规则表gz_producing表" + e.getMessage());
        }
        if (newCtGzProducingModelList == null || newCtGzProducingModelList.size() == 0) {
            return R.error(CodeEnum.FILE_UPLOAD_FAIL.getCode(), "上传失败！生产规则表gz_producing表无数据");
        }
        for (NewCtGzProducingModel newCtGzProducingModel : newCtGzProducingModelList) {
            if (newCtGzProducingModel.getSubjectNumber() == null) {
                return R.error(CodeEnum.FILE_UPLOAD_FAIL.getCode(), "生产规则表gz_producing表存在多余空行");
            }
//            if (!subjectNumber.equals(newCtGzProducingModel.getSubjectNumber())) {
//                return R.error(CodeEnum.FILE_UPLOAD_FAIL.getCode(), "生产规则表gz_producing表题库号应该是：" + subjectNumber);
//            }
            newCtGzProducingModel.setSubjectNumber(subjectNumber);
        }

        /*
        生产线规则表gz_product_line
         */
        sheet = new Sheet(9, 2, NewCtGzProductLineModel.class);
        List<NewCtGzProductLineModel> newCtGzProductLineModelList;
        try {
            newCtGzProductLineModelList = readExcel(new ByteArrayInputStream(baos.toByteArray()), sheet);
        } catch (Exception e) {
            return R.error(CodeEnum.FILE_UPLOAD_FAIL.getCode(), "生产线规则表gz_product_line表" + e.getMessage());
        }
        if (newCtGzProductLineModelList == null || newCtGzProductLineModelList.size() == 0) {
            return R.error(CodeEnum.FILE_UPLOAD_FAIL.getCode(), "上传失败！生产线规则表gz_product_line表无数据");
        }
        for (NewCtGzProductLineModel newCtGzProductLineModel : newCtGzProductLineModelList) {
            if (newCtGzProductLineModel.getSubjectNum() == null) {
                return R.error(CodeEnum.FILE_UPLOAD_FAIL.getCode(), "生产线规则表gz_product_line表存在多余空行");
            }
//            if (!subjectNumber.equals(newCtGzProductLineModel.getSubjectNum())) {
//                return R.error(CodeEnum.FILE_UPLOAD_FAIL.getCode(), "生产线规则表gz_product_line表题库号应该是：" + subjectNumber);
//            }
            newCtGzProductLineModel.setSubjectNum(subjectNumber);
        }




        /*
        产品设计gz_product_design（新增）
         */
        sheet = new Sheet(10, 2, NewCtGzProductDesignModel.class);
        List<NewCtGzProductDesignModel> newCtGzProductDesignModelList;
        try {
            newCtGzProductDesignModelList = readExcel(new ByteArrayInputStream(baos.toByteArray()), sheet);
        } catch (Exception e) {
            return R.error(CodeEnum.FILE_UPLOAD_FAIL.getCode(), "产品设计规则表gz_product_design表" + e.getMessage());
        }
        if (newCtGzProductDesignModelList == null || newCtGzProductDesignModelList.size() == 0) {
            return R.error(CodeEnum.FILE_UPLOAD_FAIL.getCode(), "上传失败！产品设计规则表gz_product_design表无数据");
        }
        for (NewCtGzProductDesignModel newCtGzProductDesignModel : newCtGzProductDesignModelList) {
            if (newCtGzProductDesignModel.getSubjectNum() == null) {
                return R.error(CodeEnum.FILE_UPLOAD_FAIL.getCode(), "产品设计规则表gz_product_design表存在多余空行");
            }
//            if (!subjectNumber.equals(newCtGzProductDesignModel.getSubjectNum())) {
//                return R.error(CodeEnum.FILE_UPLOAD_FAIL.getCode(), "产品设计规则表gz_product_design表题库号应该是：" + subjectNumber);
//            }
            newCtGzProductDesignModel.setSubjectNum(subjectNumber);
        }

        /*
        工人招聘gz_worker_recruit（新增）
         */
        sheet = new Sheet(11, 2, NewCtGzWorkerRecruitModel.class);
        List<NewCtGzWorkerRecruitModel> newCtGzWorkerRecruitModelList;
        try {
            newCtGzWorkerRecruitModelList = readExcel(new ByteArrayInputStream(baos.toByteArray()), sheet);
        } catch (Exception e) {
            return R.error(CodeEnum.FILE_UPLOAD_FAIL.getCode(), "工人招聘规则表gz_worker_recruit表" + e.getMessage());
        }
        if (newCtGzWorkerRecruitModelList == null || newCtGzWorkerRecruitModelList.size() == 0) {
            return R.error(CodeEnum.FILE_UPLOAD_FAIL.getCode(), "上传失败！工人招聘规则表gz_worker_recruit表无数据");
        }
        for (NewCtGzWorkerRecruitModel newCtGzWorkerRecruitModel : newCtGzWorkerRecruitModelList) {
            if (newCtGzWorkerRecruitModel.getSubjectNum() == null) {
                return R.error(CodeEnum.FILE_UPLOAD_FAIL.getCode(), "工人招聘规则表gz_worker_recruit表存在多余空行");
            }
//            if (!subjectNumber.equals(newCtGzWorkerRecruitModel.getSubjectNum())) {
//                return R.error(CodeEnum.FILE_UPLOAD_FAIL.getCode(), "工人招聘规则表gz_worker_recruit表题库号应该是：" + subjectNumber);
//            }
            newCtGzWorkerRecruitModel.setSubjectNum(subjectNumber);
        }



        /*
        工人培训gz_worker_train(新增)
         */
        sheet = new Sheet(12, 2, NewCtGzWorkerTrainModel.class);
        List<NewCtGzWorkerTrainModel> newCtGzWorkerTrainModelList;
        try {
            newCtGzWorkerTrainModelList = readExcel(new ByteArrayInputStream(baos.toByteArray()), sheet);
        } catch (Exception e) {
            return R.error(CodeEnum.FILE_UPLOAD_FAIL.getCode(), "工人培训规则表gz_worker_train表" + e.getMessage());
        }
        if (newCtGzWorkerTrainModelList == null || newCtGzWorkerTrainModelList.size() == 0) {
            return R.error(CodeEnum.FILE_UPLOAD_FAIL.getCode(), "上传失败！工人培训规则表gz_worker_train表无数据");
        }
        for (NewCtGzWorkerTrainModel newCtGzWorkerTrainModel : newCtGzWorkerTrainModelList) {
            if (newCtGzWorkerTrainModel.getSubjectNum() == null) {
                return R.error(CodeEnum.FILE_UPLOAD_FAIL.getCode(), "工人培训规则表gz_worker_train表存在多余空行");
            }
//            if (!subjectNumber.equals(newCtGzWorkerTrainModel.getSubjectNum())) {
//                return R.error(CodeEnum.FILE_UPLOAD_FAIL.getCode(), "工人培训规则表gz_worker_train表题库号应该是：" + subjectNumber);
//            }
            newCtGzWorkerTrainModel.setSubjectNum(subjectNumber);
        }



        /*
        贷款规则gz_loan(新增)
         */
        sheet = new Sheet(13, 2, NewCtGzLoanModel.class);
        List<NewCtGzLoanModel> newCtGzLoanModelList;
        try {
            newCtGzLoanModelList = readExcel(new ByteArrayInputStream(baos.toByteArray()), sheet);
        } catch (Exception e) {
            return R.error(CodeEnum.FILE_UPLOAD_FAIL.getCode(), "贷款规则表gz_loann表" + e.getMessage());
        }
        if (newCtGzLoanModelList == null || newCtGzLoanModelList.size() == 0) {
            return R.error(CodeEnum.FILE_UPLOAD_FAIL.getCode(), "上传失败！贷款规则表gz_loan表无数据");
        }
        for (NewCtGzLoanModel newCtGzLoanModel : newCtGzLoanModelList) {
            if (newCtGzLoanModel.getSubjectNum() == null) {
                return R.error(CodeEnum.FILE_UPLOAD_FAIL.getCode(), "贷款规则表gz_loan表存在多余空行");
            }
//            if (!subjectNumber.equals(newCtGzLoanModel.getSubjectNum())) {
//                return R.error(CodeEnum.FILE_UPLOAD_FAIL.getCode(), "贷款规则表gz_loan表题库号应该是：" + subjectNumber);
//            }
            newCtGzLoanModel.setSubjectNum(subjectNumber);
        }


        /*
        贴现规则gz_discount（新增）
         */
        sheet = new Sheet(14, 2, NewCtGzDiscountModel.class);
        List<NewCtGzDiscountModel> newCtGzDiscountModelList;
        try {
            newCtGzDiscountModelList = readExcel(new ByteArrayInputStream(baos.toByteArray()), sheet);
        } catch (Exception e) {
            return R.error(CodeEnum.FILE_UPLOAD_FAIL.getCode(), "贴现规则表gz_discount表" + e.getMessage());
        }
        if (newCtGzDiscountModelList == null || newCtGzDiscountModelList.size() == 0) {
            return R.error(CodeEnum.FILE_UPLOAD_FAIL.getCode(), "上传失败！贴现规则表gz_discount表无数据");
        }
        for (NewCtGzDiscountModel newCtGzDiscountModel : newCtGzDiscountModelList) {
            if (newCtGzDiscountModel.getSubjectNum() == null) {
                return R.error(CodeEnum.FILE_UPLOAD_FAIL.getCode(), "贴现规则表gz_discount表存在多余空行");
            }
//            if (!subjectNumber.equals(newCtGzDiscountModel.getSubjectNum())) {
//                return R.error(CodeEnum.FILE_UPLOAD_FAIL.getCode(), "贴现规则表gz_discount表题库号应该是：" + subjectNumber);
//            }
            newCtGzDiscountModel.setSubjectNum(subjectNumber);
        }



        /*
        班次规则gz_classes(新增)
         */
        sheet = new Sheet(15, 2, NewCtGzClassesModel.class);
        List<NewCtGzClassesModel> newCtGzClassesModelList;
        try {
            newCtGzClassesModelList = readExcel(new ByteArrayInputStream(baos.toByteArray()), sheet);
        } catch (Exception e) {
            return R.error(CodeEnum.FILE_UPLOAD_FAIL.getCode(), "班次规则表gz_classes表" + e.getMessage());
        }
        if (newCtGzClassesModelList == null || newCtGzClassesModelList.size() == 0) {
            return R.error(CodeEnum.FILE_UPLOAD_FAIL.getCode(), "上传失败！班次规则表gz_classes表无数据");
        }
        for (NewCtGzClassesModel newCtGzClassesModel : newCtGzClassesModelList) {
            if (newCtGzClassesModel.getSubjectNum() == null) {
                return R.error(CodeEnum.FILE_UPLOAD_FAIL.getCode(), "班次规则表gz_classes表存在多余空行");
            }
//            if (!subjectNumber.equals(newCtGzClassesModel.getSubjectNum())) {
//                return R.error(CodeEnum.FILE_UPLOAD_FAIL.getCode(), "班次规则表gz_classes表题库号应该是：" + subjectNumber);
//            }
            newCtGzClassesModel.setSubjectNum(subjectNumber);
        }


        /*
        员工激励gz_worker_incentive(新增)
         */
        sheet = new Sheet(16, 2, NewCtGzWorkerIncentiveModel.class);
        List<NewCtGzWorkerIncentiveModel> newCtGzWorkerIncentiveModelList;
        try {
            newCtGzWorkerIncentiveModelList = readExcel(new ByteArrayInputStream(baos.toByteArray()), sheet);
        } catch (Exception e) {
            return R.error(CodeEnum.FILE_UPLOAD_FAIL.getCode(), "员工激励规则表gz_worker_incentive表" + e.getMessage());
        }
        if (newCtGzWorkerIncentiveModelList == null || newCtGzWorkerIncentiveModelList.size() == 0) {
            return R.error(CodeEnum.FILE_UPLOAD_FAIL.getCode(), "上传失败！员工激励规则表gz_worker_incentive表无数据");
        }
        for (NewCtGzWorkerIncentiveModel newCtGzWorkerIncentiveModel : newCtGzWorkerIncentiveModelList) {
            if (newCtGzWorkerIncentiveModel.getSubjectNum() == null) {
                return R.error(CodeEnum.FILE_UPLOAD_FAIL.getCode(), "员工激励规则表gz_worker_incentive表存在多余空行");
            }
//            if (!subjectNumber.equals(newCtGzWorkerIncentiveModel.getSubjectNum())) {
//                return R.error(CodeEnum.FILE_UPLOAD_FAIL.getCode(), "员工激励规则表gz_worker_incentive表题库号应该是：" + subjectNumber);
//            }
            newCtGzWorkerIncentiveModel.setSubjectNum(subjectNumber);
        }


        /*
        数字化岗位gz_num(新增)
         */
        sheet = new Sheet(17, 2, NewCtGzNumModel.class);
        List<NewCtGzNumModel> newCtGzNumModelList;
        try {
            newCtGzNumModelList = readExcel(new ByteArrayInputStream(baos.toByteArray()), sheet);
        } catch (Exception e) {
            return R.error(CodeEnum.FILE_UPLOAD_FAIL.getCode(), "数字化岗位规则表gz_num表" + e.getMessage());
        }
        if (newCtGzNumModelList == null || newCtGzNumModelList.size() == 0) {
            return R.error(CodeEnum.FILE_UPLOAD_FAIL.getCode(), "上传失败！数字化岗位规则表gz_num表无数据");
        }
        for (NewCtGzNumModel newCtGzNumModel : newCtGzNumModelList) {
            if (newCtGzNumModel.getSubjectNum() == null) {
                return R.error(CodeEnum.FILE_UPLOAD_FAIL.getCode(), "数字化岗位规则表gz_num表存在多余空行");
            }
//            if (!subjectNumber.equals(newCtGzNumModel.getSubjectNum())) {
//                return R.error(CodeEnum.FILE_UPLOAD_FAIL.getCode(), "数字化岗位规则表gz_num表题库号应该是：" + subjectNumber);
//            }
            newCtGzNumModel.setSubjectNum(subjectNumber);
        }



        if (newCtSubject.getUpload() == 1) {
            /*
            先删除之前上传过，但是现在不需要上传的,再存库
             */
            newGzExcelMapper.removeRestCs(newCtGzCsModelList);
            newGzExcelMapper.removeRestAd(newCtGzAdModelList);
            newGzExcelMapper.removeRestIso(newCtGzIsoModelList);
            newGzExcelMapper.removeRestOrder(newCtGzOrderModelList);
            newGzExcelMapper.removeRestMarket(newCtGzMarketModelList);
            newGzExcelMapper.removeRestMaterial(newCtGzMaterialModelList);
            newGzExcelMapper.removeRestProduct(newCtGzProductModelList);
            newGzExcelMapper.removeRestProducing(newCtGzProducingModelList);
            newGzExcelMapper.removeRestProductLine(newCtGzProductLineModelList);

            newGzExcelMapper.removeRestProductDesign(newCtGzProductDesignModelList);
            newGzExcelMapper.removeRestWorkerRecruit(newCtGzWorkerRecruitModelList);
            newGzExcelMapper.removeRestWorkerTrain(newCtGzWorkerTrainModelList);
            newGzExcelMapper.removeRestLoan(newCtGzLoanModelList);
            newGzExcelMapper.removeRestDiscount(newCtGzDiscountModelList);
            newGzExcelMapper.removeRestClasses(newCtGzClassesModelList);
            newGzExcelMapper.removeRestWorkerIncentive(newCtGzWorkerIncentiveModelList);
            newGzExcelMapper.removeRestNum(newCtGzNumModelList);

        }
        /*
        存库
         */
        newGzExcelMapper.insertCs(newCtGzCsModelList);
        newGzExcelMapper.insertAd(newCtGzAdModelList);
        newGzExcelMapper.insertIso(newCtGzIsoModelList);
        newGzExcelMapper.insertOrder(newCtGzOrderModelList);
        newGzExcelMapper.insertMarket(newCtGzMarketModelList);
        newGzExcelMapper.insertMaterial(newCtGzMaterialModelList);
        newGzExcelMapper.insertProduct(newCtGzProductModelList);
        newGzExcelMapper.insertProducing(newCtGzProducingModelList);
        newGzExcelMapper.insertProductLine(newCtGzProductLineModelList);

        newGzExcelMapper.insertProductDesign(newCtGzProductDesignModelList);
        newGzExcelMapper.insertWorkerRecruit(newCtGzWorkerRecruitModelList);
        newGzExcelMapper.insertWorkerTrain(newCtGzWorkerTrainModelList);
        newGzExcelMapper.insertLoan(newCtGzLoanModelList);
        newGzExcelMapper.insertDiscount(newCtGzDiscountModelList);
        newGzExcelMapper.insertClasses(newCtGzClassesModelList);
        newGzExcelMapper.insertWorkerIncentive(newCtGzWorkerIncentiveModelList);
        newGzExcelMapper.insertNum(newCtGzNumModelList);


        /*
        更新此题库已经上传规则表
         */
        if (newCtSubject.getUpload() == 0) {
            newCtSubjectMapper.updateUpload(subjectNumber);
        }

        return R.success();
    }


    public R getRuleBySubjectNumber(Integer subjectNumber) {
        Map<String, Object> gzList = new HashMap<>();
        List<NewCtGzCs> newCtGzCsList = newCtGzCsMapper.selectList(new QueryWrapper<NewCtGzCs>().eq("subject_number", subjectNumber));
        List<NewCtGzAd> newCtGzAdList = newCtGzAdMapper.selectList(new QueryWrapper<NewCtGzAd>().eq("subject_num", subjectNumber));
        List<NewCtGzIso> newCtGzIsoList = newCtGzIsoMapper.selectList(new QueryWrapper<NewCtGzIso>().eq("subject_number", subjectNumber));
        List<NewCtGzOrder> newCtGzOrderList = newCtGzOrderMapper.selectList(new QueryWrapper<NewCtGzOrder>().eq("subject_number", subjectNumber));
        List<NewCtGzMarket> newCtGzMarketList = newCtGzMarketMapper.selectList(new QueryWrapper<NewCtGzMarket>().eq("subject_number", subjectNumber));
        List<NewCtGzMaterial> newCtGzMaterialList = newCtGzMaterialMapper.selectList(new QueryWrapper<NewCtGzMaterial>().eq("subject_number", subjectNumber));
        List<NewCtGzProduct> newCtGzProductList = newCtGzProductMapper.selectList(new QueryWrapper<NewCtGzProduct>().eq("subject_number", subjectNumber));
        List<NewCtGzProducing> newCtGzProducingList = newCtGzProducingMapper.selectList(new QueryWrapper<NewCtGzProducing>().eq("subject_number", subjectNumber));
        List<NewCtGzProductLine> newCtGzProductLineList = newCtGzProductLineMapper.selectList(new QueryWrapper<NewCtGzProductLine>().eq("subject_num", subjectNumber));


        List<NewCtGzProductDesign> newCtGzProductDesignList = newCtGzProductDesignMapper.selectList(new QueryWrapper<NewCtGzProductDesign>().eq("subject_num", subjectNumber));
        List<NewCtGzWorkerRecruit> newCtGzWorkerRecruitList = newCtGzWorkerRecruitMapper.selectList(new QueryWrapper<NewCtGzWorkerRecruit>().eq("subject_num", subjectNumber));
        List<NewCtGzWorkerTrain> newCtGzWorkerTrainList = newCtGzWorkerTrainMapper.selectList(new QueryWrapper<NewCtGzWorkerTrain>().eq("subject_num", subjectNumber));
        List<NewCtGzLoan> newCtGzLoanList = newCtGzLoanMapper.selectList(new QueryWrapper<NewCtGzLoan>().eq("subject_num", subjectNumber));
        List<NewCtGzDiscount> newCtGzDiscountList = newCtGzDiscountMapper.selectList(new QueryWrapper<NewCtGzDiscount>().eq("subject_num", subjectNumber));
        List<NewCtGzClasses> newCtGzClassesList = newCtGzClassesMapper.selectList(new QueryWrapper<NewCtGzClasses>().eq("subject_num", subjectNumber));
        List<NewCtGzWorkerIncentive> newCtGzWorkerIncentiveList = newCtGzWorkerIncentiveMapper.selectList(new QueryWrapper<NewCtGzWorkerIncentive>().eq("subject_num", subjectNumber));
        List<NewCtGzNum> newCtGzNumList = newCtGzNumMapper.selectList(new QueryWrapper<NewCtGzNum>().eq("subject_num", subjectNumber));

        gzList.put("newCtGzCsList", newCtGzCsList);
        gzList.put("newCtGzAdList", newCtGzAdList);
        gzList.put("newCtGzIsoList", newCtGzIsoList);
        gzList.put("newCtGzOrderList", newCtGzOrderList);
        gzList.put("newCtGzMarketList", newCtGzMarketList);
        gzList.put("newCtGzMaterialList", newCtGzMaterialList);
        gzList.put("newCtGzProductList", newCtGzProductList);
        gzList.put("newCtGzProducingList", newCtGzProducingList);
        gzList.put("newCtGzProductLineList", newCtGzProductLineList);

        gzList.put("newCtGzProductDesignList", newCtGzProductDesignList);
        gzList.put("newCtGzWorkerRecruitList", newCtGzWorkerRecruitList);
        gzList.put("newCtGzWorkerTrainList", newCtGzWorkerTrainList);
        gzList.put("newCtGzLoanList", newCtGzLoanList);
        gzList.put("newCtGzDiscountList", newCtGzDiscountList);
        gzList.put("newCtGzClassesList", newCtGzClassesList);
        gzList.put("newCtGzWorkerIncentiveList", newCtGzWorkerIncentiveList);
        gzList.put("newCtGzNumList", newCtGzNumList);
        return R.success(gzList);
    }



}
