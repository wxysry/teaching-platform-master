package com.xiyou.main.biz.newcontest;

import com.xiyou.common.enums.CodeEnum;
import com.xiyou.common.utils.R;
import com.xiyou.main.dao.contest.CtCashflowMapper;
import com.xiyou.main.dao.contest.CtYfIsoMapper;
import com.xiyou.main.dao.newcontest.NewCtCashflowMapper;
import com.xiyou.main.dao.newcontest.NewCtGzIsoMapper;
import com.xiyou.main.dao.newcontest.NewCtYfIsoMapper;
import com.xiyou.main.entity.contest.CtCashflow;
import com.xiyou.main.entity.newcontest.NewCtCashflow;
import com.xiyou.main.entity.newcontest.NewCtGzIso;
import com.xiyou.main.entity.newcontest.NewCtYfIso;
import com.xiyou.main.params.contest.ISOInvestParam;
import com.xiyou.main.params.newcontest.NewISOInvestParam;
import com.xiyou.main.service.contest.CtCashflowService;
import com.xiyou.main.service.newcontest.NewCtCashflowService;
import com.xiyou.main.utils.DateUtils;
import io.swagger.models.auth.In;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

/**
 * @program: multi-module
 * @description:
 * @author: tangcan
 * @create: 2019-09-01 12:43
 **/
@Service
public class NewISOInvestBiz {
    @Autowired
    private NewCtYfIsoMapper ctYfIsoMapper;
    @Autowired
    private NewCtCashflowService ctCashflowService;
    @Autowired
    private NewCtCashflowMapper cashflowMapper;
    @Autowired
    private NewCtGzIsoMapper newCtGzIsoMapper;
    @Autowired
    private NewActionBiz newActionBiz;

    public R list(Integer contestId, Integer studentId) {
        List<Map<String, Object>> list = ctYfIsoMapper.getIsoInvestList(studentId, contestId);
        return R.success().put("isoList", list);
    }

    @Transactional
    public R commit(NewISOInvestParam isoInvestParam) {
        if (isoInvestParam.getIsoId() == null) {
            return R.error(CodeEnum.PARAM_ERROR.getCode(), "请选择ISO投资");
        }
        // 现金
        Integer cash = cashflowMapper.getCash(isoInvestParam.getStudentId(), isoInvestParam.getContestId());
        if (cash == null) {
            cash = 0;
        }

        NewCtYfIso newCtYfIso = ctYfIsoMapper.selectById(isoInvestParam.getIsoId());
        if (NewCtYfIso.ISO_YF_ING.equals(newCtYfIso.getDiState()) || NewCtYfIso.ISO_YF_FINISH.equals(newCtYfIso.getDiState())) {
            return R.error(CodeEnum.OTHER_ERROR.getCode(), "无需重复研发");
        }

        NewCtGzIso newCtGzIso = newCtGzIsoMapper.selectById(newCtYfIso.getDiCiid());
        Integer investSum = newCtGzIso.getCiDevelopFee();
        // 当现金<sum（勾选的投资费用总和），则报错【现金不足】
        if (cash < investSum) {
            return R.error(CodeEnum.OTHER_ERROR.getCode(), "现金不足");
        }
        // 刷新cashflow，ID、ISO开拓、0、sum（勾选的投资费用总和），现金+增加-减少、扣减ISO开拓&sum（勾选的投资费用总和）、当前时间
        ctCashflowService.save(new NewCtCashflow()
                .setStudentId(isoInvestParam.getStudentId())
                .setContestId(isoInvestParam.getContestId())
                .setCAction("ISO开拓")
                .setCIn(0)
                .setCOut(investSum)
                .setCSurplus(cash - investSum)
                .setCComment("扣减ISO"+newCtGzIso.getCiName()+"开拓" + investSum + "元")
                .setCDate(isoInvestParam.getDate()));
        // 刷新yf_ISO,勾选记录now_date +1，Remain_Date -1
        Integer ciDevelopDate = newCtGzIso.getCiDevelopDate();
        Integer finishDate = DateUtils.addYearAndSeasonTime(isoInvestParam.getDate(),ciDevelopDate);
        if(ciDevelopDate != null && ciDevelopDate>0 ){
            newCtYfIso.setDiState(NewCtYfIso.ISO_YF_ING);
        }else {
            newCtYfIso.setDiState(NewCtYfIso.ISO_YF_FINISH);
        }
        newCtYfIso.setDiStartDate(isoInvestParam.getDate())
                .setDiFinishDate(finishDate);
        ctYfIsoMapper.updateById(newCtYfIso);

        //计算四张报表
        Integer bsTotalEquity = newActionBiz.calBsTotalEquity(isoInvestParam.getStudentId(),isoInvestParam.getContestId(),isoInvestParam.getDate());
        // 所有者权益为负后，自动结束比赛
        if (bsTotalEquity < 0) {
            return R.error(7000, "当前所有者权益为负，你已破产，模拟竞赛结束");
        }
        return R.success();
    }
}
