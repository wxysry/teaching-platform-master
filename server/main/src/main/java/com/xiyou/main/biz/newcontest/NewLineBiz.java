package com.xiyou.main.biz.newcontest;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.xiyou.common.enums.CodeEnum;
import com.xiyou.common.utils.CommonUtil;
import com.xiyou.common.utils.R;
import com.xiyou.main.dao.contest.CtGzProductLineMapper;
import com.xiyou.main.dao.contest.CtKcProductMapper;
import com.xiyou.main.dao.newcontest.*;
import com.xiyou.main.entity.contest.*;
import com.xiyou.main.entity.newcontest.*;
import com.xiyou.main.enums.CashFlowActionEnum;
import com.xiyou.main.enums.ErrorEnum;
import com.xiyou.main.params.contest.OnlineConfirm;
import com.xiyou.main.params.newcontest.NewLineParam;
import com.xiyou.main.service.contest.*;
import com.xiyou.main.service.newcontest.*;
import com.xiyou.main.utils.DateUtils;
import com.xiyou.main.vo.contest.OnlineLine;
import com.xiyou.main.vo.newcontest.NewCtKcMaterialNumVo;
import com.xiyou.main.vo.newcontest.NewOnlineLine;
import io.swagger.models.auth.In;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.management.Query;
import java.util.*;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.toList;


/**
 * @author dingyoumeng
 * @since 2019/07/22
 */
@Transactional
@Service
public class NewLineBiz {

    @Autowired
    private NewCtLineService lineService;
    @Autowired
    private NewCtLineMapper newCtLineMapper;
    @Autowired
    private NewCtCashflowService ctCashflowService;
    @Autowired
    private NewCtGzProductLineService ctGzProductLineService;
    @Autowired
    private NewCtKcMaterialService ctKcMaterialService;
    @Autowired
    private NewCtGzProductService ctGzProductService;
    @Autowired
    private NewCtKcProductMapper ctKcProductMapper;
    @Autowired
    private NewCtGzProductLineMapper newCtGzProductLineMapper;
    @Autowired
    private NewCtGzProducingMapper newCtGzProducingMapper;
    @Autowired
    private NewCtKcMaterialMapper newCtKcMaterialMapper;
    @Autowired
    private NewCtGzProductMapper newCtGzProductMapper;
    @Autowired
    private NewCtGzMaterialMapper newCtGzMaterialMapper;
    @Autowired
    private NewCtWorkerMapper newCtWorkerMapper;
    @Autowired
    private NewActionBiz newActionBiz;
    @Autowired
    private NewCtGzCsMapper newCtGzCsMapper;

    @Transactional
    public R add(NewCtLine line) {
        int countLine = newCtLineMapper.getCountLine(line.getContestId(), line.getStudentId());
        NewCtGzCs gzCs = newCtGzCsMapper.getByContestId(line.getContestId());
        if(countLine >=gzCs.getMaxLine()){
            return ErrorEnum.LINE_MULTI.getR();
        }
        //获取现金
        Integer cash = ctCashflowService.getCash(line.getStudentId(), line.getContestId());
        //生产线类型编号
        Integer cplid = line.getPlCplid();

        NewCtGzProductLine ctGzProductLine = ctGzProductLineService.getById(cplid);

        NewCtGzProduct ctGzProduct = ctGzProductService.getById(line.getPlCpid());
        int remain = cash - ctGzProductLine.getCplBuyFee();
        if (remain < 0) {
            return ErrorEnum.CASH_NOT_ENOUGH.getR();
        }
        line.setPlInvest(ctGzProductLine.getCplBuyFee());//原值
        Integer addDate = ctGzProductLine.getCplInstallDate();
        Integer finishDate = DateUtils.addYearAndSeasonTime(line.getPlAddTime(), addDate);
        line.setPlFinishDate(finishDate);
        line.setPlRemainDate(addDate);
        if(addDate == 0){
            line.setStatus(NewCtLine.ON_SPACE);
        }else {
            line.setStatus(NewCtLine.ON_BUILD);//状态
        }
        line.setCplName(ctGzProductLine.getCplName());//产线名称
        line.setPlDepTotal(0);
        line.setPlProductAddDate(null);
        line.setPlProductingDate(null);
        line.setPlTransferDate(null);
        line.setPlTransferAddDate(null);
        newCtLineMapper.insert(line);

        //插入消费记录
        NewCtCashflow ctCashflow = new NewCtCashflow();
        ctCashflow.setCDate(line.getPlAddTime())
                .setCSurplus(remain)
                .setCIn(0)
                .setCOut(ctGzProductLine.getCplBuyFee())
                .setCAction(CashFlowActionEnum.BUY_PRODUCT_LINE.getAction())
                .setContestId(line.getContestId())
                .setStudentId(line.getStudentId())
                .setCComment("新建生产线：" + ctGzProductLine.getCplName() + (ctGzProduct == null ? "" : ctGzProduct.getCpName()));
        ctCashflowService.save(ctCashflow);

        //计算四张报表
        Integer bsTotalEquity = newActionBiz.calBsTotalEquity(line.getStudentId(),line.getContestId(),line.getPlAddTime());
        // 所有者权益为负后，自动结束比赛
        if (bsTotalEquity < 0) {
            return R.error(7000, "当前所有者权益为负，你已破产，模拟竞赛结束");
        }
        return R.success();
    }


    /**
     * 获取所有生产线
     * @param contestId
     * @param studentId
     * @return
     */
    public R getAllLineList(Integer contestId,Integer studentId) {
        List<NewCtLine> allLineList = newCtLineMapper.getList(contestId, studentId);
        for (NewCtLine newCtLine : allLineList) {
            //获取所需高级工人和所需普通工人数
             NewCtGzProductLine newCtGzProductLine = newCtGzProductLineMapper.selectById(newCtLine.getPlCplid());
            newCtLine.setCplSeniorWorker(newCtGzProductLine.getCplSeniorWorker());
            newCtLine.setCplOrdinaryWorker(newCtGzProductLine.getCplOrdinaryWorker());

            List<NewCtWorker> gj = newCtWorkerMapper.getGjWorkerListByLineId(newCtLine.getLineId());
            List<Integer> gjId = new ArrayList<>();
            for (NewCtWorker newCtWorker : gj) {
                gjId.add(newCtWorker.getId());
            }
            newCtLine.setSeniorWorkerList(gjId);
            List<NewCtWorker> pt = newCtWorkerMapper.getPtWorkerListByLineId(newCtLine.getLineId());
            List<Integer> ptId = new ArrayList<>();
            for (NewCtWorker newCtWorker : pt) {
                ptId.add(newCtWorker.getId());
            }
            newCtLine.setOrdinaryWorkerList(ptId);
        }


        return R.success().put("list",allLineList);
    }


    public R listOnline(Integer userId, Integer contestId, Integer date) {
        List<NewOnlineLine> list = lineService.listOnline(userId, contestId, date);
        Map<String, Object> resultMap = new HashMap<>(16);
        resultMap.put("list", list);
        return R.success(resultMap);
    }





    /***
     * 转产
     * @param userId
     * @param contestId
     * @param lineId
     * @param cpId
     * @param currentTime
     * @return
     */
    public R lineTransfer(Integer userId,Integer contestId,Integer lineId,Integer cpId,Integer currentTime) {


        Integer cash = ctCashflowService.getCash(userId, contestId);
        NewCtLine line = newCtLineMapper.selectById(lineId);
        if(line.getPlCpid().equals(cpId)){
            return ErrorEnum.TRANSFER_WORING.getR();
        }
        //获取生产线规则
        NewCtGzProductLine newCtGzProductLine = newCtGzProductLineMapper.selectById(line.getPlCplid());

        int remain = cash - newCtGzProductLine.getCplTransferFee();
        if (remain < 0) {
            return ErrorEnum.CASH_NOT_ENOUGH.getR();
        }
        //插入现金交易记录
        NewCtCashflow ctCashflow = new NewCtCashflow();
        NewCtGzProduct ctGzProduct = ctGzProductService.getById(cpId);
        ctCashflow.setStudentId(userId)
                .setContestId(contestId)
                .setCAction(CashFlowActionEnum.PRODUCT_LINE_TRANSFER.getAction())
                .setCComment("生产线" + line.getLineId().toString() + "转产为" + (ctGzProduct == null ? "" : ctGzProduct.getCpName()))
                .setCIn(0)
                .setCOut(newCtGzProductLine.getCplTransferFee())//转产费用
                .setCDate(currentTime)
                .setCSurplus(remain);
        ctCashflowService.save(ctCashflow);

        Integer finishDate = DateUtils.addYearAndSeasonTime(currentTime, newCtGzProductLine.getCplTransferDate());
        line.setPlTransferAddDate(currentTime)//转产开始时间
            .setPlCpid(cpId)//转产结束时间
            .setPlTransferDate(finishDate);//转产结束时间
        if(newCtGzProductLine.getCplTransferDate() == null || newCtGzProductLine.getCplTransferDate() == 0){
            line.setStatus(NewCtLine.ON_SPACE);
        }else {
            line.setStatus(NewCtLine.ON_TRANSFER);
        }
        newCtLineMapper.updateById(line);
        return R.success();
    }

    /**
     * 出售
     * @param userId
     * @param line
     * @return
     */
    public R lineSell(Integer userId, NewCtLine line) {
        if (line.getLineId() == null) {
            return R.error(CodeEnum.PARAM_MISS.getCode(), "请选择生产线");
        }
        Integer cash = ctCashflowService.getCash(userId, line.getContestId());
        //获取生产线规则
        NewCtGzProductLine newCtGzProductLine = newCtGzProductLineMapper.selectById(line.getPlCplid());
        newCtWorkerMapper.updateWorkerListByLineIdToSpace(line.getLineId());

        // 插入现金交易记录
        // 流入=原值-累计折旧
        // 流出=原值-累计折旧-残值
        Integer cIn = line.getPlInvest() - line.getPlDepTotal();
        int cOut = line.getPlInvest() - line.getPlDepTotal() - newCtGzProductLine.getCplDepFee();
        NewCtCashflow ctCashflow = new NewCtCashflow();
        ctCashflow.setStudentId(userId)
                .setContestId(line.getContestId())
                .setCAction(CashFlowActionEnum.SELL_PRODUCT_LINE.getAction())
                .setCOut(cOut)
                .setCIn(cIn)
                .setCComment("变卖生产线" + line.getLineId() + ",增加现金" + line.getPlInvest() + "元,减少现金" + line.getPlDepTotal() + "元")
                .setCDate(line.getActionTime())//当前时间
                .setCSurplus(cash + cIn - cOut);
        ctCashflowService.save(ctCashflow);
        lineService.removeById(line.getLineId());

        //计算四张报表
        Integer bsTotalEquity = newActionBiz.calBsTotalEquity(userId,line.getContestId(),line.getActionTime());
        // 所有者权益为负后，自动结束比赛
        if (bsTotalEquity < 0) {
            return R.error(7000, "当前所有者权益为负，你已破产，模拟竞赛结束");
        }
        return R.success();
    }





    /**
     * 生产产品
     * @param userId
     * @param params
     * @return
     * @throws Exception
     */
    @Transactional
    public R producting(Integer userId, NewCtLine params) throws Exception {
        Integer cash = ctCashflowService.getCash(userId, params.getContestId());
        NewCtLine line = newCtLineMapper.selectById(params.getLineId());
        //获取当前产线生产对应的单个产品及所需的材料
        List<NewCtGzProducing> newCtGzProducings = newCtGzProducingMapper.getListByCpId(line.getPlCpid());

        //判断库存是否足够生产
        List<NewCtKcMaterialNumVo> kcNums = newCtKcMaterialMapper.listKcNum(line.getContestId(), line.getStudentId());
        Map<Integer,Integer> kcNumMap = new HashMap<>();
        for (NewCtKcMaterialNumVo kcNum : kcNums) {
            kcNumMap.put(kcNum.getMaterialNum(),kcNum.getNum());
        }
        for (NewCtGzProducing newCtGzProducing : newCtGzProducings) {
            int need  = line.getRealProduction() * newCtGzProducing.getCpNum();
            Integer kc = kcNumMap.get(newCtGzProducing.getCpMid());
            if(kc==null || kc < need){
                return ErrorEnum.MATERIAL_NOT_ENOUGH.getR();
            }
        }

        //获取生产所需加工费
        int developFee = 0;
        NewCtGzProduct newCtGzProduct = newCtGzProductMapper.selectById(params.getPlCpid());
        int fee = newCtGzProduct.getCpDevelopFee() * line.getRealProduction();
        developFee += developFee + fee;
        int remain = cash - developFee;
        if (remain < 0) {
            return ErrorEnum.CASH_NOT_ENOUGH.getR();
        }

        //获取当前生产线规则信息
        NewCtGzProductLine gzLine = newCtGzProductLineMapper.selectById(line.getPlCplid());
        
        //获取材料库存信息（按入库时间排序）
        List<NewCtKcMaterial> materials = newCtKcMaterialMapper.list(line.getContestId(), line.getStudentId());
        
        //材料规则id和编号的键值对
        List<NewCtGzMaterial> gzMaterial = newCtGzMaterialMapper.list(line.getContestId());
        Map<Integer,Integer> idAndCmIdMap = new HashMap<>();
        for (NewCtGzMaterial newCtGzMaterial : gzMaterial) {
            idAndCmIdMap.put(newCtGzMaterial.getId(),newCtGzMaterial.getCmId());
        }
        
        //把材料库存分类
        Map<Integer,List<NewCtKcMaterial>> kcMaterialMap = new HashMap<>();
        for (NewCtGzProducing newCtGzProducing : newCtGzProducings) {
            List<NewCtKcMaterial> list = new ArrayList<>();
            for (NewCtKcMaterial material : materials) {
                Integer imNum = idAndCmIdMap.get(material.getImCmId());
                if(imNum.equals(newCtGzProducing.getCpMid())){
                    list.add(material);
                }
            }
            kcMaterialMap.put(newCtGzProducing.getCpMid(),list);
        }

        //生产的产品个数进行循环 进行生产
        for (int i = 0; i < line.getRealProduction(); i++) {
            int cashNeed = 0;
            for (NewCtGzProducing newCtGzProducing : newCtGzProducings) {
                int needNum = newCtGzProducing.getCpNum();
                if(needNum == 0){ continue;}

                List<NewCtKcMaterial> materialList = kcMaterialMap.get(newCtGzProducing.getCpMid());
                for (NewCtKcMaterial material : materialList) {
                    if(material.getImNum() == 0 ){continue;}
                    if(material.getImNum() - needNum > 0){
                        //计算金额，更新材料剩余数量
                        cashNeed += material.getMaterialPrice() * needNum;
                        material.setImNum(material.getImNum() - needNum);
                        newCtKcMaterialMapper.updateById(material);
                        break;
                    }else{
                        //计算金额，删除数据（改为更新为0），结束当前循环
                        cashNeed += material.getMaterialPrice() * material.getImNum();
                        needNum = needNum - material.getImNum();
//                        material.setImNum(0);
                        newCtKcMaterialMapper.deleteById(material);
                    }
                }
            }
            cashNeed += newCtGzProduct.getCpDevelopFee();//加工费
            //生成库存数据
            NewCtKcProduct newCtKcProduct = new NewCtKcProduct();
            newCtKcProduct.setContestId(line.getContestId())
                    .setStudentId(line.getStudentId())
                    .setIpCpId(line.getPlCpid())
                    .setIpNum(1)
                    .setLineId(line.getLineId())
                    .setRealCost(cashNeed);

            Integer produceTime = gzLine.getCplProduceDate();
            //判断生产周期
            if(produceTime !=null && produceTime>0){
                Integer produceFinishTime = DateUtils.addYearAndSeasonTime(params.getActionTime(),produceTime);
                line.setPlProductingDate(produceFinishTime);//生产结束时间
                newCtKcProduct.setIsInventory(NewCtKcProduct.NOT_IN);// 产品未入库
                newCtKcProduct.setInventoryDate(produceFinishTime);//入库时间
            }else {
                line.setPlProductingDate(line.getActionTime());//生产结束时间
                newCtKcProduct.setIsInventory(NewCtKcProduct.IS_IN);// 产品入库
                newCtKcProduct.setInventoryDate(line.getActionTime());//入库时间
            }
            ctKcProductMapper.insert(newCtKcProduct);
        }

//        //更新所有记录
//        kcMaterialMap.forEach((k,v)->ctKcMaterialService.updateBatchById(v));

        //插入现金交易记录
        NewCtCashflow ctCashflow = new NewCtCashflow();
        ctCashflow.setStudentId(userId)
                .setContestId(line.getContestId())
                .setCAction(CashFlowActionEnum.PRODUCT_PRODUCING.getAction())
                .setCOut(developFee)
                .setCIn(0)
                .setCComment("生产线" + line.getLineId().toString()+"生产产品"+newCtGzProduct.getCpName())
                .setCDate(params.getActionTime())
                .setCSurplus(remain);
        ctCashflowService.save(ctCashflow);

        //更新生产线表
        Integer produceTimeDate = DateUtils.addYearAndSeasonTime(params.getActionTime(),gzLine.getCplProduceDate());

        line.setPlProductAddDate(params.getActionTime());//设置开产时间
        line.setPlProductingDate(produceTimeDate);//设置生产结束时间
        line.setStatus(NewCtLine.ON_PRODUCE);
        lineService.updateById(line);

        //计算四张报表
        Integer bsTotalEquity = newActionBiz.calBsTotalEquity(userId,params.getContestId(),params.getActionTime());
        // 所有者权益为负后，自动结束比赛
        if (bsTotalEquity < 0) {
            return R.error(7000, "当前所有者权益为负，你已破产，模拟竞赛结束");
        }

        return R.success();
    }

    /**
     * 设备规则、线型、产品获取
     * @param studentId
     * @param contestId
     * @return
     */
    public R info(Integer studentId, Integer contestId) {

        // 生产线类型
        List<NewCtGzProductLine> productLineList = ctGzProductLineService.getProductLineTypeList(contestId);
        // 生产产品
        List<NewCtGzProduct> productList = ctGzProductService.getList(contestId);
        Map<String, Object> returnMap = new HashMap<>();
        returnMap.put("productLineList", productLineList);
        returnMap.put("productList", productList);
        return R.success(returnMap);
    }

    /**
     * 产品信息获取
     * @param contestId
     * @return
     */
    public R proInfo(Integer contestId) {
        // 生产产品
        List<NewCtGzProduct> productList = ctGzProductService.getList(contestId);
        Map<String, Object> returnMap = new HashMap<>();
        returnMap.put("productList", productList);
        return R.success(returnMap);
    }


    /**
     * 产线保存工人
     * @param studentId
     * @param param
     * @return
     */
    @Transactional
    public R saveWorkerToLine(Integer studentId, NewLineParam param){
        List<Integer> workerIds1 = param.getOrdinaryWorkerList();
        List<Integer> workerIds2 = param.getSeniorWorkerList();
        List<Integer> workerIds = new ArrayList<>();
        workerIds.addAll(workerIds1);
        workerIds.addAll(workerIds2);
        List<NewCtWorker> workers = new ArrayList<>();
        if(workerIds.size() > 0){
            workers = newCtWorkerMapper.selectBatchIds(workerIds);
            for (NewCtWorker worker : workers) {
                //在其他产线
                if(worker.getLineId()!= null && !Objects.equals(worker.getLineId(), param.getLineId())){
                    return ErrorEnum.WORKER_WORKING.getR();
                }
            }
        }
        //删除原来保存的所有id
        List<NewCtWorker> allWorker = newCtWorkerMapper.getAllWorkerListByLineId(param.getLineId());
        for (NewCtWorker newCtWorker : allWorker) {
            newCtWorkerMapper.update(newCtWorker,new UpdateWrapper<NewCtWorker>().lambda()
                    .set(NewCtWorker::getLineId,null)
                    .set(NewCtWorker::getIsWork,NewCtWorker.ON_SPACE)
                    .eq(NewCtWorker::getId,newCtWorker.getId())
            );
        }

        //重新保存id
        for (NewCtWorker worker : workers) {
            worker.setLineId(param.getLineId())
                    .setIsWork(NewCtWorker.ON_WORK);
            newCtWorkerMapper.updateById(worker);
        }

        NewCtLine newCtLine = newCtLineMapper.selectById(param.getLineId());
        newCtLine.setClassesId(param.getClassesId())//班次规则
                .setRealProduction(param.getRealProduction());//实际产量
        newCtLineMapper.updateById(newCtLine);

        return R.success();
    }
}
