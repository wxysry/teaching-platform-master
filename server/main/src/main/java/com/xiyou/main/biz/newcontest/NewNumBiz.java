package com.xiyou.main.biz.newcontest;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.xiyou.common.enums.CodeEnum;
import com.xiyou.common.utils.R;
import com.xiyou.main.dao.newcontest.*;
import com.xiyou.main.entity.newcontest.*;
import com.xiyou.main.params.newcontest.NewISOInvestParam;
import com.xiyou.main.params.newcontest.NewNumParam;
import com.xiyou.main.service.newcontest.NewCtCashflowService;
import com.xiyou.main.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

/**
 * @program: multi-module
 * @description:
 * @author: tangcan
 * @create: 2019-09-01 12:43
 **/
@Service
public class NewNumBiz {
    @Autowired
    private NewCtCashflowService ctCashflowService;
    @Autowired
    private NewCtCashflowMapper cashflowMapper;
    @Autowired
    private NewCtGzNumMapper newCtGzNumMapper;
    @Autowired
    private NewCtYfNumMapper newCtYfNumMapper;

    public R list(Integer contestId, Integer studentId) {
        QueryWrapper<NewCtYfNum> qw = new QueryWrapper<>();
        qw.eq("contest_id",contestId);
        qw.eq("student_id",studentId);
        qw.orderByAsc("id");
        List<NewCtYfNum> list = newCtYfNumMapper.selectList(qw);
        return R.success().put("list", list);
    }



    @Transactional
    public R commit(NewNumParam param) {
        if (param.getId() == null) {
            return R.error(CodeEnum.PARAM_ERROR.getCode(), "请选择数字化研发");
        }
        // 现金
        Integer cash = cashflowMapper.getCash(param.getStudentId(), param.getContestId());
        if (cash == null) {
            cash = 0;
        }

        NewCtYfNum newCtYfNum = newCtYfNumMapper.selectById(param.getId());
        if (NewCtYfNum.NUM_YF_ING.equals(newCtYfNum.getState()) || NewCtYfNum.NUM_YF_FINISH.equals(newCtYfNum.getState())) {
            return R.error(CodeEnum.OTHER_ERROR.getCode(), "无需重复研发");
        }

        NewCtGzNum newCtGzNum = newCtGzNumMapper.selectById(newCtYfNum.getNumId());
        Integer investSum = newCtGzNum.getConsumeMoney();
        // 当现金<sum（勾选的投资费用总和），则报错【现金不足】
        if (cash < investSum) {
            return R.error(CodeEnum.OTHER_ERROR.getCode(), "现金不足");
        }
        // 刷新cashflow，ID、ISO开拓、0、sum（勾选的投资费用总和），现金+增加-减少、扣减ISO开拓&sum（勾选的投资费用总和）、当前时间
        ctCashflowService.save(new NewCtCashflow()
                .setStudentId(newCtYfNum.getStudentId())
                .setContestId(newCtYfNum.getContestId())
                .setCAction("数字化")
                .setCIn(0)
                .setCOut(investSum)
                .setCSurplus(cash - investSum)
                .setCComment("扣减数字化开拓" + investSum + "元")
                .setCDate(param.getDate()));
        // 刷新yf_ISO,勾选记录now_date +1，Remain_Date -1
        Integer ciDevelopDate = newCtGzNum.getTimeCostQuarter();
        Integer finishDate = DateUtils.addYearAndSeasonTime(param.getDate(),ciDevelopDate);
        if(ciDevelopDate != null && ciDevelopDate>0 ){
            newCtYfNum.setState(NewCtYfNum.NUM_YF_ING);
        }else {
            newCtYfNum.setState(NewCtYfNum.NUM_YF_FINISH);
        }
        newCtYfNum.setStartDate(param.getDate())
                .setFinishDate(finishDate);
        newCtYfNumMapper.updateById(newCtYfNum);
        return R.success();
    }
}
