package com.xiyou.main.biz.newcontest;

import com.xiyou.common.utils.R;
import com.xiyou.main.dao.newcontest.NewCtSubjectMapper;
import com.xiyou.main.entity.contest.ContestStudent;
import com.xiyou.main.entity.newcontest.NewContestStudent;
import com.xiyou.main.service.newcontest.NewContestStudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @program: multi-module
 * @description:
 * @author wangxingyu
 * @since 2023-06-11
 **/
@Service
@Transactional
public class NewRestoreDataBiz {
    @Autowired
    private NewCtSubjectMapper newCtSubjectMapper;
    @Autowired
    private NewContestStudentService newContestStudentService;

    /**
     * @Author: tangcan
     * @Description: 数据还原
     * @Param: [contestId, studentId]
     * @date: 2019/8/31
     */
    @Transactional
    public R restore(Integer contestId, Integer studentId) {
        // 先删除现有数据
        newCtSubjectMapper.removeMainTableData(contestId, studentId);
        //清除季度数据
        newCtSubjectMapper.removeSeasonLikeTableData(contestId,studentId);
        // 再从备份表里把数据插入到主表中
        newCtSubjectMapper.insertLikeTableDataToMainTable(contestId, studentId);
        //把年度备份数据放入季度备份表
        newCtSubjectMapper.insertLikeTableDataToReasonTable(contestId,studentId);
        return R.success();
    }

    @Transactional
    public R restoreSeason(Integer contestId, Integer studentId) {
//        List<Integer> dataSum = newCtSubjectMapper.seasonDataSum(contestId, studentId);
//        long sum = dataSum.stream().mapToInt(s -> s).summaryStatistics().getSum();
//        if (sum == 0L) {
//            return R.success("当前无数据可还原");
//        } else {
            //移除主表数据
            newCtSubjectMapper.removeMainTableData(contestId, studentId);
            // 再从备份表里把数据插入到主表中
            newCtSubjectMapper.insertSeasonLikeTableToMainTable(contestId, studentId);


            return R.success();
//        }
    }
    /**
     * @Author: tangcan
     * @Description: 备份数据
     * @Param: [contestId, studentId]
     * @date: 2019/8/31
     */
    public void backupData(Integer contestId, Integer studentId) {
        // 如果备份表里有数据，先删除备份表里的数据
        newCtSubjectMapper.removeLikeTableData(contestId, studentId);
        // 将主表中的数据插入到备份表中
        newCtSubjectMapper.insertMainTableToLikeTableData(contestId, studentId);
    }

    public void backupSeasonData(Integer contestId, Integer studentId) {
        // 如果备份表里有数据，先删除备份表里的数据
        newCtSubjectMapper.removeSeasonLikeTableData(contestId, studentId);
        // 将主表中的数据插入到备份表中
        newCtSubjectMapper.insertMainTableToSeasonLikeTableData(contestId, studentId);
    }

    /**
     * @Author: tangcan
     * @Description: 清除备份数据
     * @Param: [contestId, studentId]
     * @date: 2019/9/1
     */
    public void removeLikeTableData(Integer contestId, Integer studentId) {
        newCtSubjectMapper.removeLikeTableData(contestId, studentId);
    }



    /*
    删除模拟选单和已选订单
     */
    public void removeOrder(Integer studentId, Integer contestId) {
        newCtSubjectMapper.removeOrder(contestId, studentId);
    }

    /*
    将模拟选单和已选订单的备份数据加入模拟选单和已选订单
     */
    public void backupOrder(Integer studentId, Integer contestId) {
        newCtSubjectMapper.backupOrder(contestId, studentId);
    }
}
