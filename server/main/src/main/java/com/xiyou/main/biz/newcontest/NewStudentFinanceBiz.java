package com.xiyou.main.biz.newcontest;

import com.xiyou.common.utils.R;
import com.xiyou.main.dao.newcontest.*;
import com.xiyou.main.vo.newcontest.NewCashFolowEntity;
import com.xiyou.main.vo.newcontest.NewFinanceReturnEntity;
import com.xiyou.main.vo.newcontest.NewMarketReturnEntity;
import com.xiyou.main.vo.newcontest.NewProductMaterialEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

/**
 * @author wangxingyu
 * @since 2023-06-11
 * @description: 财务信息&综合财务信息&研发认证信息
 **/
@Service
public class NewStudentFinanceBiz {

    @Autowired
    NewCtCashflowMapper ctCashflowMapper;
    @Autowired
    NewCtFeeMapper ctFeeMapper;
    @Autowired
    NewCtBankLoanMapper ctBankLoanMapper;
    @Autowired
    NewCtXzOrderMapper ctXzOrderMapper;
    @Autowired
    NewCtYfMarketMapper ctYfMarketMapper;
    @Autowired
    NewCtYfIsoMapper ctYfIsoMapper;
    @Autowired
    NewCtYfProductMapper ctYfProductMapper;
    @Autowired
    NewCtKcProductMapper ctKcProductMapper;
    @Autowired
    NewCtKcMaterialMapper ctKcMaterialMapper;
    @Autowired
    NewCtDdMaterialMapper ctDdMaterialMapper;

    /**
     * @Description: 财务信息&综合财务信息
     * @Author:zhengxiaodong
     * @Param: [cashFolowEntity]
     * @return: com.xiyou.common.utils.R
     */
    public R getStudentFinanceInfo(NewCashFolowEntity cashFolowEntity) {

        Map<String, Object> returnMap = new HashMap<>();

        NewFinanceReturnEntity returnEntity = new NewFinanceReturnEntity();

        // 当前现金
        returnEntity.setCurrentMoney(ctCashflowMapper.getCash(cashFolowEntity.getStudentId(), cashFolowEntity.getContestId()));
        // 应收账款
        returnEntity.setReceivableMoney(ctFeeMapper.getStudentReceivableMoney(cashFolowEntity));
        returnMap.put("receivableMoneyList", ctFeeMapper.getStudentReceivableMoneyList(cashFolowEntity));
        // 长贷总额
        returnEntity.setLongTermLoansMoney(ctBankLoanMapper.getStudentLongAndShortTermLoansMoney(cashFolowEntity, 1));
        returnMap.put("longTermLoansMoneyList", ctBankLoanMapper.getStudentLongAndShortTermLoansMoneyList(cashFolowEntity, 1));
        // 短贷总额
        returnEntity.setShortTermLoanMoney(ctBankLoanMapper.getStudentLongAndShortTermLoansMoney(cashFolowEntity, 2));
        returnMap.put("shortTermLoanMoneyList", ctBankLoanMapper.getStudentLongAndShortTermLoansMoneyList(cashFolowEntity, 2));
        // 贴息
        returnEntity.setDiscountMoney(ctCashflowMapper.getStudentDiscountMoney(cashFolowEntity, "贴现"));
        // 利息      本息同还-利息  每季付息-利息
        returnEntity.setInterest(ctCashflowMapper.getStudentDiscountMoney(cashFolowEntity, "利息"));
        // 销售收入
        returnEntity.setSalesProfit(ctXzOrderMapper.getStudentSalesProfit(cashFolowEntity));

        // 维修费[维修费、转产费、租金、管理费、广告费]
        returnEntity.setRepairCost(ctCashflowMapper.getRepairCostToAdCost(cashFolowEntity, "维修费"));
        // 转产费
        returnEntity.setTransferCost(ctCashflowMapper.getRepairCostToAdCost(cashFolowEntity, "转产"));
        // 管理费
        returnEntity.setManageFee(ctCashflowMapper.getRepairCostToAdCost(cashFolowEntity, "管理费"));
        // 广告费
        returnEntity.setAdCost(ctCashflowMapper.getRepairCostToAdCost(cashFolowEntity, "广告费"));
        // 订单成本
        returnEntity.setDirectCost(ctXzOrderMapper.getStudentProductDevCost(cashFolowEntity));

        // ISO认证[ISO认证、直接成本、市场开拓]
        returnEntity.setISOCost(ctCashflowMapper.getStudentISOTomarketDevCost(cashFolowEntity, "ISO"));
        // 产品研发
        returnEntity.setProductDevCost(ctCashflowMapper.getStudentISOTomarketDevCost(cashFolowEntity, "产品研发"));
        // 市场开拓
        returnEntity.setMarketDevCost(ctCashflowMapper.getStudentISOTomarketDevCost(cashFolowEntity, "市场开拓"));
        //数字化研发
        returnEntity.setDigitalDevCost(ctCashflowMapper.getStudentISOTomarketDevCost(cashFolowEntity,"数字化"));

        returnMap.put("data", returnEntity);
        return R.success(returnMap);
    }

    /**
     * @Description: 研发认证信息
     * @Author:zhengxiaodong
     * @Param: []
     * @return: com.xiyou.common.utils.R
     */
    public R getResearchAndDevelopeInfo(Integer studentId, Integer contestId) {

        NewMarketReturnEntity returnEntity = new NewMarketReturnEntity();

        // 市场准入
        returnEntity.setMarket(ctYfMarketMapper.getList(contestId,studentId));
        // 生产资格
        returnEntity.setProduct(ctYfProductMapper.getList(contestId,studentId));
        // ISO认证
        returnEntity.setIso(ctYfIsoMapper.getList(contestId,studentId));

        return R.success().put("data", returnEntity);
    }

    /**
     * @Description: 库存采购信息
     * @Author:zhengxiaodong
     * @Param: []
     * @return: com.xiyou.common.utils.R
     */
    public R stockPurchaseInfo(Integer studentId, Integer contestId,Integer currentTime) {

        NewProductMaterialEntity materialEntity = new NewProductMaterialEntity();

        materialEntity.setKP(ctKcProductMapper.getKCProductNumb(studentId, contestId));


        materialEntity.setKR(ctKcMaterialMapper.getKcMaterial(studentId, contestId));


        materialEntity.setBR(ctDdMaterialMapper.getddMaterialBlackCar(studentId, contestId,currentTime));


        materialEntity.setGR(ctDdMaterialMapper.getddMaterialGreyCar(studentId, contestId,currentTime));


        return R.success().put("data", materialEntity);
    }


}
