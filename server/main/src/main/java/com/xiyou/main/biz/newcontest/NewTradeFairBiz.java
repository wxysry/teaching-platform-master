package com.xiyou.main.biz.newcontest;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.xiyou.common.enums.CodeEnum;
import com.xiyou.common.utils.CommonUtil;
import com.xiyou.common.utils.R;
import com.xiyou.main.dao.newcontest.*;
import com.xiyou.main.entity.exam.SysUser;
import com.xiyou.main.entity.newcontest.*;
import com.xiyou.main.params.newcontest.NewUserAndOrderParam;
import com.xiyou.main.service.exam.SysUserService;
import com.xiyou.main.service.newcontest.NewCtMnChooseService;
import com.xiyou.main.vo.newcontest.NewUserAdVO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import java.util.*;
import java.util.stream.Collectors;

/**
 * @program: multi-module
 * @description:
 * @author: tangcan
 * @create: 2019-07-27 16:36
 **/
@Service
@Transactional
@Slf4j
public class NewTradeFairBiz {
    @Autowired
    private NewCtMnAdMapper newCtMnAdMapper;
    @Autowired
    private NewCtMnChooseMapper newCtMnChooseMapper;
    @Autowired
    private NewCtMnChooseService newCtMnChooseService;
    @Autowired
    private SysUserService sysUserService;
    @Autowired
    private NewCtGzCsMapper newCtGzCsMapper;
    @Autowired
    private NewCtGzMarketMapper newCtGzMarketMapper;
    @Autowired
    private NewCtGzProductMapper newCtGzProductMapper;
    @Autowired
    private NewCtXzOrderMapper newCtXzOrderMapper;
    @Autowired
    private NewRestoreDataBiz newRestoreDataBiz;
    @Autowired
    private NewCtYfIsoMapper newCtYfIsoMapper;
    @Autowired
    private NewContestStudentMapper newContestStudentMapper;

    public R getUserAndOrder(NewUserAndOrderParam newUserAndOrderParam) {
        Map<String, Object> returnMap = new HashMap<>();

        // 设置当前回合数
        if (newUserAndOrderParam.getRound() == null) {
            // 为空则是第一轮
            newUserAndOrderParam.setRound(1);
        }

        // 设置学生姓名
        SysUser sysUser = sysUserService.getById(newUserAndOrderParam.getStudentId());
        newUserAndOrderParam.setName(sysUser.getAccount());

        // 查出市场编号和产品编号
        Integer cmid = newCtGzMarketMapper.getCmid(newUserAndOrderParam.getContestId(), newUserAndOrderParam.getMarketProduct().substring(0, 2));
        Integer cpid = newCtGzProductMapper.getCpid(newUserAndOrderParam.getContestId(), newUserAndOrderParam.getMarketProduct().substring(2));

        // 用户广告
        List<NewUserAdVO> userAdVOList = this.getUserAdList(newUserAndOrderParam);

        List<NewCtMnChoose> chooseList = this.getOrder(newUserAndOrderParam, userAdVOList, cmid, cpid);

        // 是否次数用完或者没有选单可选择,end表示是否本轮选单可结束
        int end = 1;
        for (NewUserAdVO p : userAdVOList) {
            // 如果还有选单次数，本轮继续
            if (p.getIsMe() == 1 && p.getNumber() > 0) {
                end = 0;
                break;
            }
        }
        if (chooseList.size() == 0) {
            end = 1;
        }

        // 依次按产品广告额、市场广告额、销售额降序排布
        userAdVOList.sort(Comparator.comparing(NewUserAdVO::getProductAd, Comparator.reverseOrder())
                .thenComparing(NewUserAdVO::getMarketAd, Comparator.reverseOrder())
                .thenComparing(NewUserAdVO::getSales, Comparator.reverseOrder()));
        // 如果产品广告额、市场广告额、销售额相同，则学生排在第一
        userAdVOList.sort((o1, o2) -> {
            if (o1.getProductAd().equals(o2.getProductAd()) &&
                    o1.getMarketAd().equals(o2.getMarketAd()) &&
                    o1.getSales().equals(o2.getSales())) {
                if (newUserAndOrderParam.getName().equals(o1.getUser())) {
                    return -1;
                } else if (newUserAndOrderParam.getName().equals(o2.getUser())) {
                    return 1;
                }
            }
            return 0;
        });
        returnMap.put("userAds", userAdVOList);
        // 选单列表
        returnMap.put("orders", chooseList);
        returnMap.put("end", end);
        log.error("人机模式，获取用户广告,参数{},end:{}", JSONObject.toJSONString(newUserAndOrderParam),end);
        return R.success(returnMap);
    }

    /**
     * @Author: tangcan
     * @Description: 获取用户广告额和选单次数列表
     * @Param: [userAndOrderParam]
     * @date: 2019/8/29
     */
    private List<NewUserAdVO> getUserAdList(NewUserAndOrderParam userAndOrderParam) {
        // 查出当年当季  该市场该产品每组的模拟广告
        List<NewCtMnAd> ctMnAdList = newCtMnAdMapper.getAdList(userAndOrderParam.getContestId(), userAndOrderParam.getStudentId(), userAndOrderParam.getYear(),userAndOrderParam.getQuarterly());

        // 规则表获取最小广告额
        NewCtGzCs ctGzCs = newCtGzCsMapper.getByContestId(userAndOrderParam.getContestId());
        int min = ctGzCs.getMin() == null ? 1 : ctGzCs.getMin();
        int tolerance = ctGzCs.getTolerance() == null ? 1 : ctGzCs.getTolerance();

        // 处理数据
        List<NewUserAdVO> userAdVOList = new ArrayList<>();
        for (NewCtMnAd ctMnAd : ctMnAdList) {
            double ad = ctMnAd.getByName(userAndOrderParam.getMarketProduct());
            double sum = ctMnAd.getSumByName(userAndOrderParam.getMarketProduct());
            Integer number = 0;
            //先判断是否小于最小广告额
            if(ad<min){
                number=0;
            }else{
                // 次数= roundown（(广告-最小)/得单公差）+2 - 回合数（每个市场的每个产品开始时，默认为1）
                number = Math.max((int) ((ad-min)/ tolerance + 2- userAndOrderParam.getRound()), 0);
            }
            if (number > 0) {
                userAdVOList.add(new NewUserAdVO()
                        .setUser(ctMnAd.getGroupNum())
                        .setProductAd((int) ad)
                        .setMarketAd((int) sum)
                        .setNumber(number)
                        .setIsMe(userAndOrderParam.getName().equals(ctMnAd.getGroupNum()) ? 1 : 0));
            }
        }
        return userAdVOList;
    }

    /**
     * @Author: tangcan
     * @Description: 获取选单列表
     * @Param: [userAndOrderParam, userAdVOList]
     * @date: 2019/8/31
     */
    private List<NewCtMnChoose> getOrder(NewUserAndOrderParam userAndOrderParam, List<NewUserAdVO> userAdVOList, Integer cmid, Integer cpid) {
        // 右边显示模拟选单mn_choose中的当年该市场该产品的定单（过滤掉选走轮次<回合数，过滤选走轮次=回合数 且 选走组号的该空广告>该组这个空的广告 （ 广告值比自己高） 
        // 过滤选走轮次=回合数 且 选走组号的该空广告=该组这个空的广告 且 选走组号该空的市场广告额>本组市场广告额 （市场广告额比自己高）
        // 过滤选走轮次=回合数 且 选走组号的该空广告=该组这个空的广告 且 选走组号该空的市场广告额=本组市场广告额 且上一年该市场总销售额>本组上一年该市场销售额（销售额比自己高）
        // 销售额=本组本市场某年所有产品总价之和（已交单的，模拟的组号默认已交单）

        // 就是如果这个单据ISO为空，就是可以选。
        // 如果为1，那该组ISO9000已经开发完成就可以选
        // 如果为2，那该组ISO14000已经开发完成就可以选
        // 如果为3，那需要两个都开发完成才能选

        // 先查出所有数据，再过滤掉选走轮次<回合数的,选走轮次=回合数且广告值比自己高
        List<NewCtMnChoose> ctMnChooseList = newCtMnChooseMapper.getOrderList(userAndOrderParam.getContestId(),
                userAndOrderParam.getStudentId(),
                cmid, cpid,
                userAndOrderParam.getYear(),userAndOrderParam.getQuarterly());

        // 从模拟广告中获取其他组上一年的销售额
        List<NewCtMnChoose> salesList = newCtMnChooseMapper.getSales(userAndOrderParam.getContestId(),
                userAndOrderParam.getStudentId(), cmid, userAndOrderParam.getYear() - 1);
        // 组号映射到销售额
        Map<String, Integer> salesMap = salesList.stream().collect(Collectors.toMap(NewCtMnChoose::getXzGroup, NewCtMnChoose::getSales, (k1, k2) -> k1));

        // 本组上一年已交单的销售额
        Integer mySales = newCtXzOrderMapper.getSales(userAndOrderParam.getContestId(),
                userAndOrderParam.getStudentId(), cmid, userAndOrderParam.getYear() - 1);
        mySales = (mySales == null ? 0 : mySales);

        // 将销售额加入userAdVOList中
        for (NewUserAdVO userAdVO : userAdVOList) {
            if (userAndOrderParam.getName().equals(userAdVO.getUser())) {
                userAdVO.setSales(mySales);
            } else {
                Integer sale = salesMap.get(userAdVO.getUser());
                userAdVO.setSales(sale == null ? 0 : sale);
            }
        }


        // 查出所有研发完成的iso
        List<NewCtYfIso> ctYfIsoList = newCtYfIsoMapper.getYfFinishList(userAndOrderParam.getContestId(), userAndOrderParam.getStudentId());
        // 将研发完成的编号放入set中，便于查询
        Set<Integer> isoSet = ctYfIsoList.stream().map(NewCtYfIso::getIsoNum).collect(Collectors.toSet());
        // 将组号映射到广告额
        Map<String, Integer> groupAdMap = userAdVOList.stream().collect(Collectors.toMap(NewUserAdVO::getUser, NewUserAdVO::getProductAd, (k1, k2) -> k1));
        // 将组号映射到市场广告额
        Map<String, Integer> groupMarketAdMap = userAdVOList.stream().collect(Collectors.toMap(NewUserAdVO::getUser, NewUserAdVO::getMarketAd, (k1, k2) -> k1));


        List<NewCtMnChoose> chooseList = new ArrayList<>(CommonUtil.getListInitCap(ctMnChooseList.size()));
        Integer myAd = groupAdMap.get(userAndOrderParam.getName());
        Integer myMarketAd = groupMarketAdMap.get(userAndOrderParam.getName());
        myAd = (myAd == null ? 0 : myAd);
        myMarketAd = (myMarketAd == null ? 0 : myMarketAd);
        for (NewCtMnChoose ctMnChoose : ctMnChooseList) {
            if (userAndOrderParam.getName().equals(ctMnChoose.getXzGroup())) {
                continue;
            }
            // 滤掉选走轮次<回合数的
            if (ctMnChoose.getXzRound() != null && ctMnChoose.getXzRound() < userAndOrderParam.getRound()) {
                continue;
            }

            // 其他过滤
            if (ctMnChoose.getXzRound() != null && userAndOrderParam.getRound().equals(ctMnChoose.getXzRound())) {
                Integer otherAd = groupAdMap.get(ctMnChoose.getXzGroup());
                Integer otherMarketAd = groupMarketAdMap.get(ctMnChoose.getXzGroup());
                otherAd = (otherAd == null ? 0 : otherAd);
                otherMarketAd = (otherMarketAd == null ? 0 : otherMarketAd);
                if (myAd < otherAd) {
                    // 过滤选走轮次=回合数 且 选走组号的该空广告>该组这个空的广告 （ 广告值比自己高）
                    continue;
                } else if (myAd.equals(otherAd) && myMarketAd < otherMarketAd) {
                    // 过滤选走轮次=回合数 且 选走组号的该空广告=该组这个空的广告 且 选走组号该空的市场广告额>本组市场广告额 （市场广告额比自己高）
                    continue;
                } else if (myAd.equals(otherAd) && myMarketAd.equals(otherMarketAd)) {
                    // 过滤选走轮次=回合数 且 选走组号的该空广告=该组这个空的广告 且 选走组号该空的市场广告额=本组市场广告额 且上一年该市场总销售额>本组上一年该市场销售额（销售额比自己高）
                    Integer otherSales = salesMap.get(ctMnChoose.getXzGroup());
                    otherSales = (otherSales == null ? 0 : otherSales);
                    if (otherSales > mySales) {
                        continue;
                    }
                }
            }
            // 如果这个单据ISO为空，就是可以选。
            // 如果为1，那该组ISO9000已经开发完成就可以选
            // 如果为2，那该组ISO14000已经开发完成就可以选
            // 如果为3，那需要两个都开发完成才能选
            Integer iso = ctMnChoose.getCiId();
            // 判断需要的iso认证是否研发完成
            boolean isoFinish = ((iso == null) || ((iso <= 2) && isoSet.contains(iso)) || ((iso == 3) && isoSet.contains(1) && isoSet.contains(2)));
            ctMnChoose.setChoosable(isoFinish ? 1 : 0);
            chooseList.add(ctMnChoose);
        }
        return chooseList;
    }

    @Transactional
    public R chooseOrder(NewUserAndOrderParam userAndOrderParam) {
        NewCtMnChoose ctMnChoose = newCtMnChooseService.getById(userAndOrderParam.getChooseId());
        if (ctMnChoose == null || !ctMnChoose.getContestId().equals(userAndOrderParam.getContestId())) {
            return R.error(CodeEnum.PARAM_ERROR.getCode(), "该选单不存在");
        }

        // 当点击选中时，xz_order新增该条记录，模拟选单mn_choose更新选走组号，更新选走轮次=回合数
        SysUser sysUser = sysUserService.getById(userAndOrderParam.getStudentId());

        //判断当前单据是否已经被选中
        if(ctMnChoose.getXzGroup()!=null && ctMnChoose.getXzGroup().equals(sysUser.getName())){
            return R.error(CodeEnum.PARAM_ERROR.getCode(), "该选单已经被选中");
        }

        ctMnChoose.setXzGroup(sysUser.getName()).setXzRound(userAndOrderParam.getRound());
        newCtMnChooseService.updateById(ctMnChoose);
        NewCtXzOrder ctXzOrder = new NewCtXzOrder()
                .setCoId(ctMnChoose.getCoId())
                .setStudentId(userAndOrderParam.getStudentId())
                .setContestId(userAndOrderParam.getContestId())
                .setDate(ctMnChoose.getDate())
                .setQuarterly(ctMnChoose.getQuarterly())
                .setCmId(ctMnChoose.getCmId())
                .setCpId(ctMnChoose.getCpId())
                .setNum(ctMnChoose.getNum())
                .setTotalPrice(ctMnChoose.getTotalPrice())
                .setDeliveryDate(ctMnChoose.getDeliveryDate())
                .setPaymentDate(ctMnChoose.getPaymentDay())
                .setIsoId(ctMnChoose.getCiId())
                .setCommitDate(0);
        newCtXzOrderMapper.insert(ctXzOrder);
        return R.success();
    }


    /**
     * 补单
     * @return
     */
    @Transactional
    public R addOrder(NewCtMnChoose newCtMnChoose) {
        Integer studentId = newCtMnChoose.getStudentId();
        Integer contestId = newCtMnChoose.getContestId();
        NewCtMnChoose ctMnChoose  = newCtMnChooseService.getOne(new LambdaQueryWrapper<NewCtMnChoose>()
                .eq(NewCtMnChoose::getCoId, newCtMnChoose.getCoId())
                .eq(NewCtMnChoose::getContestId, newCtMnChoose.getContestId())
                .eq(NewCtMnChoose::getStudentId, newCtMnChoose.getStudentId())
                .last(" limit 1")
        );
        if (ctMnChoose == null ) {
            return R.error(CodeEnum.PARAM_ERROR.getCode(), "该订单编号不存在");
        }


        //判断当前单据是否已经被选中
        SysUser sysUser = sysUserService.getById(studentId);
        if(ctMnChoose.getXzGroup()!=null && ctMnChoose.getXzGroup().equals(sysUser.getName())){
            return R.error(CodeEnum.PARAM_ERROR.getCode(), "该订单编号已被改学生选中,无需重复补单");
        }

        NewContestStudent newContestStudent = newContestStudentMapper.get(contestId,studentId);

        Integer studentDate = newContestStudent.getDate()/10;
        Integer studentYear = studentDate/10;
        Integer studentQuarterly = studentDate%10;

        Integer year =  ctMnChoose.getDate();
        Integer quarterly =  ctMnChoose.getQuarterly();

        //判断是否是同年
        if(!year.equals(studentYear)){
            return R.error(CodeEnum.PARAM_ERROR.getCode(), "该学生当前时间节点为"+studentYear+"年"+studentQuarterly+"季,补单单号所在为"+year+"年"+quarterly+"季,该补单不合规");
        }

        //判断补单的季度是否大于学生当前季度
        if(quarterly>studentQuarterly){
            return R.error(CodeEnum.PARAM_ERROR.getCode(), "该学生当前时间节点为"+studentYear+"年"+studentQuarterly+"季,补单单号所在为"+year+"年"+quarterly+"季,该补单不合规");
        }


        //判断学生本季度是否参与了选单,如果没有参与选单的话，暂时不能补单
        if(quarterly.equals(studentQuarterly)){

         List<NewCtXzOrder> list  = newCtXzOrderMapper.selectList(new LambdaQueryWrapper<NewCtXzOrder>()
                    .eq(NewCtXzOrder::getContestId,contestId)
                    .eq(NewCtXzOrder::getStudentId,studentId)
                    .eq(NewCtXzOrder::getDate,year)
                    .eq(NewCtXzOrder::getQuarterly,quarterly)
         );
         if(CollectionUtils.isEmpty(list)){
             return R.error(CodeEnum.PARAM_ERROR.getCode(), "补单单号所在为"+year+"年"+quarterly+"季,当前季度该学生,暂无选单记录");
         }
        }



        ctMnChoose.setXzGroup(sysUser.getName()).setXzRound(0);
        newCtMnChooseService.updateById(ctMnChoose);


        NewCtXzOrder ctXzOrder = new NewCtXzOrder()
                .setCoId(ctMnChoose.getCoId())
                .setStudentId(studentId)
                .setContestId(contestId)
                .setDate(ctMnChoose.getDate())
                .setQuarterly(ctMnChoose.getQuarterly())
                .setCmId(ctMnChoose.getCmId())
                .setCpId(ctMnChoose.getCpId())
                .setNum(ctMnChoose.getNum())
                .setTotalPrice(ctMnChoose.getTotalPrice())
                .setDeliveryDate(ctMnChoose.getDeliveryDate())
                .setPaymentDate(ctMnChoose.getPaymentDay())
                .setIsoId(ctMnChoose.getCiId())
                .setCommitDate(0);
        newCtXzOrderMapper.insert(ctXzOrder);
        return R.success();
    }


    public R end(Integer contestId, Integer studentId) {
        //订货会结束先 按键状态
        NewContestStudent newContestStudent = new NewContestStudent().setStudentId(studentId).setContestId(contestId).setProgress("{\"fillReportForm\":true,\"adLaunch\":false,\"attendOrderMeeting\":false}");
        newContestStudentMapper.update(newContestStudent,new LambdaQueryWrapper<NewContestStudent>()
                .eq(NewContestStudent::getStudentId,studentId)
                .eq(NewContestStudent::getContestId,contestId)
        );

        newRestoreDataBiz.backupData(contestId, studentId);
        newRestoreDataBiz.backupSeasonData(contestId,studentId);
        return R.success();
    }

    @Transactional
    public R restoreOrder(Integer studentId, Integer contestId, Integer date) {

        // 删除本年本季的已选订单
        newCtXzOrderMapper.removeNowYear(contestId, studentId, date / 10,date%10);

        // 删除模拟选单
        newRestoreDataBiz.removeOrder(studentId, contestId);

        // 将模拟选单的备份数据加入模拟选单
        newRestoreDataBiz.backupOrder(studentId, contestId);
        return R.success();
    }
}
