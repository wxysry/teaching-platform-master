package com.xiyou.main.biz.newcontest;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.xiyou.common.utils.R;
import com.xiyou.main.dao.newcontest.NewCtCorporateMapper;
import com.xiyou.main.dao.newcontest.NewCtFeeMapper;
import com.xiyou.main.entity.contest.CtCashflow;
import com.xiyou.main.entity.contest.CtProfitChart;
import com.xiyou.main.entity.gbcontest.GbCtProfitChart;
import com.xiyou.main.entity.newcontest.*;
import com.xiyou.main.service.newcontest.*;
import com.xiyou.main.vo.newcontest.NewCashFolowEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

/**
 * @program: multi-module
 * @description:
 * @author: tangcan
 * @create: 2019-07-22 16:08
 **/
@Service
public class NewYearEndBiz {


    @Autowired
    private NewRestoreDataBiz restoreDataBiz;
    @Autowired
    private NewActionBiz newActionBiz;

    @Autowired
    private NewCtCashflowService newCtCashflowService;

    @Autowired
    private NewContestStudentService newContestStudentService;


    @Transactional
    public R confirm(Integer studentId, Integer contestId, Integer date) {

        //1.自动缴纳未缴管理费 ，扣社会责任
        //2.自动缴纳未缴纳贷款本金利息 ，扣社会责任
        //3.自动缴纳应付款未付，扣社会责任
        //4.自动订单违约扣款，扣社会责任
        newActionBiz.seasonEndAction(studentId,contestId,date);



        //计算四张报表
        Integer bsTotalEquity = newActionBiz.calBsTotalEquity(studentId,contestId,date);
        // 获取现金流量表余额
        Integer cash = newCtCashflowService.getCash(studentId, contestId);

        // 所有者权益为负后，自动结束比赛
        if (bsTotalEquity < 0) {
            return R.error(7000, "当前所有者权益为负，你已破产，模拟竞赛结束");
        }
        // 现金余额为负数，自动结束比赛
        if (cash < 0) {
            return R.error(7000, "当前现金余额为负数，你已破产，模拟竞赛结束");
        }
        return R.success();
    }


    /**
     * 当年结束后->继续下个季度
     * @param cashFolowEntity
     * @return
     */
    public R currentQuarterNext(NewCashFolowEntity cashFolowEntity) {

        Integer studentId = cashFolowEntity.getStudentId();
        Integer contestId = cashFolowEntity.getContestId();
        Integer currentTime = cashFolowEntity.getCurrentTime();

        //下季度当季开始
        newActionBiz.nextSeasonAction(studentId,contestId,currentTime);

        //计算四张报表
        Integer bsTotalEquity = newActionBiz.calBsTotalEquity(studentId,contestId,currentTime);
        // 获取现金流量表余额
        Integer cashAmount = newCtCashflowService.getCash(studentId, contestId);

        // 所有者权益为负后，自动结束比赛
        if (bsTotalEquity < 0) {
            return R.error(7000, "当前所有者权益为负，你已破产，模拟竞赛结束");
        }
        // 现金余额为负数，自动结束比赛
        if (cashAmount < 0) {
            return R.error(7000, "当前现金余额为负数，你已破产，模拟竞赛结束");
        }

        //  更新学生表
        //  progress: this.buttonVisible,
        //  contestId: this.contestId,
        //  date: this.year * 100 + this.season * 10 + this.status
        NewContestStudent newContestStudent = new NewContestStudent();
        newContestStudent.setContestId(contestId);
        newContestStudent.setStudentId(studentId);
        newContestStudent.setDate(currentTime*10+1);
        newContestStudent.setProgress("{\"fillReportForm\":true,\"adLaunch\":true,\"attendOrderMeeting\":false}");
        newContestStudentService.update(newContestStudent,
                new LambdaQueryWrapper<NewContestStudent>()
                        .eq(NewContestStudent::getContestId,contestId)
                        .eq(NewContestStudent::getStudentId,studentId)
        );


        //清除季度备份表数据 重新备份
        restoreDataBiz.backupData(contestId,studentId);
        //备份当年数据
        restoreDataBiz.backupSeasonData(contestId, studentId);

        return R.success();
    }



}



