package com.xiyou.main.biz.ngbcontest;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.xiyou.common.enums.CodeEnum;
import com.xiyou.common.exception.CustomException;
import com.xiyou.common.utils.R;
import com.xiyou.main.config.NGbWebSocket;
import com.xiyou.main.dao.ngbcontest.*;
import com.xiyou.main.entity.exam.SysUser;
import com.xiyou.main.entity.ngbcontest.*;
import com.xiyou.main.entity.ngbcontest.NGbRePaymentInfo;
import com.xiyou.main.enums.CashFlowActionEnum;
import com.xiyou.main.enums.ErrorEnum;
import com.xiyou.main.params.ngbcontest.*;
import com.xiyou.main.service.ngbcontest.*;
import com.xiyou.main.utils.DateUtils;
import com.xiyou.main.vo.ngbcontest.*;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.models.auth.In;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @author dingyoumeng
 * @since 2019/07/25
 */
@Service
@Transactional(rollbackFor = Exception.class)
@Slf4j
public class NGbActionBiz {
    @Autowired
    private NGbCtGzMaterialMapper gbCtGzMaterialMapper;
    @Autowired
    private NGbCtFeeService ctFeeService;
    @Autowired
    NGbCtCashflowService ctCashflowService;
    @Autowired
    NGbCtXzOrderService ctXzOrderService;
    @Autowired
    NGbCtKcProductService ctKcProductService;
    @Autowired
    NGbCtGzProductService ctGzProductService;
    @Autowired
    NGbCtYfProductService ctYfProductService;
    @Autowired
    NGbCtGzCsService ctGzCsService;
    @Autowired
    NGbCtKcMaterialService ctKcMaterialService;
    @Autowired
    NGbCtGzMaterialService ctGzMaterialService;
    @Autowired
    NGbCtYfMarketService ctYfMarketService;
    @Autowired
    NGbCtXzOrderMapper ctXzOrderMapper;
    @Autowired
    NGbCtFeeMapper ctFeeMapper;
    @Autowired
    private NGbCtGzProductMapper ctGzProductMapper;
    @Autowired
    private NGbCtGzIsoMapper ctGzIsoMapper;
    @Autowired
    private NGbRestoreDataBiz gbRestoreDataBiz;
    @Autowired
    private NGbCtGzMarketMapper ctGzMarketMapper;
    @Autowired
    private NGbCtYfProductMapper ctYfProductMapper;
    @Autowired
    private NGbCtCorporateMapper ctCorporateMapper;
    @Autowired
    private NGbApplyForLoanBiz applyForLoanBiz;
    @Autowired
    private NGbCtWorkerMapper nGbCtWorkerMapper;
    @Autowired
    private NGbCtLineMapper ctLineMapper;
    @Autowired
    private NGbCtYfMarketMapper ctYfMarketMapper;
    @Autowired
    private NGbCtYfIsoMapper ctYfIsoMapper;
    @Autowired
    private NGbCtYfNumMapper ctYfNumMapper;
    @Autowired
    private NGbCtGzDiscountMapper gbCtGzDiscountMapper;
    @Autowired
    private NGbCtLineService gbCtLineService;
    @Autowired
    private NGbCtChargesService ctChargesService;
    @Autowired
    private NGbCtProfitChartService ctProfitChartService;
    @Autowired
    private NGbCtBalanceService ctBalanceService;
    @Autowired
    private NGbCtFinancialTargetService gbCtFinancialTargetService;
    @Autowired
    private NGbCtBankLoanService ctBankLoanService;

    @Autowired
    private NGbCtBankLoanMapper ctBankLoanMapper;

    @Autowired
    private NGbCtCorporateMapper gbCtCorporateMapper;
    @Autowired
    private NGbCtDdMaterialMapper gbCtDdMaterialMapper;
    @Autowired
    private NGbApplyForLoanBiz gbApplyForLoanBiz;
    @Autowired
    private NGbContestService nGbContestService;
    @Autowired
    private NGbContestStudentMapper nGbContestStudentMapper;
    @Autowired
    private NGbCtCapitalMapper gbCtCapitalMapper;
    @Autowired
    private NGbContestStudentService gbContestStudentService;
    @Autowired
    private NGbDownloadBiz gbDownloadBiz;

    @Autowired
    private NGbCtMnAdMapper nGbCtMnAdMapper;

    @Autowired(required = false)
    private NGbWebSocket nGbWebSocket;

    @Autowired
    private NGbCtCarbonMapper nGbCtCarbonMapper;

    @Autowired
    private NGbCtScoreMapper nGbCtScoreMapper;

    @Autowired
    private NGbCtGzClassesMapper nGbCtGzClassesMapper;

    @Autowired
    private NGbLineBiz nGbLineBiz;

    @Autowired
    private NGbCtGzWorkerRecruitMapper nGbCtGzWorkerRecruitMapper;


    @Autowired
    private NGbCtAvgSalMapper nGbCtAvgSalMapper;

    /**
     * 减分数
     */
    public void minusScoreAndLog(Integer studentId,Integer contestId,String action,Integer actionTime){
        int score = ctCorporateMapper.getScore(studentId, contestId);
        score = Math.max(score -1,0); //最低不能低于0分
        NGbCtCorporate ctCorporate = new NGbCtCorporate();
        ctCorporate.setStudentId(studentId)
                .setContestId(contestId)
                .setScore(score)
                .setAction(action)
                .setActionTime(actionTime);
        ctCorporateMapper.insert(ctCorporate);
    }

//    public R checkReceivables(Integer userId, Integer contestId) {
//        QueryWrapper<NGbCtFee> queryWrapper = new QueryWrapper<>();
//        queryWrapper.eq("student_id", userId);
//        queryWrapper.eq("contest_id", contestId);
//        queryWrapper.eq("r_remain_date", 1);
//        NGbCtFee ctFee = ctFeeService.getOne(queryWrapper);
//        Map<String, Object> resultMap = new HashMap<>(16);
//        resultMap.put("fee", ctFee);
//        return R.success(resultMap);
//    }

    /**
     * 应收款确认
     * @param userId
     * @param contestId
     * @param feeId
     * @param currentTime
     * @return
     */
    public R confirmReceivables(Integer userId, Integer contestId,Integer feeId,Integer currentTime) {
        //阻止暂停状态下 和 破产状态下的用户进行操作
        preventAction(contestId,userId);


        NGbCtFee ctFee = ctFeeService.getById(feeId);
        if (ctFee == null) {
            return R.success();
        }
        Integer cash = ctCashflowService.getCash(userId, contestId);
        int remain = cash + ctFee.getRFee();
        //插入消费记录
        NGbCtCashflow ctCashflow = new NGbCtCashflow();
        ctCashflow.setCDate(currentTime)
                .setCSurplus(remain)
                .setCIn(ctFee.getRFee())
                .setCOut(0)
                .setCAction(CashFlowActionEnum.UPDATE_RECEIVABLE.getAction())
                .setContestId(contestId)
                .setStudentId(userId)
                .setCComment("更新应收款" + ctFee.getRFee() + "元");
        ctCashflowService.save(ctCashflow);
        ctFeeService.removeById(feeId);

        //计算四张报表,判断破产情况
        isBankruptcy(userId,contestId,currentTime);

        return R.success();
    }


    /**
     * 批量收款确认
     * @param userId
     * @param contestId
     * @param currentTime
     * @return
     */
    public R receivablesBatchConfirm(Integer userId, Integer contestId, Integer currentTime) {

        //阻止暂停状态下 和 破产状态下的用户进行操作
        preventAction(contestId,userId);

        List<NGbCtFee> list = ctFeeMapper.selectList(new LambdaQueryWrapper<NGbCtFee>()
                .eq(NGbCtFee::getContestId,contestId)
                .eq(NGbCtFee::getStudentId,userId)
                .eq(NGbCtFee::getType,NGbCtFee.TO_RECEIVABLE)
                .eq(NGbCtFee::getPaymentDate,currentTime)
                .orderByDesc(NGbCtFee::getRRemainDate) //根据账期倒序排序
        );

        for (NGbCtFee nGbCtFee : list) {
            Integer feeId = nGbCtFee.getFeeId();
            Integer cash = ctCashflowService.getCash(userId, contestId);
            int remain = cash + nGbCtFee.getRFee();
            //插入消费记录
            NGbCtCashflow ctCashflow = new NGbCtCashflow();
            ctCashflow.setCDate(currentTime)
                    .setCSurplus(remain)
                    .setCIn(nGbCtFee.getRFee())
                    .setCOut(0)
                    .setCAction(CashFlowActionEnum.UPDATE_RECEIVABLE.getAction())
                    .setContestId(contestId)
                    .setStudentId(userId)
                    .setCComment("更新应收款" + nGbCtFee.getRFee() + "元");
            ctCashflowService.save(ctCashflow);
            boolean result = ctFeeService.removeById(feeId);
            if(!result){
                throw new CustomException(CodeEnum.OTHER_ERROR.getCode(), "操作失败，请重试");
            }
        }
        //计算四张报表,判断破产情况
        isBankruptcy(userId,contestId,currentTime);

        return R.success();
    }




    /**
     * 应付款确认
     * @param userId
     * @param contestId
     * @param feeId
     * @param currentTime
     * @return
     */
    public R confirmPay(Integer userId, Integer contestId,Integer feeId,Integer currentTime) {
        NGbCtFee ctFee = ctFeeService.getById(feeId);
        if (ctFee == null) {
            return R.success();
        }
        Integer cash = ctCashflowService.getCash(userId, contestId);
        int remain = cash - ctFee.getRFee();
        if(remain < 0){
            return ErrorEnum.CASH_NOT_ENOUGH.getR();
        }
        //插入消费记录
        NGbCtCashflow ctCashflow = new NGbCtCashflow();
        ctCashflow.setCDate(currentTime)
                .setCSurplus(remain)
                .setCIn(0)
                .setCOut(ctFee.getRFee())
                .setCAction(CashFlowActionEnum.UPDATE_PAY.getAction())
                .setContestId(contestId)
                .setStudentId(userId)
                .setCComment("更新应付款" + ctFee.getRFee() + "W");
        ctCashflowService.save(ctCashflow);
        ctFeeService.removeById(feeId);
        return R.success();
    }

    /**
     * 查看所有订单
     * @param userId
     * @param contestId
     * @param date
     * @return
     */
    public R listOrder(Integer userId, Integer contestId, Integer date) {
        Map<String, Object> queryMap = new HashMap<>();
        queryMap.put("studentId", userId);
        queryMap.put("contestId", contestId);
//        queryMap.put("year", date / 10);
//        queryMap.put("date", date);
        //交单时间为空
//        queryMap.put("commitDate", 0);
        List<NGbXzOrderVo> list = ctXzOrderService.listDeliveryOrder(queryMap);
        List<NGbCtGzIso> ctGzIsoList = ctGzIsoMapper.list(contestId);
        Map<Integer, String> isoNameMap = ctGzIsoList.stream().collect(Collectors.toMap(NGbCtGzIso::getCiId, NGbCtGzIso::getCiName, (k1, k2) -> k1));
        list.forEach(p -> {
            //设置ISO
            if (p.getIsoId() == null) {
                p.setCiName("-");
            } else {
                p.setCiName(isoNameMap.get(p.getIsoId()));
            }
            //设置状态  违约/已交货/未交货

            String status = "";
            if (p.getCommitDate() != null && p.getCommitDate() > 0) {
                status = "已交货";
            } else if ((date / 10 == p.getDate() && date % 10 > p.getDeliveryDate()) || ((date / 10) > p.getDate())) {
                status = "已违约";
            }
            else{
                status = "可交货";
            }
            p.setStatus(status);
        });
        Map<String, Object> resultMap = new HashMap<>(16);
        resultMap.put("list", list);
        return R.success(resultMap);
    }

    /***
     * 交货
     * @param userId
     * @param contestId
     * @param orderId
     * @param date
     * @return
     */
    public R deliveryOrder(Integer userId, Integer contestId, Integer orderId, Integer date) {

        //阻止暂停状态下 和 破产状态下的用户进行操作
        preventAction(contestId,userId);

        NGbCtXzOrder order = ctXzOrderService.getById(orderId);//获取订单
        if (order == null) {
            return R.error(CodeEnum.BUSINESS_WRONG.getCode(), "无此订单");
        }

        //判断当前订单的状态避免重复交货
        if (order.getCommitDate() != null && order.getCommitDate() > 0) {
            return R.error(CodeEnum.BUSINESS_WRONG.getCode(), "当前订单已交货无需重复交货");
        }


        NGbCtGzProduct ctGzProduct = ctGzProductMapper.get(contestId, order.getCpId());//获取规则
        if (ctGzProduct == null) {
            return R.error(CodeEnum.BUSINESS_WRONG.getCode(), "无此产品");
        }


        //获取当前产品和特征的库存
        String designNum = order.getDesignNum();
        List<NGbCtKcProduct> listKc = ctKcProductMapper.getAllKcProductAsc(contestId, userId,ctGzProduct.getId(),designNum);//ASC产品库存

        int kCNum = listKc.size();
        if (kCNum < order.getAssignedNum()) {
            return R.error(CodeEnum.BUSINESS_WRONG.getCode(), "当前产品库存不足");
        }
        order.setCommitDate(date);
        ctXzOrderService.updateById(order);

        //计算成本 删除产品的库存
        int cost = 0;//成本
        //分配数量
        int assignedNum = order.getAssignedNum();
        for (int i = 0; i < listKc.size(); i++) {
            if(i < assignedNum){
                NGbCtKcProduct gbCtKcProduct = listKc.get(i);
                int realCost = gbCtKcProduct.getRealCost();
                cost = cost + realCost;
                //更新产品库存数为0
                gbCtKcProduct.setIpNum(0);
                ctKcProductMapper.updateById(gbCtKcProduct);
            }
        }

        order.setCommitDate(date).setTotalCost(cost);
        ctXzOrderService.updateById(order);

        //报价
        Integer applyPrice = order.getApplyPrice();
        int paymentDate = order.getPaymentDate();//账期
        //产生应收账单 此处不生成现金流量表 现金流量表在季末生成
            int payDate = DateUtils.addYearAndSeasonTime(date,paymentDate);//收款日期
            NGbCtFee ctFee = new NGbCtFee();
            ctFee.setStudentId(userId)
                    .setContestId(contestId)
                    .setBorrower("经销商")
                    .setLender("销售部")
                    .setRFee(applyPrice*assignedNum)  //应收款等于  单个报价 * 订单数量
                    .setRemarks("订单收款")
                    .setRRemainDate(paymentDate)
                    .setPaymentDate(payDate)
                    .setType(NGbCtFee.TO_RECEIVABLE);
            ctFeeService.save(ctFee);
//        }

        //计算四张报表,判断破产情况
        isBankruptcy(userId,contestId,date);

        return R.success();
    }

    /**
     * 研发产品列表
     * @param userId
     * @param contestId
     * @return
     */
    public R yfProductList(Integer userId, Integer contestId) {
        List<NGbYfProductVO> list = ctGzProductService.listYfProduct(userId, contestId);
        Map<String, Object> resultMap = new HashMap<>(16);
        resultMap.put("list", list);
        return R.success(resultMap);
    }

    /**
     * 研发产品
     * @param userId
     * @param yfParam
     * @return
     */
    public R yfProduct(Integer userId, NGbYfParam yfParam) {

        //阻止暂停状态下 和 破产状态下的用户进行操作
        preventAction(yfParam.getContestId(),userId);

        Integer cpId = yfParam.getCpId();
        NGbCtGzProduct gbCtGzProduct = ctGzProductService.getById(cpId);
        int depFeeSum = gbCtGzProduct.getCpProcessingFee();
        int cash = ctCashflowService.getCash(userId, yfParam.getContestId());
        int remain = cash - depFeeSum;
        if (remain < 0) {
            return ErrorEnum.CASH_NOT_ENOUGH.getR();
        }
        NGbCtCashflow ctCashflow = new NGbCtCashflow();
        ctCashflow.setCDate(yfParam.getDate())
                .setCSurplus(remain)
                .setCIn(0)
                .setCOut(depFeeSum)
                .setCAction(CashFlowActionEnum.DEVELOP_PRODUCT.getAction())
                .setContestId(yfParam.getContestId())
                .setStudentId(userId)
                .setCComment("扣减产品"+gbCtGzProduct.getCpName()+"研发" + depFeeSum + "元");
        ctCashflowService.save(ctCashflow);

        QueryWrapper<NGbCtYfProduct> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("student_id", userId);
        queryWrapper.eq("contest_id", yfParam.getContestId());
        queryWrapper.eq("dp_cp_id", gbCtGzProduct.getId());
        NGbCtYfProduct p = ctYfProductService.getOne(queryWrapper);
        if (NGbCtYfProduct.CP_YF_ING.equals(p.getDpState()) || NGbCtYfProduct.CP_YF_FINISH.equals(p.getDpState())) {
            return R.error(CodeEnum.OTHER_ERROR.getCode(), "无需重复研发");
        }
        Integer remainDate = gbCtGzProduct.getCpDevelopDate();
        Integer finishDate = DateUtils.addYearAndSeasonTime(yfParam.getDate(), remainDate);
        if (remainDate != null && remainDate > 0) {
            p.setDpStartDate(yfParam.getDate());
            p.setDpFinishDate(finishDate);
            p.setDpState(NGbCtYfProduct.CP_YF_ING);
        } else {        //研发周期为0的情况下，直接研发成功
            p.setDpStartDate(yfParam.getDate());//申请时间
            p.setDpFinishDate(yfParam.getDate());
            p.setDpState(NGbCtYfProduct.CP_YF_FINISH);
        }
        ctYfProductService.updateById(p);

        //计算四张报表,判断破产情况
        isBankruptcy(userId,yfParam.getContestId(),yfParam.getDate());

        return R.success();
    }

    /**
     * 季末操作
     * @param studentId
     * @param contestId
     * @param date
     */
    public void seasonEndAction(Integer studentId, Integer contestId, Integer date){
        //1.自动缴纳未缴管理费 ，扣社会责任
        //2.自动缴纳未缴纳贷款本金利息 ，扣社会责任
        //3.自动收货,并扣社会责任分
        //4.自动发薪,并扣社会责任
        //5.自动缴纳应付款未付，扣社会责任
        //6.自动订单违约扣款，扣社会责任
        //规则
        NGbCtGzCs ctGzCs = ctGzCsService.getByContestId(contestId);

        //费用管理-缴纳未缴纳的管理费用,并且扣社会责任得分
        NGbRePaymentInfo overhaul = applyForLoanBiz.getOverhaul(contestId, studentId, date);
        if(overhaul != null){
            applyForLoanBiz.payOverhaul(contestId,studentId ,date);
            minusScoreAndLog(studentId,contestId,"未及时缴纳缴管理费",date);
        }

        //费用管理-缴纳利息,并且扣社会责任得分  2.费用管理-缴纳本金,并且扣社会责任得分
        List<NGbRePaymentInfo> bankLoan = applyForLoanBiz.getBankLoan(date, contestId, studentId);
        //获取现金
        for (NGbRePaymentInfo info : bankLoan) {
            Integer cash = ctCashflowService.getCash(studentId, contestId);
            applyForLoanBiz.payCurrSeasonAddCashFlow(contestId,studentId,date,info.getAmount().intValue(),info.getPayment(),info.getBlAddTime(),cash);
            //扣社会责任分
            minusScoreAndLog(studentId,contestId,"贷款利息或本金未及时偿还",date);
        }

        //应收货强制收货并产生账单和扣分
        List<NGbCtDdMaterial> materialOrderRec = gbCtDdMaterialMapper.getMaterialOrderRec(studentId, contestId, date);
        for (NGbCtDdMaterial dd : materialOrderRec) {
            gbApplyForLoanBiz.inventory(dd.getId(), date);

            //第三年后默认开启了智能生产,不扣社会责任分
            if(date/10<3){
                //扣社会责任分
                minusScoreAndLog(studentId,contestId,"原材料订单未及时收货",date);
            }
        }


        // 工人欠薪-自动发薪并扣社会责任分
        //查询是否有欠薪的工人
        List<NGbCtWorker> qxList = nGbCtWorkerMapper.selectList(new LambdaQueryWrapper<NGbCtWorker>()
                .eq(NGbCtWorker::getContestId,contestId)
                .eq(NGbCtWorker::getStudentId,studentId)
                .eq(NGbCtWorker::getIsBackPay,1)
        );
        if(qxList!=null && qxList.size()>0){
            //给每一个工人发工资
            qxList.forEach(item->{
                gbApplyForLoanBiz.paySal(date,contestId,studentId,item,true,true);
            });
            //扣社会责任分
            minusScoreAndLog(studentId,contestId,"工人未及时发薪",date);
        }



        //应付款强制付款，并且扣社会责任得分
        QueryWrapper<NGbCtFee> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("student_id", studentId);
        queryWrapper.eq("contest_id", contestId);
        queryWrapper.eq("type", NGbCtFee.TO_PAYMENT);
        queryWrapper.le("payment_date",date);
        queryWrapper.orderByAsc("payment_date");
        List<NGbCtFee> list = ctFeeMapper.selectList(queryWrapper);
        for (NGbCtFee f : list) {
            Integer cash = ctCashflowService.getCash(studentId, contestId);
            //应付款付款
            paymentAndCashFlow(f,studentId,contestId,date,cash);
            //扣社会责任分
            minusScoreAndLog(studentId,contestId,"原材料订单未及时付款",date);
        }


        // 订单交货 -违约罚款并且扣社会责任得分
        //订单年份等于今年  当前季度 = 交货期， 且交货时间为空,则视为违约，需计算违约金
        List<NGbCtXzOrder> orderList = ctXzOrderService.list(new NGbCtXzOrder().setStudentId(studentId)
                .setContestId(contestId).setDate(date / 10).setDeliveryDate(date % 10));

        for (NGbCtXzOrder order : orderList) {
            //扣社会责任分
            minusScoreAndLog(studentId,contestId,"订单未交货",date);
        }
        // 违约金比例
        double punishRate = (ctGzCs == null ? 0.0 : (double) ctGzCs.getPunish() / 100.0);
        // 违约金 = 分配数量 * 报价 * 违约金比例
        int punish = orderList.stream().mapToInt(p -> (int) Math.round(p.getApplyPrice()*p.getAssignedNum()*punishRate)).sum();
        if(punish>0){
            Integer surplus = ctCashflowService.getCash(studentId, contestId);
            // Cashflow新一条记录ID、违约金、0、sum(四舍五入【xz_order.总价（当交单时间为空、年份=当前年份）*gz_cs.违约金比例】】-、现金+增加-减少、支付违约金、当前时间
            ctCashflowService.save(new NGbCtCashflow().setStudentId(studentId)
                    .setContestId(contestId)
                    .setCAction(CashFlowActionEnum.ORDERWYJ.getAction()) //违约金
                    .setCIn(0)
                    .setCOut(punish)
                    .setCSurplus(surplus - punish)
                    .setCComment("支付违约金" + punish + "元")
                    .setCDate(date));
        }
    }


    @Autowired
    private NGbCtGzWorkerTrainMapper nGbCtGzWorkerTrainMapper;


    public void updateStatus(Integer studentId, Integer contestId, int finishDate){

        log.error("contestId:{},studentId:{}当季开始更新状态-开始,时间:{}",contestId,studentId,finishDate);

        //下个季节的时间
        // 更新发放offer 的工人状态  将is_work = 1(招聘中) 且 is_recruit(录用成功)的工人状态修改为2
        nGbCtWorkerMapper.updateWorkerToOnboarding(contestId,studentId);

        //更新工人的涨薪
        nGbCtWorkerMapper.updateWorkerSal(contestId,studentId);


        //更新工人的培训状态  培训中->已完成 并且提高员工的薪资和等级
        List<NGbCtWorker> nGbCtWorkerList = nGbCtWorkerMapper.selectList(new LambdaQueryWrapper<NGbCtWorker>()
                .eq(NGbCtWorker::getContestId,contestId)
                .eq(NGbCtWorker::getStudentId,studentId)
                .eq(NGbCtWorker::getIsWork,4) //培训中
                .eq(NGbCtWorker::getTrainEndTime,finishDate) //培训结束时间
        );
        if(CollectionUtils.isNotEmpty(nGbCtWorkerList)){
            NGbContest nGbContest = nGbContestService.getById(contestId);
            Integer subjectNumber = nGbContest.getSubjectNumber();
            NGbCtGzWorkerTrain nGbCtGzWorkerTrain = nGbCtGzWorkerTrainMapper.selectOne(new LambdaQueryWrapper<NGbCtGzWorkerTrain>()
                    .eq(NGbCtGzWorkerTrain::getSubjectNum,subjectNumber)
                    .last(" limit 1")
            );


            NGbCtGzWorkerRecruit gjWorkerGz  = nGbCtGzWorkerRecruitMapper.selectOne(new LambdaQueryWrapper<NGbCtGzWorkerRecruit>()
                    .eq(NGbCtGzWorkerRecruit::getSubjectNum,subjectNumber)
                    .eq(NGbCtGzWorkerRecruit::getRecruitNum,"GR2") //高级工人
            );

            for (NGbCtWorker nGbCtWorker : nGbCtWorkerList) {
                //先获取涨薪比例
                BigDecimal salaryIncreasePercent = nGbCtGzWorkerTrain.getSalaryIncreasePercent();
                //原本工资
                Integer initSal = nGbCtWorker.getInitSal();
                BigDecimal initSalaryBigDecimal = new BigDecimal(initSal);
                BigDecimal salaryIncreaseAmount = initSalaryBigDecimal.multiply(salaryIncreasePercent).divide(new BigDecimal(100));
                BigDecimal newSalary = initSalaryBigDecimal.add(salaryIncreaseAmount).setScale(0, RoundingMode.HALF_UP);

                //提升等级
                nGbCtWorker.setRecruitName(gjWorkerGz.getRecruitName());  //岗位名称
                nGbCtWorker.setRecruitNum("GR2"); //岗位编码

                //提高薪酬
                nGbCtWorker.setNextInitSal(newSalary.intValue());

                //计件工资上调
                nGbCtWorker.setPiece(gjWorkerGz.getPiece());  //计件工资

                //修改状态 变为空闲
                nGbCtWorker.setIsWork(2);
                nGbCtWorkerMapper.updateById(nGbCtWorker);
            }
        }


        //修改工人的工资结清状态和工资类型
        //获取上季度在生产中的产线
        List<NGbCtLine> lineList = ctLineMapper.selectList(new LambdaQueryWrapper<NGbCtLine>()
                .eq(NGbCtLine::getContestId,contestId)
                .eq(NGbCtLine::getStudentId,studentId)
                .eq(NGbCtLine::getStatus,NGbCtLine.ON_PRODUCE)
        );
        List<Integer> lineIds = lineList.stream().map(item->item.getLineId()).collect(Collectors.toList());


        //如果员工空闲/培训中则 工资类型为 人力费
        nGbCtWorkerMapper.update(new NGbCtWorker(),new UpdateWrapper<NGbCtWorker>()
                .lambda()
                .eq(NGbCtWorker::getContestId,contestId)
                .eq(NGbCtWorker::getStudentId,studentId)
                .and(i->i.eq(NGbCtWorker::getIsWork,2) //空闲
                        .or()
                        .eq(NGbCtWorker::getIsWork,4) //培训中
                 )
                .lt(NGbCtWorker::getStartWorkTime,finishDate) //正式开始工作的时间要小于当前时间,才发工资(相当于入职下个月才发工资)
                .set(NGbCtWorker::getIsBackPay,1)  //是否欠薪
                .set(NGbCtWorker::getSalType,1)  //人力费
        );


        //如果存在生产中的产线
        if(lineIds.size()>0){
            nGbCtWorkerMapper.update(new NGbCtWorker(),new UpdateWrapper<NGbCtWorker>()
                    .lambda()
                    .eq(NGbCtWorker::getContestId,contestId)
                    .eq(NGbCtWorker::getStudentId,studentId)
                    .eq(NGbCtWorker::getIsWork,3) //在岗
                    .lt(NGbCtWorker::getStartWorkTime,finishDate) //正式开始工作的时间要小于当前时间,才发工资(相当于入职下个月才发工资)
                    .notIn(NGbCtWorker::getLineId,lineIds)   //在岗未生产
                    .set(NGbCtWorker::getIsBackPay,1)  //是否欠薪
                    .set(NGbCtWorker::getSalType,1)  //人力费
            );

            //如果员工生产中 工资类型为 基本工资
            nGbCtWorkerMapper.update(new NGbCtWorker(),new UpdateWrapper<NGbCtWorker>()
                    .lambda()
                    .eq(NGbCtWorker::getContestId,contestId)
                    .eq(NGbCtWorker::getStudentId,studentId)
                    .eq(NGbCtWorker::getIsWork,3) //在岗
                    .in(NGbCtWorker::getLineId,lineIds)  // 处于生产状态
                    .lt(NGbCtWorker::getStartWorkTime,finishDate) //正式开始工作的时间要小于当前时间,才发工资(相当于入职下个月才发工资)
                    .set(NGbCtWorker::getIsBackPay,1)  //基本工资
                    .set(NGbCtWorker::getSalType,2)  //基本工资
            );
        }
        //如果没有产线在生产中,那所有的在岗工人都发人力费
        else{
            nGbCtWorkerMapper.update(new NGbCtWorker(),new UpdateWrapper<NGbCtWorker>()
                    .lambda()
                    .eq(NGbCtWorker::getContestId,contestId)
                    .eq(NGbCtWorker::getStudentId,studentId)
                    .eq(NGbCtWorker::getIsWork,3) //在岗
                    .lt(NGbCtWorker::getStartWorkTime,finishDate) //正式开始工作的时间要小于当前时间,才发工资(相当于入职下个月才发工资)
                    .set(NGbCtWorker::getIsBackPay,1)  //是否欠薪
                    .set(NGbCtWorker::getSalType,1)  //人力费
            );
        }


        //计算本季度的应付平均工资
        List<NGbCtWorker> qxWokerList= nGbCtWorkerMapper.selectList(new LambdaQueryWrapper<NGbCtWorker>()
                .eq(NGbCtWorker::getContestId,contestId)
                .eq(NGbCtWorker::getStudentId,studentId)
                .eq(NGbCtWorker::getIsBackPay,1)  //欠薪
        );
        NGbCtAvgSal nGbCtAvgSal  = new NGbCtAvgSal();
        nGbCtAvgSal.setContestId(contestId);
        nGbCtAvgSal.setStudentId(studentId);
        nGbCtAvgSal.setDate(finishDate);
        if(CollectionUtils.isNotEmpty(qxWokerList)){
            //总工资
            int salTotal = qxWokerList.stream().mapToInt(NGbCtWorker::getInitSal).sum();
            //平均工资
            BigDecimal bigDecimal = new BigDecimal(salTotal).divide(new BigDecimal(qxWokerList.size()),0,BigDecimal.ROUND_HALF_UP);
            int avgSal = bigDecimal.intValue();
            nGbCtAvgSal.setSalTotal(salTotal);
            nGbCtAvgSal.setWorkerNum(qxWokerList.size());
            nGbCtAvgSal.setAvgSal(avgSal);
        }else{
            nGbCtAvgSal.setSalTotal(0);
            nGbCtAvgSal.setWorkerNum(0);
            nGbCtAvgSal.setAvgSal(0);
        }
        nGbCtAvgSalMapper.insert(nGbCtAvgSal);




        // 更新生产线 建成状态  建造中-> 空闲
        ctLineMapper.updateLineStatusBuildToSpace(contestId,studentId,finishDate,NGbCtLine.ON_BUILD,NGbCtLine.ON_SPACE);
        // 更新生产线 转产状态  转产中-> 空闲
        ctLineMapper.updateLineStatusTransferToSpace(contestId,studentId,finishDate, NGbCtLine.ON_TRANSFER,NGbCtLine.ON_SPACE);



        //获取即将在本季度完工的产线，
        List<NGbCtLine> finishLineList = lineList.stream().filter(item-> item.getPlProductingDate()==finishDate).collect(Collectors.toList());
        finishLineList.forEach(item->{
            Integer lineId = item.getLineId();
            Integer classesId = item.getClassesId();
            NGbCtGzClasses nGbCtGzClasses = nGbCtGzClassesMapper.selectById(classesId);
            BigDecimal efficiencyLoss = nGbCtGzClasses.getEfficiencyLoss();
            // 工人的效率根据班次规则效率降低
            List<NGbCtWorker> lineWorkerList = nGbCtWorkerMapper.selectList(new LambdaQueryWrapper<NGbCtWorker>()
                    .eq(NGbCtWorker::getContestId,contestId)
                    .eq(NGbCtWorker::getStudentId,studentId)
                    .eq(NGbCtWorker::getIsWork,3) // 在岗
                    .eq(NGbCtWorker::getLineId,lineId) //当前产线
            );
            lineWorkerList.forEach(i->{
                Integer multBonus = i.getMultBonus(); //原效率
                multBonus = multBonus-efficiencyLoss.intValue(); //效率降低
                i.setMultBonus(multBonus);
                nGbCtWorkerMapper.updateById(i);
            });
            // 产线的产能需要重新计算
            nGbLineBiz.reCalRealProduction(lineId);
        });



        // 更新生产线 生产状态  生产中->空闲 ，产品数量++
        ctLineMapper.updateLineStatusProduceToSpace(contestId,studentId,finishDate,NGbCtLine.ON_PRODUCE,NGbCtLine.ON_SPACE);

        //修改产品入库状态
        ctKcProductMapper.updateProductToFinish(contestId,studentId,finishDate);



        //将员工这个月的工资分摊到产品的成本中
        //获取还在生产状态的产线
        List<NGbCtLine> productingLineList = ctLineMapper.selectList(new LambdaQueryWrapper<NGbCtLine>()
                .eq(NGbCtLine::getContestId,contestId)
                .eq(NGbCtLine::getStudentId,studentId)
                .eq(NGbCtLine::getStatus,NGbCtLine.ON_PRODUCE)
        );
        if(CollectionUtils.isNotEmpty(productingLineList)){
            productingLineList.forEach(item->{
                //产量
                Integer realProduction = item.getRealProduction();
                //获取这条产线的工人
                List<NGbCtWorker> workerOnLines = nGbCtWorkerMapper.getAllWorkerListByLineId(item.getLineId());
                //工人的这个月的基本工资(下个月发放)
                Integer jbgz = workerOnLines.stream().mapToInt(i->i.getNextInitSal()*3).sum();
                //工资分摊到每个产品的成本中
                Integer gzft = jbgz/realProduction;
                List<NGbCtKcProduct> kcProductList = ctKcProductMapper.selectList(new LambdaQueryWrapper<NGbCtKcProduct>()
                        .eq(NGbCtKcProduct::getLineId,item.getLineId())
                );
                for (NGbCtKcProduct nGbCtKcProduct : kcProductList) {
                    Integer newRealCost = nGbCtKcProduct.getRealCost() + gzft; //增加员工的工资成本
                    nGbCtKcProduct.setRealCost(newRealCost);
                }
                ctKcProductService.updateBatchById(kcProductList);
            });
            //获取产线对应的工人的工资
            //将分摊的工资加到对应的库存产品中
        }




        // 更新产品研发状态
        ctYfProductMapper.updateStateToFinish(studentId,contestId,finishDate);
        // 更新市场研发状态
        ctYfMarketMapper.updateStateToFinish(studentId,contestId,finishDate);
        // 更新ISO研发状态
        ctYfIsoMapper.updateStateToFinish(studentId,contestId,finishDate);
        // 更新数字化研发状态
        ctYfNumMapper.updateStateToFinish(studentId,contestId,finishDate);

        log.error("contestId:{},studentId:{}当季开始更新状态-结束,时间:{}",contestId,studentId,finishDate);
    }


    /**
     * 扣除维修费
     * @param studentId
     * @param contestId
     * @param date
     */
    public void payMaintenanceFee(Integer studentId, Integer contestId, Integer date){
        Integer surplus = ctCashflowService.getCash(studentId, contestId);
        int maintenanceFee = gbCtLineService.getMaintenanceFee(studentId,contestId,date);
        // cashflow新增一条记录ID、维护费、0、sum[(当前line中PL_Remain_Date为null)*生产线所对应的维护费】、现金+增加-减少、支付维护费、当前时间
        if(maintenanceFee >0) {
            ctCashflowService.save(new NGbCtCashflow().setStudentId(studentId)
                    .setContestId(contestId)
                    .setCAction(CashFlowActionEnum.PAY_MAINTENANCE.getAction())
                    .setCIn(0)
                    .setCOut(maintenanceFee)
                    .setCSurplus(surplus - maintenanceFee)
                    .setCComment("支付维修费" + maintenanceFee + "元")
                    .setCDate(date));
        }
    }


    @Autowired
    private NGbCtMemberIndexMapper nGbCtMemberIndexMapper;


    /**
     * 下个季节需要做的事
     * @param studentId
     * @param contestId
     * @param nextSeason
     * @return
     */
    public R  nextSeasonAction(Integer studentId, Integer contestId, Integer nextSeason){

        NGbContest nGbContest = nGbContestService.getById(contestId);
        int lastSeason = DateUtils.addYearAndSeasonTime(nextSeason,-1);

        // 当季结束之前判断学生当前的时间是否与传入的时间一致
        Integer studentDate  = gbContestStudentService.getStudentDate(contestId,studentId);
        Integer studentCurrentTime = studentDate/10;
        if(studentCurrentTime!=lastSeason){
            log.error("当季开始异常,学生:{},contestId:{},date:{},执行当季开始时，学生时间异常，操作失败，请刷新页面后重试",studentId,contestId,nextSeason);
            throw new CustomException(CodeEnum.OTHER_ERROR.getCode(), "执行当季开始时，学生时间异常，操作失败，请刷新页面后重试");
        }

        //计算碳排放分配  第一年 和  第二年的碳排放分配为 0
        //第三年 和 第四年的碳排放 公式是 （除了他存货活所有组总的碳排放量）/（存活组数N-1）。
        if(nextSeason==21){
            NGbCtCarbon nGbCtCarbon  = new NGbCtCarbon();
            nGbCtCarbon.setYear(2);
            nGbCtCarbon.setContestId(contestId);
            nGbCtCarbon.setStudentId(studentId);
            nGbCtCarbon.setEmissionNum(0);
            nGbCtCarbon.setNeutralizaNum(0);
            nGbCtCarbonMapper.insert(nGbCtCarbon);
        }else if(nextSeason==31 || nextSeason==41){

            //获取初始碳排放量
            NGbCtGzCs nGbCtGzCs = ctGzCsService.getByContestId(contestId);
            int initCarbon = nGbCtGzCs.getCarbonInit();


            //获取上个季度末没有破产的客户
            List<NGbContestStudent> studentList= gbContestStudentService.list(new LambdaQueryWrapper<NGbContestStudent>()
                    .eq(NGbContestStudent::getContestId,contestId));
            //获取未破产或者 破产时间 大于当年时间的
            List<Integer> studentIdList = studentList.stream().filter(item->
                    (item.getIsBankruptcy()== 0) || (item.getIsBankruptcy()== 1 && item.getBankruptcyTime()>lastSeason)
            )
                    .filter(item->!studentId.equals(item.getStudentId()))  //过滤掉自己的碳排放
                    .map(item->item.getStudentId()).collect(Collectors.toList());


            Integer assignedNum = 0;
            if(nextSeason==31){
                if(studentIdList!=null &&  studentIdList.size()>0){
                    //获取第一年 和 第二年的碳排放
                    List<NGbCtCarbon> nGbCtCarbonList = nGbCtCarbonMapper.selectList(new LambdaQueryWrapper<NGbCtCarbon>()
                            .eq(NGbCtCarbon::getContestId,contestId)
                            .lt(NGbCtCarbon::getYear,3)  //年份小于三,即第一年和第二年
                            .in(NGbCtCarbon::getStudentId,studentIdList)
                    );
                    Integer carbonTotal = nGbCtCarbonList.stream().mapToInt(NGbCtCarbon::getEmissionNum).sum();
                    assignedNum = new BigDecimal(carbonTotal).add(new BigDecimal(initCarbon)).divide(new BigDecimal(studentIdList.size()),0,RoundingMode.HALF_UP).intValue();
                }
            }else if(nextSeason==41){
                if(studentIdList!=null &&  studentIdList.size()>0){
                    //获取第三年的碳排放
                    List<NGbCtCarbon> nGbCtCarbonList = nGbCtCarbonMapper.selectList(new LambdaQueryWrapper<NGbCtCarbon>()
                            .eq(NGbCtCarbon::getContestId,contestId)
                            .eq(NGbCtCarbon::getYear,3)
                            .in(NGbCtCarbon::getStudentId,studentIdList)
                    );
                    Integer carbonTotal = nGbCtCarbonList.stream().mapToInt(NGbCtCarbon::getEmissionNum).sum();
                    assignedNum = new BigDecimal(carbonTotal).divide(new BigDecimal(studentIdList.size()),0,RoundingMode.HALF_UP).intValue();
                }
            }
            NGbCtCarbon nGbCtCarbon  = new NGbCtCarbon();
            nGbCtCarbon.setYear(nextSeason/10);
            nGbCtCarbon.setContestId(contestId);
            nGbCtCarbon.setStudentId(studentId);
            nGbCtCarbon.setAssignedNum(assignedNum);  //碳排放分配
            nGbCtCarbon.setEmissionNum(0);
            nGbCtCarbon.setNeutralizaNum(0);
            nGbCtCarbonMapper.insert(nGbCtCarbon);
        }

        log.error("当季开始-开始,学生:{},contestId:{}",studentId,contestId);
        //更新学生的季度
        Integer date = nextSeason * 10 + 1;
        nGbContestStudentMapper.saveProgress(studentId,contestId,date,"{\"fillReportForm\":true}");
        log.error("当季开始-开始,学生:{},contestId:{},date:{}",studentId,contestId,date);

        int year = nextSeason/10;
         int season = nextSeason%10;
        //判断当季属于哪个季度，每年一季度要扣税
        if((year!=1)&&(season==1)){
            //支付所得税
            // 上一年报表计算的所得税
            List<NGbCtProfitChart> profitChartList = ctProfitChartService.list(new NGbCtProfitChart().setStudentId(studentId).setContestId(contestId).setPcDate((nextSeason / 10) - 1));
            int lastYearTax = profitChartList.stream().mapToInt(NGbCtProfitChart::getPcTax).sum();

            int cash = ctCashflowService.getCash(studentId, contestId);
            ctCashflowService.save(new NGbCtCashflow().setStudentId(studentId)
                    .setContestId(contestId)
                    .setCAction("支付所得税")
                    .setCIn(0)
                    .setCOut(lastYearTax)
                    .setCSurplus(cash - lastYearTax)
                    .setCComment("支付所得税" + lastYearTax + "元")
                    .setCDate(nextSeason));
        }

        //维修费
        payMaintenanceFee(studentId,contestId, nextSeason);
        //折旧计提
        // 更新累计折旧
        gbCtLineService.updateDepFee(studentId, contestId, nextSeason);


        //折旧费计入统计报表
        //1. 先生成四张报表，用于保存折旧费，后面会被覆盖
        calBsTotalEquity(studentId,contestId,nextSeason,false);
        // 计算本季度的折旧费  + 本年前几个季度的折旧费合计 ，将折旧费存到 profit表的折旧费字段。
        //2.先获取本季度新产生的折旧费
        Integer thisSeasonDep = gbCtLineService.getDepTotal(studentId, contestId, nextSeason);
        //3.获取本年上几个季度的折旧费
        //获取本年已计算的折旧费
        NGbCtProfitChart  gbCtProfitChartTemp= ctProfitChartService.getOne(new QueryWrapper<NGbCtProfitChart>()
                .eq("student_id",studentId)
                .eq("contest_id",contestId)
                .eq("pc_date",nextSeason/10)
                .eq("bs_isxt",1)//系统自动生成
        );
        //4  更新折旧费字段的值
        gbCtProfitChartTemp.setPcDep(gbCtProfitChartTemp.getPcDep()+thisSeasonDep);
        ctProfitChartService.updateById(gbCtProfitChartTemp);



        //更新产线/产品/研发(产品、市场、数字化)状态
        updateStatus(studentId,contestId, nextSeason);


        //本科的时候
        if(nGbContest.getContestType()==1){
            //初始话当前季度的会员指数,上季度会员指数减半，做个这个月的基础会员指数
            //本科初始化会员指数
            Integer lastIndex = 0;
            NGbCtMemberIndex lastMemberIndex = nGbCtMemberIndexMapper.selectOne(new LambdaQueryWrapper<NGbCtMemberIndex>()
                    .eq(NGbCtMemberIndex::getContestId,contestId)
                    .eq(NGbCtMemberIndex::getStudentId,studentId)
                    .eq(NGbCtMemberIndex::getYear,lastSeason/10)
                    .eq(NGbCtMemberIndex::getQuarterly,lastSeason%10)
            );
            if(lastMemberIndex!=null){
                lastIndex = lastMemberIndex.getMemberIndex();
            }
            int memberIndex = lastIndex/2; //当季会员指数
            NGbCtMemberIndex nGbCtMemberIndex = new NGbCtMemberIndex();
            nGbCtMemberIndex.setContestId(contestId);
            nGbCtMemberIndex.setStudentId(studentId);
            nGbCtMemberIndex.setYear(nextSeason/10);
            nGbCtMemberIndex.setQuarterly(nextSeason%10);
            nGbCtMemberIndex.setHeatNum(0);
            nGbCtMemberIndex.setMemberIndex(memberIndex);
            nGbCtMemberIndexMapper.insert(nGbCtMemberIndex);
        }



        //计算报表,判断是否破产
        isBankruptcy(studentId,contestId,nextSeason);

        //备份当季数据时间节点+1作为下季的季初数据
        gbRestoreDataBiz.backupData(contestId,studentId,nextSeason,false);
        return R.success();
    }


    /**
     * 当季结束
     * @param studentId
     * @param contestId
     * @param date
     * @return
     */
    public  R endingSeason(Integer studentId, Integer contestId, int date) {

        // 当季结束之前判断学生当前的时间是否与传入的时间一致
        Integer studentDate  = gbContestStudentService.getStudentDate(contestId,studentId);
        Integer studentCurrentTime = studentDate/10;
        if(studentCurrentTime!=date){
            log.error("当季结束异常,学生:{},contestId:{},date:{},执行当季结束时,学生时间异常,操作失败,请刷新页面后重试",studentId,contestId,date);
            throw new CustomException(CodeEnum.OTHER_ERROR.getCode(), "执行当季结束时,学生时间异常,操作失败,请刷新页面后重试");
        }


        //如果是第一年第一季度  或 第二年第一季度的PDCA没有填写,则自动填写默认值(全部填0)
        if(date==11 || date==21){
            Integer year = date/10;
            NGbCtPdca nGbCtPdca = nGbCtPdcaMapper.selectOne(new LambdaQueryWrapper<NGbCtPdca>()
                    .eq(NGbCtPdca::getYear,year)
                    .eq(NGbCtPdca::getContestId,contestId)
                    .eq(NGbCtPdca::getStudentId,studentId)
            );
            //如果查询出来结果为空
            if(nGbCtPdca==null){
                //设置默认值
                NGbCtPdca nGbCtPdca1 = new NGbCtPdca();
                nGbCtPdca1.setDefaultValue(contestId,studentId,year);
                nGbCtPdcaMapper.insert(nGbCtPdca1);
            }
        }


        //判断当前季度是否制定预算,如果没有指定预算,给一个默认值
        NGbCtBudget nGbCtBudget = nGbCtBudgetService.getOne(new LambdaQueryWrapper<NGbCtBudget>()
                .eq(NGbCtBudget::getContestId,contestId)
                .eq(NGbCtBudget::getStudentId,studentId)
                .eq(NGbCtBudget::getYear,date/10)
                .eq(NGbCtBudget::getQuarterly,date%10)
        );
        if(nGbCtBudget==null){
            nGbCtBudget = NGbCtBudget.getDefaultBudget(contestId,studentId,date/10,date%10);
            nGbCtBudgetService.save(nGbCtBudget);
        }



        NGbContestStudent gbContestStudent= gbContestStudentService.getOne(new LambdaQueryWrapper<NGbContestStudent>()
                .eq(NGbContestStudent::getStudentId,studentId)
                .eq(NGbContestStudent::getContestId,contestId)
                );
        //校验一下当前后台数据是否已经发生了变化
        int oldDate = gbContestStudent.getDate()/10;
        if(oldDate!=date){
            throw new CustomException(CodeEnum.OTHER_ERROR.getCode(), "操作失败，请重试");
        }


        //1.自动缴纳未缴管理费 ，扣社会责任
        //2.自动缴纳未缴纳贷款本金利息 ，扣社会责任
        //3.自动收货,并扣社会责任分
        //4.自动发薪,并扣社会责任
        //5.自动缴纳应付款未付，扣社会责任
        //6.自动订单违约扣款，扣社会责任
        seasonEndAction(studentId,contestId, date);

        //计算报表,判断是否破产
        isBankruptcy(studentId,contestId,date);


        //判断是当年结束还是当季结束
        int season =date %10;
        //当季结束.直接进入下一个季度
        if(season!=4){
            //保存当季结束时间
            gbContestStudentService.insertStudentEndTime(contestId,studentId,date);

            //判断该用户是否是处于统一模式下
            if(studentIsSyn(studentId,contestId)){
                //当年结束更新学生的填表状态
                int updateRow = nGbContestStudentMapper.update(new NGbContestStudent()
                                .setDate(date*10+6)
                                .setProgress("{\"fillReportForm\":true}"),
                        new LambdaQueryWrapper<NGbContestStudent>()
                                .eq(NGbContestStudent::getContestId,contestId)
                                .eq(NGbContestStudent::getStudentId,studentId)
                                .eq(NGbContestStudent::getDate,gbContestStudent.getDate())
                );
                if(updateRow!=1){
                    throw new CustomException(CodeEnum.OTHER_ERROR.getCode(), "操作失败，请重试");
                }
                return R.success();
            }
            //非统一模式下，直接进入下季度。
            else{
                //获取下个季度的时间(折旧计提，维修费，更新产品状态)
                Integer nextSeason = DateUtils.addYearAndSeasonTime(date,1);
                return nextSeasonAction(studentId,contestId,nextSeason);
            }
        }
        //到达填写报表阶段
        else if(season==4){
            //当年结束，重新计算报表的时候，需要计算所得税
            calBsTotalEquity(studentId,contestId,date,true);

            //当年结束更新学生的填表状态
           int updateRow = nGbContestStudentMapper.update(new NGbContestStudent()
                            .setDate(date*10+5)
                            .setProgress("{\"fillReportForm\":true}"),
                    new LambdaQueryWrapper<NGbContestStudent>()
                            .eq(NGbContestStudent::getContestId,contestId)
                            .eq(NGbContestStudent::getStudentId,studentId)
                            .eq(NGbContestStudent::getDate,gbContestStudent.getDate())
            );
           if(updateRow!=1){
               throw new CustomException(CodeEnum.OTHER_ERROR.getCode(), "操作失败，请重试");
           }
            return  R.success();
        }else{
            throw  new CustomException(777,"季度错误");
        }
    }

    /**
     * 判断学生是否处于同一模式下
     * @return
     */
    public boolean studentIsSyn(int studentId,int contestId){
        //获取考试信息
        NGbContest gbContest = nGbContestService.getOne(new LambdaQueryWrapper<NGbContest>().eq(NGbContest::getContestId,contestId));
        if(1==gbContest.getIsSyn()){
            //教师时间
           int teachDate = gbContest.getTeacherDate();
           int year = teachDate/10;

           //获取学生的基本信息
           NGbContestStudent gbContestStudent = gbContestStudentService.getOne(new LambdaQueryWrapper<NGbContestStudent>()
                    .eq(NGbContestStudent::getContestId,contestId)
                    .eq(NGbContestStudent::getStudentId,studentId)
            );
            //学生的时间线
            int studentDate = gbContestStudent.getDate()/10;
            //满足三个条件，统一模式 时间线与大部队一致，则视为统一模式
            return (studentDate==teachDate);
        }else{
            return  false;
        }
    }


    @Autowired
    private NGbCtRetailAssignService nGbCtRetailAssignService;

    /**
     * 生成报表
     * @param studentId
     * @param contestId
     * @param date
     * @return
     */
    public Integer calBsTotalEquity(Integer studentId, Integer contestId, Integer date,boolean calTax){
        if((date/10)==0){
            throw  new RuntimeException("数据异常");
        }

        //获取本年已计算的折旧费
        NGbCtProfitChart  gbCtProfitChartTemp= ctProfitChartService.getOne(new QueryWrapper<NGbCtProfitChart>()
                .eq("student_id",studentId)
                .eq("contest_id",contestId)
                .eq("pc_date",date/10)
                .eq("bs_isxt",1)//系统自动生成
        );
        Integer depTotal = 0;
        if(gbCtProfitChartTemp!=null){
            depTotal = gbCtProfitChartTemp.getPcDep();
        }


        // gz_cs的管理费
        NGbCtGzCs ctGzCs = ctGzCsService.getByContestId(contestId);
        //先删除本年的这四张报表
        ctChargesService.remove(new QueryWrapper<NGbCtCharges>()
                .eq("student_id",studentId)
                .eq("contest_id",contestId)
                .eq("c_date",date/10)
        );
        ctProfitChartService.remove(new QueryWrapper<NGbCtProfitChart>()
                .eq("student_id",studentId)
                .eq("contest_id",contestId)
                .eq("pc_date",date/10)
        );
        ctBalanceService.remove(new QueryWrapper<NGbCtBalance>()
                .eq("student_id",studentId)
                .eq("contest_id",contestId)
                .eq("bs_year",date/10)
        );
        gbCtFinancialTargetService.remove(new QueryWrapper<NGbCtFinancialTarget>()
                .eq("student_id",studentId)
                .eq("contest_id",contestId)
                .eq("ft_year",date/10)
        );


        //重新生成四张报表
        // 费用表charges。当年结束后，自动插入一条记录到charges
        NGbCtCharges charges = new NGbCtCharges().setStudentId(studentId).setContestId(contestId).setCDate(date / 10);
        //生成费用表数据并计算合计值
        ctChargesService.addBySys(charges);
        ctChargesService.updateTotal(charges.getChargesId());


        /*
        添加记录到利润表profit_chart
         */
        NGbCtProfitChart profitChart = new NGbCtProfitChart().setStudentId(studentId).setContestId(contestId).setBsIsxt(1).setPcDate(date / 10).setIsSubmit("Y");
        // 销售额 = Sum(已选定单xz_order.总价) 交单时间=本年
        // 成本 =  Sum已选定单xz_order.数量*对应产品成本)交单时间=本年
        Map<String, BigDecimal> salesAndDirectCostMap = ctXzOrderService.getSalesAndDirectCost(studentId, contestId, date / 10);
        BigDecimal sales = salesAndDirectCostMap.get("sales");
        BigDecimal directCost = salesAndDirectCostMap.get("directCost");


        //零售市场的  销售额 和直接成本
        List<NGbCtRetailAssign> nGbCtRetailAssigns = nGbCtRetailAssignService.list(new LambdaQueryWrapper<NGbCtRetailAssign>()
                .eq(NGbCtRetailAssign::getContestId,contestId)
                .eq(NGbCtRetailAssign::getStudentId,studentId)
                .eq(NGbCtRetailAssign::getYear,date/10)
        );
       Integer lsSale = nGbCtRetailAssigns.stream().mapToInt(item->item.getApplyPrice() * item.getDeliveryNum()).sum();
       Integer lsDirectCost = nGbCtRetailAssigns.stream().mapToInt(NGbCtRetailAssign::getTotalCost).sum();


        profitChart.setPcSales(sales.intValue()+lsSale);
        profitChart.setPcDirectCost(directCost.intValue()+lsDirectCost);

        // 毛利=销售额-成本
        profitChart.setPcGoodsProfit(profitChart.getPcSales() - profitChart.getPcDirectCost());
        // 管理费用=Charges今年系统填写的总和数据
        profitChart.setPcTotal(ctChargesService.getTotal(studentId, contestId, date / 10, 1));
        // 折旧前利润=毛利-管理费用
        profitChart.setPcProfitBeforeDep(profitChart.getPcGoodsProfit() - profitChart.getPcTotal());

        //计算折旧费
        profitChart.setPcDep(depTotal);
        // 财务费用前利润=折旧前利润-折旧
        profitChart.setPcProfitBeforeInterests(profitChart.getPcProfitBeforeDep() - profitChart.getPcDep());


        // 财务费用=Sum(cashflow.流出)当操作类型 in （"本息同还-利息", "每季付息-利息", "贴现"）
        List<String> actionList = Arrays.asList("本息同还-利息", "每季付息-利息", "贴现");
        List<NGbCtCashflow> cashflowList = ctCashflowService.list(studentId, contestId, date / 10, actionList);
        profitChart.setPcFinanceFee(cashflowList.stream().mapToInt(NGbCtCashflow::getCOut).sum());

        //营业外收支:  出售库存  出售原材料， 变卖生产线， 违约金
        List<NGbCtCashflow> yywCashflowList = ctCashflowService.list(studentId, contestId, date / 10, Arrays.asList(CashFlowActionEnum.SELL_PRODUCT.getAction(), CashFlowActionEnum.SELL_MATERIAL.getAction(), CashFlowActionEnum.SELL_PRODUCT_LINE.getAction(),CashFlowActionEnum.ORDERWYJ.getAction()));
        profitChart.setPcNonOperating(yywCashflowList.stream().mapToInt(x->0-x.getCOut()).sum());
        // 税前利润=财务费用前利润-财务费用
        profitChart.setPcProfitBeforeTax(profitChart.getPcProfitBeforeInterests() - profitChart.getPcFinanceFee() + profitChart.getPcNonOperating()) ;
        // 税前利润
        int profitBeforeTax = profitChart.getPcProfitBeforeTax();


        // 上一年资产负债表所有者权益
        NGbCtBalance lastYearBalance = ctBalanceService.get(new NGbCtBalance().setStudentId(studentId).setContestId(contestId).setBsYear(date / 10 - 1));
        //上一年所有者权益
        int lastYearTotalEquity = lastYearBalance == null ? 0 : lastYearBalance.getBsTotalEquity();
        //上一年股东资本
        int lastYearBsEquity = lastYearBalance == null ? 0 : lastYearBalance.getBsEquity();

        //判断是否计算所得税
        if(calTax){
            // 所得税: 当【税前利润+上一年资产负债表所有者权益>max(之前年份所有者权益）】= round{【税前利润+上一年所有者权益-max[最近一个所得税>0的年份的权益，第0年权益]】*所得税率/100}
            // 所得税率比例
            double incomeTaxRate = ctGzCs == null ? 0.0 : (double) ctGzCs.getIncomeTax() / 100.0;


            // max(之前年份所有者权益）
            int maxTotalEquityPreYear = ctBalanceService.maxTotalEquityPreYear(studentId, contestId, date / 10);

            if ((profitBeforeTax + lastYearTotalEquity) > maxTotalEquityPreYear) {
                // 第0年权益
                NGbCtBalance zeroYearBalance = ctBalanceService.get(new NGbCtBalance().setStudentId(studentId).setContestId(contestId).setBsYear(0));
                int zeroYearTotalEquity = zeroYearBalance == null ? 0 : zeroYearBalance.getBsTotalEquity();

                // 最近一个所得税>0的年份的权益
                Integer equity = ctBalanceService.getRecentYEarHaveEquity(studentId, contestId, date / 10);
                int recentYEarHaveEquity = (equity == null ? 0 : equity);

                int tax = (int) Math.round(incomeTaxRate * (double) (profitBeforeTax + lastYearTotalEquity - Math.max(recentYEarHaveEquity, zeroYearTotalEquity)));
                profitChart.setPcTax(tax);
            } else {
                profitChart.setPcTax(0);
            }
        }else{
            profitChart.setPcTax(0);
        }

        profitChart.setPcAnnualNetProfit(profitBeforeTax - profitChart.getPcTax());
        ctProfitChartService.save(profitChart);

        //剩余现金
        Integer cashRest = ctCashflowService.getCash(studentId, contestId);
        /*
        资产负债表
         */
        NGbCtBalance balance = new NGbCtBalance()
                .setStudentId(studentId)
                .setContestId(contestId)
                .setBsIsxt(1)
                .setBsYear(date / 10)
                .setIsSubmit("Y");
        // 现金 = Cashflow最新现金
        balance.setBsCash(cashRest);


        // 应收款 = Fee表合计
        int fee = ctFeeService.getFeeSum(new NGbCtFee().setStudentId(studentId).setContestId(contestId).setType(NGbCtFee.TO_RECEIVABLE));
        balance.setBsReceivable(fee);


        // 在制品 = Line表剩余生产时间不为空的sum(生产产品所对应成本）
        int productInProcess = gbCtLineService.getProductInProcess(new NGbCtLine().setStudentId(studentId).setContestId(contestId));
        balance.setBsProductInProcess(productInProcess);


        // 产成品 = Kc_product表sum(库存产品-所对应成本)
        int product = ctKcProductService.getProductSum(studentId, contestId);
        balance.setBsProduct(product);

        // 原材料=Kc_material表sum（库存数*所对应成本）
        int material = ctKcMaterialService.getMaterialSum(studentId, contestId);
        balance.setBsMaterial(material);

        // 流动资产合计 = 现金+应收款+在制品+产成品+原材料
        balance.setBsTotalCurrentAsset(balance.getBsCash() + balance.getBsReceivable() + balance.getBsProductInProcess() + balance.getBsProduct() + balance.getBsMaterial());


        // 生产线=Line表PL_Finish_Date不为空的sum（原值-折旧）  已建成的生产线总价
        List<NGbCtLine> lineList = gbCtLineService.list(studentId, contestId);
        balance.setBsEquipment(lineList.stream().filter(p ->  !"在建".equals(p.getStatus())).mapToInt(p -> p.getPlInvest() - p.getPlDepTotal()).sum());

        // 在建
        balance.setBsProjectOnConstruction(lineList.stream().filter(p -> "在建".equals(p.getStatus())).mapToInt(NGbCtLine::getPlInvest).sum());

        // 固定资产合计
        balance.setBsTotalFixedAsset(balance.getBsEquipment() + balance.getBsProjectOnConstruction());

        // 资产合计
        balance.setBsTotalAsset(balance.getBsTotalCurrentAsset() + balance.getBsTotalFixedAsset());


        // 长期贷款:Bank_loan表类型为1的金额合计
        // 短期贷款:Bank_loan表类型为2的金额合计
        List<NGbCtBankLoan> bankLoanList = ctBankLoanService.list(studentId, contestId);
        balance.setBsLongLoan(bankLoanList.stream().filter(p -> p.getBlType() == 1).mapToInt(NGbCtBankLoan::getBlFee).sum());

        balance.setBsShortLoan(bankLoanList.stream().filter(p -> p.getBlType() == 2).mapToInt(NGbCtBankLoan::getBlFee).sum());




        //原材料的应付款也计入短期负债
        QueryWrapper<NGbCtFee> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("student_id", studentId);
        queryWrapper.eq("contest_id", contestId);
        queryWrapper.eq("type", NGbCtFee.TO_PAYMENT);
        List<NGbCtFee> list = ctFeeMapper.selectList(queryWrapper);
        Integer marketShouldPay = list.stream().mapToInt(NGbCtFee::getRFee).sum();



        //短期负债 =  短期贷款 + 原材料应付款 + 下季度的工人工资
        balance.setBsShortLoan(balance.getBsShortLoan()+marketShouldPay);


        // 应交税费 = 利润表中的PC_TAX
        NGbCtProfitChart ctProfitChart = ctProfitChartService.getSys(new NGbCtProfitChart().setStudentId(studentId).setContestId(contestId).setPcDate(date / 10));
        balance.setBsTax(ctProfitChart.getPcTax());


        //工作中的工人下个季度的工资计入负债
        List<NGbCtLine> productingLineList = ctLineMapper.selectList(new LambdaQueryWrapper<NGbCtLine>()
                .eq(NGbCtLine::getContestId,contestId)
                .eq(NGbCtLine::getStudentId,studentId)
                .eq(NGbCtLine::getStatus,NGbCtLine.ON_PRODUCE)
        );
        List<Integer> productingLineIds = productingLineList.stream().map(item->item.getLineId()).collect(Collectors.toList());
        Integer nextTotalSal = 0;
        if(productingLineIds.size()>0){
            List<NGbCtWorker> nGbCtWorkerList = nGbCtWorkerMapper.selectList(new LambdaQueryWrapper<NGbCtWorker>()
                    .eq(NGbCtWorker::getContestId,contestId)
                    .eq(NGbCtWorker::getStudentId,studentId)
                    .in(NGbCtWorker::getLineId,productingLineIds) //生产中
                    .eq(NGbCtWorker::getIsWork,3)   //在岗工人
            );
            nextTotalSal = nGbCtWorkerList.stream().mapToInt(item->item.getNextInitSal()*3).sum();
        }

        //其他应付款 -工人工资
        balance.setBsOtherPay(nextTotalSal);

        // 负债合计 = 长期贷款+短期贷款+应交税费+其他应付款
        balance.setBsTotalLiability(balance.getBsLongLoan() + balance.getBsShortLoan() + balance.getBsTax()+nextTotalSal);

        // 股东资本 = 第0年股东资本
        NGbCtBalance ctBalance = ctBalanceService.get(new NGbCtBalance().setStudentId(studentId).setContestId(contestId).setBsYear(0));
        balance.setBsEquity(ctBalance == null ? 0 : ctBalance.getBsEquity());

        // 利润留存 = 上一年所有者权益-上一年的股东资本
        balance.setBsRetainedEarning(lastYearTotalEquity - lastYearBsEquity);

        // 年度净利 = 利润表当年净利润
        balance.setBsAnnualNetProfit(ctProfitChart == null ? 0 : ctProfitChart.getPcAnnualNetProfit());

        // 所有者权益 = 股东资本+利润留存+年度净利
        balance.setBsTotalEquity(balance.getBsEquity() + balance.getBsRetainedEarning() + balance.getBsAnnualNetProfit());

        // 负债所有者权益合计 = 所有者权益+负债合计
        balance.setBsTotal(balance.getBsTotalEquity() + balance.getBsTotalLiability());

        ctBalanceService.save(balance);

        //财务指标
        NGbCtFinancialTarget gbCtFinancialTarget = new NGbCtFinancialTarget();
        gbCtFinancialTarget.setStudentId(studentId)
                .setContestId(contestId)
                .setFtYear(date / 10)
                .setFtIsxt(1).setIsSubmit("Y");


        //本年短期贷款+其他应付款+应交税金
        BigDecimal sum1 = new BigDecimal(balance.getBsShortLoan()+balance.getBsOtherPay()+balance.getBsTax());

        if(sum1.compareTo(BigDecimal.ZERO) != 0){
            //流动比率 :本年流动资产合计/（本年短期贷款+其他应付款+应交税金）
            BigDecimal ftCurrentRate =  new BigDecimal(balance.getBsTotalCurrentAsset())
                    .divide(sum1,2, BigDecimal.ROUND_HALF_UP);
            gbCtFinancialTarget.setFtCurrentRate(ftCurrentRate);
            //速动比率: (本年现金+本年应收款)/（本年短期贷款+其他应付款+应交税金）
            BigDecimal ftQuickRate =  new BigDecimal(balance.getBsCash()+balance.getBsReceivable())
                    .divide(new BigDecimal(balance.getBsShortLoan()+balance.getBsOtherPay()+balance.getBsTax()),2, BigDecimal.ROUND_HALF_UP);
            gbCtFinancialTarget.setFtQuickRate(ftQuickRate);
        }else{
            //流动比率 :本年流动资产合计/（本年短期贷款+其他应付款+应交税金）
            gbCtFinancialTarget.setFtCurrentRate(BigDecimal.ZERO);
            //速动比率: (本年现金+本年应收款)/（本年短期贷款+其他应付款+应交税金）
            gbCtFinancialTarget.setFtQuickRate(BigDecimal.ZERO);
        }


        //资产负债率：本年负债合计/本年资产合计
        if(BigDecimal.ZERO.compareTo(new BigDecimal(balance.getBsTotalAsset())) != 0){
            BigDecimal ftDebtRate = new BigDecimal(balance.getBsTotalLiability())
                    .divide(new BigDecimal(balance.getBsTotalAsset()),2, BigDecimal.ROUND_HALF_UP);
            gbCtFinancialTarget.setFtDebtRate(ftDebtRate);
        }else{
            gbCtFinancialTarget.setFtDebtRate(BigDecimal.ZERO);
        }



        //产权比率： 本年负债合计/本年所有者权益
        BigDecimal equity = new BigDecimal(balance.getBsTotalEquity());
        if(equity.compareTo(BigDecimal.ZERO)!=0){
            BigDecimal ftEquityRate = new BigDecimal(balance.getBsTotalLiability())
                    .divide(equity,2, BigDecimal.ROUND_HALF_UP);
            gbCtFinancialTarget.setFtEquityRate(ftEquityRate);
        }else{
            gbCtFinancialTarget.setFtEquityRate(BigDecimal.ZERO);
        }



        //营业净利润率: 本年折旧前利润/本年销售收入
        if(new BigDecimal(ctProfitChart.getPcSales()).compareTo(BigDecimal.ZERO)!=0){
            BigDecimal ftNetProfitRate = new BigDecimal(ctProfitChart.getPcProfitBeforeDep())
                    .divide(new BigDecimal(ctProfitChart.getPcSales()),2, BigDecimal.ROUND_HALF_UP);
            gbCtFinancialTarget.setFtNetProfitRate(ftNetProfitRate);
        }else{
            gbCtFinancialTarget.setFtNetProfitRate(BigDecimal.ZERO);
        }

        //成本费用率: （本年营业外收支+本年折旧前利润）/(本年直接成本+管理费用+财务费用)
         BigDecimal costExpense = new BigDecimal(ctProfitChart.getPcDirectCost()+ctProfitChart.getPcTotal()+ctProfitChart.getPcFinanceFee());
        if(costExpense.compareTo(BigDecimal.ZERO)!=0){
            BigDecimal ftCostExpenseRate = new BigDecimal(ctProfitChart.getPcNonOperating()+ctProfitChart.getPcProfitBeforeDep())
                    .divide(costExpense,2, BigDecimal.ROUND_HALF_UP);
            gbCtFinancialTarget.setFtCostExpenseRate(ftCostExpenseRate);
        }else{
            gbCtFinancialTarget.setFtCostExpenseRate(BigDecimal.ZERO);
        }




        //资产报酬率: 2*本年支付利息前利润 /（本年资产总计+去年资产总计）
        //本年支付利息前利润
        Integer lxqlr = ctProfitChart.getPcProfitBeforeInterests();
        BigDecimal zczj=new BigDecimal(balance.getBsTotalAsset()+(lastYearBalance!=null?lastYearBalance.getBsTotalAsset():0));
        if(BigDecimal.ZERO.compareTo(zczj)!=0){
            BigDecimal ftReturnAssetsRate = new BigDecimal(2)
                    .multiply(new BigDecimal(lxqlr))
                    .divide( zczj,2, BigDecimal.ROUND_HALF_UP);
            gbCtFinancialTarget.setFtReturnAssetsRate(ftReturnAssetsRate);
        }else{
            gbCtFinancialTarget.setFtReturnAssetsRate(BigDecimal.ZERO);
        }



        //净资产收益率: 2*净利润/（去年所有者权益+今年所有者权益）
        BigDecimal returnEquity = new BigDecimal(balance.getBsTotalEquity()+(lastYearBalance!=null?lastYearBalance.getBsTotalEquity():0));
        if(returnEquity.compareTo(BigDecimal.ZERO)!=0){
            BigDecimal ftReturnEquityRate =  new BigDecimal(2)
                    .multiply(new BigDecimal(balance.getBsAnnualNetProfit()))
                    .divide(returnEquity,2, BigDecimal.ROUND_HALF_UP);
            gbCtFinancialTarget.setFtReturnEquityRate(ftReturnEquityRate);
        }else{
            gbCtFinancialTarget.setFtReturnEquityRate(BigDecimal.ZERO);
        }


        //营业收入增长率:  今年销售收入/去年销售收入 -1
        //去年利润表
        NGbCtProfitChart lastYearProfitChar = ctProfitChartService.getSys(new NGbCtProfitChart().setStudentId(studentId).setContestId(contestId).setPcDate((date/10) - 1));
        if(lastYearProfitChar!=null && new BigDecimal(lastYearProfitChar.getPcSales()).compareTo(BigDecimal.ZERO)!=0){
            BigDecimal ftRevenueGrowthRate = new BigDecimal(ctProfitChart.getPcSales())
                    .divide(new BigDecimal(lastYearProfitChar.getPcSales()),100, BigDecimal.ROUND_HALF_UP)
                    .subtract(new BigDecimal(1))
                    .setScale(2, BigDecimal.ROUND_HALF_UP)
                    ;
            gbCtFinancialTarget.setFtRevenueGrowthRate(ftRevenueGrowthRate);
        }else{
            gbCtFinancialTarget.setFtRevenueGrowthRate(BigDecimal.ZERO);
        }

        //资本保值增值率:  今年所有者权益/去年所有者权益
        BigDecimal lastBsTotalEquity = new BigDecimal(lastYearBalance!=null?lastYearBalance.getBsTotalEquity():0);
        if(lastBsTotalEquity.compareTo(BigDecimal.ZERO)!=0){
            BigDecimal ftAppreciationRate = new BigDecimal(balance.getBsTotalEquity())
                    .divide(lastBsTotalEquity,2, BigDecimal.ROUND_HALF_UP);
            gbCtFinancialTarget.setFtAppreciationRate(ftAppreciationRate);
        }else{
            gbCtFinancialTarget.setFtAppreciationRate(BigDecimal.ZERO);
        }


        //总资产增长率:   今年资产总计/去年资产总计 -1
        BigDecimal lastBsTotalAsset= new BigDecimal(lastYearBalance!=null?lastYearBalance.getBsTotalAsset():0);
        if(lastBsTotalAsset.compareTo(BigDecimal.ZERO)!=0){
            BigDecimal ftTotalGrowthRate = new BigDecimal(balance.getBsTotalAsset())
                    .divide(lastBsTotalAsset,100, BigDecimal.ROUND_HALF_UP)
                    .subtract(new BigDecimal(1))
                    .setScale(2, BigDecimal.ROUND_HALF_UP)
                    ;
            gbCtFinancialTarget.setFtTotalGrowthRate(ftTotalGrowthRate);
        }else{
            gbCtFinancialTarget.setFtTotalGrowthRate(BigDecimal.ZERO);
        }



        //库存周转率      2*今年直接成本/（去年原材料+去年在制品+去年库存+今年原材料+今年在制品+今年库存）
        BigDecimal inventorySum;
        if(lastYearBalance!=null){
            inventorySum = new BigDecimal(balance.getBsProductInProcess()+balance.getBsProduct()+balance.getBsMaterial()
                    +lastYearBalance.getBsProductInProcess()+lastYearBalance.getBsProduct()+lastYearBalance.getBsMaterial());
        }else{
            inventorySum = new BigDecimal(balance.getBsProductInProcess()+balance.getBsProduct()+balance.getBsMaterial());
        }
        if(inventorySum.compareTo(BigDecimal.ZERO)!=0){
            BigDecimal ftInventoryRate = new BigDecimal(2)
                    .multiply(new BigDecimal(ctProfitChart.getPcDirectCost()))
                    .divide(inventorySum,2, BigDecimal.ROUND_HALF_UP);
            gbCtFinancialTarget.setFtInventoryRate(ftInventoryRate);
        }else{
            gbCtFinancialTarget.setFtInventoryRate(BigDecimal.ZERO);

        }

        //库存周转天数    365/库存周转率 (先四舍五入两位小数)
        if(gbCtFinancialTarget.getFtInventoryRate().compareTo(BigDecimal.ZERO)!=0){
            BigDecimal ftInventoryDays = new BigDecimal(365)
                    .divide(gbCtFinancialTarget.getFtInventoryRate()
                            ,2, BigDecimal.ROUND_HALF_UP);
            gbCtFinancialTarget.setFtInventoryDays(ftInventoryDays);
        }else{
            gbCtFinancialTarget.setFtInventoryDays(BigDecimal.ZERO);

        }


        //应收账款周转率  2 * 今年销售收入/（去年应收账款+今年应收账款）
        BigDecimal receivable = new BigDecimal(lastYearBalance!=null?lastYearBalance.getBsReceivable():0)
                .add(new BigDecimal(balance.getBsReceivable()));
        if(receivable.compareTo(BigDecimal.ZERO)!=0){
            BigDecimal ftReceivableRate = new BigDecimal(2)
                    .multiply(new BigDecimal(ctProfitChart.getPcSales()))
                    .divide(receivable,2, BigDecimal.ROUND_HALF_UP);
            gbCtFinancialTarget.setFtReceivableRate(ftReceivableRate);

        }else{
            gbCtFinancialTarget.setFtReceivableRate(BigDecimal.ZERO);

        }

        //应收账款周转天数 365/应收账款周转率
        if(gbCtFinancialTarget.getFtReceivableRate().compareTo(BigDecimal.ZERO)!=0){
            BigDecimal ftReceivableDays = new BigDecimal(365)
                    .divide(gbCtFinancialTarget.getFtReceivableRate()
                            ,2, BigDecimal.ROUND_HALF_UP);
            gbCtFinancialTarget.setFtReceivableDays(ftReceivableDays);
        }else{
            gbCtFinancialTarget.setFtReceivableDays(BigDecimal.ZERO);
        }


        //现金周转期     存货周转天数+应收账款周转天数
        BigDecimal ftCashPeriod = gbCtFinancialTarget.getFtInventoryDays()
                .add(gbCtFinancialTarget.getFtReceivableDays())
                .setScale(2, BigDecimal.ROUND_HALF_UP);;
        gbCtFinancialTarget.setFtCashPeriod(ftCashPeriod);

        gbCtFinancialTargetService.save(gbCtFinancialTarget);


        //计算本季度的得分
        calScore(date,contestId,studentId,balance);

        return balance.getBsTotalEquity();
    }


    /**
     * 计算本季度的学生得分
     */
    public void calScore(Integer date, Integer contestId, Integer studentId,NGbCtBalance balance){
        //计算该学生本季度的成绩
        //1先删除本季度之前的成绩
        nGbCtScoreMapper.delete(new LambdaQueryWrapper<NGbCtScore>()
                .eq(NGbCtScore::getContestId,contestId)
                .eq(NGbCtScore::getStudentId,studentId)
                .eq(NGbCtScore::getYear,date/10)
                .eq(NGbCtScore::getQuarterly,date%10)

        );
        NGbCtScore nGbCtScore = new NGbCtScore();
        nGbCtScore.setContestId(contestId);
        nGbCtScore.setStudentId(studentId);
        nGbCtScore.setYear(date/10);
        nGbCtScore.setQuarterly(date%10);

        //权益值 -所有人权益
        nGbCtScore.setEquity(balance.getBsTotalEquity());


        //商誉值 获取当前的商誉值
        NGbCtCorporate nGbCtCorporate = ctCorporateMapper.selectOne(new LambdaQueryWrapper<NGbCtCorporate>()
                .eq(NGbCtCorporate::getContestId,contestId)
                .eq(NGbCtCorporate::getStudentId,studentId)
                .le(NGbCtCorporate::getActionTime, date)
                .orderByDesc(NGbCtCorporate::getId)  //根据id 倒序排序取最后一条记录
                .last(" limit 1")
        );
        Integer sy = nGbCtCorporate.getScore();
        nGbCtScore.setSy(sy);

        //数字化得分
        Integer szh = 0;
        nGbCtScore.setSzh(szh);


        //扣分
        //系统扣分
        Integer kf = 0;
        //获取上一个季度的扣分情况
        Integer lastSeason = DateUtils.addYearAndSeasonTime(date,-1);
        NGbCtScore lastScore = nGbCtScoreMapper.selectOne(new LambdaQueryWrapper<NGbCtScore>()
                .eq(NGbCtScore::getContestId,contestId)
                .eq(NGbCtScore::getStudentId,studentId)
                .eq(NGbCtScore::getYear,lastSeason/10)
                .eq(NGbCtScore::getQuarterly,lastSeason%10)
        );
        if(lastScore!=null){
            kf += lastScore.getKf();
        }

        //然后扣分等于你每次填预算控制错误的。一个空10000。错误的标准，在实际值上下浮动百分之二十以外
        //查询预算
        //获取本季度的预算
        NGbCtBudget nGbCtBudget = nGbCtBudgetService.getOne(new LambdaQueryWrapper<NGbCtBudget>()
                .eq(NGbCtBudget::getContestId, contestId)
                .eq(NGbCtBudget::getStudentId, studentId)
                .eq(NGbCtBudget::getYear, date / 10)
                .eq(NGbCtBudget::getQuarterly, date % 10)
        );
        //如果本季还没有指定预算则不扣分
        if (nGbCtBudget != null) {
            //销售预算
            Integer initSaleBudget = nGbCtBudget.getInitSaleBudget().intValue();
            //生产预算
            Integer initProductBudget = nGbCtBudget.getInitProductBudget().intValue();
            //财务预算
            Integer initHrBudget = nGbCtBudget.getInitHrBudget().intValue();

            //销售预算使用
            Integer saleBudgetUsed = nGbCtBudgetService.getSeasonUsed(contestId, studentId, date, "1");
            //生产预算使用
            Integer productBudgetUsed = nGbCtBudgetService.getSeasonUsed(contestId, studentId, date, "2");
            //人力预算使用
            Integer hrBudgetUsed = nGbCtBudgetService.getSeasonUsed(contestId, studentId, date, "3");

            //销售预算扣分
            if (initSaleBudget == 0) {
                // 实际使用的销售预算>0,则视为超出预算需要扣分,否则无需扣分
                if (saleBudgetUsed != 0) {
                    kf += 10000;
                }
            } else {
                //如果不在上下浮动百分之二十的范围内会扣分
                double up = initSaleBudget * 1.2;
                double down = initSaleBudget * 0.8;
                if (saleBudgetUsed > up || saleBudgetUsed < down) {
                    kf += 10000;
                }
            }


            //生产预算扣分
            if (initProductBudget == 0) {
                // 实际使用的生产预算>0,则视为超出预算需要扣分,否则无需扣分
                if (productBudgetUsed != 0) {
                    kf += 10000;
                }
            } else {
                //如果不在上下浮动百分之二十的范围内会扣分
                double up = initProductBudget * 1.2;
                double down = initProductBudget * 0.8;
                if (productBudgetUsed > up || productBudgetUsed < down) {
                    kf += 10000;
                }
            }
            //人力预算扣分
            if (initHrBudget == 0) {
                // 实际使用的人力预算>0,则视为超出预算需要扣分,否则无需扣分
                if (hrBudgetUsed != 0) {
                    kf += 10000;
                }
            } else {
                //如果不在上下浮动百分之二十的范围内会扣分
                double up = initHrBudget * 1.2;
                double down = initHrBudget * 0.8;
                if (hrBudgetUsed > up || hrBudgetUsed < down) {
                    kf += 10000;
                }
            }
        }

        nGbCtScore.setKf(kf);

        //碳中和
        NGbCtCarbon nGbCtCarbon = nGbCtCarbonMapper.selectOne(new LambdaQueryWrapper<NGbCtCarbon>()
                .eq(NGbCtCarbon::getContestId,contestId)
                .eq(NGbCtCarbon::getStudentId,studentId)
                .eq(NGbCtCarbon::getYear,date/10)
        );
        BigDecimal neutralizaRate = new BigDecimal(0);
        Integer emissionNum = nGbCtCarbon.getEmissionNum();
        //计算本年的碳中和率  已中和/已排
        if(emissionNum==0){
            neutralizaRate = new BigDecimal(0);
        }else{
            Integer neutralizaNum = nGbCtCarbon.getNeutralizaNum();
            neutralizaRate = new BigDecimal(neutralizaNum).multiply(new BigDecimal(100)).divide(new BigDecimal(emissionNum),2,RoundingMode.HALF_UP);
        }
        nGbCtScore.setNeutralizaRate(neutralizaRate);

        //总分
        // 企业经营发展指数=企业商誉值×（企业权益-系统扣分）*（1+碳中和率）
        Long totalScore = new BigDecimal(sy)
                .multiply(new BigDecimal(balance.getBsTotalEquity()).subtract(new BigDecimal(kf)))
                .multiply(new BigDecimal(1).add(neutralizaRate.divide(new BigDecimal(100))))
                .setScale(0, RoundingMode.HALF_UP).longValue();

        nGbCtScore.setScore(totalScore);

        nGbCtScoreMapper.insert(nGbCtScore);
    }

    /**
     * 显示账单（应付、应收）
     * @param userId
     * @param contestId
     * @param type
     * @return
     */
    public R showFee(Integer userId, Integer contestId, Integer type, Integer currentTime) {
        QueryWrapper<NGbCtFee> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("student_id", userId);
        queryWrapper.eq("contest_id", contestId);
        queryWrapper.eq("type", type);
        queryWrapper.orderByAsc("r_remain_date");
        List<NGbCtFee> list = ctFeeMapper.selectList(queryWrapper);


        //计算贴现利率
        list.forEach(item->{
            //贴现率
            //计算剩余时间
            Integer remainDate = DateUtils.calSeasonInterval(currentTime,item.getPaymentDate());
            if(remainDate>0){
                List<NGbCtGzDiscount> gbCtGzDiscounts = gbCtGzDiscountMapper.getGzDiscountByStudentIdAndContestId(contestId,remainDate);
                if( gbCtGzDiscounts!=null && gbCtGzDiscounts.size()>0){
                    NGbCtGzDiscount gbCtGzDiscount = gbCtGzDiscounts.get(0);
                    item.setRate(gbCtGzDiscount.getDiscountInterest());
                }else{
                    item.setRate(new BigDecimal(0));
                }
            }else{
                item.setRate(new BigDecimal(0));
            }
        });

        Map<String, Object> resultMap = new HashMap<>(16);
        resultMap.put("list", list);
        return R.success(resultMap);
    }

    /**
     * 贴现
     * @param param
     * @param userId
     * @return
     */
    public R discount(NGbDiscountParam param, Integer userId) {

        //阻止暂停状态下 和 破产状态下的用户进行操作
        preventAction(param.getContestId(),userId);

        NGbCtFee f = ctFeeService.getById(param.getId());
        if(param.getFee() > f.getRFee()){
            return ErrorEnum.DISCOUNT_ERROR.getR();
        }
        int cash = ctCashflowService.getCash(userId, param.getContestId());
        //计算剩余时间
        Integer remainDate = DateUtils.calSeasonInterval(param.getDate(),f.getPaymentDate());
        List<NGbCtGzDiscount> gbCtGzDiscounts = gbCtGzDiscountMapper.getGzDiscountByStudentIdAndContestId(param.getContestId(),remainDate);
        if( gbCtGzDiscounts!=null && gbCtGzDiscounts.size()>0){
            NGbCtGzDiscount gbCtGzDiscount = gbCtGzDiscounts.get(0);
            //贴息
            int  out = new BigDecimal(param.getFee()).multiply(gbCtGzDiscount.getDiscountInterest()).divide(new BigDecimal(100))
                    .setScale(0, RoundingMode.CEILING)
                    .intValue();
            int remain = cash + param.getFee() - out;
            //现金流量表
            NGbCtCashflow ctCashflow = new NGbCtCashflow();
            ctCashflow.setCDate(param.getDate())
                    .setCSurplus(remain)
                    .setCIn(param.getFee())
                    .setCOut(out)
                    .setCAction(CashFlowActionEnum.DISCOUNT.getAction())
                    .setContestId(param.getContestId())
                    .setStudentId(userId)
                    .setCComment("应收日期:"+(f.getPaymentDate()/10)+"年"+(f.getPaymentDate()%10)+"季"+",贴现"+param.getFee()+"元");
            ctCashflowService.save(ctCashflow);
            //更新应收款数据
            int remianAmount = f.getRFee() - param.getFee();
            if(remianAmount>0){
                f.setRFee(f.getRFee() - param.getFee());
                ctFeeService.updateById(f);
            }else{
                ctFeeService.removeById(f);
            }
        }

        //计算报表,判断是否破产
        isBankruptcy(userId,param.getContestId(),param.getDate());

        return R.success();
    }

    /**
     * 付款
     * @return
     */
    public R payment(Integer userId,Integer contestId,Integer feeId,Integer currentTime){
        //阻止暂停状态下 和 破产状态下的用户进行操作
        preventAction(contestId,userId);

        NGbCtFee f = ctFeeService.getById(feeId);
        int cash = ctCashflowService.getCash(userId, contestId);
        if(cash<f.getRFee()){
            return ErrorEnum.CASH_NOT_ENOUGH.getR();
        }
        paymentAndCashFlow(f,userId,contestId,currentTime,cash);

        //计算报表,判断是否破产
        isBankruptcy(userId,contestId,currentTime);

        return R.success();
    }



    public R batchPayment(Integer userId, Integer contestId, Integer currentTime) {
        //阻止暂停状态下 和 破产状态下的用户进行操作
        preventAction(contestId,userId);

        List<NGbCtFee> list = ctFeeMapper.selectList(new LambdaQueryWrapper<NGbCtFee>()
                .eq(NGbCtFee::getContestId,contestId)
                .eq(NGbCtFee::getStudentId,userId)
                .eq(NGbCtFee::getType,NGbCtFee.TO_PAYMENT)
                .eq(NGbCtFee::getPaymentDate,currentTime)
                .orderByDesc(NGbCtFee::getRRemainDate) //根据账期倒序排序
        );
        //判断钱是否还够
        int cash = ctCashflowService.getCash(userId, contestId);
        Integer allPayAmount = list.stream().mapToInt(NGbCtFee::getRFee).sum();
        if(cash<allPayAmount){
            return ErrorEnum.CASH_NOT_ENOUGH.getR();
        }
        //循环支付
        for (NGbCtFee nGbCtFee : list) {
          cash = paymentAndCashFlow(nGbCtFee,userId,contestId,currentTime,cash);
        }
        //计算报表,判断是否破产
        isBankruptcy(userId,contestId,currentTime);

        return R.success();
    }


    //应付款并产生现金流量表
    public Integer paymentAndCashFlow(NGbCtFee f ,Integer userId,Integer contestId ,Integer currentTime,Integer cash){
        cash = cash - f.getRFee();
        //现金流量表
        NGbCtCashflow ctCashflow = new NGbCtCashflow();
        ctCashflow.setCDate(currentTime)
                .setCSurplus(cash)
                .setCIn(0)
                .setCOut(f.getRFee())
                .setCAction(CashFlowActionEnum.PAY_MATERIAL.getAction())
                .setContestId(contestId)
                .setStudentId(userId)
                .setCComment(f.getRemarks()+f.getRFee()+"元");
        ctCashflowService.save(ctCashflow);
        //删除应付款数据
        boolean deleteSuccess = ctFeeService.removeById(f.getFeeId());
        if(!deleteSuccess){
            throw new CustomException(CodeEnum.OTHER_ERROR.getCode(), "已付款,请勿重复支付");
        }
        return cash;
    }



    /**
     * 紧急采购原材料
     * @param userId
     * @param param
     * @return
     */
    public R buyMaterial(Integer userId, NGbUrgentBuyParam param) {
        //阻止暂停状态下 和 破产状态下的用户进行操作
        preventAction(param.getContestId(),userId);

        //判断现金是否足够
        int cash = ctCashflowService.getCash(userId, param.getContestId());
        // 紧急采购成本
        int cost = (param.getUrgentPrice() == null ? 0:param.getUrgentPrice()) * (param.getNum()==null? 0:param.getNum());
        if (cost == 0) {
            return R.success();
        }
        int remain = cash - cost;
        if (remain < 0) {
            return ErrorEnum.CASH_NOT_ENOUGH.getR();
        }
        NGbCtGzMaterial gbCtGzMaterial = gbCtGzMaterialMapper.selectById(param.getId());
        if(null == gbCtGzMaterial){
            return ErrorEnum.MATERIAL_NOT_EXIST.getR();
        }

        //记录现金流量表
        NGbCtCashflow ctCashflow = new NGbCtCashflow();
        ctCashflow.setCDate(param.getDate())
                .setCSurplus(remain)
                .setCIn(0)
                .setCOut(cost)
                .setCAction(CashFlowActionEnum.URGENT_MATERIAL_COST.getAction())
                .setContestId(param.getContestId())
                .setStudentId(userId)
                .setCComment("原材料紧急采购成本" + cost + "元");
        ctCashflowService.save(ctCashflow);


        //采购入库更新库存
        NGbCtKcMaterial gbCtKcMaterial = new NGbCtKcMaterial();
        gbCtKcMaterial.setStudentId(userId)
                .setContestId(param.getContestId())
                .setImNum(param.getNum())
                .setMaterialPrice(param.getUrgentPrice())
                .setInInventoryDate(param.getDate())
                .setImCmId(param.getId())
                .setMaterialName(gbCtGzMaterial.getCnName());
        ctKcMaterialService.save(gbCtKcMaterial);

        //计算报表,判断是否破产
        isBankruptcy(userId,param.getContestId(),param.getDate());


        return R.success();
    }

    /**
     * 紧急采购产品
     * @param userId
     * @param param
     * @return
     */
    @Autowired
    private NGbCtGzProductMapper gbCtGzProductMapper;

    public R buyProduct(Integer userId, NGbUrgentBuyParam param) {

        //阻止暂停状态下 和 破产状态下的用户进行操作
        preventAction(param.getContestId(),userId);

        if(StringUtils.isEmpty(param.getFeatureNum())){
            throw new CustomException(CodeEnum.OTHER_ERROR.getCode(), "产品特征不能为空");
        }
        //判断现金是否足够
        int cash = ctCashflowService.getCash(userId, param.getContestId());
        // 紧急采购成本
        int cost = (param.getUrgentPrice() == null ? 0:param.getUrgentPrice()) * (param.getNum()==null? 0:param.getNum());
        if (cost == 0) {
            return R.success();
        }
        int remain = cash - cost;
        if (remain < 0) {
            return ErrorEnum.CASH_NOT_ENOUGH.getR();
        }
        NGbCtGzProduct gbCtGzProduct = gbCtGzProductMapper.selectById(param.getId());
        if(null == gbCtGzProduct){
            return ErrorEnum.PRODUCT_NOT_EXIST.getR();
        }

        //记录现金流量表
        NGbCtCashflow ctCashflow = new NGbCtCashflow();
        ctCashflow.setCDate(param.getDate())
                .setCSurplus(remain)
                .setCIn(0)
                .setCOut(cost)
                .setCAction(CashFlowActionEnum.URGENT_PRODUCT_COST.getAction())
                .setContestId(param.getContestId())
                .setStudentId(userId)
                .setCComment("产成品紧急采购成本" + cost + "元");
        ctCashflowService.save(ctCashflow);


        //采购入库更新库存
        List<NGbCtKcProduct> list = new ArrayList<>();
        for (int i = 0; i < param.getNum(); i++) {
            NGbCtKcProduct gbCtKcProduct = new NGbCtKcProduct();
            gbCtKcProduct.setStudentId(userId)
                    .setContestId(param.getContestId())
                    .setInitNum(1)
                    .setIpNum(1)
                    .setRealCost(param.getUrgentPrice())
                    .setIsInventory(NGbCtKcProduct.IS_IN)
                    .setInventoryDate(param.getDate())
                    .setIpCpId(param.getId())
                    .setFeatureNum(param.getFeatureNum())
                    .setFeatureName(param.getFeatureName());
            list.add(gbCtKcProduct);
        }
        ctKcProductService.saveOrUpdateBatch(list);

        //计算报表,判断是否破产
        isBankruptcy(userId,param.getContestId(),param.getDate());

        return R.success();
    }

    /**
     * 判断是否破产
     * @param studentId
     * @param contestId
     * @param currentTime
     */
    public void isBankruptcy(Integer studentId,Integer contestId,Integer currentTime){
        isBankruptcy(studentId,contestId,currentTime,false);
    }




    /**
     * 判断是否破产
     * @param studentId
     * @param contestId
     * @param currentTime
     */
    public void isBankruptcy(Integer studentId,Integer contestId,Integer currentTime,boolean calTax){
        //获取当前的现金流量表余额
        int cash = ctCashflowService.getCash(studentId,contestId);
        //实时计算四张报表
        //计算四张报表
        calBsTotalEquity(studentId,contestId,currentTime,false);

        //获取之前的破产状态
        NGbContestStudent gbContestStudent = nGbContestStudentMapper.selectOne(new LambdaQueryWrapper<NGbContestStudent>()
                .eq(NGbContestStudent::getContestId,contestId)
                .eq(NGbContestStudent::getStudentId,studentId)
        );
        //判断当前是否处于破产状态
        Integer isPc=gbContestStudent.getIsBankruptcy();

        // 所有者权益为负后，自动结束比赛
        if (cash < 0) {
            //如果之前没有破产
            if(isPc==0){
                nGbContestStudentMapper.updateById(new NGbContestStudent()
                        .setId(gbContestStudent.getId())
                        .setIsBankruptcy(1) //是否破产
                        .setBankruptcyTime(currentTime) //破产时间
                );
            }
        }
        //没有破产
        else{
            //如果之前已经破产，则将其激活
            if(isPc==1){
                nGbContestStudentMapper.update(new NGbContestStudent(),new UpdateWrapper<NGbContestStudent>()
                        .lambda()
                        .eq(NGbContestStudent::getId,gbContestStudent.getId())
                        .set(NGbContestStudent::getIsBankruptcy,0)  //改为非破产状态
                        .set(NGbContestStudent::getBankruptcyTime,null)
                );
            }
        }


        //发送通知刷新数据
        Map<String, Object> messageMap = new HashMap<>();
        messageMap.put("type","refresh"); //数据刷新
        String message = JSONObject.toJSONString(messageMap);
        nGbWebSocket.sendMessageToUser(contestId,studentId,message); //给所有人广播消息
    }






//    public R listSellKc(Integer contestId, Integer userId) {
//        List<NGbProductVo> kcProductList = ctKcProductService.listSellKc(contestId, userId);
//        Map<String, Object> resultMap = new HashMap<>();
//        resultMap.put("kcProductList", kcProductList);
//        return R.success(resultMap);
//    }

    @Autowired
    private NGbCtKcMaterialMapper gbCtKcMaterialMapper;


    /**
     * 暂停状态下/破产状态下禁止用户进行操作
     * @param contestId
     * @param studentId
     */
    public void preventAction(Integer contestId,Integer studentId){
        //判断当前用户是否已经破产,以及比赛是否暂停
        NGbContestStudent nGbContestStudent = gbContestStudentService.get(contestId,studentId);
        NGbContest nGbContest = nGbContestService.getById(contestId);
        if(1==nGbContest.getIsSuspend()){
            throw new CustomException(CodeEnum.OTHER_ERROR.getCode(), "当前比赛已经暂停，不允许继续操作");
        }
        if(1==nGbContestStudent.getIsBankruptcy()){
            throw new CustomException(CodeEnum.OTHER_ERROR.getCode(), "当前用户已破产，不允许继续操作");
        }
    }




    public R sellMaterial(Integer userId, NGbSellMaterialParam param) {
        //阻止暂停状态下 和 破产状态下的用户进行操作
        preventAction(param.getContestId(),userId);

        //获取
        NGbCtGzCs ctGzCs = ctGzCsService.getByContestId(param.getContestId());
        List<NGbCtKcMaterial> materials = gbCtKcMaterialMapper.listByPriceAndDateAndCmId(param.getContestId(), userId,param.getImCmId(), param.getMaterialPrice(), param.getInInventoryDate());
        int kcSum = materials.stream().mapToInt(NGbCtKcMaterial::getImNum).sum();
        if(param.getImNum() > kcSum){
            return ErrorEnum.MATERIAL_NOT_ENOUGH.getR();
        }

        int sellSum = param.getImNum();
        int sellTotal = 0;//出售总价
        int directCost = 0;// 本来的本钱
        for (NGbCtKcMaterial material : materials) {
            Integer imNum = material.getImNum();
            if(sellSum < imNum ){//更新数量，结束
                imNum = imNum - sellSum;
                material.setImNum(imNum);
                sellTotal += sellSum * material.getMaterialPrice()*ctGzCs.getInventoryDiscountMaterial()/100;
                directCost += sellSum * material.getMaterialPrice();
                gbCtKcMaterialMapper.updateById(material);
                break;
            }else {//删除
                sellSum = sellSum - material.getImNum();
                sellTotal += material.getImNum() * material.getMaterialPrice()*ctGzCs.getInventoryDiscountMaterial()/100;
                directCost += material.getImNum() * material.getMaterialPrice();
                gbCtKcMaterialMapper.deleteById(material);
                if(sellSum == 0){
                    break;
                }
            }
        }

        int loss = directCost - sellTotal;
        int cash = ctCashflowService.getCash(userId, param.getContestId());

        //现金流量表
        NGbCtCashflow ctCashflow = new NGbCtCashflow();
        ctCashflow.setCDate(param.getDate())
                .setCSurplus(cash + sellTotal)
                .setCIn(directCost)
                .setCOut(loss)
                .setCAction(CashFlowActionEnum.SELL_MATERIAL.getAction())
                .setContestId(param.getContestId())
                .setStudentId(userId)
                .setCComment("出售原材料");
        ctCashflowService.save(ctCashflow);

        //计算报表,判断是否破产
        isBankruptcy(userId,param.getContestId(),param.getDate());

        return R.success();
    }

    @Autowired
    private NGbCtKcProductMapper ctKcProductMapper;

    public R sellProduct(Integer userId, NGbSellProductParam param) {

        //阻止暂停状态下 和 破产状态下的用户进行操作
        preventAction(param.getContestId(),userId);

        //获取
        NGbCtGzCs ctGzCs = ctGzCsService.getByContestId(param.getContestId());

        List<NGbCtKcProduct> kcProductList = ctKcProductMapper.getKcProductByPriceAndInDate(param.getContestId(),userId,param.getIpCpId(),param.getInventoryDate(),param.getRealCost(),param.getFeatureNum());
        if(param.getNum() > kcProductList.size()){
            return ErrorEnum.PRODUCT_NOT_ENOUGH.getR();
        }

        //出售总价
        int sellTotal = param.getNum() * param.getRealCost() * ctGzCs.getInventoryDiscountProduct() / 100;

        // 本来的本钱
        int directCost = param.getNum() * param.getRealCost();

        int loss = directCost - sellTotal;
        int cash = ctCashflowService.getCash(userId, param.getContestId());

        //现金流量表
        NGbCtCashflow ctCashflow = new NGbCtCashflow();
        ctCashflow.setCDate(param.getDate())
                .setCSurplus(cash + sellTotal)
                .setCIn(directCost)
                .setCOut(loss)
                .setCAction(CashFlowActionEnum.SELL_PRODUCT.getAction())
                .setContestId(param.getContestId())
                .setStudentId(userId)
                .setCComment("出售产品");
        ctCashflowService.save(ctCashflow);

        //达到出售数量，则不再出售
        for (int i = 0; i < kcProductList.size(); i++) {
            if(i >= param.getNum()){
                break;
            }
            NGbCtKcProduct gbCtKcProduct = kcProductList.get(i);
            gbCtKcProduct.setIpNum(0); //修改剩余库存
            ctKcProductService.updateById(gbCtKcProduct);
        }


        //计算报表,判断是否破产
        isBankruptcy(userId,param.getContestId(),param.getDate());

        return R.success();
    }



    public R xzorder(Integer studentId, Integer contestId, Integer date) {
        List<NGbCtXzOrder> list = ctXzOrderMapper.getList(contestId, studentId);
        list.forEach(p -> {
            String status = "-";
            if (p.getCommitDate() != null && p.getCommitDate() > 0) {
                status = "已交货";
            } else if ((date / 10 == p.getDate() && date % 10 > p.getDeliveryDate()) || ((date / 10) > p.getDate())) {
                status = "已违约";
            } else {
                status = "未到期";
            }
            p.setStatus(status);
        });
        Map<String, Object> resultMap = new HashMap<>(16);
        resultMap.put("list", list);
        return R.success(resultMap);
    }

    public R marketList(Integer userId, Integer contestId) {
        List<NGbMarketVo> list = ctYfMarketService.listYfMarket(userId, contestId);
        Map<String, Object> resultMap = new HashMap<>(16);
        resultMap.put("list", list);
        return R.success(resultMap);
    }

    public R marketYf(Integer userId, NGbMarketYfParam param) {
        //阻止暂停状态下 和 破产状态下的用户进行操作
        preventAction(param.getContestId(),userId);

        List<NGbMarketVo> list = ctYfMarketService.listYfMarket(userId, param.getContestId());
        Map<Integer, Integer> map = new HashMap<>(16);
        for (NGbMarketVo m : list) {
            map.put(m.getMarketId(), m.getCmDevelopFee());
        }
        int cash = ctCashflowService.getCash(userId, param.getContestId());
        NGbCtYfMarket yf = ctYfMarketService.getById(param.getMarketId());
        NGbCtGzMarket gz = ctGzMarketMapper.selectById(yf.getDmCmId());

        if (NGbCtYfMarket.SC_YF_ING.equals(yf.getDmState()) || NGbCtYfMarket.SC_YF_FINISH.equals(yf.getDmState())) {
            return R.error(CodeEnum.OTHER_ERROR.getCode(), "无需重复研发");
        }
        int sum = gz.getCmDevelopFee();
        int remain = cash - sum;
        if (remain < 0) {
            return ErrorEnum.CASH_NOT_ENOUGH.getR();
        }
        NGbCtCashflow ctCashflow = new NGbCtCashflow();
        ctCashflow.setCDate(param.getDate())
                .setCSurplus(remain)
                .setCIn(0)
                .setCOut(sum)
                .setCAction(CashFlowActionEnum.DEVELOP_MARKET.getAction())
                .setContestId(param.getContestId())
                .setStudentId(userId)
                .setCComment("扣减市场"+gz.getCmName()+"开拓" + sum + "元");
        ctCashflowService.save(ctCashflow);

        //更新市场
        Integer cmDevelopDate = gz.getCmDevelopDate();
        Integer fininshDate = DateUtils.addYearAndSeasonTime(param.getDate(), cmDevelopDate);
        if(cmDevelopDate!= null && cmDevelopDate>0){
            yf.setDmState(NGbCtYfMarket.SC_YF_ING);
        }else {
            yf.setDmState(NGbCtYfMarket.SC_YF_FINISH);
        }
        yf.setDmStartDate(param.getDate());//申请时间
        yf.setDmFinishDate(fininshDate);//结束时间
        ctYfMarketService.updateById(yf);

        //计算报表,判断是否破产
        isBankruptcy(userId,param.getContestId(),param.getDate());

        return R.success();
    }

    /**
     * 获取社会责任分
     */
    public R  getCorporateScore(Integer userId, Integer contestId){
        int score = gbCtCorporateMapper.getScore(userId, contestId);
        return R.success().put("data",score);
    }


    /**
     * 获取扣分明细
     * @param userId
     * @param contestId
     * @return
     */
    public R getCorporateScoreDetail(Integer userId, Integer contestId) {
        List<NGbCtCorporate> nGbCtCorporates = gbCtCorporateMapper.selectList(new LambdaQueryWrapper<NGbCtCorporate>()
                .eq(NGbCtCorporate::getContestId,contestId)
                .eq(NGbCtCorporate::getStudentId,userId)
                .orderByDesc(NGbCtCorporate::getId)
        );
        return R.success().put("list",nGbCtCorporates);
    }


    /**
     * 注入资本
     * @return
     */
    public R addCapital(NGbCapitalParam param){
        JSONArray array = new JSONArray();
        for (NGbCapitalParam.MaterialIdAndMaterialNum obj : param.getMaterialIdAndMaterialNumList()) {
            JSONObject js = new JSONObject();
            js.put("materialId",obj.getMaterialId());
            js.put("materialNum",obj.getMaterialNum());
            js.put("materialName",obj.getCnName());
            array.add(js);
        }
        NGbContestStudent gbContestStudent = gbContestStudentService.get(param.getContestId(), param.getStudentId());
        int caDate = gbContestStudent.getDate() / 10;
        NGbCtCapital ctCapital = new NGbCtCapital();
        ctCapital.setContestId(param.getContestId())
                .setStudentId(param.getStudentId())
                .setAddCapital(param.getAddCapital())
                .setCaDate(caDate)
                .setMaterialInfo(array.toString());
        gbCtCapitalMapper.insert(ctCapital);

        int cash = ctCashflowService.getCash(param.getStudentId(), param.getContestId());
        //入库存
        Integer materialCash=0;
        for (NGbCapitalParam.MaterialIdAndMaterialNum obj : param.getMaterialIdAndMaterialNumList()) {
            NGbCtGzMaterial gbCtGzMaterial = gbCtGzMaterialMapper.selectById(obj.getMaterialId());
            NGbCtKcMaterial kc = new NGbCtKcMaterial();
            kc.setStudentId(param.getStudentId())//学生id
                    .setContestId(param.getContestId())//竞赛id
                    .setImCmId(obj.getMaterialId())//原料商店产品id
                    .setImNum(obj.getMaterialNum())//数量
                    .setMaterialName(gbCtGzMaterial.getCnName())//原料名称
                    .setInInventoryDate(caDate)//入库时间
                    .setMaterialPrice(gbCtGzMaterial.getCmBuyFee());//价格
            gbCtKcMaterialMapper.insert(kc);
            //注入原材料，同步增加股东资本
            materialCash+= obj.getMaterialNum()*gbCtGzMaterial.getCmBuyFee();
        }

        if(param.getAddCapital() != 0 ){
            // 改变现金流量表
            NGbCtCashflow ctCashflow = new NGbCtCashflow();
            ctCashflow.setCDate(caDate)
                    .setCSurplus(cash + param.getAddCapital())
                    .setCIn(param.getAddCapital())
                    .setCOut(0)
                    .setCAction(CashFlowActionEnum.ADD_CAPITAL.getAction())
                    .setContestId(param.getContestId())
                    .setStudentId(param.getStudentId())
                    .setCComment("注入资本" + param.getAddCapital() + "元");
            ctCashflowService.save(ctCashflow);

            //修改第0年股东资本
            // 股东资本 = 第0年股东资本
            NGbCtBalance ctBalance = ctBalanceService.get(new NGbCtBalance().setStudentId(param.getStudentId()).setContestId(param.getContestId()).setBsYear(0));
            //原股东资本 + 注资金额 + 注入原材料(转化成对应金额)
            Integer newInitCash = ctBalance.getBsEquity()+param.getAddCapital()+materialCash;
            ctBalance
                    .setBsCash(newInitCash)
                    .setBsTotalCurrentAsset(newInitCash)
                    .setBsTotalAsset(newInitCash)
                    .setBsEquity(newInitCash)
                    .setBsTotalEquity(newInitCash)
                    .setBsTotal(newInitCash);
            ctBalanceService.updateById(ctBalance);

            //重新计算报表并判断是否破产
            isBankruptcy(param.getStudentId(), param.getContestId(),caDate);
        }


        return R.success();

    }


//    public R getOtherInfo(HttpServletResponse response, Integer userId, Integer otherStudentId, Integer contestId, Integer date, Integer amount) {
//        //扣取现金
//        int cash = ctCashflowService.getCash(userId, contestId);
//        int remain = cash - amount;
//        if (remain < 0) {
//            return ErrorEnum.CASH_NOT_ENOUGH.getR();
//        }
//        NGbCtCashflow ctCashflow = new NGbCtCashflow();
//        ctCashflow.setCDate(date)
//                .setCSurplus(remain)
//                .setCIn(0)
//                .setCOut(amount)
//                .setCAction(CashFlowActionEnum.INFO_OTHER.getAction())
//                .setContestId(contestId)
//                .setStudentId(userId)
//                .setCComment("扣取数据咨询费" + amount + "元");
//        ctCashflowService.save(ctCashflow);
//        //下载
//        gbDownloadBiz.downloadResult(response,contestId,otherStudentId,"数据咨询");
//        return R.success();
//    }


    @Autowired
    private NGbCtBudgetService nGbCtBudgetService;

    @Autowired
    private NGbCtBudgetApplyService nGbCtBudgetApplyService;

    /**
     * 获取预算信息
     * @param userId
     * @param contestId
     * @return
     */
    public R getBudget(Integer userId, Integer contestId) {
        Integer date = gbContestStudentService.getStudentDate(contestId,userId);
        Integer currentTime = date/10; //保留前两位数
        Integer year = currentTime/10; //年
        Integer quarterly = currentTime%10; //季度
        List<NGbCtBudgetVo> nGbCtBudgetVoList= new ArrayList<>();


        //获取本季度的预算
        NGbCtBudget nGbCtBudget = nGbCtBudgetService.getOne(new LambdaQueryWrapper<NGbCtBudget>()
                .eq(NGbCtBudget::getContestId,contestId)
                .eq(NGbCtBudget::getStudentId,userId)
                .eq(NGbCtBudget::getYear,year)
                .eq(NGbCtBudget::getQuarterly,quarterly)
        );

        //获取上季度的预算
        //获取上一季度的时间
        Integer lastRoundTime = DateUtils.addYearAndSeasonTime(currentTime,-1);
        NGbCtBudget lastNGbCtBudget = nGbCtBudgetService.getOne(new LambdaQueryWrapper<NGbCtBudget>()
                .eq(NGbCtBudget::getContestId,contestId)
                .eq(NGbCtBudget::getStudentId,userId)
                .eq(NGbCtBudget::getYear,lastRoundTime/10)
                .eq(NGbCtBudget::getQuarterly,lastRoundTime%10)
        );


        //获取学生当前的季度
        // 获取市场营销部预算   上季度预算  上季度使用  使用率  本季度预算  本季度使用
        NGbCtBudgetVo  vo1= new NGbCtBudgetVo();
        vo1.setDepartment("市场营销部");
        vo1.setType("1");

        Integer lastInitSeasonBudget1 = null;
        Integer lastSeasonUsed1 = null;

        //判断是否是第一个季度,第一个季度的上季度预算和上季度使用都为空
        if(lastRoundTime<11){
            lastInitSeasonBudget1 = null;
            lastSeasonUsed1 = null;
        }else{
            //获取上季度季度预算
            lastInitSeasonBudget1 = lastNGbCtBudget==null ? null:lastNGbCtBudget.getInitSaleBudget().intValue();
            //获取上季度使用
            lastSeasonUsed1 =  nGbCtBudgetService.getSeasonUsed(contestId,userId,lastRoundTime,"1");
        }

        //计算使用率
        BigDecimal lastSeasonUsedRate1 = null;
        if(lastSeasonUsed1!=null && lastInitSeasonBudget1!=null){
            //如果预算等于0的情况下,得到计算的结果为0
            if(lastInitSeasonBudget1==0){
                lastSeasonUsedRate1 = new BigDecimal(0);
            }else{
                lastSeasonUsedRate1 = new BigDecimal(lastSeasonUsed1)
                        .divide(new BigDecimal(lastInitSeasonBudget1),2,BigDecimal.ROUND_HALF_UP);
            }
        }
        //本季度初始预算
        Integer initseasonBudget1 = nGbCtBudget==null ? null:nGbCtBudget.getInitSaleBudget().intValue();
        //本季度当前预算
        Integer seasonBudget1 = nGbCtBudget==null ? null:nGbCtBudget.getSaleBudget().intValue();

        //本季度使用
        Integer seasonUsed1 = nGbCtBudgetService.getSeasonUsed(contestId,userId,currentTime,"1");
        vo1.setLastSeasonBudget(lastInitSeasonBudget1);
        vo1.setLastSeasonUsed(lastSeasonUsed1);
        vo1.setLastSeasonUsedRate(lastSeasonUsedRate1);
        vo1.setInitBudget(initseasonBudget1);  //初始预算
        vo1.setBudget(seasonBudget1);   //最终的预算
        vo1.setUsed(seasonUsed1);



        // 获取生产设计部预算   上季度预算  上季度使用  使用率  本季度预算  本季度使用
        NGbCtBudgetVo  vo2= new NGbCtBudgetVo();
        vo2.setDepartment("生产设计部");
        vo2.setType("2");


        Integer lastInitSeasonBudget2 = null;
        Integer lastSeasonUsed2 = null;

        //判断是否是第一个季度,第一个季度的上季度预算和上季度使用都为空
        if(lastRoundTime<11){
            lastInitSeasonBudget2 = null;
            lastSeasonUsed2 = null;
        }else{
            //获取上季度季度预算
            lastInitSeasonBudget2 = lastNGbCtBudget==null ? null:lastNGbCtBudget.getInitProductBudget().intValue();
            //获取上季度使用
            lastSeasonUsed2 =  nGbCtBudgetService.getSeasonUsed(contestId,userId,lastRoundTime,"2");
        }
        //计算使用率
        BigDecimal lastSeasonUsedRate2 = null;
        if(lastSeasonUsed2!=null && lastInitSeasonBudget2!=null){
            //如果预算等于0的情况下,得到计算的结果为0
            if(lastInitSeasonBudget2==0){
                lastSeasonUsedRate2 = new BigDecimal(0);
            }else{
                lastSeasonUsedRate2 = new BigDecimal(lastSeasonUsed2)
                        .divide(new BigDecimal(lastInitSeasonBudget2),2,BigDecimal.ROUND_HALF_UP);
            }
        }

        //本季度初始预算
        Integer initseasonBudget2 = nGbCtBudget==null ? null:nGbCtBudget.getInitProductBudget().intValue();
        //本季度最终预算
        Integer seasonBudget2 = nGbCtBudget==null ? null:nGbCtBudget.getProductBudget().intValue();
        //本季度使用
        Integer seasonUsed2  = nGbCtBudgetService.getSeasonUsed(contestId,userId,currentTime,"2");
        vo2.setLastSeasonBudget(lastInitSeasonBudget2);
        vo2.setLastSeasonUsed(lastSeasonUsed2);
        vo2.setLastSeasonUsedRate(lastSeasonUsedRate2);
        vo2.setInitBudget(initseasonBudget2);
        vo2.setBudget(seasonBudget2);
        vo2.setUsed(seasonUsed2);



        // 获取人力资源部预算   上季度预算  上季度使用  使用率  本季度预算  本季度使用
        NGbCtBudgetVo  vo3= new NGbCtBudgetVo();
        vo3.setDepartment("人力资源部");
        vo3.setType("3");

        //获取上季度季度预算
        Integer lastInitSeasonBudget3 = null;
        //获取上季度使用
        Integer lastSeasonUsed3 =  null;

        if(lastRoundTime<11){
            lastInitSeasonBudget3 = null;
            lastSeasonUsed3 = null;
        }else{
            //获取上季度季度预算
            lastInitSeasonBudget3 = lastNGbCtBudget==null ? null:lastNGbCtBudget.getInitHrBudget().intValue();
            //本季度使用
            lastSeasonUsed3 = nGbCtBudgetService.getSeasonUsed(contestId,userId,lastRoundTime,"3");
        }


        //计算使用率
        BigDecimal lastSeasonUsedRate3 = null;
        if(lastSeasonUsed3!=null && lastInitSeasonBudget3!=null){
            //如果预算等于0的情况下,得到计算的结果为0
            if(lastInitSeasonBudget3==0){
                lastSeasonUsedRate3 = new BigDecimal(0);
            }else{
                lastSeasonUsedRate3 = new BigDecimal(lastSeasonUsed3)
                        .divide(new BigDecimal(lastInitSeasonBudget3),2,BigDecimal.ROUND_HALF_UP);
            }
        }


        //本季度初始预算
        Integer initseasonBudget3 = nGbCtBudget==null ? null:nGbCtBudget.getInitHrBudget().intValue();
        //本季度最终预算
        Integer seasonBudget3 = nGbCtBudget==null ? null:nGbCtBudget.getHrBudget().intValue();


        //本季度使用
        Integer seasonUsed3 = nGbCtBudgetService.getSeasonUsed(contestId,userId,currentTime,"3");
        vo3.setLastSeasonBudget(lastInitSeasonBudget3);
        vo3.setLastSeasonUsed(lastSeasonUsed3);
        vo3.setLastSeasonUsedRate(lastSeasonUsedRate3);
        vo3.setInitBudget(initseasonBudget3);
        vo3.setBudget(seasonBudget3);
        vo3.setUsed(seasonUsed3);


        nGbCtBudgetVoList.add(vo1);
        nGbCtBudgetVoList.add(vo2);
        nGbCtBudgetVoList.add(vo3);


        Map<String,Object> map = new HashMap<>();
        map.put("list",nGbCtBudgetVoList); //预算结果
        if(nGbCtBudget!=null){
            map.put("isSubmit",true);  //本季度已提交预算
        }else{
            map.put("isSubmit",false); //未提交预算
        }
        return R.success(map);
    }

    /**
     * 提交本季度的预算
     * @param userId
     * @param nGbCtBudget
     * @return
     */
    public R submitBudget(Integer userId, NGbCtBudget nGbCtBudget) {
        //阻止暂停状态下 和 破产状态下的用户进行操作
        preventAction(nGbCtBudget.getContestId(),userId);

        nGbCtBudget.setStudentId(userId);
        //先判断当前季度是否已经录入预算
        NGbCtBudget nGbCtBudget1 = nGbCtBudgetService.getOne(new LambdaQueryWrapper<NGbCtBudget>()
                .eq(NGbCtBudget::getContestId,nGbCtBudget.getContestId())
                .eq(NGbCtBudget::getStudentId,nGbCtBudget.getStudentId())
                .eq(NGbCtBudget::getYear,nGbCtBudget.getYear())
                .eq(NGbCtBudget::getQuarterly,nGbCtBudget.getQuarterly())
        );
        if(nGbCtBudget1!=null){
            return R.error(CodeEnum.PARAM_ERROR.getCode(), "本季度预算已提交不能重复提交");
        }else{
            nGbCtBudgetService.save(nGbCtBudget);
            return R.success();
        }
    }


    /**
     * 预算申请
     * @param userId
     * @param nGbCtBudgetApply
     * @return
     */
    public R applyBudget(Integer userId, NGbCtBudgetApply nGbCtBudgetApply) {
        //阻止暂停状态下 和 破产状态下的用户进行操作
        preventAction(nGbCtBudgetApply.getContestId(),userId);

        //判断当前季度还存在 本类型 未审批的预算审批,则不允许继续操作
        List<NGbCtBudgetApply> dspList = nGbCtBudgetApplyService.list(new LambdaQueryWrapper<NGbCtBudgetApply>()
                .eq(NGbCtBudgetApply::getContestId,nGbCtBudgetApply.getContestId())
                .eq(NGbCtBudgetApply::getStudentId,userId)
                .eq(NGbCtBudgetApply::getYear, nGbCtBudgetApply.getYear())
                .eq(NGbCtBudgetApply::getQuarterly, nGbCtBudgetApply.getQuarterly())
                .eq(NGbCtBudgetApply::getType, nGbCtBudgetApply.getType())
                .eq(NGbCtBudgetApply::getApproveResult,"1")
        );
        if(dspList!=null && dspList.size()>0){
            throw new CustomException(CodeEnum.OTHER_ERROR.getCode(), "当前类型的预算存在待审批的记录，请项目总监完成审批后，再重新发起");
        }

        //新增对应的预算申请记录
        nGbCtBudgetApply.setApproveResult("1"); //未审批
        nGbCtBudgetApply.setStudentId(userId);
        nGbCtBudgetApplyService.save(nGbCtBudgetApply);

        //发送流程给待审判的人
        Map<String, Object> messageMap = new HashMap<>();
        messageMap.put("type","yssp");
        messageMap.put("data",nGbCtBudgetApply);
        String message = JSONObject.toJSONString(messageMap);
        nGbWebSocket.sendMessageToUser(nGbCtBudgetApply.getContestId(),userId,message);


        return R.success();
    }


    /**
     * 预算审批
     * @param userId
     * @param nGbCtBudgetApply
     * @return
     */
    public R approveBudget(Integer userId, NGbCtBudgetApply nGbCtBudgetApply) {
        //阻止暂停状态下 和 破产状态下的用户进行操作
        preventAction(nGbCtBudgetApply.getContestId(),userId);

        String approveResult = nGbCtBudgetApply.getApproveResult();

        //保存预算审批记录
        //先判断当前是否已经完成审判,避免重复审批
        nGbCtBudgetApply = nGbCtBudgetApplyService.getById(nGbCtBudgetApply.getId());
        if(!"1".equals(nGbCtBudgetApply.getApproveResult())){
            throw new CustomException(CodeEnum.OTHER_ERROR.getCode(), "当前预算已审批完成，无需重复审批");
        }
        nGbCtBudgetApply.setApproveResult(approveResult);
        nGbCtBudgetApplyService.updateById(nGbCtBudgetApply);

        //判断是否审批通过
        //审批通过
        if("2".equals(approveResult)){
            //获取年
            Integer year = nGbCtBudgetApply.getYear();
            //获取季度
            Integer quarterly = nGbCtBudgetApply.getQuarterly();

            //查询当前的预算审批
            NGbCtBudget nGbCtBudget  = nGbCtBudgetService.getOne(new LambdaQueryWrapper<NGbCtBudget>()
                    .eq(NGbCtBudget::getYear,year)
                    .eq(NGbCtBudget::getQuarterly,quarterly)
                    .eq(NGbCtBudget::getContestId,nGbCtBudgetApply.getContestId())
                    .eq(NGbCtBudget::getStudentId,nGbCtBudgetApply.getStudentId())
            );
            if(nGbCtBudget==null){
                throw new CustomException(CodeEnum.OTHER_ERROR.getCode(), "当前季度还未进行预算审批请重试");
            }else{
                //销售(营销)
                if("1".equals(nGbCtBudgetApply.getType())){
                    BigDecimal budeget = nGbCtBudget.getSaleBudget().add(nGbCtBudgetApply.getAmount());
                    nGbCtBudget.setSaleBudget(budeget);
                }
                //生产
                else if("2".equals(nGbCtBudgetApply.getType())){
                    BigDecimal budeget = nGbCtBudget.getProductBudget().add(nGbCtBudgetApply.getAmount());
                    nGbCtBudget.setProductBudget(budeget);
                }
                //人力
                else if("3".equals(nGbCtBudgetApply.getType())){
                    BigDecimal budeget = nGbCtBudget.getHrBudget().add(nGbCtBudgetApply.getAmount());
                    nGbCtBudget.setHrBudget(budeget);
                }
                //更新数据
                nGbCtBudgetService.updateById(nGbCtBudget);
            }
            //TODO 审批通过通知
        }
        //审批已驳回
        else if("3".equals(approveResult)){
            //TODO 驳回通知
        }
        return R.success();
    }

    /**
     * 查询本季度的预算申请记录
     * @param userId
     * @param nGbCtBudgetApply
     * @return
     */
    public R getBudgetApplyList(Integer userId, NGbCtBudgetApply nGbCtBudgetApply) {
        //判断当前季度还存在 本类型 未审批的预算审批,则不允许继续操作
        List<NGbCtBudgetApply> list = nGbCtBudgetApplyService.list(new LambdaQueryWrapper<NGbCtBudgetApply>()
                .eq(NGbCtBudgetApply::getContestId,nGbCtBudgetApply.getContestId())
                .eq(NGbCtBudgetApply::getStudentId,userId)
                .eq(NGbCtBudgetApply::getYear, nGbCtBudgetApply.getYear())
                .eq(NGbCtBudgetApply::getQuarterly, nGbCtBudgetApply.getQuarterly())
                .last(" order by case when approve_result = 1 then 0 else 1 end asc,id")
        );
        return R.success().put("list",list);
    }



    @Autowired
    private  NGbCtDesignProgressService nGbCtDesignProgressService;

    /**
     * 查看特征研发进度列表
     * @param userId
     * @param contestId
     * @return
     */
    public R designProcessList(Integer userId, Integer contestId) {
        List<NGbCtDesignProgress> list  = nGbCtDesignProgressService.list(new LambdaQueryWrapper<NGbCtDesignProgress>()
                .eq(NGbCtDesignProgress::getContestId,contestId)
                .eq(NGbCtDesignProgress::getStudentId,userId)
        );
        return R.success().put("list",list);
    }


    /**
     * 特征研发
     * @param userId
     * @param nGbCtDesignProgress
     * @return
     */
    public R designProgressResearch(Integer userId, NGbCtDesignProgress nGbCtDesignProgress) {
        Integer contestId = nGbCtDesignProgress.getContestId();

        //阻止暂停状态下 和 破产状态下的用户进行操作
        preventAction(contestId,userId);

        NGbContest nGbContest = nGbContestService.getById(contestId);
        Integer fairOrderState = nGbContest.getFairOrderState();
        if(fairOrderState==1){
            throw new CustomException(CodeEnum.OTHER_ERROR.getCode(), "当前正在选单阶段，不允许研发特性");
        }
        Integer date = gbContestStudentService.getStudentDate(contestId,userId);
        Integer currentTime = date/10;

        Integer progressId = nGbCtDesignProgress.getId();
        //查询之前的进度
        NGbCtDesignProgress nGbCtDesignProgressHis = nGbCtDesignProgressService.getById(progressId);
        Integer unitCostUpgrade = nGbCtDesignProgressHis.getUnitCostUpgrade();

        //程度差
        Integer nowValue = nGbCtDesignProgress.getNowValue();
        Integer oldValue = nGbCtDesignProgressHis.getNowValue();

        //获取研发上限
        Integer upperLimit = nGbCtDesignProgressHis.getUpperLimit();

        if(nowValue>upperLimit ){
            throw new CustomException(CodeEnum.OTHER_ERROR.getCode(), "当前研发值超过上限值");
        }
        if(nowValue<=oldValue){
            throw new CustomException(CodeEnum.OTHER_ERROR.getCode(), "当前研发值小于原研发值");
        }
        //升级了多少单位
        int unit  = nowValue - oldValue;
        //计算金额
        Integer amount  = unitCostUpgrade * unit;
        //判断钱够不够
        int cash = ctCashflowService.getCash(userId, contestId);
        // 获取消耗现金的金额
        int remain = cash - amount;
        if (remain < 0) {
            throw new CustomException(ErrorEnum.CASH_NOT_ENOUGH.getCode(), ErrorEnum.CASH_NOT_ENOUGH.getMsg());
        }
        //记录现金流量表
        NGbCtCashflow ctCashflow = new NGbCtCashflow();
        ctCashflow.setCDate(currentTime)
                .setCSurplus(remain)
                .setCIn(0)
                .setCOut(amount)
                .setCAction(CashFlowActionEnum.TZYF.getAction())
                .setContestId(contestId)
                .setStudentId(userId)
                .setCComment("特性【"+nGbCtDesignProgressHis.getFeatureName()+"】研发花费"+ amount + "元");
        ctCashflowService.save(ctCashflow);


        //存最新的研发值
        nGbCtDesignProgressService.updateById(nGbCtDesignProgress);


        //重新计算报表
        //计算报表,判断是否破产
        isBankruptcy(userId,contestId,currentTime);

        return R.success();
    }



    @Autowired(required = false)
    private NGbCtDesignFeatureMapper nGbCtDesignFeatureMapper;


    /**
     * 获取产品特性
     * @param userId
     * @param contestId
     * @return
     */
    public R designFeatureList(Integer userId, Integer contestId) {
        List<NGbCtGzProduct> list  = nGbCtDesignFeatureMapper.getProductList(userId,contestId);
        return R.success().put("list",list);
    }




    @Autowired
    private NGbCtGzProductDesignMapper nGbCtGzProductDesignMapper;

    /**
     * 产品特性添加
     * @param userId
     * @param nGbCtDesignFeature
     * @return
     */
    public R designFeatureAdd(Integer userId, NGbCtDesignFeature nGbCtDesignFeature) {

        Integer contestId = nGbCtDesignFeature.getContestId();

        //阻止暂停状态下 和 破产状态下的用户进行操作
        preventAction(contestId,userId);

        Integer date = gbContestStudentService.getStudentDate(contestId,userId);
        NGbContest ngbContest = nGbContestService.getById(contestId);

        Integer currentTime = date/10;
        //产品编号
        Integer cpId = nGbCtDesignFeature.getCpId();
        //查询该产品之前的特性
        NGbCtDesignFeature nGbCtDesignFeature1 = nGbCtDesignFeatureMapper.selectOne(new LambdaQueryWrapper<NGbCtDesignFeature>()
                .eq(NGbCtDesignFeature::getContestId,contestId)
                .eq(NGbCtDesignFeature::getStudentId,userId)
                .eq(NGbCtDesignFeature::getCpId,cpId)
                .orderByDesc(NGbCtDesignFeature::getId)
                .last(" limit 1")
        );
        //如果之前该产品之前已经有特性了，那么特性不能相同,否
        if(nGbCtDesignFeature1!=null){
            if(nGbCtDesignFeature1.getFeatureNum().equals(nGbCtDesignFeature.getFeatureNum())){
                throw new CustomException(CodeEnum.OTHER_ERROR.getCode(), "产品特性未发生改变,请刷新重试");
            }
        }


        //计算需要花费多少钱
        NGbCtGzProductDesign nGbCtGzProductDesign  = nGbCtGzProductDesignMapper.selectOne(new LambdaQueryWrapper<NGbCtGzProductDesign>()
                .eq(NGbCtGzProductDesign::getSubjectNum,ngbContest.getSubjectNumber())
                .eq(NGbCtGzProductDesign::getDesignNum,nGbCtDesignFeature.getFeatureNum())
                .orderByDesc(NGbCtGzProductDesign::getId)
                .last("limit 1")
        );
        //设计花费
        Integer designCost = nGbCtGzProductDesign.getDesignCost();

        int cash = ctCashflowService.getCash(userId, contestId);
        // 获取消耗现金的金额
        int remain = cash - designCost;
        if (remain < 0) {
            throw new CustomException(ErrorEnum.CASH_NOT_ENOUGH.getCode(), ErrorEnum.CASH_NOT_ENOUGH.getMsg());
        }
        //记录现金流量表
        NGbCtCashflow ctCashflow = new NGbCtCashflow();
        ctCashflow.setCDate(currentTime)
                .setCSurplus(remain)
                .setCIn(0)
                .setCOut(designCost)
                .setCAction(CashFlowActionEnum.CPSJ.getAction())
                .setContestId(contestId)
                .setStudentId(userId)
                .setCComment(nGbCtDesignFeature.getCpName()+"产品设计"+nGbCtDesignFeature.getFeatureName()+"花费"+ designCost + "元");
        ctCashflowService.save(ctCashflow);


        //保存产品设计记录
        nGbCtDesignFeature.setStudentId(userId);
        nGbCtDesignFeature.setFeatureName(nGbCtGzProductDesign.getFeatureName());
        nGbCtDesignFeatureMapper.insert(nGbCtDesignFeature);


        //重新计算报表
        //计算报表,判断是否破产
        isBankruptcy(userId,contestId,currentTime);

        return R.success();
    }


    /**
     * 根据产品ID获取最新的产品特性
     * @param userId
     * @param nGbCtDesignFeature
     * @return
     */
    public R getZxFeature(Integer userId, NGbCtDesignFeature nGbCtDesignFeature) {
       Integer cpId = nGbCtDesignFeature.getCpId();
       Integer contestId = nGbCtDesignFeature.getContestId();

        NGbCtDesignFeature data = nGbCtDesignFeatureMapper.selectOne(new LambdaQueryWrapper<NGbCtDesignFeature>()
               .eq(NGbCtDesignFeature::getContestId,contestId)
               .eq(NGbCtDesignFeature::getStudentId,userId)
               .eq(NGbCtDesignFeature::getCpId,cpId)
               .orderByDesc(NGbCtDesignFeature::getId)
               .last(" limit 1")
       );
       return R.success().put("data",data);
    }


    @Autowired
    private NGbCtPdcaMapper nGbCtPdcaMapper;

    /**
     * 获取本年的PDCA数据
     *
     * @param nGbCtPdca
     * @return
     */
    public R getPdca(NGbCtPdca nGbCtPdca) {
        Integer contestId = nGbCtPdca.getContestId();
        Integer studentId = nGbCtPdca.getStudentId();
        Integer year = nGbCtPdca.getYear();
        NGbCtPdca pdca  = getYearPdca(contestId,studentId,year);
        return R.success().put("data", pdca);
    }


    public NGbCtPdca getYearPdca(Integer contestId,Integer studentId,Integer year){
        NGbCtPdca nGbCtPdca = nGbCtPdcaMapper.selectOne(new LambdaQueryWrapper<NGbCtPdca>()
                .eq(NGbCtPdca::getContestId, contestId)
                .eq(NGbCtPdca::getStudentId, studentId)
                .eq(NGbCtPdca::getYear, year)
        );

        //如果PDCA已经填写的情况下，再计算PDCA的实际值及其得分
        //判断本年的报表是否已经填报，填报之后才能查询实际值及其得分
        NGbCtCharges nGbCtCharges = ctChargesService.getOne(new LambdaQueryWrapper<NGbCtCharges>()
                .eq(NGbCtCharges::getContestId,contestId)
                .eq(NGbCtCharges::getStudentId,studentId)
                .eq(NGbCtCharges::getCDate,year)
                .eq(NGbCtCharges::getBsIsxt,0) //手工录入
                .eq(NGbCtCharges::getIsSubmit,"Y") //已提交
        );
        if((nGbCtPdca!=null) && (nGbCtCharges!=null)){
            //获取本年的收入目标实际值 && 得分
            List<NGbCtXzOrder> nGbCtXzOrders = ctXzOrderMapper.selectList(new LambdaQueryWrapper<NGbCtXzOrder>()
                    .eq(NGbCtXzOrder::getContestId,contestId)
                    .eq(NGbCtXzOrder::getStudentId,studentId)
                    .eq(NGbCtXzOrder::getDate,year) //年份
                    .isNotNull(NGbCtXzOrder::getCommitDate) //已交货
            );
            Integer sjzSrmb = 0;
            if(nGbCtXzOrders!=null && nGbCtXzOrders.size()>0){
                sjzSrmb =  nGbCtXzOrders.stream().mapToInt(item->item.getApplyPrice() * item.getAssignedNum()).sum();
            }
            nGbCtPdca.setSjzSrmb(sjzSrmb);
            nGbCtPdca.setScoreSrmb(calScore(nGbCtPdca.getSrmb(),sjzSrmb)); //得分


            //获取本年的产线建设数量实际值 && 得分
            List<NGbCtLine>  nGbCtLines = ctLineMapper.getYearJcLineList(contestId,studentId,year);
            Integer sjzCxjsNum = 0;
            if(nGbCtLines!=null && nGbCtLines.size()>0){
                sjzCxjsNum = nGbCtLines.size();
            }
            nGbCtPdca.setSjzCxjsNum(sjzCxjsNum);
            nGbCtPdca.setScoreCxjsNum(calScore(nGbCtPdca.getCxjsNum(),sjzCxjsNum)); //得分



            //获取本年的产品入库数量数量实际值 && 得分
            List<NGbCtKcProduct> nGbCtKcProducts = ctKcProductMapper.getYearRkList(contestId,studentId,year);
            Integer sjzCprkNum = 0;
            if(nGbCtKcProducts!=null && nGbCtKcProducts.size()>0){
                sjzCprkNum =  nGbCtKcProducts.size();
            }
            nGbCtPdca.setSjzCprkNum(sjzCprkNum);
            nGbCtPdca.setScoreCprkNum(calScore(nGbCtPdca.getCprkNum(),sjzCprkNum)); //得分


            //获取本年的工人招聘数量实际值 && 得分
            List<NGbCtWorker> nGbCtWorkers = nGbCtWorkerMapper.getYearRzList(contestId,studentId,year);
            Integer sjzGrzpNum = 0;
            if(nGbCtWorkers!=null && nGbCtWorkers.size()>0){
                sjzGrzpNum =  nGbCtWorkers.size();
            }
            nGbCtPdca.setSjzGrzpNum(sjzGrzpNum);
            nGbCtPdca.setScoreGrzpNum(calScore(nGbCtPdca.getGrzpNum(),sjzGrzpNum)); //得分


            //获取本年的市场开拓数量实际值 && 得分
            List<NGbCtYfMarket> nGbCtYfMarkets = ctYfMarketMapper.getYearYfList(contestId,studentId,year);
            Integer sjzScktNum = 0;
            if(nGbCtYfMarkets!=null && nGbCtYfMarkets.size()>0){
                sjzScktNum =  nGbCtYfMarkets.size();
            }
            nGbCtPdca.setSjzScktNum(sjzScktNum);
            nGbCtPdca.setScoreScktNum(calScore(nGbCtPdca.getScktNum(),sjzScktNum)); //得分


            //获取本年的产品研发数量实际值 && 得分
            List<NGbCtYfProduct> nGbCtYfProducts = ctYfProductMapper.getYearYfList(contestId,studentId,year);
            Integer sjzCpyfNum = 0;
            if(nGbCtYfProducts!=null && nGbCtYfProducts.size()>0){
                sjzCpyfNum =  nGbCtYfProducts.size();
            }
            nGbCtPdca.setSjzCpyfNum(sjzCpyfNum);
            nGbCtPdca.setScoreCpyfNum(calScore(nGbCtPdca.getCpyfNum(),sjzCpyfNum)); //得分



            //获取本年的贷款额度实际值 && 得分
            List<NGbCtBankLoan> nGbCtBankLoans = ctBankLoanMapper.getYearDkList(contestId,studentId,year);
            Integer sjzDked = 0;
            if(nGbCtBankLoans!=null && nGbCtBankLoans.size()>0){
                sjzDked = nGbCtBankLoans.stream().mapToInt(NGbCtBankLoan::getBlFee).sum();
            }
            nGbCtPdca.setSjzDked(sjzDked);
            nGbCtPdca.setScoreDked(calScore(nGbCtPdca.getDked(),sjzDked)); //得分



            //获取本年的广告投放金额实际值 && 得分
            List<NGbCtMnAd> nGbCtMnAds = nGbCtMnAdMapper.selectList(new LambdaQueryWrapper<NGbCtMnAd>()
                    .eq(NGbCtMnAd::getContestId,contestId)
                    .eq(NGbCtMnAd::getStudentId,studentId)
                    .eq(NGbCtMnAd::getYear,year)
            );
            Integer sjzGgtf = 0;
            if(nGbCtMnAds!=null && nGbCtMnAds.size()>0){
                sjzGgtf = nGbCtMnAds.stream().mapToInt(NGbCtMnAd::getAmount).sum();
            }
            nGbCtPdca.setSjzGgtf(sjzGgtf);
            nGbCtPdca.setScoreGgtf(calScore(nGbCtPdca.getGgtf(),sjzGgtf)); //得分


            //获取本年的营销部门预算实际值 && 得分
            List<NGbCtCashflow> nGbCtCashflows1= ctCashflowService.list(new LambdaQueryWrapper<NGbCtCashflow>()
                    .eq(NGbCtCashflow::getContestId,contestId)
                    .eq(NGbCtCashflow::getStudentId,studentId)
                    .in(NGbCtCashflow::getCAction,NGbCtCashflow.getActionListByDeptType("1"))
                    .apply("floor(c_date / 10) = {0}", year)
            );
            Integer sjzYxbmys = 0;
            if(nGbCtCashflows1!=null && nGbCtCashflows1.size()>0){
                sjzYxbmys = nGbCtCashflows1.stream().mapToInt(NGbCtCashflow::getCOut).sum();
            }
            nGbCtPdca.setSjzYxbmys(sjzYxbmys);
            nGbCtPdca.setScoreYxbmys(calScore(nGbCtPdca.getYxbmys(),sjzYxbmys)); //得分




            //获取本年的生产部门预算实际值 && 得分
            List<NGbCtCashflow> nGbCtCashflows2= ctCashflowService.list(new LambdaQueryWrapper<NGbCtCashflow>()
                    .eq(NGbCtCashflow::getContestId,contestId)
                    .eq(NGbCtCashflow::getStudentId,studentId)
                    .in(NGbCtCashflow::getCAction,NGbCtCashflow.getActionListByDeptType("2"))
                    .apply("floor(c_date / 10) = {0}", year)
            );
            Integer sjzScbmys = 0;
            if(nGbCtCashflows2!=null && nGbCtCashflows2.size()>0){
                sjzScbmys = nGbCtCashflows2.stream().mapToInt(NGbCtCashflow::getCOut).sum();
            }
            nGbCtPdca.setSjzScbmys(sjzScbmys);
            nGbCtPdca.setScoreScbmys(calScore(nGbCtPdca.getScbmys(),sjzScbmys)); //得分



            //获取本年的人力部门预算实际值 && 得分
            List<NGbCtCashflow> nGbCtCashflows3= ctCashflowService.list(new LambdaQueryWrapper<NGbCtCashflow>()
                    .eq(NGbCtCashflow::getContestId,contestId)
                    .eq(NGbCtCashflow::getStudentId,studentId)
                    .in(NGbCtCashflow::getCAction,NGbCtCashflow.getActionListByDeptType("3"))
                    .apply("floor(c_date / 10) = {0}", year)
            );
            Integer sjzRlbmys = 0;
            if(nGbCtCashflows3!=null && nGbCtCashflows3.size()>0){
                sjzRlbmys = nGbCtCashflows3.stream().mapToInt(NGbCtCashflow::getCOut).sum();
            }
            nGbCtPdca.setSjzRlbmys(sjzRlbmys);
            nGbCtPdca.setScoreRlbmys(calScore(nGbCtPdca.getRlbmys(),sjzRlbmys)); //得分

            nGbCtPdca.setShowScore(true); //显示得分
        }

        return nGbCtPdca;
    }


    public Integer calScore(Integer yjValue,Integer sjValue){
        //预计值和实际值都为0则为满分
        if(yjValue==0 && sjValue == 0){
            return 100;
        }
        //预计值为0分，实际值不等于预计值为0分
        else if(yjValue==0 && sjValue!=0){
            return 0;
        }
        else{
             //（1- abs ( 预计 - 实际) /预计）* 总分 。 结果为负数视为0分
           BigDecimal bigDecimalResult = new BigDecimal(1).subtract(  new BigDecimal(yjValue).subtract(new BigDecimal(sjValue)).divide(new BigDecimal(yjValue),2, BigDecimal.ROUND_HALF_UP).abs()).multiply(new BigDecimal(100));
           int result  =  bigDecimalResult.intValue();
           if(result<0){
               return 0;
           }else{
               return result;
           }
        }
    }



    /**
     *
     * @param nGbCtPdca
     * @return
     */
    public R submitPdca(NGbCtPdca nGbCtPdca) {
        Integer contestId = nGbCtPdca.getContestId();
        Integer studentId = nGbCtPdca.getStudentId();

        //阻止暂停状态下 和 破产状态下的用户进行操作
        preventAction(contestId,studentId);

        Integer year = nGbCtPdca.getYear();

        //只有第一年 和 第二年的一季度才能提交PDCA
        NGbContestStudent gbContestStudent= gbContestStudentService.getOne(new LambdaQueryWrapper<NGbContestStudent>()
                .eq(NGbContestStudent::getStudentId,studentId)
                .eq(NGbContestStudent::getContestId,contestId)
        );
        Integer currentTime = gbContestStudent.getDate()/10;
        if(year==1){
            if(currentTime!=11){
                throw new CustomException(CodeEnum.OTHER_ERROR.getCode(), "PDCA仅第一/二年的一季度才能填报");
            }
        }else if(year==2){
            if(currentTime!=21){
                throw new CustomException(CodeEnum.OTHER_ERROR.getCode(), "PDCA仅第一/二年的一季度才能填报");
            }
        }else{
            throw new CustomException(CodeEnum.OTHER_ERROR.getCode(), "PDCA仅第一/二年的一季度才能填报");
        }

        NGbCtPdca nGbCtPdca1 = nGbCtPdcaMapper.selectOne(new LambdaQueryWrapper<NGbCtPdca>()
                .eq(NGbCtPdca::getContestId, contestId)
                .eq(NGbCtPdca::getStudentId, studentId)
                .eq(NGbCtPdca::getYear, year)
        );
        if(nGbCtPdca1!=null){
            throw new CustomException(CodeEnum.OTHER_ERROR.getCode(), "本年PDCA已提交，请刷新");
        }
        nGbCtPdcaMapper.insert(nGbCtPdca);
        return  R.success();
    }


    @Autowired
    private NGbBuyDataConsultMapper nGbBuyDataConsultMapper;

    /**
     * 购买xx人的情报
     * @param nGbBuyDataConsult
     * @return
     */
    public R buyDataConsult(NGbBuyDataConsult nGbBuyDataConsult) {
        Integer contestId = nGbBuyDataConsult.getContestId();
        Integer studentId = nGbBuyDataConsult.getStudentId();

        //阻止暂停状态下 和 破产状态下的用户进行操作
        preventAction(contestId,studentId);

        Integer zxStudentId = nGbBuyDataConsult.getZxStudentId();
        Integer date = nGbBuyDataConsult.getDate();

        //查询本季度是否已经购买了该学生的情报
        List<NGbBuyDataConsult> nGbBuyDataConsults = nGbBuyDataConsultMapper.selectList(new LambdaQueryWrapper<NGbBuyDataConsult>()
                .eq(NGbBuyDataConsult::getContestId,contestId)
                .eq(NGbBuyDataConsult::getStudentId,studentId)
                .eq(NGbBuyDataConsult::getZxStudentId,zxStudentId)
                .eq(NGbBuyDataConsult::getDate,date)
        );
        if(nGbBuyDataConsults!=null && nGbBuyDataConsults.size()>0){
            throw new CustomException(CodeEnum.OTHER_ERROR.getCode(), "该组情报已购买，请刷新");
        }
        //查询规则表
        NGbCtGzCs  nGbCtGzCs = ctGzCsService.getByContestId(contestId);
        //信息费
        Integer information  = nGbCtGzCs.getInformation();

        //扣除现金流量表
        int cash = ctCashflowService.getCash(studentId, contestId);
        // 获取消耗现金的金额
        int remain = cash - information;
        if (remain < 0) {
            throw new CustomException(ErrorEnum.CASH_NOT_ENOUGH.getCode(), ErrorEnum.CASH_NOT_ENOUGH.getMsg());
        }
        //记录现金流量表
        NGbCtCashflow ctCashflow = new NGbCtCashflow();
        ctCashflow.setCDate(date)
                .setCSurplus(remain)
                .setCIn(0)
                .setCOut(information)
                .setCAction(CashFlowActionEnum.INFO_OTHER.getAction())
                .setContestId(contestId)
                .setStudentId(studentId)
                .setCComment("购买情报消耗" + information + "元");
        ctCashflowService.save(ctCashflow);

        nGbBuyDataConsultMapper.insert(nGbBuyDataConsult);

        //重新计算报表
        isBankruptcy(studentId,contestId,date);

        return R.success();
    }


    /**
     * 获取购买了情报的用户
     * @param nGbBuyDataConsult
     * @return
     */
    public R getDataConsultList(NGbBuyDataConsult nGbBuyDataConsult) {
        Integer contestId = nGbBuyDataConsult.getContestId();
        Integer studentId = nGbBuyDataConsult.getStudentId();
        Integer date = nGbBuyDataConsult.getDate();

        //获取当前比赛的所有用户 - 得到 studentId -> account 的map
        List<SysUser> userList = nGbContestStudentMapper.getStudentListByContestId(contestId);
        Map<Integer, String> groupNumMap = new HashMap<>();
        userList.stream().forEach(item -> groupNumMap.put(item.getId(), item.getAccount()));


        //查询本季度是否已经购买了该学生的情报
        List<NGbBuyDataConsult> nGbBuyDataConsults = nGbBuyDataConsultMapper.selectList(new LambdaQueryWrapper<NGbBuyDataConsult>()
                .eq(NGbBuyDataConsult::getContestId,contestId)
                .eq(NGbBuyDataConsult::getStudentId,studentId)
                .eq(NGbBuyDataConsult::getDate,date)
        );
        //查询学生对应的组号
        if(CollectionUtils.isNotEmpty(nGbBuyDataConsults)){
            nGbBuyDataConsults.forEach(item->{
                Integer zxStudentId = item.getZxStudentId();
                item.setZxGroupNum(groupNumMap.get(zxStudentId));
            });
        }
        return R.success().put("list",nGbBuyDataConsults);
    }


    /**
     * 碳中和
     * @param nGbCtCarbon
     * @return
     */
    public R carbonNeutraliza(NGbCtCarbon nGbCtCarbon) {

        Integer addNeutralizaNum   = nGbCtCarbon.getNeutralizaNum(); //增加的碳排放量

        Integer contestId = nGbCtCarbon.getContestId();
        Integer studentId = nGbCtCarbon.getStudentId();

        //阻止暂停状态下 和 破产状态下的用户进行操作
        preventAction(contestId,studentId);
        Integer date = gbContestStudentService.getStudentDate(contestId, studentId);
        Integer currentTime = date / 10;
        Integer year = currentTime / 10;

        NGbCtGzCs nGbCtGzCs = ctGzCsService.getByContestId(contestId);
        int zhdj = nGbCtGzCs.getCarbonPrice();  //碳中和单价每吨xx元
        Integer amount = addNeutralizaNum * zhdj;


        //扣除现金流量表
        int cash = ctCashflowService.getCash(studentId, contestId);
        // 获取消耗现金的金额
        int remain = cash - amount;
        if (remain < 0) {
            throw new CustomException(ErrorEnum.CASH_NOT_ENOUGH.getCode(), ErrorEnum.CASH_NOT_ENOUGH.getMsg());
        }


        NGbCtCarbon nGbCtCarbon1 = nGbCtCarbonMapper.selectOne(new LambdaQueryWrapper<NGbCtCarbon>()
                .eq(NGbCtCarbon::getContestId,contestId)
                .eq(NGbCtCarbon::getStudentId,studentId)
                .eq(NGbCtCarbon::getYear,year)
        );
        int emissionNum = nGbCtCarbon1.getEmissionNum();
        int neutralizaNum = nGbCtCarbon1.getNeutralizaNum(); //原本的碳中和量

        neutralizaNum+=addNeutralizaNum;

        //碳中和量大于已排放量
        if(neutralizaNum>emissionNum){
            throw new CustomException(CodeEnum.OTHER_ERROR.getCode(), "碳中和量超过碳排放，请刷新后重新提交");
        }

        nGbCtCarbon1.setNeutralizaNum(neutralizaNum); //加上本次申请的碳中和量
        nGbCtCarbonMapper.updateById(nGbCtCarbon1); //更新碳中和的量

        //记录现金流量表
        NGbCtCashflow ctCashflow = new NGbCtCashflow();
        ctCashflow.setCDate(currentTime)
                .setCSurplus(remain)
                .setCIn(0)
                .setCOut(amount)
                .setCAction(CashFlowActionEnum.TZH.getAction())
                .setContestId(contestId)
                .setStudentId(studentId)
                .setCComment("碳中和"+addNeutralizaNum+"吨，花费"+amount+"元");
        ctCashflowService.save(ctCashflow);

        //重新计算报表
        isBankruptcy(studentId, contestId, currentTime);

        return R.success();
    }

    public R carbonInfo(Integer contestId, Integer userId) {
        Integer date = gbContestStudentService.getStudentDate(contestId, userId);
        Integer currentTime = date / 10;
        Integer year = currentTime / 10;

        //本年已分配排放量  已排放碳量  已中和碳量  本年碳中和率   上年中和率

        NGbCtCarbon nGbCtCarbon = nGbCtCarbonMapper.selectOne(new LambdaQueryWrapper<NGbCtCarbon>()
                .eq(NGbCtCarbon::getContestId,contestId)
                .eq(NGbCtCarbon::getStudentId,userId)
                .eq(NGbCtCarbon::getYear,year)
        );

         Integer emissionNum = nGbCtCarbon.getEmissionNum();
        //计算本年的碳中和率  已中和/已排
        if(emissionNum==0){
            nGbCtCarbon.setNeutralizaRate(new BigDecimal(0));
        }else{
            Integer neutralizaNum = nGbCtCarbon.getNeutralizaNum();
            BigDecimal neutralizaRate = new BigDecimal(neutralizaNum).multiply(new BigDecimal(100)).divide(new BigDecimal(emissionNum),2,RoundingMode.HALF_UP);
            nGbCtCarbon.setNeutralizaRate(neutralizaRate);  //本年的碳中和率
        }


        //上一年的碳中率计算
        NGbCtCarbon lastYearCarbon = nGbCtCarbonMapper.selectOne(new LambdaQueryWrapper<NGbCtCarbon>()
                .eq(NGbCtCarbon::getContestId,contestId)
                .eq(NGbCtCarbon::getStudentId,userId)
                .eq(NGbCtCarbon::getYear,year-1)
        );
        if(lastYearCarbon!=null){
            Integer lastEmissionNum = lastYearCarbon.getEmissionNum();
            //计算本年的碳中和率  已中和/已排
            if(lastEmissionNum==0){
                nGbCtCarbon.setLastYearNeutralizaRate(new BigDecimal(0));
            }else{
                Integer lastNeutralizaNum = lastYearCarbon.getNeutralizaNum();
                BigDecimal lastYearNeutralizaRate = new BigDecimal(lastNeutralizaNum).multiply(new BigDecimal(100)).divide(new BigDecimal(lastEmissionNum),2,RoundingMode.HALF_UP);
                nGbCtCarbon.setLastYearNeutralizaRate(lastYearNeutralizaRate);  //本年的碳中和率
            }
        }else{
            nGbCtCarbon.setLastYearNeutralizaRate(new BigDecimal(0));
        }

        //如果本年未分配则值为0
        if(nGbCtCarbon.getAssignedNum()==null){
            nGbCtCarbon.setAssignedNum(0);
        }

        return R.success().put("data",nGbCtCarbon);
    }




    /**
     * 获取企业的经营排名
     * @param contestId
     * @return
     */
    public R operateRanking(Integer contestId) {
        List result = new ArrayList<>();

        //获取当前比赛的所有用户 - 得到 studentId -> account 的map
        List<SysUser> userList = nGbContestStudentMapper.getStudentListByContestId(contestId);
        Map<Integer, String> groupNumMap = new HashMap<>();
        userList.stream().forEach(item -> groupNumMap.put(item.getId(), item.getAccount()));


        //获取每个学生所有季度的成绩
        List<NGbCtScore> nGbCtScores = nGbCtScoreMapper.selectList(new LambdaQueryWrapper<NGbCtScore>()
                .eq(NGbCtScore::getContestId,contestId)
        );
        //每个学生，每年的成绩取最大的一份

        Map<Integer,List<NGbCtScore>> yearMap = nGbCtScores.stream().collect(Collectors.groupingBy(NGbCtScore::getYear));

        yearMap.forEach((k,v)->{
            Map<String,Object> map = new HashMap<>();
            map.put("year",k); //年份

            //将成绩根据学生分组
            Map<Integer,List<NGbCtScore>> studentScoreMap = v.stream().collect(Collectors.groupingBy(NGbCtScore::getStudentId));

            //每个学生只取当年最新的一份成绩
            List<NGbCtScore> list = new ArrayList<>();
            studentScoreMap.forEach((k1,v1)->{
                //将该学生的得分按照季度倒序排序，以获取最新值
               List<NGbCtScore> nGbCtScoreList = v1.stream().sorted(Comparator.comparing(NGbCtScore::getQuarterly).reversed()).collect(Collectors.toList());
               list.add(nGbCtScoreList.get(0));
            });

            //根据得分进行排名
            list.sort(Comparator.comparingLong(NGbCtScore::getScore).reversed()); // 降序排序
            int currentRank = 1; // 初始化排名为1
            long currentScore = list.get(0).getScore(); // 当前遍历到的分数

            // 设置排名
            for (int index = 0; index < list.size(); index++) {
                //判断当前的得分 是否 和上一名的一样,如果一样的话,排名用上一名的排名
                NGbCtScore nGbCtScore = list.get(index);
                Integer studentId = nGbCtScore.getStudentId();
                nGbCtScore.setGroupNum(groupNumMap.get(studentId));  //企业名

                // 如果当前学生的分数与上一个学生的分数不同，则更新排名
                if (nGbCtScore.getScore() != currentScore) {
                    currentRank = index + 1; // 排名从1开始
                    currentScore = nGbCtScore.getScore(); // 更新当前分数
                }
                nGbCtScore.setRank(currentRank); // 设置当前学生的排名
            }
            map.put("list",list);
            result.add(map);
        });
        return R.success().put("list",result);
    }


    /**
     *
     * @param contestId
     * @return
     */
    public R viewScore(Integer contestId) {
        List result = new ArrayList<>();

        //获取当前比赛的所有用户 - 得到 studentId -> account 的map
        List<SysUser> userList = nGbContestStudentMapper.getStudentListByContestId(contestId);
        Map<Integer, String> groupNumMap = new HashMap<>();
        userList.stream().forEach(item -> groupNumMap.put(item.getId(), item.getAccount()));


        //获取每个学生所有季度的成绩
        List<NGbCtScore> nGbCtScores = nGbCtScoreMapper.selectList(new LambdaQueryWrapper<NGbCtScore>()
                .eq(NGbCtScore::getContestId,contestId)
                .orderByDesc(NGbCtScore::getYear)
                .orderByDesc(NGbCtScore::getQuarterly)
        );
        //每个学生，每年的成绩取最大的一份

        Map<Integer,List<NGbCtScore>> yearMap = nGbCtScores.stream()
                .collect(Collectors.groupingBy(item->item.getYear()*10+item.getQuarterly()));

        Set<Integer> yearSet = yearMap.keySet();
        List<Integer> yearList = new ArrayList<>(yearSet);
        Collections.sort(yearList);

        yearList.forEach(k->{
            Map<String,Object> map = new HashMap<>();
            map.put("date",k); //年季
            //当前季度每个学生的成绩
            List<NGbCtScore> list = yearMap.get(k);
            //根据得分进行排名
            list.sort(Comparator.comparingLong(NGbCtScore::getScore).reversed()); // 降序排序
            int currentRank = 1; // 初始化排名为1
            Long currentScore = list.get(0).getScore(); // 当前遍历到的分数

            // 设置排名
            for (int index = 0; index < list.size(); index++) {
                //判断当前的得分 是否 和上一名的一样,如果一样的话,排名用上一名的排名
                NGbCtScore nGbCtScore = list.get(index);
                Integer studentId = nGbCtScore.getStudentId();
                nGbCtScore.setGroupNum(groupNumMap.get(studentId));  //企业名

                // 如果当前学生的分数与上一个学生的分数不同，则更新排名
                if (nGbCtScore.getScore() != currentScore) {
                    currentRank = index + 1; // 排名从1开始
                    currentScore = nGbCtScore.getScore(); // 更新当前分数
                }
                nGbCtScore.setRank(currentRank); // 设置当前学生的排名
            }
            map.put("list",list);
            result.add(map);
        });

        return R.success().put("list",result);
    }
}
