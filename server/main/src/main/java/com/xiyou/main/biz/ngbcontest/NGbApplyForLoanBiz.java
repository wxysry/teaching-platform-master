package com.xiyou.main.biz.ngbcontest;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.xiyou.common.enums.CodeEnum;
import com.xiyou.common.exception.CustomException;
import com.xiyou.common.utils.R;
import com.xiyou.main.dao.ngbcontest.*;
import com.xiyou.main.entity.ngbcontest.*;
import com.xiyou.main.entity.newcontest.NewCtBankLoan;
import com.xiyou.main.enums.CashFlowActionEnum;
import com.xiyou.main.enums.ErrorEnum;
import com.xiyou.main.params.ngbcontest.NGbWorkerIncentiveParam;
import com.xiyou.main.service.ngbcontest.*;
import com.xiyou.main.utils.DateUtils;
import com.xiyou.main.vo.ngbcontest.NGbCashFolowEntity;
import com.xiyou.main.vo.ngbcontest.NGbMaterialVo;
import io.swagger.models.auth.In;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.*;
import java.util.stream.Collectors;

/**
 *
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class NGbApplyForLoanBiz {

    @Autowired
    NGbCtCashflowService gbCtCashflowService;
    @Autowired
    NGbCtBankLoanService gbCtBankLoanService;
    @Autowired
    NGbCtBalanceService gbCtBalanceService;
    @Autowired
    NGbCtBankLoanMapper gbCtBankLoanMapper;
    @Autowired
    NGbCtGzLoanMapper gbCtGzLoanMapper;
    @Autowired
    NGbCtGzCsMapper gbCtGzCsMapper;
    @Autowired
    NGbActionBiz gbActionBiz;
    @Autowired
    private NGbCtWorkerMapper nGbCtWorkerMapper;
    @Autowired
    private NGbRePaymentInfoMapper gbRePaymentInfoMapper;

    @Autowired
    private  NGbLineBiz nGbLineBiz;

    /**
     * 申请贷款-获取最大贷款额度&需贷款年限
     *
     * @param cashFolowEntity
     * @return
     */
    public R getMaxLoanAmountInfo(NGbCashFolowEntity cashFolowEntity) {
        return R.success().put("data", gbCtBalanceService.getAllMaxLoanAmount(cashFolowEntity));
    }

    /**
     * 申请贷款-确认
     */
    public R applyForLoanCommit( NGbCashFolowEntity cashFolowEntity) {

        //阻止暂停状态下 和 破产状态下的用户进行操作
        gbActionBiz.preventAction(cashFolowEntity.getContestId(),cashFolowEntity.getStudentId());

        //获取融资规则
        NGbCtGzLoan ctGzLoan = gbCtGzLoanMapper.selectById(cashFolowEntity.getGzLoanId());
        Integer loanTime = ctGzLoan.getLoanTime();
        if (loanTime == null) {
            return R.error(CodeEnum.OTHER_ERROR.getCode(), "该规则中没有贷款时间");
        }
        Integer repaymentDate = DateUtils.addYearAndSeasonTime(cashFolowEntity.getCurrentTime() , loanTime);//贷款时间
        Integer loanAmount = cashFolowEntity.getLoanAmount();

        Integer maxLoanAmount = gbCtBalanceService.getMaxLoanAmount(cashFolowEntity);
        if (loanAmount == null  || loanAmount > maxLoanAmount) {
            return ErrorEnum.LOAN_ERROR.getR();
        }

        //先判断当前季度是否已经有该贷款记录
        NGbCtBankLoan ctBankLoan= gbCtBankLoanService.getOne(new LambdaQueryWrapper<NGbCtBankLoan>()
                .eq(NGbCtBankLoan::getContestId,cashFolowEntity.getContestId())
                .eq(NGbCtBankLoan::getStudentId,cashFolowEntity.getStudentId())
                .eq(NGbCtBankLoan::getBlName,ctGzLoan.getLoanName())
                .eq(NGbCtBankLoan::getBlAddTime,cashFolowEntity.getCurrentTime())
                .last("limit 1")
        );
        if(ctBankLoan==null){
            //每期利息
            BigDecimal interest = BigDecimal.valueOf(loanAmount).multiply(ctGzLoan.getLoanRate()).divide(BigDecimal.valueOf(100),0, BigDecimal.ROUND_UP);
            BigDecimal allInterest = new BigDecimal(0);
            if("2".equals(ctGzLoan.getLoanRepayment())){
                allInterest = interest.multiply(new BigDecimal(loanTime));
            }else {
                allInterest = interest;
            }

            //插入贷款记录
            NGbCtBankLoan loan = new NGbCtBankLoan();
            loan.setContestId(cashFolowEntity.getContestId())
                    .setStudentId(cashFolowEntity.getStudentId())
                    .setBlType((ctGzLoan.getLoanTime()!=null && ctGzLoan.getLoanTime()> 4 )? NewCtBankLoan.LONG_LOAN:NewCtBankLoan.SHORT_LOAN)//贷款方式
                    .setBlName(ctGzLoan.getLoanName())//贷款名称
                    .setBlAddTime(cashFolowEntity.getCurrentTime())//贷款时间
                    .setBlFee(loanAmount)//贷款金额
                    .setRepaymentType(ctGzLoan.getLoanRepayment())//还款方式
                    .setBlRepaymentDate(repaymentDate)//还款时间
                    .setRate(ctGzLoan.getLoanRate())//利率
                    .setInterest(allInterest)//利息
                    .setBlRemainTime(ctGzLoan.getLoanTime())//贷款时长
                    .setIsDelete(0); //未删除
            gbCtBankLoanService.save(loan);

            //生成还款信息
            if("2".equals(ctGzLoan.getLoanRepayment())) {
                for (int i = 1; i <= ctGzLoan.getLoanTime(); i++) {
                    Integer payTime = DateUtils.addYearAndSeasonTime(cashFolowEntity.getCurrentTime(), i);
                    NGbRePaymentInfo info = new NGbRePaymentInfo();
                    info.setAmount(interest)
                            .setBlAddTime(loan.getBlAddTime())
                            .setPayment(CashFlowActionEnum.INTEREST_I.getAction())//每季付息-利息
                            .setContestId(cashFolowEntity.getContestId())
                            .setStudentId(cashFolowEntity.getStudentId())
                            .setLoanId(loan.getBlId())
                            .setPayTime(payTime)//缴费时间
                            .setDescription("利息");//说明
                    gbRePaymentInfoMapper.insert(info);
                    if (i == ctGzLoan.getLoanTime()) {
                        NGbRePaymentInfo infoB = new NGbRePaymentInfo();
                        infoB.setAmount(new BigDecimal(loanAmount))
                                .setBlAddTime(loan.getBlAddTime())
                                .setPayment(CashFlowActionEnum.INTEREST_R.getAction())//每季付息-本金
                                .setContestId(cashFolowEntity.getContestId())
                                .setStudentId(cashFolowEntity.getStudentId())
                                .setLoanId(loan.getBlId())
                                .setPayTime(payTime)//缴费时间
                                .setDescription("本金");//说明本息同还
                        gbRePaymentInfoMapper.insert(infoB);
                    }
                }
            }else {
                Integer payTime = DateUtils.addYearAndSeasonTime(cashFolowEntity.getCurrentTime(), ctGzLoan.getLoanTime());
                NGbRePaymentInfo info1 = new NGbRePaymentInfo();
                info1.setAmount(interest)
                        .setBlAddTime(loan.getBlAddTime())
                        .setPayment(CashFlowActionEnum.REPAYMENT_INTEREST_I.getAction())//本息同还-利息
                        .setContestId(cashFolowEntity.getContestId())
                        .setStudentId(cashFolowEntity.getStudentId())
                        .setLoanId(loan.getBlId())
                        .setPayTime(payTime)//缴费时间
                        .setDescription("利息");//说明
                gbRePaymentInfoMapper.insert(info1);

                NGbRePaymentInfo info2 = new NGbRePaymentInfo();
                info2.setAmount(new BigDecimal(loanAmount))
                        .setBlAddTime(loan.getBlAddTime())
                        .setPayment(CashFlowActionEnum.REPAYMENT_INTEREST_R.getAction())//本息同还-本金
                        .setContestId(cashFolowEntity.getContestId())
                        .setStudentId(cashFolowEntity.getStudentId())
                        .setLoanId(loan.getBlId())
                        .setPayTime(payTime)//缴费时间
                        .setDescription("本金");//说明
                gbRePaymentInfoMapper.insert(info2);
            }

        }
        //本季已贷款同种类型的款项
        else{
            //获取总的贷款金额
            Integer allLoanAmount = cashFolowEntity.getLoanAmount()+ctBankLoan.getBlFee();

            //每期利息
            BigDecimal interest = BigDecimal.valueOf(allLoanAmount)
                    .multiply(ctGzLoan.getLoanRate())
                    .divide(BigDecimal.valueOf(100),0, BigDecimal.ROUND_UP);
            BigDecimal allInterest = new BigDecimal(0);
            if("2".equals(ctGzLoan.getLoanRepayment())){
                allInterest = interest.multiply(new BigDecimal(loanTime));
            }else {
                allInterest = interest;
            }

            //更新贷款记录表
            ctBankLoan.setBlFee(allLoanAmount)//贷款金额
                    .setInterest(allInterest);//利息
            gbCtBankLoanService.updateById(ctBankLoan);

            //生成还款信息
            //2代表每季付息-最后本息偿还
            if("2".equals(ctGzLoan.getLoanRepayment())) {
                for (int i = 1; i <= ctGzLoan.getLoanTime(); i++) {
                    Integer payTime = DateUtils.addYearAndSeasonTime(cashFolowEntity.getCurrentTime(), i);
                    NGbRePaymentInfo info = gbRePaymentInfoMapper.selectOne(new LambdaQueryWrapper<NGbRePaymentInfo>()
                            .eq(NGbRePaymentInfo::getContestId,cashFolowEntity.getContestId())
                            .eq(NGbRePaymentInfo::getStudentId,cashFolowEntity.getStudentId())
                            .eq(NGbRePaymentInfo::getBlAddTime,ctBankLoan.getBlAddTime())
                            .eq(NGbRePaymentInfo::getPayment,CashFlowActionEnum.INTEREST_I.getAction()) //每季付息-利息
                            .eq(NGbRePaymentInfo::getLoanId,ctBankLoan.getBlId())
                            .eq(NGbRePaymentInfo::getPayTime,payTime)
                            .last("limit 1")
                    );

                    info.setAmount(interest);
                    gbRePaymentInfoMapper.updateById(info);
//

                    //最后一季本息同还
                    if (i == ctGzLoan.getLoanTime()) {
                        NGbRePaymentInfo infoB = gbRePaymentInfoMapper.selectOne(new LambdaQueryWrapper<NGbRePaymentInfo>()
                                .eq(NGbRePaymentInfo::getContestId,cashFolowEntity.getContestId())
                                .eq(NGbRePaymentInfo::getStudentId,cashFolowEntity.getStudentId())
                                .eq(NGbRePaymentInfo::getBlAddTime,ctBankLoan.getBlAddTime())
                                .eq(NGbRePaymentInfo::getPayment,CashFlowActionEnum.INTEREST_R.getAction()) //每季付息-本金
                                .eq(NGbRePaymentInfo::getLoanId,ctBankLoan.getBlId())
                                .eq(NGbRePaymentInfo::getPayTime,payTime)
                                .last("limit 1")
                        );
                        infoB.setAmount(new BigDecimal(allLoanAmount));
                        gbRePaymentInfoMapper.updateById(infoB);
                    }
                }
            }
            //代表本息同还
            else {
                Integer payTime = DateUtils.addYearAndSeasonTime(cashFolowEntity.getCurrentTime(), ctGzLoan.getLoanTime());
                NGbRePaymentInfo info1 = gbRePaymentInfoMapper.selectOne(new LambdaQueryWrapper<NGbRePaymentInfo>()
                        .eq(NGbRePaymentInfo::getContestId,cashFolowEntity.getContestId())
                        .eq(NGbRePaymentInfo::getStudentId,cashFolowEntity.getStudentId())
                        .eq(NGbRePaymentInfo::getBlAddTime,ctBankLoan.getBlAddTime())
                        .eq(NGbRePaymentInfo::getPayment,CashFlowActionEnum.REPAYMENT_INTEREST_I.getAction()) //本息同还-利息
                        .eq(NGbRePaymentInfo::getLoanId,ctBankLoan.getBlId())
                        .eq(NGbRePaymentInfo::getPayTime,payTime)
                        .last("limit 1")
                );
                info1.setAmount(interest);
                gbRePaymentInfoMapper.updateById(info1);



                NGbRePaymentInfo info2 =  gbRePaymentInfoMapper.selectOne(new LambdaQueryWrapper<NGbRePaymentInfo>()
                        .eq(NGbRePaymentInfo::getContestId,cashFolowEntity.getContestId())
                        .eq(NGbRePaymentInfo::getStudentId,cashFolowEntity.getStudentId())
                        .eq(NGbRePaymentInfo::getBlAddTime,ctBankLoan.getBlAddTime())
                        .eq(NGbRePaymentInfo::getPayment,CashFlowActionEnum.REPAYMENT_INTEREST_R.getAction()) //本息同还-本金
                        .eq(NGbRePaymentInfo::getLoanId,ctBankLoan.getBlId())
                        .eq(NGbRePaymentInfo::getPayTime,payTime)
                        .last("limit 1")
                );
                info2.setAmount(new BigDecimal(allLoanAmount));
                gbRePaymentInfoMapper.updateById(info2);
            }
        }



        //插入现金交易记录
        Integer cash = gbCtCashflowService.getCash(cashFolowEntity.getStudentId(), cashFolowEntity.getContestId());
        NGbCtCashflow ctCashflow = new NGbCtCashflow();
        ctCashflow.setStudentId(cashFolowEntity.getStudentId())
                .setContestId(cashFolowEntity.getContestId())
                .setCIn(loanAmount)
                .setCOut(0)
                .setCDate(cashFolowEntity.getCurrentTime())
                .setCSurplus(cash + loanAmount)
                .setCComment("贷款" + loanAmount + "元")
                .setCAction((ctGzLoan.getLoanTime()!=null && ctGzLoan.getLoanTime()> 4)? CashFlowActionEnum.LONG_LOAN.getAction():CashFlowActionEnum.SHORT_LOAN.getAction());
        gbCtCashflowService.save(ctCashflow);


        //计算报表
        gbActionBiz.isBankruptcy(cashFolowEntity.getStudentId(),cashFolowEntity.getContestId(),cashFolowEntity.getCurrentTime());

        return R.success();
    }

    /**
     * 获取融资（贷款）信息
     * @param contestId 竞赛ID
     * @param studentId 学生ID
     * @return
     */
    public R getLoanBankInfo(Integer contestId,Integer studentId){
        List<NGbCtBankLoan> list = gbCtBankLoanMapper.getList(contestId, studentId);
        return R.success().put("list",list);
    }

    /**
     * 获取融资（贷款）规则
     * @param contestId 竞赛ID
     * @return
     */
    public R getLoanGz(Integer contestId){
        List<NGbCtGzLoan> gbCtGzLoans = gbCtGzLoanMapper.listCtGzLoan(contestId);
        return R.success().put("data",gbCtGzLoans);
    }


    /**
     * 生成管理费信息（缴费了的就返回null）
     * @param contestId
     * @param studentId
     * @param currentTime
     * @return
     */
    public NGbRePaymentInfo getOverhaul(Integer contestId,Integer studentId ,Integer currentTime ){
        NGbRePaymentInfo overhaulInfo = gbRePaymentInfoMapper.getOverhaul(contestId, studentId, currentTime);
        if(overhaulInfo == null){
            NGbCtGzCs ctGz = gbCtGzCsMapper.getByContestId(contestId);
            int overhaul = ctGz.getOverhaul();//获取管理费
            overhaul = overhaul *3; //每季度管理费 * 3
            NGbRePaymentInfo info = new NGbRePaymentInfo();
            info.setAmount(new BigDecimal(overhaul))
                    .setStudentId(studentId)
                    .setContestId(contestId)
                    .setPayment("管理费")
                    .setBlAddTime(currentTime)
                    .setDescription("每季"+overhaul+"元");
            gbRePaymentInfoMapper.insert(info);
            return info;
        }else if(overhaulInfo.getPayTime() == null || overhaulInfo.getPayTime() == 0){
            return overhaulInfo;
        }
        return null;
    }

    /**
     * 生成管理费信息（缴费了的就返回null）
     * @param contestId
     * @param studentId
     * @param currentTime
     * @return
     */
    public void payOverhaul(Integer contestId,Integer studentId ,Integer currentTime){
        //查询是否有需要缴纳的管理费
        NGbRePaymentInfo gbRePaymentInfo = getOverhaul(contestId, studentId, currentTime);
        //如果已经缴费,填写校验避免重复缴费
        if(gbRePaymentInfo == null){
            return ;
        }
        Integer overhaul = gbRePaymentInfo.getAmount().intValue();
        //获取现金
        Integer cash = gbCtCashflowService.getCash(studentId, contestId);
        NGbCtCashflow gbCtCashflow = new NGbCtCashflow();
        gbCtCashflow.setStudentId(studentId);
        gbCtCashflow.setContestId(contestId);
        gbCtCashflow.setCDate(currentTime);
        gbCtCashflow.setCSurplus(cash - overhaul);
        gbCtCashflow.setCComment("支付管理费:"+overhaul+"元");
        gbCtCashflow.setCIn(0);
        gbCtCashflow.setCOut(overhaul);
        gbCtCashflow.setCAction(CashFlowActionEnum.PAY_OVERHAUL.getAction());
        gbCtCashflowService.save(gbCtCashflow);
        //更新管理费表
        gbRePaymentInfo.setPayTime(currentTime);
        gbRePaymentInfoMapper.updateById(gbRePaymentInfo);
    }




    @Autowired
    private NGbCtCashflowMapper gbCtCashflowMapper;
    @Autowired
    private NGbContestMapper gbContestMapper;
    /**
     *
     * @param currentTime
     * @param contestId
     * @param studentId
     * @return
     */
    public R startCurrSeason(Integer currentTime,Integer contestId ,Integer studentId){

        //获取管理费
        List<NGbRePaymentInfo> listData = new ArrayList<>();
        NGbRePaymentInfo overhaul = getOverhaul(contestId, studentId, currentTime);
        if(overhaul != null){
            listData.add(overhaul);
        }
        //需要缴纳利息和本金
        List<NGbRePaymentInfo> bankLoans = getBankLoan(currentTime, contestId, studentId);
        listData.addAll(bankLoans);

        //获取贷款利息
        return R.success().put("data",listData);
    }


    /**
     * 获取利息和贷款
     * @param currentTime
     * @param contestId
     * @param studentId
     * @return
     */
    public List<NGbRePaymentInfo> getBankLoan(Integer currentTime, Integer contestId , Integer studentId){
        //获取当季需要偿还的贷款利息和本金
        List<NGbRePaymentInfo> listBankInfo = gbRePaymentInfoMapper.getListByTime(contestId, studentId, currentTime);
        return listBankInfo;
    }
    /**
     * 支付当季利息或本金
     * @param contestId
     * @param studentId
     * @param currentTime
     * @param amount
     * @param payment
     * @param blAddTime
     * @param cash
     */
    public void payCurrSeasonAddCashFlow(Integer contestId,Integer studentId ,Integer currentTime,Integer amount,String payment,Integer blAddTime,Integer cash){
        cash = cash - amount;
        if(CashFlowActionEnum.INTEREST_R.getAction().equals(payment) || CashFlowActionEnum.REPAYMENT_INTEREST_R.getAction().equals(payment)){
            List<NGbRePaymentInfo> gbRePaymentInfos = gbRePaymentInfoMapper.selectByAmountAndBlAddTimeAndPayment(contestId, studentId, currentTime, payment, blAddTime);
            for (NGbRePaymentInfo gbRePaymentInfo : gbRePaymentInfos) {

                //贷款已经还清的会将其标记为删除
                NGbCtBankLoan nGbCtBankLoan = new NGbCtBankLoan();
                nGbCtBankLoan.setBlId(gbRePaymentInfo.getLoanId());
                nGbCtBankLoan.setIsDelete(1);
                gbCtBankLoanMapper.updateById(nGbCtBankLoan);
            }
        }
        //生成流量表
        NGbCtCashflow gbCtCashflow = new NGbCtCashflow();
        gbCtCashflow.setStudentId(studentId);
        gbCtCashflow.setContestId(contestId);
        gbCtCashflow.setCDate(currentTime);
        gbCtCashflow.setCSurplus(cash);
        gbCtCashflow.setCComment("偿还本金或利息"+amount+"元");
        gbCtCashflow.setCIn(0);
        gbCtCashflow.setCOut(amount);
        gbCtCashflow.setCAction(payment);
        gbCtCashflowService.save(gbCtCashflow);
        //删除
        int i = gbRePaymentInfoMapper.deleteByAmountAndBlAddTimeAndPayment(contestId,studentId,currentTime,payment,blAddTime);
        if(i==0){
            throw new CustomException(CodeEnum.OTHER_ERROR.getCode(), "当前费用已完成缴纳,无需重复缴纳");
        }
    }

    /**
     * 缴费
     * @param currentTime
     * @param contestId
     * @param studentId
     * @param
     * @return
     */
    public R payCurrSeason(Integer contestId,Integer studentId,Integer currentTime,String payment,Integer blAddTime,Integer amount){

        //阻止暂停状态下 和 破产状态下的用户进行操作
        gbActionBiz.preventAction(contestId,studentId);

        Integer cash = gbCtCashflowService.getCash(studentId, contestId);
        if(cash<amount){
            return ErrorEnum.CASH_NOT_ENOUGH.getR();
        }

        if("管理费".equals(payment)){
            payOverhaul(contestId,studentId,currentTime);
        }else {
            payCurrSeasonAddCashFlow(contestId,studentId,currentTime,amount,payment,blAddTime,cash);
        }

        //计算报表,判断是否破产
        gbActionBiz.isBankruptcy(studentId,contestId,currentTime);


        return R.success();
    }


    /**
     * 批量缴费
     * @param contestId
     * @param userId
     * @param currentTime
     * @return
     */
    public R batchPayFee(Integer contestId, Integer userId, Integer currentTime) {

        //阻止暂停状态下 和 破产状态下的用户进行操作
        gbActionBiz.preventAction(contestId,userId);

        Integer cash = gbCtCashflowService.getCash(userId, contestId);
        //费用管理-缴纳未缴纳的管理费用,并且扣社会责任得分
        NGbRePaymentInfo overhaul = getOverhaul(contestId, userId, currentTime);
        //管理费
        Integer glf = 0;
        if(overhaul != null){
            glf = overhaul.getAmount().intValue();
        }
        //费用管理-缴纳利息  2.费用管理-缴纳本金,
        List<NGbRePaymentInfo> bankLoan = getBankLoan(currentTime, contestId, userId);
        Integer bjlx = 0;//本金利息
        if(bankLoan!=null && bankLoan.size()>0){
            bjlx = bankLoan.stream().mapToInt(item->item.getAmount().intValue()).sum();
        }
        Integer total = glf + bjlx;

        if(total==0){
            throw new CustomException(CodeEnum.OTHER_ERROR.getCode(), "当前暂无所需缴纳的费用");
        }


        if(cash<total){
            return ErrorEnum.CASH_NOT_ENOUGH.getR();
        }

        //支付管理费
        if(overhaul != null){
            payOverhaul(contestId,userId ,currentTime);
        }
        //获取现金
        for (NGbRePaymentInfo info : bankLoan) {
            Integer cash1 = gbCtCashflowService.getCash(userId, contestId);
            payCurrSeasonAddCashFlow(contestId,userId,currentTime,info.getAmount().intValue(),info.getPayment(),info.getBlAddTime(),cash1);
        }

        //计算报表,判断是否破产
        gbActionBiz.isBankruptcy(userId,contestId,currentTime);


        return R.success();
    }



    /**
     * 显示工人列表
     */
    @Autowired
    private NGbCtGzWorkerRecruitMapper gbCtGzWorkerRecruitMapper;


    @Autowired
    private NGbCtWorkerMarketService nGbCtWorkerMarketService;


    public R getWorkerList(Integer contestId,Integer studentId,Integer currentTime){

       //先查询当前市场的所有工人
       List<NGbCtWorkerMarket> nGbCtWorkerMarkets = nGbCtWorkerMarketService.list(new LambdaQueryWrapper<NGbCtWorkerMarket>()
               .eq(NGbCtWorkerMarket::getContestId,contestId)
               .eq(NGbCtWorkerMarket::getDate,currentTime)
               .orderByAsc(NGbCtWorkerMarket::getRecruitName) //根据工人的类别进行排序
       );
       //再查询自己当前已拥有的工人
       List<NGbCtWorker> nGbCtWorkers = nGbCtWorkerMapper.selectList(new LambdaQueryWrapper<NGbCtWorker>()
                .eq(NGbCtWorker::getContestId,contestId)
                .eq(NGbCtWorker::getStudentId,studentId)
        );

       //已经发放offer的工人进行标记
       nGbCtWorkerMarkets.stream().forEach(item->{
          List<NGbCtWorker> nGbCtWorkerList = nGbCtWorkers.stream().filter(i->i.getWokerMarketId().equals(item.getId())).collect(Collectors.toList());
          if(nGbCtWorkerList!=null && nGbCtWorkerList.size()>0){
              item.setSendOffer(true);
              NGbCtWorker nGbCtWorker = nGbCtWorkerList.get(0);
              item.setOfferSal(nGbCtWorker.getInitSal());
          }else{
              item.setSendOffer(false);
          }
        });

        return R.success().put("data",nGbCtWorkerMarkets);
    }





    /**
     * 发送offer
     * @param nGbCtWorkerMarket
     * @param studentId
     * @return
     */
    public R sendOffer(NGbCtWorkerMarket nGbCtWorkerMarket,Integer studentId){

        //阻止暂停状态下 和 破产状态下的用户进行操作
        gbActionBiz.preventAction(nGbCtWorkerMarket.getContestId(),studentId);

        //先判断offer薪资是否在70%-90%之间的范围内
        boolean isWithinRange = isSalaryWithinRange(nGbCtWorkerMarket.getInitSal(),nGbCtWorkerMarket.getOfferSal());
        if(!isWithinRange){
            throw new CustomException(CodeEnum.OTHER_ERROR.getCode(), "offer薪资需在70%-100%之间");
        }

        //先判断该员工是否已经招聘
        NGbCtWorker nGbCtWorker1 = nGbCtWorkerMapper.selectOne(
                new LambdaQueryWrapper<NGbCtWorker>()
                        .eq(NGbCtWorker::getContestId,nGbCtWorkerMarket.getContestId())
                        .eq(NGbCtWorker::getStudentId,studentId)
                        .eq(NGbCtWorker::getWokerMarketId,nGbCtWorkerMarket.getId())
        );
        //如果已经发送了offer则将其删除，重新发放offer
        if(nGbCtWorker1!=null){
            Integer id = nGbCtWorker1.getId();
            nGbCtWorkerMapper.deleteById(id);
        }

        NGbCtWorker nGbCtWorker  = new NGbCtWorker();
        nGbCtWorker.setContestId(nGbCtWorkerMarket.getContestId());
        nGbCtWorker.setStudentId(studentId);
        nGbCtWorker.setWorkerName(nGbCtWorkerMarket.getWorkerName());
        nGbCtWorker.setRecruitName(nGbCtWorkerMarket.getRecruitName());
        nGbCtWorker.setRecruitNum(nGbCtWorkerMarket.getRecruitNum());
        nGbCtWorker.setInitSal(nGbCtWorkerMarket.getOfferSal()); // offer 薪资
        nGbCtWorker.setNextInitSal(nGbCtWorkerMarket.getOfferSal()); // offer 薪资
        nGbCtWorker.setMultBonus(nGbCtWorkerMarket.getMultBonus());
        nGbCtWorker.setPiece(nGbCtWorkerMarket.getPiece());
        nGbCtWorker.setStartWorkTime(DateUtils.addYearAndSeasonTime(nGbCtWorkerMarket.getDate(),1));
        nGbCtWorker.setIsWork(NGbCtWorker.ON_BUSI); //招聘中
        nGbCtWorker.setIsBackPay(2); //是否欠薪
        nGbCtWorker.setSalType(1); //工资类型-人力费
        nGbCtWorker.setWokerMarketId(nGbCtWorkerMarket.getId());

        //能否成功招聘
        //通过一个随机数判断本次录用是否成功
        boolean isHiredSuccessfully = decideHireBasedOnAttendanceRate(nGbCtWorkerMarket.getOfferSal(),nGbCtWorkerMarket.getInitSal());
        nGbCtWorker.setIsRecruit(isHiredSuccessfully?1:2); //1 能成功录用  2 无法录用

        nGbCtWorkerMapper.insert(nGbCtWorker);
        return R.success();
    }


    /**
     * 取消招聘
     * @param nGbCtWorkerMarket
     * @param userId
     * @return
     */
    public R cancelOffer(NGbCtWorkerMarket nGbCtWorkerMarket, Integer userId) {

        //阻止暂停状态下 和 破产状态下的用户进行操作
        gbActionBiz.preventAction(nGbCtWorkerMarket.getContestId(),userId);

        //先判断该员工是否已经招聘
        NGbCtWorker nGbCtWorker = nGbCtWorkerMapper.selectOne(
                new LambdaQueryWrapper<NGbCtWorker>()
                        .eq(NGbCtWorker::getContestId,nGbCtWorkerMarket.getContestId())
                        .eq(NGbCtWorker::getStudentId,userId)
                        .eq(NGbCtWorker::getWokerMarketId,nGbCtWorkerMarket.getId())
        );
        //如果已经发送了offer则将其删除
        if(nGbCtWorker!=null){
            Integer id = nGbCtWorker.getId();
            nGbCtWorkerMapper.deleteById(id);
        }else{
            throw new CustomException(CodeEnum.OTHER_ERROR.getCode(), "当前工人offer已取消");
        }
        return R.success();
    }




    public  boolean decideHireBasedOnAttendanceRate(int price, int expectedSalary) {
        // 避免整数除法，先将int转为double进行计算
        double attendanceRate = (double) price / expectedSalary;

        // 到岗率乘以100，得到一个百分比形式的阈值，用于与随机数比较
        double threshold =  (attendanceRate * 100);

        // 生成一个[0, 100)之间的随机整数
        Random random = new Random();
        double randomNumber = random.nextDouble()*100;

        // 根据阈值和随机数决定录用是否成功
        return randomNumber <= threshold;
    }


    public  boolean isSalaryWithinRange(int expectedSalary, int givenSalary) {
        double lowerBound = expectedSalary * 0.7;
        double upperBound = expectedSalary;

        return givenSalary >= lowerBound && givenSalary <= upperBound;
    }

    /**
     * 获取原料商店
     * @param contestId
     * @return
     */
    @Autowired
    private NGbCtGzMaterialMapper gbCtGzMaterialMapper;
    public R getGzMaterialList(Integer contestId, Integer studentId,Integer currentTime){
        //规则表数量
        List<NGbCtGzMaterial> gzs = gbCtGzMaterialMapper.list(contestId);
        //原料订单sum聚合
        List<NGbCtDdMaterial> dds = gbCtDdMaterialMapper.getMaterialOrderEachSum(contestId,currentTime);
        Map<Integer,NGbCtDdMaterial> ddsMap = new HashMap<>();
        for (NGbCtDdMaterial dd : dds) {
            ddsMap.put(dd.getOmCmid(),dd);
        }
        for (NGbCtGzMaterial gz : gzs) {
            if(ddsMap.containsKey(gz.getId())){
                //减去订单数量
                int i = gz.getNum() - ddsMap.get(gz.getId()).getNum();
                gz.setNum(i);
            }
        }
        return R.success().put("data",gzs);
    }

    @Autowired
    private NGbCtFeeMapper gbCtFeeMapper;
    /**
     * 原料商店确认下单
     * @param gbMaterialVo
     * @param studentId
     * @return
     */
    public R getGzMaterialShopConfirm(NGbMaterialVo gbMaterialVo , Integer studentId){

        Integer contestId = gbMaterialVo.getContestId();

        //阻止暂停状态下 和 破产状态下的用户进行操作
        gbActionBiz.preventAction(contestId,studentId);


        //1、是否限制原材料订购不得高于资产 //如是，则未付款材料+本次购买材料必须小于当前总资产
        NGbContest nGbContest = gbContestMapper.selectById(contestId);
        Integer limitMaterial = nGbContest.getLimitMaterial();
        if(limitMaterial == 1){
            Integer date = nGbContestStudentService.getStudentDate(contestId,studentId);
            Integer currentTime = date/10;
            Integer year = currentTime/10;
            //本次采购所需金额
            int thisMoney = gbMaterialVo.getNum() *  gbMaterialVo.getCmBuyFee();
            //未收货的原材料
            List<NGbCtDdMaterial> noInventoryList = gbCtDdMaterialMapper.selectList(new LambdaQueryWrapper<NGbCtDdMaterial>()
                    .eq(NGbCtDdMaterial::getContestId,contestId)
                    .eq(NGbCtDdMaterial::getStudentId,studentId)
                    .eq(NGbCtDdMaterial::getIsInventory,0) //未入库
            );
           //获取未入库的原材料总价值
            int noInventoryMoney = 0;
            for (NGbCtDdMaterial dd : noInventoryList) {
                noInventoryMoney += dd.getNum() * dd.getPrice();
            }

            //已入库未付款的原材料总价值
            List<NGbCtFee> noPayList = gbCtFeeMapper.selectList(new LambdaQueryWrapper<NGbCtFee>()
                    .eq(NGbCtFee::getContestId,contestId)
                    .eq(NGbCtFee::getStudentId,studentId)
                    .eq(NGbCtFee::getType,NGbCtFee.TO_PAYMENT)  //欠原材料款
            );
            int noPayMoney = 0;
            for (NGbCtFee fee : noPayList) {
                noPayMoney += fee.getRFee();
            }
            //总资产
            NGbCtBalance nGbCtBalance = gbCtBalanceService.getOne(new LambdaQueryWrapper<NGbCtBalance>()
                    .eq(NGbCtBalance::getContestId,contestId)
                    .eq(NGbCtBalance::getStudentId,studentId)
                    .eq(NGbCtBalance::getBsIsxt,1) //系统自动生成的
                    .eq(NGbCtBalance::getBsYear,year)
                    .last("limit 1")
            );
            int bsTotalAsset = nGbCtBalance.getBsTotalAsset();
            if(bsTotalAsset<(thisMoney+noInventoryMoney+noPayMoney)){
                throw new CustomException(CodeEnum.OTHER_ERROR.getCode(), "未付款材料+本次购买材料必须小于当前总资产");
            }
        }


        //收货期
        Integer remainDate = DateUtils.addYearAndSeasonTime(gbMaterialVo.getCurrentTime(), gbMaterialVo.getCmLeadDate());
        //新增订单
        NGbCtDdMaterial gbCtDdMaterial = new NGbCtDdMaterial();
        gbCtDdMaterial
                .setContestId(gbMaterialVo.getContestId())
                .setStudentId(studentId)
                .setOmCmid(gbMaterialVo.getCmId())
                .setCmName(gbMaterialVo.getCnName())
                .setPurchaseDate(gbMaterialVo.getCurrentTime())
                .setRemainDate(remainDate)
                .setIsInventory(0)
                .setPayDate(gbMaterialVo.getCmPayDate())
                .setIsPay(1)
                .setPrice(gbMaterialVo.getCmBuyFee())
                .setNum(gbMaterialVo.getNum());
        gbCtDdMaterialMapper.insert(gbCtDdMaterial);

        return R.success();
    }
    /**
     * 获取原料订单
     * @param contestId
     * @return
     */
    @Autowired
    private NGbCtDdMaterialMapper gbCtDdMaterialMapper;
    public R getMaterialOrderList(Integer contestId, Integer studentId){
        List<NGbCtDdMaterial> list = gbCtDdMaterialMapper.list(contestId,studentId);
        return R.success().put("data",list);
    }
    /**
     * 获取原料库存
     * @param contestId
     * @return
     */
    @Autowired
    private NGbCtKcMaterialMapper gbCtKcMaterialMapper;
    public R getMaterialInventoryList(Integer contestId, Integer studentId){
        List<NGbCtKcMaterial> list = gbCtKcMaterialMapper.listKc(contestId, studentId);
        return R.success().put("data",list);
    }

    
    /**
     * 获取产品库存 按入库时间，价格，产品分组展示
     * @param contestId
     * @return
     */
    @Autowired
    private NGbCtKcProductMapper gbCtKcProductMapper;
    public R getProductInventory(Integer contestId, Integer studentId){
        List<NGbCtKcProduct> list = gbCtKcProductMapper.listKcGroupByPriceAndInventory(contestId, studentId,NGbCtKcProduct.IS_IN);
        return R.success().put("data",list);
    }

    /**
     * 收货
     * @param id
     * @param currentTime
     * @return
     */
    public R materialToInventory(Integer id, Integer currentTime){
        NGbCtDdMaterial dd = gbCtDdMaterialMapper.selectById(id);

        //阻止暂停状态下 和 破产状态下的用户进行操作
        gbActionBiz.preventAction(dd.getContestId(),dd.getStudentId());

        if(dd.getIsInventory() == 1){
            return R.error(CodeEnum.IS_INVENTORY,"已经收货！");
        }
        if(currentTime < dd.getRemainDate()){
            return R.error(CodeEnum.NOT_REMAIN_DATE,"收货期未到！");
        }

        //收货
        inventory(id,currentTime);

        //计算报表,判断是否破产
        gbActionBiz.isBankruptcy(dd.getStudentId(),dd.getContestId(),currentTime);

        return R.success();
    }


    /**
     * 原材料收货
     * @param id
     * @param currentTime
     */
    public void inventory(Integer id, Integer currentTime){
        NGbCtDdMaterial dd = gbCtDdMaterialMapper.selectById(id);
        //更新订单入库状态/付款账期
        dd.setIsInventory(NGbCtDdMaterial.IS_INVENTORY);
        gbCtDdMaterialMapper.updateById(dd);
        //付款账期
        int remainDate = dd.getRemainDate();
        int paymentDate = DateUtils.addYearAndSeasonTime(currentTime,dd.getPayDate());
        int rFee = dd.getNum() * dd.getPrice();
        NGbCtFee fee = new NGbCtFee();
        fee.setStudentId(dd.getStudentId())
                .setContestId(dd.getContestId())
                .setBorrower("1")
                .setLender("供应商")
                .setRFee(rFee)
                .setRemarks("原材料"+dd.getCmName()+"付款")
                .setRRemainDate(remainDate)
                .setPaymentDate(paymentDate)
                .setType(NGbCtFee.TO_PAYMENT);
        gbCtFeeMapper.insert(fee);
        //产生入库库存
        NGbCtKcMaterial kc = new NGbCtKcMaterial();
        kc.setStudentId(dd.getStudentId())//学生id
                .setContestId(dd.getContestId())//竞赛id
                .setImCmId(dd.getOmCmid())//原料商店产品id
                .setImNum(dd.getNum())//数量
                .setMaterialName(dd.getCmName())//原料名称
                .setInInventoryDate(currentTime)//入库时间
                .setMaterialPrice(dd.getPrice());//价格
        gbCtKcMaterialMapper.insert(kc);

    }




    /**
     *获取在职工人信息
     * @param contestId
     * @param studentId
     * @return
     */
    public R getWorkerOnTheJobList(Integer contestId,Integer studentId ){
        List<NGbCtWorker> workerList = nGbCtWorkerMapper.selectList(new LambdaQueryWrapper<NGbCtWorker>()
                .eq(NGbCtWorker::getContestId,contestId)
                .eq(NGbCtWorker::getStudentId,studentId)
                .ne(NGbCtWorker::getIsWork,1) //还未入职剔除掉
                .ne(NGbCtWorker::getIsWork,5) //剔除已开除的员工
        );

        if(workerList.size()>0){
            workerList.forEach(item->{
                //如果员工在产线中，还需要判断产线状态,产线正在生产中的情况下，员工状态更新为生产中
                if(NGbCtWorker.ON_WORK.equals(item.getIsWork())){
                   Integer lineId = item.getLineId();
                   NGbCtLine nGbCtLine = gbCtLineMapper.selectById(lineId);
                   String lineStatus =  nGbCtLine.getStatus();
                   //如果产线状态为生产中
                   if(NGbCtLine.ON_PRODUCE.equals(lineStatus)){
                       item.setIsWork(NGbCtWorker.ON_PRODUCE);
                   }
                }
            });
        }

        return R.success().put("data",workerList);
    }

    /**
     *获取班次规则
     * @param contestId
     * @param studentId
     * @return
     */
    @Autowired
    private NGbCtGzClassesMapper gbCtGzClassesMapper;
    public R getClassList(Integer contestId){
        List<NGbCtGzClasses> classes = gbCtGzClassesMapper.getClassesByContestId(contestId);
        return R.success().put("data",classes);
    }


    @Autowired
    private NGbCtLineMapper gbCtLineMapper;
    public R getLineGroupInfo(Integer studentId,Integer contestId,Integer currentTime){
        List<NGbCtLine> listLine = gbCtLineMapper.getLineListOrderByFinishTime(studentId, contestId, currentTime);
        return R.success().put("list",listLine);
    }

    public R getAllWorker(Integer contestId, Integer userId) {
        List<NGbCtWorker> workerList = nGbCtWorkerMapper.selectList(new LambdaQueryWrapper<NGbCtWorker>()
                .eq(NGbCtWorker::getContestId,contestId)
                .eq(NGbCtWorker::getStudentId,userId)
        );
        return R.success().put("data",workerList);
    }


    @Autowired
    private NGbContestService nGbContestService;

    @Autowired
    private NGbCtGzWorkerTrainService nGbCtGzWorkerTrainService;

    @Autowired
    NGbCtCashflowService nGbCtCashflowService;


    public R workerTrain(List<NGbCtWorker> nGbCtWorkerList, Integer userId,Integer currentTime) {
        Integer contestId  = nGbCtWorkerList.get(0).getContestId();

        //阻止暂停状态下 和 破产状态下的用户进行操作
        gbActionBiz.preventAction(contestId,userId);

        List<Integer> ids = nGbCtWorkerList.stream().map(item->item.getId()).collect(Collectors.toList());
        //判断当前是否有人当前正在培训
        List<NGbCtWorker> nGbCtWorkers = nGbCtWorkerMapper.selectList(new LambdaQueryWrapper<NGbCtWorker>()
                .eq(NGbCtWorker::getContestId,contestId)
                .eq(NGbCtWorker::getStudentId,userId)
                .eq(NGbCtWorker::getIsWork,4)  //培训中
        );
        if(nGbCtWorkers!=null && nGbCtWorkers.size()>0){
            throw new CustomException(CodeEnum.OTHER_ERROR.getCode(), "当前已有培训任务进行中");
        }
        //判断这一批参与培训的员工是否忙碌
        List<NGbCtWorker> nGbCtWorkers1 = nGbCtWorkerMapper.selectList(new LambdaQueryWrapper<NGbCtWorker>()
                .eq(NGbCtWorker::getContestId,contestId)
                .eq(NGbCtWorker::getStudentId,userId)
                .ne(NGbCtWorker::getIsWork,2)  //不是空闲的状态
                .in(NGbCtWorker::getId,ids)
        );
        if(nGbCtWorkers1!=null && nGbCtWorkers1.size()>0){
            throw new CustomException(CodeEnum.OTHER_ERROR.getCode(), "当前参与培训的员工繁忙,请重新刷新列表数据");
        }

        //获取培训的规则
        NGbContest nGbContest = nGbContestService.getById(contestId);

        NGbCtGzWorkerTrain nGbCtGzWorkerTrain  = nGbCtGzWorkerTrainService.getOne(new LambdaQueryWrapper<NGbCtGzWorkerTrain>()
                .eq(NGbCtGzWorkerTrain::getSubjectNum,nGbContest.getSubjectNumber())
                .last(" limit 1")
        );
        if(nGbCtGzWorkerTrain==null){
            throw new CustomException(CodeEnum.OTHER_ERROR.getCode(), "工人培训规则不能为空");
        }

        //修改工人的培训状态
        int i = nGbCtWorkerMapper.update(new NGbCtWorker(),new UpdateWrapper<NGbCtWorker>()
                .lambda()
                .eq(NGbCtWorker::getContestId,contestId)
                .eq(NGbCtWorker::getStudentId,userId)
                .eq(NGbCtWorker::getIsWork,2)  //是空闲的状态
                .in(NGbCtWorker::getId,ids)
                .set(NGbCtWorker::getIsWork,4) //设置工作状态 - 培训中
                //设置培训结束时间
                .set(NGbCtWorker::getTrainEndTime,DateUtils.addYearAndSeasonTime(currentTime,nGbCtGzWorkerTrain.getTimeCostQuarter()))
        );

        //判断现金是否足够
        //获取当前的现金余额
        int cash = nGbCtCashflowService.getCash(userId, contestId);
        // 获取消耗现金的金额
        int cost = i * nGbCtGzWorkerTrain.getCashCost();
        int remain = cash - cost;
        if (remain < 0) {
            throw new CustomException(ErrorEnum.CASH_NOT_ENOUGH.getCode(), ErrorEnum.CASH_NOT_ENOUGH.getMsg());
        }

        //记录现金流量表
        NGbCtCashflow ctCashflow = new NGbCtCashflow();
        ctCashflow.setCDate(currentTime)
                .setCSurplus(remain)
                .setCIn(0)
                .setCOut(cost)
                .setCAction(CashFlowActionEnum.GRPX.getAction())
                .setContestId(contestId)
                .setStudentId(userId)
                .setCComment(i+"名工人参加培训" + cost + "元");
        nGbCtCashflowService.save(ctCashflow);

        //重新计算报表
        //计算报表,判断是否破产
        gbActionBiz.isBankruptcy(userId,contestId,currentTime);

        return R.success();
    }


    @Autowired
    private NGbContestStudentService nGbContestStudentService;

    /**
     * 解雇工人
     * @param nGbCtWorker
     * @param userId
     * @return
     */
    public R workerDismiss(NGbCtWorker nGbCtWorker, Integer userId) {
        Integer contestId = nGbCtWorker.getContestId();

        //阻止暂停状态下 和 破产状态下的用户进行操作
        gbActionBiz.preventAction(contestId,userId);

        Integer date = nGbContestStudentService.getStudentDate(contestId,userId);
        Integer currentTime = date/10;

        //判断当前工人的状态 - 从数据库里面重新查询一下
        nGbCtWorker = nGbCtWorkerMapper.selectById(nGbCtWorker.getId());
        if(!nGbCtWorker.getIsWork().equals(2)){
            throw new CustomException(CodeEnum.OTHER_ERROR.getCode(), "当前员工繁忙不允许解聘");
        }

        if(nGbCtWorker.getIsBackPay()==1){
            throw new CustomException(CodeEnum.OTHER_ERROR.getCode(), "当前员工工资未结清，请先结清工资");
        }

        //计算解聘需要赔付的金额
        //入职时间
        Integer startWorkTime = nGbCtWorker.getStartWorkTime();
        //计算时间差
        Integer sjc = DateUtils.calSeasonInterval(startWorkTime,currentTime);
        //计算赔偿款  赔偿 n+1,不足一年按一年算
        Integer pck =   nGbCtWorker.getNextInitSal() * ((int)Math.ceil(sjc/4)+1);

        //判断现金是否足够
        //获取当前的现金余额
        int cash = nGbCtCashflowService.getCash(userId, contestId);
        // 获取消耗现金的金额
        int remain = cash - pck;
        if (remain < 0) {
            throw new CustomException(ErrorEnum.CASH_NOT_ENOUGH.getCode(), ErrorEnum.CASH_NOT_ENOUGH.getMsg());
        }
        //记录现金流量表
        NGbCtCashflow ctCashflow = new NGbCtCashflow();
        ctCashflow.setCDate(currentTime)
                .setCSurplus(remain)
                .setCIn(0)
                .setCOut(pck)
                .setCAction(CashFlowActionEnum.GRPCK.getAction())
                .setContestId(contestId)
                .setStudentId(userId)
                .setCComment("开除员工"+nGbCtWorker.getWorkerName()+"赔偿"+ pck + "元");
        nGbCtCashflowService.save(ctCashflow);

        //删除该员工
        nGbCtWorker.setIsWork(5); //开除状态
        nGbCtWorkerMapper.updateById(nGbCtWorker);

        //判断当前员工是否在产线上,如果在产线上,还需要从产线中剔除，修改产线的实际产量
        if(!StringUtils.isEmpty(nGbCtWorker.getLineId())){
            nGbLineBiz.reCalRealProduction(nGbCtWorker.getLineId()); //重新计算产量
        }

        //重新计算报表
        //计算报表,判断是否破产
        gbActionBiz.isBankruptcy(userId,contestId,currentTime);

        return R.success();
    }


    /**
     * 工人发薪
     * @param nGbCtWorker
     * @param userId
     * @return
     */
    public R workerPay(NGbCtWorker nGbCtWorker, Integer userId) {
        Integer contestId = nGbCtWorker.getContestId();

        //阻止暂停状态下 和 破产状态下的用户进行操作
        gbActionBiz.preventAction(contestId,userId);

        Integer date = nGbContestStudentService.getStudentDate(contestId,userId);
        Integer currentTime = date/10;

        //判断当前工人的状态 - 从数据库里面重新查询一下
        nGbCtWorker = nGbCtWorkerMapper.selectById(nGbCtWorker.getId());

        //判断该员工工资是否已结清
        if(nGbCtWorker.getIsBackPay()!=1){
            throw new CustomException(CodeEnum.OTHER_ERROR.getCode(), "该员工上季度工资已结清，无需重复操作");
        }

        if(nGbCtWorker.getIsWork()==5){
            throw new CustomException(CodeEnum.OTHER_ERROR.getCode(), "当前员工已被开除");
        }

        int i = paySal(currentTime,contestId,userId,nGbCtWorker,false,false);

        //更新条数为0,则说明该员工欠薪状态未修改，抛出异常
        if(i==0){
            throw new CustomException(CodeEnum.OTHER_ERROR.getCode(), "该员工上季度工资已结清，无需重复操作");
        }

        //重新计算报表
        //计算报表,判断是否破产
        gbActionBiz.isBankruptcy(userId,contestId,currentTime);


        return R.success();
    }




    /**
     * 一键发薪
     * @param contestId
     * @param userId
     * @param currentTime
     * @return
     */
    public R workerPayBatch(Integer contestId, Integer userId, Integer currentTime) {

        //阻止暂停状态下 和 破产状态下的用户进行操作
        gbActionBiz.preventAction(contestId,userId);

        //查询是否有欠薪的工人
        List<NGbCtWorker> qxList = nGbCtWorkerMapper.selectList(new LambdaQueryWrapper<NGbCtWorker>()
                .eq(NGbCtWorker::getContestId,contestId)
                .eq(NGbCtWorker::getStudentId,userId)
                .eq(NGbCtWorker::getIsBackPay,1)
        );
        if(qxList!=null && qxList.size()>0){
            //给每一个工人发工资
            qxList.forEach(item->{
                paySal(currentTime,contestId,userId,item,false,false);
            });
        }
        //重新计算报表
        //计算报表,判断是否破产
        gbActionBiz.isBankruptcy(userId,contestId,currentTime);
        return R.success();
    }




    /**
     * 发工资
     */
    public int paySal(Integer currentTime,Integer contestId,Integer userId,NGbCtWorker nGbCtWorker,boolean  skipCashCheck,boolean isAuto){
        Integer initSal = nGbCtWorker.getInitSal();
        Integer initSal3 = initSal*3;

        int cash = nGbCtCashflowService.getCash(userId, contestId);
        // 获取消耗现金的金额
        int remain = cash - initSal3;
        //如果跳过校验则补检查金额是不是足够
        if(!skipCashCheck){
            if(remain<0){
                throw new CustomException(CodeEnum.OTHER_ERROR.getCode(), "现金不足");
            }
        }
        NGbCtCashflow ctCashflow = new NGbCtCashflow();


        String action = null;
        //判断费用类型
        ///人力费
        if(nGbCtWorker.getSalType()==1){
            action = CashFlowActionEnum.RLF.getAction();
        }
        //工人工资
        else{
            action = CashFlowActionEnum.GRGZ.getAction();
        }

        ctCashflow.setCDate(currentTime)
                .setCSurplus(remain)
                .setCIn(0)
                .setCOut(initSal3)
                .setCAction(action)  //类型可能会有人力费 和 工人工资两种类型
                .setContestId(contestId)
                .setStudentId(userId)
                .setCComment("付工人"+nGbCtWorker.getWorkerName()+"工资"+ initSal3 + "元");
        nGbCtCashflowService.save(ctCashflow);

        LambdaUpdateWrapper<NGbCtWorker> updateWrapper = new UpdateWrapper<NGbCtWorker>()
                .lambda()
                .eq(NGbCtWorker::getId,nGbCtWorker.getId())
                .eq(NGbCtWorker::getIsBackPay,1) //是否欠薪资
                .set(NGbCtWorker::getIsBackPay,2);

        //如果是自动扣款工人的效率会减半
        if(isAuto){
            Integer multBonus = nGbCtWorker.getMultBonus();
            BigDecimal result = new BigDecimal(multBonus).divide(new BigDecimal(2),0,RoundingMode.HALF_UP);
            updateWrapper.set(NGbCtWorker::getMultBonus,result.intValue());
        }
        int i = nGbCtWorkerMapper.update(new NGbCtWorker(),updateWrapper);
        if(i==0){
            throw new CustomException(CodeEnum.OTHER_ERROR.getCode(), "已付款,请勿重复支付");
        }
        return i;
    }

    @Autowired
    private NGbCtGzWorkerIncentiveService nGbCtGzWorkerIncentiveService;

    /**
     * 激励员工
     * @param nGbWorkerIncentiveParam
     * @param userId
     * @return
     */
    public R workerIncentive(NGbWorkerIncentiveParam nGbWorkerIncentiveParam, Integer userId) {

        Integer contestId  = nGbWorkerIncentiveParam.getContestId();
        //阻止暂停状态下 和 破产状态下的用户进行操作
        gbActionBiz.preventAction(contestId,userId);

        NGbContest nGbContest = nGbContestService.getById(contestId);
        Integer subjectNumber  = nGbContest.getSubjectNumber();
        Integer currentTime = nGbWorkerIncentiveParam.getCurrentTime();
        String type = nGbWorkerIncentiveParam.getType();
        NGbCtWorker nGbCtWorker = nGbWorkerIncentiveParam.getNGbCtWorker();
        Integer amount = nGbWorkerIncentiveParam.getAmount();

        //从数据库查询最新的工人数据
        nGbCtWorker = nGbCtWorkerMapper.selectById(nGbCtWorker.getId());
        Integer isWork = nGbCtWorker.getIsWork();
        if(NGbCtWorker.ON_DISMISS.equals(isWork)){
            throw new CustomException(CodeEnum.OTHER_ERROR.getCode(), "当前员工已解雇");
        }
        if(NGbCtWorker.ON_TRAIN.equals(isWork)){
            throw new CustomException(CodeEnum.OTHER_ERROR.getCode(), "当前员工正在培训中");
        }
        NGbCtLine nGbCtLine = null;
        if(NGbCtWorker.ON_WORK.equals(isWork)){
            Integer lineId = nGbCtWorker.getLineId();
            nGbCtLine = gbCtLineMapper.selectById(lineId);
            String lineStatus =  nGbCtLine.getStatus();
            //如果产线状态为生产中
            if(NGbCtLine.ON_PRODUCE.equals(lineStatus)){
                throw new CustomException(CodeEnum.OTHER_ERROR.getCode(), "当前员工正在生产过程中");
            }
        }


        //先判断类型
        //激励 - 直接扣除现金。并提升员工的效率
        if("JL1".equals(type)){
            //判断现金是否足够
            //获取当前的现金余额
            int cash = nGbCtCashflowService.getCash(userId, contestId);
            // 获取消耗现金的金额
            int remain = cash - amount;
            if (remain < 0) {
                throw new CustomException(ErrorEnum.CASH_NOT_ENOUGH.getCode(), ErrorEnum.CASH_NOT_ENOUGH.getMsg());
            }
            //记录现金流量表
            NGbCtCashflow ctCashflow = new NGbCtCashflow();
            ctCashflow.setCDate(currentTime)
                    .setCSurplus(remain)
                    .setCIn(0)
                    .setCOut(amount)
                    .setCAction(CashFlowActionEnum.GRJL.getAction())
                    .setContestId(contestId)
                    .setStudentId(userId)
                    .setCComment("给"+nGbCtWorker.getWorkerName()+"发放奖金"+ amount + "元");
            nGbCtCashflowService.save(ctCashflow);

            //提升员工效率
            NGbCtGzWorkerIncentive nGbCtGzWorkerIncentive = nGbCtGzWorkerIncentiveService.getOne(
                    new LambdaQueryWrapper<NGbCtGzWorkerIncentive>()
                    .eq(NGbCtGzWorkerIncentive::getSubjectNum,subjectNumber)
                    .eq(NGbCtGzWorkerIncentive::getIncentiveNum,"JL1") //激励
            );
            BigDecimal efficiencyPercent = nGbCtGzWorkerIncentive.getEfficiencyPercent(); //效率提升
             // 提升的效率 =提升工资或发放奖金/ (10000/提升效率比例)
            BigDecimal tsxl = new BigDecimal(amount).multiply(efficiencyPercent).divide(new BigDecimal(10000),0, RoundingMode.DOWN);
            BigDecimal newMultBonus = new BigDecimal(nGbCtWorker.getMultBonus()).add(tsxl);

            //效率最大不能超过100
            int multBonus = Math.min(newMultBonus.intValue(),100);
            nGbCtWorker.setMultBonus(multBonus);
            nGbCtWorkerMapper.updateById(nGbCtWorker);
        }
        //涨薪 - 提升员工下个月的薪酬,本月不花钱
        else if("JL2".equals(type)){

            Integer nextInitSal = nGbCtWorker.getNextInitSal()+amount;
            if(nextInitSal>1000000 || nextInitSal<=0){
                throw new CustomException(CodeEnum.OTHER_ERROR.getCode(), "涨薪金额异常，请输入合理的数值");
            }

            //提升员工效率
            NGbCtGzWorkerIncentive nGbCtGzWorkerIncentive = nGbCtGzWorkerIncentiveService.getOne(
                    new LambdaQueryWrapper<NGbCtGzWorkerIncentive>()
                            .eq(NGbCtGzWorkerIncentive::getSubjectNum,subjectNumber)
                            .eq(NGbCtGzWorkerIncentive::getIncentiveNum,"JL2") //激励
            );
            BigDecimal efficiencyPercent = nGbCtGzWorkerIncentive.getEfficiencyPercent(); //效率提升
            // 提升的效率 =提升工资或发放奖金/ (10000/提升效率比例)
            BigDecimal tsxl = new BigDecimal(amount).multiply(efficiencyPercent).divide(new BigDecimal(10000),0, RoundingMode.DOWN);
            BigDecimal newMultBonus = new BigDecimal(nGbCtWorker.getMultBonus()).add(tsxl);

            //效率立马提升 最大值不能超过100
            int multBonus = Math.min(newMultBonus.intValue(),100);
            nGbCtWorker.setMultBonus(multBonus);
            //下个月的基本工资提高
            nGbCtWorker.setNextInitSal(nextInitSal);
            nGbCtWorkerMapper.updateById(nGbCtWorker);
        }


        //如果激励的员工在产线中，还需要重新更新并计算产线的产量
        if(nGbCtLine!=null){
            nGbLineBiz.reCalRealProduction(nGbCtLine.getLineId());
        }

        //重新计算报表
        //计算报表,判断是否破产
        gbActionBiz.isBankruptcy(userId,contestId,currentTime);

        return R.success();
    }





    /**
     * 一键激励员工
     * @param nGbWorkerIncentiveParam
     * @param userId
     * @return
     */
    public R incentiveBatch(NGbWorkerIncentiveParam nGbWorkerIncentiveParam, Integer userId) {
        Integer contestId  = nGbWorkerIncentiveParam.getContestId();

        //阻止暂停状态下 和 破产状态下的用户进行操作
        gbActionBiz.preventAction(contestId,userId);

        Integer currentTime = nGbWorkerIncentiveParam.getCurrentTime();  //时间
        //工人类型
        String  recruitNum  = nGbWorkerIncentiveParam.getRecruitNum(); //工人类型
        Integer multBonus  = nGbWorkerIncentiveParam.getMultBonus();  //需要提升到的效率

        if(multBonus>100){
            throw new CustomException(CodeEnum.OTHER_ERROR.getCode(), "效率不能超过100");
        }

         List<NGbCtWorker> workerList = new ArrayList<>();
         //计算所需的金额，并获取工人
         int needMoney = calNeedMoney(contestId,userId,recruitNum,multBonus,workerList);

        //判断现金是否足够
        //获取当前的现金余额
        int cash = nGbCtCashflowService.getCash(userId, contestId);
        // 获取消耗现金的金额
        int remain = cash - needMoney;
        if (remain < 0) {
            throw new CustomException(ErrorEnum.CASH_NOT_ENOUGH.getCode(), ErrorEnum.CASH_NOT_ENOUGH.getMsg());
        }
        //记录现金流量表
        NGbCtCashflow ctCashflow = new NGbCtCashflow();
        ctCashflow.setCDate(currentTime)
                .setCSurplus(remain)
                .setCIn(0)
                .setCOut(needMoney)
                .setCAction(CashFlowActionEnum.GRJL.getAction())
                .setContestId(contestId)
                .setStudentId(userId)
                .setCComment("批量给"+recruitNum+"工人发放奖金"+ needMoney + "元，提升效率至"+multBonus);
        nGbCtCashflowService.save(ctCashflow);


        //更新工人的效率
        for (NGbCtWorker nGbCtWorker : workerList) {
            nGbCtWorkerMapper.updateById(nGbCtWorker);
        }


        //如果工人在工厂，还需要重新计算工厂的效率
        Set<Integer> lineIdSet = workerList.stream().filter(item->NGbCtWorker.ON_WORK.equals(item.getIsWork()))
                .map(NGbCtWorker::getLineId).collect(Collectors.toSet());

        lineIdSet.forEach(lineId->{
            nGbLineBiz.reCalRealProduction(lineId);
        });


        //重新计算报表
        //计算报表,判断是否破产
        gbActionBiz.isBankruptcy(userId,contestId,currentTime);
        return R.success();
    }


    public int calNeedMoney(Integer contestId,Integer userId,String recruitNum,Integer multBonus,List<NGbCtWorker> workerList){

        NGbContest nGbContest = nGbContestService.getById(contestId);
        Integer subjectNumber  = nGbContest.getSubjectNumber();
        //先查询所有的员工
        List<NGbCtWorker> workers = nGbCtWorkerMapper.selectList(new LambdaQueryWrapper<NGbCtWorker>()
                .eq(NGbCtWorker::getContestId,contestId)
                .eq(NGbCtWorker::getStudentId,userId)
                .ne(NGbCtWorker::getIsWork,1) //还未入职剔除掉
                .ne(NGbCtWorker::getIsWork,5) //剔除已开除的员工
                .ne(NGbCtWorker::getIsWork,4) //剔除在培训中的员工
                .eq(NGbCtWorker::getRecruitNum,recruitNum) //员工类型
                .lt(NGbCtWorker::getMultBonus,multBonus) //效率小于所要求的的效率才能够进行提升
        );

        workerList.addAll(workers); //这一步的目的是为了把workerList传递出去

        workerList = workerList.stream().filter(item->{
            //如果员工在产线中，还需要判断产线状态,产线正在生产中的情况下员工不能进行效率提升
            if(NGbCtWorker.ON_WORK.equals(item.getIsWork())){
                Integer lineId = item.getLineId();
                NGbCtLine nGbCtLine = gbCtLineMapper.selectById(lineId);
                String lineStatus =  nGbCtLine.getStatus();
                //如果产线状态为生产中
                if(NGbCtLine.ON_PRODUCE.equals(lineStatus)){
                    return false;
                }else{
                    return true;
                }
            }
            else{
                return true;
            }
        }).collect(Collectors.toList());

        //提升员工效率
        NGbCtGzWorkerIncentive nGbCtGzWorkerIncentive = nGbCtGzWorkerIncentiveService.getOne(
                new LambdaQueryWrapper<NGbCtGzWorkerIncentive>()
                        .eq(NGbCtGzWorkerIncentive::getSubjectNum,subjectNumber)
                        .eq(NGbCtGzWorkerIncentive::getIncentiveNum,"JL1") //激励
        );
        BigDecimal efficiencyPercent = nGbCtGzWorkerIncentive.getEfficiencyPercent(); //效率提升

        Integer needMoney = 0;

        //计算效率提升所需的金额
        for (NGbCtWorker nGbCtWorker : workerList) {
            Integer oldMultBonus = nGbCtWorker.getMultBonus();
            //需要提升的效率
            int xlts = multBonus - oldMultBonus;
            //计算需要花费多少钱  需要提升的效率 * (10000/提升效率比例)
            BigDecimal costMoneyBigcimal = new BigDecimal(xlts).multiply(new BigDecimal(10000)).divide(efficiencyPercent,0,RoundingMode.UP);
            int costMoney = costMoneyBigcimal.intValue();
            needMoney+=costMoney;
            nGbCtWorker.setMultBonus(multBonus);
        }
        return needMoney;
    }


    public R incentiveBatchMoney(NGbWorkerIncentiveParam nGbWorkerIncentiveParam, Integer userId) {
        Integer contestId  = nGbWorkerIncentiveParam.getContestId();
        Integer currentTime = nGbWorkerIncentiveParam.getCurrentTime();  //时间
        //工人类型
        String  recruitNum  = nGbWorkerIncentiveParam.getRecruitNum(); //工人类型
        Integer multBonus  = nGbWorkerIncentiveParam.getMultBonus();  //需要提升到的效率

        if(multBonus>100){
            throw new CustomException(CodeEnum.OTHER_ERROR.getCode(), "效率不能超过100");
        }

        List<NGbCtWorker> workerList = new ArrayList<>();
        //计算所需的金额，并获取工人
        int needMoney = calNeedMoney(contestId,userId,recruitNum,multBonus,workerList);

        return R.success().put("data",needMoney);
    }
}
