package com.xiyou.main.biz.ngbcontest;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.xiyou.common.enums.CodeEnum;
import com.xiyou.common.utils.R;
import com.xiyou.main.dao.ngbcontest.*;
import com.xiyou.main.entity.ngbcontest.*;
import com.xiyou.main.service.ngbcontest.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @program: multi-module
 * @description:
 * @author: wangxingyu
 * @create: 2023-06-11
 **/
@Service
@Transactional(rollbackFor = Exception.class)
@Slf4j
public class NGbBusinessBiz {

    @Autowired
    private NGbActionBiz gbActionBiz;

    @Autowired
    private NGbContestService gbContestService;
    @Autowired
    private NGbContestStudentService gbContestStudentService;
    @Autowired
    private NGbContestStudentMapper gbContestStudentMapper;
    @Autowired
    private NGbCtSubjectMapper gbCtSubjectMapper;

    @Autowired
    private NGbCtGzIsoMapper gbCtGzIsoMapper;
    @Autowired
    private NGbCtGzProductMapper gbCtGzProductMapper;
    @Autowired
    private NGbCtYfProductMapper gbCtYfProductMapper;
    @Autowired
    private NGbCtGzMarketMapper gbCtGzMarketMapper;
    @Autowired
    private NGbCtGzOrderMapper gbCtGzOrderMapper;
    @Autowired
    private NGbCtGzNumMapper gbCtGzNumMapper;
    @Autowired
    private NGbCtYfNumMapper gbCtYfNumMapper;
    @Autowired
    private NGbCtCorporateMapper gbCtCorporateMapper;
    @Autowired
    private NGbCtBalanceService gbCtBalanceService;
    @Autowired
    private NGbCtCashflowService gbCtCashflowService;
    @Autowired
    @Lazy
    private NGbRestoreDataBiz gbRestoreDataBiz;
    @Autowired
    private NGbCtYfIsoMapper gbCtYfIsoMapper;
    @Autowired
    private NGbCtYfMarketMapper gbCtYfMarketMapper;

    @Autowired
    private NGbCtCarbonMapper nGbCtCarbonMapper;

    @Autowired
    private NGbCtGzProductDesignMapper nGbCtGzProductDesignMapper;

    @Autowired
    private NGbCtDesignProgressService nGbCtDesignProgressService;

    @Autowired
    private NGbCtMemberIndexMapper nGbCtMemberIndexMapper;


    public R start(Integer studentId, Integer contestId) {

        NGbContestStudent gbContestStudent = gbContestStudentService.get(contestId, studentId);
        if (gbContestStudent == null) {
            return R.error(CodeEnum.OTHER_ERROR.getCode(), "竞赛不存在");
        }
        if (gbContestStudent.getStart() == 1) {
            return R.error(CodeEnum.OTHER_ERROR.getCode(), "已开始经营");
        }
        if (gbContestStudent.getFinishTime() != null) {
            return R.error(CodeEnum.OTHER_ERROR.getCode(), "已完成该竞赛模拟");
        }
        // 初始权益作为初始资金
        NGbContest gbContest = gbContestService.getById(contestId);

        Integer cash = gbContest == null ? 0 : gbContest.getEquity();

        // balance新增一条记录
        // 学生ID、考试ID、1、0、规则表该套题中的cash、0、0、0、0、规则表该套题中的cash、0、0、0、0、规则表该套题中的cash、0、0、0、0、规则表该套题中的cash、0、0、规则表该套题中的cash、规则表该套题中的cash
        NGbCtBalance gbCtBalance = new NGbCtBalance()
                .setStudentId(studentId)
                .setContestId(contestId)
                .setBsIsxt(1)
                .setBsYear(0)
                .setBsCash(cash)
                .setBsReceivable(0)
                .setBsProductInProcess(0)
                .setBsProduct(0)
                .setBsMaterial(0)
                .setBsTotalCurrentAsset(cash)
                .setBsEquipment(0)
                .setBsProjectOnConstruction(0)
                .setBsTotalFixedAsset(0)
                .setBsTotalAsset(cash)
                .setBsLongLoan(0)
                .setBsShortLoan(0)
                .setBsOtherPay(0)
                .setBsTax(0)
                .setBsTotalLiability(0)
                .setBsEquity(cash)
                .setBsRetainedEarning(0)
                .setBsAnnualNetProfit(0)
                .setBsTotalEquity(cash)
                .setBsTotal(cash)
                .setIsSubmit("Y");
        gbCtBalanceService.save(gbCtBalance);

        // Cashflow新增一条记录，具体为：
        // 学生ID、考试ID、顺序号、开始经营、规则表该套题中的cash、0、规则表该套题中的cash、公司成立、10
        NGbCtCashflow cashflow = new NGbCtCashflow()
                .setStudentId(studentId)
                .setContestId(contestId)
                .setCAction("开始经营")
                .setCIn(cash)
                .setCOut(0)
                .setCSurplus(cash)
                .setCComment("公司成立")
                .setCDate(10);
        gbCtCashflowService.save(cashflow);

        // ISO研发情况新增记录，具体为：
        // 该套规则中含有的所有ISO编号各生成一条记录，ISO编号=ISO编号，总研发时间=研发周期，已研发时间=0，剩余研发时间=总研发周期，完成时间 is null
        List<NGbCtGzIso> ctGzIsoList = gbCtGzIsoMapper.list(contestId);
        List<NGbCtYfIso> ctYfIsoList = new ArrayList<>();
        for (NGbCtGzIso gbCtGzIso : ctGzIsoList) {
            ctYfIsoList.add(new NGbCtYfIso()
                    .setStudentId(studentId)
                    .setContestId(contestId)
                    .setDiCiid(gbCtGzIso.getId())
                    .setDiTotalDate(gbCtGzIso.getCiDevelopDate())
                    .setDiNowDate(0)
                    .setDiRemainDate(gbCtGzIso.getCiDevelopDate())
                    .setDiFinishDate(null)
                    .setDiState(NGbCtYfIso.ISO_YF_NONE)
            )
            ;
        }
        if (ctYfIsoList.size() > 0) {
            gbCtYfIsoMapper.insertBatch(ctYfIsoList);
        }
        // 市场研发、产品研发同ISO
        List<NGbCtGzMarket> ctGzMarketList = gbCtGzMarketMapper.list(contestId);
        List<NGbCtYfMarket> ctYfMarketList = new ArrayList<>();
        for (NGbCtGzMarket ctGzMarket : ctGzMarketList) {
            ctYfMarketList.add(new NGbCtYfMarket()
                    .setStudentId(studentId)
                    .setContestId(contestId)
                    .setDmCmId(ctGzMarket.getId())
                    .setDmTotalDate(ctGzMarket.getCmDevelopDate())
                    .setDmNowDate(0)
                    .setDmRemainDate(ctGzMarket.getCmDevelopDate())
                    .setDmFinishDate(null)
                    .setDmState(NGbCtYfMarket.SC_YF_NONE)
            );
        }
        if (ctYfMarketList.size() > 0) {
            gbCtYfMarketMapper.insertBatch(ctYfMarketList);
        }

        List<NGbCtGzProduct> ctGzProductList = gbCtGzProductMapper.getList(contestId);
        List<NGbCtYfProduct> ctYfProductList = new ArrayList<>();
        for (NGbCtGzProduct ctGzProduct : ctGzProductList) {
            ctYfProductList.add(new NGbCtYfProduct()
                    .setStudentId(studentId)
                    .setContestId(contestId)
                    .setDpCpId(ctGzProduct.getId())
                    .setDpTotalDate(ctGzProduct.getCpDevelopDate())
                    .setDpNowDate(0)
                    .setDpRemainDate(ctGzProduct.getCpDevelopDate())
                    .setDpFinishDate(null)
                    .setDpState(NGbCtYfProduct.CP_YF_NONE)
            );
        }
        if (ctYfProductList.size() > 0) {
            gbCtYfProductMapper.insertBatch(ctYfProductList);
        }


        //数字化研发  如果是本科需要花钱研发,如果是高职直接研发成功
        List<NGbCtGzNum> gbCtGzNums = gbCtGzNumMapper.getList(contestId);
        for (NGbCtGzNum gbCtGzNum : gbCtGzNums) {
            NGbCtYfNum gbCtYfNum = new NGbCtYfNum();
            gbCtYfNum.setStudentId(studentId)
                    .setContestId(contestId)
                    .setPostName(gbCtGzNum.getPostName())
                    .setPostNum(gbCtGzNum.getPostNum());
            //高职
            if(gbContest.getContestType()==0){
                gbCtYfNum.setConsumeMoney(0)
                        .setTimeCostQuarter(0)
                        .setFinishDate(31) //3年一季度完成研发工作
                        .setState(NGbCtYfNum.NUM_YF_ING);
            }
            //本科
            else{
                gbCtYfNum.setConsumeMoney(gbCtGzNum.getConsumeMoney())
                        .setTimeCostQuarter(gbCtGzNum.getTimeCostQuarter())
                        .setState(NGbCtYfNum.NUM_YF_NONE);
            }
            gbCtYfNumMapper.insert(gbCtYfNum);
        }


        //特征研发
        List<NGbCtGzProductDesign>  nGbCtGzProductDesigns = nGbCtGzProductDesignMapper.getList(contestId);
        List<NGbCtDesignProgress> nGbCtDesignProgresses = new ArrayList<>();
        for (NGbCtGzProductDesign nGbCtGzProductDesign : nGbCtGzProductDesigns) {
            NGbCtDesignProgress nGbCtDesignProgress = new NGbCtDesignProgress();
            BeanUtils.copyProperties(nGbCtGzProductDesign,nGbCtDesignProgress);
            nGbCtDesignProgress.setNowValue(nGbCtDesignProgress.getInitialValue()); //设置当前研发值为初始值
            nGbCtDesignProgress.setContestId(contestId);
            nGbCtDesignProgress.setStudentId(studentId);
            nGbCtDesignProgresses.add(nGbCtDesignProgress);
        }
        if (nGbCtDesignProgresses.size() > 0) {
            nGbCtDesignProgressService.saveBatch(nGbCtDesignProgresses);
        }


        //初始化社会责任分
        NGbCtCorporate ctCorporate = new NGbCtCorporate();
        ctCorporate.setContestId(contestId)
                .setScore(NGbCtCorporate.DEFAULT_SCORE)
                .setAction("初始值")
                .setActionTime(10)
                .setStudentId(studentId);
        gbCtCorporateMapper.insert(ctCorporate);


        // 更新开始状态
        gbContestStudentMapper.updateStart(contestId, studentId);


        //第一年生产碳排放数据
        NGbCtCarbon nGbCtCarbon  = new NGbCtCarbon();
        nGbCtCarbon.setYear(1);
        nGbCtCarbon.setContestId(contestId);
        nGbCtCarbon.setStudentId(studentId);
        nGbCtCarbon.setEmissionNum(0);
        nGbCtCarbon.setNeutralizaNum(0);
        nGbCtCarbonMapper.insert(nGbCtCarbon);


        //本科初始化会员指数
        if(gbContest.getContestType()==1){
            //初始化第一年第一季会员指数
            NGbCtMemberIndex nGbCtMemberIndex = new NGbCtMemberIndex();
            nGbCtMemberIndex.setContestId(contestId);
            nGbCtMemberIndex.setStudentId(studentId);
            nGbCtMemberIndex.setYear(1);
            nGbCtMemberIndex.setQuarterly(1);
            nGbCtMemberIndex.setHeatNum(0);
            nGbCtMemberIndex.setMemberIndex(0);
            nGbCtMemberIndexMapper.insert(nGbCtMemberIndex);
        }


        //生成初始报表
        gbActionBiz.isBankruptcy(studentId,contestId,11);

        //完成以上操作后，除了规则表，其余备份到新的表。
        gbRestoreDataBiz.backupData(contestId, studentId,11,false);
        return R.success();
    }
    /**
     *
     */
    public R saveProgress(Integer studentId, Integer contestId, Integer date, String progress) {
        gbContestStudentMapper.saveProgress(studentId, contestId, date, progress);
        return R.success();
    }

    public R getProgress(Integer studentId, Integer contestId) {
        NGbContestStudent gbContestStudent = gbContestStudentMapper.get(contestId, studentId);
        if (gbContestStudent == null) {
            return R.error(CodeEnum.PARAM_ERROR.getCode(), "竞赛不存在");
        }
        if (gbContestStudent.getFinishTime() != null) {
            return R.error(CodeEnum.OTHER_ERROR.getCode(), "已完成该竞赛模拟");
        }
        NGbContest gbContest = gbContestService.getById(contestId);
        Map<String, Object> returnMap = new HashMap<>();
//        returnMap.put("restore", gbContest.getRestore());
//        returnMap.put("restoreSeason", gbContest.getRestoreSeason());
//        returnMap.put("restart", gbContest.getRestart());
        returnMap.put("title", gbContest.getTitle());
        returnMap.put("progress", gbContestStudent.getProgress());
        returnMap.put("date", gbContestStudent.getDate());
        returnMap.put("data",gbContestStudent);
        return R.success(returnMap);
    }


    public R end(Integer studentId, Integer contestId) {
        NGbContestStudent gbContestStudent = gbContestStudentMapper.get(contestId, studentId);
        if (gbContestStudent == null) {
            return R.error(CodeEnum.OTHER_ERROR);
        }
        if (gbContestStudent.getFinishTime() != null) {
            return R.error(CodeEnum.OTHER_ERROR.getCode(), "已完成该竞赛模拟");
        }
        gbContestStudent.setFinishTime(LocalDateTime.now());
        gbContestStudentService.updateById(gbContestStudent);

//        // 清除本年度备份的数据
//        gbRestoreDataBiz.removeLikeTableDataAll(contestId, studentId);

        return R.success();
    }

    /**
     * 学生点击继续按钮
     * @param userId
     * @param contestId
     * @return
     */
    public R nextStatus(Integer userId, Integer contestId) {
        log.error("点击继续按钮,contestId:{},studentId:{}",contestId,userId);
        NGbContestStudent gbContestStudent = gbContestStudentService.getOne(new LambdaQueryWrapper<NGbContestStudent>()
                .eq(NGbContestStudent::getContestId,contestId)
                .eq(NGbContestStudent::getStudentId,userId)
        );
        //获取学生当前的status
        if(gbContestStudent!=null){
            int date = gbContestStudent.getDate();
            int status = date%10;
            //只有
            if(status<=3){
                gbContestStudent.setDate(date+1);
                log.error("点击继续按钮,contestId:{},studentId:{},date:{}",contestId,userId,gbContestStudent.getDate());
                gbContestStudentService.update(new NGbContestStudent(),new UpdateWrapper<NGbContestStudent>()
                        .eq("id",gbContestStudent.getId())
                        .eq("date",date)
                        .set("date",date+1)
                );
            }
        }
        return R.success();
    }

}
