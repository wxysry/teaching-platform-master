package com.xiyou.main.biz.ngbcontest;

import com.alibaba.excel.support.ExcelTypeEnum;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.xiyou.common.enums.CodeEnum;
import com.xiyou.common.exception.CustomException;
import com.xiyou.common.office.utils.EasyPOIUtil;
import com.xiyou.common.office.utils.FileUtil;
import com.xiyou.common.redis.utils.RedisCache;
import com.xiyou.common.utils.CommonUtil;
import com.xiyou.common.utils.R;
import com.xiyou.main.config.NGbWebSocket;
import com.xiyou.main.dao.exam.ResourcesMapper;
import com.xiyou.main.dao.ngbcontest.*;
import com.xiyou.main.entity.exam.Resources;
import com.xiyou.main.entity.exam.SysUser;
import com.xiyou.main.entity.gbcontest.GbCtLine;
import com.xiyou.main.entity.ngbcontest.*;
import com.xiyou.main.enums.CashFlowActionEnum;
import com.xiyou.main.enums.ErrorEnum;
import com.xiyou.main.enums.RedisEnum;
import com.xiyou.main.params.contest.ContestParam;
import com.xiyou.main.params.contest.ContestStudentParam;
import com.xiyou.main.params.ngbcontest.NGbCompanyParam;
import com.xiyou.main.params.ngbcontest.NGbDigitalizeParam;
import com.xiyou.main.schedule.ScheduleTaskService;
import com.xiyou.main.service.exam.SysUserService;
import com.xiyou.main.service.exam.TeachClassService;
import com.xiyou.main.service.ngbcontest.*;
import com.xiyou.main.utils.DateUtils;
import com.xiyou.main.vo.contest.ContestScore;
import com.xiyou.main.vo.ngbcontest.*;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @program: multi-module
 * @description:
 * @author: tangcan
 * @create: 2019-08-25 10:39
 **/
@Service
@Transactional(rollbackFor = Exception.class)
@Slf4j
public class NGbContestBiz {
    @Autowired
    private NGbContestService gbContestService;
    @Autowired
    private TeachClassService teachClassService;
    @Autowired
    private NGbContestStudentService gbContestStudentService;
    @Autowired
    private NGbCtSubjectMapper gbCtSubjectMapper;
    @Autowired
    private ResourcesMapper resourcesMapper;
    @Autowired
    private SysUserService sysUserService;
    @Autowired
    private NGbCtGzCsService gbCtGzCsService;
    @Autowired
    private NGbCtGzMarketMapper gbCtGzMarketMapper;
    @Autowired
    private NGbCtGzProductMapper gbCtGzProductMapper;
    @Autowired
    private NGbCtGzMaterialMapper gbCtGzMaterialMapper;
    @Autowired
    private NGbCtGzProducingMapper gbCtGzProducingMapper;
    @Autowired
    private NGbCtKcProductMapper gbCtKcProductMapper;
    @Autowired
    private NGbCtLineMapper gbCtLineMapper;
    @Autowired
    private NGbCtGzClassesMapper gbCtGzClassesMapper;
    @Autowired
    private NGbCtFeeMapper gbCtFeeMapper;
    @Autowired
    private NGbCtCashflowService gbCtCashflowService;
    @Autowired
    private NGbCtWorkerMapper gbCtWorkerMapper;
    @Autowired
    private NGbCtKcMaterialMapper gbCtKcMaterialMapper;
    @Autowired
    private NGbContestMapper gbContestMapper;
    @Autowired
    private NGbContestStudentMapper gbContestStudentMapper;
    @Autowired
    private ScheduleTaskService scheduleTaskService;
    @Autowired
    private RedisCache redisCache;
    @Autowired
    private NGbRestoreDataBiz gbRestoreDataBiz;
    @Autowired
    private NGbCtProfitChartMapper gbCtProfitChartMapper;
    @Autowired
    private NGbCtChargesMapper gbCtChargesMapper;
    @Autowired
    private NGbCtBalanceMapper gbCtBalanceMapper;
    @Autowired
    private NGbCtAnnouncementMapper gbCtAnnouncementMapper;
    @Autowired
    private NGbDownloadBiz gbDownloadBiz;
    @Autowired
    private NGbFillReportBiz gbFillReportBiz;
    @Autowired
    private NGbCtChargesService gbCtChargesService;
    @Autowired
    private NGbCtDdMaterialMapper gbCtDdMaterialMapper;
    @Autowired
    private NGbCtYfIsoMapper gbCtYfIsoMapper;
    @Autowired
    private NGbCtYfProductMapper gbCtYfProductMapper;
    @Autowired
    private NGbCtYfMarketMapper gbCtYfMarketMapper;
    @Autowired
    private NGbCtBankLoanMapper gbCtBankLoanMapper;
    @Autowired
    private NGbCtXzOrderMapper gbCtXzOrderMapper;
    @Autowired
    private NGbCtCashflowMapper gbCtCashflowMapper;

    @Autowired
    private NGbCtDesignProgressMapper nGbCtDesignProgressMapper;

    @Autowired
    private NGbCtCarbonMapper nGbCtCarbonMapper;

    @Autowired
    private NGbLineBiz nGbLineBiz;

    @Autowired
    private NGbTradeFairBiz gbTradeFairBiz;
    @Autowired
    private NGbCtMnAdMapper gbCtMnAdMapper;

    @Autowired(required = false)
    private NGbWebSocket nGbWebSocket;

    @Autowired
    private NGbCtGzIsoMapper nGbCtGzIsoMapper;

    @Autowired(required = false)
    private NGbCtDesignFeatureMapper nGbCtDesignFeatureMapper;

    @Autowired
    private NGbCtGzRetailMarketMapper nGbCtGzRetailMarketMapper;

    @Autowired
    private NGbCtRetailApplyMapper nGbCtRetailApplyMapper;

    @Autowired
    private NGbCtRetailAssignService nGbCtRetailAssignService;

    @Autowired
    private NGbCtMemberIndexMapper memberIndexMapper;

    @Autowired
    Environment environment;

    public R listByTeacher(ContestParam contestParam) {
        Page<NGbContest> page = gbContestService.getPage(contestParam);
        for (NGbContest gbContest : page.getRecords()) {
            // 竞赛状态
            int status;
            LocalDateTime now = LocalDateTime.now();
            if (gbContest.getIsPublish() == 0) {
                status = 0;
            } else if(gbContest.getIsPublish() == 1){
                status = 1;
            } else {
                status = 2;
            }
            gbContest.setStatus(status);
        }
        Map<String, Object> returnMap = new HashMap<>();
        returnMap.put("total", page.getTotal());
        returnMap.put("list", page.getRecords());
        return R.success(returnMap);
    }

    public R listByAdmin(ContestParam contestParam) {
        Page<NGbContest> page = gbContestService.getPageAdmin(contestParam);
        for (NGbContest gbContest : page.getRecords()) {
            // 竞赛状态
            int status;
            LocalDateTime now = LocalDateTime.now();
            if (gbContest.getIsPublish() == 0) {
                status = 0;
            } else if(gbContest.getIsPublish() == 1){
                status = 1;
            } else {
                status = 2;
            }
            gbContest.setStatus(status);
        }
        Map<String, Object> returnMap = new HashMap<>();
        returnMap.put("total", page.getTotal());
        returnMap.put("list", page.getRecords());
        return R.success(returnMap);
    }

    public R listByStudent(ContestParam contestParam) {
        Page<NGbContest> page = gbContestService.getStudentContestPage(contestParam);
        for (NGbContest contest : page.getRecords()) {
            // 竞赛状态
            int status;
            LocalDateTime now = LocalDateTime.now();
            if(contest.getFinishTime()==null){
                //已开始
                status = 1;
            }else{
                //已结束
                status = 2;
            }
            contest.setStatus(status);
        }
        Map<String, Object> returnMap = new HashMap<>();
        returnMap.put("total", page.getTotal());
        returnMap.put("list", page.getRecords());
        return R.success(returnMap);
    }

    public R get(Integer contestId) {
        Map<String, Object> returnMap = new HashMap<>();
        NGbContest gbContest = gbContestService.getById(contestId);
        if (gbContest == null) {
            return R.error(CodeEnum.OTHER_ERROR.getCode(), "无此竞赛");
        }
        NGbCtSubject gbCtSubject = gbCtSubjectMapper.getBySubjectNumber(gbContest.getSubjectNumber());
        if (gbCtSubject != null) {
            gbContest.setGroupId(gbCtSubject.getGroupId());
            gbContest.setStudentList(gbContestStudentService.getStudentIdList(contestId));
        }
        //如果没有暂停，则取redis中的剩余时间
        if(gbContest.getIsSuspend()==0){
            int  remainTime = (int)redisCache.getExpire(RedisEnum.GZHHDJS+":" + contestId);
            if(remainTime <= 0){
                remainTime = gbContest.getRemainTime();
            }
            gbContest.setRemainTime(remainTime);
        }
        returnMap.put("data", gbContest);

        return R.success(returnMap);
    }

    public R add(NGbContest gbContest) {
        List<Integer> studentIdList;
        if (gbContest.getPublishAll() != null && gbContest.getPublishAll() == 1) {
            studentIdList = teachClassService.getStudentIdList(gbContest.getTeacherId());
        } else {
            studentIdList = gbContest.getStudentList();
        }
        if (studentIdList != null) {
            studentIdList = studentIdList.stream().distinct().collect(Collectors.toList());
        }
        gbContest.setStudentNum(studentIdList == null ? 0 : studentIdList.size());
        gbContest.setTeacherDate(11);
        List<NGbCtGzMarket> markets = gbCtGzMarketMapper.selectList(new QueryWrapper<NGbCtGzMarket>().eq("subject_number", gbContest.getSubjectNumber()));
        StringBuilder marketSort = new StringBuilder();
        for (int i = 0; i < markets.size(); i++) {
            marketSort.append(markets.get(i).getCmName());
            if(i<markets.size()-1){
                marketSort.append("-->");
            }
        }
        gbContest.setMarketSort(marketSort.toString());//市场排序
        List<NGbCtGzProduct> products = gbCtGzProductMapper.selectList(new QueryWrapper<NGbCtGzProduct>().eq("subject_number", gbContest.getSubjectNumber()));
        StringBuilder productSort = new StringBuilder();
        for (int i = 0; i < products.size(); i++) {
            productSort.append(products.get(i).getCpName());
            if(i<products.size()-1){
                productSort.append("-->");
            }
        }
        gbContest.setProductSort(productSort.toString());//产品排序
        gbContest.setCountDown("[25,25,25,25,25,25,25,25,25,25,25,25,25,25,25,25,25,25,25,25,25,25,25,25,25,25,25,25,25,25,25,25]");
        gbContest.setRemainTime(10*60);
        gbContest.setIsSuspend(1);
        gbContestService.save(gbContest);
        if (studentIdList != null && studentIdList.size() > 0) {
            gbContestStudentService.insertBatch(gbContest.getContestId(), studentIdList);
        }


        return R.success().put("contestId", gbContest.getContestId());
    }


    public R update(NGbContest gbContest) {
        if (gbContest.getContestId() == null) {
            return R.error(CodeEnum.PARAM_MISS.getCode(), "请选择竞赛");
        }
        NGbContest gbContest1 = gbContestService.getById(gbContest.getContestId());
        if (gbContest1 == null || !gbContest1.getTeacherId().equals(gbContest.getTeacherId())) {
            return R.error(CodeEnum.PARAM_ERROR.getCode(), "无此竞赛");
        }
        // 未开始才能修改学生
        boolean start = (gbContest.getIsPublish() == 1);
        if (!start) {
            List<Integer> studentIdList;
            if (gbContest.getPublishAll() != null && gbContest.getPublishAll() == 1) {
                studentIdList = teachClassService.getStudentIdList(gbContest.getTeacherId());
            } else {
                studentIdList = gbContest.getStudentList();
            }
            if (studentIdList != null) {
                studentIdList = studentIdList.stream().distinct().collect(Collectors.toList());
            }
            gbContest.setStudentNum(studentIdList == null ? 0 : studentIdList.size());

            // 先删掉原来的学生
            gbContestStudentService.remove(gbContest.getContestId());
            if (studentIdList != null && studentIdList.size() > 0) {
                // 再插入
                gbContestStudentService.insertBatch(gbContest.getContestId(), studentIdList);
            }
        }
        if (start) {
            // 开始了就不再更改初始权益
            gbContest.setEquity(null);
        }
        gbContestService.updateById(gbContest);
        return R.success();
    }


    public R delete(Integer contestId) {
        //根据ID删除gb_contest
        gbContestService.removeById(contestId);
        QueryWrapper<NGbContestStudent> wrapper = new QueryWrapper<>();
        wrapper.eq("contest_id", contestId);
        //删除学生
        gbContestStudentService.remove(wrapper);
        //删除当前比赛相关数据
        gbContestMapper.deleteData(contestId);
        //删除备份数据
        gbContestMapper.deleteLikeData(contestId);
        //删除备份数据
        gbContestMapper.deleteSingleLikeData(contestId);

        return R.success();
    }


    public R publish(Integer teacherId, Integer contestId) {
        SysUser user = sysUserService.getById(teacherId);
        // 只能同时发布一个竞赛
        if (gbContestService.getUnEndContest(teacherId) >= user.getContestNum()) {
            return R.error(CodeEnum.OTHER_ERROR.getCode(), "只能同时发布 " + user.getContestNum() + " 套竞赛模拟");
        }
        NGbContest gbContest = gbContestService.getById(contestId);
        if (gbContest == null || !gbContest.getTeacherId().equals(teacherId)) {
            return R.error(CodeEnum.PARAM_ERROR.getCode(), "竞赛不存在");
        }
        if (gbContest.getIsPublish() == 1) {
            return R.error(CodeEnum.OTHER_ERROR.getCode(), "竞赛已发布！");
        }
        if (gbContest.getStudentNum() == 0) {
            return R.error(CodeEnum.OTHER_ERROR.getCode(), "暂无考生，不能发布！");
        }
        gbContestService.publish(teacherId, contestId);
        return R.success();
    }

    public R studentList(ContestStudentParam contestStudentParam) {
        Page<NGbContestStudent> page = gbContestStudentService.getPage(contestStudentParam);
        Map<String, Object> returnMap = new HashMap<>();
        returnMap.put("total", page.getTotal());
        returnMap.put("list", page.getRecords());
        return R.success(returnMap);
    }


    public R getRuleAttachment(Integer contestId) {
        Resources resources = gbCtSubjectMapper.getRuleAttachment(contestId);
        if (resources == null) {
            return R.error(CodeEnum.OTHER_ERROR.getCode(), "暂无规则预测详单");
        }
        return R.success().put("info", resources);
    }


    public R getRuleAttachmentBySubjectNumber(Integer subjectNumber) {
        Resources resources = gbCtSubjectMapper.getRuleAttachmentBySubjectNumber(subjectNumber);
        if (resources == null) {
            return R.error(CodeEnum.OTHER_ERROR.getCode(), "暂无规则预测详单");
        }
        return R.success().put("info", resources);
    }


    public R studentScore(Integer id, Double score) {
        gbContestStudentService.updateScore(id, score);
        return R.success();
    }

    public R getEquity(Integer subjectNumber) {
        // 规则表该套题中的cash
        NGbCtGzCs ctGzCs = gbCtGzCsService.getBySubjectNumber(subjectNumber);
        Integer cash = ctGzCs == null ? 0 : ctGzCs.getCash();
        return R.success().put("equity", cash);
    }

    public void downloadScore(Integer contestId, HttpServletResponse response) {
        NGbContest gbContest = gbContestService.getById(contestId);
        if (gbContest == null) {
            return;
        }
        List<ContestScore> contestScoreList = gbContestStudentService.getScoreList(contestId);
        Map<String, Object> paramMap = new HashMap<>();
        List<Map<String, Object>> mapList = new ArrayList<>(CommonUtil.getListInitCap(contestScoreList.size()));
        int num = 0;
        Map<String, Object> map;
        for (ContestScore contestScore : contestScoreList) {
            map = new HashMap<>();
            map.put("id", ++num);
            map.put("name", contestScore.getName());
            map.put("account", contestScore.getAccount());
            map.put("school", contestScore.getSchool());
            map.put("equity", contestScore.getEquity() == null ? "-" : (contestScore.getEquity() + "W"));
            map.put("score", contestScore.getScore() == null ? "-" : contestScore.getScore());
            mapList.add(map);
        }
        paramMap.put("maplist", mapList);
        String paperName = "《" + gbContest.getTitle() + "》";
        paramMap.put("title", paperName);
        try {
            EasyPOIUtil.exportMultiSheetExcelByMap(response, paperName + "竞赛成绩" + ".xlsx", CommonUtil.getClassesPath() + "templates/contest_score.xlsx", paramMap, false);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    /**
     * 数字化
     * @param userId
     * @param param
     * @return
     */
    public R digitalizeStart(Integer userId,  NGbDigitalizeParam param){

        Integer currentTime = param.getCurrentTime();
        Integer year = currentTime/10;
        Integer contestId = param.getContestId();

        //阻止暂停状态下 和 破产状态下的用户进行操作
        gbActionBiz.preventAction(contestId,userId);

        try {
            List<NGbDigitalizeParam.LineIdAndCpId> LineIdCpIds = param.getList();
            for (NGbDigitalizeParam.LineIdAndCpId lineIdAndCpId : LineIdCpIds) {
                NGbCtLine ctLine = gbCtLineMapper.selectById(lineIdAndCpId.getLineId());
                String status = ctLine.getStatus();
                if (!GbCtLine.ON_SPACE.equals(status)) {
                    throw new CustomException(CodeEnum.OTHER_ERROR.getCode(), "操作失败，请重试");
                }
            }

            //获取当前学生的操作时间
            Integer studentDate = gbContestStudentService.getStudentDate(param.getContestId(), userId);
            param.setCurrentTime(studentDate / 10);


            //获取所有空闲的工人
            List<NGbCtWorker> workers = gbCtWorkerMapper.getNGbCtWorkerListOnTheJob(param.getContestId(), userId, NGbCtWorker.ON_SPACE);
            //获取班次
            NGbCtGzClasses classes = gbCtGzClassesMapper.selectById(param.getClassesId());
            List<NGbCtWorker> workerGj = new ArrayList<>();//高级工人
            List<NGbCtWorker> workerPt = new ArrayList<>();//普通工人
            for (NGbCtWorker worker : workers) {
                if ("GR1".equals(worker.getRecruitNum())) {
                    workerPt.add(worker);
                }
                if ("GR2".equals(worker.getRecruitNum())) {
                    workerGj.add(worker);
                }
            }

            //获取原料库存信息
            List<NGbCtKcMaterial> materials = gbCtKcMaterialMapper.list(param.getContestId(), userId);

            //材料规则id和材料编号的键值对
            List<NGbCtGzMaterial> materialGzs = gbCtGzMaterialMapper.list(param.getContestId());
            Map<Integer, Integer> idAndCmIdMap = new HashMap<>();
            for (NGbCtGzMaterial gbCtGzMaterial : materialGzs) {
                idAndCmIdMap.put(gbCtGzMaterial.getId(), gbCtGzMaterial.getCmId());
            }
            //获取原料规则
            Map<Integer, NGbCtGzMaterial> materialGzMaps = new HashMap<>();
            for (NGbCtGzMaterial materialGz : materialGzs) {
                materialGzMaps.put(materialGz.getCmId(), materialGz);
            }
            //把材料库存分类
            Map<Integer, List<NGbCtKcMaterial>> kcMaterialMap = new HashMap<>();
            for (NGbCtGzMaterial materialGz : materialGzs) {
                List<NGbCtKcMaterial> list = new ArrayList<>();
                for (NGbCtKcMaterial material : materials) {
                    Integer imNum = idAndCmIdMap.get(material.getImCmId());
                    if (imNum.equals(materialGz.getCmId())) {
                        list.add(material);
                    }
                }
                kcMaterialMap.put(materialGz.getCmId(), list);
            }

            List<NGbDigitalizeParam.LineIdAndCpId> LineIdAndCpIds = param.getList();
            for (NGbDigitalizeParam.LineIdAndCpId lineIdAndCpId : LineIdAndCpIds) {

                NGbCtLine ctLine = gbCtLineMapper.selectById(lineIdAndCpId.getLineId());


                //生产线规则
                NGbCtGzProductLine gbCtGzProductLine = gbCtLineMapper.getGzLineByLineId(lineIdAndCpId.getLineId());
                //产品规则
                NGbCtGzProduct gbCtGzProduct = gbCtGzProductMapper.selectById(lineIdAndCpId.getCpId());

                int lineCarbon = gbCtGzProductLine.getCplCarbon();
                int productCarbon = gbCtGzProduct.getCpCarbon();


                //获取当前产品所需的材料及数量
                List<NGbCtGzProducing> gbCtGzProducings = gbCtGzProducingMapper.getListByCpId(lineIdAndCpId.getCpId());


                //获取对应的产品是否有特性
                NGbCtDesignFeature nGbCtDesignFeature = nGbCtDesignFeatureMapper.selectOne(new LambdaQueryWrapper<NGbCtDesignFeature>()
                        .eq(NGbCtDesignFeature::getContestId,contestId)
                        .eq(NGbCtDesignFeature::getStudentId,userId)
                        .eq(NGbCtDesignFeature::getCpId,lineIdAndCpId.getCpId())
                        .orderByDesc(NGbCtDesignFeature::getId)
                        .last(" limit 1")
                );
                //判断是否有最新的特性
                String featureNum;
                String featureName;
                if(nGbCtDesignFeature!=null){
                    featureName = nGbCtDesignFeature.getFeatureName();
                    featureNum = nGbCtDesignFeature.getFeatureNum();
                }
                else{
                    throw new CustomException(CodeEnum.OTHER_ERROR.getCode(), "产品"+gbCtGzProduct.getCpName()+"无产品特性，请先进行产品设计");
                }


                Integer cplSeniorWorker = gbCtGzProductLine.getCplSeniorWorker();//高级工人数
                Integer cplOrdinaryWorker = gbCtGzProductLine.getCplOrdinaryWorker();//普通工人数
                Integer productFinishTime = DateUtils.addYearAndSeasonTime(param.getCurrentTime(), gbCtGzProductLine.getCplProduceDate() == null ? 0 : gbCtGzProductLine.getCplProduceDate());


                //查询出当前产线的工人
                List<NGbCtWorker> workerOnLines = gbCtWorkerMapper.getAllWorkerListByLineId(ctLine.getLineId());
                List<NGbCtWorker> lineWorkerGjs = new ArrayList<>();//高级工人
                List<NGbCtWorker> lineWorkerPts = new ArrayList<>();//普通工人
                for (NGbCtWorker worker : workerOnLines) {
                    if ("GR1".equals(worker.getRecruitNum())) {
                        lineWorkerPts.add(worker);
                    }
                    if ("GR2".equals(worker.getRecruitNum())) {
                        lineWorkerGjs.add(worker);
                    }
                }
                //当前产线还需要xx名手工工人 和 高级工人
                int cplSeniorWorkerNeed = cplSeniorWorker - lineWorkerGjs.size();
                int cplOrdinaryWorkerNeed = cplOrdinaryWorker - lineWorkerPts.size();

                //判断工人数量是否充足
                if (((workerGj.size() - cplSeniorWorkerNeed) >= 0) && ((workerPt.size() - cplOrdinaryWorkerNeed) >= 0)) {

                    //分配高级工人
                    for (int i = 0; i < cplSeniorWorkerNeed; i++) {
                        NGbCtWorker gbCtWorker = workerGj.get(i);
                        gbCtWorker.setIsWork(NGbCtWorker.ON_WORK)
                                .setLineId(ctLine.getLineId());
                        gbCtWorkerMapper.updateById(gbCtWorker);
                        lineWorkerGjs.add(gbCtWorker);
                        workerGj.remove(gbCtWorker);
                    }

                    //分配普通工人
                    for (int i = 0; i < cplOrdinaryWorkerNeed; i++) {
                        NGbCtWorker gbCtWorker = workerPt.get(i);
                        gbCtWorker.setIsWork(NGbCtWorker.ON_WORK)
                                .setLineId(ctLine.getLineId());
                        gbCtWorkerMapper.updateById(gbCtWorker);
                        lineWorkerPts.add(gbCtWorker);
                        workerPt.remove(gbCtWorker);
                    }


                    //计算该产线的产量
                    //基础产能*（1+手工工人效率/4+高级技工效率）*班次加成
                    Integer sjxl = lineWorkerPts.stream().mapToInt(NGbCtWorker::getMultBonus).sum();
                    Integer gjxl = lineWorkerGjs.stream().mapToInt(NGbCtWorker::getMultBonus).sum();

                    BigDecimal result = new BigDecimal(gbCtGzProductLine.getCplProduction()).multiply(
                            new BigDecimal(100).add(new BigDecimal(gjxl)).add(new BigDecimal(sjxl).divide(new BigDecimal(4)))
                                    .divide(new BigDecimal(100))
                    ).multiply(new BigDecimal(classes.getOutputMulti() )).setScale(0, RoundingMode.DOWN);
                    int production  = result.intValue();


                    //碳排放计算
                    int addCarbon = lineCarbon + productCarbon * production;

                    //获取当年的碳排放数量
                    NGbCtCarbon nGbCtCarbon = nGbCtCarbonMapper.selectOne(new LambdaQueryWrapper<NGbCtCarbon>()
                            .eq(NGbCtCarbon::getContestId,contestId)
                            .eq(NGbCtCarbon::getStudentId,userId)
                            .eq(NGbCtCarbon::getYear,year)
                    );
                    int emissionNum = nGbCtCarbon.getEmissionNum()+addCarbon;
                    nGbCtCarbon.setEmissionNum(emissionNum);

                    //第三年 和 第四年要限制碳排放
                    if(year==3 || year==4){
                        int assignedNum =  nGbCtCarbon.getAssignedNum();
                        //如果实际排放大于分配的排放,则无法生产
                        if(assignedNum<emissionNum){
                            throw new CustomException(CodeEnum.OTHER_ERROR.getCode(), "碳排放量超标");
                        }
                    }


                    //获取生产所需加工费
                    int developFee = gbCtGzProduct.getCpDevelopFee() * production;
                    Integer cash = gbCtCashflowService.getCash(userId, param.getContestId());
                    int remain = cash - developFee;
                    if (remain < 0) {
                        return ErrorEnum.CASH_NOT_ENOUGH.getR();
                    }


                    //计件工资
                    Integer ptjjPrice = lineWorkerPts.stream().mapToInt(NGbCtWorker::getPiece).sum();
                    Integer gjjjPrice = lineWorkerGjs.stream().mapToInt(NGbCtWorker::getPiece).sum();
                    Integer jjgz = production * (ptjjPrice + gjjjPrice);
                    Integer remain2 = remain - jjgz;
                    if (remain2 < 0) {
                        return ErrorEnum.CASH_NOT_ENOUGH.getR();
                    }



                    //更新碳排放
                    nGbCtCarbonMapper.updateById(nGbCtCarbon);


                    //插入现金交易记录 - 加工费
                    NGbCtCashflow ctCashflow = new NGbCtCashflow();
                    ctCashflow.setStudentId(userId)
                            .setContestId(param.getContestId())
                            .setCAction(CashFlowActionEnum.PRODUCT_PRODUCING.getAction())
                            .setCOut(developFee)
                            .setCIn(0)
                            .setCComment("生产线" + lineIdAndCpId.getLineId().toString() + "生产产品" + gbCtGzProduct.getCpName() + "加工费")
                            .setCDate(param.getCurrentTime())
                            .setCSurplus(remain);
                    gbCtCashflowService.save(ctCashflow);


                    //插入现金交易记录 - 计件工资
                    NGbCtCashflow ctCashflow2 = new NGbCtCashflow();
                    ctCashflow2.setStudentId(userId)
                            .setContestId(param.getContestId())
                            .setCAction(CashFlowActionEnum.JJGZ.getAction())
                            .setCOut(jjgz)
                            .setCIn(0)
                            .setCComment("生产线" + lineIdAndCpId.getLineId().toString() + "生产产品" + gbCtGzProduct.getCpName() + "计件工资")
                            .setCDate(param.getCurrentTime())
                            .setCSurplus(remain2);
                    gbCtCashflowService.save(ctCashflow2);



                    //下个季度的工资
                    Integer ptgz = lineWorkerPts.stream().mapToInt(item->item.getNextInitSal()*3).sum();
                    Integer gjgz = lineWorkerGjs.stream().mapToInt(item->item.getNextInitSal()*3).sum();

                    //工资分摊 下月工人工资之和 /产品数量
                     Integer gzft = (ptgz+gjgz)/production;



                     //需要采购的原材料-及其数量
                    Map<Integer,Integer> needBuyMaterialMap = new HashMap<>();


                    //开始生产产品
                    for (int i = 0; i < production; i++) {
                        int cashNeedOne = 0; //计算产品的成本
                        for (NGbCtGzProducing gbCtGzProducing : gbCtGzProducings) {
                            int needNum = gbCtGzProducing.getCpNum();
                            if (needNum == 0) {
                                continue;
                            }

                            List<NGbCtKcMaterial> materialList = kcMaterialMap.get(gbCtGzProducing.getCpMid());

                            for (NGbCtKcMaterial material : materialList) {
                                if (material.getImNum() - needNum > 0) {
                                    //计算成本，更新材料剩余数量
                                    cashNeedOne = cashNeedOne + material.getMaterialPrice() * needNum;
                                    material.setImNum(material.getImNum() - needNum);
                                    needNum = 0;
                                    gbCtKcMaterialMapper.updateById(material);
                                    break;
                                } else {
                                    //计算金额，删除数据（改为更新为0），结束当前循环
                                    cashNeedOne = cashNeedOne + material.getMaterialPrice() * material.getImNum();
                                    needNum = needNum - material.getImNum();
                                    material.setImNum(0);
                                    gbCtKcMaterialMapper.deleteById(material);
                                }
                            }
                            if (needNum != 0) {//库存不足  直接原价购买
                                NGbCtGzMaterial gbCtGzMaterial = materialGzMaps.get(gbCtGzProducing.getCpMid());
                                int fee = needNum * gbCtGzMaterial.getCmBuyFee();
                                cashNeedOne += fee;

                                //记录下需要额外采购的原材料的数量,并累加到Map里面
                                if(needBuyMaterialMap.containsKey(gbCtGzProducing.getCpMid())){
                                    int num = needBuyMaterialMap.get(gbCtGzProducing.getCpMid());
                                    needBuyMaterialMap.put(gbCtGzProducing.getCpMid(),needNum+num);
                                }else{
                                    needBuyMaterialMap.put(gbCtGzProducing.getCpMid(),needNum);
                                }
                            }
                        }
                        cashNeedOne += gbCtGzProduct.getCpDevelopFee();//加工费

                        //工人的计件工资还需要核算到产品的成本里面去
                        cashNeedOne += (gjjjPrice + ptjjPrice);

                        //工人的基本工资都要核算到成本里面

                        //最后一个人的工资分摊
                        if(i== (production-1)){
                            int lastFt = (ptgz+gjgz) - gzft*(production-1);
                            cashNeedOne += lastFt;
                        }else{
                            //工人的基本工资都要核算到成本里面
                            cashNeedOne += gzft;
                        }


                        //生成库存数据
                        NGbCtKcProduct gbCtKcProduct = new NGbCtKcProduct();
                        gbCtKcProduct.setContestId(param.getContestId())
                                .setStudentId(userId)
                                .setIpCpId(lineIdAndCpId.getCpId())
                                .setInitNum(1)
                                .setIpNum(1)
                                .setFeatureName(featureName) //特性名称
                                .setFeatureNum(featureNum)  //特性编码
                                .setLineId(lineIdAndCpId.getLineId())
                                .setRealCost(cashNeedOne);
                        // 产品入库
                        if (gbCtGzProduct.getCpDevelopDate() != null && gbCtGzProduct.getCpDevelopDate() > 0) {
                            gbCtKcProduct.setIsInventory(NGbCtKcProduct.NOT_IN);
                        } else {
                            gbCtKcProduct.setIsInventory(NGbCtKcProduct.IS_IN);
                        }
                        gbCtKcProduct.setInventoryDate(productFinishTime);//入库时间
                        gbCtKcProductMapper.insert(gbCtKcProduct);
                    }


                    //生成采购原材料的应付款信息
                    needBuyMaterialMap.forEach((k,v)->{
                        NGbCtGzMaterial gbCtGzMaterial = materialGzMaps.get(k);
                        int fee = v * gbCtGzMaterial.getCmBuyFee();
                        //产生应付款账期
                        NGbCtFee ctFee = new NGbCtFee();
                        int paymentDate = DateUtils.addYearAndSeasonTime(param.getCurrentTime(), gbCtGzMaterial.getCmPayDate());
                        ctFee.setRFee(fee)
                                .setType(NGbCtFee.TO_PAYMENT)
                                .setContestId(param.getContestId())
                                .setStudentId(userId)
                                .setBorrower("1")
                                .setLender("供应商")
                                .setPaymentDate(paymentDate)
                                .setRRemainDate(gbCtGzMaterial.getCmPayDate())
                                .setRemarks("智能生产采购"+v+"个原料"+gbCtGzMaterial.getCnName());
                        gbCtFeeMapper.insert(ctFee);
                    });


                    //更新生产线
                    if (gbCtGzProduct.getCpDevelopDate() != null && gbCtGzProduct.getCpDevelopDate() > 0) {
                        ctLine.setStatus(NGbCtLine.ON_PRODUCE);
                    } else {
                        ctLine.setStatus(NGbCtLine.ON_SPACE);
                    }
                    ctLine.setPlProductAddDate(param.getCurrentTime())
                            .setPlProductingDate(productFinishTime)
                            .setRealProduction(production)
                            .setPlCpid(lineIdAndCpId.getCpId())
                            .setFeatureName(featureName)
                            .setFeatureNum(featureNum)
                            .setClassesId(param.getClassesId());
                    gbCtLineMapper.updateById(ctLine);
                }
            }
            return R.success();
        }
        finally {
            //重新计算报表
            //计算报表,判断是否破产
            gbActionBiz.isBankruptcy(userId,param.getContestId(),param.getCurrentTime());    //重新计算报表
        }
    }

    /**
     * 暂停竞赛
     * @param contestId
     * @return
     */
    public R pause(Integer contestId){
        // 持久化剩余时间
        long expire = redisCache.getExpire(RedisEnum.GZHHDJS+":" + contestId);
        //小于0说明redis已过期
        if(expire<0){
            expire=0;
        }
        NGbContest gbContest = gbContestService.getById(contestId);
        gbContest.setRemainTime((int)expire);
        gbContestService.updateById(gbContest);
        // 清除任务
        this.stopContestScheduleTask(contestId);
        // 更新暂停状态为暂停
        gbContestMapper.pauseContest(contestId);

        Map<String, Object> messageMap = new HashMap<>();
        messageMap.put("type","refresh"); //数据刷新
        String message = JSONObject.toJSONString(messageMap);
        nGbWebSocket.sendMessageToAll(contestId,message); //给所有人广播消息
        return R.success();
    }

    /**
     * 恢复竞赛
     * @param contestId
     * @return
     */
    public R recover(Integer contestId){
        //添加调度任务
        NGbContest gbContest = gbContestService.getById(contestId);
        Integer remainTime = gbContest.getRemainTime();

        //判断是否还有剩余时间,如果是没有剩余时间的情况下，点击恢复，则无需计时
        if(remainTime>0){
            this.addContestScheduleTask(contestId,remainTime);
        }
        //更新暂停状态
        gbContestMapper.recoverContest(contestId);

        Map<String, Object> messageMap = new HashMap<>();
        messageMap.put("type","refresh"); //数据刷新
        String message = JSONObject.toJSONString(messageMap);
        nGbWebSocket.sendMessageToAll(contestId,message); //给所有人广播消息
        return R.success();
    }


    /**
     * 上一回合
     * @param contestId
     * @return
     */
    public R lastRound(Integer contestId,Integer currentTime){

        NGbContest  nGbContest =  gbContestService.getById(contestId);
        Integer teacherDate = nGbContest.getTeacherDate();
        if(!teacherDate.equals(currentTime)){
            throw new CustomException(CodeEnum.OTHER_ERROR.getCode(), "时间错误，请刷新页面后重试");
        }


        Integer lastRoundTime = DateUtils.addYearAndSeasonTime(currentTime,-1);

        // 暂停比赛
        this.pause(contestId);
        // 还原数据
        if(currentTime<=11){
            return ErrorEnum.NO_LAST_ROUND.getR();
        }

        //如果当前回合的学生数量为0，假设 2-3 -> 2-4 . 如果所有的学生都没有跳成功,而教师跳至2-4。可只还原教师端不还原学生端
        //获取符合进行下一回合条件的学生，：1.时间线与大部队一致
        List<NGbContestStudent> studentList= gbContestStudentService.list(new LambdaQueryWrapper<NGbContestStudent>()
                .eq(NGbContestStudent::getContestId,contestId));
        List<NGbContestStudent> lastRoundStudentList = studentList.stream().filter(item->{
            //学生的时间线
            int studentDate = item.getDate()/10;
            return (studentDate==currentTime);
        }).collect(Collectors.toList());

        if(CollectionUtils.isNotEmpty(lastRoundStudentList)){
            // 全部还原学生数据   currentTime 不超过44
            gbRestoreDataBiz.restoreAll(contestId,Math.min(currentTime,44),lastRoundTime);
        }


        // 重新计时
        int seasonTime = getSeasonTime(contestId, lastRoundTime);
        //计算最后一次订货会的时间，取gb_ct_xz_order里面最大的年份 和 季度
         NGbCtXzOrder gbCtXzOrder = gbCtXzOrderMapper.selectOne(new LambdaQueryWrapper<NGbCtXzOrder>()
                .eq(NGbCtXzOrder::getContestId,contestId)
                .orderByDesc(NGbCtXzOrder::getDate)
                .orderByDesc(NGbCtXzOrder::getQuarterly)
                .last("limit 1")
        );
        Integer lastFairOrderDate= 10;
         if(gbCtXzOrder!=null){
             lastFairOrderDate=gbCtXzOrder.getDate()*10+gbCtXzOrder.getQuarterly();
         }

        //更新gb_contest  剩余时间/暂停状态/教师当前时间
        //最后一次订货会时间
        gbContestMapper.updateById(new NGbContest()
                .setContestId(contestId)
                .setRemainTime(seasonTime)
                //不暂停
                .setIsSuspend(0)
                .setTeacherDate(lastRoundTime)
                //设置最后一次订货会时间
                .setLastFairOrderDate(lastFairOrderDate)
                //订货会状态为0
                .setFairOrderState(0)
        );
         //倒计时开始
        this.addContestScheduleTask(contestId,seasonTime);

        Map<String, Object> messageMap = new HashMap<>();
        messageMap.put("type","refresh"); //数据刷新
        String message = JSONObject.toJSONString(messageMap);
        nGbWebSocket.sendMessageToAll(contestId,message); //给所有人广播消息
        return R.success();
    }



    /**
     * 下一回合
     * @param teacherId
     * @param contestId
     * @param currentTime
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    public R nextRound(Integer teacherId,Integer contestId,Integer currentTime){
        NGbContest  nGbContest =  gbContestService.getById(contestId);
        Integer subjectNumber = nGbContest.getSubjectNumber();
        Integer teacherDate = nGbContest.getTeacherDate();
        if(!teacherDate.equals(currentTime)){
            throw new CustomException(CodeEnum.OTHER_ERROR.getCode(), "时间错误，请刷新页面后重试");
        }

        log.error("点击下一回合,开始,contestId:{},currentTime:{}",contestId,currentTime);
        Integer nextSeason = DateUtils.addYearAndSeasonTime(currentTime,1);
        int season = currentTime%10;
        int year = currentTime/10;


        //获取符合进行下一回合条件的学生，：1.时间线与大部队一致
        List<NGbContestStudent> studentList= gbContestStudentService.list(new LambdaQueryWrapper<NGbContestStudent>()
                .eq(NGbContestStudent::getContestId,contestId));
        List<NGbContestStudent> nextRoundStudentList = studentList.stream().filter(item->{
            //学生的时间线
            int studentDate = item.getDate()/10;
            //跳到31 和 41的时候,需要保证所有人的时间都跟上了,不然不允许跳到下一回合。此出的目的是为了保障碳排放分配的数据准确
            if(currentTime==24 || currentTime==34){
                if(currentTime!=studentDate){
                    throw new CustomException(CodeEnum.OTHER_ERROR.getCode(), "当前有学生仍在"+(studentDate/10)+"年"+(studentDate%10)+"季，请等待");
                }
            }
            //学生最近一次还原的年份
            log.error("下一回合-学生:{},studentDate:{}",item.getStudentId(),item.getDate());
            log.error("下一回合-当前学生是否进入下一回合:{}",(studentDate==currentTime));
            return (studentDate==currentTime);
        }).collect(Collectors.toList());


        if(CollectionUtils.isEmpty(nextRoundStudentList)){
            throw new CustomException(CodeEnum.OTHER_ERROR.getCode(), "当前学生不满足条件，无法进行到下一回合，请刷新后重试");
        }



        //如果是本科类型的考试，有网络营销功能，需要进行网络订单的分配和交货
        if(nGbContest.getContestType()==1){
            assignNetworkMarket(nextRoundStudentList,contestId,currentTime,subjectNumber);
        }


        //跳至下一回合
        for (NGbContestStudent gbContestStudent : nextRoundStudentList) {
            int status = gbContestStudent.getDate()%10;

            //小于等于4说明未进行当季/当年结束的操作，系统自动进行当季结束操作
            if(status <= 4){
                // 当季结束/当季结束   保存结束时间-并不会备份数据
                gbActionBiz.endingSeason(gbContestStudent.getStudentId(),contestId,currentTime);
            }

            //判断是否是某年的第四个季度。该季度需要填表
            if(season==4){
                // 判断当年报表是否填写，没有填写的话，自动填写，值全部填0.并标记报表错误年份
                NGbCtCharges gbCtCharges = gbCtChargesService.getOne(new NGbCtCharges()
                        .setStudentId(gbContestStudent.getStudentId()).setCDate(currentTime / 10).setContestId(contestId).setBsIsxt(0));

                //为空则需要自动填写报表
                if(null == gbCtCharges){
                    gbFillReportBiz.fillDefault(gbContestStudent.getStudentId(),currentTime/10,contestId);
                }
            }


        }





        //跳至下一回合
        for (NGbContestStudent gbContestStudent : nextRoundStudentList) {
            //判断是否结束,如果未结束，则开始到下一个季度
            if(currentTime<44){
                //开始下一个季度
                gbActionBiz.nextSeasonAction(gbContestStudent.getStudentId(),contestId,nextSeason);
            }else{
                gbContestStudent.setFinishTime(LocalDateTime.now());
                gbContestStudentService.updateById(gbContestStudent);
                log.error("比赛已结束,contestId",contestId);
            }
        }


        if(currentTime<44){
            // 重新计算下一回合时间
            int seasonTime = getSeasonTime(contestId, nextSeason);
            //更新gb_contest  剩余时间/暂停状态/教师当前时间
            gbContestMapper.updateById(new NGbContest()
                    .setContestId(contestId)
                    .setRemainTime(seasonTime)
                    .setIsSuspend(0)
                    .setTeacherDate(nextSeason)
                    //更新订货会状态
                    .setFairOrderState(0)
            );
            //执行调度
            this.addContestScheduleTask(contestId,seasonTime);
        }else{
            gbContestMapper.updateById(new NGbContest()
                    .setContestId(contestId)
                    .setRemainTime(0)
                    .setIsSuspend(0)
                    .setTeacherDate(nextSeason)
            );
        }
        Map<String, Object> messageMap = new HashMap<>();
        messageMap.put("type","refresh"); //数据刷新
        String message = JSONObject.toJSONString(messageMap);
        nGbWebSocket.sendMessageToAll(contestId,message); //给所有人广播消息
        log.error("点击下一回合,结束,contestId:{},currentTime:{}",contestId,currentTime);
        return R.success();
    }


    /**
     * 网络市场的分配和交单处理
     * @param nextRoundStudentList
     * @param contestId
     * @param currentTime
     * @param subjectNumber
     */
    public void assignNetworkMarket(List<NGbContestStudent> nextRoundStudentList,Integer contestId,Integer currentTime,Integer subjectNumber){

        int season = currentTime%10;
        int year = currentTime/10;
        //获取未破产的学生id
        List<Integer> studentIds =  nextRoundStudentList.stream().filter(item->item.getIsBankruptcy()==0).map(item-> item.getStudentId()).collect(Collectors.toList());
        //如果学生全都破产了就不需要分配了
        if(CollectionUtils.isNotEmpty(studentIds)){
            List<NGbCtGzRetailMarket> marketList = nGbCtGzRetailMarketMapper.selectList(new LambdaQueryWrapper<NGbCtGzRetailMarket>()
                    .eq(NGbCtGzRetailMarket::getSubjectNumber,subjectNumber)
                    .eq(NGbCtGzRetailMarket::getYear,year)
                    .eq(NGbCtGzRetailMarket::getQuarterly,season)
            );

            //遍历分配
            for (NGbCtGzRetailMarket nGbCtGzRetailMarket : marketList) {
                Integer retailMarketId = nGbCtGzRetailMarket.getId();
                List<NGbCtRetailApply> applyList  = nGbCtRetailApplyMapper.selectList(new LambdaQueryWrapper<NGbCtRetailApply>()
                        .eq(NGbCtRetailApply::getContestId,contestId)
                        .in(NGbCtRetailApply::getStudentId,studentIds)
                        .eq(NGbCtRetailApply::getRetailId,retailMarketId)
                );

                NGbCtGzProduct ctGzProduct = gbCtGzProductMapper.get(contestId,nGbCtGzRetailMarket.getCpId());//获取规则


                for (NGbCtRetailApply nGbCtRetailApply : applyList) {
                    Integer studentId = nGbCtRetailApply.getStudentId();
                    //获取会员指数
                    NGbCtMemberIndex nGbCtMemberIndex = memberIndexMapper.selectOne(new LambdaQueryWrapper<NGbCtMemberIndex>()
                            .eq(NGbCtMemberIndex::getContestId,contestId)
                            .eq(NGbCtMemberIndex::getStudentId,studentId)
                            .eq(NGbCtMemberIndex::getYear,year)
                            .eq(NGbCtMemberIndex::getQuarterly,season)
                    );
                    Integer memberIndex = nGbCtMemberIndex.getMemberIndex();

                    //单价承受能力
                    Integer supportPrice = nGbCtGzRetailMarket.getSupportPrice();
                    //定价
                    Integer applyPrice = nGbCtRetailApply.getApplyPrice();

                    //计算零售指数  零售指数 = 会员指数*(单价承受能力-定价)*0.01 向下取整
                    BigDecimal retailIndexBigcimal = new BigDecimal(memberIndex).multiply(new BigDecimal(supportPrice).subtract(new BigDecimal(applyPrice))).divide(new BigDecimal(100),0,RoundingMode.DOWN);

                    Integer retailIndex = retailIndexBigcimal.intValue();

                    //上架量
                    Integer sjl = nGbCtRetailApply.getApplyNum();

                    //竞争指数
                    Integer competeIndex = Math.min(sjl,retailIndex);

                    nGbCtRetailApply.setMemberIndex(memberIndex); //会员指数
                    nGbCtRetailApply.setRetailIndex(retailIndex); //零售指数
                    nGbCtRetailApply.setCompeteIndex(competeIndex); //竞争指数
                }

                //进行分配任务
                //获取市场需求量
                int needNum = nGbCtGzRetailMarket.getNum();
                //先计算竞争指数之和
                int competeIndexTotal = applyList.stream().mapToInt(NGbCtRetailApply::getCompeteIndex).sum();

                //如果竞争指数之和小于或等于市场需求量,则销量 = 竞争指数
                if(competeIndexTotal<=needNum){
                    applyList.forEach(item->item.setAssignedNum(item.getCompeteIndex()));
                }
                //如果竞争指数之和大于市场需求量,则按照比例进行分配，向下取整
                else{
                    applyList.forEach(item->{
                        BigDecimal assignNumBigDecimal = new BigDecimal(needNum)
                                .multiply(new BigDecimal(item.getCompeteIndex()))
                                .divide(new BigDecimal(competeIndexTotal),0,RoundingMode.DOWN);
                        item.setAssignedNum(assignNumBigDecimal.intValue());
                    });
                }

                // 分配完成后需要进行进行交单
                applyList.stream().forEach(item-> {
                    //订单分配
                    NGbCtRetailAssign nGbCtRetailAssign = new NGbCtRetailAssign();
                    BeanUtils.copyProperties(item, nGbCtRetailAssign);
                    nGbCtRetailAssign.setId(null);
                    nGbCtRetailAssignService.save(nGbCtRetailAssign);

                    //交单
                    //减少库存量
                    List<NGbCtKcProduct> listKc = gbCtKcProductMapper.getAllKcProductAsc(contestId,item.getStudentId(),ctGzProduct.getId(),item.getDesignNum());//ASC产品库存

                    //判断库存是否够交单
                    if(listKc.size()<item.getAssignedNum()){
                        //扣社会责任分
                        gbActionBiz.minusScoreAndLog(item.getStudentId(),contestId,"零售市场交货库存数量不足",currentTime);
                    }

                    int deliveryNum = Math.min(item.getAssignedNum(),listKc.size()) ;
                    int cost = 0;//成本
                    for (int i = 0; i < listKc.size(); i++) {
                        if(i < deliveryNum){
                            NGbCtKcProduct gbCtKcProduct = listKc.get(i);
                            int realCost = gbCtKcProduct.getRealCost();
                            cost = cost + realCost;
                            //更新产品库存数为0
                            //更新产品库存数为0
                            gbCtKcProduct.setIpNum(0);
                            gbCtKcProductMapper.updateById(gbCtKcProduct);
                        }
                    }
                    nGbCtRetailAssign.setDeliveryNum(deliveryNum); //交货量
                    nGbCtRetailAssign.setTotalCost(cost); //成本
                    nGbCtRetailAssignService.updateById(nGbCtRetailAssign);


                    //得到现金
                    int cash = gbCtCashflowService.getCash(item.getStudentId(),contestId);
                    int saleAmount = nGbCtRetailAssign.getApplyPrice() * deliveryNum;
                    gbCtCashflowService.save(new NGbCtCashflow().setStudentId(item.getStudentId())
                            .setContestId(contestId)
                            .setCAction(CashFlowActionEnum.LSDDJH.getAction())
                            .setCIn(saleAmount)
                            .setCOut(0)
                            .setCSurplus(cash + saleAmount)
                            .setCComment("零售市场销售"+item.getCpId()+"产品"+item.getDesignNum()+"特征,单价"+item.getApplyPrice()+"数量"+item.getAssignedNum())
                            .setCDate(currentTime));
                });
            }
        }
    }



    /**
     * 还原本季
     * @param contestId
     * @param currentTime
     * @return
     */
    public R restoreCurrentRound(Integer contestId,Integer currentTime){

        // 暂停比赛
        this.pause(contestId);
        // 全部还原学生数据
        gbRestoreDataBiz.restoreAll(contestId,currentTime,currentTime);
        // 重新计时
        int seasonTime = getSeasonTime(contestId, currentTime);
        //计算最后一次订货会的时间，取gb_ct_xz_order里面最大的年份 和 季度
        NGbCtXzOrder gbCtXzOrder = gbCtXzOrderMapper.selectOne(new LambdaQueryWrapper<NGbCtXzOrder>()
                .orderByDesc(NGbCtXzOrder::getDate)
                .orderByDesc(NGbCtXzOrder::getQuarterly)
                .last("limit 1")
        );
        Integer lastFairOrderDate= 10;
        if(gbCtXzOrder!=null){
            lastFairOrderDate=gbCtXzOrder.getDate()*10+gbCtXzOrder.getQuarterly();
        }
        //更新gb_contest  剩余时间/暂停状态/教师当前时间
        //最后一次订货会时间
        gbContestMapper.updateById(new NGbContest()
                .setContestId(contestId)
                .setRemainTime(seasonTime)
                //不暂停
                .setIsSuspend(0)
                .setTeacherDate(currentTime)
                //设置最后一次订货会时间
                .setLastFairOrderDate(lastFairOrderDate)
                //更新订货会状态
                .setFairOrderState(0)
        );
        //倒计时开始
        this.addContestScheduleTask(contestId,seasonTime);
        Map<String, Object> messageMap = new HashMap<>();
        messageMap.put("type","refresh"); //数据刷新
        String message = JSONObject.toJSONString(messageMap);
        nGbWebSocket.sendMessageToAll(contestId,message); //给所有人广播消息
        return R.success();
    }










    /**
     * 添加竞赛计时调度
     * @param contestId
     * @param seasonTime
     */
    public void addContestScheduleTask(Integer contestId,Integer seasonTime){
        scheduleTaskService.add(RedisEnum.GZHHDJS+":" + contestId, new Runnable() {
            @Override
            public void run() {
                log.error("时间到了,暂停比赛");
                // 更新暂停状态为暂停
                //时间到了需要暂停比赛
                gbContestMapper.pauseContest(contestId);
                gbContestMapper.updateById(new NGbContest().setContestId(contestId).setRemainTime(0));
                Map<String, Object> messageMap = new HashMap<>();
                messageMap.put("type","refresh"); //数据刷新
                String message = JSONObject.toJSONString(messageMap);
                nGbWebSocket.sendMessageToAll(contestId,message); //给所有人广播消息
            }
        },seasonTime);
    }

    /**
     * 停止竞赛计时调度
     * @param contestId
     */
    public void stopContestScheduleTask(Integer contestId){
        scheduleTaskService.stop(RedisEnum.GZHHDJS+":" + contestId);
    }

    /**
     *设置各季度时间
     * @param contestId
     * @param countDown
     * @return
     */
    public R setSeasonTime(Integer contestId,String countDown){
        NGbContest gbContest = gbContestService.getById(contestId);
        gbContest.setCountDown(countDown);
        gbContestService.updateById(gbContest);
        return R.success();
    }

    /**
     * 公共信息
     * @param contestId
     * @param year
     * @return
     */
    public R getPublicInfo(Integer contestId,Integer year){
        List<NGbCtBalance> gbCtBalanceList = gbCtBalanceMapper.getListByContestId(contestId,year);//资产负债表
        List<NGbCtCharges> gbCtChargesList = gbCtChargesMapper.getListByContestId(contestId, year);//综合费用表
        List<NGbCtProfitChart> gbCtProfitChartsList = gbCtProfitChartMapper.getListByContestId(contestId, year);//利润表
        //塞入结束时间
        for (NGbCtBalance gbCtBalance : gbCtBalanceList) {

            NGbContestStudent gbContestStudent = gbContestStudentMapper.selectOne(new LambdaQueryWrapper<NGbContestStudent>()
                    .eq(NGbContestStudent::getContestId,contestId)
                    .eq(NGbContestStudent::getStudentId,gbCtBalance.getStudentId())
                    .last("limit 1")
            );
            String everySeasonEndTime = gbContestStudent.getEverySeasonEndTime();
            if((everySeasonEndTime==null)||("".equals(everySeasonEndTime))){
                everySeasonEndTime="[]";
            }
//          String endTime = gbContestStudentService.getEndTime(contestId, gbCtBalance.getStudentId(), year*10+4);
            gbCtBalance.setEndTime1(getDateFinshTime(everySeasonEndTime,year*10+1));
            gbCtBalance.setEndTime2(getDateFinshTime(everySeasonEndTime,year*10+2));
            gbCtBalance.setEndTime3(getDateFinshTime(everySeasonEndTime,year*10+3));
            gbCtBalance.setEndTime(getDateFinshTime(everySeasonEndTime,year*10+4));
        }
        return Objects.requireNonNull(Objects.requireNonNull(R.success().
                put("gbCtBalanceList", gbCtBalanceList)).
                put("gbCtChargesList", gbCtChargesList)).
                put("gbCtProfitChartsList",gbCtProfitChartsList);
    }


    /**
     * 根据时间获取结束时间
     * @param everySeasonEndTime
     * @param dateTime
     * @return
     */
    public String getDateFinshTime(String everySeasonEndTime,int dateTime){

        JSONArray jsonArray = JSONArray.parseArray(everySeasonEndTime);

        String targetTime = null;
        for (int i = 0; i < jsonArray.size(); i++) {
            JSONObject jsonObject = jsonArray.getJSONObject(i);
            int date = jsonObject.getIntValue("date");
            String time = jsonObject.getString("time");
            if (date == dateTime) {
                targetTime = time;
                break;
            }
        }
        //如果没有对应的结束时间，则为空
        if(targetTime==null){
            return "";
        }else{
            return targetTime;
        }
    }



    @Autowired
    private  NGbActionBiz gbActionBiz;

    /**
     * 企业信息
     * @param
     * @param
     * @return
     */
    public R getCompanyInfo(NGbCompanyParam gbCompanyParam){
        Integer studentId = gbCompanyParam.getStudentId();
        Integer contestId = gbCompanyParam.getContestId();
        List<?> list = new ArrayList<>();
        String type = gbCompanyParam.getType();
        switch (type) {
            case "原料订购":
                list = gbCtDdMaterialMapper.list(contestId, studentId);
                break;
            case "产品库存":
                list = gbCtKcProductMapper.listKc(contestId, studentId);
                break;
            case "原料库存":
                list = gbCtKcMaterialMapper.listKcGroupNumber(contestId, studentId);
                break;
            case "应付款":
                list = gbCtFeeMapper.getPayList(studentId, contestId);
                break;
            case "应收款":
                list = gbCtFeeMapper.getRecList(studentId, contestId);
                break;
            case "贷款信息":
                list = gbCtBankLoanMapper.getList(contestId, studentId);
                break;
            case "市场开拓":
                list = gbCtYfMarketMapper.getList(contestId, studentId);
                break;
            case "产品研发":
                list = gbCtYfProductMapper.getList(contestId, studentId);
                break;
            case "ISO认证":
                list = gbCtYfIsoMapper.getList(contestId, studentId);
                break;
            case "特性等级":
                list = nGbCtDesignProgressMapper.selectList(new LambdaQueryWrapper<NGbCtDesignProgress>()
                        .eq(NGbCtDesignProgress::getContestId,contestId)
                        .eq(NGbCtDesignProgress::getStudentId,studentId)
                );
                break;
            case "生产线信息":
                list = gbCtLineMapper.getList(contestId, studentId);
                break;
            case "订单信息":
                List<NGbCtXzOrder> orderList = gbCtXzOrderMapper.getList(contestId, studentId);
                List<NGbCtGzIso> ctGzIsoList = nGbCtGzIsoMapper.list(contestId);
                Map<Integer, String> isoNameMap = ctGzIsoList.stream().collect(Collectors.toMap(NGbCtGzIso::getCiId, NGbCtGzIso::getCiName, (k1, k2) -> k1));
                orderList.forEach(p-> {
                    //设置ISO
                    if (p.getIsoId() == null) {
                        p.setCiName("-");
                    } else {
                        p.setCiName(isoNameMap.get(p.getIsoId()));
                    }
                }
                );
                list = orderList;

                break;
            case "现金流量表":
                list = gbCtCashflowMapper.getList(contestId, studentId);
                break;


            case "第一年PDCA":
                NGbCtPdca nGbCtPdca1 = gbActionBiz.getYearPdca(contestId,studentId,1);
                return R.success().put("data",nGbCtPdca1);


            case "第二年PDCA":
                NGbCtPdca nGbCtPdca2 = gbActionBiz.getYearPdca(contestId,studentId,2);
                return R.success().put("data",nGbCtPdca2);


            default:
                return ErrorEnum.CHOOSE_TYEP_WRONG.getR();
        }
        return R.success().put("data",list);
    }





    public R dataConsultInfo(NGbCompanyParam gbCompanyParam) {
        Integer studentId = gbCompanyParam.getStudentId();
        Integer contestId = gbCompanyParam.getContestId();
        List<?> list = new ArrayList<>();
        String type = gbCompanyParam.getType();
        switch (type) {
            case "产品库存":
                list = gbCtKcProductMapper.listKc(contestId, studentId);
                break;
            case "原料库存":
                list = gbCtKcMaterialMapper.listKcGroupNumber(contestId, studentId);

                break;
            case "生产线信息":
                list = gbCtLineMapper.getList(contestId, studentId);
                break;
            case "特性等级":
                list = nGbCtDesignProgressMapper.selectList(new LambdaQueryWrapper<NGbCtDesignProgress>()
                        .eq(NGbCtDesignProgress::getContestId,contestId)
                        .eq(NGbCtDesignProgress::getStudentId,studentId)
                );
                break;
            //查询三表 综合费用表   利润表 资产负债表
            case "综合费用表":
                //综合费用表
                list =  gbCtChargesMapper.selectList(new LambdaQueryWrapper<NGbCtCharges>()
                        .eq(NGbCtCharges::getContestId,contestId)
                        .eq(NGbCtCharges::getStudentId,studentId)
                        .eq(NGbCtCharges::getBsIsxt,1) //系统生成
                );
                break;
            case "利润表":
                //利润表
                list =  gbCtProfitChartMapper.selectList(new LambdaQueryWrapper<NGbCtProfitChart>()
                        .eq(NGbCtProfitChart::getContestId,contestId)
                        .eq(NGbCtProfitChart::getStudentId,studentId)
                        .eq(NGbCtProfitChart::getBsIsxt,1) //系统生成
                );
                break;

            case "资产负债表":
                //资产负债表
                list =  gbCtBalanceMapper.selectList(new LambdaQueryWrapper<NGbCtBalance>()
                        .eq(NGbCtBalance::getContestId,contestId)
                        .eq(NGbCtBalance::getStudentId,studentId)
                        .ne(NGbCtBalance::getBsYear,0) //除去第0年的表
                        .eq(NGbCtBalance::getBsIsxt,1) //系统生成
                );
            break;
            default:
                return ErrorEnum.CHOOSE_TYEP_WRONG.getR();
        }
        return R.success().put("list",list);
    }



    /**
     * 获取季度时间
     * @param contestId
     * @param date
     * @return
     */
    public  int getSeasonTime(Integer contestId,Integer date){
        int index = (date/10-1)*4 + date%10 -1;
        NGbContest gbContest = gbContestService.getById(contestId);
        String countDown = gbContest.getCountDown();
        JSONArray jsonArray = JSONArray.parseArray(countDown);
        return (Integer) jsonArray.get(index)*60;
    }

    /**
     * 公告下发
     * @param gbCtAnnouncement
     * @return
     */
    public R addAnnouncement(NGbCtAnnouncement gbCtAnnouncement) throws IOException {
        //获取学生表中时间最大的值
        Integer maxDate = gbContestStudentService.getMaxDate(gbCtAnnouncement.getContestId());
        //获取最大的结束时间
        Integer maxFinishDate = gbContestStudentService.getMaxFinishDate(gbCtAnnouncement.getContestId());
        //获取广告投放的最大时间
        Integer maxAdDate = gbContestStudentService.getMaxAdDate(gbCtAnnouncement.getContestId());


        //报表时间取当前进度最达到的学生的时间
        gbCtAnnouncement.setAnDate(maxDate);

        String tempFilePath = CommonUtil.getCatalinaHome() + environment.getProperty("teaching-platform.temp-excel");


        //广告
        if("1".equals(gbCtAnnouncement.getAdvertising())){
            String uniqueFileName = CommonUtil.getUniqueFileName(gbCtAnnouncement.getContestId()+maxAdDate+"所有广告");
            uniqueFileName = uniqueFileName+"--"+DateUtils.dateToCN(maxAdDate)+"广告下发";
            File excelFile = FileUtil.createExcelFile(tempFilePath, uniqueFileName);
            OutputStream out = new FileOutputStream(excelFile,true);
            gbDownloadBiz.adAll(out,gbCtAnnouncement.getContestId(),maxAdDate);
            out.close();
            gbCtAnnouncement.setAdvertising(excelFile.getName());
        }
        //财务报表
        if("1".equals(gbCtAnnouncement.getFinancial())){
            String uniqueFileName = CommonUtil.getUniqueFileName(gbCtAnnouncement.getContestId()+maxFinishDate+"所有企业报表");
            uniqueFileName = uniqueFileName+"--第"+maxFinishDate+"年企业报表";
            File excelFile = FileUtil.createExcelFile(tempFilePath, uniqueFileName);
            OutputStream out = new FileOutputStream(excelFile,true);
            gbDownloadBiz.allFinancial(out,gbCtAnnouncement.getContestId(),maxFinishDate);
            out.close();
            gbCtAnnouncement.setFinancial(excelFile.getName());
        }
        //一键下载
        if("1".equals(gbCtAnnouncement.getAllData())){
            String uniqueFileName = CommonUtil.getUniqueFileName(gbCtAnnouncement.getContestId()+gbCtAnnouncement.getAnDate()+"所有竞赛数据");
            uniqueFileName = uniqueFileName+"--"+DateUtils.dateToCN(maxDate)+"一键下载";
            File zipFile = FileUtil.createZipFile(tempFilePath, uniqueFileName);
            List<Integer> studentIdList = gbContestStudentService.getStudentIdList(gbCtAnnouncement.getContestId());
            Map<Integer, String> studentIdAndAccountMap = gbContestStudentService.getStudentIdAndAccountMap(gbCtAnnouncement.getContestId());
            //临时文件夹
            String uniqueDir = tempFilePath + CommonUtil.getUniqueFileName(gbCtAnnouncement.getContestId()+maxDate+"") + "/";
            List<String> filesNames = new ArrayList<>();
            List<File> files = new ArrayList<>();
            for (Integer studentId : studentIdList) {
                String fileName = studentIdAndAccountMap.get(studentId)+ ExcelTypeEnum.XLSX.getValue();;
                gbDownloadBiz.saveResultNoCashFlowToFile(gbCtAnnouncement.getContestId(), studentId,fileName,uniqueDir);
                filesNames.add(fileName);
                File temp = new File(uniqueDir+fileName);
                files.add(temp);
            }
            FileUtil.saveFileToZip(files,zipFile,filesNames);
            files.forEach(File::delete);
            gbCtAnnouncement.setAllData(zipFile.getName());
        }
        gbCtAnnouncement.setCreateTime(LocalDateTime.now());
        gbCtAnnouncementMapper.insert(gbCtAnnouncement);
        return R.success();
    }


    /**
     * 获取公告信息
     * @param contestId
     * @return
     */
    public R getAnnouncement(Integer contestId) {
        QueryWrapper<NGbCtAnnouncement> qw = new QueryWrapper<>();
        qw.eq("contest_id",contestId);
        List<NGbCtAnnouncement> gbCtAnnouncements = gbCtAnnouncementMapper.selectList(qw);
        return R.success().put("list",gbCtAnnouncements);
    }

//    /**
//     * 生成复盘数据
//     * @param contestId
//     * @return
//     */
//    public void generateSubject(HttpServletResponse response,Integer contestId,Integer subjectNumber) {
//        gbDownloadBiz.downAutoSubjectModelFile(response,contestId,subjectNumber);
//    }


    /***
     * 获取可还原时间
     * @param contestId
     * @param studentId
     * @return
     */
    public R getRestoreTime(Integer contestId, Integer studentId) {
        List<Integer> restoreTime = gbContestStudentMapper.getRestoreTime(contestId, studentId);
        NGbContest contest = gbContestService.getById(contestId);

        //获取最后一次的 选单记录
        NGbCtXzOrder gbCtXzOrder= gbCtXzOrderMapper.selectOne(new LambdaQueryWrapper<NGbCtXzOrder>()
                .eq(NGbCtXzOrder::getContestId,contestId)
                .orderByDesc(NGbCtXzOrder::getDate)
                .orderByDesc(NGbCtXzOrder::getQuarterly)
                .last("limit 1")
        );

        //计算最后一次订货会的时间
        Integer lastFairOrderDate = 11;
        if(gbCtXzOrder!=null){
            lastFairOrderDate = gbCtXzOrder.getDate()*10+gbCtXzOrder.getQuarterly();
        }



        //获取最后一次的零售市场
        NGbCtRetailAssign nGbCtRetailAssign = nGbCtRetailAssignService.getOne(new LambdaQueryWrapper<NGbCtRetailAssign>()
                .eq(NGbCtRetailAssign::getContestId,contestId)
                .orderByDesc(NGbCtRetailAssign::getYear)
                .orderByDesc(NGbCtRetailAssign::getQuarterly)
                .last("limit 1")
        );

        //计算最后一次零售分配的时间
        Integer lastRetailDate = 11;
        if(nGbCtRetailAssign!=null){
            lastRetailDate = nGbCtRetailAssign.getYear()*10+nGbCtRetailAssign.getQuarterly();
        }


        for (int i = restoreTime.size() - 1; i >= 0; i--) {
            //1 如果发生的订货会，那么学生不能还原到订货会之前的时间点
            //2 如果发生了零售市场分配，同样学生不能还原到 最后一次零售分配的时间以及其之前的时间点
            if((restoreTime.get(i) < lastFairOrderDate) || (restoreTime.get(i) <= lastRetailDate) ){
                restoreTime.remove(i);
            }
        }
        return R.success().put("list",restoreTime);
    }


    /**
     * 获取市场调研结果
     * @param contestId
     * @return
     */
    public R marketForecast(Integer contestId) {
        List<NGbMarketForecast> table = gbContestMapper.marketForecast(contestId);
        Map<String, List<NGbMarketForecast>> listMap = table.stream().collect(Collectors.groupingBy(NGbMarketForecast::getDate));
        List<Map<String, Object>> list = new ArrayList<>();
        listMap.forEach((k, v) -> {
            Map<String, Object> map = new HashMap<>();
            String a;
            map.put("date", k);
            map.put("dataList", v);
            list.add(map);
        });

        //排序
        list.sort(new Comparator<Map<String, Object>>() {
            @Override
            public int compare(Map<String, Object> o1, Map<String, Object> o2) {
                String date1 = (String)o1.get("date");
                String date2 = (String)o2.get("date");
                return date1.compareTo(date2);
            }
        });

        return R.success().put("list", list);
    }


    /**
     * 获取各个小组的累计广告投放金额
     * @param contestId
     * @return
     */
    public R bigDataAdTotal(Integer contestId) {

        //获取各个小组的累计广告投放金额
        List<NGbBigDataAdTotalVo> vos  = gbCtMnAdMapper.getAllAdTotal(contestId);


        return R.success().put("list",vos);
    }


    @Autowired
    private NGbCtAvgSalMapper nGbCtAvgSalMapper;

    /**
     * 查询当前季度的平均工资
     * @param contestId
     * @param currentTime
     * @return
     */
    public R bigDataAverageSal(Integer contestId, Integer currentTime) {
        List<NGbBigDataAvgSalVo> vos  = nGbCtAvgSalMapper.getAverageSal(contestId,currentTime);
        return R.success().put("list",vos);
    }

    /**
     * 查询各个小组的生产线数量
     * @param contestId
     * @param currentTime
     * @return
     */
    public R bigDataLineNum(Integer contestId, Integer currentTime) {
        List<NGbBigDataLineNumVo> vos  = gbCtLineMapper.bigDataLineNum(contestId,currentTime);
        return R.success().put("list",vos);
    }

    public R bigDataSumSaleAmount(Integer contestId, Integer currentTime) {
        List<NGbBigDataSaleAmountVo> vos  = nGbCtAvgSalMapper.bigDataSumSaleAmount(contestId,currentTime);
        return R.success().put("list",vos);
    }
}
