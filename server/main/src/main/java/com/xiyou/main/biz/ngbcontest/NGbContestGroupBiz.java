package com.xiyou.main.biz.ngbcontest;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.xiyou.common.enums.CodeEnum;
import com.xiyou.common.utils.R;
import com.xiyou.main.entity.ngbcontest.NGbContestGroup;
import com.xiyou.main.service.ngbcontest.NGbContestGroupService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @program: multi-module
 * @description:
 * @author: tangcan
 * @create: 2019-07-09 12:22
 **/
@Service
@Transactional(rollbackFor = Exception.class)
public class NGbContestGroupBiz {


    @Autowired
    private NGbContestGroupService ngbContestGroupService;

    public R list() {
        Map<String, Object> returnMap = new HashMap<>();
        returnMap.put("list", ngbContestGroupService.list());
        return R.success(returnMap);
    }

    public R add(NGbContestGroup contestGroup) {
        QueryWrapper<NGbContestGroup> wrapper = new QueryWrapper<>();
        wrapper.eq("group_name", contestGroup.getGroupName());
        if (ngbContestGroupService.getOne(wrapper) != null) {
            return R.error(CodeEnum.PARAM_ERROR.getCode(), "该分组已存在");
        }
        ngbContestGroupService.insert(contestGroup);
        NGbContestGroup group = ngbContestGroupService.getOne(wrapper);
        Map<String, Object> returnMap = new HashMap<>();
        returnMap.put("id", group.getId());
        return R.success(returnMap);
    }

    public R update(NGbContestGroup contestGroup) {
        if (ngbContestGroupService.checkNameExist(contestGroup)) {
            return R.error(CodeEnum.PARAM_ERROR.getCode(), "该分组已存在");
        }
        ngbContestGroupService.updateById(contestGroup);
        return R.success();
    }

    public R delete(Integer[] ids) {
        ngbContestGroupService.removeByIds(Arrays.asList(ids));
        return R.success();
    }

    public R listByteacher(Integer teacherId) {
        List<NGbContestGroup> list = ngbContestGroupService.getListByTeacher(teacherId);
        return R.success().put("list", list);
    }
}
