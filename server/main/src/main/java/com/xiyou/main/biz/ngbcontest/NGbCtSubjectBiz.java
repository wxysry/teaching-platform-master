package com.xiyou.main.biz.ngbcontest;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.xiyou.common.enums.CodeEnum;
import com.xiyou.common.utils.R;
import com.xiyou.main.dao.ngbcontest.NGbCtSubjectMapper;
import com.xiyou.main.entity.exam.SysUser;
import com.xiyou.main.entity.ngbcontest.NGbContest;
import com.xiyou.main.entity.ngbcontest.NGbCtSubject;
import com.xiyou.main.params.contest.SubjectParam;
import com.xiyou.main.service.exam.ResourcesService;
import com.xiyou.main.service.exam.SysUserService;
import com.xiyou.main.service.ngbcontest.NGbContestService;
import com.xiyou.main.service.ngbcontest.NGbCtSubjectService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @program: multi-module
 * @description:
 * @author: tangcan
 * @create: 2019-08-25 13:01
 **/
@Service
@Transactional(rollbackFor = Exception.class)
public class NGbCtSubjectBiz {
    @Autowired
    private NGbCtSubjectService gbCtSubjectService;
    @Autowired
    private NGbCtSubjectMapper gbCtSubjectMapper;
    @Autowired
    private ResourcesService resourcesService;

    @Autowired
    private NGbContestBiz gbContestBiz;

    @Autowired
    private NGbContestService gbContestService;



    public R listByAdmin(SubjectParam subjectParam) {
        Page<NGbCtSubject> page = gbCtSubjectService.listByAdmin(subjectParam);
        Map<String, Object> returnMap = new HashMap<>();
        returnMap.put("list", page.getRecords());
        returnMap.put("total", page.getTotal());
        return R.success(returnMap);
    }

    @Autowired
    SysUserService sysUserService;

    public R listByTeacher(SubjectParam subjectParam) {
        //判断是否是管理员，管理员可以重选所有的数据
        SysUser sysUser = sysUserService.get(new SysUser().setId(subjectParam.getTeacherId()));
        if(sysUser.getRoleId()==1){
            subjectParam.setTeacherId(null);
        }
        Page<NGbCtSubject> page = gbCtSubjectService.listByTeacher(subjectParam);
        Map<String, Object> returnMap = new HashMap<>();
        returnMap.put("list", page.getRecords());
        returnMap.put("total", page.getTotal());
        return R.success(returnMap);
    }

    public R listByTeacher(Integer teacherId,Integer groupId) {
        List<NGbCtSubject> list = gbCtSubjectService.listByTeacher(teacherId,groupId);
        return R.success().put("list", list);
    }

    public R add(NGbCtSubject gbCtSubject) {



        if(gbCtSubject.getCreateUserId() == null){
            return R.error(CodeEnum.PARAM_ERROR.getCode(), "无创建者id");
        }
        if (gbCtSubjectMapper.getBySubjectNumber(gbCtSubject.getSubjectNumber()) != null) {
            return R.error(CodeEnum.PARAM_ERROR.getCode(), "该题库号已存在");
        }
        if (gbCtSubject.getRuleAttachment() != null) {
            gbCtSubject.setRuleAttachment(gbCtSubject.getRuleAttachment().replaceAll(" ", ""));
        }
        if (StringUtils.isNotBlank(gbCtSubject.getRuleAttachment())) {
            if (!resourcesService.check(gbCtSubject.getRuleAttachment())) {
                return R.error(CodeEnum.PARAM_ERROR.getCode(), "该规则预测详单不存在");
            }
        } else {
            gbCtSubject.setRuleAttachment(null);
        }

        gbCtSubjectService.save(gbCtSubject);
        return R.success();
    }

    public R update(NGbCtSubject gbCtSubject) {
        if (gbCtSubject.getSubjectId() == null) {
            return R.error(CodeEnum.PARAM_ERROR.getCode(), "请选择题库");
        }
        NGbCtSubject subject = gbCtSubjectMapper.getBySubjectNumber(gbCtSubject.getSubjectNumber());
        if (subject != null && !subject.getSubjectId().equals(gbCtSubject.getSubjectId())) {
            return R.error(CodeEnum.PARAM_ERROR.getCode(), "该题库号已存在");
        } else if (subject == null) {
            gbCtSubject.setUpload(0);
        }

        if (gbCtSubject.getRuleAttachment() != null) {
            gbCtSubject.setRuleAttachment(gbCtSubject.getRuleAttachment().replaceAll(" ", ""));
        }
        if (StringUtils.isNotBlank(gbCtSubject.getRuleAttachment())) {
            if (!resourcesService.check(gbCtSubject.getRuleAttachment())) {
                return R.error(CodeEnum.PARAM_ERROR.getCode(), "该规则预测详单不存在");
            }
        } else {
            gbCtSubject.setRuleAttachment(null);
        }
        gbCtSubjectMapper.update(gbCtSubject);
        return R.success();
    }

    public R get(Integer subjectId) {
        Map<String, Object> returnMap = new HashMap<>();
        returnMap.put("info", gbCtSubjectMapper.getById(subjectId));
        return R.success(returnMap);
    }

    public R delete(Integer subjectId) {
        NGbCtSubject gbCtSubject = gbCtSubjectMapper.getById(subjectId);
        if (gbCtSubject == null) {
            return R.success();
        }
        gbCtSubjectService.removeById(subjectId);
        // 删除规则表中的数据
        gbCtSubjectMapper.removeGz(gbCtSubject.getSubjectNumber());


        //删除相关的比赛数据
        List<NGbContest> contestList = gbContestService.list(new LambdaQueryWrapper<NGbContest>()
                .eq(NGbContest::getSubjectNumber,gbCtSubject.getSubjectNumber())
        );
        for(NGbContest contest:contestList){
            gbContestBiz.delete(contest.getContestId());
        }

        return R.success();
    }

    public R cascadeByTeacher(Integer teacherId) {
        return R.success().put("list", gbCtSubjectMapper.getTeacherSubject(teacherId));
    }

    /**
     * 查询当前题库下的比赛数据
     * @param subjectNumber
     * @return
     */
    public R queryContest(Integer subjectNumber) {
        List<NGbContest> contestList = gbContestService.list(new LambdaQueryWrapper<NGbContest>()
                .eq(NGbContest::getSubjectNumber,subjectNumber)
        );
        String tips = contestList.stream().map(NGbContest::getTitle).collect(Collectors.joining("/"));
        return R.success().put("data",tips);
    }
}
