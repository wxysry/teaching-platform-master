package com.xiyou.main.biz.ngbcontest;


import com.xiyou.main.dao.ngbcontest.NGbCtSubjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author: zhengxiaodong
 * @description: P9当季开始
 * @since: 2019-07-24 14:51:52
 **/
@Service
@Transactional(rollbackFor = Exception.class)
public class NGbCurrentQuarterBiz {


    @Autowired
    NGbCtSubjectMapper ctSubjectMapper;
    @Autowired
    NGbRestoreDataBiz restoreDataBiz;

//    /**
//     * @Description: 当季开始
//     * @Author:zhengxiaodong
//     * @Param: [cashFolowEntity]
//     * @return: com.xiyou.common.utils.R
//     */
//    public R currentQuarterBegin(CashFolowEntity cashFolowEntity) {
//
//        // 现金流量表当前现金
//        Integer currentMoney = ctCashflowService.getCash(cashFolowEntity.getStudentId(), cashFolowEntity.getContestId());
//        currentMoney = currentMoney == null ? 0 : currentMoney;
//        // （bank_loan表中贷款类型为2且归还时间=（当前时间+1）的金额）
//        Integer fee = ctBankLoanMapper.getFee(gb CashFolowEntity()
//                .setStudentId(cashFolowEntity.getStudentId())
//                .setContestId(cashFolowEntity.getContestId())
//                .setBlType(2)
//                .setCurrentTime(cashFolowEntity.getCurrentTime()));
//        // gz表中Short_Loan_Interests
//        Integer shortLoanInterests = ctGzCsMapper.getShortLoanInterests(cashFolowEntity);
//        shortLoanInterests = shortLoanInterests == null ? 0 : shortLoanInterests;
//        // 获取运行结果
//        if ((currentMoney - Math.round(fee * (100 + shortLoanInterests) / 100.0)) < 0) {
//            return R.error(CodeEnum.OTHER_ERROR.getCode(), "现金不足，无法开始");
//        }
//        // 更新time表
//        ctTTimeService.updateCtime(cashFolowEntity);
//        // 现金流量表新增记录一
//        // bank_loan表中贷款类型为2且归还时间=当前时间的金额
//        Integer currentBankLoan = ctBankLoanMapper.getFee(gb CashFolowEntity()
//                .setStudentId(cashFolowEntity.getStudentId())
//                .setContestId(cashFolowEntity.getContestId())
//                .setBlType(2)
//                .setCurrentTime(cashFolowEntity.getCurrentTime()));
//        CtCashflow cashFolowOne = gb CtCashflow();
//        cashFolowOne.setCAction("归还短期贷款")
//                .setCIn(0)
//                .setCOut(currentBankLoan)
//                .setCSurplus(currentMoney - currentBankLoan)
//                .setCComment("归还短期贷款" + currentBankLoan + "W")
//                .setCDate(cashFolowEntity.getCurrentTime())
//                .setStudentId(cashFolowEntity.getStudentId())
//                .setContestId(cashFolowEntity.getContestId());
//        ctCashflowService.save(cashFolowOne);
//
//
//        // 现金流量表新增记录二
//        // 【（bank_loan表中贷款类型为2且归还时间=当前时间的金额）*gz_cs中的Short_Loan_Interests/100】四舍五入取整
//        long interest = Math.round(currentBankLoan *shortLoanInterests/100.0);
//        CtCashflow cashFolowTwo = gb CtCashflow();
//        cashFolowTwo.setCAction("归还短期贷款利息")
//                .setCIn(0)
//                .setCOut((int) interest)
//                .setCSurplus((int) (currentMoney - currentBankLoan - interest))
//                .setCComment("归还短期贷款利息" + interest + "W")
//                .setCDate(cashFolowEntity.getCurrentTime())
//                .setStudentId(cashFolowEntity.getStudentId())
//                .setContestId(cashFolowEntity.getContestId());
//        ctCashflowService.save(cashFolowTwo);
//
//        // 更新产成品表，如果生产线表剩余生产时间为0，则在产成品表中update库存数量=生产线.生产产品相同的+1，如果有多条则增加多个
//        ctKcProductMapper.updateKcNum(cashFolowEntity.getStudentId(), cashFolowEntity.getContestId());
//        // 更新生产线表，如果剩余生产时间为0，则开产时间、剩余生产时间置空，如剩余生产时间>0,则开产时间不变，剩余生产时间=原剩余生产时间-1
//        ctLineMapper.updateProductAddDateAndProductingDate(cashFolowEntity.getStudentId(), cashFolowEntity.getContestId());
//        // 更新生产线表，如果在建剩余时间为0，则置空；转产剩余时间为0，则置空剩余时间及转产时间，如>0，则-1
//        ctLineMapper.updateRemainDateAndTransferAddDate(cashFolowEntity.getStudentId(), cashFolowEntity.getContestId(), cashFolowEntity.getCurrentTime());
//        // 更新bank_loan表，删除还款时间等于当前时间的记录
//        QueryWrapper<CtBankLoan> wrapper = gb QueryWrapper<>();
//        wrapper.eq("bl_repayment_date", cashFolowEntity.getCurrentTime())
//                .eq("student_id", cashFolowEntity.getStudentId())
//                .eq("contest_id", cashFolowEntity.getContestId())
//                .eq("bl_type", 2);
//        ctBankLoanService.remove(wrapper);
//
//        //清除季度备份表数据 重新备份
//        restoreDataBiz.backupSeasonData(cashFolowEntity.getContestId(),cashFolowEntity.getStudentId());
//        return R.success();
//    }

}
