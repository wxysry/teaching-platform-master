package com.xiyou.main.biz.ngbcontest;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.xiyou.common.enums.CodeEnum;
import com.xiyou.common.exception.CustomException;
import com.xiyou.common.utils.R;
import com.xiyou.main.dao.ngbcontest.NGbContestMapper;
import com.xiyou.main.dao.ngbcontest.NGbCtGzMarketMapper;
import com.xiyou.main.entity.exam.SysUser;
import com.xiyou.main.entity.ngbcontest.NGbContest;
import com.xiyou.main.entity.ngbcontest.NGbCtCashflow;
import com.xiyou.main.entity.ngbcontest.NGbCtGzMarket;
import com.xiyou.main.entity.ngbcontest.NGbCtMnAd;
import com.xiyou.main.enums.ErrorEnum;
import com.xiyou.main.service.exam.SysUserService;
import com.xiyou.main.service.ngbcontest.NGbContestStudentService;
import com.xiyou.main.service.ngbcontest.NGbCtCashflowService;
import com.xiyou.main.service.ngbcontest.NGbCtMnAdService;
import com.xiyou.main.service.ngbcontest.NGbCtYfMarketService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @program: multi-module
 * @description:
 * @author: tangcan
 * @create: 2019-07-25 15:03
 **/
@Service
@Slf4j
@Transactional(rollbackFor = Exception.class)
public class NGbDeliverAdsBiz {
    @Autowired
    private NGbCtYfMarketService gbCtYfMarketService;
    @Autowired
    private NGbCtMnAdService nGbCtMnAdService;
    @Autowired
    private NGbCtCashflowService gbCtCashflowService;

    @Autowired
    private SysUserService sysUserService;

    @Autowired
    private NGbActionBiz gbActionBiz;

    @Autowired
    private NGbCtGzMarketMapper nGbCtGzMarketMapper;

    @Autowired
    private NGbContestMapper nGbContestMapper;


    //已研发完成的市场列表
    public R finishYfMarket(Integer studentId, Integer contestId) {
        List<String> hadYfFinishMarketList = gbCtYfMarketService.getHadYfFinishMarkets(studentId, contestId);
        Map<String, Object> returnMap = new HashMap<>();
        returnMap.put("marketList", hadYfFinishMarketList);
        return R.success(returnMap);
    }



    public R deliverAds(Integer studentId, NGbCtMnAd mnAd) {

        //阻止暂停状态下 和 破产状态下的用户进行操作
        gbActionBiz.preventAction(mnAd.getContestId(),studentId);
        
        mnAd.setStudentId(studentId);
        Integer year = mnAd.getYear();
        Integer quarterly = mnAd.getQuarterly();
        Integer currentTime = year*10 + quarterly;
        //先校验投放广告的金额
        Integer  amount = mnAd.getAmount();
        if(amount<=0){
            throw new CustomException(CodeEnum.OTHER_ERROR.getCode(), "广告投放金额不能小于或等于0");
        }
        //判断当前是否在订单申报阶段,申报阶段不允许投放广告
        NGbContest nGbContest = nGbContestMapper.selectById(mnAd.getContestId());
        if(nGbContest.getFairOrderState()==1){
            throw new CustomException(CodeEnum.OTHER_ERROR.getCode(), "当前处于订单申报阶段，不允许投放广告");
        }

        int cash = gbCtCashflowService.getCash(mnAd.getStudentId(), mnAd.getContestId());
        if (amount > cash ) {
            return R.error(CodeEnum.OTHER_ERROR.getCode(), "现金不足");
        }
        // Cashflow表写入ID，支付广告费，0，所有填写金额之和，现金+流入-流出，投放广告费，当前时间
        gbCtCashflowService.save(new NGbCtCashflow().setStudentId(studentId)
                .setContestId(mnAd.getContestId())
                .setCAction("支付广告费")
                .setCIn(0)
                .setCOut(amount)
                .setCSurplus(cash - amount)
                .setCComment(mnAd.getCmName()+"市场投放广告费" + (int) amount + "元")
                .setCDate(currentTime));


        //广告费投放存表
        //判断当前市场当前季度是否已经投放了广告
        NGbCtMnAd nGbCtMnAd  = nGbCtMnAdService.getOne(new LambdaQueryWrapper<NGbCtMnAd>()
                .eq(NGbCtMnAd::getContestId,mnAd.getContestId())
                .eq(NGbCtMnAd::getStudentId,mnAd.getStudentId())
                .eq(NGbCtMnAd::getYear,year)
                .eq(NGbCtMnAd::getQuarterly,quarterly)
                .eq(NGbCtMnAd::getCmId,mnAd.getCmId())
        );
        if(nGbCtMnAd==null){
            nGbCtMnAdService.save(mnAd);
        }else{
            //投入金额累加
            nGbCtMnAd.setAmount(nGbCtMnAd.getAmount()+amount);
            nGbCtMnAdService.updateById(nGbCtMnAd);
        }





        //计算报表,判断是否破产
        gbActionBiz.isBankruptcy(studentId, mnAd.getContestId(), currentTime);
        return R.success();
    }



    /**
     * 获取当前学生本季度投放的广告
     * @param contestId
     * @param userId
     * @param currentTime
     * @return
     */
    public R getFillAd(Integer contestId, Integer userId, Integer currentTime) {
//        SysUser sysUser = sysUserService.getById(userId);
        List<NGbCtMnAd> resultList = new ArrayList<>();
        NGbContest nGbContest = nGbContestMapper.selectById(contestId);
        List<NGbCtGzMarket> nGbCtGzMarketList  = nGbCtGzMarketMapper.selectList(new LambdaQueryWrapper<NGbCtGzMarket>()
                .eq(NGbCtGzMarket::getSubjectNumber,nGbContest.getSubjectNumber())
        );
        for (NGbCtGzMarket nGbCtGzMarket : nGbCtGzMarketList) {
            //获取当前市场各个学生广告投放的排名情况
            List<NGbCtMnAd> nGbCtMnAds = nGbCtMnAdService.getAdRank(contestId,currentTime/10,currentTime%10,nGbCtGzMarket.getCmId(),nGbCtGzMarket.getCmName());
            //过滤当前该学生的排名
            NGbCtMnAd mnAd = nGbCtMnAds.stream().filter(item->userId.equals(item.getStudentId())).collect(Collectors.toList()).get(0);
            resultList.add(mnAd);
        }
        return R.success().put("list",resultList);
    }
}
