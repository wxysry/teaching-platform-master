package com.xiyou.main.biz.ngbcontest;

import com.alibaba.excel.ExcelWriter;
import com.alibaba.excel.metadata.Font;
import com.alibaba.excel.metadata.Sheet;
import com.alibaba.excel.metadata.Table;
import com.alibaba.excel.metadata.TableStyle;
import com.alibaba.excel.support.ExcelTypeEnum;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.xiyou.common.office.utils.EasyPOIUtil;
import com.xiyou.common.office.utils.FileUtil;
import com.xiyou.common.utils.CommonUtil;
import com.xiyou.main.async.ngbcontest.NGbContestResultAsyncService;
import com.xiyou.main.dao.ngbcontest.*;
import com.xiyou.main.entity.ngbcontest.*;
import com.xiyou.main.service.ngbcontest.NGbContestStudentService;
import com.xiyou.main.service.ngbcontest.NGbCtMnAdService;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.util.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.net.URLEncoder;
import java.util.*;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.stream.Collectors;

/**
 * @program: multi-module
 * @description:
 * @author: tangcan
 * @create: 2019-08-28 14:23
 **/
@Service
@Transactional(rollbackFor = Exception.class)
public class NGbDownloadBiz {

    @Autowired
    private NGbCtMnAdService gbCtMnAdService;
    @Autowired
    private NGbContestStudentMapper contestStudentMapper;
    @Autowired
    private NGbContestResultAsyncService contestResultAsyncService;
    @Autowired
    private NGbCtMnAdMapper gbCtMnAdMapper;
    @Autowired
    private NGbCtBalanceMapper gbCtBalanceMapper;
    @Autowired
    private NGbCtProfitChartMapper gbCtProfitChartMapper;
    @Autowired
    private NGbCtChargesMapper gbCtChargesMapper;
    @Autowired
    private NGbContestStudentService gbContestStudentService;
    @Autowired
    private NGbCtGzCsMapper gbCtGzCsMapper;
    @Autowired
    private NGbCtGzAdMapper gbCtGzAdMapper;
    @Autowired
    private NGbCtGzIsoMapper gbCtGzIsoMapper;
    @Autowired
    private NGbCtGzOrderMapper gbCtGzOrderMapper;
    @Autowired
    private NGbCtGzMarketMapper gbCtGzMarketMapper;
    @Autowired
    private NGbCtGzMaterialMapper gbCtGzMaterialMapper;
    @Autowired
    private NGbCtGzProductMapper gbCtGzProductMapper;
    @Autowired
    private NGbCtGzProducingMapper gbCtGzProducingMapper;
    @Autowired
    private NGbCtGzProductLineMapper gbCtGzProductLineMapper;
    @Autowired
    private NGbCtGzProductDesignMapper gbCtGzProductDesignMapper;
    @Autowired
    private NGbCtGzWorkerRecruitMapper gbCtGzWorkerRecruitMapper;
    @Autowired
    private NGbCtGzWorkerTrainMapper gbCtGzWorkerTrainMapper;
    @Autowired
    private NGbCtGzLoanMapper gbCtGzLoanMapper;
    @Autowired
    private NGbCtGzDiscountMapper gbCtGzDiscountMapper;
    @Autowired
    private NGbCtGzClassesMapper gbCtGzClassesMapper;
    @Autowired
    private NGbCtGzWorkerIncentiveMapper gbCtGzWorkerIncentiveMapper;
    @Autowired
    private NGbCtGzNumMapper gbCtGzNumMapper;
    @Autowired
    private NGbContestMapper gbContestMapper;
    @Autowired
    private NGbCtMnChooseMapper gbCtMnChooseMapper;
    @Autowired
    Environment environment;


    /***
     * 字符转换
     * @param localP1
     * @return
     */
    private String convertToString(Integer localP1) {
        return localP1 == null ? "0" : String.valueOf((int) Math.floor(localP1));
    }

//    /***
//     * 下载学生竞赛结果
//     * @param response
//     * @param contestId
//     * @param studentId
//     */
//    public void downloadResult(HttpServletResponse response, Integer contestId, Integer studentId,String fileName) {
//        NGbContestStudent contestStudent = contestStudentMapper.get(contestId, studentId);
//        if (contestStudent == null) {
//            return;
//        }
//
//        Integer date = contestStudent.getDate() == null ? null : (contestStudent.getDate() / 10);
//
//        // 库存信息
//        Map<String, Object> kcxx;
//        Future<Map<String, Object>> kcxxFuture = contestResultAsyncService.getKCXX(contestId, studentId);
//
//        // 银行贷款
//        Map<String, Object> yhdk;
//        Future<Map<String, Object>> yhdkFuture = contestResultAsyncService.getYHDK(contestId, studentId, date);
//
//        // 研发认证
//        Map<String, Object> yfrz;
//        Future<Map<String, Object>> yfrzFuture = contestResultAsyncService.getYFRZ(contestId, studentId);
//
//        // 厂房与生产线
//        Map<String, Object> cfyscx;
//        Future<Map<String, Object>> cfyscxFuture = contestResultAsyncService.getCFYSCX(contestId, studentId);
//
//        // 订单信息
//        Map<String, Object> ddxx;
//        Future<Map<String, Object>> ddxxFuture = contestResultAsyncService.getDDXX(contestId, studentId, date);
//
//        // 现金流量表
//        Map<String, Object> xjllb;
//        Future<Map<String, Object>> xjllbFuture = contestResultAsyncService.getXJLLB(contestId, studentId);
//
//        // 企业财务报表
//        Map<String, Object> qycwbb;
//        Future<Map<String, Object>> qycwbbFuture = contestResultAsyncService.getQYCWBB(contestId, studentId);
//
////        // 四年广告投放
////        Map<String, Object> ggtf;
////        Future<Map<String, Object>> ggtfFuture = contestResultAsyncService.getGGTF(contestId, studentId);
//
//        // 所有异步任务执行完成
//        while (true) {
//            if (kcxxFuture.isDone() &&
//                    yhdkFuture.isDone() &&
//                    yfrzFuture.isDone() &&
//                    cfyscxFuture.isDone() &&
//                    ddxxFuture.isDone() &&
//                    xjllbFuture.isDone() &&
//                    qycwbbFuture.isDone()
////                    && ggtfFuture.isDone()
//            ) {
//                break;
//            }
//        }
//
//        // 获取异步任务返回的数据
//        try {
//            kcxx = kcxxFuture.get();
//            yhdk = yhdkFuture.get();
//            cfyscx = cfyscxFuture.get();
//            ddxx = ddxxFuture.get();
//            yfrz = yfrzFuture.get();
//            xjllb = xjllbFuture.get();
//            qycwbb = qycwbbFuture.get();
////            ggtf = ggtfFuture.get();
//        } catch (InterruptedException | ExecutionException e) {
//            e.printStackTrace();
//            return;
//        }
//
//        /*
//        数据封装
//         */
//        Map<String, Object> param = new HashMap<>();
//        param.putAll(kcxx);
//        param.putAll(yhdk);
//        param.putAll(yfrz);
//        param.putAll(cfyscx);
//        param.putAll(ddxx);
//        param.putAll(xjllb);
//        param.putAll(qycwbb);
////        param.putAll(ggtf);
//
//        String filePath = CommonUtil.getClassesPath() + "templates/ngb_contest_result.xlsx";
//        Map<Integer, String> studentIdAndAccountMap = gbContestStudentService.getStudentIdAndAccountMap(contestId);
//        if(fileName == null){
//            fileName = "竞赛数据-" + studentIdAndAccountMap.get(studentId) + ".xlsx";
//        }else {
//            fileName += "-" + studentIdAndAccountMap.get(studentId) + ".xlsx";
//        }
//        try {
//            EasyPOIUtil.exportMultiSheetExcelByMap(response, fileName + ".xlsx", filePath, param, true);
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//    }


    /***
     * 企业报表
     * @param outputStream
     * @param contestId
     * @param year
     */
    public void allFinancial(OutputStream outputStream,Integer contestId, Integer year) {
        ExcelWriter writer = new ExcelWriter(outputStream, ExcelTypeEnum.XLSX);
        Map<Integer, String> studentIdAndAccountMap = gbContestStudentService.getStudentIdAndAccountMap(contestId);
        /*
        获取综合费用表
         */

        {
            List<NGbCtCharges> gbCtChargesList = gbCtChargesMapper.getListByContestId(contestId, year);//综合费用表
            if (gbCtChargesList.size() == 0) {
                gbCtChargesList.add(new NGbCtCharges());
            }
            // 设置每列宽度
            Map<Integer, Integer> columnWidth = new HashMap<>();
            for (int i = 0; i <= gbCtChargesList.size(); i++) {
                columnWidth.put(i, 4000);
            }

            Sheet sheet = new Sheet(1, 0);
            sheet.setColumnWidthMap(columnWidth);
            sheet.setSheetName("综合费用表");
            sheet.setStartRow(1);   // 前面空一行

            /*
            表格式
             */
            TableStyle tableStyle = new TableStyle();
            Font font = new Font();
            font.setFontName("Arial");
            font.setFontHeightInPoints((short) 10);
            tableStyle.setTableHeadFont(font);
            tableStyle.setTableHeadBackGroundColor(IndexedColors.GREY_25_PERCENT);
            tableStyle.setTableContentFont(font);
            tableStyle.setTableContentBackGroundColor(IndexedColors.WHITE);

            Table table1 = new Table(1);
            String title = "综合费用表";
            List<List<String>> head = new ArrayList<>();
            List<String> headCoulumn = new ArrayList<>();
            headCoulumn.add(title);
            headCoulumn.add("组号");
            head.add(headCoulumn);
            for (NGbCtCharges gbCtCharges : gbCtChargesList) {
                List<String> headCoulumn1 = new ArrayList<>();
                headCoulumn1.add(title);
                headCoulumn1.add(studentIdAndAccountMap.get(gbCtCharges.getStudentId()));
                head.add(headCoulumn1);
            }
            table1.setHead(head);
            table1.setTableStyle(tableStyle);

            List<List<String>> data = new ArrayList<>();
            List<String> data1 = new ArrayList<>();
            data.add(data1);
            data1.add("管理费");
            data1.addAll(gbCtChargesList.stream().map(k->convertObjToString(k.getCOverhaul())).collect(Collectors.toList()));
            List<String> data2 = new ArrayList<>();
            data.add(data2);
            data2.add("广告费");
            data2.addAll(gbCtChargesList.stream().map(k->convertObjToString(k.getCAd())).collect(Collectors.toList()));
            List<String> data3 = new ArrayList<>();
            data.add(data3);
            data3.add("产线维修费");
            data3.addAll(gbCtChargesList.stream().map(k->convertObjToString(k.getCMaintenance())).collect(Collectors.toList()));
            List<String> data4 = new ArrayList<>();
            data.add(data4);
            data4.add("转产费");
            data4.addAll(gbCtChargesList.stream().map(k->convertObjToString(k.getCTransfer())).collect(Collectors.toList()));
            List<String> data5 = new ArrayList<>();
            data.add(data5);
            data5.add("市场开拓费");
            data5.addAll(gbCtChargesList.stream().map(k->convertObjToString(k.getCDevelopMarket())).collect(Collectors.toList()));
            List<String> data6 = new ArrayList<>();
            data.add(data6);
            data6.add("产品资质申请");
            data6.addAll(gbCtChargesList.stream().map(k->convertObjToString(k.getCDevelopProduct())).collect(Collectors.toList()));
            List<String> data7 = new ArrayList<>();
            data.add(data7);
            data7.add("ISO认证申请");
            data7.addAll(gbCtChargesList.stream().map(k->convertObjToString(k.getCDevelopIso())).collect(Collectors.toList()));


            List<String> data8 = new ArrayList<>();
            data.add(data8);
            data8.add("信息费");
            data8.addAll(gbCtChargesList.stream().map(k->convertObjToString(k.getCInformation())).collect(Collectors.toList()));



            List<String> data9 = new ArrayList<>();
            data.add(data9);
            data9.add("产品设计费");
            data9.addAll(gbCtChargesList.stream().map(k->convertObjToString(k.getCProductDesign())).collect(Collectors.toList()));


            List<String> data10 = new ArrayList<>();
            data.add(data10);
            data10.add("辞退福利");
            data10.addAll(gbCtChargesList.stream().map(k->convertObjToString(k.getCDismissFee())).collect(Collectors.toList()));



            List<String> data11 = new ArrayList<>();
            data.add(data11);
            data11.add("培训费");
            data11.addAll(gbCtChargesList.stream().map(k->convertObjToString(k.getCTrainFee())).collect(Collectors.toList()));


            List<String> data12 = new ArrayList<>();
            data.add(data12);
            data12.add("激励费");
            data12.addAll(gbCtChargesList.stream().map(k->convertObjToString(k.getCIncentiveFee())).collect(Collectors.toList()));


            List<String> data13 = new ArrayList<>();
            data.add(data13);
            data13.add("人力费");
            data13.addAll(gbCtChargesList.stream().map(k->convertObjToString(k.getCHr())).collect(Collectors.toList()));


            List<String> data14 = new ArrayList<>();
            data.add(data14);
            data14.add("碳中和费用");
            data14.addAll(gbCtChargesList.stream().map(k->convertObjToString(k.getCCarbon())).collect(Collectors.toList()));

            List<String> data15 = new ArrayList<>();
            data.add(data15);
            data15.add("特性研发");
            data15.addAll(gbCtChargesList.stream().map(k->convertObjToString(k.getCFeature())).collect(Collectors.toList()));


            List<String> data16 = new ArrayList<>();
            data.add(data16);
            data16.add("数字化研发费");
            data16.addAll(gbCtChargesList.stream().map(k->convertObjToString(k.getCDigitalization())).collect(Collectors.toList()));


            List<String> data17 = new ArrayList<>();
            data.add(data17);
            data17.add("合计");
            data17.addAll(gbCtChargesList.stream().map(k->convertObjToString(k.getCTotal())).collect(Collectors.toList()));


            writer.write0(data, sheet, table1);
        }
        /*
        获取利润表
         */
        {
            List<NGbCtProfitChart> gbCtProfitChartsList = gbCtProfitChartMapper.getListByContestId(contestId, year);//利润表
            if (gbCtProfitChartsList.size() == 0) {
                gbCtProfitChartsList.add(new NGbCtProfitChart());
            }
            // 设置每列宽度
            Map<Integer, Integer> columnWidth = new HashMap<>();
            for (int i = 0; i < gbCtProfitChartsList.size(); i++) {
                columnWidth.put(i, 4000);
            }

            Sheet sheet = new Sheet(2, 0);
            sheet.setColumnWidthMap(columnWidth);
            sheet.setSheetName("利润表");
            sheet.setStartRow(1);   // 前面空一行

            /*
            表格式
             */
            TableStyle tableStyle = new TableStyle();
            Font font = new Font();
            font.setFontName("Arial");
            font.setFontHeightInPoints((short) 10);
            tableStyle.setTableHeadFont(font);
            tableStyle.setTableHeadBackGroundColor(IndexedColors.GREY_25_PERCENT);
            tableStyle.setTableContentFont(font);
            tableStyle.setTableContentBackGroundColor(IndexedColors.WHITE);

            Table table1 = new Table(1);
            String title = "利润表";
            List<List<String>> head = new ArrayList<>();
            List<String> headCoulumn = new ArrayList<>();
            headCoulumn.add(title);
            headCoulumn.add("组号");
            head.add(headCoulumn);
            for (NGbCtProfitChart gbCtProfitChart : gbCtProfitChartsList) {
                List<String> headCoulumn1 = new ArrayList<>();
                headCoulumn1.add(title);
                headCoulumn1.add(studentIdAndAccountMap.get(gbCtProfitChart.getStudentId()));
                head.add(headCoulumn1);
            }
            table1.setHead(head);
            table1.setTableStyle(tableStyle);

            List<List<String>> data = new ArrayList<>();
            List<String> data1 = new ArrayList<>();
            data.add(data1);
            data1.add("销售收入");
            data1.addAll(gbCtProfitChartsList.stream().map(k->convertObjToString(k.getPcSales())).collect(Collectors.toList()));
            List<String> data2 = new ArrayList<>();
            data.add(data2);
            data2.add("直接成本");
            data2.addAll(gbCtProfitChartsList.stream().map(k->convertObjToString(k.getPcDirectCost())).collect(Collectors.toList()));
            List<String> data3 = new ArrayList<>();
            data.add(data3);
            data3.add("毛利");
            data3.addAll(gbCtProfitChartsList.stream().map(k->convertObjToString(k.getPcGoodsProfit())).collect(Collectors.toList()));
            List<String> data4 = new ArrayList<>();
            data.add(data4);
            data4.add("综合管理费用");
            data4.addAll(gbCtProfitChartsList.stream().map(k->convertObjToString(k.getPcTotal())).collect(Collectors.toList()));
            List<String> data5 = new ArrayList<>();
            data.add(data5);
            data5.add("折旧前利润");
            data5.addAll(gbCtProfitChartsList.stream().map(k->convertObjToString(k.getPcProfitBeforeDep())).collect(Collectors.toList()));
            List<String> data6 = new ArrayList<>();
            data.add(data6);
            data6.add("折旧");
            data6.addAll(gbCtProfitChartsList.stream().map(k->convertObjToString(k.getPcDep())).collect(Collectors.toList()));
            List<String> data7 = new ArrayList<>();
            data.add(data7);
            data7.add("支付利息前利润");
            data7.addAll(gbCtProfitChartsList.stream().map(k->convertObjToString(k.getPcProfitBeforeInterests())).collect(Collectors.toList()));
            List<String> data8 = new ArrayList<>();
            data.add(data8);
            data8.add("财务费用");
            data8.addAll(gbCtProfitChartsList.stream().map(k->convertObjToString(k.getPcFinanceFee())).collect(Collectors.toList()));
            List<String> data9 = new ArrayList<>();
            data.add(data9);
            data9.add("营业外收支");
            data9.addAll(gbCtProfitChartsList.stream().map(k->convertObjToString(k.getPcNonOperating())).collect(Collectors.toList()));
            List<String> data10 = new ArrayList<>();
            data.add(data10);
            data10.add("税前利润");
            data10.addAll(gbCtProfitChartsList.stream().map(k->convertObjToString(k.getPcProfitBeforeTax())).collect(Collectors.toList()));
            List<String> data11 = new ArrayList<>();
            data.add(data11);
            data11.add("所得税");
            data11.addAll(gbCtProfitChartsList.stream().map(k->convertObjToString(k.getPcTax())).collect(Collectors.toList()));
            List<String> data12 = new ArrayList<>();
            data.add(data12);
            data12.add("净利润");
            data12.addAll(gbCtProfitChartsList.stream().map(k->convertObjToString(k.getPcAnnualNetProfit())).collect(Collectors.toList()));


            writer.write0(data, sheet, table1);
        }
        /*
        获取资产负债表
         */

        {
            List<NGbCtBalance> gbCtBalanceList = gbCtBalanceMapper.getListByContestId(contestId,year);//资产负债表
            if (gbCtBalanceList.size() == 0) {
                gbCtBalanceList.add(new NGbCtBalance());
            }
            // 设置每列宽度
            Map<Integer, Integer> columnWidth = new HashMap<>();
            for (int i = 0; i < gbCtBalanceList.size(); i++) {
                columnWidth.put(i, 4000);
            }

            Sheet sheet = new Sheet(3, 0);
            sheet.setColumnWidthMap(columnWidth);
            sheet.setSheetName("资产负债表");
            sheet.setStartRow(1);   // 前面空一行

            /*
            表格式
             */
            TableStyle tableStyle = new TableStyle();
            Font font = new Font();
            font.setFontName("Arial");
            font.setFontHeightInPoints((short) 10);
            tableStyle.setTableHeadFont(font);
            tableStyle.setTableHeadBackGroundColor(IndexedColors.GREY_25_PERCENT);
            tableStyle.setTableContentFont(font);
            tableStyle.setTableContentBackGroundColor(IndexedColors.WHITE);


            Table table1 = new Table(1);
            String title = "资产负债表";
            List<List<String>> head = new ArrayList<>();
            List<String> headCoulumn = new ArrayList<>();
            headCoulumn.add(title);
            headCoulumn.add("组号");
            head.add(headCoulumn);
            for (NGbCtBalance gbCtBalance : gbCtBalanceList) {
                List<String> headCoulumn1 = new ArrayList<>();
                headCoulumn1.add(title);
                headCoulumn1.add(studentIdAndAccountMap.get(gbCtBalance.getStudentId()));
                head.add(headCoulumn1);
            }
            table1.setHead(head);
            table1.setTableStyle(tableStyle);

            List<List<String>> data = new ArrayList<>();
            List<String> data1 = new ArrayList<>();
            data.add(data1);
            data1.add("现金");
            data1.addAll(gbCtBalanceList.stream().map(k->convertObjToString(convertObjToString(k.getBsCash()))).collect(Collectors.toList()));
            List<String> data2 = new ArrayList<>();
            data.add(data2);
            data2.add("应收款");
            data2.addAll(gbCtBalanceList.stream().map(k->convertObjToString(convertObjToString(k.getBsReceivable()))).collect(Collectors.toList()));
            List<String> data3 = new ArrayList<>();
            data.add(data3);
            data3.add("在制品");
            data3.addAll(gbCtBalanceList.stream().map(k->convertObjToString(k.getBsProductInProcess())).collect(Collectors.toList()));
            List<String> data4 = new ArrayList<>();
            data.add(data4);
            data4.add("产成品");
            data4.addAll(gbCtBalanceList.stream().map(k->convertObjToString(k.getBsProduct())).collect(Collectors.toList()));
            List<String> data5 = new ArrayList<>();
            data.add(data5);
            data5.add("原材料");
            data5.addAll(gbCtBalanceList.stream().map(k->convertObjToString(k.getBsMaterial())).collect(Collectors.toList()));
            List<String> data6 = new ArrayList<>();
            data.add(data6);
            data6.add("流动资产合计");
            data6.addAll(gbCtBalanceList.stream().map(k->convertObjToString(k.getBsTotalCurrentAsset())).collect(Collectors.toList()));
            List<String> data7 = new ArrayList<>();
            data.add(data7);
            data7.add("机器和设备");
            data7.addAll(gbCtBalanceList.stream().map(k->convertObjToString(k.getBsEquipment())).collect(Collectors.toList()));
            List<String> data8 = new ArrayList<>();
            data.add(data8);
            data8.add("在建工程");
            data8.addAll(gbCtBalanceList.stream().map(k->convertObjToString(k.getBsProjectOnConstruction())).collect(Collectors.toList()));
            List<String> data9 = new ArrayList<>();
            data.add(data9);
            data9.add("固定资产合计");
            data9.addAll(gbCtBalanceList.stream().map(k->convertObjToString(k.getBsTotalFixedAsset())).collect(Collectors.toList()));
            List<String> data10 = new ArrayList<>();
            data.add(data10);
            data10.add("资产合计");
            data10.addAll(gbCtBalanceList.stream().map(k->convertObjToString(k.getBsTotalAsset())).collect(Collectors.toList()));
            List<String> data11 = new ArrayList<>();
            data.add(data11);
            data11.add("长期贷款");
            data11.addAll(gbCtBalanceList.stream().map(k->convertObjToString(k.getBsLongLoan())).collect(Collectors.toList()));
            List<String> data12 = new ArrayList<>();
            data.add(data12);
            data12.add("短期贷款");
            data12.addAll(gbCtBalanceList.stream().map(k->convertObjToString(k.getBsShortLoan())).collect(Collectors.toList()));
            List<String> data13 = new ArrayList<>();
            data.add(data13);
            data13.add("其他应付款");
            data13.addAll(gbCtBalanceList.stream().map(k->convertObjToString(k.getBsOtherPay())).collect(Collectors.toList()));
            List<String> data14 = new ArrayList<>();
            data.add(data14);
            data14.add("应交税费");
            data14.addAll(gbCtBalanceList.stream().map(k->convertObjToString(k.getBsTax())).collect(Collectors.toList()));
            List<String> data15 = new ArrayList<>();
            data.add(data15);
            data15.add("负债合计");
            data15.addAll(gbCtBalanceList.stream().map(k->convertObjToString(k.getBsTotalLiability())).collect(Collectors.toList()));
            List<String> data16 = new ArrayList<>();
            data.add(data16);
            data16.add("股东资本");
            data16.addAll(gbCtBalanceList.stream().map(k->convertObjToString(k.getBsEquity())).collect(Collectors.toList()));
            List<String> data17 = new ArrayList<>();
            data.add(data17);
            data17.add("利润留存");
            data17.addAll(gbCtBalanceList.stream().map(k->convertObjToString(k.getBsRetainedEarning())).collect(Collectors.toList()));
            List<String> data18 = new ArrayList<>();
            data.add(data18);
            data18.add("年度净利");
            data18.addAll(gbCtBalanceList.stream().map(k->convertObjToString(k.getBsAnnualNetProfit())).collect(Collectors.toList()));
            List<String> data19 = new ArrayList<>();
            data.add(data19);
            data19.add("所有者权益");
            data19.addAll(gbCtBalanceList.stream().map(k->convertObjToString(k.getBsTotalEquity())).collect(Collectors.toList()));
            List<String> data20 = new ArrayList<>();
            data.add(data20);
            data20.add("负债所有者权益合计");
            data20.addAll(gbCtBalanceList.stream().map(k->convertObjToString(k.getBsTotal())).collect(Collectors.toList()));

            table1.setHead(head);
            table1.setTableStyle(tableStyle);
            writer.write0(data, sheet, table1);
        }
        writer.finish();
    }

    /**
     * 获取广告投放情况保存到服务器
     * @param outputStream
     * @param contestId
     * @param date
     */
    public void adAll(OutputStream outputStream,Integer contestId, Integer date) {
        String name = "第" + date / 10 + "年"+date%10+"季广告投放";
        ExcelWriter writer = null;
        writer = new ExcelWriter(outputStream, ExcelTypeEnum.XLSX);

        /*
        获取广告投放情况
         */
        List<NGbCtMnAd> ctMnAdList = gbCtMnAdMapper.getAdListByContestId(contestId,date / 10,date % 10);
//        Map<Integer,NGbCtMnAd> dataMap = new HashMap<>();
//        for (NGbCtMnAd gbCtMnAd : ctMnAdList) {
//            dataMap.put(gbCtMnAd.getStudentId(),gbCtMnAd);
//        }
        List<Integer> studentIdList = gbContestStudentService.getStudentIdList(contestId);
        Map<Integer, String> studentIdAndAccountMap = gbContestStudentService.getStudentIdAndAccountMap(contestId);
        // 设置每列宽度
        Map<Integer, Integer> columnWidth = new HashMap<>();
        for (int i = 0; i < 6; i++) {
            columnWidth.put(i, 4000);
        }

        Sheet sheet = new Sheet(1, 0);
        sheet.setColumnWidthMap(columnWidth);
        sheet.setSheetName(name);
        sheet.setStartRow(0);   // 前面空一行

        /*
        表格式
         */
        TableStyle tableStyle = new TableStyle();
        Font font = new Font();
        font.setFontName("Arial");
        font.setFontHeightInPoints((short) 10);
        tableStyle.setTableHeadFont(font);
        tableStyle.setTableHeadBackGroundColor(IndexedColors.GREY_25_PERCENT);
        tableStyle.setTableContentFont(font);
        tableStyle.setTableContentBackGroundColor(IndexedColors.WHITE);

        // 表格数
        String[] strs = {"组号", "本地", "区域", "国内", "亚洲", "国际"};
        List<List<String>> head = new ArrayList<>();
        String title = "第" + date / 10 + "年"+date%10+"季广告投放情况";
        for (int j = 0; j < 6; j++) {
            List<String> headCoulumn = new ArrayList<>();
            // 第一行的头（名称一样则自动合并成一个单元格）
            headCoulumn.add(title);
            // 第二行的头
            headCoulumn.add(strs[j]);
            head.add(headCoulumn);
        }

        Table table1 = new Table(0);
        table1.setHead(head);
        table1.setTableStyle(tableStyle);
        List<List<String>> data = new ArrayList<>();


        //一个学生一行记录
        for (Integer studentId : studentIdList) {

            String account = studentIdAndAccountMap.get(studentId);
            //获取cmid
            List<NGbCtMnAd> nGbCtMnAdList1 = ctMnAdList.stream().filter(item-> item.getStudentId().equals(studentId) && item.getCmId()==1 ).collect(Collectors.toList());
            List<NGbCtMnAd> nGbCtMnAdList2 = ctMnAdList.stream().filter(item-> item.getStudentId().equals(studentId) && item.getCmId()==2 ).collect(Collectors.toList());
            List<NGbCtMnAd> nGbCtMnAdList3 = ctMnAdList.stream().filter(item-> item.getStudentId().equals(studentId) && item.getCmId()==3 ).collect(Collectors.toList());
            List<NGbCtMnAd> nGbCtMnAdList4 = ctMnAdList.stream().filter(item-> item.getStudentId().equals(studentId) && item.getCmId()==4 ).collect(Collectors.toList());
            List<NGbCtMnAd> nGbCtMnAdList5 = ctMnAdList.stream().filter(item-> item.getStudentId().equals(studentId) && item.getCmId()==5 ).collect(Collectors.toList());

            Integer ad1 = (nGbCtMnAdList1!=null && nGbCtMnAdList1.size()>0)? nGbCtMnAdList1.get(0).getAmount():0;
            Integer ad2 = (nGbCtMnAdList2!=null && nGbCtMnAdList2.size()>0)? nGbCtMnAdList2.get(0).getAmount():0;
            Integer ad3 = (nGbCtMnAdList3!=null && nGbCtMnAdList3.size()>0)? nGbCtMnAdList3.get(0).getAmount():0;
            Integer ad4 = (nGbCtMnAdList4!=null && nGbCtMnAdList4.size()>0)? nGbCtMnAdList4.get(0).getAmount():0;
            Integer ad5 = (nGbCtMnAdList5!=null && nGbCtMnAdList5.size()>0)? nGbCtMnAdList5.get(0).getAmount():0;

            data.add(Arrays.asList(account,
                    this.convertToString(ad1),
                    this.convertToString(ad2),
                    this.convertToString(ad3),
                    this.convertToString(ad4),
                    this.convertToString(ad5)));
        }
        writer.write0(data, sheet, table1);
        writer.finish();
    }

    /**
     * 根据文件名下载信息
     * @param response
     * @param fileName
     * @throws IOException
     */
    public void downloadFile(HttpServletResponse response,String fileName) throws IOException {
        String tempFilePath = CommonUtil.getCatalinaHome() + environment.getProperty("teaching-platform.temp-excel");
        int lastIndex = fileName.lastIndexOf("--");
        String newFileName = fileName.substring(lastIndex + 2);
        EasyPOIUtil.exportFileByPath(response, tempFilePath+fileName,newFileName);
    }


    /**
     * 根据模板保存竞赛数据文件
     * @param contestId
     * @param studentId
     * @return
     */
    public void saveResultToFile(Integer contestId, Integer studentId,String fileName,String saveFilePath) {
        NGbContestStudent contestStudent = contestStudentMapper.get(contestId, studentId);
        if (contestStudent == null) {
            return;
        }

        Integer date = contestStudent.getDate() == null ? null : (contestStudent.getDate() / 10);
//        if(!maxDate.equals(date)){
//            return;
//        }
        // 库存信息
        Map<String, Object> kcxx;
        Future<Map<String, Object>> kcxxFuture = contestResultAsyncService.getKCXX(contestId, studentId);

        // 银行贷款
        Map<String, Object> yhdk;
        Future<Map<String, Object>> yhdkFuture = contestResultAsyncService.getYHDK(contestId, studentId, date);

        // 研发认证
        Map<String, Object> yfrz;
        Future<Map<String, Object>> yfrzFuture = contestResultAsyncService.getYFRZ(contestId, studentId);

        // 厂房与生产线
        Map<String, Object> cfyscx;
        Future<Map<String, Object>> cfyscxFuture = contestResultAsyncService.getCFYSCX(contestId, studentId);

        // 订单信息
        Map<String, Object> ddxx;
        Future<Map<String, Object>> ddxxFuture = contestResultAsyncService.getDDXX(contestId, studentId, date);

        // 现金流量表
        Map<String, Object> xjllb;
        Future<Map<String, Object>> xjllbFuture = contestResultAsyncService.getXJLLB(contestId, studentId);

        // 企业财务报表
        Map<String, Object> qycwbb;
        Future<Map<String, Object>> qycwbbFuture = contestResultAsyncService.getQYCWBB(contestId, studentId);

//        // 四年广告投放
//        Map<String, Object> ggtf;
//        Future<Map<String, Object>> ggtfFuture = contestResultAsyncService.getGGTF(contestId, studentId);

        // 所有异步任务执行完成
        while (true) {
            if (kcxxFuture.isDone() &&
                    yhdkFuture.isDone() &&
                    yfrzFuture.isDone() &&
                    cfyscxFuture.isDone() &&
                    ddxxFuture.isDone() &&
                    xjllbFuture.isDone() &&
                    qycwbbFuture.isDone()
//                    && ggtfFuture.isDone()
            ) {
                break;
            }
        }

        // 获取异步任务返回的数据
        try {
            kcxx = kcxxFuture.get();
            yhdk = yhdkFuture.get();
            cfyscx = cfyscxFuture.get();
            ddxx = ddxxFuture.get();
            yfrz = yfrzFuture.get();
            xjllb = xjllbFuture.get();
            qycwbb = qycwbbFuture.get();
//            ggtf = ggtfFuture.get();
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
            return;
        }

        /*
        数据封装
         */
        Map<String, Object> param = new HashMap<>();
        param.putAll(kcxx);
        param.putAll(yhdk);
        param.putAll(yfrz);
        param.putAll(cfyscx);
        param.putAll(ddxx);
        param.putAll(xjllb);
        param.putAll(qycwbb);
//        param.putAll(ggtf);


//        String uniqueFileName = CommonUtil.getUniqueFileName(studentId+"竞赛数据");
        String templateFilePath = CommonUtil.getClassesPath() + "templates/ngb_contest_result.xlsx";
        try {
            EasyPOIUtil.saveMultiSheetExcelByMap( saveFilePath ,fileName ,templateFilePath,param,true);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * 根据模板保存竞赛数据文件
     * @param contestId
     * @param studentId
     * @return
     */
    public void saveResultNoCashFlowToFile(Integer contestId, Integer studentId,String fileName,String saveFilePath) {
        NGbContestStudent contestStudent = contestStudentMapper.get(contestId, studentId);
        if (contestStudent == null) {
            return;
        }

        Integer date = contestStudent.getDate() == null ? null : (contestStudent.getDate() / 10);
//        if(!maxDate.equals(date)){
//            return;
//        }
        // 库存信息
        Map<String, Object> kcxx;
        Future<Map<String, Object>> kcxxFuture = contestResultAsyncService.getKCXX(contestId, studentId);

        // 银行贷款
        Map<String, Object> yhdk;
        Future<Map<String, Object>> yhdkFuture = contestResultAsyncService.getYHDK(contestId, studentId, date);

        // 研发认证
        Map<String, Object> yfrz;
        Future<Map<String, Object>> yfrzFuture = contestResultAsyncService.getYFRZ(contestId, studentId);

        // 厂房与生产线
        Map<String, Object> cfyscx;
        Future<Map<String, Object>> cfyscxFuture = contestResultAsyncService.getCFYSCX(contestId, studentId);

        // 订单信息
        Map<String, Object> ddxx;
        Future<Map<String, Object>> ddxxFuture = contestResultAsyncService.getDDXX(contestId, studentId, date);

//        // 现金流量表
//        Map<String, Object> xjllb;
//        Future<Map<String, Object>> xjllbFuture = contestResultAsyncService.getXJLLB(contestId, studentId);

        // 企业财务报表
        Map<String, Object> qycwbb;
        Future<Map<String, Object>> qycwbbFuture = contestResultAsyncService.getQYCWBB(contestId, studentId);

//        // 四年广告投放
//        Map<String, Object> ggtf;
//        Future<Map<String, Object>> ggtfFuture = contestResultAsyncService.getGGTF(contestId, studentId);

        // 所有异步任务执行完成
        while (true) {
            if (kcxxFuture.isDone() &&
                    yhdkFuture.isDone() &&
                    yfrzFuture.isDone() &&
                    cfyscxFuture.isDone() &&
                    ddxxFuture.isDone() &&
//                    xjllbFuture.isDone() &&
                    qycwbbFuture.isDone()
//                    && ggtfFuture.isDone()
            ) {
                break;
            }
        }

        // 获取异步任务返回的数据
        try {
            kcxx = kcxxFuture.get();
            yhdk = yhdkFuture.get();
            cfyscx = cfyscxFuture.get();
            ddxx = ddxxFuture.get();
            yfrz = yfrzFuture.get();
//            xjllb = xjllbFuture.get();
            qycwbb = qycwbbFuture.get();
//            ggtf = ggtfFuture.get();
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
            return;
        }

        /*
        数据封装
         */
        Map<String, Object> param = new HashMap<>();
        param.putAll(kcxx);
        param.putAll(yhdk);
        param.putAll(yfrz);
        param.putAll(cfyscx);
        param.putAll(ddxx);
//        param.putAll(xjllb);
        param.putAll(qycwbb);
//        param.putAll(ggtf);


//        String uniqueFileName = CommonUtil.getUniqueFileName(studentId+"竞赛数据");
        String templateFilePath = CommonUtil.getClassesPath() + "templates/ngb_contest_result_no_cash_flow.xlsx";
        try {
            EasyPOIUtil.saveMultiSheetExcelByMap( saveFilePath ,fileName ,templateFilePath,param,true);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    /**
     * 一键导出
     * @param response
     * @param contestId
     * @throws IOException
     */
    public void downAllFile(HttpServletResponse response,Integer contestId) throws IOException {
        Integer maxDate = gbContestStudentService.getMaxDate(contestId);
        String tempFilePath = CommonUtil.getCatalinaHome() + environment.getProperty("teaching-platform.temp-excel");
        String uniqueDir = CommonUtil.getUniqueFileName(contestId+maxDate+"");
        tempFilePath = tempFilePath+uniqueDir+"/";
        List<Integer> studentIdList = gbContestStudentService.getStudentIdList(contestId);
        Map<Integer, String> studentIdAndAccountMap = gbContestStudentService.getStudentIdAndAccountMap(contestId);
        List<String> filesNames = new ArrayList<>();
        List<File> files = new ArrayList<>();
        //每一个学生的所有竞赛数据
        for (Integer studentId : studentIdList) {
            String fileName = studentIdAndAccountMap.get(studentId) + ExcelTypeEnum.XLSX.getValue();
            this.saveResultToFile(contestId, studentId, fileName, tempFilePath);
            filesNames.add(fileName);
            File file = new File(tempFilePath+fileName);
            files.add(file);
        }
        //多年的广告投放和报表填写数据
        for (int year = 1; year <= maxDate/10; year++) {
            String fileName = "第"+year+"年";
            File excelFile = FileUtil.createExcelFile(tempFilePath,fileName);
            files.add(excelFile);
            filesNames.add(excelFile.getName());
            this.addAndFinancial(excelFile,contestId,year);
        }

        FileUtil.toZipAndDownload(response,files,filesNames,"第"+maxDate/10+"年"+maxDate%10+"季一键导出.zip");
        files.forEach(File::delete);
    }


    /**
     * 广告和财务报表信息
     * @param excelFile
     * @param contestId
     * @param year
     * @throws FileNotFoundException
     */
    public void addAndFinancial(File excelFile ,Integer contestId,Integer year) throws FileNotFoundException {
        //学生和账号的map
        Map<Integer, String> studentMap = gbContestStudentService.getStudentIdAndAccountMap(contestId);

        FileOutputStream fos = new FileOutputStream(excelFile);
        ExcelWriter writer = new ExcelWriter(fos, ExcelTypeEnum.XLSX);
        //广告投放数据
        {
            /*
            表格式
             */
            TableStyle tableStyle = new TableStyle();
            Font font = new Font();
            font.setFontName("Arial");
            font.setFontHeightInPoints((short) 10);
            tableStyle.setTableHeadFont(font);
            tableStyle.setTableHeadBackGroundColor(IndexedColors.GREY_25_PERCENT);
            tableStyle.setTableContentFont(font);
            tableStyle.setTableContentBackGroundColor(IndexedColors.WHITE);
            // 设置每列宽度
            Map<Integer, Integer> columnWidth = new HashMap<>();
            for (int i = 0; i < 24; i++) {
                columnWidth.put(i, 4000);
            }
            String name = "第" + year + "年广告投放";
            Sheet sheet = new Sheet(1, 0);
            sheet.setColumnWidthMap(columnWidth);
            sheet.setSheetName(name);
            sheet.setStartRow(1);   // 前面空一行

            List<NGbCtMnAd> ctMnAdList = gbCtMnAdMapper.getAdListAndAccountByYear(contestId,year);


            List<List<String>> head = new ArrayList<>();
            Table table1 = new Table(0);
            String[] strs = {"组号", "本地", "区域", "国内", "亚洲", "国际"};
            for (int season = 1; season <= 4; season++) {
                String title = "第" + year + "年" + "第" + season + "季广告投放";
                //获取当季数据
                for (int j = 0; j < 6; j++) {
                    List<String> headCoulumn = new ArrayList<>();
                    // 第一行的头（名称一样则自动合并成一个单元格）
                    headCoulumn.add(title);
                    // 第二行的头
                    headCoulumn.add(strs[j]);
                    head.add(headCoulumn);
                }
            }
            table1.setHead(head);
            table1.setTableStyle(tableStyle);

            List<List<String>> data = new ArrayList<>();

            //遍历学生一个学生一条记录
            for (Integer studentId : studentMap.keySet()) {
                String account = studentMap.get(studentId);
                List<String> cloumn = new ArrayList<>();
                for (int season = 1; season <= 4; season++) {

                    //获取学生在对应季度的广告投放
                    int finalSeason = season;
                    List<NGbCtMnAd> nGbCtMnAdList1 = ctMnAdList.stream().filter(item-> item.getQuarterly() == finalSeason && item.getStudentId().equals(studentId) && item.getCmId()==1 ).collect(Collectors.toList());
                    List<NGbCtMnAd> nGbCtMnAdList2 = ctMnAdList.stream().filter(item-> item.getQuarterly() == finalSeason && item.getStudentId().equals(studentId) && item.getCmId()==2 ).collect(Collectors.toList());
                    List<NGbCtMnAd> nGbCtMnAdList3 = ctMnAdList.stream().filter(item-> item.getQuarterly() == finalSeason && item.getStudentId().equals(studentId) && item.getCmId()==3 ).collect(Collectors.toList());
                    List<NGbCtMnAd> nGbCtMnAdList4 = ctMnAdList.stream().filter(item-> item.getQuarterly() == finalSeason && item.getStudentId().equals(studentId) && item.getCmId()==4 ).collect(Collectors.toList());
                    List<NGbCtMnAd> nGbCtMnAdList5 = ctMnAdList.stream().filter(item-> item.getQuarterly() == finalSeason && item.getStudentId().equals(studentId) && item.getCmId()==5 ).collect(Collectors.toList());

                    Integer ad1 = (nGbCtMnAdList1!=null && nGbCtMnAdList1.size()>0)? nGbCtMnAdList1.get(0).getAmount():0;
                    Integer ad2 = (nGbCtMnAdList2!=null && nGbCtMnAdList2.size()>0)? nGbCtMnAdList2.get(0).getAmount():0;
                    Integer ad3 = (nGbCtMnAdList3!=null && nGbCtMnAdList3.size()>0)? nGbCtMnAdList3.get(0).getAmount():0;
                    Integer ad4 = (nGbCtMnAdList4!=null && nGbCtMnAdList4.size()>0)? nGbCtMnAdList4.get(0).getAmount():0;
                    Integer ad5 = (nGbCtMnAdList5!=null && nGbCtMnAdList5.size()>0)? nGbCtMnAdList5.get(0).getAmount():0;

                    cloumn.add(account);
                    cloumn.add(this.convertToString(ad1));
                    cloumn.add(this.convertToString(ad2));
                    cloumn.add(this.convertToString(ad3));
                    cloumn.add(this.convertToString(ad4));
                    cloumn.add(this.convertToString(ad5));
                }
                data.add(cloumn);
            }
            writer.write0(data, sheet, table1);
        }
        /*
        获取综合费用表
         */
        {
            List<NGbCtCharges> gbCtChargesList = gbCtChargesMapper.getListByContestId(contestId, year);//综合费用表
            if (gbCtChargesList.size() == 0) {
                gbCtChargesList.add(new NGbCtCharges());
            }
            // 设置每列宽度
            Map<Integer, Integer> columnWidth = new HashMap<>();
            for (int i = 0; i <= gbCtChargesList.size(); i++) {
                columnWidth.put(i, 4000);
            }

            Sheet sheet = new Sheet(2, 0);
            sheet.setColumnWidthMap(columnWidth);
            sheet.setSheetName("综合费用表");
            sheet.setStartRow(1);   // 前面空一行

            /*
            表格式
             */
            TableStyle tableStyle = new TableStyle();
            Font font = new Font();
            font.setFontName("Arial");
            font.setFontHeightInPoints((short) 10);
            tableStyle.setTableHeadFont(font);
            tableStyle.setTableHeadBackGroundColor(IndexedColors.GREY_25_PERCENT);
            tableStyle.setTableContentFont(font);
            tableStyle.setTableContentBackGroundColor(IndexedColors.WHITE);

            Table table1 = new Table(1);
            String title = "综合费用表";
            List<List<String>> head = new ArrayList<>();
            List<String> headCoulumn = new ArrayList<>();
            headCoulumn.add(title);
            headCoulumn.add("组号");
            head.add(headCoulumn);
            for (NGbCtCharges gbCtCharges : gbCtChargesList) {
                List<String> headCoulumn1 = new ArrayList<>();
                headCoulumn1.add(title);
                headCoulumn1.add(studentMap.get(gbCtCharges.getStudentId()));
                head.add(headCoulumn1);
            }
            table1.setHead(head);
            table1.setTableStyle(tableStyle);

            List<List<String>> data = new ArrayList<>();
            List<String> data1 = new ArrayList<>();
            data.add(data1);
            data1.add("管理费");
            data1.addAll(gbCtChargesList.stream().map(k->convertObjToString(k.getCOverhaul())).collect(Collectors.toList()));
            List<String> data2 = new ArrayList<>();
            data.add(data2);
            data2.add("广告费");
            data2.addAll(gbCtChargesList.stream().map(k->convertObjToString(k.getCAd())).collect(Collectors.toList()));
            List<String> data3 = new ArrayList<>();
            data.add(data3);
            data3.add("产线维修费");
            data3.addAll(gbCtChargesList.stream().map(k->convertObjToString(k.getCMaintenance())).collect(Collectors.toList()));
            List<String> data4 = new ArrayList<>();
            data.add(data4);
            data4.add("转产费");
            data4.addAll(gbCtChargesList.stream().map(k->convertObjToString(k.getCTransfer())).collect(Collectors.toList()));
            List<String> data5 = new ArrayList<>();
            data.add(data5);
            data5.add("市场开拓费");
            data5.addAll(gbCtChargesList.stream().map(k->convertObjToString(k.getCDevelopMarket())).collect(Collectors.toList()));
            List<String> data6 = new ArrayList<>();
            data.add(data6);
            data6.add("产品资质申请");
            data6.addAll(gbCtChargesList.stream().map(k->convertObjToString(k.getCDevelopProduct())).collect(Collectors.toList()));
            List<String> data7 = new ArrayList<>();
            data.add(data7);
            data7.add("ISO认证申请");
            data7.addAll(gbCtChargesList.stream().map(k->convertObjToString(k.getCDevelopIso())).collect(Collectors.toList()));


            List<String> data8 = new ArrayList<>();
            data.add(data8);
            data8.add("信息费");
            data8.addAll(gbCtChargesList.stream().map(k->convertObjToString(k.getCInformation())).collect(Collectors.toList()));



            List<String> data9 = new ArrayList<>();
            data.add(data9);
            data9.add("产品设计费");
            data9.addAll(gbCtChargesList.stream().map(k->convertObjToString(k.getCProductDesign())).collect(Collectors.toList()));


            List<String> data10 = new ArrayList<>();
            data.add(data10);
            data10.add("辞退福利");
            data10.addAll(gbCtChargesList.stream().map(k->convertObjToString(k.getCDismissFee())).collect(Collectors.toList()));



            List<String> data11 = new ArrayList<>();
            data.add(data11);
            data11.add("培训费");
            data11.addAll(gbCtChargesList.stream().map(k->convertObjToString(k.getCTrainFee())).collect(Collectors.toList()));


            List<String> data12 = new ArrayList<>();
            data.add(data12);
            data12.add("激励费");
            data12.addAll(gbCtChargesList.stream().map(k->convertObjToString(k.getCIncentiveFee())).collect(Collectors.toList()));


            List<String> data13 = new ArrayList<>();
            data.add(data13);
            data13.add("人力费");
            data13.addAll(gbCtChargesList.stream().map(k->convertObjToString(k.getCHr())).collect(Collectors.toList()));


            List<String> data14 = new ArrayList<>();
            data.add(data14);
            data14.add("碳中和费用");
            data14.addAll(gbCtChargesList.stream().map(k->convertObjToString(k.getCCarbon())).collect(Collectors.toList()));

            List<String> data15 = new ArrayList<>();
            data.add(data15);
            data15.add("特性研发");
            data15.addAll(gbCtChargesList.stream().map(k->convertObjToString(k.getCFeature())).collect(Collectors.toList()));


            List<String> data16 = new ArrayList<>();
            data.add(data16);
            data16.add("数字化研发费");
            data16.addAll(gbCtChargesList.stream().map(k->convertObjToString(k.getCDigitalization())).collect(Collectors.toList()));


            List<String> data17 = new ArrayList<>();
            data.add(data17);
            data17.add("合计");
            data17.addAll(gbCtChargesList.stream().map(k->convertObjToString(k.getCTotal())).collect(Collectors.toList()));

            writer.write0(data, sheet, table1);
        }
        /*
        获取利润表
         */
        {
            List<NGbCtProfitChart> gbCtProfitChartsList = gbCtProfitChartMapper.getListByContestId(contestId, year);//利润表
            if (gbCtProfitChartsList.size() == 0) {
                gbCtProfitChartsList.add(new NGbCtProfitChart());
            }
            // 设置每列宽度
            Map<Integer, Integer> columnWidth = new HashMap<>();
            for (int i = 0; i < gbCtProfitChartsList.size(); i++) {
                columnWidth.put(i, 4000);
            }

            Sheet sheet = new Sheet(3, 0);
            sheet.setColumnWidthMap(columnWidth);
            sheet.setSheetName("利润表");
            sheet.setStartRow(1);   // 前面空一行

            /*
            表格式
             */
            TableStyle tableStyle = new TableStyle();
            Font font = new Font();
            font.setFontName("Arial");
            font.setFontHeightInPoints((short) 10);
            tableStyle.setTableHeadFont(font);
            tableStyle.setTableHeadBackGroundColor(IndexedColors.GREY_25_PERCENT);
            tableStyle.setTableContentFont(font);
            tableStyle.setTableContentBackGroundColor(IndexedColors.WHITE);

            Table table1 = new Table(1);
            String title = "利润表";
            List<List<String>> head = new ArrayList<>();
            List<String> headCoulumn = new ArrayList<>();
            headCoulumn.add("组号");
            head.add(headCoulumn);
            for (NGbCtProfitChart gbCtProfitChart : gbCtProfitChartsList) {
                List<String> headCoulumn1 = new ArrayList<>();
                headCoulumn1.add(title);
                headCoulumn1.add(studentMap.get(gbCtProfitChart.getStudentId()));
                head.add(headCoulumn1);
            }
            table1.setHead(head);
            table1.setTableStyle(tableStyle);

            List<List<String>> data = new ArrayList<>();
            List<String> data1 = new ArrayList<>();
            data.add(data1);
            data1.add("销售收入");
            data1.addAll(gbCtProfitChartsList.stream().map(k->convertObjToString(k.getPcSales())).collect(Collectors.toList()));
            List<String> data2 = new ArrayList<>();
            data.add(data2);
            data2.add("直接成本");
            data2.addAll(gbCtProfitChartsList.stream().map(k->convertObjToString(k.getPcDirectCost())).collect(Collectors.toList()));
            List<String> data3 = new ArrayList<>();
            data.add(data3);
            data3.add("毛利");
            data3.addAll(gbCtProfitChartsList.stream().map(k->convertObjToString(k.getPcGoodsProfit())).collect(Collectors.toList()));
            List<String> data4 = new ArrayList<>();
            data.add(data4);
            data4.add("综合管理费用");
            data4.addAll(gbCtProfitChartsList.stream().map(k->convertObjToString(k.getPcTotal())).collect(Collectors.toList()));
            List<String> data5 = new ArrayList<>();
            data.add(data5);
            data5.add("折旧前利润");
            data5.addAll(gbCtProfitChartsList.stream().map(k->convertObjToString(k.getPcProfitBeforeDep())).collect(Collectors.toList()));
            List<String> data6 = new ArrayList<>();
            data.add(data6);
            data6.add("折旧");
            data6.addAll(gbCtProfitChartsList.stream().map(k->convertObjToString(k.getPcDep())).collect(Collectors.toList()));
            List<String> data7 = new ArrayList<>();
            data.add(data7);
            data7.add("支付利息前利润");
            data7.addAll(gbCtProfitChartsList.stream().map(k->convertObjToString(k.getPcProfitBeforeInterests())).collect(Collectors.toList()));
            List<String> data8 = new ArrayList<>();
            data.add(data8);
            data8.add("财务费用");
            data8.addAll(gbCtProfitChartsList.stream().map(k->convertObjToString(k.getPcFinanceFee())).collect(Collectors.toList()));
            List<String> data9 = new ArrayList<>();
            data.add(data9);
            data9.add("营业外收支");
            data9.addAll(gbCtProfitChartsList.stream().map(k->convertObjToString(k.getPcNonOperating())).collect(Collectors.toList()));
            List<String> data10 = new ArrayList<>();
            data.add(data10);
            data10.add("税前利润");
            data10.addAll(gbCtProfitChartsList.stream().map(k->convertObjToString(k.getPcProfitBeforeTax())).collect(Collectors.toList()));
            List<String> data11 = new ArrayList<>();
            data.add(data11);
            data11.add("所得税");
            data11.addAll(gbCtProfitChartsList.stream().map(k->convertObjToString(k.getPcTax())).collect(Collectors.toList()));
            List<String> data12 = new ArrayList<>();
            data.add(data12);
            data12.add("净利润");
            data12.addAll(gbCtProfitChartsList.stream().map(k->convertObjToString(k.getPcAnnualNetProfit())).collect(Collectors.toList()));


            writer.write0(data, sheet, table1);
        }
        /*
        获取资产负债表
         */
        {
            List<NGbCtBalance> gbCtBalanceList = gbCtBalanceMapper.getListByContestId(contestId,year);//资产负债表
            if (gbCtBalanceList.size() == 0) {
                gbCtBalanceList.add(new NGbCtBalance());
            }
            // 设置每列宽度
            Map<Integer, Integer> columnWidth = new HashMap<>();
            for (int i = 0; i < gbCtBalanceList.size(); i++) {
                columnWidth.put(i, 4000);
            }

            Sheet sheet = new Sheet(4, 0);
            sheet.setColumnWidthMap(columnWidth);
            sheet.setSheetName("资产负债表");
            sheet.setStartRow(1);   // 前面空一行

            /*
            表格式
             */
            TableStyle tableStyle = new TableStyle();
            Font font = new Font();
            font.setFontName("Arial");
            font.setFontHeightInPoints((short) 10);
            tableStyle.setTableHeadFont(font);
            tableStyle.setTableHeadBackGroundColor(IndexedColors.GREY_25_PERCENT);
            tableStyle.setTableContentFont(font);
            tableStyle.setTableContentBackGroundColor(IndexedColors.WHITE);


            Table table1 = new Table(1);
            String title = "资产负债表";
            List<List<String>> head = new ArrayList<>();
            List<String> headCoulumn = new ArrayList<>();
            headCoulumn.add("组号");
            head.add(headCoulumn);
            for (NGbCtBalance gbCtBalance : gbCtBalanceList) {
                List<String> headCoulumn1 = new ArrayList<>();
                headCoulumn1.add(title);
                headCoulumn1.add(studentMap.get(gbCtBalance.getStudentId()));
                head.add(headCoulumn1);
            }
            table1.setHead(head);
            table1.setTableStyle(tableStyle);

            List<List<String>> data = new ArrayList<>();
            List<String> data1 = new ArrayList<>();
            data.add(data1);
            data1.add("现金");
            data1.addAll(gbCtBalanceList.stream().map(k->convertObjToString(convertObjToString(k.getBsCash()))).collect(Collectors.toList()));
            List<String> data2 = new ArrayList<>();
            data.add(data2);
            data2.add("应收款");
            data2.addAll(gbCtBalanceList.stream().map(k->convertObjToString(convertObjToString(k.getBsReceivable()))).collect(Collectors.toList()));
            List<String> data3 = new ArrayList<>();
            data.add(data3);
            data3.add("在制品");
            data3.addAll(gbCtBalanceList.stream().map(k->convertObjToString(k.getBsProductInProcess())).collect(Collectors.toList()));
            List<String> data4 = new ArrayList<>();
            data.add(data4);
            data4.add("产成品");
            data4.addAll(gbCtBalanceList.stream().map(k->convertObjToString(k.getBsProduct())).collect(Collectors.toList()));
            List<String> data5 = new ArrayList<>();
            data.add(data5);
            data5.add("原材料");
            data5.addAll(gbCtBalanceList.stream().map(k->convertObjToString(k.getBsMaterial())).collect(Collectors.toList()));
            List<String> data6 = new ArrayList<>();
            data.add(data6);
            data6.add("流动资产合计");
            data6.addAll(gbCtBalanceList.stream().map(k->convertObjToString(k.getBsTotalCurrentAsset())).collect(Collectors.toList()));
            List<String> data7 = new ArrayList<>();
            data.add(data7);
            data7.add("机器和设备");
            data7.addAll(gbCtBalanceList.stream().map(k->convertObjToString(k.getBsEquipment())).collect(Collectors.toList()));
            List<String> data8 = new ArrayList<>();
            data.add(data8);
            data8.add("在建工程");
            data8.addAll(gbCtBalanceList.stream().map(k->convertObjToString(k.getBsProjectOnConstruction())).collect(Collectors.toList()));
            List<String> data9 = new ArrayList<>();
            data.add(data9);
            data9.add("固定资产合计");
            data9.addAll(gbCtBalanceList.stream().map(k->convertObjToString(k.getBsTotalFixedAsset())).collect(Collectors.toList()));
            List<String> data10 = new ArrayList<>();
            data.add(data10);
            data10.add("资产合计");
            data10.addAll(gbCtBalanceList.stream().map(k->convertObjToString(k.getBsTotalAsset())).collect(Collectors.toList()));
            List<String> data11 = new ArrayList<>();
            data.add(data11);
            data11.add("长期贷款");
            data11.addAll(gbCtBalanceList.stream().map(k->convertObjToString(k.getBsTotalAsset())).collect(Collectors.toList()));
            List<String> data12 = new ArrayList<>();
            data.add(data12);
            data12.add("短期贷款");
            data12.addAll(gbCtBalanceList.stream().map(k->convertObjToString(k.getBsShortLoan())).collect(Collectors.toList()));
            List<String> data13 = new ArrayList<>();
            data.add(data13);
            data13.add("其他应付款");
            data13.addAll(gbCtBalanceList.stream().map(k->convertObjToString(k.getBsOtherPay())).collect(Collectors.toList()));
            List<String> data14 = new ArrayList<>();
            data.add(data14);
            data14.add("应交税费");
            data14.addAll(gbCtBalanceList.stream().map(k->convertObjToString(k.getBsTax())).collect(Collectors.toList()));
            List<String> data15 = new ArrayList<>();
            data.add(data15);
            data15.add("负债合计");
            data15.addAll(gbCtBalanceList.stream().map(k->convertObjToString(k.getBsTotalLiability())).collect(Collectors.toList()));
            List<String> data16 = new ArrayList<>();
            data.add(data16);
            data16.add("股东资本");
            data16.addAll(gbCtBalanceList.stream().map(k->convertObjToString(k.getBsEquity())).collect(Collectors.toList()));
            List<String> data17 = new ArrayList<>();
            data.add(data17);
            data17.add("利润留存");
            data17.addAll(gbCtBalanceList.stream().map(k->convertObjToString(k.getBsRetainedEarning())).collect(Collectors.toList()));
            List<String> data18 = new ArrayList<>();
            data.add(data18);
            data18.add("年度净利");
            data18.addAll(gbCtBalanceList.stream().map(k->convertObjToString(k.getBsAnnualNetProfit())).collect(Collectors.toList()));
            List<String> data19 = new ArrayList<>();
            data.add(data19);
            data19.add("所有者权益");
            data19.addAll(gbCtBalanceList.stream().map(k->convertObjToString(k.getBsTotalEquity())).collect(Collectors.toList()));
            List<String> data20 = new ArrayList<>();
            data.add(data20);
            data20.add("负债所有者权益合计");
            data20.addAll(gbCtBalanceList.stream().map(k->convertObjToString(k.getBsTotal())).collect(Collectors.toList()));

            table1.setHead(head);
            table1.setTableStyle(tableStyle);
            writer.write0(data, sheet, table1);
        }
        writer.finish();
        IOUtils.closeQuietly(fos);
    }


//    /**
//     * 生成复盘数据
//     * @param response
//     * @param contestId
//     * @param newSubjectNumber
//     */
//    public void downAutoSubjectModelFile(HttpServletResponse response, Integer contestId, Integer newSubjectNumber) {
//        Map<String,Object> resultMap = new HashMap<>();
//        NGbContest contest = gbContestMapper.selectById(contestId);
//        Integer subjectNumber = contest.getSubjectNumber();
//        // 规则cs
//        List<Map<String, Object>> cs = new ArrayList<>();
//        List<NGbCtGzCs> gbCtGzCsList = gbCtGzCsMapper.selectList(new QueryWrapper<NGbCtGzCs>().eq("subject_number", subjectNumber));
//        for (NGbCtGzCs ctGzCs : gbCtGzCsList) {
//            Map<String, Object> map = new HashMap<>();
//            map.put("subjectNumber",newSubjectNumber);
//            map.put("punish",ctGzCs.getPunish());
//            map.put("inventoryDiscountProduct",ctGzCs.getInventoryDiscountProduct());
//            map.put("inventoryDiscountMaterial",ctGzCs.getInventoryDiscountMaterial());
//            map.put("loanCeiling",ctGzCs.getLoanCeiling());
//            map.put("cash",ctGzCs.getCash());
//            map.put("overhaul",ctGzCs.getOverhaul());
//            map.put("information",ctGzCs.getInformation());
//            map.put("emergenceMultipleMaterial",ctGzCs.getEmergenceMultipleMaterial());
//            map.put("emergenceMultipleProduct",ctGzCs.getEmergenceMultipleProduct());
//            map.put("incomeTax",ctGzCs.getIncomeTax());
//            map.put("maxLine",ctGzCs.getMaxLine());
//            cs.add(map);
//        }
//        resultMap.put("cs",cs);
//        //广告
//        List<Map<String, Object>> ad = new ArrayList<>();
//        List<NGbCtMnAd> gbCtMnAds = gbCtMnAdMapper.selectList(new QueryWrapper<NGbCtMnAd>().eq("contest_id", contestId));
//        for (NGbCtMnAd gbCtMnAd : gbCtMnAds) {
//            Map<String, Object> map = new HashMap<>();
//            map.put("subjectNumber",newSubjectNumber);
//            map.put("year",gbCtMnAd.getYear());
//            map.put("quarterly",gbCtMnAd.getQuarterly());
//            if(gbCtMnAd.getGroupNum() != null && StringUtils.isNotEmpty(gbCtMnAd.getGroupNum())){
//                map.put("groupNum","人机"+gbCtMnAd.getGroupNum());
//            }
//            map.put("localP1",gbCtMnAd.getLocalP1());
//            map.put("localP2",gbCtMnAd.getLocalP2());
//            map.put("localP3",gbCtMnAd.getLocalP3());
//            map.put("localP4",gbCtMnAd.getLocalP4());
//            map.put("localP5",gbCtMnAd.getLocalP5());
//            map.put("regionalP1",gbCtMnAd.getRegionalP1());
//            map.put("regionalP2",gbCtMnAd.getRegionalP2());
//            map.put("regionalP3",gbCtMnAd.getRegionalP3());
//            map.put("regionalP4",gbCtMnAd.getRegionalP4());
//            map.put("regionalP5",gbCtMnAd.getRegionalP5());
//            map.put("nationalP1",gbCtMnAd.getNationalP1());
//            map.put("nationalP2",gbCtMnAd.getNationalP2());
//            map.put("nationalP3",gbCtMnAd.getNationalP3());
//            map.put("nationalP4",gbCtMnAd.getNationalP4());
//            map.put("nationalP5",gbCtMnAd.getNationalP5());
//            map.put("asianP1",gbCtMnAd.getAsianP1());
//            map.put("asianP2",gbCtMnAd.getAsianP2());
//            map.put("asianP3",gbCtMnAd.getAsianP3());
//            map.put("asianP4",gbCtMnAd.getAsianP4());
//            map.put("asianP5",gbCtMnAd.getAsianP5());
//            map.put("internationalP1",gbCtMnAd.getInternationalP1());
//            map.put("internationalP2",gbCtMnAd.getInternationalP2());
//            map.put("internationalP3",gbCtMnAd.getInternationalP3());
//            map.put("internationalP4",gbCtMnAd.getInternationalP4());
//            map.put("internationalP5",gbCtMnAd.getInternationalP5());
//            ad.add(map);
//        }
//        resultMap.put("ad",ad);
//        //ISO
//        List<Map<String, Object>> iso = new ArrayList<>();
//        List<NGbCtGzIso> gbCtGzIsoList = gbCtGzIsoMapper.selectList(new QueryWrapper<NGbCtGzIso>().eq("subject_number", subjectNumber));
//        for (NGbCtGzIso gbCtGzIso : gbCtGzIsoList) {
//            Map<String, Object> map = new HashMap<>();
//            map.put("subjectNumber",newSubjectNumber);
//            map.put("ciId",gbCtGzIso.getCiId());
//            map.put("ciName",gbCtGzIso.getCiName());
//            map.put("ciDevelopFee",gbCtGzIso.getCiDevelopFee());
//            map.put("ciDevelopDate",gbCtGzIso.getCiDevelopDate());
//            iso.add(map);
//        }
//        resultMap.put("iso",iso);
//        //order
//        List<Map<String, Object>> order = new ArrayList<>();
//        List<NGbCtMnChoose> gbCtMnChooses = gbCtMnChooseMapper.selectList(new QueryWrapper<NGbCtMnChoose>().eq("contest_id", contestId));
//        for (NGbCtMnChoose gbCtMnChoose : gbCtMnChooses) {
//            Map<String, Object> map = new HashMap<>();
//            map.put("subjectNumber",newSubjectNumber);
//            map.put("coId",gbCtMnChoose.getCoId());
//            map.put("date",gbCtMnChoose.getDate());
//            map.put("quarterly",gbCtMnChoose.getQuarterly());
//            map.put("cmId",gbCtMnChoose.getCmId());
//            map.put("cpId",gbCtMnChoose.getCpId());
//            map.put("num",gbCtMnChoose.getNum());
//            map.put("totalPrice",gbCtMnChoose.getTotalPrice());
//            map.put("deliveryDate",gbCtMnChoose.getDeliveryDate());
//            map.put("paymentDate",gbCtMnChoose.getPaymentDay());
//            map.put("ciId",gbCtMnChoose.getCiId());
//            if(gbCtMnChoose.getXzRound() != null && StringUtils.isNotEmpty(gbCtMnChoose.getXzRound().toString())){
//                map.put("xzRound",+gbCtMnChoose.getXzRound());
//            }
//            if(gbCtMnChoose.getXzGroup() != null && StringUtils.isNotEmpty(gbCtMnChoose.getXzGroup().toString())){
//                map.put("xzGroup","人机"+gbCtMnChoose.getXzGroup());
//            }
//            order.add(map);
//        }
//        resultMap.put("order",order);
//        //market
//        List<Map<String, Object>> market = new ArrayList<>();
//        List<NGbCtGzMarket> markets = gbCtGzMarketMapper.selectList(new QueryWrapper<NGbCtGzMarket>().eq("subject_number", subjectNumber));
//        for (NGbCtGzMarket gbCtGzMarket : markets) {
//            Map<String, Object> map = new HashMap<>();
//            map.put("subjectNumber",newSubjectNumber);
//            map.put("cmId",gbCtGzMarket.getCmId());
//            map.put("cmName",gbCtGzMarket.getCmName());
//            map.put("cmDevelopFee",gbCtGzMarket.getCmDevelopFee());
//            map.put("cmDevelopDate",gbCtGzMarket.getCmDevelopDate());
//            market.add(map);
//        }
//        resultMap.put("market",market);
//        //material
//        List<Map<String, Object>> material = new ArrayList<>();
//        List<NGbCtGzMaterial> materials = gbCtGzMaterialMapper.selectList(new QueryWrapper<NGbCtGzMaterial>().eq("subject_number", subjectNumber));
//        for (NGbCtGzMaterial gbCtGzMaterial : materials) {
//            Map<String, Object> map = new HashMap<>();
//            map.put("subjectNumber",newSubjectNumber);
//            map.put("cmId",gbCtGzMaterial.getCmId());
//            map.put("cnName",gbCtGzMaterial.getCnName());
//            map.put("cmLeadDate",gbCtGzMaterial.getCmLeadDate());
//            map.put("cmBuyFee",gbCtGzMaterial.getCmBuyFee());
//            map.put("cmPayDate",gbCtGzMaterial.getCmPayDate());
//            map.put("num",gbCtGzMaterial.getNum());
//            material.add(map);
//        }
//        resultMap.put("material",material);
//        //product
//        List<Map<String, Object>> product = new ArrayList<>();
//        List<NGbCtGzProduct> products = gbCtGzProductMapper.selectList(new QueryWrapper<NGbCtGzProduct>().eq("subject_number", subjectNumber));
//        for (NGbCtGzProduct gbCtGzProduct : products) {
//            Map<String, Object> map = new HashMap<>();
//            map.put("subjectNumber",newSubjectNumber);
//            map.put("cpId",gbCtGzProduct.getCpId());
//            map.put("cpName",gbCtGzProduct.getCpName());
//            map.put("cpProcessingFee",gbCtGzProduct.getCpProcessingFee());
//            map.put("cpDevelopDate",gbCtGzProduct.getCpDevelopDate());
//            map.put("cpDevelopFee",gbCtGzProduct.getCpDevelopFee());
//            map.put("cpDirectCost",gbCtGzProduct.getCpDirectCost());
//            product.add(map);
//        }
//        resultMap.put("product",product);
//        //producing
//        List<Map<String, Object>> producing = new ArrayList<>();
//        List<NGbCtGzProducing> producings = gbCtGzProducingMapper.selectList(new QueryWrapper<NGbCtGzProducing>().eq("subject_number", subjectNumber));
//        for (NGbCtGzProducing gbCtGzProducing : producings) {
//            Map<String, Object> map = new HashMap<>();
//            map.put("subjectNumber",newSubjectNumber);
//            map.put("cpId",gbCtGzProducing.getCpId());
//            map.put("cpName",gbCtGzProducing.getCpName());
//            map.put("cpMid",gbCtGzProducing.getCpMid());
//            map.put("cpNum",gbCtGzProducing.getCpNum());
//            producing.add(map);
//        }
//        resultMap.put("producing",producing);
//        //productLine
//        List<Map<String, Object>> productLine = new ArrayList<>();
//        List<NGbCtGzProductLine> productLines = gbCtGzProductLineMapper.selectList(new QueryWrapper<NGbCtGzProductLine>().eq("subject_num", subjectNumber));
//        for (NGbCtGzProductLine line : productLines) {
//            Map<String, Object> map = new HashMap<>();
//            map.put("subjectNumber",newSubjectNumber);
//            map.put("cplId",line.getCplId());
//            map.put("cplName",line.getCplName());
//            map.put("cplBuyFee",line.getCplBuyFee());
//            map.put("cplInstallDate",line.getCplInstallDate());
//            map.put("cplProduceDate",line.getCplProduceDate());
//            map.put("cplTransferDate",line.getCplTransferDate());
//            map.put("cplTransferFee",line.getCplTransferFee());
//            map.put("cplMaintenanceFee",line.getCplMaintenanceFee());
//            map.put("cplScarpFee",line.getCplScarpFee());
//            map.put("cplDepDate",line.getCplDepDate());
//            map.put("cplDepFee",line.getCplDepFee());
//            map.put("cplSeniorWorker",line.getCplSeniorWorker());
//            map.put("cplOrdinaryWorker",line.getCplOrdinaryWorker());
//            map.put("cplProduction",line.getCplProduction());
//            productLine.add(map);
//        }
//        resultMap.put("productLine",productLine);
//
//        //productDesign
//        List<Map<String, Object>> productDesign = new ArrayList<>();
//        List<NGbCtGzProductDesign> productDesigns = gbCtGzProductDesignMapper.selectList(new QueryWrapper<NGbCtGzProductDesign>().eq("subject_num", subjectNumber));
//        for (NGbCtGzProductDesign design : productDesigns) {
//            Map<String, Object> map = new HashMap<>();
//            map.put("subjectNumber",newSubjectNumber);
//            map.put("featureName",design.getFeatureName());
//            map.put("designNum",design.getDesignNum());
//            map.put("costMarkup",design.getCostMarkup());
//            map.put("designCost",design.getDesignCost());
//            map.put("unitCostUpgrade",design.getUnitCostUpgrade());
//            map.put("initialValue",design.getInitialValue());
//            map.put("upperLimit",design.getUpperLimit());
//            productDesign.add(map);
//        }
//        resultMap.put("productDesign",productDesign);
//
//        //workerRecruit
//        List<Map<String, Object>> workerRecruit = new ArrayList<>();
//        List<NGbCtGzWorkerRecruit> workerRecruits = gbCtGzWorkerRecruitMapper.selectList(new QueryWrapper<NGbCtGzWorkerRecruit>().eq("subject_num", subjectNumber));
//        for (NGbCtGzWorkerRecruit recruit : workerRecruits) {
//            Map<String, Object> map = new HashMap<>();
//            map.put("subjectNumber",newSubjectNumber);
//            map.put("recruitName",recruit.getRecruitName());
//            map.put("recruitNum",recruit.getRecruitNum());
//            map.put("initSalExpect",recruit.getInitSalExpect());
//            map.put("piece",recruit.getPiece());
//            map.put("qtyPerQtr",recruit.getQtyPerQtr());
//            map.put("multBonus",recruit.getMultBonus());
//            workerRecruit.add(map);
//        }
//        resultMap.put("workerRecruit",workerRecruit);
//
//        //workerTrain
//        List<Map<String, Object>> workerTrain = new ArrayList<>();
//        List<NGbCtGzWorkerTrain> workerTrains = gbCtGzWorkerTrainMapper.selectList(new QueryWrapper<NGbCtGzWorkerTrain>().eq("subject_num", subjectNumber));
//        for (NGbCtGzWorkerTrain train : workerTrains) {
//            Map<String, Object> map = new HashMap<>();
//            map.put("subjectNumber",newSubjectNumber);
//            map.put("trainingName",train.getTrainingName());
//            map.put("cashCost",train.getCashCost());
//            map.put("timeCostQuarter",train.getTimeCostQuarter());
//            map.put("originalPosition",train.getOriginalPosition());
//            map.put("trainedPosition",train.getTrainedPosition());
//            map.put("salaryIncreasePercent",train.getSalaryIncreasePercent());
//            workerTrain.add(map);
//        }
//        resultMap.put("workerTrain",workerTrain);
//
//        //loan
//        List<Map<String, Object>> loan = new ArrayList<>();
//        List<NGbCtGzLoan> loans = gbCtGzLoanMapper.selectList(new QueryWrapper<NGbCtGzLoan>().eq("subject_num", subjectNumber));
//        for (NGbCtGzLoan gbCtGzLoan : loans) {
//            Map<String, Object> map = new HashMap<>();
//            map.put("subjectNumber",newSubjectNumber);
//            map.put("loanName",gbCtGzLoan.getLoanName());
//            map.put("loanNum",gbCtGzLoan.getLoanNum());
//            map.put("loanMax",gbCtGzLoan.getLoanMax());
//            map.put("loanTime",gbCtGzLoan.getLoanTime());
//            map.put("loanRepayment",gbCtGzLoan.getLoanRepayment());
//            map.put("loanRate",gbCtGzLoan.getLoanRate());
//            loan.add(map);
//        }
//        resultMap.put("loan",loan);
//
//        //discount
//        List<Map<String, Object>> discount = new ArrayList<>();
//        List<NGbCtGzDiscount> discounts = gbCtGzDiscountMapper.selectList(new QueryWrapper<NGbCtGzDiscount>().eq("subject_num", subjectNumber));
//        for (NGbCtGzDiscount gbCtGzDiscount : discounts) {
//            Map<String, Object> map = new HashMap<>();
//            map.put("subjectNumber",newSubjectNumber);
//            map.put("discountName",gbCtGzDiscount.getDiscountName());
//            map.put("discountNum",gbCtGzDiscount.getDiscountNum());
//            map.put("discountTime",gbCtGzDiscount.getDiscountTime());
//            map.put("discountInterest",gbCtGzDiscount.getDiscountInterest());
//            discount.add(map);
//        }
//        resultMap.put("discount",discount);
//
//        //classe
//        List<Map<String, Object>> classe = new ArrayList<>();
//        List<NGbCtGzClasses> classes = gbCtGzClassesMapper.selectList(new QueryWrapper<NGbCtGzClasses>().eq("subject_num", subjectNumber));
//        for (NGbCtGzClasses aClass : classes) {
//            Map<String, Object> map = new HashMap<>();
//            map.put("subjectNumber",newSubjectNumber);
//            map.put("classesName",aClass.getClassesName());
//            map.put("classesNum",aClass.getClassesNum());
//            map.put("outputMulti",aClass.getOutputMulti());
//            map.put("efficiencyLoss",aClass.getEfficiencyLoss());
//            classe.add(map);
//        }
//        resultMap.put("classe",classe);
//
//        //workerIncentive
//        List<Map<String, Object>> workerIncentive = new ArrayList<>();
//        List<NGbCtGzWorkerIncentive> workerIncentives = gbCtGzWorkerIncentiveMapper.selectList(new QueryWrapper<NGbCtGzWorkerIncentive>().eq("subject_num", subjectNumber));
//        for (NGbCtGzWorkerIncentive incentive : workerIncentives) {
//            Map<String, Object> map = new HashMap<>();
//            map.put("subjectNumber",newSubjectNumber);
//            map.put("incentiveName",incentive.getIncentiveName());
//            map.put("incentiveNum",incentive.getIncentiveNum());
//            map.put("efficiencyPercent",incentive.getEfficiencyPercent());
//            workerIncentive.add(map);
//        }
//        resultMap.put("workerIncentive",workerIncentive);
//
//        //num
//        List<Map<String, Object>> num = new ArrayList<>();
//        List<NGbCtGzNum> numList = gbCtGzNumMapper.selectList(new QueryWrapper<NGbCtGzNum>().eq("subject_num", subjectNumber));
//        for (NGbCtGzNum gbCtGzNum : numList) {
//            Map<String, Object> map = new HashMap<>();
//            map.put("subjectNumber",newSubjectNumber);
//            map.put("postNum",gbCtGzNum.getPostNum());
//            map.put("consumeMoney",gbCtGzNum.getConsumeMoney());
//            map.put("timeCostQuarter",gbCtGzNum.getTimeCostQuarter());
//            num.add(map);
//        }
//        resultMap.put("num",num);
//
//        String filePath = CommonUtil.getClassesPath() + "templates/gb_contest_to_new_gz.xlsx";
//        String fileName = "新平台模拟单人版";
//        try {
//            EasyPOIUtil.exportMultiSheetExcelByMap(response, fileName + ".xlsx", filePath, resultMap, true);
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//
//    }

    /**
     * 下载所有企业报表
     */
    public void downAllFinancial(HttpServletResponse response, Integer contestId, Integer year) throws IOException {
        String name = "第"+year+"年各组企业报表";
        try {
            response.setHeader("Content-Disposition", "attachment;filename=" + URLEncoder.encode(name + ".xlsx", "utf-8"));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        allFinancial(response.getOutputStream(),contestId,year);
    }

    public String convertObjToString(Object obj){
        if(obj != null){
            return obj.toString();
        }
        return "";
    }


    public void downViewScore(HttpServletResponse response, Integer contestId, Integer currentTime) throws IOException  {
        Integer year = currentTime/10;
        Integer quarterly = currentTime%10;

        String name = "第"+year+"年"+quarterly+"季各组成绩";
        try {
            response.setHeader("Content-Disposition", "attachment;filename=" + URLEncoder.encode(name + ".xlsx", "utf-8"));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        viewScore(response.getOutputStream(),contestId,year,quarterly);
    }


    @Autowired
    private NGbCtScoreMapper nGbCtScoreMapper;

    /***
     * 企业报表
     * @param outputStream
     * @param contestId
     * @param year
     */
    public void viewScore(OutputStream outputStream,Integer contestId, Integer year,Integer quarterly) {
        ExcelWriter writer = new ExcelWriter(outputStream, ExcelTypeEnum.XLSX);
        Map<Integer, String> studentIdAndAccountMap = gbContestStudentService.getStudentIdAndAccountMap(contestId);


        //获取本年本季度的成绩
        List<NGbCtScore> list = nGbCtScoreMapper.selectList(new LambdaQueryWrapper<NGbCtScore>()
                .eq(NGbCtScore::getContestId,contestId)
                .eq(NGbCtScore::getYear,year)
                .eq(NGbCtScore::getQuarterly,quarterly)
        );
        //根据得分进行排名
        list.sort(Comparator.comparingLong(NGbCtScore::getScore).reversed()); // 降序排序
        int currentRank = 1; // 初始化排名为1
        long currentScore = list.get(0).getScore(); // 当前遍历到的分数

        // 设置排名
        for (int index = 0; index < list.size(); index++) {
            //判断当前的得分 是否 和上一名的一样,如果一样的话,排名用上一名的排名
            NGbCtScore nGbCtScore = list.get(index);
            Integer studentId = nGbCtScore.getStudentId();
            nGbCtScore.setGroupNum(studentIdAndAccountMap.get(studentId));  //企业名

            // 如果当前学生的分数与上一个学生的分数不同，则更新排名
            if (nGbCtScore.getScore() != currentScore) {
                currentRank = index + 1; // 排名从1开始
                currentScore = nGbCtScore.getScore(); // 更新当前分数
            }
            nGbCtScore.setRank(currentRank); // 设置当前学生的排名
        }


        Sheet sheet = new Sheet(1, 0);
        // 设置每列宽度
        Map<Integer, Integer> columnWidth = new HashMap<>();
        for (int i = 0; i <= 7; i++) {
            columnWidth.put(i, 4000);
        }
        sheet.setColumnWidthMap(columnWidth);
        sheet.setSheetName(year+"年"+quarterly+"季");
        sheet.setStartRow(1);   // 前面空一行


        /*表格样式*/
        TableStyle tableStyle = new TableStyle();
        Font font = new Font();
        font.setFontName("Arial");
        font.setFontHeightInPoints((short) 10);
        tableStyle.setTableHeadFont(font);
        tableStyle.setTableHeadBackGroundColor(IndexedColors.GREY_25_PERCENT);
        tableStyle.setTableContentFont(font);
        tableStyle.setTableContentBackGroundColor(IndexedColors.WHITE);

        Table table1 = new Table(1);
        String title = "第"+year+"年"+quarterly+"季";
        List<List<String>> head = new ArrayList<>();

        List<String> headCoulumn = new ArrayList<>();
        headCoulumn.add(title);
        headCoulumn.add("排名");
        head.add(headCoulumn);


        List<String> headCoulumn1 = new ArrayList<>();
        headCoulumn1.add(title);
        headCoulumn1.add("企业名");
        head.add(headCoulumn1);

        List<String> headCoulumn2 = new ArrayList<>();
        headCoulumn2.add(title);
        headCoulumn2.add("权益值");
        head.add(headCoulumn2);

        List<String> headCoulumn3 = new ArrayList<>();
        headCoulumn3.add(title);
        headCoulumn3.add("商誉");
        head.add(headCoulumn3);


        List<String> headCoulumn4 = new ArrayList<>();
        headCoulumn4.add(title);
        headCoulumn4.add("数字化");
        head.add(headCoulumn4);

        List<String> headCoulumn5 = new ArrayList<>();
        headCoulumn5.add(title);
        headCoulumn5.add("扣分");
        head.add(headCoulumn5);

        List<String> headCoulumn6 = new ArrayList<>();
        headCoulumn6.add(title);
        headCoulumn6.add("碳中和率");
        head.add(headCoulumn6);

        List<String> headCoulumn7 = new ArrayList<>();
        headCoulumn7.add(title);
        headCoulumn7.add("总分");
        head.add(headCoulumn7);

        table1.setHead(head);
        table1.setTableStyle(tableStyle);

        List<List<String>> data = new ArrayList<>();

        for (NGbCtScore nGbCtScore : list) {
            List<String> data1 = new ArrayList<>();
            data1.add(convertObjToString(nGbCtScore.getRank())); //排名
            data1.add(convertObjToString(nGbCtScore.getGroupNum())); //企业名
            data1.add(convertObjToString(nGbCtScore.getEquity())); //权益值
            data1.add(convertObjToString(nGbCtScore.getSy())); //商誉
            data1.add(convertObjToString(nGbCtScore.getSzh())); //数字化
            data1.add(convertObjToString(nGbCtScore.getKf())); //扣分
            data1.add(convertObjToString(nGbCtScore.getNeutralizaRate())); //碳中和率
            data1.add(convertObjToString(nGbCtScore.getScore())); //总分
            data.add(data1);
        }
        writer.write0(data, sheet, table1);

        writer.finish();
    }




}
