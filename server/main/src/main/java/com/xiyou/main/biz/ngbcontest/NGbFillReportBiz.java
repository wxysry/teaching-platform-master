package com.xiyou.main.biz.ngbcontest;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.xiyou.common.utils.R;
import com.xiyou.main.dao.ngbcontest.NGbContestStudentMapper;
import com.xiyou.main.entity.ngbcontest.*;
import com.xiyou.main.params.ngbcontest.NGbFillReportParam;
import com.xiyou.main.service.ngbcontest.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @program: multi-module
 * @description:
 * @author: wangxingyu
 * @create: 2023-06-28 21:42
 **/
@Service
@Transactional(rollbackFor = Exception.class)
public class NGbFillReportBiz {
    @Autowired
    private NGbCtProfitChartService gbCtProfitChartService;
    @Autowired
    private NGbCtChargesService gbCtChargesService;
    @Autowired
    private NGbCtBalanceService gbCtBalanceService;
    @Autowired
    private NGbCtFinancialTargetService gbCtFinancialTargetService;
    @Autowired
    private NGbContestStudentService gbContestStudentService;
    @Autowired
    private  NGbContestStudentMapper gbContestStudentMapper;



    /**
     * 如果老师点击下一回合学生没有填报表，自动填0（记得先提前判断当前是否有填写报表）
     * @param userId
     * @param date
     * @param contestId
     */
    public void fillDefault(Integer userId,Integer date,Integer contestId){
        NGbCtCharges charges =  new NGbCtCharges()
                .setZero()
                .setStudentId(userId)
                .setContestId(contestId)
                .setCDate(date)
                .setBsIsxt(0)
                .setIsSubmit("Y");
        NGbCtProfitChart profitChart = new NGbCtProfitChart()
                .setZero()
                .setStudentId(userId)
                .setContestId(contestId)
                .setPcDate(date)
                .setBsIsxt(0)
                .setIsSubmit("Y");
        NGbCtBalance balance = new NGbCtBalance()
                .setZero()
                .setStudentId(userId)
                .setContestId(contestId)
                .setBsYear(date)
                .setBsIsxt(0)
                .setIsSubmit("Y");
        NGbCtFinancialTarget financialTarget= new NGbCtFinancialTarget()
                .setZero()
                .setStudentId(userId)
                .setContestId(contestId)
                .setFtYear(date)
                .setFtIsxt(0)
                .setIsSubmit("Y");

        NGbFillReportParam gbFillReportParam =  new NGbFillReportParam();
        gbFillReportParam
                .setCharges(charges)
                .setProfitChart(profitChart)
                .setBalance(balance)
                .setFinancialTarget(financialTarget);
        //填写报表
        batchFill(userId,gbFillReportParam);
    }

    /**
     * 填写四张报表
     * @param userId
     * @param gbFillReportParam
     * @return
     */
    public R batchFill(Integer userId, NGbFillReportParam gbFillReportParam) {
        Boolean fillCharges = addCharges(userId,gbFillReportParam.getCharges());
        Boolean fillProfitChart = addProfitChart(userId, gbFillReportParam.getProfitChart());
        Boolean fillBalance = addBalance(userId,gbFillReportParam.getBalance());
        Boolean fillFinancialTarget = addFinancialTarget(userId,gbFillReportParam.getFinancialTarget());

        NGbCtCharges gbCtCharges=gbFillReportParam.getCharges();
        //判断是否暂存
        if("N".equals(gbCtCharges.getIsSubmit())){
            return R.success("暂存成功");
        }else{
            //更新学生的填表状态
            gbContestStudentMapper.update(new NGbContestStudent().setProgress("{\"fillReportForm\":false}"),
                    new LambdaQueryWrapper<NGbContestStudent>()
                    .eq(NGbContestStudent::getContestId,gbFillReportParam.getCharges().getContestId())
                    .eq(NGbContestStudent::getStudentId,userId)
            );
        }

        //保存当季结束时间
        int date = gbFillReportParam.getCharges().getCDate()*10+4;
        int contestId = gbFillReportParam.getCharges().getContestId();
        gbContestStudentService.insertStudentEndTime(contestId,userId,date);


        if(fillCharges&&fillProfitChart&&fillBalance&&fillFinancialTarget){
            return R.success("报表正确");
        }else{
            int year = gbCtCharges.getCDate() >= 10 ? (gbCtCharges.getCDate() / 10) : gbCtCharges.getCDate();
            NGbContestStudent contestStudent = gbContestStudentService.get(gbCtCharges.getContestId(), userId);
            gbContestStudentService.updateErrorReportYear(contestStudent, year);
            return R.success("报表错误");
        }
    }



    /**
     * 填写综合费用表
     * @param studentId
     * @param charges
     * @return
     */
    public boolean addCharges(Integer studentId, NGbCtCharges charges) {
        if (charges.getStudentId() == null) {
            charges.setStudentId(studentId);
        }
        charges.setBsIsxt(0);
        if (charges.getCDate() > 10) {
            charges.setCDate(charges.getCDate() / 10);
        }

        if(StringUtils.isEmpty(charges.getChargesId())){
            gbCtChargesService.save(charges);
        }else{
            gbCtChargesService.updateById(charges);
        }

        //判断是暂存还是提交
        if("Y".equals(charges.getIsSubmit())){
            boolean correct = true;
            // 同时判断这三张表当年填入0和1的记录是否相同，如果不同则提示报表错误，否则提示报表正确。
            NGbCtCharges sysCharges = gbCtChargesService.getSys(studentId, charges.getContestId(), charges.getCDate());
            if (sysCharges == null
                    || !charges.getCOverhaul().equals(sysCharges.getCOverhaul())
                    || !charges.getCAd().equals(sysCharges.getCAd())
                    || !charges.getCMaintenance().equals(sysCharges.getCMaintenance())
                    || !charges.getCTransfer().equals(sysCharges.getCTransfer())
                    || !charges.getCDevelopMarket().equals(sysCharges.getCDevelopMarket())
                    || !charges.getCDevelopIso().equals(sysCharges.getCDevelopIso())
                    || !charges.getCDevelopProduct().equals(sysCharges.getCDevelopProduct())
                    || !charges.getCInformation().equals(sysCharges.getCInformation())

                    || !charges.getCProductDesign().equals(sysCharges.getCProductDesign())
                    || !charges.getCDismissFee().equals(sysCharges.getCDismissFee())
                    || !charges.getCTrainFee().equals(sysCharges.getCTrainFee())
                    || !charges.getCIncentiveFee().equals(sysCharges.getCIncentiveFee())
                    || !charges.getCHr().equals(sysCharges.getCHr())
                    || !charges.getCCarbon().equals(sysCharges.getCCarbon())
                    || !charges.getCFeature().equals(sysCharges.getCFeature())

                    || !charges.getCDigitalization().equals(sysCharges.getCDigitalization())
                    || !charges.getCTotal().equals(sysCharges.getCTotal())
            ) {
                correct = false;
            }
            if (correct) {
                return true;
            }
            return false;
        }else{
            return true;
        }
    }


    /**
     * 填写利润表
     * @param studentId
     * @param profitChart
     * @return
     */
    public boolean addProfitChart(Integer studentId, NGbCtProfitChart profitChart) {
        if (profitChart.getStudentId() == null) {
            profitChart.setStudentId(studentId);
        }
        profitChart.setBsIsxt(0);
        if (profitChart.getPcDate() > 10) {
            profitChart.setPcDate(profitChart.getPcDate() / 10);
        }
        if(StringUtils.isEmpty(profitChart.getProfitChartId())){
            gbCtProfitChartService.save(profitChart);
        }else{
            gbCtProfitChartService.updateById(profitChart);
        }

        if("Y".equals(profitChart.getIsSubmit())){
            boolean correct = true;
            // 同时判断这三张表当年填入0和1的记录是否相同，如果不同则提示报表错误，否则提示报表正确。
            NGbCtProfitChart sysProfitChart = gbCtProfitChartService.getSys(profitChart);
            if (sysProfitChart == null
                    || !profitChart.getPcSales().equals(sysProfitChart.getPcSales())
                    || !profitChart.getPcDirectCost().equals(sysProfitChart.getPcDirectCost())
                    || !profitChart.getPcGoodsProfit().equals(sysProfitChart.getPcGoodsProfit())
                    || !profitChart.getPcTotal().equals(sysProfitChart.getPcTotal())
                    || !profitChart.getPcProfitBeforeDep().equals(sysProfitChart.getPcProfitBeforeDep())
                    || !profitChart.getPcDep().equals(sysProfitChart.getPcDep())
                    || !profitChart.getPcProfitBeforeInterests().equals(sysProfitChart.getPcProfitBeforeInterests())
                    || !profitChart.getPcFinanceFee().equals(sysProfitChart.getPcFinanceFee())
                    || !profitChart.getPcNonOperating().equals(sysProfitChart.getPcNonOperating())
                    || !profitChart.getPcProfitBeforeTax().equals(sysProfitChart.getPcProfitBeforeTax())
                    || !profitChart.getPcTax().equals(sysProfitChart.getPcTax())
                    || !profitChart.getPcAnnualNetProfit().equals(sysProfitChart.getPcAnnualNetProfit())) {
                correct = false;
            }
            if (correct) {
                return true;
            }
            return false;
        }else{
            return true;
        }
    }


    /**
     * 填写资产负债表
     * @param studentId
     * @param balance
     * @return
     */
    public boolean addBalance(Integer studentId, NGbCtBalance balance) {
        if (balance.getStudentId() == null) {
            balance.setStudentId(studentId);
        }
        balance.setBsIsxt(0);
        if (balance.getBsYear() > 10) {
            balance.setBsYear(balance.getBsYear() / 10);
        }
        if(StringUtils.isEmpty(balance.getBalanceId())){
            gbCtBalanceService.save(balance);
        }else{
            gbCtBalanceService.updateById(balance);
        }

        if("Y".equals(balance.getIsSubmit())){
            boolean correct = true;
            // 同时判断这三张表当年填入0和1的记录是否相同，如果不同则提示报表错误，否则提示报表正确。
            NGbCtBalance sysBalance = gbCtBalanceService.get(balance);
            if (sysBalance == null
                    || !balance.getBsCash().equals(sysBalance.getBsCash())
                    || !balance.getBsReceivable().equals(sysBalance.getBsReceivable())
                    || !balance.getBsProductInProcess().equals(sysBalance.getBsProductInProcess())
                    || !balance.getBsProduct().equals(sysBalance.getBsProduct())
                    || !balance.getBsMaterial().equals(sysBalance.getBsMaterial())
                    || !balance.getBsTotalCurrentAsset().equals(sysBalance.getBsTotalCurrentAsset())
                    || !balance.getBsEquipment().equals(sysBalance.getBsEquipment())
                    || !balance.getBsProjectOnConstruction().equals(sysBalance.getBsProjectOnConstruction())
                    || !balance.getBsTotalFixedAsset().equals(sysBalance.getBsTotalFixedAsset())
                    || !balance.getBsTotalAsset().equals(sysBalance.getBsTotalAsset())
                    || !balance.getBsLongLoan().equals(sysBalance.getBsLongLoan())
                    || !balance.getBsShortLoan().equals(sysBalance.getBsShortLoan())
                    || !balance.getBsOtherPay().equals(sysBalance.getBsOtherPay())
                    || !balance.getBsTax().equals(sysBalance.getBsTax())
                    || !balance.getBsTotalLiability().equals(sysBalance.getBsTotalLiability())
                    || !balance.getBsEquity().equals(sysBalance.getBsEquity())
                    || !balance.getBsRetainedEarning().equals(sysBalance.getBsRetainedEarning())
                    || !balance.getBsAnnualNetProfit().equals(sysBalance.getBsAnnualNetProfit())
                    || !balance.getBsTotalEquity().equals(sysBalance.getBsTotalEquity())
                    || !balance.getBsTotal().equals(sysBalance.getBsTotal())) {
                correct = false;
            }
            if (correct) {
                return true;
            }
            return false;
        }else{
            return true;
        }
    }


    /**
     * 填写财务指标
     * @param studentId
     * @param ctFinancialTarget
     * @return
     */
    public boolean addFinancialTarget(Integer studentId, NGbCtFinancialTarget ctFinancialTarget) {
        if (ctFinancialTarget.getStudentId() == null) {
            ctFinancialTarget.setStudentId(studentId);
        }
        ctFinancialTarget.setFtIsxt(0);
        if (ctFinancialTarget.getFtYear() > 10) {
            ctFinancialTarget.setFtYear(ctFinancialTarget.getFtYear() / 10);
        }
        if(StringUtils.isEmpty(ctFinancialTarget.getFtId())){
            gbCtFinancialTargetService.save(ctFinancialTarget);
        }else{
            gbCtFinancialTargetService.updateById(ctFinancialTarget);
        }

        if("Y".equals(ctFinancialTarget.getIsSubmit())){

            boolean correct = true;
            // 同时判断这三张表当年填入0和1的记录是否相同，如果不同则提示报表错误，否则提示报表正确。
            NGbCtFinancialTarget sysTarget = gbCtFinancialTargetService.get(ctFinancialTarget);
            if (sysTarget == null
                    || ctFinancialTarget.getFtCurrentRate().compareTo(sysTarget.getFtCurrentRate())!=0
                    || ctFinancialTarget.getFtQuickRate().compareTo(sysTarget.getFtQuickRate())!=0
                    || ctFinancialTarget.getFtDebtRate().compareTo(sysTarget.getFtDebtRate())!=0
                    || ctFinancialTarget.getFtEquityRate().compareTo(sysTarget.getFtEquityRate())!=0
                    || ctFinancialTarget.getFtNetProfitRate().compareTo(sysTarget.getFtNetProfitRate())!=0
                    || ctFinancialTarget.getFtCostExpenseRate().compareTo(sysTarget.getFtCostExpenseRate())!=0
                    || ctFinancialTarget.getFtReturnAssetsRate().compareTo(sysTarget.getFtReturnAssetsRate())!=0
                    || ctFinancialTarget.getFtReturnEquityRate().compareTo(sysTarget.getFtReturnEquityRate())!=0
                    || ctFinancialTarget.getFtRevenueGrowthRate().compareTo(sysTarget.getFtRevenueGrowthRate())!=0
                    || ctFinancialTarget.getFtAppreciationRate().compareTo(sysTarget.getFtAppreciationRate())!=0
                    || ctFinancialTarget.getFtTotalGrowthRate().compareTo(sysTarget.getFtTotalGrowthRate())!=0
                    || ctFinancialTarget.getFtInventoryRate().compareTo(sysTarget.getFtInventoryRate())!=0
                    || ctFinancialTarget.getFtInventoryDays().compareTo(sysTarget.getFtInventoryDays())!=0
                    || ctFinancialTarget.getFtReceivableRate().compareTo(sysTarget.getFtReceivableRate())!=0
                    || ctFinancialTarget.getFtReceivableDays().compareTo(sysTarget.getFtReceivableDays())!=0
                    || ctFinancialTarget.getFtCashPeriod().compareTo(sysTarget.getFtCashPeriod())!=0
            ) {
                correct = false;
            }

            if (correct) {
                return true;
            }
            return false;
        }else{
            return false;
        }
    }

    /**
     * 获取暂存数据
     * @param studentId
     * @param contestId
     * @param year
     * @return
     */
    public R getTemp(Integer studentId, Integer contestId, Integer year) {
        NGbCtCharges charges=gbCtChargesService.getTemp(studentId, contestId, year);
        NGbCtProfitChart profitChart=gbCtProfitChartService.getTemp(studentId, contestId, year);
        NGbCtBalance balance = gbCtBalanceService.getTemp(studentId, contestId, year);
        NGbCtFinancialTarget financialTarget= gbCtFinancialTargetService.getTemp(studentId,contestId,year);
        Map<String, Object> list = new HashMap<>();
        list.put("charges",charges);
        list.put("profitChart",profitChart);
        list.put("balance",balance);
        list.put("financialTarget",financialTarget);
        return R.success(list);

    }


    /**
     * 获取系统自动生成的报表数据
     * @param studentId
     * @param contestId
     * @param year
     * @return
     */
    public R getAutoFill(Integer studentId, Integer contestId, Integer year) {
        NGbCtCharges charges=gbCtChargesService.getSys(studentId, contestId, year);
        NGbCtProfitChart profitChart=gbCtProfitChartService.getSys(new NGbCtProfitChart().setStudentId(studentId).setContestId(contestId).setPcDate(year));
        NGbCtBalance balance = gbCtBalanceService.get(new NGbCtBalance().setStudentId(studentId).setContestId(contestId).setBsYear(year));
        NGbCtFinancialTarget financialTarget= gbCtFinancialTargetService.get(new NGbCtFinancialTarget().setStudentId(studentId).setContestId(contestId).setFtYear(year));
        Map<String, Object> list = new HashMap<>();
        list.put("charges",charges);
        list.put("profitChart",profitChart);
        list.put("balance",balance);
        list.put("financialTarget",financialTarget);
        return R.success(list);

    }

    /**
     * 获取报告填写时间
     * @param contestId
     * @param year
     * @return
     */
    public R getFillReportResult(Integer contestId, Integer year) {
        int date = year*10+4;
        List<NGbContestStudent> list = gbContestStudentMapper.getListByContestId(contestId);
        List<Map<String,Object>> result = new ArrayList<>();
        for (NGbContestStudent gbContestStudent : list) {
            String endTimeByStr = gbContestStudentService.getEndTimeByStr(gbContestStudent.getEverySeasonEndTime(), date);
            Map<String,Object> map = new HashMap<>();
            map.put("account",gbContestStudent.getAccount());
            map.put("endTime",endTimeByStr);
            if(endTimeByStr!=null){
                //判断本年的填写结果是否正确
                //先获取报表错误年份
                String errorReportYear = gbContestStudent.getErrorReportYear();
                if (errorReportYear!=null && errorReportYear.contains(String.valueOf(year))) {
                    map.put("result","错误");
                }else{
                    map.put("result","正确");
                }
            }else{
                map.put("result","");
            }
            result.add(map);
        }

        return R.success().put("list",result);
    }
}
