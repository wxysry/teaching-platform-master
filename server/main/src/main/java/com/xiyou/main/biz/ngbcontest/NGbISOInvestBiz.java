package com.xiyou.main.biz.ngbcontest;

import com.xiyou.common.enums.CodeEnum;
import com.xiyou.common.utils.R;
import com.xiyou.main.dao.ngbcontest.NGbCtCashflowMapper;
import com.xiyou.main.dao.ngbcontest.NGbCtGzIsoMapper;
import com.xiyou.main.dao.ngbcontest.NGbCtYfIsoMapper;
import com.xiyou.main.entity.ngbcontest.NGbCtCashflow;
import com.xiyou.main.entity.ngbcontest.NGbCtGzIso;
import com.xiyou.main.entity.ngbcontest.NGbCtYfIso;
import com.xiyou.main.params.ngbcontest.NGbISOInvestParam;
import com.xiyou.main.service.ngbcontest.NGbCtCashflowService;
import com.xiyou.main.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

/**
 * @program: multi-module
 * @description:
 * @author: tangcan
 * @create: 2019-09-01 12:43
 **/
@Service
@Transactional(rollbackFor = Exception.class)
public class NGbISOInvestBiz {
    @Autowired
    private NGbCtYfIsoMapper ctYfIsoMapper;
    @Autowired
    private NGbCtCashflowService ctCashflowService;
    @Autowired
    private NGbCtCashflowMapper cashflowMapper;
    @Autowired
    private NGbCtGzIsoMapper gbCtGzIsoMapper;
    @Autowired
    private NGbActionBiz gbActionBiz;

    public R list(Integer contestId, Integer studentId) {
        List<Map<String, Object>> list = ctYfIsoMapper.getIsoInvestList(studentId, contestId);
        return R.success().put("isoList", list);
    }

    public R commit(NGbISOInvestParam isoInvestParam) {

        //阻止暂停状态下 和 破产状态下的用户进行操作
        gbActionBiz.preventAction(isoInvestParam.getContestId(),isoInvestParam.getStudentId());

        if (isoInvestParam.getIsoId() == null) {
            return R.error(CodeEnum.PARAM_ERROR.getCode(), "请选择ISO投资");
        }
        // 现金
        Integer cash = cashflowMapper.getCash(isoInvestParam.getStudentId(), isoInvestParam.getContestId());
        if (cash == null) {
            cash = 0;
        }

        NGbCtYfIso gbCtYfIso = ctYfIsoMapper.selectById(isoInvestParam.getIsoId());
        if (NGbCtYfIso.ISO_YF_ING.equals(gbCtYfIso.getDiState()) || NGbCtYfIso.ISO_YF_FINISH.equals(gbCtYfIso.getDiState())) {
            return R.error(CodeEnum.OTHER_ERROR.getCode(), "无需重复研发");
        }

        NGbCtGzIso gbCtGzIso = gbCtGzIsoMapper.selectById(gbCtYfIso.getDiCiid());
        Integer investSum = gbCtGzIso.getCiDevelopFee();
        // 当现金<sum（勾选的投资费用总和），则报错【现金不足】
        if (cash < investSum) {
            return R.error(CodeEnum.OTHER_ERROR.getCode(), "现金不足");
        }
        // 刷新cashflow，ID、ISO开拓、0、sum（勾选的投资费用总和），现金+增加-减少、扣减ISO开拓&sum（勾选的投资费用总和）、当前时间
        ctCashflowService.save(new NGbCtCashflow()
                .setStudentId(isoInvestParam.getStudentId())
                .setContestId(isoInvestParam.getContestId())
                .setCAction("ISO开拓")
                .setCIn(0)
                .setCOut(investSum)
                .setCSurplus(cash - investSum)
                .setCComment("扣减ISO"+gbCtGzIso.getCiName()+"开拓" + investSum + "元")
                .setCDate(isoInvestParam.getDate()));
        // 刷新yf_ISO,勾选记录now_date +1，Remain_Date -1
        Integer ciDevelopDate = gbCtGzIso.getCiDevelopDate();
        Integer finishDate = DateUtils.addYearAndSeasonTime(isoInvestParam.getDate(),ciDevelopDate);
        if(ciDevelopDate != null && ciDevelopDate>0 ){
            gbCtYfIso.setDiState(NGbCtYfIso.ISO_YF_ING);
        }else {
            gbCtYfIso.setDiState(NGbCtYfIso.ISO_YF_FINISH);
        }
        gbCtYfIso.setDiStartDate(isoInvestParam.getDate())
                .setDiFinishDate(finishDate);
        ctYfIsoMapper.updateById(gbCtYfIso);

        //计算报表,判断是否破产
        gbActionBiz.isBankruptcy(isoInvestParam.getStudentId(),isoInvestParam.getContestId(),isoInvestParam.getDate());

        return R.success();
    }
}
