package com.xiyou.main.biz.ngbcontest;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.xiyou.common.enums.CodeEnum;
import com.xiyou.common.exception.CustomException;
import com.xiyou.common.utils.R;
import com.xiyou.main.dao.ngbcontest.*;
import com.xiyou.main.entity.ngbcontest.*;
import com.xiyou.main.enums.CashFlowActionEnum;
import com.xiyou.main.enums.ErrorEnum;
import com.xiyou.main.params.ngbcontest.NGbLineParam;
import com.xiyou.main.service.ngbcontest.*;
import com.xiyou.main.utils.DateUtils;
import com.xiyou.main.vo.ngbcontest.NGbCtKcMaterialNumVo;
import com.xiyou.main.vo.ngbcontest.NGbOnlineLine;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.*;


/**
 * @author dingyoumeng
 * @since 2019/07/22
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class NGbLineBiz {

    @Autowired
    private NGbCtLineService lineService;
    @Autowired
    private NGbCtLineMapper gbCtLineMapper;
    @Autowired
    private NGbCtCashflowService ctCashflowService;
    @Autowired
    private NGbCtGzProductLineService ctGzProductLineService;
    @Autowired
    private NGbCtKcMaterialService ctKcMaterialService;
    @Autowired
    private NGbCtGzProductService ctGzProductService;
    @Autowired
    private NGbCtKcProductMapper ctKcProductMapper;
    @Autowired
    private NGbCtGzProductLineMapper gbCtGzProductLineMapper;
    @Autowired
    private NGbCtGzProducingMapper gbCtGzProducingMapper;
    @Autowired
    private NGbCtKcMaterialMapper gbCtKcMaterialMapper;
    @Autowired
    private NGbCtGzProductMapper gbCtGzProductMapper;
    @Autowired
    private NGbCtGzMaterialMapper gbCtGzMaterialMapper;
    @Autowired
    private NGbCtWorkerMapper gbCtWorkerMapper;
    @Autowired
    private NGbActionBiz gbActionBiz;
    @Autowired
    private NGbCtGzCsMapper gbCtGzCsMapper;

    @Autowired
    private NGbCtCarbonMapper nGbCtCarbonMapper;

    @Autowired
    private NGbContestStudentService gbContestStudentService;


    @Autowired
    private NGbCtGzClassesMapper nGbCtGzClassesMapper;


    public R add(NGbCtLine line) {
        //阻止暂停状态下 和 破产状态下的用户进行操作
        gbActionBiz.preventAction(line.getContestId(),line.getStudentId());
        int countLine = gbCtLineMapper.getCountLine(line.getContestId(), line.getStudentId());
        NGbCtGzCs gzCs = gbCtGzCsMapper.getByContestId(line.getContestId());
        if(countLine >=gzCs.getMaxLine()){
            return ErrorEnum.LINE_MULTI.getR();
        }

        //获取转产后最新的 产品特性
        NGbCtDesignFeature nGbCtDesignFeature = nGbCtDesignFeatureMapper.selectOne(new LambdaQueryWrapper<NGbCtDesignFeature>()
                .eq(NGbCtDesignFeature::getContestId,line.getContestId())
                .eq(NGbCtDesignFeature::getStudentId,line.getStudentId())
                .eq(NGbCtDesignFeature::getCpId,line.getPlCpid())
                .orderByDesc(NGbCtDesignFeature::getId)
                .last(" limit 1")
        );
        //如果产品特性不等于空,则获取最新的特性
        if(nGbCtDesignFeature!=null){
            line.setFeatureNum(nGbCtDesignFeature.getFeatureNum());
            line.setFeatureName(nGbCtDesignFeature.getFeatureName());
        }


        //获取现金
        Integer cash = ctCashflowService.getCash(line.getStudentId(), line.getContestId());
        //生产线类型编号
        Integer cplid = line.getPlCplid();

        NGbCtGzProductLine ctGzProductLine = ctGzProductLineService.getById(cplid);

        NGbCtGzProduct ctGzProduct = ctGzProductService.getById(line.getPlCpid());
        int remain = cash - ctGzProductLine.getCplBuyFee();
        if (remain < 0) {
            return ErrorEnum.CASH_NOT_ENOUGH.getR();
        }
        line.setPlInvest(ctGzProductLine.getCplBuyFee());//原值
        Integer addDate = ctGzProductLine.getCplInstallDate();
        Integer finishDate = DateUtils.addYearAndSeasonTime(line.getPlAddTime(), addDate);
        line.setPlFinishDate(finishDate);
        line.setPlRemainDate(addDate);
        if(addDate == 0){
            line.setStatus(NGbCtLine.ON_SPACE);
        }else {
            line.setStatus(NGbCtLine.ON_BUILD);//状态
        }
        line.setCplName(ctGzProductLine.getCplName());//产线名称
        line.setPlDepTotal(0);
        line.setPlProductAddDate(null);
        line.setPlProductingDate(null);
        line.setPlTransferDate(null);
        line.setPlTransferAddDate(null);
        gbCtLineMapper.insert(line);

        //插入消费记录
        NGbCtCashflow ctCashflow = new NGbCtCashflow();
        ctCashflow.setCDate(line.getPlAddTime())
                .setCSurplus(remain)
                .setCIn(0)
                .setCOut(ctGzProductLine.getCplBuyFee())
                .setCAction(CashFlowActionEnum.BUY_PRODUCT_LINE.getAction())
                .setContestId(line.getContestId())
                .setStudentId(line.getStudentId())
                .setCComment("新建生产线：" + ctGzProductLine.getCplName() + (ctGzProduct == null ? "" : ctGzProduct.getCpName()));
        ctCashflowService.save(ctCashflow);


        //计算报表,判断是否破产
        gbActionBiz.isBankruptcy(line.getStudentId(),line.getContestId(),line.getPlAddTime());
        return R.success();
    }


    /**
     * 获取所有生产线
     * @param contestId
     * @param studentId
     * @return
     */
    public R getAllLineList(Integer contestId,Integer studentId) {
        //获取所有的产线-排除掉已经售卖掉的产线
        List<NGbCtLine> allLineList = gbCtLineMapper.getList(contestId, studentId);
        for (NGbCtLine gbCtLine : allLineList) {
            //获取所需高级工人和所需普通工人数
             NGbCtGzProductLine gbCtGzProductLine = gbCtGzProductLineMapper.selectById(gbCtLine.getPlCplid());
            gbCtLine.setCplSeniorWorker(gbCtGzProductLine.getCplSeniorWorker());
            gbCtLine.setCplOrdinaryWorker(gbCtGzProductLine.getCplOrdinaryWorker());


            List<NGbCtWorker> gj = gbCtWorkerMapper.getGjWorkerListByLineId(gbCtLine.getLineId());
            List<Integer> gjId = new ArrayList<>();
            for (NGbCtWorker gbCtWorker : gj) {
                gjId.add(gbCtWorker.getId());
            }
            gbCtLine.setGjWorkerList(gj);
            gbCtLine.setSeniorWorkerList(gjId);


            List<NGbCtWorker> pt = gbCtWorkerMapper.getPtWorkerListByLineId(gbCtLine.getLineId());
            List<Integer> ptId = new ArrayList<>();
            for (NGbCtWorker gbCtWorker : pt) {
                ptId.add(gbCtWorker.getId());
            }
            gbCtLine.setPtWorkerList(pt);
            gbCtLine.setOrdinaryWorkerList(ptId);
        }


        return R.success().put("list",allLineList);
    }




    /**
     * 重新计算产线的产量
     * @return
     */
    public void reCalRealProduction(Integer lineId) {

        NGbCtLine gbCtLine = gbCtLineMapper.selectById(lineId);

        //获取所需高级工人和所需普通工人数
        NGbCtGzProductLine gbCtGzProductLine = gbCtGzProductLineMapper.selectById(gbCtLine.getPlCplid());

        Integer cplSeniorWorker = gbCtGzProductLine.getCplSeniorWorker(); //所需高级技工
        Integer cplOrdinaryWorker = gbCtGzProductLine.getCplOrdinaryWorker(); //所需手工技工

        //获取当前的高级技工
        List<NGbCtWorker> gj = gbCtWorkerMapper.getGjWorkerListByLineId(gbCtLine.getLineId());

        //获取当前的初级及公共
        List<NGbCtWorker> pt = gbCtWorkerMapper.getPtWorkerListByLineId(gbCtLine.getLineId());

        //获取当前的班次规则
        Integer classesId = gbCtLine.getClassesId();
        double outputMulti = 0;
        if(!StringUtils.isEmpty(classesId)){
            NGbCtGzClasses nGbCtGzClasses = nGbCtGzClassesMapper.selectById(classesId);
            //班次加成
            outputMulti = nGbCtGzClasses.getOutputMulti();
        }

        //产量计算
        Integer realProduction = 0;
        //高级技工人数不符产量为0
        if(gj.size()!=cplSeniorWorker){
            realProduction = 0;
        }
        //初级技工人数不符产量为0
        else if(pt.size()!=cplOrdinaryWorker){
            realProduction = 0;
        }
        //计算公式为：基础产能*（1+手工工人效率/4+高级技工效率）*班次加成
        else{
            //高级效率汇总
            int seniorEfficiencySum = gj.stream().mapToInt(NGbCtWorker::getMultBonus).sum();
            //初级效率汇总
            int ordinaryEfficiencySum = pt.stream().mapToInt(NGbCtWorker::getMultBonus).sum();
            //基础产能
            int cplProduction = gbCtGzProductLine.getCplProduction();

            BigDecimal result = new BigDecimal(cplProduction).multiply(
                    new BigDecimal(100).add(new BigDecimal(seniorEfficiencySum)).add(new BigDecimal(ordinaryEfficiencySum).divide(new BigDecimal(4)))
                    .divide(new BigDecimal(100))
            ).multiply(new BigDecimal(outputMulti)).setScale(0,RoundingMode.DOWN);
            realProduction = result.intValue();
        }

        //更新产量
        gbCtLine.setRealProduction(realProduction);
        gbCtLineMapper.updateById(gbCtLine);
    }





    public R listOnline(Integer userId, Integer contestId, Integer date) {
        List<NGbOnlineLine> list = lineService.listOnline(userId, contestId, date);
        Map<String, Object> resultMap = new HashMap<>(16);
        resultMap.put("list", list);
        return R.success(resultMap);
    }





    @Autowired(required = false)
    private NGbCtDesignFeatureMapper nGbCtDesignFeatureMapper;


    /***
     * 转产
     * @param userId
     * @param contestId
     * @param lineId
     * @param cpId
     * @param currentTime
     * @return
     */
    public R lineTransfer(Integer userId,Integer contestId,Integer lineId,Integer cpId,Integer currentTime) {

        //阻止暂停状态下 和 破产状态下的用户进行操作
        gbActionBiz.preventAction(contestId,userId);

        //判断产线的状态
        NGbCtLine line = gbCtLineMapper.selectById(lineId);

        if(NGbCtLine.SALED.equals(line.getStatus())){
            throw new CustomException(CodeEnum.OTHER_ERROR.getCode(), "当前产线已被售卖");
        }

        if(!NGbCtLine.ON_SPACE.equals(line.getStatus())){
            throw new CustomException(CodeEnum.OTHER_ERROR.getCode(), "当前产线繁忙");
        }


        Integer cash = ctCashflowService.getCash(userId, contestId);
        if(line.getPlCpid().equals(cpId)){
            return ErrorEnum.TRANSFER_WORING.getR();
        }


//        //获取转产后最新的 产品特性
//        NGbCtDesignFeature nGbCtDesignFeature = nGbCtDesignFeatureMapper.selectOne(new LambdaQueryWrapper<NGbCtDesignFeature>()
//                .eq(NGbCtDesignFeature::getContestId,contestId)
//                .eq(NGbCtDesignFeature::getStudentId,userId)
//                .eq(NGbCtDesignFeature::getCpId,cpId)
//                .orderByDesc(NGbCtDesignFeature::getId)
//                .last(" limit 1")
//        );
//        //如果产品特性不等于空,则获取最新的特性
//        if(nGbCtDesignFeature!=null){
//            line.setFeatureNum(nGbCtDesignFeature.getFeatureNum());
//            line.setFeatureName(nGbCtDesignFeature.getFeatureName());
//        }


        //获取生产线规则
        NGbCtGzProductLine gbCtGzProductLine = gbCtGzProductLineMapper.selectById(line.getPlCplid());

        int remain = cash - gbCtGzProductLine.getCplTransferFee();
        if (remain < 0) {
            return ErrorEnum.CASH_NOT_ENOUGH.getR();
        }
        //插入现金交易记录
        NGbCtCashflow ctCashflow = new NGbCtCashflow();
        NGbCtGzProduct ctGzProduct = ctGzProductService.getById(cpId);
        ctCashflow.setStudentId(userId)
                .setContestId(contestId)
                .setCAction(CashFlowActionEnum.PRODUCT_LINE_TRANSFER.getAction())
                .setCComment("生产线" + line.getLineId().toString() + "转产为" + (ctGzProduct == null ? "" : ctGzProduct.getCpName()))
                .setCIn(0)
                .setCOut(gbCtGzProductLine.getCplTransferFee())//转产费用
                .setCDate(currentTime)
                .setCSurplus(remain);
        ctCashflowService.save(ctCashflow);

        Integer finishDate = DateUtils.addYearAndSeasonTime(currentTime, gbCtGzProductLine.getCplTransferDate());
        line.setPlTransferAddDate(currentTime)//转产开始时间
            .setPlCpid(cpId)//转产结束时间
            .setPlTransferDate(finishDate);//转产结束时间
        if(gbCtGzProductLine.getCplTransferDate() == null || gbCtGzProductLine.getCplTransferDate() == 0){
            line.setStatus(NGbCtLine.ON_SPACE);
        }else {
            line.setStatus(NGbCtLine.ON_TRANSFER);
        }
        //更新产线状态,并且清空特性
        gbCtLineMapper.update(line,new UpdateWrapper<NGbCtLine>().lambda()
                .eq(NGbCtLine::getLineId,line.getLineId())
                .set(NGbCtLine::getFeatureName,null)
                .set(NGbCtLine::getFeatureNum,null)
        );

        //计算报表,判断是否破产
        gbActionBiz.isBankruptcy(userId,line.getContestId(),currentTime);

        return R.success();
    }

    /**
     * 出售
     * @param userId
     * @param line
     * @return
     */
    public R lineSell(Integer userId, NGbCtLine line) {

        //阻止暂停状态下 和 破产状态下的用户进行操作
        gbActionBiz.preventAction(line.getContestId(),userId);

        Integer actionTime = line.getActionTime();

        if (line.getLineId() == null) {
            return R.error(CodeEnum.PARAM_MISS.getCode(), "请选择生产线");
        }

        //判断产线当前的状态
        line = gbCtLineMapper.selectById(line.getLineId());

        if(NGbCtLine.SALED.equals(line.getStatus())){
            throw new CustomException(CodeEnum.OTHER_ERROR.getCode(), "当前产线已被售卖");
        }

        if(!NGbCtLine.ON_SPACE.equals(line.getStatus())){
            throw new CustomException(CodeEnum.OTHER_ERROR.getCode(), "当前产线繁忙");
        }



        Integer cash = ctCashflowService.getCash(userId, line.getContestId());
        //获取生产线规则
        NGbCtGzProductLine gbCtGzProductLine = gbCtGzProductLineMapper.selectById(line.getPlCplid());
        //释放当前产线的工人
        gbCtWorkerMapper.updateWorkerListByLineIdToSpace(line.getLineId());

        // 插入现金交易记录
        // 流入=原值-累计折旧
        // 流出=原值-累计折旧-残值
        Integer cIn = line.getPlInvest() - line.getPlDepTotal();
        int cOut = line.getPlInvest() - line.getPlDepTotal() - gbCtGzProductLine.getCplDepFee();
        NGbCtCashflow ctCashflow = new NGbCtCashflow();
        ctCashflow.setStudentId(userId)
                .setContestId(line.getContestId())
                .setCAction(CashFlowActionEnum.SELL_PRODUCT_LINE.getAction())
                .setCOut(cOut)
                .setCIn(cIn)
                .setCComment("变卖生产线" + line.getLineId() + ",增加现金" + line.getPlInvest() + "元,减少现金" + line.getPlDepTotal() + "元")
                .setCDate(actionTime)//当前时间
                .setCSurplus(cash + cIn - cOut);
        ctCashflowService.save(ctCashflow);


        //产线状态修改为为已售
        line.setStatus(NGbCtLine.SALED);
        lineService.updateById(line);


        //计算报表,判断是否破产
        gbActionBiz.isBankruptcy(userId,line.getContestId(),actionTime);

        return R.success();
    }





    /**
     * 生产产品
     * @param userId
     * @param params
     * @return
     * @throws Exception
     */
    public R producting(Integer userId, NGbCtLine params) throws Exception {

        //阻止暂停状态下 和 破产状态下的用户进行操作
        gbActionBiz.preventAction(params.getContestId(),userId);

        //获取当前学生的操作时间
        Integer studentDate = gbContestStudentService.getStudentDate(params.getContestId(),userId);
        params.setActionTime(studentDate/10);
        Integer currentTime = studentDate/10;
        Integer year = currentTime/10;

        Integer cash = ctCashflowService.getCash(userId, params.getContestId());
        NGbCtLine line = gbCtLineMapper.selectById(params.getLineId());


        if(NGbCtLine.SALED.equals(line.getStatus())){
            throw new CustomException(CodeEnum.OTHER_ERROR.getCode(), "当前产线已被售卖");
        }


        //产线状态判断  如果产线不是空闲状态
        if(!NGbCtLine.ON_SPACE.equals(line.getStatus())){
            throw new CustomException(CodeEnum.OTHER_ERROR.getCode(), "当前产线繁忙");
        }

        //判断产品类型是否发生变化
        if((!params.getPlCpid().equals(line.getPlCpid()))){
            throw new CustomException(CodeEnum.OTHER_ERROR.getCode(), "当前产线产品已发生变化,请刷新");
        }


        //判断产品类型是否发生变化
        if(params.getFeatureNum()!=null && (!params.getFeatureNum().equals(line.getFeatureNum()))){
            throw new CustomException(CodeEnum.OTHER_ERROR.getCode(), "产品特征已发生变化,请重新刷新数据再进行操作");
        }


        //判断产线的产量是否为0
        if(line.getRealProduction()==null||line.getRealProduction() == 0){
            throw new CustomException(CodeEnum.OTHER_ERROR.getCode(), "当前产线的产量为0，请检查工人分配情况");
        }


        //获取当前生产线规则信息
        NGbCtGzProductLine gzLine = gbCtGzProductLineMapper.selectById(line.getPlCplid());
        NGbCtGzProduct gbCtGzProduct = gbCtGzProductMapper.selectById(params.getPlCpid());

        //计算碳排放
        int lineCarbon = gzLine.getCplCarbon();
        int productCarbon = gbCtGzProduct.getCpCarbon();
        int addCarbon = lineCarbon + productCarbon * line.getRealProduction();

        //获取当年的碳排放数量
        NGbCtCarbon nGbCtCarbon = nGbCtCarbonMapper.selectOne(new LambdaQueryWrapper<NGbCtCarbon>()
                .eq(NGbCtCarbon::getContestId,params.getContestId())
                .eq(NGbCtCarbon::getStudentId,userId)
                .eq(NGbCtCarbon::getYear,year)
        );
        int emissionNum = nGbCtCarbon.getEmissionNum()+addCarbon;
        nGbCtCarbon.setEmissionNum(emissionNum);
        //第三年 和 第四年要限制碳排放
        if(year==3 || year==4){
           int assignedNum =  nGbCtCarbon.getAssignedNum();
           //如果实际排放大于分配的排放,则无法生产
           if(assignedNum<emissionNum){
               throw new CustomException(CodeEnum.OTHER_ERROR.getCode(), "碳排放量超标");
           }
        }

        //碳排放保存
        nGbCtCarbonMapper.updateById(nGbCtCarbon);


        //先判断当前的产品是否具备特性,有的话不用管,没有特性,则获取当前产品最新的特性
        if(StringUtils.isEmpty(line.getFeatureNum())){
            //获取转产后最新的 产品特性
            NGbCtDesignFeature nGbCtDesignFeature = nGbCtDesignFeatureMapper.selectOne(new LambdaQueryWrapper<NGbCtDesignFeature>()
                    .eq(NGbCtDesignFeature::getContestId,line.getContestId())
                    .eq(NGbCtDesignFeature::getStudentId,userId)
                    .eq(NGbCtDesignFeature::getCpId,line.getPlCpid())
                    .orderByDesc(NGbCtDesignFeature::getId)
                    .last(" limit 1")
            );
            //如果产品特性不等于空,则获取最新的特性
            if(nGbCtDesignFeature!=null){
                line.setFeatureNum(nGbCtDesignFeature.getFeatureNum());
                line.setFeatureName(nGbCtDesignFeature.getFeatureName());
            }
            //没有特性抛出异常,不允许生产没有特性的产品
            else{
                throw new CustomException(CodeEnum.OTHER_ERROR.getCode(), "当前产品还未进行特征设计，不允许生产");
            }
        }


        //获取当前产线生产对应的单个产品及所需的材料
        List<NGbCtGzProducing> gbCtGzProducings = gbCtGzProducingMapper.getListByCpId(line.getPlCpid());

        //判断库存是否足够生产
        List<NGbCtKcMaterialNumVo> kcNums = gbCtKcMaterialMapper.listKcNum(line.getContestId(), line.getStudentId());
        Map<Integer,Integer> kcNumMap = new HashMap<>();
        for (NGbCtKcMaterialNumVo kcNum : kcNums) {
            kcNumMap.put(kcNum.getMaterialNum(),kcNum.getNum());
        }
        for (NGbCtGzProducing gbCtGzProducing : gbCtGzProducings) {
            int need  = line.getRealProduction() * gbCtGzProducing.getCpNum();
            Integer kc = kcNumMap.get(gbCtGzProducing.getCpMid());
            if(kc==null || kc < need){
                throw new CustomException(CodeEnum.OTHER_ERROR.getCode(), "原材料不足");
            }
        }

        //获取生产所需加工费
        int developFee = 0;
        int fee = gbCtGzProduct.getCpDevelopFee() * line.getRealProduction();
        developFee += developFee + fee;
        int remain = cash - developFee;
        if (remain < 0) {
            throw new CustomException(CodeEnum.OTHER_ERROR.getCode(), "现金不足");
        }



        //获取这条产线的工人
        List<NGbCtWorker> workerOnLines = gbCtWorkerMapper.getAllWorkerListByLineId(line.getLineId());

        //工人的计件工资
        Integer jjgz = workerOnLines.stream().mapToInt(NGbCtWorker::getPiece).sum();
        //总共的几件工资
        Integer jjgzTotal = jjgz * line.getRealProduction();

        //剩余金额 - 计件工资 * 数量
        int remain2 = remain-jjgzTotal;
        if (remain2 < 0) {
            return ErrorEnum.CASH_NOT_ENOUGH.getR();
        }

        
        //获取材料库存信息（按入库时间排序）
        List<NGbCtKcMaterial> materials = gbCtKcMaterialMapper.list(line.getContestId(), line.getStudentId());
        
        //材料规则id和编号的键值对
        List<NGbCtGzMaterial> gzMaterial = gbCtGzMaterialMapper.list(line.getContestId());
        Map<Integer,Integer> idAndCmIdMap = new HashMap<>();
        for (NGbCtGzMaterial gbCtGzMaterial : gzMaterial) {
            idAndCmIdMap.put(gbCtGzMaterial.getId(),gbCtGzMaterial.getCmId());
        }
        
        //把材料库存分类
        Map<Integer,List<NGbCtKcMaterial>> kcMaterialMap = new HashMap<>();
        for (NGbCtGzProducing gbCtGzProducing : gbCtGzProducings) {
            List<NGbCtKcMaterial> list = new ArrayList<>();
            for (NGbCtKcMaterial material : materials) {
                Integer imNum = idAndCmIdMap.get(material.getImCmId());
                if(imNum.equals(gbCtGzProducing.getCpMid())){
                    list.add(material);
                }
            }
            kcMaterialMap.put(gbCtGzProducing.getCpMid(),list);
        }




        //工人的这个月的基本工资(下个月发放)
        Integer jbgz = workerOnLines.stream().mapToInt(item->item.getNextInitSal()*3).sum();
        //工资分摊到每个产品的成本中
        Integer gzft = jbgz/line.getRealProduction();

        //生产的产品个数进行循环 进行生产
        for (int i = 0; i < line.getRealProduction(); i++) {
            int cashNeed = 0;
            for (NGbCtGzProducing gbCtGzProducing : gbCtGzProducings) {
                int needNum = gbCtGzProducing.getCpNum();
                if(needNum == 0){ continue;}

                List<NGbCtKcMaterial> materialList = kcMaterialMap.get(gbCtGzProducing.getCpMid());
                for (NGbCtKcMaterial material : materialList) {
                    if(material.getImNum() == 0 ){continue;}
                    if(material.getImNum() - needNum > 0){
                        //计算金额，更新材料剩余数量
                        cashNeed += material.getMaterialPrice() * needNum;
                        material.setImNum(material.getImNum() - needNum);
                        break;
                    }else{
                        //计算金额，删除数据（改为更新为0），结束当前循环
                        cashNeed += material.getMaterialPrice() * material.getImNum();
                        needNum = needNum - material.getImNum();
                        material.setImNum(0);
                    }
                }
            }
            cashNeed += gbCtGzProduct.getCpDevelopFee();//加工费


            cashNeed += jjgz; //计件工资


            //最后一个人的工资分摊
            if(i== (line.getRealProduction()-1)){
                int lastFt = jbgz - gzft*(line.getRealProduction()-1);
                cashNeed += lastFt;
            }else{
                //工人的基本工资都要核算到成本里面
                cashNeed += gzft;
            }




            //生成库存数据 tips:由于每个产品所用的原材料存在差异,所以成本会有所差异
            NGbCtKcProduct gbCtKcProduct = new NGbCtKcProduct();
            gbCtKcProduct.setContestId(line.getContestId())
                    .setStudentId(line.getStudentId())
                    .setIpCpId(line.getPlCpid())
                    .setInitNum(1)
                    .setIpNum(1)
                    .setLineId(line.getLineId())
                    .setRealCost(cashNeed)
                    //产品特征
                    .setFeatureName(line.getFeatureName())
                    .setFeatureNum(line.getFeatureNum());

            Integer produceTime = gzLine.getCplProduceDate();
            //判断生产周期
            if(produceTime !=null && produceTime>0){
                Integer produceFinishTime = DateUtils.addYearAndSeasonTime(params.getActionTime(),produceTime);
                line.setPlProductingDate(produceFinishTime);//生产结束时间
                gbCtKcProduct.setIsInventory(NGbCtKcProduct.NOT_IN);// 产品未入库
                gbCtKcProduct.setInventoryDate(produceFinishTime);//入库时间
            }else {
                line.setPlProductingDate(line.getActionTime());//生产结束时间
                gbCtKcProduct.setIsInventory(NGbCtKcProduct.IS_IN);// 产品入库
                gbCtKcProduct.setInventoryDate(line.getActionTime());//入库时间
            }
            ctKcProductMapper.insert(gbCtKcProduct);
        }

        //更新所有记录
        kcMaterialMap.forEach((k,v)->ctKcMaterialService.updateBatchById(v));





        //插入现金交易记录 - 加工费
        NGbCtCashflow ctCashflow = new NGbCtCashflow();
        ctCashflow.setStudentId(userId)
                .setContestId(line.getContestId())
                .setCAction(CashFlowActionEnum.PRODUCT_PRODUCING.getAction())
                .setCOut(developFee)
                .setCIn(0)
                .setCComment("生产线" + line.getLineId().toString() + "生产产品" + gbCtGzProduct.getCpName() + "加工费")
                .setCDate(params.getActionTime())
                .setCSurplus(remain);
        ctCashflowService.save(ctCashflow);


        //插入现金交易记录 - 计件工资
        NGbCtCashflow ctCashflow2 = new NGbCtCashflow();
        ctCashflow2.setStudentId(userId)
                .setContestId(line.getContestId())
                .setCAction(CashFlowActionEnum.JJGZ.getAction())
                .setCOut(jjgzTotal)
                .setCIn(0)
                .setCComment("生产线" + line.getLineId().toString() + "生产产品" + gbCtGzProduct.getCpName() + "计件工资")
                .setCDate(params.getActionTime())
                .setCSurplus(remain2);
        ctCashflowService.save(ctCashflow2);


        //更新生产线表
        Integer produceTimeDate = DateUtils.addYearAndSeasonTime(params.getActionTime(),gzLine.getCplProduceDate());

        line.setPlProductAddDate(params.getActionTime());//设置开产时间
        line.setPlProductingDate(produceTimeDate);//设置生产结束时间
        line.setStatus(NGbCtLine.ON_PRODUCE);
        lineService.updateById(line);


        //计算报表,判断是否破产
        gbActionBiz.isBankruptcy(userId,line.getContestId(),params.getActionTime());

        return R.success();
    }

    /**
     * 设备规则、线型、产品获取
     * @param studentId
     * @param contestId
     * @return
     */
    public R info(Integer studentId, Integer contestId) {

        // 生产线类型
        List<NGbCtGzProductLine> productLineList = ctGzProductLineService.getProductLineTypeList(contestId);
        // 生产产品
        List<NGbCtGzProduct> productList = ctGzProductService.getList(contestId);
        Map<String, Object> returnMap = new HashMap<>();
        returnMap.put("productLineList", productLineList);
        returnMap.put("productList", productList);
        return R.success(returnMap);
    }

    /**
     * 产品信息获取
     * @param contestId
     * @return
     */
    public R proInfo(Integer contestId) {
        // 生产产品
        List<NGbCtGzProduct> productList = ctGzProductService.getList(contestId);
        Map<String, Object> returnMap = new HashMap<>();
        returnMap.put("productList", productList);
        return R.success(returnMap);
    }


    /**
     * 产线保存工人
     * @param studentId
     * @param param
     * @return
     */
    public R saveWorkerToLine(Integer studentId, NGbLineParam param){

        //阻止暂停状态下 和 破产状态下的用户进行操作
        gbActionBiz.preventAction(param.getContestId(),studentId);

        //产线状态判断
        NGbCtLine gbCtLine = gbCtLineMapper.selectById(param.getLineId());
        if(NGbCtLine.SALED.equals(gbCtLine.getStatus())){
            throw new CustomException(CodeEnum.OTHER_ERROR.getCode(), "当前产线已被售卖");
        }
        if(NGbCtLine.ON_PRODUCE.equals(gbCtLine.getStatus())){
            throw new CustomException(CodeEnum.OTHER_ERROR.getCode(), "当前产线正在生产中");
        }

        List<Integer> workerIds1 = param.getOrdinaryWorkerList();
        List<Integer> workerIds2 = param.getSeniorWorkerList();
        List<Integer> workerIds = new ArrayList<>();
        workerIds.addAll(workerIds1);
        workerIds.addAll(workerIds2);
        List<NGbCtWorker> workers = new ArrayList<>();
        if(workerIds.size() > 0){
            workers = gbCtWorkerMapper.selectBatchIds(workerIds);
            for (NGbCtWorker worker : workers) {
                //在其他产线
                if(worker.getLineId()!= null && !Objects.equals(worker.getLineId(), param.getLineId())){
                    return ErrorEnum.WORKER_WORKING.getR();
                }
            }
        }
        //删除原来保存的所有id
        List<NGbCtWorker> allWorker = gbCtWorkerMapper.getAllWorkerListByLineId(param.getLineId());
        for (NGbCtWorker gbCtWorker : allWorker) {
            gbCtWorkerMapper.update(gbCtWorker,new UpdateWrapper<NGbCtWorker>().lambda()
                    .set(NGbCtWorker::getLineId,null)
                    .set(NGbCtWorker::getIsWork,NGbCtWorker.ON_SPACE)
                    .eq(NGbCtWorker::getId,gbCtWorker.getId())
            );
        }

        //重新保存id
        for (NGbCtWorker worker : workers) {
            worker.setLineId(param.getLineId())
                    .setIsWork(NGbCtWorker.ON_WORK);
            gbCtWorkerMapper.updateById(worker);
        }

        gbCtLine.setClassesId(param.getClassesId())//班次规则
                .setRealProduction(param.getRealProduction());//实际产量
        gbCtLineMapper.updateById(gbCtLine);

        return R.success();
    }


    /**
     * 更新产线的BOOM
     * @param userId
     * @param line
     * @return
     */
    public R updateBOM(Integer userId, NGbCtLine line) {

        //阻止暂停状态下 和 破产状态下的用户进行操作
        gbActionBiz.preventAction(line.getContestId(),userId);

        Integer contestId = line.getContestId();
        //先判断当前产线的状态
        NGbCtLine line1 = gbCtLineMapper.selectById(line.getLineId());
        String status = line1.getStatus();

        //当前产线正在生产中
        if(NGbCtLine.ON_PRODUCE.equals(status)){
            throw new CustomException(CodeEnum.OTHER_ERROR.getCode(), "当前产线正在生产中");
        }

        //判断产线是否已售
        if(NGbCtLine.SALED.equals(status)){
            throw new CustomException(CodeEnum.OTHER_ERROR.getCode(), "当前产线已被售卖");
        }

        //判断产品是否发生变化
        if(!line.getPlCpid().equals(line1.getPlCpid())){
            throw new CustomException(CodeEnum.OTHER_ERROR.getCode(), "当前产线生产产品发生变化,请刷新数据重试");
        }

        //获取最新的产品特性
        Integer cpId = line1.getPlCpid();

        NGbCtDesignFeature nGbCtDesignFeature = nGbCtDesignFeatureMapper.selectOne(new LambdaQueryWrapper<NGbCtDesignFeature>()
                .eq(NGbCtDesignFeature::getContestId,contestId)
                .eq(NGbCtDesignFeature::getStudentId,userId)
                .eq(NGbCtDesignFeature::getCpId,cpId)
                .orderByDesc(NGbCtDesignFeature::getId)
                .last(" limit 1")
        );
        //判断是否有最新的特性
        if(nGbCtDesignFeature!=null){
            gbCtLineMapper.updateById(
                    new NGbCtLine()
                            .setLineId(line.getLineId())
                            .setFeatureName(nGbCtDesignFeature.getFeatureName())
                            .setFeatureNum(nGbCtDesignFeature.getFeatureNum())
            );
            line.setFeatureNum(nGbCtDesignFeature.getFeatureNum());
            line.setFeatureName(nGbCtDesignFeature.getFeatureName());
        }
        return R.success();
    }
}
