package com.xiyou.main.biz.ngbcontest;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.xiyou.common.enums.CodeEnum;
import com.xiyou.common.utils.R;
import com.xiyou.main.dao.ngbcontest.NGbCtCashflowMapper;
import com.xiyou.main.dao.ngbcontest.NGbCtGzNumMapper;
import com.xiyou.main.dao.ngbcontest.NGbCtYfNumMapper;
import com.xiyou.main.entity.ngbcontest.NGbContest;
import com.xiyou.main.entity.ngbcontest.NGbCtCashflow;
import com.xiyou.main.entity.ngbcontest.NGbCtGzNum;
import com.xiyou.main.entity.ngbcontest.NGbCtYfNum;
import com.xiyou.main.enums.CashFlowActionEnum;
import com.xiyou.main.params.ngbcontest.NGbNumParam;
import com.xiyou.main.service.ngbcontest.NGbContestService;
import com.xiyou.main.service.ngbcontest.NGbCtCashflowService;
import com.xiyou.main.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @program: multi-module
 * @description:
 * @author: tangcan
 * @create: 2019-09-01 12:43
 **/
@Service
@Transactional(rollbackFor = Exception.class)
public class NGbNumBiz {
    @Autowired
    private NGbCtCashflowService ctCashflowService;
    @Autowired
    private NGbCtCashflowMapper cashflowMapper;
    @Autowired
    private NGbCtGzNumMapper gbCtGzNumMapper;
    @Autowired
    private NGbCtYfNumMapper gbCtYfNumMapper;

    @Autowired
    private NGbActionBiz gbActionBiz;

    @Autowired
    private NGbContestService nGbContestService;


    /**
     * 获取数字化研发的列表
     * @param contestId
     * @param studentId
     * @return
     */
    public R list(Integer contestId, Integer studentId) {
        QueryWrapper<NGbCtYfNum> qw = new QueryWrapper<>();
        qw.eq("contest_id",contestId);
        qw.eq("student_id",studentId);
        qw.orderByAsc("id");
        List<NGbCtYfNum> list = gbCtYfNumMapper.selectList(qw);
        return R.success().put("list", list);
    }



    public R commit(NGbNumParam param) {
        Integer contestId = param.getContestId();

        if (param.getId() == null) {
            return R.error(CodeEnum.PARAM_ERROR.getCode(), "请选择数字化研发");
        }
        // 现金
        Integer cash = cashflowMapper.getCash(param.getStudentId(), param.getContestId());
        if (cash == null) {
            cash = 0;
        }

        NGbCtYfNum gbCtYfNum = gbCtYfNumMapper.selectById(param.getId());
        if (NGbCtYfNum.NUM_YF_ING.equals(gbCtYfNum.getState()) || NGbCtYfNum.NUM_YF_FINISH.equals(gbCtYfNum.getState())) {
            return R.error(CodeEnum.OTHER_ERROR.getCode(), "无需重复研发");
        }

        //获取对应的规则
        NGbContest nGbContest = nGbContestService.getById(contestId);
        Integer subjectNumber = nGbContest.getSubjectNumber();
        NGbCtGzNum gbCtGzNum  = gbCtGzNumMapper.selectOne(new LambdaQueryWrapper<NGbCtGzNum>()
                .eq(NGbCtGzNum::getSubjectNum,subjectNumber)
                .eq(NGbCtGzNum::getPostNum,gbCtYfNum.getPostNum())
                .last("limit 1")
        );

        Integer investSum = gbCtGzNum.getConsumeMoney();
        // 当现金<sum（勾选的投资费用总和），则报错【现金不足】
        if (cash < investSum) {
            return R.error(CodeEnum.OTHER_ERROR.getCode(), "现金不足");
        }

        String action = "";
        //营销数字化
        if("1".equals(gbCtGzNum.getPostNum())){
            action = CashFlowActionEnum.YXSZH.getAction();
        }
        //生产数字化
        else if("2".equals(gbCtGzNum.getPostNum())){
            action = CashFlowActionEnum.SCSZH.getAction();
        }
        //人力数字化
        else if("3".equals(gbCtGzNum.getPostNum())){
            action = CashFlowActionEnum.RLSZH.getAction();
        }
        //财务数字化
        else if("4".equals(gbCtGzNum.getPostNum())){
            action = CashFlowActionEnum.CWSZH.getAction();
        }

        // 刷新cashflow，ID、ISO开拓、0、sum（勾选的投资费用总和），现金+增加-减少、扣减ISO开拓&sum（勾选的投资费用总和）、当前时间
        ctCashflowService.save(new NGbCtCashflow()
                .setStudentId(gbCtYfNum.getStudentId())
                .setContestId(gbCtYfNum.getContestId())
                .setCAction(action)
                .setCIn(0)
                .setCOut(investSum)
                .setCSurplus(cash - investSum)
                .setCComment("扣减"+gbCtYfNum.getPostName()+"开拓" + investSum + "元")
                .setCDate(param.getDate()));
        // 刷新yf_ISO,勾选记录now_date +1，Remain_Date -1
        Integer ciDevelopDate = gbCtGzNum.getTimeCostQuarter();
        Integer finishDate = DateUtils.addYearAndSeasonTime(param.getDate(),ciDevelopDate);
        if(ciDevelopDate != null && ciDevelopDate>0 ){
            gbCtYfNum.setState(NGbCtYfNum.NUM_YF_ING);
        }else {
            gbCtYfNum.setState(NGbCtYfNum.NUM_YF_FINISH);
        }
        gbCtYfNum.setStartDate(param.getDate())
                .setFinishDate(finishDate);
        gbCtYfNumMapper.updateById(gbCtYfNum);

        //破产判断,生成报表
        gbActionBiz.isBankruptcy(param.getStudentId(),param.getContestId(),param.getDate());

        return R.success();
    }
}
