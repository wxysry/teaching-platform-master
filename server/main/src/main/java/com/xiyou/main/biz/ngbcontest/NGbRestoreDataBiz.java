package com.xiyou.main.biz.ngbcontest;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.xiyou.common.utils.R;
import com.xiyou.main.dao.ngbcontest.NGbCtSubjectMapper;
import com.xiyou.main.entity.ngbcontest.NGbContestStudent;
import com.xiyou.main.enums.ErrorEnum;
import com.xiyou.main.service.ngbcontest.NGbContestStudentService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

/**
 * @program: multi-module
 * @description:
 * @author wangxingyu
 * @since 2023-06-11
 **/
@Slf4j
@Service
@Transactional(rollbackFor = Exception.class)
public class NGbRestoreDataBiz {

    @Autowired
    private NGbCtSubjectMapper gbCtSubjectMapper;
    @Autowired
    private NGbContestStudentService gbContestStudentService;


//    /**
//     * 判断是否最后一场订货会之前
//     * @param currentTime
//     * @param restoreTime
//     * @return
//     */
//    public boolean checkIsBeforeTradeFair(Integer contestId,Integer currentTime,Integer restoreTime){
//        // 如果这个时间节点在最近一个订货会之前 不允许还原
//        NGbContest contest = gbContestService.getById(contestId);
//        return restoreTime < contest.getLastFairOrderDate();
//    }




    /**
     * 统一模式还原所有学生
     * @param contestId
     * @param currentTime
     * @return
     */
    public void restoreAll(Integer contestId,Integer currentTime,Integer restoreTime){
        //查询出所有学生还原
        List<Integer> studentIdList = gbContestStudentService.getStudentIdList(contestId);

        //大部队所在的年份
        int year = currentTime/10;
        List<NGbContestStudent> studentList= gbContestStudentService.list(new LambdaQueryWrapper<NGbContestStudent>()
                .eq(NGbContestStudent::getContestId,contestId));
        List<NGbContestStudent> RestoreStudentList = studentList.stream().filter(item->{
            //学生的时间线
            int studentDate = item.getDate()/10;
            return (studentDate==currentTime);
        }).collect(Collectors.toList());

        //还原学生
        for (NGbContestStudent student : RestoreStudentList) {
            restoreDataByTime(contestId,student.getStudentId(),currentTime,restoreTime);

            //清空最后一次还原时间
            gbContestStudentService.update(new NGbContestStudent(), new UpdateWrapper<NGbContestStudent>()
                    .eq("contest_id", contestId)
                    .eq("student_id", student.getStudentId())
                    .set("last_restore_time",null)
            );
        }
        //删除系统公告数据和重置选单信息
        restoreSystemNoticeAndChooseByTime(contestId,currentTime,restoreTime);

    }


    /**
     * @Author: tangcan
     * @Description: 备份数据
     * @Param: [contestId, studentId]
     * @date: 2019/8/31
     */
    public void backupData(Integer contestId, Integer studentId,Integer currentTime,boolean isChooseOrder) {

        //跳季度的时候备份数据，数据除了要备份到like 表还要覆盖到 single_like 表
        //如果是订货会结束备份数据。则对应的like表不覆盖。

        // 更新原表的 back_up_date 字段的值
        gbCtSubjectMapper.updateTableBackUpDate(contestId, studentId,currentTime);

        if(!isChooseOrder){
            // 备份数据
            // 1.先删除备份表中之前备份的数据
            gbCtSubjectMapper.removeLikeTableDataByTime(contestId, studentId,currentTime);
            // 2.将主表的数据插入到备份表
            gbCtSubjectMapper.insertMainTableToLikeTableData(contestId, studentId);
        }

        //备份数据到single_like表
        gbCtSubjectMapper.removeSingleLikeTableDataByTime(contestId, studentId,currentTime);
        gbCtSubjectMapper.insertMainTableToSingleLikeTableData(contestId, studentId);

    }



    /**
     * 非统一模式还原学生
     * @param contestId
     * @param studentId
     * @param restoreTime
     * @return
     */
    public R restoreOneData(Integer contestId, Integer studentId,Integer restoreTime){
        NGbContestStudent studentContest = gbContestStudentService.getOne(new QueryWrapper<NGbContestStudent>().
                eq("contest_id", contestId).eq("student_id", studentId));
        int currentTime = studentContest.getDate()/10;
        log.error("【还原单个学生】：contestId:"+contestId+",studentId:"+studentId+",从"+currentTime+"还原到"+restoreTime);



        //将学生还原到对应的时间节点
        restoreSingleDataByTime(contestId,studentId,currentTime,restoreTime);

        //记录该学生最近一次还原到的时间
        gbContestStudentService.update(new NGbContestStudent().setLastRestoreTime(restoreTime),
                new QueryWrapper<NGbContestStudent>().eq("contest_id", contestId).eq("student_id", studentId));

        return R.success();
    }

    /**
     * 统一还原-按时间节点还原数据
     * @param contestId
     * @param studentId
     * @param
     * @return
     */
    public R restoreDataByTime(Integer contestId, Integer studentId,Integer currentTime,Integer restoreTime){

        if(restoreTime > currentTime){
            return ErrorEnum.RESTORE_TIME_ERROR.getR();
        }
        // 1.先删除现有数据
        gbCtSubjectMapper.removeMainTableData(contestId, studentId);

        //2.删除这个时间节点的后备份表里的数据
        gbCtSubjectMapper.removeLikeTableDataAfterRestoreTime(contestId,studentId,restoreTime);


        //3.再从备份表里把数据插入到竞赛表
        gbCtSubjectMapper.insertLikeTableDataToMainTable(contestId, studentId,restoreTime);

        //4.single_like 中的数据要进行清洗
        //将当前时间点后面的数据删除,用季初的数据覆盖掉single_like里面的数据
        gbCtSubjectMapper.removeSingleLikeTableDataAfterRestoreTime(contestId,studentId,restoreTime);
        gbCtSubjectMapper.removeSingleLikeTableDataByTime(contestId, studentId,currentTime);
        gbCtSubjectMapper.insertMainTableToSingleLikeTableData(contestId, studentId);

        return R.success();
    }





    /**
     * 单个学生还原-按时间节点还原数据
     * @param contestId
     * @param studentId
     * @param
     * @return
     */
    public R restoreSingleDataByTime(Integer contestId, Integer studentId,Integer currentTime,Integer restoreTime){

        if(restoreTime > currentTime){
            return ErrorEnum.RESTORE_TIME_ERROR.getR();
        }
        // 1.先删除现有数据
        gbCtSubjectMapper.removeMainTableData(contestId, studentId);

        //2.删除这个时间节点的后备份表里的数据
        gbCtSubjectMapper.removeSingleLikeTableDataAfterRestoreTime(contestId,studentId,restoreTime);
        gbCtSubjectMapper.removeLikeTableDataAfterRestoreTime(contestId,studentId,restoreTime);

        //3.再从备份表里把数据插入到竞赛表
        gbCtSubjectMapper.insertSingleLikeTableDataToMainTable(contestId, studentId,restoreTime);

        return R.success();
    }




    public R restoreSystemNoticeAndChooseByTime(Integer contestId,Integer currentTime,Integer restoreTime){
        if(restoreTime > currentTime){
            return ErrorEnum.RESTORE_TIME_ERROR.getR();
        }
        // 删除系统公告数据和重置选单信息
        gbCtSubjectMapper.removeSystemNoticeAfterRestoreTime(contestId,restoreTime);
        return R.success();
    }

}
