package com.xiyou.main.biz.ngbcontest;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.xiyou.common.utils.R;
import com.xiyou.main.dao.ngbcontest.*;
import com.xiyou.main.entity.ngbcontest.NGbCtBalance;
import com.xiyou.main.entity.ngbcontest.NGbCtRetailAssign;
import com.xiyou.main.service.ngbcontest.NGbCtBalanceService;
import com.xiyou.main.service.ngbcontest.NGbCtRetailAssignService;
import com.xiyou.main.vo.ngbcontest.NGbCashFolowEntity;
import com.xiyou.main.vo.ngbcontest.NGbFinanceReturnEntity;
import com.xiyou.main.vo.ngbcontest.NGbMarketReturnEntity;
import com.xiyou.main.vo.ngbcontest.NGbProductMaterialEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author wangxingyu
 * @since 2023-06-11
 * @description: 财务信息&综合财务信息&研发认证信息
 **/
@Service
@Transactional(rollbackFor = Exception.class)
public class NGbStudentFinanceBiz {

    @Autowired
    NGbCtCashflowMapper ctCashflowMapper;
    @Autowired
    NGbCtFeeMapper ctFeeMapper;
    @Autowired
    NGbCtBankLoanMapper ctBankLoanMapper;
    @Autowired
    NGbCtXzOrderMapper ctXzOrderMapper;
    @Autowired
    NGbCtYfMarketMapper ctYfMarketMapper;
    @Autowired
    NGbCtYfIsoMapper ctYfIsoMapper;
    @Autowired
    NGbCtYfProductMapper ctYfProductMapper;
    @Autowired
    NGbCtKcProductMapper ctKcProductMapper;
    @Autowired
    NGbCtKcMaterialMapper ctKcMaterialMapper;
    @Autowired
    NGbCtDdMaterialMapper ctDdMaterialMapper;

    @Autowired
    NGbCtBalanceService nGbCtBalanceService;

    @Autowired
    private NGbCtRetailAssignService nGbCtRetailAssignService;

    /**
     * @Description: 财务信息&综合财务信息
     * @Author:zhengxiaodong
     * @Param: [cashFolowEntity]
     * @return: com.xiyou.common.utils.R
     */
    public R getStudentFinanceInfo(NGbCashFolowEntity cashFolowEntity) {

        Map<String, Object> returnMap = new HashMap<>();

        NGbFinanceReturnEntity returnEntity = new NGbFinanceReturnEntity();

        // 当前现金
        returnEntity.setCurrentMoney(ctCashflowMapper.getCash(cashFolowEntity.getStudentId(), cashFolowEntity.getContestId()));
        // 应收账款
        returnEntity.setReceivableMoney(ctFeeMapper.getStudentReceivableMoney(cashFolowEntity));
        returnMap.put("receivableMoneyList", ctFeeMapper.getStudentReceivableMoneyList(cashFolowEntity));
        // 长贷总额
        returnEntity.setLongTermLoansMoney(ctBankLoanMapper.getStudentLongAndShortTermLoansMoney(cashFolowEntity, 1));
        returnMap.put("longTermLoansMoneyList", ctBankLoanMapper.getStudentLongAndShortTermLoansMoneyList(cashFolowEntity, 1));
        // 短贷总额
        returnEntity.setShortTermLoanMoney(ctBankLoanMapper.getStudentLongAndShortTermLoansMoney(cashFolowEntity, 2));
        returnMap.put("shortTermLoanMoneyList", ctBankLoanMapper.getStudentLongAndShortTermLoansMoneyList(cashFolowEntity, 2));
        // 贴息
        returnEntity.setDiscountMoney(ctCashflowMapper.getStudentDiscountMoney(cashFolowEntity, "贴现"));
        // 利息      本息同还-利息  每季付息-利息
        returnEntity.setInterest(ctCashflowMapper.getStudentDiscountMoney(cashFolowEntity, "利息"));


        //本年的零售收入
        List<NGbCtRetailAssign> nGbCtRetailAssigns = nGbCtRetailAssignService.list(new LambdaQueryWrapper<NGbCtRetailAssign>()
                .eq(NGbCtRetailAssign::getContestId,cashFolowEntity.getContestId())
                .eq(NGbCtRetailAssign::getStudentId,cashFolowEntity.getStudentId())
                .eq(NGbCtRetailAssign::getYear,cashFolowEntity.getCurrentYear())
        );
        Integer lsSale = nGbCtRetailAssigns.stream().mapToInt(item->item.getApplyPrice() * item.getDeliveryNum()).sum();
        Integer lsDirectCost = nGbCtRetailAssigns.stream().mapToInt(NGbCtRetailAssign::getTotalCost).sum();



        //销售收入 = 交货订单金额 + 零售市场销售金额
        returnEntity.setSalesProfit(ctXzOrderMapper.getStudentSalesProfit(cashFolowEntity)+lsSale);

        // 订单成本 = 交货订单成本 + 零售市场销售成本
        returnEntity.setDirectCost(ctXzOrderMapper.getStudentProductDevCost(cashFolowEntity)+lsDirectCost);

        // 维修费[维修费、转产费、租金、管理费、广告费]
        returnEntity.setRepairCost(ctCashflowMapper.getRepairCostToAdCost(cashFolowEntity, "维修费"));
        // 转产费
        returnEntity.setTransferCost(ctCashflowMapper.getRepairCostToAdCost(cashFolowEntity, "转产"));
        // 管理费
        returnEntity.setManageFee(ctCashflowMapper.getRepairCostToAdCost(cashFolowEntity, "管理费"));

        // 信息费
        returnEntity.setInformationCost(ctCashflowMapper.getRepairCostToAdCost(cashFolowEntity, "信息费"));
        // 广告费
        returnEntity.setAdCost(ctCashflowMapper.getRepairCostToAdCost(cashFolowEntity, "广告费"));




        // ISO认证[ISO认证、直接成本、市场开拓]
        returnEntity.setISOCost(ctCashflowMapper.getStudentISOTomarketDevCost(cashFolowEntity, "ISO开拓"));
        // 产品研发
        returnEntity.setProductDevCost(ctCashflowMapper.getStudentISOTomarketDevCost(cashFolowEntity, "产品研发"));
        // 市场开拓
        returnEntity.setMarketDevCost(ctCashflowMapper.getStudentISOTomarketDevCost(cashFolowEntity, "市场开拓"));
        //数字化研发
        returnEntity.setDigitalDevCost(ctCashflowMapper.getStudentISOTomarketDevCost(cashFolowEntity,"数字化"));


        //产品设计费
        returnEntity.setProductDesign(ctCashflowMapper.getStudentISOTomarketDevCost(cashFolowEntity,"产品设计"));
        //辞退福利
        returnEntity.setDismissFee(ctCashflowMapper.getStudentISOTomarketDevCost(cashFolowEntity,"辞退福利"));
        //培训费
        returnEntity.setTrainFee(ctCashflowMapper.getStudentISOTomarketDevCost(cashFolowEntity,"培训费"));
        //激励费
        returnEntity.setIncentiveFee(ctCashflowMapper.getStudentISOTomarketDevCost(cashFolowEntity,"激励费"));
        //人力费
        returnEntity.setHr(ctCashflowMapper.getStudentISOTomarketDevCost(cashFolowEntity,"人力费"));
        //碳中和费
        returnEntity.setCarbon(ctCashflowMapper.getStudentISOTomarketDevCost(cashFolowEntity,"碳中和"));
        //特性研发
        returnEntity.setFeature(ctCashflowMapper.getStudentISOTomarketDevCost(cashFolowEntity,"特性研发"));


        //获取实时权益
        NGbCtBalance balance = nGbCtBalanceService.get(new NGbCtBalance().setStudentId(cashFolowEntity.getStudentId()).setContestId(cashFolowEntity.getContestId()).setBsYear(cashFolowEntity.getCurrentYear()));
        Integer bsTotalEquity = balance.getBsTotalEquity();
        returnEntity.setSsqy(bsTotalEquity);


        returnMap.put("data", returnEntity);
        return R.success(returnMap);
    }

    /**
     * @Description: 研发认证信息
     * @Author:zhengxiaodong
     * @Param: []
     * @return: com.xiyou.common.utils.R
     */
    public R getResearchAndDevelopeInfo(Integer studentId, Integer contestId) {

        NGbMarketReturnEntity returnEntity = new NGbMarketReturnEntity();

        // 市场准入
        returnEntity.setMarket(ctYfMarketMapper.getList(contestId,studentId));
        // 生产资格
        returnEntity.setProduct(ctYfProductMapper.getList(contestId,studentId));
        // ISO认证
        returnEntity.setIso(ctYfIsoMapper.getList(contestId,studentId));

        return R.success().put("data", returnEntity);
    }

    /**
     * @Description: 库存采购信息
     * @Author:zhengxiaodong
     * @Param: []
     * @return: com.xiyou.common.utils.R
     */
    public R stockPurchaseInfo(Integer studentId, Integer contestId,Integer currentTime) {

        NGbProductMaterialEntity materialEntity = new NGbProductMaterialEntity();

        materialEntity.setKP(ctKcProductMapper.getKCProductNumb(studentId, contestId));


        materialEntity.setKR(ctKcMaterialMapper.getKcMaterial(studentId, contestId));


        materialEntity.setBR(ctDdMaterialMapper.getddMaterialBlackCar(studentId, contestId,currentTime));


        materialEntity.setGR(ctDdMaterialMapper.getddMaterialGreyCar(studentId, contestId,currentTime));


        return R.success().put("data", materialEntity);
    }


}
