package com.xiyou.main.biz.ngbcontest;


import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.xiyou.common.enums.CodeEnum;
import com.xiyou.common.exception.CustomException;
import com.xiyou.common.utils.R;
import com.xiyou.main.config.NGbWebSocket;
import com.xiyou.main.dao.ngbcontest.*;
import com.xiyou.main.entity.contest.CtXzOrder;
import com.xiyou.main.entity.exam.SysUser;
import com.xiyou.main.entity.gbcontest.GbContest;
import com.xiyou.main.entity.gbcontest.GbCtGzProductDesign;
import com.xiyou.main.entity.ngbcontest.*;
import com.xiyou.main.enums.CashFlowActionEnum;
import com.xiyou.main.enums.ErrorEnum;
import com.xiyou.main.params.ngbcontest.NGbCtMnChooseParam;
import com.xiyou.main.params.ngbcontest.NGbCtNetworkPlanParam;
import com.xiyou.main.params.ngbcontest.NGbCtRetailOrderParam;
import com.xiyou.main.service.exam.SysUserService;
import com.xiyou.main.service.ngbcontest.*;
import com.xiyou.main.utils.DateUtils;
import com.xiyou.main.vo.ngbcontest.NGbCtMnAdVo;
import io.swagger.models.auth.In;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @program: multi-module
 * @description:
 * @author: tangcan
 * @create: 2019-07-27 16:36
 **/
@Service
@Slf4j
public class NGbTradeFairBiz {

    @Autowired
    private NGbCtMnAdMapper gbCtMnAdMapper;

    @Autowired
    private NGbCtMnAdService gbCtMnAdService;


    @Autowired
    private NGbCtMnChooseMapper gbCtMnChooseMapper;
    @Autowired
    private NGbCtMnChooseService gbCtMnChooseService;
    @Autowired
    private NGbCtXzOrderMapper gbCtXzOrderMapper;


    @Autowired
    private NGbCtXzOrderService nGbCtXzOrderService;

    @Autowired
    private NGbContestService gbContestService;
    @Autowired
    private NGbContestStudentMapper gbContestStudentMapper;
    @Autowired
    private NGbTradeFairBizTranscation tradeFairBizTranscation;

    @Autowired
    private NGbCtOrderApplyMapper nGbCtOrderApplyMapper;


    @Autowired
    private NGbCtGzOrderMapper nGbCtGzOrderMapper;

    @Autowired
    private SysUserService sysUserService;

    @Autowired
    private NGbCtCorporateMapper nGbCtCorporateMapper;

    @Autowired
    private NGbCtDesignProgressMapper nGbCtDesignProgressMapper;


    @Autowired
    private NGbCtGzMarketMapper nGbCtGzMarketMapper;

    @Autowired
    private NGbCtGzIsoMapper nGbCtGzIsoMapper;

    @Autowired
    private NGbCtGzProductMapper nGbCtGzProductMapper;

    @Autowired
    private NGbCtYfMarketMapper nGbCtYfMarketMapper;

    @Autowired
    private NGbCtGzProductDesignMapper nGbCtGzProductDesignMapper;


    @Autowired(required = false)
    private NGbWebSocket NGbWebSocket;

    @Autowired
    private NGbContestStudentService gbContestStudentService;


    @Autowired
    private NGbRestoreDataBiz gbRestoreDataBiz;

    @Autowired
    private NGbCtYfIsoMapper ngbCtYfIsoMapper;




    /**
     * 开启投放广告
     * @return
     */
    public R startAd(Integer contestId,Integer date){
        log.error("订货会-开启投放广告,contestId:{},date:{}",contestId,date);
        NGbContest contest = gbContestService.getById(contestId);
        Integer sysFairOrderState = contest.getFairOrderState();
        if(sysFairOrderState != 0){
            return ErrorEnum.FAIR_ORDER_STATE_ERROR.getR();
        }else {
            contest.setFairOrderState(1);
            contest.setLastFairOrderDate(date);
            gbContestService.updateById(contest);
        };
        log.error("订货会-开启投放广告结束,contestId:{},date:{}",contestId,date);
        return R.success();
    }

    /**
     * 进入查看广告阶段
     * @return
     */
    public R showAd(Integer contestId){
        log.error("订货会-进入查看广告阶段,contestId:{},date:{}",contestId);
        NGbContest contest = gbContestService.getById(contestId);
        Integer sysFairOrderState = contest.getFairOrderState();
        if(sysFairOrderState != 1){
            return ErrorEnum.FAIR_ORDER_STATE_ERROR.getR();
        }else {
            contest.setFairOrderState(2);
            gbContestService.updateById(contest);
        };
        log.error("订货会-进入查看广告阶段结束,contestId:{},date:{}",contestId);
        return R.success();
    }


    /**
     * 开始竞争订单
     * @param contestId
     * @param date
     * @param type
     * @return
     */
    public R startCompeteOrder(Integer contestId, Integer date, Integer type) {
        if(type==1){
            return startApplyOrder(contestId,date);
        }else if(type==2){
            return allocationOrder(contestId,date);
        }else{
            return ErrorEnum.FAIR_ORDER_STATE_ERROR.getR();
        }
    }


    /**
     * 开启申报订单的功能
     * @return
     */
    public R startApplyOrder(Integer contestId,Integer date){
        log.error("竞单管理-教师端开启订单申报-开始,contestId:{},date:{}",contestId,date);
        NGbContest contest = gbContestService.getById(contestId);
        Integer sysFairOrderState = contest.getFairOrderState();
        if(sysFairOrderState != 0){
            return ErrorEnum.FAIR_ORDER_STATE_ERROR.getR();
        }else {
            contest.setFairOrderState(1);
            contest.setLastFairOrderDate(date);
            contest.setOrderStartTime(LocalDateTime.now());
            gbContestService.updateById(contest);
        };
        log.error("竞单管理-教师端开启订单申报-结束,contestId:{},date:{}",contestId,date);

        //通知给班级所有的学生
        Map<String, Object> messageMap = new HashMap<>();
        messageMap.put("type","refresh"); //数据刷新
        String message = JSONObject.toJSONString(messageMap);
        NGbWebSocket.sendMessageToAll(contestId,message); //给所有人广播消息
        return R.success();
    }

    /**
     * 开启申报订单的功能
     *
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    public R allocationOrder(Integer contestId, Integer date) {
        log.error("竞单管理-教师端-分配订单开始,contestId:{},date:{}", contestId, date);
        NGbContest contest = gbContestService.getById(contestId);
        Integer sysFairOrderState = contest.getFairOrderState();
        Integer year = date / 10;
        Integer quarterly = date % 10;

        //获取当前比赛的所有用户 - 得到 studentId -> account 的map
        List<SysUser> userList = gbContestStudentMapper.getStudentListByContestId(contestId);
        Map<Integer, String> groupNumMap = new HashMap<>();
        userList.stream().forEach(item -> groupNumMap.put(item.getId(), item.getAccount()));


        //获取破产的学生id
        List<NGbContestStudent> pcStudentList = gbContestStudentMapper.selectList(new LambdaQueryWrapper<NGbContestStudent>()
                .eq(NGbContestStudent::getContestId,contestId)
                .eq(NGbContestStudent::getIsBankruptcy,1)
        );
        List<Integer> pcStudentIds =  pcStudentList.stream().map(NGbContestStudent::getStudentId).collect(Collectors.toList());


        if (sysFairOrderState != 1) {
            return ErrorEnum.FAIR_ORDER_STATE_ERROR.getR();
        } else {
            //对订单进行分配
            //先获取当前季度的所有市场订单
            List<NGbCtGzOrder> gzOrderList = nGbCtGzOrderMapper.selectList(new LambdaQueryWrapper<NGbCtGzOrder>()
                    .eq(NGbCtGzOrder::getSubjectNumber, contest.getSubjectNumber())
                    .eq(NGbCtGzOrder::getDate, year)
                    .eq(NGbCtGzOrder::getQuarterly, quarterly)
            );
            //所有的选单列表
            List<NGbCtXzOrder> xzOrders = new ArrayList<>();
            //遍历所有的订单
            for (NGbCtGzOrder nGbCtGzOrder : gzOrderList) {
                //获取参与本次竞单的学生
                List<NGbCtOrderApply> nGbCtOrderApplies = nGbCtOrderApplyMapper.selectList(new LambdaQueryWrapper<NGbCtOrderApply>()
                        .eq(NGbCtOrderApply::getContestId, contestId) //考试
                        .eq(NGbCtOrderApply::getYear, year) //年
                        .eq(NGbCtOrderApply::getQuarterly, quarterly) //季度
                        .eq(NGbCtOrderApply::getCoId, nGbCtGzOrder.getCoId())
                );

                //将破产学生的申请剔除掉
                nGbCtOrderApplies = nGbCtOrderApplies.stream().filter(item->!pcStudentIds.contains(item.getStudentId())).collect(Collectors.toList());


                //Y=知名度+商誉*市场占有率*（参考价-报价）+1000*特性研发值
                //计算Y值
                for (NGbCtOrderApply nGbCtOrderApply : nGbCtOrderApplies) {
                    Integer studentId = nGbCtOrderApply.getStudentId();
                    //知名度
                    NGbCtMnAd nGbCtMnAd = gbCtMnAdMapper.selectOne(new LambdaQueryWrapper<NGbCtMnAd>()
                            .eq(NGbCtMnAd::getContestId, contestId)
                            .eq(NGbCtMnAd::getStudentId, studentId)
                            .eq(NGbCtMnAd::getYear, year)
                            .eq(NGbCtMnAd::getQuarterly, quarterly)
                            .eq(NGbCtMnAd::getCmId, nGbCtGzOrder.getCmId())
                    );
                    Integer zmd = nGbCtMnAd == null ? 0 : nGbCtMnAd.getAmount(); //知名度
                    //商誉
                    NGbCtCorporate nGbCtCorporate = nGbCtCorporateMapper.selectOne(new LambdaQueryWrapper<NGbCtCorporate>()
                            .eq(NGbCtCorporate::getContestId, contestId)
                            .eq(NGbCtCorporate::getStudentId, studentId)
                            .orderByDesc(NGbCtCorporate::getId)
                            .last(" limit 1")
                    );
                    Integer sy = nGbCtCorporate.getScore();
                    //参考价
                    Integer ckj = nGbCtGzOrder.getPrice();
                    //报价
                    Integer bj = nGbCtOrderApply.getApplyPrice();
                    //特性研发值
                    NGbCtDesignProgress nGbCtDesignProgress = nGbCtDesignProgressMapper.selectOne(new LambdaQueryWrapper<NGbCtDesignProgress>()
                            .eq(NGbCtDesignProgress::getContestId, contestId)
                            .eq(NGbCtDesignProgress::getStudentId, studentId)
                            .eq(NGbCtDesignProgress::getDesignNum, nGbCtGzOrder.getDesignNum())
                            .last(" limit 1")
                    );
                    Integer txyfz = nGbCtDesignProgress.getNowValue();

                    //市场占有率 = 上一年该市场销售额/上一年该市场所有销售额
                    BigDecimal sczyl;
                    //上一年该学生的市场销售额
                    Integer sale = gbCtXzOrderMapper.getStudentYearSales(contestId, studentId, nGbCtGzOrder.getCmId(), year - 1);
                    //上一年该市场销售额
                    Integer allSale = gbCtXzOrderMapper.getAllYearSales(contestId, nGbCtGzOrder.getCmId(), year - 1);
                    //如果去年市场销售额为空,则值为1
                    if (allSale == 0) {
                        sczyl = new BigDecimal(1);
                    } else {
                        sczyl = new BigDecimal(sale).divide(new BigDecimal(allSale), 2, BigDecimal.ROUND_HALF_UP);
                    }
                    //Y=（知名度+商誉*市场占有率*（参考价-报价）+1000*特性研发值）*10000
                    BigDecimal y = new BigDecimal(zmd)
                            .add(new BigDecimal(sy).multiply(sczyl).multiply(new BigDecimal(ckj - bj)))
                            .add(new BigDecimal(1000).multiply(new BigDecimal(txyfz)));
                    nGbCtOrderApply.setYValue(y);
                }

                //根据Y值从高到低进行订单分配
                //先获取最高Y值的 小组 进行分配，依次往下
                Set<BigDecimal> ySet = nGbCtOrderApplies.stream().map(NGbCtOrderApply::getYValue).collect(Collectors.toSet());
                List<BigDecimal> ylist = new ArrayList<>(ySet);
                Collections.sort(ylist,Comparator.reverseOrder());
                //根据Y值先对申请进行分组
                Map<BigDecimal,List<NGbCtOrderApply>> map = nGbCtOrderApplies.stream().collect(Collectors.groupingBy(NGbCtOrderApply::getYValue));
                //根据Y值从高到低
                Integer orderTotalNum = nGbCtGzOrder.getNum();
                for (BigDecimal y : ylist) {
                    if (orderTotalNum == 0) {
                        break;
                    } else {
                        //获取本轮参与分单的的申请
                        List<NGbCtOrderApply> applyList = map.get(y);
                        int rs = applyList.size();
                        //申请的总数量
                        int applyTotalNum  = applyList.stream().mapToInt(NGbCtOrderApply::getApplyNum).sum();
                        //如果申请数量小于或等于订单剩余的总数量的情况下，则所有人都拿满 (分配数量等于申请数量)
                        if (applyTotalNum <= orderTotalNum) {
                            for (NGbCtOrderApply nGbCtOrderApply : applyList) {
                                Integer assignedNum = nGbCtOrderApply.getApplyNum();
                                orderTotalNum = orderTotalNum - assignedNum;
                                //分配订单
                                NGbCtXzOrder nGbCtXzOrder = new NGbCtXzOrder();
                                BeanUtils.copyProperties(nGbCtGzOrder, nGbCtXzOrder);
                                nGbCtXzOrder.setContestId(contestId);
                                nGbCtXzOrder.setStudentId(nGbCtOrderApply.getStudentId());
                                nGbCtXzOrder.setIsoId(nGbCtGzOrder.getCiId());
                                nGbCtXzOrder.setAssignedNum(assignedNum);
                                nGbCtXzOrder.setApplyPrice(nGbCtOrderApply.getApplyPrice());
                                //组号
                                nGbCtXzOrder.setGroupNum(groupNumMap.get(nGbCtOrderApply.getStudentId()));
                                xzOrders.add(nGbCtXzOrder);
                            }
                        }
                        else{
                            //申请最少的数量
                            int minApplyNum = applyList.get(0).getApplyNum();
                            for (NGbCtOrderApply orderApply : applyList) {
                                minApplyNum = Math.min(minApplyNum,orderApply.getApplyNum());
                            }

                            int zs = minApplyNum * rs;
                            //如果申报少的数*同名次组数 <= 总数，则都拿到申报数量少的数，
                            //如果申报少的数*同名次组数 > 总数，则平分，平分的余数给下一个排名的组
                            if(zs<=orderTotalNum){
                                Integer assignedNum = minApplyNum;
                                for (NGbCtOrderApply nGbCtOrderApply : applyList) {
                                    orderTotalNum = orderTotalNum - assignedNum;
                                    //分配订单
                                    NGbCtXzOrder nGbCtXzOrder = new NGbCtXzOrder();
                                    BeanUtils.copyProperties(nGbCtGzOrder, nGbCtXzOrder);
                                    nGbCtXzOrder.setContestId(contestId);
                                    nGbCtXzOrder.setStudentId(nGbCtOrderApply.getStudentId());
                                    nGbCtXzOrder.setIsoId(nGbCtGzOrder.getCiId());
                                    nGbCtXzOrder.setAssignedNum(assignedNum);
                                    nGbCtXzOrder.setApplyPrice(nGbCtOrderApply.getApplyPrice());
                                    //组号
                                    nGbCtXzOrder.setGroupNum(groupNumMap.get(nGbCtOrderApply.getStudentId()));
                                    xzOrders.add(nGbCtXzOrder);
                                }
                            }else{
                                int assignedNum = orderTotalNum/rs;  //向下取整
                                for (NGbCtOrderApply nGbCtOrderApply : applyList) {
                                    orderTotalNum = orderTotalNum - assignedNum;
                                    //分配订单
                                    NGbCtXzOrder nGbCtXzOrder = new NGbCtXzOrder();
                                    BeanUtils.copyProperties(nGbCtGzOrder, nGbCtXzOrder);
                                    nGbCtXzOrder.setContestId(contestId);
                                    nGbCtXzOrder.setStudentId(nGbCtOrderApply.getStudentId());
                                    nGbCtXzOrder.setIsoId(nGbCtGzOrder.getCiId());
                                    nGbCtXzOrder.setAssignedNum(assignedNum);
                                    nGbCtXzOrder.setApplyPrice(nGbCtOrderApply.getApplyPrice());
                                    //组号
                                    nGbCtXzOrder.setGroupNum(groupNumMap.get(nGbCtOrderApply.getStudentId()));
                                    xzOrders.add(nGbCtXzOrder);
                                }
                            }
                        }
                    }
                }
            }
            //将订单的分配结果存库
            if (xzOrders.size() > 0) {
                nGbCtXzOrderService.saveBatch(xzOrders);
            }
            //更新教师端的订单状态
            contest.setFairOrderState(0);
            gbContestService.updateById(contest);
        }


        //选单结束备份当前的数据
        //备份学生数据
        List<NGbContestStudent> studentIdList = gbContestStudentMapper.selectList(new LambdaQueryWrapper<NGbContestStudent>()
                .eq(NGbContestStudent::getContestId,contestId)
        );
        //备份学生数据
        for (NGbContestStudent student : studentIdList) {
            gbRestoreDataBiz.backupData(contestId,student.getStudentId(),student.getDate()/10,true);
        }
        log.error("选单已结束,contestId:{},contest:",contestId,JSONObject.toJSONString(contest));




        //通知给班级所有的学生
        Map<String, Object> messageMap = new HashMap<>();
        messageMap.put("type","refresh"); //数据刷新
        String message = JSONObject.toJSONString(messageMap);
        NGbWebSocket.sendMessageToAll(contestId,message); //给所有人广播消息
        log.error("竞单管理-教师端-分配订单结束,contestId:{},date:{}", contestId, date);
        return R.success();
    }


    /**
     * 重新分配订单
     * @param contestId
     * @return
     */
    public R reAssignOrder(Integer contestId) {
        NGbContest nGbContest = gbContestService.getById(contestId);
        Integer teacherDate  = nGbContest.getTeacherDate();
        Integer lastFairOrderDate  = nGbContest.getLastFairOrderDate();

        log.error("竞单管理-教师端-重新分配订单开始,contestId:{},date:{}", contestId, teacherDate);

        if(!teacherDate.equals(lastFairOrderDate)){
            throw new CustomException(CodeEnum.OTHER_ERROR.getCode(), "当前季度未进行选单");
        }
        //当前竞单未结束
        if(nGbContest.getFairOrderState()==1){
            throw new CustomException(CodeEnum.OTHER_ERROR.getCode(), "本季度竞单还未结束");
        }

        int year = teacherDate/10;
        int quarterly = teacherDate%10;

        //判断本季度是否有订单已经进行了操作，进行了操作的情况下提醒其还原
        List<NGbCtXzOrder> commitOrders = gbCtXzOrderMapper.selectList(new LambdaQueryWrapper<NGbCtXzOrder>()
                .eq(NGbCtXzOrder::getContestId,contestId)
                .eq(NGbCtXzOrder::getDate,year) //年
                .eq(NGbCtXzOrder::getQuarterly,quarterly) //季度
                .isNotNull(NGbCtXzOrder::getCommitDate) //已经交单
        );
        if(CollectionUtils.isNotEmpty(commitOrders)){
            NGbCtXzOrder  order = commitOrders.get(0);
            throw new CustomException(CodeEnum.OTHER_ERROR.getCode(), order.getGroupNum()+"已对本次交货的订单进行过交货操作，请先该学生还原当季");
        }


        //删除已经分配的订单
        gbCtXzOrderMapper.delete(new LambdaQueryWrapper<NGbCtXzOrder>()
                .eq(NGbCtXzOrder::getContestId,contestId)
                .eq(NGbCtXzOrder::getDate,year) //年
                .eq(NGbCtXzOrder::getQuarterly,quarterly) //季度
        );

        //重新进行订单分配
        nGbContest.setFairOrderState(1);  //更新到竞单状态
        gbContestService.updateById(nGbContest);
        allocationOrder(contestId,teacherDate);


        return R.success();
    }



    /**
     * 获取当季所有的订单
     * @param contestId
     * @param date
     * @param userId
     * @return
     */
    public R getMarketOrderList(Integer contestId, Integer date, Integer userId) {
        List<NGbCtGzOrder> list = nGbCtGzOrderMapper.getMarketOrderList(contestId,userId,date/10,date%10);

        NGbContest nGbContest = gbContestService.getById(contestId);
        Integer subjectNumber = nGbContest.getSubjectNumber();
        //获取ISO名字
        List<NGbCtGzIso> ctGzIsoList = nGbCtGzIsoMapper.selectList(new LambdaQueryWrapper<NGbCtGzIso>()
                .eq(NGbCtGzIso::getSubjectNumber,subjectNumber)
        );
        Map<Integer, String> isoNameMap = ctGzIsoList.stream().collect(Collectors.toMap(NGbCtGzIso::getCiId, NGbCtGzIso::getCiName, (k1, k2) -> k1));

        //获取市场名字
        List<NGbCtGzMarket> ctGzMarketList = nGbCtGzMarketMapper.selectList(new LambdaQueryWrapper<NGbCtGzMarket>()
                .eq(NGbCtGzMarket::getSubjectNumber,subjectNumber)
        );
        Map<Integer, String> marketNameMap = ctGzMarketList.stream().collect(Collectors.toMap(NGbCtGzMarket::getCmId, NGbCtGzMarket::getCmName, (k1, k2) -> k1));

        //获取产品名字
        List<NGbCtGzProduct> ctGzProductList = nGbCtGzProductMapper.selectList(new LambdaQueryWrapper<NGbCtGzProduct>()
                .eq(NGbCtGzProduct::getSubjectNumber,subjectNumber)
        );
        Map<String, String> productNameMap = ctGzProductList.stream().collect(Collectors.toMap(NGbCtGzProduct::getCpId, NGbCtGzProduct::getCpName, (k1, k2) -> k1));


        //获取特性名称
        List<NGbCtGzProductDesign> ctGzProductDesignList = nGbCtGzProductDesignMapper.selectList(new LambdaQueryWrapper<NGbCtGzProductDesign>()
                .eq(NGbCtGzProductDesign::getSubjectNum,subjectNumber)
        );
        Map<String, String> designNameMap = ctGzProductDesignList.stream().collect(Collectors.toMap(NGbCtGzProductDesign::getDesignNum, NGbCtGzProductDesign::getFeatureName, (k1, k2) -> k1));
        list.forEach(item->{
            //市场名称
            item.setCmName(marketNameMap.get(item.getCmId()));
            //产品名称
            item.setCpName(productNameMap.get(item.getCpId()));
            //特性名称
            item.setFeatureName(designNameMap.get(item.getDesignNum()));
            //ISO名称
            //设置ISO
            if (item.getCiId() == null) {
                item.setCiName("-");
            }else {
                item.setCiName(isoNameMap.get(item.getCiId()));
            }
        });
        return R.success().put("list",list);
    }


    /**
     * 获取已经申报的订单
     * @param contestId
     * @param date
     * @param userId
     * @return
     */
    public R getApplyOrderList(Integer contestId, Integer date, Integer userId) {
        List<NGbCtGzOrder> list = nGbCtGzOrderMapper.getApplyOrderList(contestId,userId,date/10,date%10);
        NGbContest nGbContest = gbContestService.getById(contestId);
        Integer subjectNumber = nGbContest.getSubjectNumber();

        //获取ISO名字
        List<NGbCtGzIso> ctGzIsoList = nGbCtGzIsoMapper.selectList(new LambdaQueryWrapper<NGbCtGzIso>()
                .eq(NGbCtGzIso::getSubjectNumber,subjectNumber)
        );
        Map<Integer, String> isoNameMap = ctGzIsoList.stream().collect(Collectors.toMap(NGbCtGzIso::getCiId, NGbCtGzIso::getCiName, (k1, k2) -> k1));

        //获取市场名字
        List<NGbCtGzMarket> ctGzMarketList = nGbCtGzMarketMapper.selectList(new LambdaQueryWrapper<NGbCtGzMarket>()
                .eq(NGbCtGzMarket::getSubjectNumber,subjectNumber)
        );
        Map<Integer, String> marketNameMap = ctGzMarketList.stream().collect(Collectors.toMap(NGbCtGzMarket::getCmId, NGbCtGzMarket::getCmName, (k1, k2) -> k1));

        //获取产品名字
        List<NGbCtGzProduct> ctGzProductList = nGbCtGzProductMapper.selectList(new LambdaQueryWrapper<NGbCtGzProduct>()
                .eq(NGbCtGzProduct::getSubjectNumber,subjectNumber)
        );
        Map<String, String> productNameMap = ctGzProductList.stream().collect(Collectors.toMap(NGbCtGzProduct::getCpId, NGbCtGzProduct::getCpName, (k1, k2) -> k1));

        //获取特性名称
        List<NGbCtGzProductDesign> ctGzProductDesignList = nGbCtGzProductDesignMapper.selectList(new LambdaQueryWrapper<NGbCtGzProductDesign>()
                .eq(NGbCtGzProductDesign::getSubjectNum,subjectNumber)
        );
        Map<String, String> designNameMap = ctGzProductDesignList.stream().collect(Collectors.toMap(NGbCtGzProductDesign::getDesignNum, NGbCtGzProductDesign::getFeatureName, (k1, k2) -> k1));
        list.forEach(item->{
            //市场名称
            item.setCmName(marketNameMap.get(item.getCmId()));
            //产品名称
            item.setCpName(productNameMap.get(item.getCpId()));
            //特性名称
            item.setFeatureName(designNameMap.get(item.getDesignNum()));
            //ISO名称
            //设置ISO
            if (item.getCiId() == null) {
                item.setCiName("-");
            } else {
                item.setCiName(isoNameMap.get(item.getCiId()));
            }
        });

        return R.success().put("list",list);
    }




//    /**
//     * 开启订货会
//     * @param contestId
//     */
//    public R startTrade(Integer contestId,Integer date,Integer fairOrderState){
//        switch (fairOrderState){
//            case 1:
//                return startAd(contestId,date);
//            case 2:
//                //初始化
//                return showAd(contestId);
//            case 3:
//                return startChoose(contestId,date);
//            default:
//                return ErrorEnum.FAIR_ORDER_STATE_ERROR.getR();
//        }
//    }


//    public  R pauseOrContinue(Integer contestId ,Integer fairOrderState){
//        //继续
//        if(fairOrderState == 3){
//            return continueChoose(contestId,fairOrderState);
//        }
//        //暂停
//        if(fairOrderState == 4){
//            return pauseChoose(contestId,fairOrderState);
//        }
//        return ErrorEnum.FAIR_ORDER_STATE_ERROR.getR();
//    }


//    /**
//     * 暂停选单
//     * @param contestId
//     * @return
//     */
//    public synchronized  R pauseChoose(Integer contestId,Integer fairOrderState){
//        tradeFairBizTranscation.pauseChoose(contestId,fairOrderState);
//        return R.success();
//    }
//
//    /**
//     * 继续选单
//     * @param contestId
//     * @return
//     */
//    public synchronized R continueChoose(Integer contestId,Integer fairOrderState){
//        tradeFairBizTranscation.continueChoose(contestId,fairOrderState);
//        return R.success();
//    }


//    /**
//     * 开始选单
//     * @param
//     */
//    public synchronized R startChoose(Integer contestId,Integer date){
//        //开始选单
//        tradeFairBizTranscation.startChoose(contestId,date);
//        return R.success();
//    }


//    /**
//     * 选单
//     * @param
//     * @return
//     */
//    public synchronized  R  chooseOrder(Integer contestId,Integer studentId,String market,Integer chooseId) {
//        tradeFairBizTranscation.chooseOrder(contestId,studentId,market,chooseId);
//        return R.success();
//    }


//    /**
//     * 放弃选单
//     * @param contestId
//     * @param studentId
//     * @param market
//     * @param isTimeOut
//     * @return
//     */
//    public synchronized R  giveUpOrder(Integer contestId,Integer studentId,String market,boolean isTimeOut) {
//        tradeFairBizTranscation.giveUpOrder(contestId,studentId,market,isTimeOut);
//        return R.success();
//    }



//    /**
//     * 还原订单
//     * @param contestId
//     * @param date
//     * @return
//     */
//    public synchronized R restoreOrder(Integer contestId, Integer date) {
//        tradeFairBizTranscation.restoreOrder(contestId,date);
//        return R.success();
//    }
//


//    /**
//     * 学生获取广告信息和选单列表
//     * @param
//     * @return
//     */
//    public R getUserAndOrder(Integer contestId,Integer studentId,String market) {
//        return tradeFairBizTranscation.getUserAndOrder(contestId,studentId,market);
//    }
//
//






    /**
     * 临时添加订单
     * @param
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    public R addOrder(NGbCtXzOrder nGbCtXzOrder) {

        Integer contestId = nGbCtXzOrder.getContestId();
        Integer studentId = nGbCtXzOrder.getStudentId();
        Integer assignedNum = nGbCtXzOrder.getAssignedNum(); //分配数量
        Integer applyPrice = nGbCtXzOrder.getApplyPrice(); // 申请价格
        String coId = nGbCtXzOrder.getCoId(); //订单ID



        List<NGbCtXzOrder> nGbCtXzOrders = nGbCtXzOrderService.list(new LambdaQueryWrapper<NGbCtXzOrder>()
                .eq(NGbCtXzOrder::getContestId,contestId)
                .eq(NGbCtXzOrder::getStudentId,studentId)
                .eq(NGbCtXzOrder::getCoId,coId)
        );
        if(nGbCtXzOrders!=null && nGbCtXzOrders.size()>0){
            return R.error(CodeEnum.PARAM_ERROR.getCode(), "当前学生已有该订单，不允许重复分配");
        }

        NGbContest nGbContest = gbContestService.getById(contestId);
        NGbCtGzOrder nGbCtGzOrder  = nGbCtGzOrderMapper.selectOne(new LambdaQueryWrapper<NGbCtGzOrder>()
                .eq(NGbCtGzOrder::getSubjectNumber,nGbContest.getSubjectNumber())
                .eq(NGbCtGzOrder::getCoId,coId)
        );

        //查询学生当前的时间
        Integer date = gbContestStudentService.getStudentDate(contestId,studentId);
        Integer currentTime = date/10;
        //判断订单时间和学生时间是否相同
        if(currentTime!=(nGbCtGzOrder.getDate()*10 +nGbCtGzOrder.getQuarterly())){
            return R.error(CodeEnum.PARAM_ERROR.getCode(), "当前学生的处于"+(date/10)+"年"+(date%10)+"季，与订单时间不符");

        }

        //分配给学生
        NGbCtXzOrder ctXzOrder = new NGbCtXzOrder()
                .setCoId(coId)
                .setStudentId(studentId)
                .setContestId(contestId)
                .setDate(nGbCtGzOrder.getDate())
                .setQuarterly(nGbCtGzOrder.getQuarterly())
                .setCmId(nGbCtGzOrder.getCmId())
                .setCpId(nGbCtGzOrder.getCpId())
                .setDesignNum(nGbCtGzOrder.getDesignNum())
                .setDesignNum(nGbCtGzOrder.getDesignNum())
                .setNum(nGbCtGzOrder.getNum())
                .setPrice(nGbCtGzOrder.getPrice())
                .setDeliveryDate(nGbCtGzOrder.getDeliveryDate())
                .setPaymentDate(nGbCtGzOrder.getPaymentDate())
                .setIsoId(nGbCtGzOrder.getCiId())
                .setAssignedNum(assignedNum)  //分配数量
                .setApplyPrice(applyPrice);  //申请价格

        gbCtXzOrderMapper.insert(ctXzOrder);

        //TODO 通知到学生

        return R.success();
    }



    /**
     * 获取选单
     * @param
     * @param
     * @return
     */
    public R getMnOrder(NGbCtMnChooseParam gbCtMnChooseParam){
        Integer contestId = gbCtMnChooseParam.getContestId();
        for (Integer date : gbCtMnChooseParam.getDates()) {
            List<NGbCtMnChooseParam.yearSeason> list = new ArrayList<>();
            NGbCtMnChooseParam.yearSeason yearSeason = new NGbCtMnChooseParam.yearSeason();
            yearSeason.setYear(date/10);
            yearSeason.setQuarterly(date%10);
            list.add(yearSeason);
            gbCtMnChooseParam.setYearSeasons(list);
        }
        Page<NGbCtXzOrder> page = new Page<>(gbCtMnChooseParam.getPage(), gbCtMnChooseParam.getLimit());
        gbCtXzOrderMapper.getPage(page, gbCtMnChooseParam);

        Map<String, Object> returnMap = new HashMap<>();

        //订单状态
        //设置状态  违约/已交货/未交货
        List<NGbCtGzIso> ctGzIsoList = nGbCtGzIsoMapper.list(contestId);
        Map<Integer, String> isoNameMap = ctGzIsoList.stream().collect(Collectors.toMap(NGbCtGzIso::getCiId, NGbCtGzIso::getCiName, (k1, k2) -> k1));
        List<NGbCtXzOrder> list = page.getRecords();
        list.forEach(p->{
            //设置ISO
            if (p.getIsoId() == null) {
                p.setCiName("-");
            } else {
                p.setCiName(isoNameMap.get(p.getIsoId()));
            }

            //设置状态  已交货/未交货
            String status = "";
            if (p.getCommitDate() != null && p.getCommitDate() > 0) {
                status = "已交货";
            }
            else{
                status = "未交货";
            }
            p.setStatus(status);
        });

        returnMap.put("total", page.getTotal());
        returnMap.put("list", page.getRecords());
        return R.success(returnMap);
    }




    /**
     * 获取当季所有广告
     * @param contestId
     * @param date
     * @return
     */
    public R adList(Integer contestId, Integer date){

        //学生和账号的map
        Map<Integer, String> studentMap = gbContestStudentService.getStudentIdAndAccountMap(contestId);

        List resultList = new ArrayList<>();
        //一个市场一组数据, 每组罗列学生的排名
        NGbContest nGbContest = gbContestService.getById(contestId);
        List<NGbCtGzMarket> nGbCtGzMarketList  = nGbCtGzMarketMapper.selectList(new LambdaQueryWrapper<NGbCtGzMarket>()
                .eq(NGbCtGzMarket::getSubjectNumber,nGbContest.getSubjectNumber())
        );

        for (NGbCtGzMarket nGbCtGzMarket : nGbCtGzMarketList) {
            //获取当前市场各个学生广告投放的排名情况
            List<NGbCtMnAd> nGbCtMnAds = gbCtMnAdService.getAdRank(contestId,date/10,date%10,nGbCtGzMarket.getCmId(),nGbCtGzMarket.getCmName());
            //设置组号
            nGbCtMnAds.forEach(item->{
                item.setGroupNum(studentMap.get(item.getStudentId()));
            });
            resultList.add(nGbCtMnAds);
        }


        return R.success().put("list",resultList);
    }



    /**
     * 获取当季订单
     * @param contestId
     * @param date
     * @return
     */
    public R getChooseOrder(Integer contestId, Integer date) {
        NGbContest contest = gbContestService.getById(contestId);
        Integer year = date/10;
        Integer quarterly = date%10;
        //对订单进行分配
        //先获取当前季度的所有市场订单
        List<NGbCtGzOrder> gzOrderList = nGbCtGzOrderMapper.selectList(new LambdaQueryWrapper<NGbCtGzOrder>()
                .eq(NGbCtGzOrder::getSubjectNumber, contest.getSubjectNumber())
                .eq(NGbCtGzOrder::getDate, year)
                .eq(NGbCtGzOrder::getQuarterly, quarterly)
        );

        return R.success().put("list",gzOrderList);
    }


    /**
     * 竞单
     * @param nGbCtOrderApply
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    public R applyOrder(NGbCtOrderApply nGbCtOrderApply) {
        //先判断竞单的状态 为1可以竞单
        Integer contestId = nGbCtOrderApply.getContestId();
        Integer studentId = nGbCtOrderApply.getStudentId();
        String coid = nGbCtOrderApply.getCoId();
        //报价
        Integer applyPrice = nGbCtOrderApply.getApplyPrice();
        //申报数量
        Integer applyNum = nGbCtOrderApply.getApplyNum();
        NGbContest nGbContest = gbContestService.getById(contestId);
        Integer fairOrderState = nGbContest.getFairOrderState();
        if (fairOrderState != 1) {
            throw new CustomException(CodeEnum.OTHER_ERROR.getCode(), "当前不在竞单阶段，不允许竞单");
        }

        //判断申报值是否处于90%-100%之间
        NGbCtGzOrder nGbCtGzOrder = nGbCtGzOrderMapper.selectOne(new LambdaQueryWrapper<NGbCtGzOrder>()
                .eq(NGbCtGzOrder::getSubjectNumber,nGbContest.getSubjectNumber())
                .eq(NGbCtGzOrder::getDate,nGbCtOrderApply.getYear())
                .eq(NGbCtGzOrder::getQuarterly,nGbCtOrderApply.getQuarterly())
                .eq(NGbCtGzOrder::getCoId,nGbCtOrderApply.getCoId())
        );

        double low = nGbCtGzOrder.getPrice()*0.9;
        if(applyPrice<low || applyPrice> nGbCtGzOrder.getPrice()){
            throw new CustomException(CodeEnum.OTHER_ERROR.getCode(), "订单的报价需要在参考价90%-100%之间");
        }


        //判断申报的订单数量不能超过总数量的30%
        Integer limitOrder = nGbContest.getLimitOrder();
        if(limitOrder==1){
            //订单总数量
            Integer totalNum = nGbCtGzOrder.getNum();
            //totalNum * 0.3 取整,舍去小数
            Integer limitNum = (int) (totalNum*0.3);
            if(applyNum>limitNum){
                throw new CustomException(CodeEnum.OTHER_ERROR.getCode(), "最大获取订单不得超过订单30%");
            }
        }

        //获取对应的市场规则
        NGbCtGzMarket nGbCtGzMarket = nGbCtGzMarketMapper.selectOne(new LambdaQueryWrapper<NGbCtGzMarket>()
                .eq(NGbCtGzMarket::getSubjectNumber,nGbContest.getSubjectNumber())
                .eq(NGbCtGzMarket::getCmId,nGbCtGzOrder.getCmId())
        );

        //查询市场的研发状态
        NGbCtYfMarket nGbCtYfMarket = nGbCtYfMarketMapper.selectOne(new LambdaQueryWrapper<NGbCtYfMarket>()
                .eq(NGbCtYfMarket::getContestId,contestId)
                .eq(NGbCtYfMarket::getStudentId,studentId)
                .eq(NGbCtYfMarket::getDmCmId,nGbCtGzMarket.getId())
        );

        //申报订单先判断市场是否研发，为研发当前市场不允许竞单
        if (!"已完成".equals(nGbCtYfMarket.getDmState())){
            throw new CustomException(CodeEnum.OTHER_ERROR.getCode(), "当前市场的研发尚未完成，不能参与当前市场的竞单");
        }

        //判断ISO
        Integer ciId = nGbCtGzOrder.getCiId();
        if(ciId!=null){
            // 查出所有研发完成的iso
            List<NGbCtYfIso> ctYfIsoList = ngbCtYfIsoMapper.getYfFinishList(contestId,studentId);
            // 将研发完成的编号放入set中，便于查询
            Set<Integer> isoSet = ctYfIsoList.stream().map(NGbCtYfIso::getIsoNum).collect(Collectors.toSet());
            if (!isoSet.contains(ciId)){
                throw new CustomException(CodeEnum.OTHER_ERROR.getCode(), "当前订单的ISO尚未研发完成，不能进行申报");
            }
        }



//        for (NGbCtMnChoose ctMnChoose : ctMnChooseList) {
//            // 滤掉已经选走的订单
//            if (ctMnChoose.getXzGroup()!=null) {
//                continue;
//            }
//            // 如果这个单据ISO为空，就是可以选。
//            // 如果为1，那该组ISO9000已经开发完成就可以选
//            // 如果为2，那该组ISO14000已经开发完成就可以选
//            // 如果为3，那需要两个都开发完成才能选
//            Integer iso = ctMnChoose.getCiId();
//            // 判断需要的iso认证是否研发完成
//            boolean isoFinish = ((iso == null) || ((iso <= 2) && isoSet.contains(iso)) || ((iso == 3) && isoSet.contains(1) && isoSet.contains(2)));
//            ctMnChoose.setChoosable(isoFinish ? 1 : 0);
//            chooseList.add(ctMnChoose);
//        }



        Integer year = nGbCtOrderApply.getYear();
        Integer quarterly = nGbCtOrderApply.getQuarterly();
        //先删除当前订单的申报
        nGbCtOrderApplyMapper.delete(new LambdaQueryWrapper<NGbCtOrderApply>()
                .eq(NGbCtOrderApply::getContestId,contestId)
                .eq(NGbCtOrderApply::getStudentId,studentId)
                .eq(NGbCtOrderApply::getYear,year)
                .eq(NGbCtOrderApply::getQuarterly,quarterly)
                .eq(NGbCtOrderApply::getCoId,coid)
        );
        if(nGbCtOrderApply.getApplyNum() != 0){
            nGbCtOrderApplyMapper.insert(nGbCtOrderApply);
        }
        return R.success();
    }

    /**
     * 获取本季度已分配的订单
     * @param nGbCtXzOrder
     * @return
     */
    public R getAssignedOrderList(NGbCtXzOrder nGbCtXzOrder) {
        Integer contestId = nGbCtXzOrder.getContestId();
        NGbContest nGbContest = gbContestService.getById(contestId);
        Integer subjectNumber = nGbContest.getSubjectNumber();

        List<NGbCtXzOrder> list = gbCtXzOrderMapper.selectList(new LambdaQueryWrapper<NGbCtXzOrder>()
                .eq(NGbCtXzOrder::getContestId,contestId)
                .eq(NGbCtXzOrder::getDate,nGbCtXzOrder.getDate())
                .eq(NGbCtXzOrder::getQuarterly,nGbCtXzOrder.getQuarterly())
        );
        //根据产品过滤
        if(!StringUtils.isEmpty(nGbCtXzOrder.getCpId())){
           list =  list.stream().filter(item->item.getCpId().equals(nGbCtXzOrder.getCpId())).collect(Collectors.toList());
        }
        //根据组号对数据过滤
        if(!StringUtils.isEmpty(nGbCtXzOrder.getGroupNum())){
            list =  list.stream().filter(item->item.getGroupNum().equals(nGbCtXzOrder.getGroupNum())).collect(Collectors.toList());
        }



        //获取ISO名字
        List<NGbCtGzIso> ctGzIsoList = nGbCtGzIsoMapper.selectList(new LambdaQueryWrapper<NGbCtGzIso>()
                .eq(NGbCtGzIso::getSubjectNumber,subjectNumber)
        );
        Map<Integer, String> isoNameMap = ctGzIsoList.stream().collect(Collectors.toMap(NGbCtGzIso::getCiId, NGbCtGzIso::getCiName, (k1, k2) -> k1));

        //获取市场名字
        List<NGbCtGzMarket> ctGzMarketList = nGbCtGzMarketMapper.selectList(new LambdaQueryWrapper<NGbCtGzMarket>()
                .eq(NGbCtGzMarket::getSubjectNumber,subjectNumber)
        );
        Map<Integer, String> marketNameMap = ctGzMarketList.stream().collect(Collectors.toMap(NGbCtGzMarket::getCmId, NGbCtGzMarket::getCmName, (k1, k2) -> k1));

        //获取产品名字
        List<NGbCtGzProduct> ctGzProductList = nGbCtGzProductMapper.selectList(new LambdaQueryWrapper<NGbCtGzProduct>()
                .eq(NGbCtGzProduct::getSubjectNumber,subjectNumber)
        );
        Map<String, String> productNameMap = ctGzProductList.stream().collect(Collectors.toMap(NGbCtGzProduct::getCpId, NGbCtGzProduct::getCpName, (k1, k2) -> k1));

        //获取特性名称
        List<NGbCtGzProductDesign> ctGzProductDesignList = nGbCtGzProductDesignMapper.selectList(new LambdaQueryWrapper<NGbCtGzProductDesign>()
                .eq(NGbCtGzProductDesign::getSubjectNum,subjectNumber)
        );
        Map<String, String> designNameMap = ctGzProductDesignList.stream().collect(Collectors.toMap(NGbCtGzProductDesign::getDesignNum, NGbCtGzProductDesign::getFeatureName, (k1, k2) -> k1));
        list.forEach(item->{
            //市场名称
            item.setCmName(marketNameMap.get(item.getCmId()));
            //产品名称
            item.setCpName(productNameMap.get(item.getCpId()));
            //特性名称
            item.setFeatureName(designNameMap.get(item.getDesignNum()));
            //ISO名称
            //设置ISO
            if (item.getIsoId() == null) {
                item.setCiName("-");
            } else {
                item.setCiName(isoNameMap.get(item.getIsoId()));
            }
        });


        return R.success().put("list",list);
    }


    /**
     * 获取可选订单的时间
     * @param contestId
     * @return
     */
    public R getOrderDate(Integer contestId) {

        //过滤掉小于或等于最后一次选单时间的
        NGbContest gbContest = gbContestService.getById(contestId);
        Integer subjectNumber = gbContest.getSubjectNumber();

        List<NGbCtGzOrder>  gbCtGzOrders= nGbCtGzOrderMapper.getOrderDate(subjectNumber);

        int lastFairOrderDate = 10;
        if (!StringUtils.isEmpty(gbContest.getLastFairOrderDate())) {
            lastFairOrderDate = gbContest.getLastFairOrderDate();
        }
        //获取时间线最大的学生的时间
        NGbContestStudent maxNGbContestStudent = gbContestStudentMapper.selectOne(new LambdaQueryWrapper<NGbContestStudent>()
                .eq(NGbContestStudent::getContestId, contestId)
                .orderByDesc(NGbContestStudent::getDate)
                .last("limit 1")
        );

        int finalLastFairOrderDate = lastFairOrderDate;
        gbCtGzOrders = gbCtGzOrders.stream().filter(item -> (item.getDate() * 10 + item.getQuarterly()) > finalLastFairOrderDate).collect(Collectors.toList());


        if (maxNGbContestStudent != null) {
            int maxDate = maxNGbContestStudent.getDate() / 10;
            gbCtGzOrders.stream().forEach(
                    item -> {
                        if ((item.getDate() * 10 + item.getQuarterly()) != maxDate) {
                            item.setDisabled(true);
                        }
                    }
            );
        }
        return R.success().put("list", gbCtGzOrders);
    }


    @Autowired
    private NGbCtYfNumMapper nGbCtYfNumMapper;

    @Autowired
    private NGbCtGzDrainageMapper nGbCtGzDrainageMapper;

    @Autowired
    private NGbCtGzRetailMarketMapper nGbCtGzRetailMarketMapper;

    @Autowired
    private NGbCtRetailApplyMapper nGbCtRetailApplyMapper;

    @Autowired
    private NGbCtMemberIndexMapper nGbCtMemberIndexMapper;

    @Autowired
    private NGbCtRetailAssignMapper nGbCtRetailAssignMapper;



    @Autowired
    private NGbCtCashflowService gbCtCashflowService;

    @Autowired
    private NGbActionBiz gbActionBiz;

    @Autowired
    private NGbCtGzProducingMapper nGbCtGzProducingMapper;


    @Autowired
    private NGbCtGzMaterialMapper nGbCtGzMaterialMapper;




    public R getNetworkInfo(Integer contestId, Integer userId, Integer currentTime) {
        Integer year = currentTime/10;
        Integer quarterly = currentTime%10;
        NGbContest nGbContest = gbContestService.getById(contestId);
        Integer subjectNumber = nGbContest.getSubjectNumber();

        //获取产品名字
        List<NGbCtGzProduct> ctGzProductList = nGbCtGzProductMapper.selectList(new LambdaQueryWrapper<NGbCtGzProduct>()
                .eq(NGbCtGzProduct::getSubjectNumber,subjectNumber)
        );
        Map<String, String> productNameMap = ctGzProductList.stream().collect(Collectors.toMap(NGbCtGzProduct::getCpId, NGbCtGzProduct::getCpName, (k1, k2) -> k1));


        //获取特性名称
        List<NGbCtGzProductDesign> ctGzProductDesignList = nGbCtGzProductDesignMapper.selectList(new LambdaQueryWrapper<NGbCtGzProductDesign>()
                .eq(NGbCtGzProductDesign::getSubjectNum,subjectNumber)
        );
        Map<String, String> designNameMap = ctGzProductDesignList.stream().collect(Collectors.toMap(NGbCtGzProductDesign::getDesignNum, NGbCtGzProductDesign::getFeatureName, (k1, k2) -> k1));


        //网络投放
        //获取零售市场本季度的订单
        List<NGbCtGzRetailMarket> marketList = nGbCtGzRetailMarketMapper.selectList(new LambdaQueryWrapper<NGbCtGzRetailMarket>()
                .eq(NGbCtGzRetailMarket::getSubjectNumber,subjectNumber)
                .eq(NGbCtGzRetailMarket::getYear,year)
                .eq(NGbCtGzRetailMarket::getQuarterly,quarterly)
        );
        List<NGbCtRetailApply> applyList = marketList.stream().map(item->{
           NGbCtRetailApply nGbCtRetailApply  = new NGbCtRetailApply();

           BeanUtils.copyProperties(item,nGbCtRetailApply);
           nGbCtRetailApply.setContestId(contestId);
           nGbCtRetailApply.setStudentId(userId);
           nGbCtRetailApply.setRetailId(item.getId());
           nGbCtRetailApply.setCpName(productNameMap.get(item.getCpId()));
           nGbCtRetailApply.setDesignName(designNameMap.get(item.getDesignNum()));

           //能承受的最大价格
           nGbCtRetailApply.setSupportPrice(item.getSupportPrice());


           //M - 5M  M为生产该材料所需的成本
           String cpId = nGbCtRetailApply.getCpId();
           List<NGbCtGzProducing> gbCtGzProducings = nGbCtGzProducingMapper.selectList(new LambdaQueryWrapper<NGbCtGzProducing>()
                   .eq(NGbCtGzProducing::getSubjectNumber,subjectNumber)
                   .eq(NGbCtGzProducing::getCpId,cpId)
           );
           Integer m = 0;
           for (NGbCtGzProducing producing : gbCtGzProducings) {
               Integer cpNum = producing.getCpNum();
               //获取原材料规则表
               NGbCtGzMaterial nGbCtGzMaterial = nGbCtGzMaterialMapper.selectOne(new LambdaQueryWrapper<NGbCtGzMaterial>()
                       .eq(NGbCtGzMaterial::getSubjectNumber,subjectNumber)
                       .eq(NGbCtGzMaterial::getCmId,producing.getCpMid())
               );
               m += (cpNum*nGbCtGzMaterial.getCmBuyFee()) ;
           }
           nGbCtRetailApply.setM(m);


           //获取定价和数量
           NGbCtRetailApply apply  = nGbCtRetailApplyMapper.selectOne(new LambdaQueryWrapper<NGbCtRetailApply>()
                   .eq(NGbCtRetailApply::getContestId,contestId)
                   .eq(NGbCtRetailApply::getStudentId,userId)
                   .eq(NGbCtRetailApply::getRetailId,nGbCtRetailApply.getRetailId())
           );
           if(apply!=null){
               nGbCtRetailApply.setApplyNum(apply.getApplyNum());
               nGbCtRetailApply.setApplyPrice(apply.getApplyPrice());
           }

           return nGbCtRetailApply;
       }).collect(Collectors.toList());

        //判断是否已经提交过网络市场申请
        boolean isSubmit = false;
        List<NGbCtRetailApply> nGbCtRetailApplyList  = nGbCtRetailApplyMapper.selectList(new LambdaQueryWrapper<NGbCtRetailApply>()
                .eq(NGbCtRetailApply::getContestId,contestId)
                .eq(NGbCtRetailApply::getStudentId,userId)
                .eq(NGbCtRetailApply::getYear,year)
                .eq(NGbCtRetailApply::getQuarterly,quarterly)
        );
        if(CollectionUtils.isNotEmpty(nGbCtRetailApplyList)){
            isSubmit = true;
        }

        //新媒体广告
        NGbCtMemberIndex nGbCtMemberIndex = nGbCtMemberIndexMapper.selectOne(new LambdaQueryWrapper<NGbCtMemberIndex>()
                .eq(NGbCtMemberIndex::getContestId,contestId)
                .eq(NGbCtMemberIndex::getStudentId,userId)
                .eq(NGbCtMemberIndex::getYear,year)
                .eq(NGbCtMemberIndex::getQuarterly,quarterly)
        );
        Integer memberIndex = nGbCtMemberIndex.getMemberIndex();
        Map<String,Object> map = new HashMap<>();
        map.put("memberIndex",memberIndex);
        map.put("applyList",applyList);
        map.put("isSubmit",isSubmit);
        return R.success(map);
    }




    public R submitNetworkPlan(NGbCtNetworkPlanParam nGbCtNetworkPlanParam, Integer userId) {
       Integer currentTime = nGbCtNetworkPlanParam.getCurrentTime();
       Integer contestId = nGbCtNetworkPlanParam.getContestId();
       Integer year = currentTime/10;
       Integer quarterly = currentTime%10;

        //阻止暂停状态下 和 破产状态下的用户进行操作
        gbActionBiz.preventAction(contestId,userId);

       NGbContest nGbContest = gbContestService.getById(contestId);


       //判断营销数字化研发是否已经完成
        NGbCtYfNum nGbCtYfNum  = nGbCtYfNumMapper.selectOne(new LambdaQueryWrapper<NGbCtYfNum>()
                .eq(NGbCtYfNum::getContestId,contestId)
                .eq(NGbCtYfNum::getStudentId,userId)
                .eq(NGbCtYfNum::getPostNum,1)
                .last("limit 1")
        );
        if(!NGbCtYfNum.NUM_YF_FINISH.equals(nGbCtYfNum.getState())){
            throw new CustomException(CodeEnum.OTHER_ERROR.getCode(), "当前营销数字化研发未完成不允许进行网络营销");
        }


       //网络投放
       // 先判断当前本季度是否已经有了网络投放,同一个季度不能多次网络投放
       List<NGbCtRetailApply> nGbCtRetailApplies = nGbCtRetailApplyMapper.selectList(new LambdaQueryWrapper<NGbCtRetailApply>()
                .eq(NGbCtRetailApply::getContestId,contestId)
                .eq(NGbCtRetailApply::getStudentId,userId)
                .eq(NGbCtRetailApply::getYear,year)
                .eq(NGbCtRetailApply::getQuarterly,quarterly)
        );
        if(CollectionUtils.isEmpty(nGbCtRetailApplies)){
            List<NGbCtRetailApply> applyList = nGbCtNetworkPlanParam.getApplyList();
            applyList = applyList.stream().filter(item-> (item.getApplyNum()!=null) && (item.getApplyNum()>0)).collect(Collectors.toList());
            for (NGbCtRetailApply nGbCtRetailApply : applyList) {
                nGbCtRetailApplyMapper.insert(nGbCtRetailApply);
            }
        }



       //新媒体广告投放
       Integer adAmount = nGbCtNetworkPlanParam.getAdAmount();
       if(adAmount!=null && adAmount>0){
           int cash = gbCtCashflowService.getCash(userId,contestId);
           if (adAmount > cash ) {
               return R.error(CodeEnum.OTHER_ERROR.getCode(), "现金不足");
           }

           // Cashflow表写入ID，支付广告费，0，所有填写金额之和，现金+流入-流出，投放广告费，当前时间
           gbCtCashflowService.save(new NGbCtCashflow().setStudentId(userId)
                   .setContestId(contestId)
                   .setCAction(CashFlowActionEnum.XMTGG.getAction())
                   .setCIn(0)
                   .setCOut(adAmount)
                   .setCSurplus(cash - adAmount)
                   .setCComment("新媒体广告"+adAmount+"元")
                   .setCDate(currentTime));

           //判断当前季度是否有新媒体投放
           NGbCtMemberIndex nGbCtMemberIndex = nGbCtMemberIndexMapper.selectOne(new LambdaQueryWrapper<NGbCtMemberIndex>()
                   .eq(NGbCtMemberIndex::getContestId,contestId)
                   .eq(NGbCtMemberIndex::getStudentId,userId)
                   .eq(NGbCtMemberIndex::getYear,year)
                   .eq(NGbCtMemberIndex::getQuarterly,quarterly)
           );
           Integer heatNum = nGbCtMemberIndex.getHeatNum();
           heatNum +=adAmount;
           //获取引流参数
           //先获取引流参数的值
           NGbCtGzDrainage nGbCtGzDrainage = nGbCtGzDrainageMapper.selectOne(new LambdaQueryWrapper<NGbCtGzDrainage>()
                   .eq(NGbCtGzDrainage::getSubjectNum,nGbContest.getSubjectNumber())
                   .orderByAsc(NGbCtGzDrainage::getId)
                   .last("limit 1")
           );
           BigDecimal drainageParam = nGbCtGzDrainage.getDrainageParam();
           //计算会员指数
           Integer memberIndex = nGbCtMemberIndex.getMemberIndex();

           //本次增加的会员指数 //会员指数 = 热度 * 商誉 * 引流参数 * 0.0001，向下取整
           //获取当前的商誉
           NGbCtCorporate nGbCtCorporate = nGbCtCorporateMapper.selectOne(new LambdaQueryWrapper<NGbCtCorporate>()
                   .eq(NGbCtCorporate::getContestId, contestId)
                   .eq(NGbCtCorporate::getStudentId, userId)
                   .orderByDesc(NGbCtCorporate::getId)
                   .last(" limit 1")
           );
           Integer sy = nGbCtCorporate.getScore();
           BigDecimal addIndex  = new BigDecimal(adAmount).multiply(new BigDecimal(sy)).multiply(drainageParam).divide(new BigDecimal(10000),0,BigDecimal.ROUND_DOWN);
           memberIndex += addIndex.intValue();
           nGbCtMemberIndex.setHeatNum(heatNum);
           nGbCtMemberIndex.setMemberIndex(memberIndex);
           nGbCtMemberIndexMapper.updateById(nGbCtMemberIndex);

           //破产
           gbActionBiz.isBankruptcy(userId,contestId,currentTime);    //重新计算报表

       }
       return R.success();
    }



    /**
     * 查看零售订单
     * @param contestId
     * @param userId
     * @param currentTime
     * @return
     */
    public R viewRetailOrder(Integer contestId, Integer userId, Integer currentTime) {
        NGbContest nGbContest = gbContestService.getById(contestId);
        Integer subjectNumber = nGbContest.getSubjectNumber();

        //获取产品名字
        List<NGbCtGzProduct> ctGzProductList = nGbCtGzProductMapper.selectList(new LambdaQueryWrapper<NGbCtGzProduct>()
                .eq(NGbCtGzProduct::getSubjectNumber,subjectNumber)
        );
        Map<String, String> productNameMap = ctGzProductList.stream().collect(Collectors.toMap(NGbCtGzProduct::getCpId, NGbCtGzProduct::getCpName, (k1, k2) -> k1));


        //获取特性名称
        List<NGbCtGzProductDesign> ctGzProductDesignList = nGbCtGzProductDesignMapper.selectList(new LambdaQueryWrapper<NGbCtGzProductDesign>()
                .eq(NGbCtGzProductDesign::getSubjectNum,subjectNumber)
        );
        Map<String, String> designNameMap = ctGzProductDesignList.stream().collect(Collectors.toMap(NGbCtGzProductDesign::getDesignNum, NGbCtGzProductDesign::getFeatureName, (k1, k2) -> k1));


        List<NGbCtRetailAssign> assignList = nGbCtRetailAssignMapper.selectList(new LambdaQueryWrapper<NGbCtRetailAssign>()
                .eq(NGbCtRetailAssign::getContestId,contestId)
                .eq(NGbCtRetailAssign::getStudentId,userId)
        );

        //获取当前比赛的所有用户 - 得到 studentId -> account 的map
        List<SysUser> userList = gbContestStudentMapper.getStudentListByContestId(contestId);
        Map<Integer, String> groupNumMap = new HashMap<>();
        userList.stream().forEach(item -> groupNumMap.put(item.getId(), item.getAccount()));

        //设置产品名称和特性名称
        assignList.forEach(item->{
            item.setCpName(productNameMap.get(item.getCpId()));
            item.setDesignName(designNameMap.get(item.getDesignNum()));
            item.setGroupNum(groupNumMap.get(userId));
        });
        return R.success().put("list",assignList);
    }

    public R getAllNetWorkOrder(NGbCtRetailOrderParam nGbCtRetailOrderParam) {
        Integer contestId = nGbCtRetailOrderParam.getContestId();
        Integer date = nGbCtRetailOrderParam.getDate();


        NGbContest nGbContest = gbContestService.getById(contestId);
        Integer subjectNumber = nGbContest.getSubjectNumber();

        //获取产品名字
        List<NGbCtGzProduct> ctGzProductList = nGbCtGzProductMapper.selectList(new LambdaQueryWrapper<NGbCtGzProduct>()
                .eq(NGbCtGzProduct::getSubjectNumber,subjectNumber)
        );
        Map<String, String> productNameMap = ctGzProductList.stream().collect(Collectors.toMap(NGbCtGzProduct::getCpId, NGbCtGzProduct::getCpName, (k1, k2) -> k1));


        //获取特性名称
        List<NGbCtGzProductDesign> ctGzProductDesignList = nGbCtGzProductDesignMapper.selectList(new LambdaQueryWrapper<NGbCtGzProductDesign>()
                .eq(NGbCtGzProductDesign::getSubjectNum,subjectNumber)
        );
        Map<String, String> designNameMap = ctGzProductDesignList.stream().collect(Collectors.toMap(NGbCtGzProductDesign::getDesignNum, NGbCtGzProductDesign::getFeatureName, (k1, k2) -> k1));


        //获取当前比赛的所有用户 - 得到 studentId -> account 的map
        List<SysUser> userList = gbContestStudentMapper.getStudentListByContestId(contestId);
        Map<Integer, String> groupNumMap = new HashMap<>();
        userList.stream().forEach(item -> groupNumMap.put(item.getId(), item.getAccount()));


        Page<NGbCtRetailAssign> page = new Page<>(nGbCtRetailOrderParam.getPage(), nGbCtRetailOrderParam.getLimit());


        LambdaQueryWrapper<NGbCtRetailAssign> lambdaQueryWrapper = new LambdaQueryWrapper<NGbCtRetailAssign>()
                .eq(NGbCtRetailAssign::getContestId,contestId);
         if(date!=null){
             Integer year =date/10;
             Integer quarterly =date%10;
             lambdaQueryWrapper
                     .eq(NGbCtRetailAssign::getYear,year)
                     .eq(NGbCtRetailAssign::getQuarterly,quarterly);
         }

        //分页查询数据
         nGbCtRetailAssignMapper.selectPage(page,lambdaQueryWrapper);


        List<NGbCtRetailAssign> list = page.getRecords();
        list.forEach(p->{
            //组号
            p.setGroupNum(groupNumMap.get(p.getStudentId()));

            //产品名称
            p.setCpName((productNameMap.get(p.getCpId())));

            //特征名称
            p.setDesignName((designNameMap.get(p.getDesignNum())));
        });

        Map<String, Object> returnMap = new HashMap<>();
        returnMap.put("total", page.getTotal());
        returnMap.put("list", page.getRecords());
        return R.success(returnMap);
    }
}
