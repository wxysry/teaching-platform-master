package com.xiyou.main.biz.ngbcontest;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.xiyou.common.enums.CodeEnum;
import com.xiyou.common.redis.utils.RedisCache;
import com.xiyou.common.utils.CommonUtil;
import com.xiyou.common.utils.R;
import com.xiyou.main.dao.ngbcontest.*;
import com.xiyou.main.entity.exam.SysUser;
import com.xiyou.main.entity.ngbcontest.*;
import com.xiyou.main.enums.ErrorEnum;
import com.xiyou.main.enums.RedisEnum;
import com.xiyou.main.schedule.ScheduleTaskService;
import com.xiyou.main.service.exam.SysUserService;
import com.xiyou.main.service.ngbcontest.NGbContestService;
import com.xiyou.main.service.ngbcontest.NGbCtMnChooseService;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;
import java.util.stream.Collectors;

/**
 * @program: multi-module
 * @description:
 * @author: tangcan
 * @create: 2019-07-27 16:36
 **/
@Service
@Slf4j
@Transactional(rollbackFor = Exception.class)
public class NGbTradeFairBizTranscation {
    private static final Integer MARKET_DIS_OPEN = 0;
    private static final Integer MARKET_CHOOSING = 1;
    private static final Integer MARKET_OVER = 2;


    @Autowired
    private NGbTradeFairBiz gbTradeFairBiz;
    @Autowired
    private NGbCtMnAdMapper gbCtMnAdMapper;
    @Autowired
    private NGbCtMnChooseMapper gbCtMnChooseMapper;
    @Autowired
    private NGbCtMnChooseService gbCtMnChooseService;
    @Autowired
    private SysUserService sysUserService;
    @Autowired
    private NGbCtGzCsMapper gbCtGzCsMapper;
    @Autowired
    private NGbCtGzMarketMapper gbCtGzMarketMapper;
    @Autowired
    private NGbCtGzProductMapper gbCtGzProductMapper;
    @Autowired
    private NGbCtXzOrderMapper gbCtXzOrderMapper;
    @Autowired
    private NGbRestoreDataBiz gbRestoreDataBiz;
    @Autowired
    private NGbCtYfIsoMapper gbCtYfIsoMapper;
    @Autowired
    private NGbContestService contestService;
    @Autowired
    private RedisCache redisCache;
    @Autowired
    private ScheduleTaskService scheduleTaskService;
    @Autowired
    private NGbContestStudentMapper gbContestStudentMapper;
    @Autowired
    private NGbCtXdOrderTempMapper gbCtXdOrderTempMapper;

    public static String[] MARKET_LIST = {"本地","区域","国内","亚洲","国际"};
    public static String[] PRODUCT_LIST = {"P1","P2","P3","P4","P5"};

//    /**
//     * 获取首单选单时间
//     * @param contestId
//     * @return
//     */
//    public int getFistChooserTime(Integer contestId){
//        NGbCtGzCs ctGzCs = gbCtGzCsMapper.getByContestId(contestId);
//        return ctGzCs.getOrderTimeout() + ctGzCs.getOrderFixTime();
//    }

//    //获取选单时间
//    public int getOtherChooserTime(Integer contestId){
//        NGbCtGzCs ctGzCs = gbCtGzCsMapper.getByContestId(contestId);
//        return ctGzCs.getOrderTimeout();
//    }

    /**
     * 初始化最开始的市场和产品
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    public void initMarketAndProduct(Integer contestId){
        NGbContest contest = contestService.getById(contestId);
        JSONArray jsonArray = new JSONArray();
        for (String market : MARKET_LIST) {
            JSONObject jObj = new JSONObject();
            jObj.put("market", market);
            jObj.put("status", MARKET_DIS_OPEN);
            jsonArray.add(jObj);
        }
        contest.setMarketList(jsonArray.toJSONString());
        contestService.updateById(contest);
    }


    /**
     * 暂停选单
     * @param contestId
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    public  R pauseChoose(Integer contestId,Integer fairOrderState){
        log.error("暂停选单开始,contestId:{},fairOrderState:{}",contestId,fairOrderState);
        NGbContest contest = contestService.getById(contestId);
        Integer sysFairOrderState = contest.getFairOrderState();
        //判断当前是否是选单状态
        if(sysFairOrderState != 3){
            return ErrorEnum.FAIR_ORDER_STATE_ERROR.getR();
        }
        //T 持久化订货会剩余选单时间 并结束调度
        String marketList = contest.getMarketList();
        JSONArray arrays = JSONArray.parseArray(marketList);
        for (Object array : arrays) {
            JSONObject jObj = (JSONObject) array;
            Integer status = Integer.parseInt(jObj.get("status").toString());
            if (status.equals(MARKET_CHOOSING)) {
                String market = jObj.get("market").toString();
                int remainTime = getRedisRemainTime(contestId,market );
                jObj.put("remainTime", remainTime);
                // 结束调度
                this.stopChooseTaskScheduling(contestId,market);
            }
        }
        contest.setMarketList(arrays.toJSONString());
        contest.setFairOrderState(fairOrderState);
        contestService.updateById(contest);
        log.error("暂停选单结束,contestId:{},marketList:{}",contest,arrays.toJSONString());
        return R.success();
    }



//    /**
//     * 继续选单
//     * @param contestId
//     * @return
//     */
//    @Transactional(rollbackFor = Exception.class)
//    public R continueChoose(Integer contestId,Integer fairOrderState){
//        log.error("继续选单开始,contestId:{},fairOrderState:{}",contestId,fairOrderState);
//        NGbContest contest = contestService.getById(contestId);
//        Integer lastFairOrderDate = contest.getLastFairOrderDate();
//        //判断当前是否是暂停状态
//        Integer sysFairOrderState = contest.getFairOrderState();
//        if(sysFairOrderState != 4){
//            return ErrorEnum.FAIR_ORDER_STATE_ERROR.getR();
//        }
//        //更新市场信息
//        String marketList = contest.getMarketList();
//        JSONArray arrays = JSONArray.parseArray(marketList);
//        for (Object array : arrays) {
//            JSONObject jObj = (JSONObject) array;
//            Integer status = Integer.parseInt(jObj.get("status").toString());
//            if (status.equals(MARKET_CHOOSING)) {
//                String market = jObj.get("market").toString();
//                int remainTime = Integer.parseInt(jObj.get("remainTime").toString());
//                int chooseStudentId = Integer.parseInt(jObj.get("chooseStudentId").toString());
//                //执行度读度
//                addChooseTaskScheduling(contestId,chooseStudentId,market,remainTime);
//            }
//        }
//        contest.setMarketList(arrays.toJSONString());
//        contest.setFairOrderState(fairOrderState);
//        contestService.updateById(contest);
//        log.error("继续选单结束,contestId:{},marketList:{}",contest,arrays.toJSONString());
//        return R.success();
//    }
//
//
//
//    /**
//     * 开始选单
//     * @param
//     */
//    public R startChoose(Integer contestId,Integer date){
//        log.error("开始选单,contestId:"+contestId+",选单时间:"+date);
//        Integer year = date/10;
//        Integer quarterly = date%10;
//        gbCtXdOrderTempMapper.delete(new LambdaQueryWrapper<NGbCtXdOrderTemp>()
//                .eq(NGbCtXdOrderTemp::getContestId,contestId)
//                .eq(NGbCtXdOrderTemp::getYear,year)
//                .eq(NGbCtXdOrderTemp::getQuarterly,quarterly)
//        );
//        //初始化和状态先更新
//        initMarketAndProduct(contestId);
//        //更新选单状态为一开始
//        NGbContest contest0 = contestService.getById(contestId);
//        contest0.setLastFairOrderDate(date);// 记录开启最近订货会的时间
//        contest0.setFairOrderState(3);//进入选单状态
//        contestService.updateById(contest0);
//
//        //初始化话选单顺序数据 gb_ct_xd_order_temp
//        for(String market:MARKET_LIST){
//            for(String product:PRODUCT_LIST){
//                Integer cmid = gbCtGzMarketMapper.getCmid(contestId, market);
//                Integer cpid = gbCtGzProductMapper.getCpid(contestId, product);
//                int round = 0; //从第一轮开始
//                while (true){
//                    round++;
//                    List<NGbCtXdOrderTemp> gbCtXdOrderTemps = this.getUserAdList(contestId, year,quarterly, round,market+product,cmid);
//                    if(gbCtXdOrderTemps!=null && gbCtXdOrderTemps.size()>0){
//                        for (NGbCtXdOrderTemp xdOrder:gbCtXdOrderTemps){
//                            //设置市场和从产品
//                            xdOrder.setMarket(market);
//                            xdOrder.setProduct(product);
//                            gbCtXdOrderTempMapper.insert(xdOrder);
//                        }
//                    }else{
//                        //当前轮次没有选单时，更换一个产品
//                        break;
//                    }
//                }
//            }
//        }
//        NGbContest contest = contestService.getById(contestId);
//        String marketList = contest.getMarketList();
//        JSONArray arrays = JSONArray.parseArray(marketList);
//        //按照顺序开启两个市场
//        for(Object array:arrays){
//            JSONObject jObj = (JSONObject) array;
//            if((MARKET_LIST[0].equals(jObj.get("market")))
//                    ||(MARKET_LIST[1].equals(jObj.get("market")))
//            ){
//                jObj.put("status", MARKET_CHOOSING);//市场正在开放
//                jObj.put("round",1);
//            }
//        }
//        contest.setMarketList(arrays.toJSONString());
//        contestService.updateById(contest);
//
//        log.error("marketList:",arrays.toJSONString());
//        toNextStudent(contestId,year,quarterly,MARKET_LIST[0]);
//        toNextStudent(contestId,year,quarterly,MARKET_LIST[1]);
//
//        return R.success();
//    }


//    /**
//     *获取市场的信息
//     * @param contestId
//     * @param market
//     * @return
//     */
//    public JSONObject getMarketInfo(Integer contestId ,String market){
//        NGbContest contest = contestService.getById(contestId);
//        String marketList = contest.getMarketList();
//        JSONArray arrays = JSONArray.parseArray(marketList);
//        for (Object array : arrays) {
//            JSONObject jObj = (JSONObject) array;
//            if (market.equals(jObj.get("market").toString())) {
//                return jObj;
//            }
//        }
//        return null;
//    }
//
//
//    /**
//     * 学生获取广告信息和选单列表
//     * @param
//     * @return
//     */
//    public R getUserAndOrder(Integer contestId,Integer studentId,String market) {
//        Map<String, Object> returnMap = new HashMap<>();
//        NGbContest contest = contestService.getById(contestId);
//        Integer date = contest.getLastFairOrderDate();
//        int year = date/10;
//        int quarterly = date%10;
//        JSONObject marketInfo = getMarketInfo(contestId,market);
//        String product  = marketInfo.get("product").toString();
//        // 查出市场编号和产品编号
//        Integer cmid = gbCtGzMarketMapper.getCmid(contestId, market);
//        Integer cpid = gbCtGzProductMapper.getCpid(contestId, product);
//        //获取用户广告
//        List<NGbCtXdOrderTemp> gbCtXdOrderTemps= gbCtXdOrderTempMapper.selectList(new LambdaQueryWrapper<NGbCtXdOrderTemp>()
//                .eq(NGbCtXdOrderTemp::getContestId,contestId)
//                .eq(NGbCtXdOrderTemp::getYear,year)
//                .eq(NGbCtXdOrderTemp::getQuarterly,quarterly)
//                .eq(NGbCtXdOrderTemp::getMarket,market) //市场
//                .eq(NGbCtXdOrderTemp::getProduct,product) //产品
//                .eq(NGbCtXdOrderTemp::getDeleteStatus,0)
//                //根据ID升序排序
//                .orderByAsc(NGbCtXdOrderTemp::getId)
//        );
//        //去重，每个学生保存一条ID最小的记录
//        Map<Integer, NGbCtXdOrderTemp> uniqueRecords = new HashMap<>();
//        for (NGbCtXdOrderTemp record : gbCtXdOrderTemps) {
//            int userId = record.getStudentId();
//            if ((!uniqueRecords.containsKey(userId)) || record.getId() < uniqueRecords.get(userId).getId()) {
//                uniqueRecords.put(userId, record);
//            }
//        }
//        List<NGbCtXdOrderTemp> userAdVOList = new ArrayList<>(uniqueRecords.values());
//        //设置是否是自己
//        for (NGbCtXdOrderTemp gbUserAdVO : userAdVOList) {
//            if(gbUserAdVO.getStudentId().equals(studentId)){
//                gbUserAdVO.setIsMe(1);
//            }else {
//                gbUserAdVO.setIsMe(0);
//            }
//        }
//        //获取选单
//        List<NGbCtMnChoose> chooseList = this.getOrder(contestId,studentId,year,
//                quarterly, cmid, cpid);
//        // 依次按产品广告额、市场广告额、销售额降序排布
//        userAdVOList.sort(Comparator.comparing(NGbCtXdOrderTemp::getId,Comparator.naturalOrder()));
//
//        //获取当前市场的剩余时间
//        int remainTime = 0;
//        //判断是否暂停,如果未暂停取redis时间
//        if(contest.getFairOrderState()==3){
//            remainTime = this.getRedisRemainTime(contestId,market);
//        }
//        //已暂停则取数据库中存储的时间
//        else if(contest.getFairOrderState()==4) {
//            JSONObject jobj = getMarketInfo(contestId, market);
//            int status = Integer.parseInt(jobj.get("status").toString());
//            if (MARKET_CHOOSING == status) {
//                remainTime = Integer.parseInt(jobj.get("remainTime") == null ? "0" : jobj.get("remainTime").toString());
//            }
//        }
//        returnMap.put("remainTime", remainTime);
//        returnMap.put("userAds", userAdVOList);
//        // 选单列表
//        returnMap.put("orders", chooseList);
//        return R.success(returnMap);
//    }
//
//
//
//    /**
//     * 获取用户广告额和选单次数和销售额
//     * @param contestId
//     * @param marketProduct
//     * @param year
//     * @param quarterly
//     * @param round
//     * @param cmid
//     * @return
//     */
//    private List<NGbCtXdOrderTemp> getUserAdList(Integer contestId,Integer year,Integer quarterly,Integer round,String marketProduct,Integer cmid) {
//        List<NGbCtMnAd> ctMnAdList = gbCtMnAdMapper.getAdListByContestId(contestId,year,quarterly);
//        // 规则表获取最小广告额
//        NGbCtGzCs ctGzCs = gbCtGzCsMapper.getByContestId(contestId);
//        int min = ctGzCs.getMin() == null ? 10000 : ctGzCs.getMin();
//        int tolerance = ctGzCs.getTolerance() == null ? 2*min : ctGzCs.getTolerance();
//        // 处理数据
//        List<NGbCtXdOrderTemp> userAdVOList = new ArrayList<>();
//        for (NGbCtMnAd ctMnAd : ctMnAdList) {
//            double ad = ctMnAd.getByName(marketProduct);
//            double sum = ctMnAd.getSumByName(marketProduct);
//            int number;
//            if(ad<min){
//                number=0;
//            }else{
//                // 次数=  roundown（(广告-最小)/得单公差）+2 - 回合数（每个市场的每个产品开始时，默认为1）
//                number = Math.max((int) ((ad-min)/ tolerance + 2- round), 0);
//            }
//            //销售额
//            if(number>0){
//                Integer sales = gbCtXzOrderMapper.getStudentYearSales(contestId,ctMnAd.getStudentId(),cmid, year - 1);
//                userAdVOList.add(
//                        new NGbCtXdOrderTemp()
//                                .setStudentId(ctMnAd.getStudentId())
//                                .setContestId(contestId)
//                                .setYear(year)
//                                .setQuarterly(quarterly)
//                                .setGroupNum(ctMnAd.getGroupNum())
//                                .setRound(round)
//                                .setProductAd((int) ad)
//                                .setMarketAd((int) sum)
//                                .setRemainNum(number)
//                                .setSales(sales == null ? 0 : sales)
//                                .setDeleteStatus(0)
//                                .setRemark("")
//                );
//            }
//        }
//        // 依次按产品广告额、市场广告额、销售额降序排布
//        userAdVOList.sort(Comparator.comparing(NGbCtXdOrderTemp::getProductAd, Comparator.reverseOrder())
//                .thenComparing(NGbCtXdOrderTemp::getMarketAd, Comparator.reverseOrder())
//                .thenComparing(NGbCtXdOrderTemp::getSales, Comparator.reverseOrder()));
//        return userAdVOList;
//    }
//

    /**
     * @Author: tangcan
     * @Description: 获取选单列表
     * @Param: [userAndOrderParam, userAdVOList]
     * @date: 2019/8/31
     */
    private List<NGbCtMnChoose> getOrder(Integer contestId,Integer studentId,Integer year,Integer quarterly,Integer cmid, Integer cpid) {
        //获取规则表内所有订单
        List<NGbCtMnChoose> ctMnChooseList = gbCtMnChooseMapper.getOrderList(contestId,cmid,cpid,year,quarterly);
        // 查出所有研发完成的iso
        List<NGbCtYfIso> ctYfIsoList = gbCtYfIsoMapper.getYfFinishList(contestId,studentId);
        // 将研发完成的编号放入set中，便于查询
        Set<Integer> isoSet = ctYfIsoList.stream().map(NGbCtYfIso::getIsoNum).collect(Collectors.toSet());
        List<NGbCtMnChoose> chooseList = new ArrayList<>(CommonUtil.getListInitCap(ctMnChooseList.size()));
        for (NGbCtMnChoose ctMnChoose : ctMnChooseList) {
            // 滤掉已经选走的订单
            if (ctMnChoose.getXzGroup()!=null) {
                continue;
            }
            // 如果这个单据ISO为空，就是可以选。
            // 如果为1，那该组ISO9000已经开发完成就可以选
            // 如果为2，那该组ISO14000已经开发完成就可以选
            // 如果为3，那需要两个都开发完成才能选
            Integer iso = ctMnChoose.getCiId();
            // 判断需要的iso认证是否研发完成
            boolean isoFinish = ((iso == null) || ((iso <= 2) && isoSet.contains(iso)) || ((iso == 3) && isoSet.contains(1) && isoSet.contains(2)));
            ctMnChoose.setChoosable(isoFinish ? 1 : 0);
            chooseList.add(ctMnChoose);
        }
        return chooseList;
    }



//    /**
//     * 选单
//     * @param
//     * @return
//     */
//    public  R  chooseOrder(Integer contestId,Integer studentId,String market,Integer chooseId) {
//        log.error("选择订单开始，contestId:{},market:{},studentId:{},chooseId:{}",contestId,market,studentId,chooseId);
//        NGbCtMnChoose ctMnChoose = gbCtMnChooseService.getById(chooseId);
//        JSONObject marketInfo = this.getMarketInfo(contestId, market);
//        NGbContest contest = contestService.getById(contestId);
//        int date = contest.getLastFairOrderDate();
//        int year = date/10;
//        int quarterly = date%10;
//        int round = Integer.parseInt(marketInfo.get("round").toString());
//        String product = marketInfo.get("product").toString();
//        String chooseStudentId = marketInfo.get("chooseStudentId").toString();
//        if(!contest.getFairOrderState().equals(3)){
//            log.error("选择订单发生异常，contestId:{},market:{},studentId:{},chooseId:{},异常原因:{}",contestId,studentId,market,chooseId,"订货未进行");
//            return R.error(CodeEnum.PARAM_ERROR.getCode(),"订货未进行");
//        }
//        if(!chooseStudentId.equals(studentId.toString())){
//            log.error("选择订单发生异常，contestId:{},market:{},studentId:{},chooseId:{},异常原因:{}",contestId,studentId,market,chooseId,"当前学生不是选单人");
//            return R.error(CodeEnum.PARAM_ERROR.getCode(),"当前学生不是选单人");
//        }
//        if (ctMnChoose == null || !ctMnChoose.getContestId().equals(contestId)) {
//            log.error("选择订单发生异常，contestId:{},market:{},studentId:{},chooseId:{},异常原因:{}",contestId,studentId,market,chooseId,"该选单不存在");
//            return R.error(CodeEnum.PARAM_ERROR.getCode(), "该选单不存在");
//        }
//        if (ctMnChoose.getXzGroup() != null) {
//            log.error("选择订单发生异常，contestId:{},market:{},studentId:{},chooseId:{},异常原因:{}",contestId,studentId,market,chooseId,"选单已经被选走");
//            return R.error(CodeEnum.PARAM_ERROR.getCode(), "选单已经被选走");
//        }
//        //获取最新的选单顺序
//        NGbCtXdOrderTemp gbCtXdOrderTemp= gbCtXdOrderTempMapper.selectOne(new LambdaQueryWrapper<NGbCtXdOrderTemp>()
//                .eq(NGbCtXdOrderTemp::getContestId,contestId)
//                .eq(NGbCtXdOrderTemp::getYear,year)
//                .eq(NGbCtXdOrderTemp::getQuarterly,quarterly)
//                .eq(NGbCtXdOrderTemp::getMarket,market) //市场
//                .eq(NGbCtXdOrderTemp::getDeleteStatus,0)
//                //根据ID升序排序
//                .orderByAsc(NGbCtXdOrderTemp::getId)
//                .last("limit 1")
//        );
//
//        //当前选单学生ID不同  //当前选单产品不同 //当前选单轮次不同 都将抛出异常
//        if((gbCtXdOrderTemp==null)
//                ||(!gbCtXdOrderTemp.getStudentId().equals(studentId))
//                || (!gbCtXdOrderTemp.getProduct().equals(product))
//                || (!gbCtXdOrderTemp.getRound().equals(round))
//        ){
//            log.error("选择订单发生异常，contestId:{},market:{},studentId:{},chooseId:{},异常原因:{}",contestId,market,studentId,chooseId,"选单异常,请重新选单");
//            return R.error(CodeEnum.PARAM_ERROR.getCode(), "选单异常,请重新选单");
//        }
//
//        //  //校验当前选择的订单的其市场/产品 正确
//        if((!gbCtXdOrderTemp.getMarket().equals(MARKET_LIST[ctMnChoose.getCmId()-1]))||
//                (!gbCtXdOrderTemp.getProduct().equals(PRODUCT_LIST[ctMnChoose.getCpId()-1]))
//        ){
//            log.error("选择订单发生异常，contestId:{},market:{},studentId:{},chooseId:{},异常原因:{}",contestId,market,studentId,chooseId,"当前选单市场/产品错误,请重新选单");
//            return R.error(CodeEnum.PARAM_ERROR.getCode(), "当前选单市场/产品错误,请重新选单");
//        }
//
//        gbCtXdOrderTemp.setDeleteStatus(1); //已删除
//        gbCtXdOrderTemp.setRemark("已选");
//        gbCtXdOrderTempMapper.updateById(gbCtXdOrderTemp);
//        // 当点击选中时，xz_order新增该条记录，模拟选单mn_choose更新选走组号，更新选走轮次=回合数
//        SysUser sysUser = sysUserService.getById(studentId);
//        ctMnChoose.setXzGroup(sysUser.getAccount()).setXzRound(round);
//        gbCtMnChooseService.updateById(ctMnChoose);
//        NGbCtXzOrder ctXzOrder = new NGbCtXzOrder()
//                .setCoId(ctMnChoose.getCoId())
//                .setStudentId(studentId)
//                .setContestId(contestId)
//                .setDate(ctMnChoose.getDate())
//                .setQuarterly(ctMnChoose.getQuarterly())
//                .setCmId(ctMnChoose.getCmId())
////                .setCpId(ctMnChoose.getCpId())
//                .setNum(ctMnChoose.getNum())
//                .setPrice(ctMnChoose.getTotalPrice())
//                .setDeliveryDate(ctMnChoose.getDeliveryDate())
//                .setPaymentDate(ctMnChoose.getPaymentDay())
//                .setIsoId(ctMnChoose.getCiId())
//                .setCommitDate(0);
//        gbCtXzOrderMapper.insert(ctXzOrder);
//        //终止当前的定时任务
//        this.stopChooseTaskScheduling(contestId,market);
//
//        log.error("选择订单完成，contestId:{},market:{},studentId:{},chooseId:{}",contestId,studentId,market,chooseId);
//        log.error("轮到下一个学生选单");
//        // 跳到下一个学生选单
//        toNextStudent(contestId,year,quarterly,market);
//        return R.success();
//    }



//    /**
//     * 判断当前订货会是否结束
//     * @param contestId
//     */
//    public  boolean checkAllMarketOver(Integer contestId){
//        NGbContest contest = contestService.getById(contestId);
//        log.error("contest",JSONObject.toJSON(contest));
//        log.error("marketList",contest.getMarketList());
//        JSONArray arrays = JSONArray.parseArray(contest.getMarketList());
//        boolean flag = true;
//        for (Object obj : arrays) {
//            JSONObject jObj = (JSONObject) obj;
//            Integer  status = Integer.parseInt(jObj.get("status").toString());
//            if(!MARKET_OVER.equals(status)){
//                flag = false;
//            }
//        }
//        if(flag){
//            //更新为未开启（关闭）
//            contest.setFairOrderState(0);
//            contestService.updateById(contest);
//            //备份学生数据，只备份参加了订货会的学生
//            List<NGbContestStudent> studentIdList = gbContestStudentMapper.selectList(new LambdaQueryWrapper<NGbContestStudent>()
//                    .eq(NGbContestStudent::getContestId,contestId)
//            );
//            //备份学生数据
//            for (NGbContestStudent student : studentIdList) {
//                log.error("gbRestoreDataBiz",JSONObject.toJSON(gbRestoreDataBiz));
//                log.error("student",JSONObject.toJSON(student));
//                log.error("student.getDate()",student.getDate());
//                gbRestoreDataBiz.backupData(contestId,student.getStudentId(),student.getDate()/10);
//            }
//            log.error("选单已结束,contestId:{},contest:",contestId,JSONObject.toJSONString(contest));
//        }
//        return flag;
//    }


//    /**
//     * 下个学生开始选单
//     * @param contestId
//     * @param year
//     * @param quarterly
//     * @throws Exception
//     */
//    public  void toNextStudent(Integer contestId,Integer year,Integer quarterly,String market){
//        //判断当前市场是否还有学生
//        //获取最新的选单顺序
//        NGbCtXdOrderTemp gbCtXdOrderTemp= gbCtXdOrderTempMapper.selectOne(new LambdaQueryWrapper<NGbCtXdOrderTemp>()
//                .eq(NGbCtXdOrderTemp::getContestId,contestId)
//                .eq(NGbCtXdOrderTemp::getYear,year)
//                .eq(NGbCtXdOrderTemp::getQuarterly,quarterly)
//                .eq(NGbCtXdOrderTemp::getMarket,market) //市场
//                .eq(NGbCtXdOrderTemp::getDeleteStatus,0)
//                //根据ID升序排序
//                .orderByAsc(NGbCtXdOrderTemp::getId)
//                .last("limit 1")
//        );
//
//        NGbContest contest = contestService.getById(contestId);
//        String marketList = contest.getMarketList();
//        JSONArray arrays = JSONArray.parseArray(marketList);
//        log.error("下一个学生开始：contestId:{},year:{},quarterly:{},market:{},marketList:{}",contestId,year,quarterly,market,arrays.toJSONString());
//        for (Object obj : arrays) {
//            JSONObject jObj = (JSONObject) obj;
//            if(market.equals(jObj.get("market").toString())){
//
//                if(gbCtXdOrderTemp!=null){
//
//                    //判断当前市场下的当前产品是否还有订单，如果没有订单，则把当前市场/产品下该学生移除
//                    Integer cmid = gbCtGzMarketMapper.getCmid(contestId, market);
//                    Integer cpid = gbCtGzProductMapper.getCpid(contestId, gbCtXdOrderTemp.getProduct());
//                    List<NGbCtMnChoose> chooseList = this.getOrder(contestId,gbCtXdOrderTemp.getStudentId(),year,
//                            quarterly, cmid, cpid);
//                    //
//                    if(chooseList!=null && chooseList.size()>0){
//                        //下一个学生 // 判断是否切换产品
//                        String newProduct = gbCtXdOrderTemp.getProduct();
//                        Integer newRound = gbCtXdOrderTemp.getRound();
//
//                        //判断是否变化产品或者市场,如果发生变化则视为首单
//                        String nowMarket = jObj.get("market")==null?"":jObj.get("market").toString();
//                        String nowProduct = jObj.get("product")==null?"":jObj.get("product").toString();
//                        int chooseTime=0;
//                        if(newProduct.equals(nowProduct)&&nowMarket.equals(market)){
//                            chooseTime = getOtherChooserTime(contestId);
//                        }else{
//                            chooseTime = getFistChooserTime(contestId);
//                        }
//                        jObj.put("market",market);
//                        jObj.put("product",newProduct);
//                        jObj.put("round",newRound);
//                        jObj.put("status",MARKET_CHOOSING); //选单中
//                        jObj.put("chooseStudentId",gbCtXdOrderTemp.getStudentId());
//                        jObj.put("chooseStudentName",gbCtXdOrderTemp.getGroupNum());
//                        //添加定时任务
//                        addChooseTaskScheduling(contestId,gbCtXdOrderTemp.getStudentId(),market,chooseTime);
//                        contest.setMarketList(arrays.toJSONString());
//                        log.error("下一个学生结束：contestId:{},year:{},quarterly:{},market:{},marketList:{}",contestId,year,quarterly,market,arrays.toJSONString());
//                        contestService.updateById(contest);
//                        return;
//                    }
//                    else {
//                        //将当前的市场和产品的无效
//                        gbCtXdOrderTempMapper.update(new NGbCtXdOrderTemp().setDeleteStatus(1).setRemark("无单可选")
//                                ,new LambdaQueryWrapper<NGbCtXdOrderTemp>()
//                                        .eq(NGbCtXdOrderTemp::getContestId,contestId)
//                                        .eq(NGbCtXdOrderTemp::getYear,year)
//                                        .eq(NGbCtXdOrderTemp::getQuarterly,quarterly)
//                                        .eq(NGbCtXdOrderTemp::getMarket,market) //市场
//                                        .eq(NGbCtXdOrderTemp::getProduct,gbCtXdOrderTemp.getProduct()) //市场
//                                        .eq(NGbCtXdOrderTemp::getDeleteStatus,0)
//                        );
//                        //跳转至下一个学生
//                        toNextStudent(contestId,year,quarterly,market);
//                    }
//                }
//                else{
//                    //关闭当前市场
//                    jObj.put("status",MARKET_OVER);
//                    contest.setMarketList(arrays.toJSONString());
//                    contestService.updateById(contest);
//
//                    //开启下一个市场
//                    toNextMarket(contestId,year,quarterly,market);
//                }
//            }
//        }
//    }

//    /**
//     * 下个市场
//     * @param contestId
//     * @param year
//     * @param quarterly
//     */
//    public  void toNextMarket(Integer contestId,Integer year,Integer quarterly,String market){
//        //判断是否所有市场都结束了 结束订货会
//        if(checkAllMarketOver(contestId)){
//            return;
//        }
//        NGbContest contest = contestService.getById(contestId);
//        String marketList = contest.getMarketList();
//        JSONArray arrays = JSONArray.parseArray(marketList);
//        for (int i = 0; i < arrays.size(); i++) {
//            JSONObject jObj = (JSONObject) arrays.get(i);
//            if(market.equals(jObj.get("market"))){
//                //判断是否是最后一个市场
//                if(i == arrays.size()-1){
//                    return;
//                }else {
//                    //如果不是最后一个市场,则开启下一个未开启的市场
//                    JSONObject nextJObj = (JSONObject)arrays.get(i+1);
//                    int nextStatus = Integer.parseInt(nextJObj.get("status").toString());
//                    String nextMarket = nextJObj.get("market").toString();
//                    //如果当前市场处于未开启状态则开启该市场,否则递归开启下一个市场
//                    if(nextStatus == MARKET_DIS_OPEN){
//                        //开启该市场
//                        //去获取下一个学生
//                        toNextStudent(contestId,year,quarterly,nextMarket);
//                    } else {
//                        //当前市场为开启，递归开启下个市场
//                        toNextMarket(contestId,year,quarterly,nextMarket);
//                    }
//                }
//
//            }
//        }
//    }


//    /**
//     * 放弃选单
//     * @param contestId
//     * @param studentId
//     * @param market
//     * @param isTimeOut
//     * @return
//     */
//    public  R  giveUpOrder(Integer contestId,Integer studentId,String market,boolean isTimeOut) {
//        log.error("contestId:"+contestId+",market:"+market+" ,studentId:"+studentId+",放弃选单,是否超时"+isTimeOut+",开始");
//        NGbContest contest = contestService.getById(contestId);
//        Integer date = contest.getLastFairOrderDate();
//        //先获取当前市场最新的选单
//        NGbCtXdOrderTemp gbCtXdOrderTemp= gbCtXdOrderTempMapper.selectOne(new LambdaQueryWrapper<NGbCtXdOrderTemp>()
//                .eq(NGbCtXdOrderTemp::getContestId,contestId)
//                .eq(NGbCtXdOrderTemp::getYear,date/10)
//                .eq(NGbCtXdOrderTemp::getQuarterly,date%10)
//                .eq(NGbCtXdOrderTemp::getMarket,market) //市场
//                .eq(NGbCtXdOrderTemp::getDeleteStatus,0)
//                //根据ID升序排序
//                .orderByAsc(NGbCtXdOrderTemp::getId)
//                .last("limit 1")
//        );
//        if(gbCtXdOrderTemp==null||(!studentId.equals(gbCtXdOrderTemp.getStudentId()))){
//            log.error("放弃选单发生异常");
//            return R.error(CodeEnum.PARAM_ERROR.getCode(), "放弃选单异常");
//        }
//        //将当前学生在当前产品的所有记录无效
//        gbCtXdOrderTempMapper.update(new NGbCtXdOrderTemp().setDeleteStatus(1).setRemark("放弃选单")
//                ,new LambdaQueryWrapper<NGbCtXdOrderTemp>()
//                .eq(NGbCtXdOrderTemp::getContestId,contestId)
//                .eq(NGbCtXdOrderTemp::getYear,date/10)
//                .eq(NGbCtXdOrderTemp::getQuarterly,date%10)
//                .eq(NGbCtXdOrderTemp::getMarket,market) //市场
//                .eq(NGbCtXdOrderTemp::getProduct,gbCtXdOrderTemp.getProduct()) //市场
//                .eq(NGbCtXdOrderTemp::getStudentId,gbCtXdOrderTemp.getStudentId()) //学生
//                .eq(NGbCtXdOrderTemp::getDeleteStatus,0)
//        );
//        log.error("contestId:"+contestId+",market:"+market+" ,studentId:"+studentId+",放弃选单,是否超时"+isTimeOut+",完成");
//
//        // 主动放弃则需要删除定时任务，时间到了则无需删除，直接进行到下一个人，
//        if(!isTimeOut){
//            log.error("放弃选单,移除定时任务");
//            this.stopChooseTaskScheduling(contestId,market);
//        }
//        // 跳到下一个学生选单
//        log.error("放弃选单,跳转下一个学生");
//        toNextStudent(contestId,date/10,date%10, market);
//        return R.success();
//    }





//    /**
//     * 还原订单
//     * @param contestId
//     * @param date
//     * @return
//     */
//    public R restoreOrder(Integer contestId, Integer date) {
//
//        //1.直接删除因本次订货会 产生的gt_ct_xz_order
//        gbCtXzOrderMapper.removeNowYear(contestId, date / 10,date%10);
//
//        //2.重置当季的选单信息
//        gbRestoreDataBiz.resetOrderChoose(contestId,date / 10,date%10);
//
//        //3.移出定时
//        NGbContest contest = contestService.getById(contestId);
//        String marketList = contest.getMarketList();
//        JSONArray jsonArray = JSONObject.parseArray(marketList);
//        for (Object obj : jsonArray) {
//            JSONObject jObj = (JSONObject)obj;
//            int status = (Integer)jObj.get("status");
//            if(MARKET_CHOOSING.equals(status)){
//                this.stopChooseTaskScheduling(contestId,jObj.get("market").toString());
//            }
//        }
//        //4.重新开始选单
//        this.startChoose(contestId,contest.getLastFairOrderDate());
//        return R.success();
//    }




    /**
     * 开启定时任务，redis倒计时
     * @param contestId
     * @param studentId
     */
    public void addChooseTaskScheduling(Integer contestId,Integer studentId,String market,int addTime){
        scheduleTaskService.add(RedisEnum.XDDJS + ":"+ contestId + market,new Runnable() {
            @SneakyThrows
            @Override
            public void run() {
                //时间到了，则放弃选单
//                gbTradeFairBiz.giveUpOrder( contestId, studentId, market,true);
            }
        },addTime);
    }

    /**
     * 关闭选单定时任务
     */
    public void stopChooseTaskScheduling (Integer contestId,String market){
        scheduleTaskService.stop(RedisEnum.XDDJS+":"+contestId + market);
    }

    /**
     * 获取时间
     */
    public int getRedisRemainTime (Integer contestId,String market){
        return (int)redisCache.getExpire(RedisEnum.XDDJS+":"+contestId + market);
    }



}
