package com.xiyou.main.biz.ngbcontest;

import com.xiyou.common.utils.R;
import com.xiyou.main.vo.ngbcontest.NGbCashFolowEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * @program: multi-module
 * @description:
 * @author: tangcan
 * @create: 2019-07-22 16:08
 **/
@Service
@Transactional(rollbackFor = Exception.class)
public class NGbYearEndBiz {


    @Autowired
    private NGbActionBiz gbActionBiz;



    /**
     * 确认当年结束
     */
    public R confirm(Integer studentId, Integer contestId, Integer date) {

        //阻止暂停状态下 和 破产状态下的用户进行操作
        gbActionBiz.preventAction(contestId,studentId);
        return gbActionBiz.endingSeason(studentId,contestId,date);
    }


    /**
     * 当年结束后->继续下个季度
     * @param cashFolowEntity
     * @return
     */
    public R currentQuarterNext(NGbCashFolowEntity cashFolowEntity) {

        //阻止暂停状态下 和 破产状态下的用户进行操作
        gbActionBiz.preventAction(cashFolowEntity.getContestId(),cashFolowEntity.getStudentId());

        Integer studentId = cashFolowEntity.getStudentId();
        Integer contestId = cashFolowEntity.getContestId();
        Integer currentTime = cashFolowEntity.getCurrentTime();
        gbActionBiz.nextSeasonAction(studentId,contestId,currentTime);
        return R.success();
    }
}
