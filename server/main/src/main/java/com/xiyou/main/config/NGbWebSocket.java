package com.xiyou.main.config;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import javax.websocket.*;
import javax.websocket.server.PathParam;
import javax.websocket.server.ServerEndpoint;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArraySet;

@Slf4j
@Component
@ServerEndpoint("/ngbwebsocket/{contestId}/{userId}")  // 接口路径 ws://localhost:9999/webSocket/contestId;
public class NGbWebSocket {

    //与某个客户端的连接会话，需要通过它来给客户端发送数据
    private Session session;

    /**
     * 考试Id
     */
    private Integer contestId;
    /**
     * 用户ID
     */
    private Integer userId;




    //concurrent包的线程安全Set，用来存放每个客户端对应的MyWebSocket对象。
    //虽然@Component默认是单例模式的，但springboot还是会为每个websocket连接初始化一个bean，所以可以用一个静态set保存起来。
    //  注：底下WebSocket是当前类名
    private static CopyOnWriteArraySet<NGbWebSocket> NGbWebSockets =new CopyOnWriteArraySet<>();

    //先根据contestId 进行分组,然后再根据学生userId进行分组,由于一个学生账户同一时间可能存在多人登录的情况,所以用list进行存储
    private static ConcurrentHashMap<Integer,ConcurrentHashMap<Integer, List<Session>>> sessionPool = new ConcurrentHashMap<>();


    /**
     * 链接成功调用的方法
     */
    @OnOpen
    public void onOpen(Session session, @PathParam(value="contestId")Integer contestId,@PathParam(value="userId")Integer userId) {
        try {
            this.contestId = contestId;
            this.userId = userId;
            this.session = session;
            NGbWebSockets.add(this);

            //先尝试从 seesionPool中获取
            ConcurrentHashMap<Integer, List<Session>> concurrentHashMap = sessionPool.get(contestId);
            //为空则重新创建一个
            if(concurrentHashMap==null){
                concurrentHashMap = new ConcurrentHashMap<Integer, List<Session>>();
                sessionPool.put(contestId,concurrentHashMap);
                //把当前的session 塞进去
                List<Session> list = new LinkedList();
                list.add(session);
                concurrentHashMap.put(userId,list);
            }else{
                List<Session> list = concurrentHashMap.get(userId);
                //当前用户无人登录，则自己创建一个list,塞到map里面去
                if(list==null){
                    list = new LinkedList<>();
                    list.add(session);
                    concurrentHashMap.put(userId,list);
                }else{
                    list.add(session);
                }
            }

            log.info("【websocket消息】有新的连接，总数为:"+ NGbWebSockets.size());
            System.out.println("【websocket消息】有新的连接，总数为:"+ NGbWebSockets.size());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 链接关闭调用的方法
     */
    @OnClose
    public void onClose() {
        try {
            NGbWebSockets.remove(this);
            //删除sessionPool中的内容
            ConcurrentHashMap<Integer, List<Session>> concurrentHashMap = sessionPool.get(this.contestId);
            if(concurrentHashMap!=null){
               List<Session> list = concurrentHashMap.get(this.userId);
               if(list!=null){
                   list.remove(this);
               }
            }
            log.info("【websocket消息】连接断开，总数为:"+ NGbWebSockets.size());
            System.out.println("【websocket消息】连接断开，总数为:"+ NGbWebSockets.size());
        } catch (Exception e) {
        }
    }
    /**
     * 收到客户端消息后调用的方法
     *
     * @param message
     */
    @OnMessage
    public void onMessage(String message) {
        log.info("【websocket消息】收到客户端消息:"+message);
    }

    /** 发送错误时的处理
     * @param session
     * @param error
     */
    @OnError
    public void onError(Session session, Throwable error) {

        log.error("用户错误,原因:"+error.getMessage());
        error.printStackTrace();
    }


    // 此为广播消息
    public void sendAllMessage(String message) {
        log.info("【websocket消息】广播消息:"+message);
        for(NGbWebSocket NGbWebSocket : NGbWebSockets) {
            try {
                if(NGbWebSocket.session.isOpen()) {
                    NGbWebSocket.session.getAsyncRemote().sendText(message);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }


    /**
     * 给本次竞赛所有的学生发送消息
     */
    public void sendMessageToAll(Integer contestId, String message) {
        ConcurrentHashMap<Integer, List<Session>> concurrentHashMap = sessionPool.get(contestId);
        if (concurrentHashMap != null && concurrentHashMap.size()>0) {
            concurrentHashMap.forEach((k, v) -> {
                if (v != null) {
                    v.forEach(item -> {
                        try {
                            if (item.isOpen()) {
                                item.getAsyncRemote().sendText(message);
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    });
                }
            });
        }
    }


    /**
     * 给本次竞赛的某个学生发送消息
     */
    public void sendMessageToUser(Integer contestId, Integer userId, String message) {
        ConcurrentHashMap<Integer, List<Session>> concurrentHashMap = sessionPool.get(contestId);
        if (concurrentHashMap != null && concurrentHashMap.size()>0) {
            List<Session> sessionList = concurrentHashMap.get(userId);
            if(sessionList!=null && sessionList.size()>0){
                sessionList.forEach(item -> {
                    try {
                        if (item.isOpen()) {
                            item.getAsyncRemote().sendText(message);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                });
            }
        }
    }



}
