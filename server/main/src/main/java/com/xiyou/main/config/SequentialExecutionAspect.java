package com.xiyou.main.config;

import com.xiyou.common.shiro.utils.ShiroUtil;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.RedisConnection;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;

@Aspect
@Configuration
public class SequentialExecutionAspect {

    private final RedisTemplate<String, String> redisTemplate;

    public SequentialExecutionAspect(RedisTemplate<String, String> redisTemplate) {
        this.redisTemplate = redisTemplate;
    }

    @Around("@annotation(com.xiyou.main.config.Sequential)")
    public Object sequentialExecution(ProceedingJoinPoint joinPoint) throws Throwable {
        MethodSignature signature = (MethodSignature) joinPoint.getSignature();
        Sequential sequential = signature.getMethod().getAnnotation(Sequential.class);
        String type = sequential.type();
        Integer accountId = ShiroUtil.getUserId();
        String lockKey = type+":" + accountId;

        // 加锁
        RedisConnectionFactory redisConnectionFactory = redisTemplate.getConnectionFactory();
        RedisConnection connection = redisConnectionFactory.getConnection();
        boolean lockAcquired = false;
        try {
            while (!lockAcquired) {
                lockAcquired = connection.setNX(lockKey.getBytes(), "locked".getBytes());
                if (lockAcquired) {
                    connection.expire(lockKey.getBytes(), 60);
                } else {
                    Thread.sleep(100); // 可根据需求调整等待时间
                }
            }
            // 执行方法体
            return joinPoint.proceed();
        } finally {
            // 解锁
            if (lockAcquired) {
                connection.del(lockKey.getBytes());
            }
            connection.close();
        }
    }
}