package com.xiyou.main.constants;

/**
 * @Author: tangcan
 * @Description: 角色常量
 * @Param:
 * @date: 2019/6/25
 */
public interface RoleConstant {
    int ADMIN_ROLE_ID = 1;
    int TEACHER_ROLE_ID = 2;
    int STUDENT_ROLE_ID = 3;
    String ADMIN = "admin";
    String TEACHER = "teacher";
    String STUDENT = "student";
}
