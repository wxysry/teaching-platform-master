package com.xiyou.main.controller.contest;

import com.xiyou.common.controller.BaseController;
import com.xiyou.common.utils.R;
import com.xiyou.main.biz.contest.ActionBiz;
import com.xiyou.main.constants.RoleConstant;
import com.xiyou.main.params.contest.*;
import com.xiyou.main.vo.contest.MarketVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.shiro.authz.annotation.RequiresRoles;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotNull;

/**
 * @author dingyoumeng
 * @since 2019/07/25
 */
@RestController
@RequestMapping("/tp/contest")
@Api(tags = "p19-p29")
@Validated
public class ActionController extends BaseController {

    @Autowired
    ActionBiz actionBiz;

    @ResponseBody
    @GetMapping("/receivables/check")
    @ApiOperation(value = "应收款项-p19")
    @RequiresRoles(RoleConstant.STUDENT)
    public R receivables(@RequestParam @NotNull(message = "contestId不能为空") Integer contestId) {
        return actionBiz.checkReceivables(getUserId(), contestId);
    }


    @ResponseBody
    @PostMapping("/receivables/confirm")
    @ApiOperation(value = "应收款更新确认-p19")
    @RequiresRoles(RoleConstant.STUDENT)
    public R receivablesConfirm(@RequestBody @Validated OnlineConfirm onlineConfirm) {
        return actionBiz.confirmReceivables(getUserId(), onlineConfirm);
    }


    @ResponseBody
    @GetMapping("/order/list")
    @ApiOperation(value = "交货订单-p20")
    @RequiresRoles(RoleConstant.STUDENT)
    public R listOrder(@RequestParam @NotNull(message = "contestId不能为空") Integer contestId,
                       @RequestParam @NotNull(message = "date不能为空") Integer date) {
        return actionBiz.listOrder(getUserId(), contestId, date);
    }


    @ResponseBody
    @GetMapping("/order/delivery")
    @ApiOperation(value = "交货订单-交货-p20")
    @RequiresRoles(RoleConstant.STUDENT)
    public R deliveryOrder(@RequestParam @NotNull(message = "contestId不能为空") Integer contestId,
                           @RequestParam @NotNull(message = "orderId不能为空") Integer orderId,
                           @RequestParam @NotNull(message = "date不能为空") Integer date) {
        return actionBiz.deliveryOrder(getUserId(), contestId, orderId, date);
    }

    @ResponseBody
    @GetMapping("/workshop/sellList")
    @ApiOperation(value = "厂房处理-卖出(买转租)-列表-p21")
    @RequiresRoles(RoleConstant.STUDENT)
    public R workshopSellList(@RequestParam @NotNull(message = "contestId不能为空") Integer contestId) {
        return actionBiz.workshopSellList(getUserId(), contestId);
    }

    @ResponseBody
    @GetMapping("/workshop/sell")
    @ApiOperation(value = "厂房处理-卖出(买转租)-确认-p21")
    @RequiresRoles(RoleConstant.STUDENT)
    public R workshopSell(@RequestParam @NotNull(message = "contestId不能为空") Integer contestId,
                          @RequestParam @NotNull(message = "workshopId不能为空") Integer workshopId,
                          @RequestParam @NotNull(message = "date不能为空") Integer date) {
        return actionBiz.workshopSell(getUserId(), contestId, workshopId, date);
    }

    @ResponseBody
    @GetMapping("/workshop/surrenderList")
    @ApiOperation(value = "厂房处理-退租-列表-p21")
    @RequiresRoles(RoleConstant.STUDENT)
    public R workshopSurrenderList(@RequestParam @NotNull(message = "contestId不能为空") Integer contestId) {
        return actionBiz.workshopSurrenderList(getUserId(), contestId);
    }

    @ResponseBody
    @GetMapping("/workshop/surrender")
    @ApiOperation(value = "厂房处理-退租-确认-p21")
    @RequiresRoles(RoleConstant.STUDENT)
    public R workshopSurrender(@RequestParam @NotNull(message = "workshopId不能为空") Integer workshopId) {
        return actionBiz.surrender(workshopId);
    }

    @ResponseBody
    @GetMapping("/workshop/zzmList")
    @ApiOperation(value = "厂房处理-租转买-列表-p21")
    @RequiresRoles(RoleConstant.STUDENT)
    public R zzmList(@RequestParam @NotNull(message = "contestId不能为空") Integer contestId,
                     @RequestParam @NotNull(message = "date不能为空") Integer date) {
        return actionBiz.zzmList(getUserId(), contestId, date);
    }

    @ResponseBody
    @GetMapping("/workshop/zzm")
    @ApiOperation(value = "厂房处理-租转买-确定-p21")
    @RequiresRoles(RoleConstant.STUDENT)
    public R zzm(@RequestParam @NotNull(message = "contestId不能为空") Integer contestId,
                 @RequestParam @NotNull(message = "workshopId不能为空") Integer workshopId,
                 @RequestParam @NotNull(message = "date不能为空") Integer date) {
        return actionBiz.zzm(getUserId(), contestId, workshopId, date);
    }


    @ResponseBody
    @GetMapping("/product/yfList")
    @ApiOperation(value = "产品研发-列表-p22")
    @RequiresRoles(RoleConstant.STUDENT)
    public R yfProductList(@RequestParam @NotNull(message = "contestId不能为空") Integer contestId) {
        return actionBiz.yfProductList(getUserId(), contestId);
    }

    @ResponseBody
    @PostMapping("/product/yf")
    @ApiOperation(value = "产品研发-确认-p22")
    @RequiresRoles(RoleConstant.STUDENT)
    public R yfProduct(@RequestBody @Validated YfParam yfParam) {
        return actionBiz.yfProduct(getUserId(), yfParam);
    }

    @ResponseBody
    @GetMapping("/endingSeason")
    @ApiOperation(value = "当季结束 -p23")
    @RequiresRoles(RoleConstant.STUDENT)
    public R yfProduct(@RequestParam @NotNull(message = "contestId不能为空") Integer contestId,
                       @RequestParam @NotNull(message = "date不能为空") Integer date) {
        return actionBiz.endingSeason(getUserId(), contestId, date);
    }

    @ResponseBody
    @GetMapping("/show/fee")
    @ApiOperation(value = "显示贴现- p24")
    @RequiresRoles(RoleConstant.STUDENT)
    public R showFee(@RequestParam @NotNull(message = "contestId不能为空") Integer contestId) {
        return actionBiz.showFee(getUserId(), contestId);
    }

    @ResponseBody
    @PostMapping("/discount")
    @ApiOperation(value = "显示贴现 - 确认- p24")
    @RequiresRoles(RoleConstant.STUDENT)
    public R discount(@RequestBody @Validated DiscountParam discountParam) {
        return actionBiz.discount(discountParam, getUserId());
    }


    @ResponseBody
    @GetMapping("/list/kc")
    @ApiOperation(value = "紧急采购 - 列表 - p25")
    @RequiresRoles(RoleConstant.STUDENT)
    public R listKc(@RequestParam @NotNull(message = "contestId不能为空") Integer contestId) {
        return actionBiz.listKc(contestId, getUserId());
    }

    @ResponseBody
    @PostMapping("/urgent/material")
    @ApiOperation(value = "紧急采购 - 材料采购 - p25")
    @RequiresRoles(RoleConstant.STUDENT)
    public R materialUrgentBuy(@RequestBody @Validated UrgentBuyParam param) {
        return actionBiz.buyMaterial(getUserId(), param);
    }

    @ResponseBody
    @PostMapping("/urgent/product")
    @ApiOperation(value = "紧急采购 - 产成品采购 - p25")
    @RequiresRoles(RoleConstant.STUDENT)
    public R productUrgentBuy(@RequestBody @Validated UrgentBuyParam param) {
        return actionBiz.buyProduct(getUserId(), param);
    }

    @ResponseBody
    @GetMapping("/list/sell/kc")
    @ApiOperation(value = "出售库存 - 列表 - p26")
    @RequiresRoles(RoleConstant.STUDENT)
    public R listSellKc(@RequestParam @NotNull(message = "contestId不能为空") Integer contestId) {
        return actionBiz.listSellKc(contestId, getUserId());
    }

    @ResponseBody
    @PostMapping("/sell/material")
    @ApiOperation(value = "出售库存 - 出售材料 - p26")
    @RequiresRoles(RoleConstant.STUDENT)
    public R sellMaterial(@RequestBody @Validated UrgentBuyParam param) {
        return actionBiz.sellMaterial(getUserId(), param);
    }

    @ResponseBody
    @PostMapping("/sell/product")
    @ApiOperation(value = "出售库存 - 出售产品 - p26")
    @RequiresRoles(RoleConstant.STUDENT)
    public R sellProduct(@RequestBody @Validated UrgentBuyParam param) {
        return actionBiz.sellProduct(getUserId(), param);
    }

    @ResponseBody
    @GetMapping("/workshop/discount")
    @ApiOperation(value = "厂房贴现 - p27")
    @RequiresRoles(RoleConstant.STUDENT)
    public R workshopDiscount(@RequestParam @NotNull(message = "contestId不能为空") Integer contestId) {
        return actionBiz.listWorkshopDiscount(getUserId(), contestId);
    }


    @ResponseBody
    @PostMapping("/workshop/discount/confirm")
    @ApiOperation(value = "厂房贴现- 确认 - p27")
    @RequiresRoles(RoleConstant.STUDENT)
    public R workshopDiscountConfirm(@RequestBody WsDiscountParam param) {
        return actionBiz.workshopDiscount(getUserId(), param.getContestId(), param.getWorkshopIds(), param.getDate());
    }

    @ResponseBody
    @GetMapping("/xzorder")
    @ApiOperation(value = "订单信息- p28")
    @RequiresRoles(RoleConstant.STUDENT)
    public R xzorder(@RequestParam @NotNull(message = "contestId不能为空") Integer contestId,
                     @RequestParam @NotNull(message = "当前时间") Integer date) {
        return actionBiz.xzorder(getUserId(), contestId, date);
    }

    @ResponseBody
    @GetMapping("/market/list")
    @ApiOperation(value = "市场开拓 - 显示 - p29")
    @RequiresRoles(RoleConstant.STUDENT)
    public R marketList(@RequestParam @NotNull(message = "contestId不能为空") Integer contestId) {
        return actionBiz.marketList(getUserId(), contestId);
    }

    @ResponseBody
    @PostMapping("/market/yf")
    @ApiOperation(value = "市场开拓 - 确认 - p29")
    @RequiresRoles(RoleConstant.STUDENT)
    public R marketYf(@RequestBody @Validated MarketYfParam param) {
        return actionBiz.marketYf(getUserId(), param);
    }

}
