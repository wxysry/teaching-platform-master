package com.xiyou.main.controller.contest;


import com.xiyou.common.controller.BaseController;
import com.xiyou.common.utils.R;
import com.xiyou.main.biz.exam.ContestGroupBiz;
import com.xiyou.main.constants.RoleConstant;
import com.xiyou.main.entity.contest.ContestGroup;
import com.xiyou.main.groups.Add;
import com.xiyou.main.groups.Update;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.apache.shiro.authz.annotation.RequiresRoles;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotNull;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author tangcan
 * @since 2019-08-25
 */
@RestController
@RequestMapping("/tp/contestGroup")
@Api(tags = "竞赛模拟-竞赛模拟分组")
@Validated
public class ContestGroupController extends BaseController {
    @Autowired
    private ContestGroupBiz contestGroupBiz;

    @ResponseBody
    @GetMapping("/list")
    @ApiOperation(value = "管理员获取所有竞赛模拟分组的列表")
    @RequiresRoles(RoleConstant.ADMIN)
    public R list() {
        return contestGroupBiz.list();
    }

    @ResponseBody
    @GetMapping("/listByteacher")
    @ApiOperation(value = "教师获取竞赛模拟分组的列表")
    @RequiresRoles(RoleConstant.TEACHER)
    public R listByteacher() {
        return contestGroupBiz.listByteacher(getUserId());
    }

    @ResponseBody
    @PostMapping("/add")
    @ApiOperation(value = "添加分组")
    @RequiresRoles(RoleConstant.ADMIN)
    public R add(@RequestBody @Validated({Add.class}) @ApiParam(value = "分组信息", required = true) ContestGroup contestGroup) {
        return contestGroupBiz.add(contestGroup);
    }

    @ResponseBody
    @PostMapping("/update")
    @ApiOperation(value = "更新分组信息")
    @RequiresRoles(RoleConstant.ADMIN)
    public R update(@RequestBody @Validated({Update.class}) @ApiParam(value = "分组信息", required = true) ContestGroup contestGroup) {
        return contestGroupBiz.update(contestGroup);
    }

    @ResponseBody
    @GetMapping("/delete/{id}")
    @ApiOperation(value = "删除竞赛模拟分组", notes = "会删去分组下的视频教程(传多个id时，为批量删除，中间逗号隔开)")
    @RequiresRoles(RoleConstant.ADMIN)
    public R delete(@PathVariable(value = "id") @NotNull @ApiParam(value = "ids", required = true) Integer[] ids) {
        return contestGroupBiz.delete(ids);
    }

}

