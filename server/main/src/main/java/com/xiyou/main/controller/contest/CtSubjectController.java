package com.xiyou.main.controller.contest;


import com.xiyou.common.controller.BaseController;
import com.xiyou.common.log.annotations.OptLog;
import com.xiyou.common.utils.R;
import com.xiyou.main.biz.contest.CtSubjectBiz;
import com.xiyou.main.constants.RoleConstant;
import com.xiyou.main.entity.contest.ContestGroup;
import com.xiyou.main.entity.contest.CtSubject;
import com.xiyou.main.groups.Add;
import com.xiyou.main.params.contest.SubjectParam;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.apache.shiro.authz.annotation.RequiresRoles;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotNull;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author tangcan
 * @since 2019-08-25
 */
@RestController
@RequestMapping("/tp/ctSubject")
@Api(tags = "竞赛模拟-题库管理")
@Validated
public class CtSubjectController extends BaseController {
    @Autowired
    private CtSubjectBiz ctSubjectBiz;

    @ResponseBody
    @PostMapping("/list/admin")
    @ApiOperation(value = "管理员获取题库列表")
    @RequiresRoles(RoleConstant.ADMIN)
    public R listByAdmin(@RequestBody @Validated @ApiParam(value = "题库查询参数", required = true) SubjectParam subjectParam) {
        return ctSubjectBiz.listByAdmin(subjectParam);
    }

    @ResponseBody
    @GetMapping("/cascade/teacher")
    @ApiOperation(value = "教师获取题库级联")
    @RequiresRoles(RoleConstant.TEACHER)
    public R cascadeByTeacher() {
        return ctSubjectBiz.cascadeByTeacher(getUserId());
    }

    @ResponseBody
    @GetMapping("/list/teacher")
    @ApiOperation(value = "教师获取题库列表")
    @RequiresRoles(RoleConstant.TEACHER)
    public R listByTeacher(@RequestParam @ApiParam(value = "分组id", required = true) Integer groupId) {
        return ctSubjectBiz.listByTeacher(groupId);
    }

    @ResponseBody
    @GetMapping("/get")
    @ApiOperation(value = "获取题库基本信息")
    @RequiresRoles(RoleConstant.ADMIN)
    public R get(@RequestParam @NotNull Integer subjectId) {
        return ctSubjectBiz.get(subjectId);
    }

    @ResponseBody
    @PostMapping("/add")
    @ApiOperation(value = "添加题库")
    @RequiresRoles(RoleConstant.ADMIN)
    @OptLog(description = "添加竞赛模拟题库")
    public R add(@RequestBody @Validated @ApiParam(value = "题库信息", required = true) CtSubject ctSubject) {
        return ctSubjectBiz.add(ctSubject);
    }

    @ResponseBody
    @PostMapping("/update")
    @ApiOperation(value = "更新题库基本信息")
    @RequiresRoles(RoleConstant.ADMIN)
    public R update(@RequestBody @Validated @ApiParam(value = "题库信息", required = true) CtSubject ctSubject) {
        return ctSubjectBiz.update(ctSubject);
    }

    @ResponseBody
    @GetMapping("/delete")
    @ApiOperation(value = "删除题库")
    @RequiresRoles(RoleConstant.ADMIN)
    @OptLog(description = "删除竞赛模拟题库")
    public R delete(@RequestParam @NotNull @ApiParam(value = "题库id", required = true) Integer subjectId) {
        return ctSubjectBiz.delete(subjectId);
    }
}

