package com.xiyou.main.controller.contest;

import com.xiyou.common.controller.BaseController;
import com.xiyou.common.utils.R;
import com.xiyou.main.biz.contest.DeliverAdsBiz;
import com.xiyou.main.constants.RoleConstant;
import com.xiyou.main.entity.contest.CtMnAd;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.apache.shiro.authz.annotation.RequiresRoles;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotNull;

/**
 * @program: multi-module
 * @description: 投放广告
 * @author: tangcan
 * @create: 2019-07-25 15:01
 **/
@RestController
@RequestMapping("/tp/deliverAds")
@Api(tags = "p37:投放广告")
@Validated
public class DeliverAdsController extends BaseController {
    @Autowired
    private DeliverAdsBiz deliverAdsBiz;

    @ResponseBody
    @GetMapping("/market/yf/finish")
    @ApiOperation(value = "已开发完成的市场列表")
    @RequiresRoles(RoleConstant.STUDENT)
    public R finishYfMarket(@RequestParam @NotNull @ApiParam(value = "考试id", required = true) Integer contestId) {
        return deliverAdsBiz.finishYfMarket(getUserId(), contestId);
    }

    @ResponseBody
    @PostMapping("/mnads/deliver")
    @ApiOperation(value = "投放广告")
    @RequiresRoles(RoleConstant.STUDENT)
    public R deliverAds(@RequestBody @Validated @ApiParam(value = "模拟广告", required = true) CtMnAd mnAd) {
        return deliverAdsBiz.deliverAds(getUserId(), mnAd);
    }

}
