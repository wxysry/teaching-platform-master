package com.xiyou.main.controller.contest;

import com.xiyou.common.controller.BaseController;
import com.xiyou.main.biz.contest.DownloadBiz;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.NotNull;

/**
 * @program: multi-module
 * @description: 文件下载
 * @author: tangcan
 * @create: 2019-08-28 14:21
 **/
@RestController
@RequestMapping("/tp/contest/download")
@Api(tags = "竞赛模拟-下载")
@Validated
public class DownloadController extends BaseController {
    @Autowired
    private DownloadBiz downloadBiz;

    @ResponseBody
    @GetMapping("/ad")
    @ApiOperation(value = "下载其他组广告")
    public void ad(HttpServletResponse response,
                   @RequestParam @NotNull @ApiParam(value = "考试id") Integer contestId,
                   @RequestParam @NotNull @ApiParam(value = "学生id") Integer userId,
                   @RequestParam @NotNull @ApiParam(value = "当前时间") Integer date) {
        downloadBiz.ad(response, userId, contestId, date);
    }

    @ResponseBody
    @GetMapping("/result/download")
    @ApiOperation(value = "下载学生竞赛模拟结果")
    public void downloadResult(HttpServletResponse response,
                               @RequestParam @ApiParam(value = "竞赛id", required = true) Integer contestId,
                               @RequestParam @ApiParam(value = "学生id", required = true) Integer studentId) {
        downloadBiz.downloadResult(response, contestId, studentId);
    }


}
