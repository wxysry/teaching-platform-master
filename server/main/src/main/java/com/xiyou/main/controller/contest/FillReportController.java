package com.xiyou.main.controller.contest;

import com.xiyou.common.controller.BaseController;
import com.xiyou.common.utils.R;
import com.xiyou.main.biz.contest.FillReportBiz;
import com.xiyou.main.constants.RoleConstant;
import com.xiyou.main.entity.contest.CtBalance;
import com.xiyou.main.entity.contest.CtCharges;
import com.xiyou.main.entity.contest.CtProfitChart;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.apache.shiro.authz.annotation.RequiresRoles;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * @program: multi-module
 * @description: 综合费用表
 * @author: tangcan
 * @create: 2019-07-24 21:34
 **/
@RestController
@RequestMapping("/tp/fillReport")
@Api(tags = "p36:填写报表")
@Validated
public class FillReportController extends BaseController {
    @Autowired
    private FillReportBiz fillReportBiz;

    @ResponseBody
    @PostMapping("/charges/add")
    @ApiOperation(value = "填写综合费用表", notes = "信息费不用，其他就是损失")
    @RequiresRoles(RoleConstant.STUDENT)
    public R addCharges(@RequestBody @Validated @ApiParam(value = "综合费用表", required = true) CtCharges charges) {
        return fillReportBiz.addCharges(getUserId(), charges);
    }

    @ResponseBody
    @PostMapping("/profitChart/add")
    @ApiOperation(value = "填写利润表")
    @RequiresRoles(RoleConstant.STUDENT)
    public R addProfitChart(@RequestBody @Validated @ApiParam(value = "利润表", required = true) CtProfitChart profitChart) {
        return fillReportBiz.addProfitChart(getUserId(), profitChart);
    }

    @ResponseBody
    @PostMapping("/balance/add")
    @ApiOperation(value = "填写资产负债表")
    @RequiresRoles(RoleConstant.STUDENT)
    public R addBalance(@RequestBody @Validated @ApiParam(value = "资产负债表", required = true) CtBalance balance) {
        return fillReportBiz.addBalance(getUserId(), balance);
    }


}
