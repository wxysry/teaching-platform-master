package com.xiyou.main.controller.contest;

import com.xiyou.common.controller.BaseController;
import com.xiyou.common.utils.R;
import com.xiyou.main.biz.contest.ISOInvestBiz;
import com.xiyou.main.constants.RoleConstant;
import com.xiyou.main.params.contest.ISOInvestParam;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.shiro.authz.annotation.RequiresRoles;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotNull;

/**
 * @program: multi-module
 * @description: ISO投资
 * @author: tangcan
 * @create: 2019-09-01 12:42
 **/
@RestController
@RequestMapping("/tp/contest/isoInvest")
@Api(tags = "p30 ISO投资")
@Validated
public class ISOInvestController extends BaseController {
    @Autowired
    private ISOInvestBiz isoInvestBiz;

    @ResponseBody
    @GetMapping("/list")
    @ApiOperation(value = "获取ISO列表")
    @RequiresRoles(RoleConstant.STUDENT)
    public R list(@RequestParam @NotNull(message = "contestId不能为空") Integer contestId) {
        return isoInvestBiz.list(contestId, getUserId());
    }

    @ResponseBody
    @PostMapping("/commit")
    @ApiOperation(value = "确认ISO投资")
    @RequiresRoles(RoleConstant.STUDENT)
    public R commit(@RequestBody @Validated ISOInvestParam isoInvestParam) {
        isoInvestParam.setStudentId(getUserId());
        return isoInvestBiz.commit(isoInvestParam);
    }
}
