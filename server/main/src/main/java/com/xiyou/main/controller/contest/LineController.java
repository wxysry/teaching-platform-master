package com.xiyou.main.controller.contest;

import com.xiyou.common.controller.BaseController;
import com.xiyou.common.utils.R;
import com.xiyou.main.biz.contest.LineBiz;
import com.xiyou.main.constants.RoleConstant;
import com.xiyou.main.entity.contest.CtLine;
import com.xiyou.main.groups.Add;
import com.xiyou.main.params.contest.OnlineConfirm;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.apache.shiro.authz.annotation.RequiresRoles;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotNull;

/**
 * @author dingyoumeng
 * @since 2019/07/22
 */
@RestController
@RequestMapping("/tp/contest/line")
@Api(tags = "p14生产线")
@Validated
public class LineController extends BaseController {

    @Autowired
    LineBiz lineBiz;

    @ResponseBody
    @GetMapping("/product/info")
    @ApiOperation(value = "产品信息获取")
    @RequiresRoles(RoleConstant.STUDENT)
    public R proInfo(@RequestParam @NotNull(message = "contestId不能为空") Integer contestId) {
        return lineBiz.proInfo(contestId);
    }

    @ResponseBody
    @GetMapping("/info")
    @ApiOperation(value = "新建生产线-厂房、类型、产品获取")
    @RequiresRoles(RoleConstant.STUDENT)
    public R info(@RequestParam @NotNull(message = "contestId不能为空") Integer contestId) {
        return lineBiz.info(getUserId(), contestId);
    }

    @ResponseBody
    @PostMapping("/add")
    @ApiOperation(value = "新建生产线-确认")
    @RequiresRoles(RoleConstant.STUDENT)
    public R add(@RequestBody @Validated({Add.class}) @ApiParam(value = "生产线", required = true) CtLine line) {
        line.setStudentId(getUserId());
        return lineBiz.add(line);
    }


    @ResponseBody
    @GetMapping("/listOnline")
    @ApiOperation(value = "在建生产线-弹窗列表")
    @RequiresRoles(RoleConstant.STUDENT)
    public R list(@RequestParam @NotNull(message = "contestId不能为空") Integer contestId,
                  @RequestParam @NotNull(message = "当前时间不能为空") Integer date) {
        return lineBiz.listOnline(getUserId(), contestId, date);
    }


    @ResponseBody
    @PostMapping("/online/confirm")
    @ApiOperation(value = "在建生产线-确定")
    @RequiresRoles(RoleConstant.STUDENT)
    public R confirm(@RequestBody @Validated OnlineConfirm onlineConfirm) {
        return lineBiz.building(getUserId(), onlineConfirm);
    }

    @ResponseBody
    @GetMapping("/list/transfer")
    @ApiOperation(value = "转产生产线-列表OR出售生产表列表")
    @RequiresRoles(RoleConstant.STUDENT)
    public R listTransfer(@RequestParam @NotNull(message = "contestId不能为空") Integer contestId) {
        return lineBiz.listTransfer(getUserId(), contestId);
    }

    @ResponseBody
    @PostMapping("/transfer")
    @ApiOperation(value = "转产生产线-转产")
    @RequiresRoles(RoleConstant.STUDENT)
    public R lineTransfer(@RequestBody OnlineConfirm confirm) {
        return lineBiz.lineTransfer(getUserId(), confirm);
    }

    @ResponseBody
    @PostMapping("/sell")
    @ApiOperation(value = "转产生产线-出售")
    @RequiresRoles(RoleConstant.STUDENT)
    public R lineSell(@RequestBody @Validated OnlineConfirm confirm) {
        return lineBiz.lineSell(getUserId(), confirm);
    }

    @ResponseBody
    @GetMapping("/list/noProducting")
    @ApiOperation(value = "开始下一批生产-列表")
    @RequiresRoles(RoleConstant.STUDENT)
    public R listNoProducting(@RequestParam @NotNull(message = "contestId不能为空") Integer contestId) {
        return lineBiz.listNoProducting(getUserId(), contestId);
    }

    @ResponseBody
    @PostMapping("/producting")
    @ApiOperation(value = "开始下一批生产-确认")
    @RequiresRoles(RoleConstant.STUDENT)
    public R producting(@RequestBody @Validated OnlineConfirm confirm) {
        return lineBiz.producting(getUserId(), confirm);
    }
}
