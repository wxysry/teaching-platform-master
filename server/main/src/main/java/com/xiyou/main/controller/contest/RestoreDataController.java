package com.xiyou.main.controller.contest;

import com.xiyou.common.controller.BaseController;
import com.xiyou.common.utils.R;
import com.xiyou.main.biz.contest.RestoreDataBiz;
import com.xiyou.main.constants.RoleConstant;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.shiro.authz.annotation.RequiresRoles;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotNull;

/**
 * @program: multi-module
 * @description: 数据还原
 * @author: tangcan
 * @create: 2019-08-31 21:05
 **/
@RestController
@RequestMapping("/tp/contest/data")
@Api(tags = "竞赛模拟-还原本年数据")
@Validated
public class RestoreDataController extends BaseController {
    @Autowired
    private RestoreDataBiz restoreDataBiz;

    @ResponseBody
    @GetMapping("/restore")
    @ApiOperation(value = "还原本年数据到第一季度-学生操作")
    @RequiresRoles(RoleConstant.STUDENT)
    public R restore(@RequestParam @NotNull(message = "contestId不能为空") Integer contestId) {
        return restoreDataBiz.restore(contestId, getUserId());
    }

    @ResponseBody
    @GetMapping("/restore/teacher")
    @ApiOperation(value = "还原本年数据到第一季度-教师操作")
    @RequiresRoles(RoleConstant.TEACHER)
    public R restoreByTeacher(@RequestParam @NotNull(message = "contestId不能为空") Integer contestId,
                              @RequestParam @NotNull(message = "studentId不能为空") Integer studentId) {
        return restoreDataBiz.restore(contestId, studentId);
    }

    @ApiOperation(value = "还原当季")
    @GetMapping("/restore/restoreSeason")
    public R restoreSeason(Integer contestId) {
        return restoreDataBiz.restoreSeason(contestId,getUserId());
    }
}
