package com.xiyou.main.controller.contest;

import com.xiyou.common.controller.BaseController;
import com.xiyou.common.enums.CodeEnum;
import com.xiyou.common.utils.R;
import com.xiyou.main.biz.contest.TradeFairBiz;
import com.xiyou.main.constants.RoleConstant;
import com.xiyou.main.params.contest.UserAndOrderParam;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.apache.shiro.authz.annotation.RequiresRoles;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * @program: multi-module
 * @description: 订货会
 * @author: tangcan
 * @create: 2019-07-26 10:04
 **/
@RestController
@RequestMapping("/tp/tradeFair")
@Api(tags = "p40:参加订货会")
@Validated
public class TradeFairController extends BaseController {
    @Autowired
    private TradeFairBiz tradeFairBiz;

    @ResponseBody
    @PostMapping("/getUserAndOrder")
    @ApiOperation(value = "获取用户和订单")
    @RequiresRoles(RoleConstant.STUDENT)
    public R getUserAndOrder(@RequestBody @Validated UserAndOrderParam userAndOrderParam) {
        userAndOrderParam.setStudentId(getUserId());
        userAndOrderParam.setYear(userAndOrderParam.getDate() / 10);
        return tradeFairBiz.getUserAndOrder(userAndOrderParam);
    }

    @ResponseBody
    @PostMapping("/order/choose")
    @ApiOperation(value = "选单")
    @RequiresRoles(RoleConstant.STUDENT)
    public R chooseOrder(@RequestBody @Validated UserAndOrderParam userAndOrderParam) {
        if (userAndOrderParam.getChooseId() == null) {
            return R.error(CodeEnum.PARAM_ERROR.getCode(), "选单id不能为空");
        }
        userAndOrderParam.setStudentId(getUserId());
        userAndOrderParam.setYear(userAndOrderParam.getDate() / 10);
        return tradeFairBiz.chooseOrder(userAndOrderParam);
    }

    @ResponseBody
    @GetMapping("/end")
    @ApiOperation(value = "结束订货会，备份数据")
    @RequiresRoles(RoleConstant.STUDENT)
    public R end(@RequestParam @NotNull(message = "contestId不能为空") Integer contestId) {
        return tradeFairBiz.end(contestId, getUserId());
    }

    @ResponseBody
    @GetMapping("/order/restore")
    @ApiOperation(value = "重新选单")
    @RequiresRoles(RoleConstant.STUDENT)
    public R restoreOrder(@RequestParam @NotNull(message = "contestId不能为空") Integer contestId,
                          @RequestParam @NotNull(message = "date不能为空") Integer date) {
        return tradeFairBiz.restoreOrder(getUserId(), contestId, date);
    }
}
