package com.xiyou.main.controller.contest;

import com.xiyou.common.controller.BaseController;
import com.xiyou.common.utils.R;
import com.xiyou.main.biz.contest.YearEndBiz;
import com.xiyou.main.constants.RoleConstant;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.apache.shiro.authz.annotation.RequiresRoles;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotNull;

/**
 * @program: multi-module
 * @description: 当年结束后
 * @author: tangcan
 * @create: 2019-07-22 16:04
 **/
@RestController
@RequestMapping("/tp/yearEnd")
@Api(tags = "p31~34:当年结束后")
@Validated
public class YearEndController extends BaseController {
    @Autowired
    private YearEndBiz yearEndBiz;

    @ResponseBody
    @GetMapping("/confirm")
    @ApiOperation(value = "p31:确认当年结束")
    @RequiresRoles(RoleConstant.STUDENT)
    public R confirm(@RequestParam @NotNull @ApiParam(value = "考试id", required = true) Integer contestId,
                        @RequestParam @NotNull @ApiParam(value = "当前时间", required = true) Integer date) {
        return yearEndBiz.confirm(getUserId(), contestId, date);
    }

}
