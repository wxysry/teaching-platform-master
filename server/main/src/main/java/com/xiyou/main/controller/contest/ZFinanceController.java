package com.xiyou.main.controller.contest;

import com.xiyou.common.controller.BaseController;
import com.xiyou.common.utils.R;
import com.xiyou.main.biz.contest.*;
import com.xiyou.main.constants.RoleConstant;
import com.xiyou.main.params.contest.MaterialParam;
import com.xiyou.main.vo.contest.CashFolowEntity;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.apache.shiro.authz.annotation.RequiresRoles;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotNull;

/**
 * @author: zhengxiaodong
 * @description: 财务信息统计
 * @since: 2019-07-22 15:37:46
 **/
@RestController
@RequestMapping("/tp/finance")
@Validated
@Api(tags = "p3-财务信息&综合财务信息")
public class ZFinanceController extends BaseController {

    @Autowired
    private StudentFinanceBiz studentFinanceBiz;
    @Autowired
    private P6FactoryInfoBiz p6FactoryInfoBiz;
    @Autowired
    private ApplyForLoanBiz applyForLoanBiz;
    @Autowired
    private CurrentQuarterBiz currentQuarterBiz;
    @Autowired
    private RawMaterialUpdateBiz rawMaterialUpdateBiz;
    @Autowired
    private FactoryAndProdLineBiz factoryAndProdLineBiz;

    @ApiOperation(value = "p3财务信息&综合财务信息查询")
    @GetMapping("/statistical/info")
    @RequiresRoles(RoleConstant.STUDENT)
    public R getStudentFinanceInfor(@RequestParam @NotNull @ApiParam(value = "竞赛id") Integer contestId,
                                    @RequestParam @NotNull @ApiParam(value = "当前时间") Integer currentTime) {

        CashFolowEntity cashFolowEntity = new CashFolowEntity();
        cashFolowEntity.setStudentId(getUserId());
        cashFolowEntity.setContestId(contestId);
        // 当前时间
        cashFolowEntity.setCurrentTime(currentTime);
        // 当前时间
        cashFolowEntity.setCurrentYear(currentTime / 10);

        return studentFinanceBiz.getStudentFinanceInfo(cashFolowEntity);
    }

    @ApiOperation(value = "P4研发认证信息")
    @GetMapping("/researchAndAuth/info")
    @RequiresRoles(RoleConstant.STUDENT)
    public R researchAndAuthenInfo(@RequestParam @ApiParam(value = "竞赛id") Integer contestId) {

        return studentFinanceBiz.getResearchAndDevelopeInfo(getUserId(), contestId);
    }

    @ApiOperation(value = "P5库存采购信息")
    @GetMapping("/stockPurchase/info")
    @RequiresRoles(RoleConstant.STUDENT)
    public R getStockPurchaseInfo(@RequestParam @ApiParam(value = "竞赛id") Integer contestId) {
        return studentFinanceBiz.stockPurchaseInfo(getUserId(), contestId);
    }

    @ApiOperation(value = "P6厂房信息")
    @GetMapping("/workshop/info")
    @RequiresRoles(RoleConstant.STUDENT)
    public R getWorkshopInfo(@RequestParam @ApiParam(value = "竞赛id") Integer contestId,
                             @RequestParam @ApiParam(value = "当前时间") Integer currentTime) {
        CashFolowEntity cashFolowEntity = new CashFolowEntity().setStudentId(getUserId()).setContestId(contestId).setCurrentTime(currentTime).setCurrentYear(currentTime / 10);
        return p6FactoryInfoBiz.getWorkshopInfo(cashFolowEntity);
    }

    @ApiOperation(value = "P8申请长贷")
    @GetMapping("/longloan/info")
    @RequiresRoles(RoleConstant.STUDENT)
    public R applyForLongLoan(@RequestParam @ApiParam(value = "竞赛id") Integer contestId,
                              @RequestParam @ApiParam(value = "当前时间") Integer currentTime) {
        CashFolowEntity cashFolowEntity = new CashFolowEntity()
                .setStudentId(getUserId())
                .setContestId(contestId)
                .setCurrentTime(currentTime)
                .setCurrentYear(currentTime / 10);
        cashFolowEntity.setBlType(1);
        return applyForLoanBiz.getMaxLoanAmountInfo(cashFolowEntity);
    }

    @ApiOperation(value = "P8申请长贷-确认")
    @PostMapping("/longloan/commit")
    @RequiresRoles(RoleConstant.STUDENT)
    public R applyForLongLoanCommit(@RequestBody CashFolowEntity cashFolowEntity) {

        // 获取学生ID
        cashFolowEntity.setStudentId(getUserId());
        cashFolowEntity.setBlType(1);
        cashFolowEntity.setCurrentYear(cashFolowEntity.getCurrentTime() / 10);

        return applyForLoanBiz.applyForLoanCommit(cashFolowEntity);
    }

    @ApiOperation(value = "p9当季开始")
    @GetMapping("/currentQuarter/start")
    @RequiresRoles(RoleConstant.STUDENT)
    public R currentQuarterStart(@RequestParam @NotNull @ApiParam(value = "竞赛id") Integer contestId,
                                 @RequestParam @NotNull @ApiParam(value = "当前时间") Integer currentTime) {
        CashFolowEntity cashFolowEntity = new CashFolowEntity()
                .setCurrentTime(currentTime)
                .setCurrentYear(currentTime / 10)
                .setStudentId(getUserId())
                .setContestId(contestId);
        return currentQuarterBiz.currentQuarterBegin(cashFolowEntity);
    }

    @ApiOperation(value = "P10申请短贷")
    @GetMapping("/shortloan/info")
    @RequiresRoles(RoleConstant.STUDENT)
    public R applyForShortLoan(@RequestParam @ApiParam(value = "竞赛id") Integer contestId,
                               @RequestParam @ApiParam(value = "当前时间") Integer currentTime) {
        CashFolowEntity cashFolowEntity = new CashFolowEntity()
                .setStudentId(getUserId())
                .setContestId(contestId)
                .setCurrentTime(currentTime)
                .setCurrentYear(currentTime / 10);
        cashFolowEntity.setBlType(2);
        return applyForLoanBiz.getMaxLoanAmountInfo(cashFolowEntity);
    }

    @ApiOperation(value = "P10申请短贷-确认")
    @PostMapping("/shortloan/commit")
    @RequiresRoles(RoleConstant.STUDENT)
    public R applyForShortLoanCommit(@RequestBody CashFolowEntity cashFolowEntity) {

        // 获取学生ID
        cashFolowEntity.setStudentId(getUserId());
        cashFolowEntity.setBlType(2);
        cashFolowEntity.setCurrentYear(cashFolowEntity.getCurrentTime() / 10);

        return applyForLoanBiz.applyForLoanCommit(cashFolowEntity);
    }

    @ApiOperation(value = "P11更新原料：现付金额")
    @GetMapping("/material/update")
    @RequiresRoles(RoleConstant.STUDENT)
    public R updateMaterial(@RequestParam @ApiParam(value = "竞赛id") Integer contestId,
                            @RequestParam @ApiParam(value = "当前时间") Integer currentTime) {
        CashFolowEntity cashFolowEntity = new CashFolowEntity()
                .setStudentId(getUserId())
                .setContestId(contestId)
                .setCurrentTime(currentTime)
                .setCurrentYear(currentTime / 10);
        return rawMaterialUpdateBiz.getNeedPayMoney(cashFolowEntity);
    }

    @ApiOperation(value = "P11更新原料：确认")
    @GetMapping("/material/confirm")
    @RequiresRoles(RoleConstant.STUDENT)
    public R confirmMaterial(@RequestParam @ApiParam(value = "竞赛id") Integer contestId,
                             @RequestParam @ApiParam(value = "当前时间") Integer currentTime) {
        CashFolowEntity cashFolowEntity = new CashFolowEntity()
                .setStudentId(getUserId())
                .setContestId(contestId)
                .setCurrentTime(currentTime)
                .setCurrentYear(currentTime / 10);
        return rawMaterialUpdateBiz.confirmUpdateMaterial(cashFolowEntity);
    }

    @ApiOperation(value = "P12订购原料：获取原料")
    @GetMapping("/material/list")
    @RequiresRoles(RoleConstant.STUDENT)
    public R materialList(@RequestParam @ApiParam(value = "竞赛id") Integer contestId,
                          @RequestParam @ApiParam(value = "当前时间") Integer currentTime) {
        CashFolowEntity cashFolowEntity = new CashFolowEntity()
                .setStudentId(getUserId())
                .setContestId(contestId)
                .setCurrentTime(currentTime)
                .setCurrentYear(currentTime / 10);
        return rawMaterialUpdateBiz.getBuyMaterialList(cashFolowEntity);
    }

    @ApiOperation(value = "P12订购原料：确认")
    @PostMapping("/material/buy")
    @RequiresRoles(RoleConstant.STUDENT)
    public R buyMaterial(@RequestBody @Validated @ApiParam(value = "materialList需要传omCmid、omNum、remainDate（原料列表中的运货期cmLeadDate）") MaterialParam materialParam) {
        materialParam.setStudentId(getUserId());
        return rawMaterialUpdateBiz.confirmBuyMaterialList(materialParam);
    }

    @ApiOperation(value = "P13购租厂房：厂房列表")
    @GetMapping("/factory/list")
    @RequiresRoles(RoleConstant.STUDENT)
    public R factoryList(@RequestParam @ApiParam(value = "竞赛id") Integer contestId) {
        return factoryAndProdLineBiz.getGzWorkshopList(contestId, getUserId());
    }

    @ApiOperation(value = "P13购租厂房：确认")
    @GetMapping("/factory/confirm")
    @RequiresRoles(RoleConstant.STUDENT)
    public R factoryConfirm(@RequestParam @NotNull @ApiParam(value = "竞赛id") Integer contestId,
                            @RequestParam @NotNull @ApiParam(value = "当前时间") Integer currentTime,
                            @RequestParam @NotNull @ApiParam(value = "厂房id") Integer cwid,
                            @RequestParam @NotNull @ApiParam(value = "订购方式[0:买，1:租]") Integer buyPattern) {
        CashFolowEntity cashFolowEntity = new CashFolowEntity()
                .setStudentId(getUserId())
                .setContestId(contestId)
                .setCurrentTime(currentTime)
                .setCurrentYear(currentTime / 10)
                .setCwid(cwid)
                .setBuyPattern(buyPattern);
        return factoryAndProdLineBiz.confirmBuyOrRentWorkshop(cashFolowEntity);
    }
}