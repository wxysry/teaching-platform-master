package com.xiyou.main.controller.exam;

import com.xiyou.common.shiro.service.JWTTokenService;
import com.xiyou.common.utils.StrDecoder;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Map;

/**
 * @program: multi-module
 * @description: nginx静态文件访问第三方认证接口
 * @author: tangcan
 * @create: 2019-10-08 16:18
 **/
@RestController
@RequestMapping("/nginx")
@Api(tags = "第三方认证", hidden = true)
public class AuthController {

    @Autowired
    private JWTTokenService jwtTokenService;

    @ResponseBody
    @RequestMapping("/auth")
    @ApiOperation(value = "认证文件查看权限", hidden = true)
    public void auth(HttpServletRequest request, HttpServletResponse response) {
        String cookie = request.getHeader("Cookie");
        /*
        Cookie格式：token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9; JSESSIONID=7A58E69B145086BBA5286C69E8861F60
         */
        Map<String, String> map = StrDecoder.str2ParamMap(cookie, ";");
        String token = map.get("token");
        if (token != null && jwtTokenService.get(token) != null) {
            response.setStatus(200);
        } else {
            response.setStatus(403);
        }
    }
}
