package com.xiyou.main.controller.exam;


import com.xiyou.common.controller.BaseController;
import com.xiyou.common.utils.R;
import com.xiyou.main.biz.exam.ExamBiz;
import com.xiyou.main.constants.RoleConstant;
import com.xiyou.main.params.exam.ExamPaperParam;
import com.xiyou.main.params.exam.ExamStudentParam;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.apache.shiro.authz.annotation.RequiresRoles;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.NotNull;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author tangcan
 * @since 2019-07-10
 */
@RestController
@RequestMapping("/tp/exam")
@Api(tags = "学生考卷")
@Validated
public class  ExamController extends BaseController {
    @Autowired
    private ExamBiz examBiz;

    @ResponseBody
    @ApiOperation(value = "学生获取自己的考卷列表")
    @RequiresRoles(RoleConstant.STUDENT)
    @PostMapping("/paper/list")
    public R paperList(@RequestBody @Validated @ApiParam(value = "查询条件", required = true) ExamPaperParam examPaperParam) {
        return examBiz.paperList(getUserId(), examPaperParam);
    }

    @ResponseBody
    @PostMapping("/student/list")
    @ApiOperation(value = "教师获取考卷的学生列表")
    @RequiresRoles(RoleConstant.TEACHER)
    public R studentList(@RequestBody @Validated @ApiParam(value = "查询条件", required = true) ExamStudentParam examStudentParam) {
        return examBiz.studentList(getUserId(), examStudentParam);
    }

    @ResponseBody
    @GetMapping("/get")
    @ApiOperation(value = "试卷基础信息")
    @RequiresRoles(RoleConstant.STUDENT)
    public R get(@RequestParam @NotNull @ApiParam(value = "考卷列表中的id，不是paperId", required = true) Integer examId) {
        return examBiz.get(getUserId(), examId);
    }

    @ResponseBody
    @GetMapping("/start")
    @ApiOperation(value = "学生")
    @RequiresRoles(RoleConstant.STUDENT)
    public R start(@RequestParam @NotNull @ApiParam(value = "考卷列表中的id，不是paperId", required = true) Integer examId) {
        return examBiz.start(getUserId(), examId);
    }

    @ResponseBody
    @GetMapping("/interrupt")
    @ApiOperation(value = "学生中断考试")
    @RequiresRoles(RoleConstant.STUDENT)
    public R interrupt(@RequestParam @NotNull @ApiParam(value = "考卷列表中的id，不是paperId", required = true) Integer examId) {
        return examBiz.interrupt(getUserId(), examId);
    }

    @ResponseBody
    @GetMapping("/handIn")
    @ApiOperation(value = "学生提交试卷")
    @RequiresRoles(RoleConstant.STUDENT)
    public R handIn(@RequestParam @NotNull @ApiParam(value = "考卷列表中的id，不是paperId", required = true) Integer examId) {
        return examBiz.handIn(getUserId(), examId);
    }

    @ResponseBody
    @GetMapping("/analysis")
    @ApiOperation(value = "考试分析")
    @RequiresRoles(RoleConstant.TEACHER)
    public R analysis(@RequestParam @NotNull @ApiParam(value = "paperId", required = true) Integer paperId) {
        return examBiz.analysis(paperId);
    }

    @ResponseBody
    @GetMapping("/analysis/download")
    @ApiOperation(value = "考试分析下载")
    public void downloadAnalysis(HttpServletResponse response,
                                 @RequestParam @NotNull @ApiParam(value = "paperId", required = true) Integer paperId) {
        examBiz.downloadAnalysis(paperId, response);
    }

    @ResponseBody
    @GetMapping("/score/download")
    @ApiOperation(value = "考试成绩下载")
    public void downloadScore(HttpServletResponse response,
                              @RequestParam @NotNull @ApiParam(value = "paperId", required = true) Integer paperId) {
        examBiz.downloadScore(paperId, response);
    }

}

