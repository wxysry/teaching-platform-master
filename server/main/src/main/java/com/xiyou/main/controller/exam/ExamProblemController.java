package com.xiyou.main.controller.exam;


import com.xiyou.common.controller.BaseController;
import com.xiyou.common.utils.R;
import com.xiyou.main.biz.exam.ExamProblemBiz;
import com.xiyou.main.constants.RoleConstant;
import com.xiyou.main.params.exam.HandInAnswerParam;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.apache.shiro.authz.annotation.RequiresRoles;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author tangcan
 * @since 2019-07-10
 */
@RestController
@RequestMapping("/tp/examProblem")
@Api(tags = "学生考试题目")
@Validated
public class ExamProblemController extends BaseController {
    @Autowired
    private ExamProblemBiz examProblemBiz;

    @ResponseBody
    @GetMapping("/listByStudent")
    @ApiOperation(value = "学生考试题目", notes = "包含已经提交的答案")
    @RequiresRoles(RoleConstant.STUDENT)
    public R listByStudent(@RequestParam @NotNull @ApiParam(value = "考卷列表中的id，不是paperId", required = true) Integer examId) {
        return examProblemBiz.listByStudent(getUserId(), examId);
    }

    @ResponseBody
    @GetMapping("/student/view")
    @ApiOperation(value = "学生查看已阅试卷的试题", notes = "包含已经提交的答案")
    @RequiresRoles(RoleConstant.STUDENT)
    public R studentView(@RequestParam @NotNull @ApiParam(value = "考卷列表中的id，不是paperId", required = true) Integer examId) {
        return examProblemBiz.studentView(getUserId(), examId);
    }

    @ResponseBody
    @GetMapping("/listByTeacher")
    @ApiOperation(value = "教师获取学生的答卷", notes = "包含已经提交的答案，以及批改记录")
    @RequiresRoles(RoleConstant.TEACHER)
    public R listByTeacher(@RequestParam @NotNull @ApiParam(value = "考卷列表中的id，不是paperId", required = true) Integer examId) {
        return examProblemBiz.listByTeacher(getUserId(), examId);
    }

    @ResponseBody
    @PostMapping("/student/save")
    @ApiOperation(value = "保存学生答案")
    @RequiresRoles(RoleConstant.STUDENT)
    public R handIn(@RequestBody @Validated @ApiParam(value = "答案信息", required = true) HandInAnswerParam answerParam) {
        return examProblemBiz.save(getUserId(), answerParam);
    }

    @ResponseBody
    @GetMapping("/teacher/correct")
    @ApiOperation(value = "教师批改答案")
    @RequiresRoles(RoleConstant.TEACHER)
    public R correct(@RequestParam @NotNull @ApiParam(value = "学生考试题目的id", required = true) Integer problemId,
                     @RequestParam @NotNull @ApiParam(value = "考题列表中的examId", required = true) Integer examId,
                     @RequestParam @NotNull(message = "分数不能为空") @Min(value = 0, message = "分数不能小于0") @ApiParam(value = "学生分数", required = true) Double studentScore) {
        return examProblemBiz.correct(getUserId(), examId, problemId, studentScore);
    }

    @ResponseBody
    @GetMapping("/teacher/correct/complete")
    @ApiOperation(value = "教师完成批改")
    @RequiresRoles(RoleConstant.TEACHER)
    public R completeCorrect(@RequestParam @NotNull @ApiParam(value = "考题列表中的examId", required = true) Integer examId) {
        return examProblemBiz.completeCorrect(getUserId(), examId);
    }
}

