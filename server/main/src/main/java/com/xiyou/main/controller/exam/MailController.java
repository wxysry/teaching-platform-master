package com.xiyou.main.controller.exam;

import com.xiyou.common.email.service.MailService;
import com.xiyou.common.controller.BaseController;
import com.xiyou.common.enums.CodeEnum;
import com.xiyou.common.utils.R;
import com.xiyou.main.entity.exam.SysUser;
import com.xiyou.main.service.exam.SysUserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;

/**
 * @program: multi-module
 * @description: 邮箱
 * @author: tangcan
 * @create: 2019-06-19 13:24
 **/
@RestController
@Api(tags = "邮箱验证码")
@RequestMapping("/tp/email/code")
@Validated
public class MailController extends BaseController {
    @Autowired
    private MailService mailService;
    @Autowired
    private SysUserService sysUserService;

    @ResponseBody
    @GetMapping("/getByEmail")
    @ApiOperation(value = "获取")
    public R getCaptcha(@RequestParam @NotBlank(message = "邮箱不能为空") @Email(message = "邮箱格式错误") @ApiParam(value = "邮箱", required = true) String email) {
        if (sysUserService.get(new SysUser().setEmail(email)) == null) {
            return R.error(CodeEnum.PARAM_ERROR.getCode(), "邮箱未注册");
        }
        mailService.sendMailCode(email);
        return R.success();
    }
}
