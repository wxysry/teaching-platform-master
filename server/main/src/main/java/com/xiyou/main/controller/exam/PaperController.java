package com.xiyou.main.controller.exam;


import com.xiyou.common.annotations.ValidFile;
import com.xiyou.common.constants.FileType;
import com.xiyou.common.controller.BaseController;
import com.xiyou.common.log.annotations.OptLog;
import com.xiyou.common.utils.R;
import com.xiyou.main.biz.exam.PaperBiz;
import com.xiyou.main.constants.RoleConstant;
import com.xiyou.main.entity.exam.Paper;
import com.xiyou.main.params.exam.PaperParam;
import com.xiyou.main.params.exam.PaperProblemParam;
import com.xiyou.main.params.exam.PaperStudentParam;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.apache.shiro.authz.annotation.Logical;
import org.apache.shiro.authz.annotation.RequiresRoles;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.NotNull;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author tangcan
 * @since 2019-07-04
 */
@RestController
@RequestMapping("/tp/paper")
@Api(tags = "教师管理试卷")
@Validated
public class PaperController extends BaseController {
    @Autowired
    private PaperBiz paperBiz;

    @ResponseBody
    @PostMapping("/pic/upload")
    @ApiOperation(value = "封面上传")
    @RequiresRoles(RoleConstant.TEACHER)
    public R uploadCover(@RequestParam("pic") @ValidFile(file = {FileType.JPG, FileType.PNG}) @ApiParam(value = "封面图片", required = true) MultipartFile file) {
        return paperBiz.uploadCover(file);
    }

    @ResponseBody
    @GetMapping("/pic/show")
    @ApiOperation(value = "封面显示")
    public void show(HttpServletResponse response,
                     @RequestParam @ApiParam(value = "图片名称", required = true) String pic) {
        paperBiz.show(response, pic);
    }

    @ResponseBody
    @PostMapping("/addOrUpdate")
    @ApiOperation(value = "创建试卷或更新")
    @RequiresRoles(RoleConstant.TEACHER)
    public R addOrUpdate(@RequestBody @Validated @ApiParam(value = "试卷信息", required = true) Paper paper) {
        return paperBiz.addOrUpdate(getUserId(), paper);
    }

    @ResponseBody
    @PostMapping("/problem/add")
    @ApiOperation(value = "批量添加试题")
    @RequiresRoles(RoleConstant.TEACHER)
    public R addProblem(@RequestBody @Validated @ApiParam(value = "试题列表", required = true) PaperProblemParam problemParam) {
        return paperBiz.addProblem(getUserId(), problemParam);
    }

    @ResponseBody
    @PostMapping("/student/add")
    @ApiOperation(value = "批量添加考生")
    @RequiresRoles(RoleConstant.TEACHER)
    public R addStudent(@RequestBody @Validated @ApiParam(value = "学生列表", required = true) PaperStudentParam studentParam) {
        return paperBiz.addStudent(getUserId(), studentParam);
    }

    @ResponseBody
    @GetMapping("/get")
    @ApiOperation(value = "获取试卷基本信息")
    @RequiresRoles(RoleConstant.TEACHER)
    public R getPaper(@RequestParam @NotNull @ApiParam(value = "试卷id", required = true) Integer paperId) {
        return paperBiz.getPaper(getUserId(), paperId);
    }

    @ResponseBody
    @GetMapping("/problem/get")
    @ApiOperation(value = "获取试卷试题")
    @RequiresRoles(value = {RoleConstant.TEACHER, RoleConstant.STUDENT}, logical = Logical.OR)
    public R getProblem(@RequestParam @NotNull @ApiParam(value = "试卷id", required = true) Integer paperId) {
        return paperBiz.getProblem(paperId);
    }

    @ResponseBody
    @GetMapping("/student/get")
    @ApiOperation(value = "获取试卷考生")
    @RequiresRoles(RoleConstant.TEACHER)
    public R getStudent(@RequestParam @NotNull @ApiParam(value = "试卷id", required = true) Integer paperId) {
        return paperBiz.getStudent(paperId);
    }

    @ResponseBody
    @GetMapping("/preview")
    @ApiOperation(value = "预览试卷")
    @RequiresRoles(RoleConstant.TEACHER)
    public R preview(@RequestParam @NotNull @ApiParam(value = "试卷id", required = true) Integer paperId) {
        return paperBiz.preview(getUserId(), paperId);
    }

    @ResponseBody
    @GetMapping("/publish")
    @ApiOperation(value = "发布试卷")
    @RequiresRoles(RoleConstant.TEACHER)
    public R publish(@RequestParam @NotNull @ApiParam(value = "试卷id", required = true) Integer paperId) {
        return paperBiz.publish(getUserId(), paperId);
    }

    @ResponseBody
    @PostMapping("/list")
    @ApiOperation(value = "获取试卷列表")
    @RequiresRoles(RoleConstant.TEACHER)
    public R list(@RequestBody @Validated @ApiParam(value = "请求参数", required = true) PaperParam paperParam) {
        return paperBiz.list(getUserId(), paperParam);
    }

    @ResponseBody
    @GetMapping("/delete")
    @ApiOperation(value = "删除试卷")
    @RequiresRoles(RoleConstant.TEACHER)
    @OptLog(description = "删除试卷")
    public R delete(@RequestParam @NotNull @ApiParam(value = "试卷id", required = true) Integer paperId) {
        return paperBiz.delete(getUserId(), paperId);
    }
}

