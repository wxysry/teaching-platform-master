package com.xiyou.main.controller.exam;


import com.xiyou.common.annotations.ValidFile;
import com.xiyou.common.constants.FileType;
import com.xiyou.common.controller.BaseController;
import com.xiyou.common.utils.R;
import com.xiyou.main.biz.exam.PptCourseBiz;
import com.xiyou.main.constants.RoleConstant;
import com.xiyou.main.entity.exam.PptCourse;
import com.xiyou.main.groups.Update;
import com.xiyou.main.params.exam.CourseQueryParam;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.apache.shiro.authz.annotation.RequiresRoles;
import org.hibernate.validator.constraints.Length;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author tangcan
 * @since 2019-06-28
 */
@RestController
@RequestMapping("/tp/pptCourse")
@Api(tags = "ppt课程管理")
@Validated
public class PptCourseController extends BaseController {
    @Autowired
    private PptCourseBiz pptCourseBiz;

//    @ResponseBody
//    @PostMapping("/add")
//    @ApiOperation(value = "添加ppt教程")
//    public R add(@RequestBody @Validated(value = Add.class) @ApiParam(value = "请求参数", required = true) PptCourse course) {
//        course.setUploadUserId(getUserId());
//        return pptCourseBiz.addCourse(course);
//    }

    @ResponseBody
    @GetMapping("/delete/{ids}")
    @ApiOperation(value = "删除PPT教程")
    @RequiresRoles(RoleConstant.ADMIN)
    public R delete(@PathVariable("ids") @ApiParam(value = "删除的ids,传多个时，用逗号隔开") @NotNull Integer[] ids) {
        return pptCourseBiz.deleteCourse(ids);
    }

    @ResponseBody
    @PostMapping("/update")
    @ApiOperation(value = "更新PPT教程")
    @RequiresRoles(RoleConstant.ADMIN)
    public R update(@RequestBody @Validated(value = Update.class) @ApiParam(value = "请求参数", required = true) PptCourse course) {
        return pptCourseBiz.updateCourse(course);
    }

    @ResponseBody
    @PostMapping("/list")
    @ApiOperation(value = "管理员查询PPT教程列表")
    @RequiresRoles(RoleConstant.ADMIN)
    public R list(@RequestBody @Validated @ApiParam(value = "请求参数", required = true) CourseQueryParam param) {
        return pptCourseBiz.listCourse(param);
    }

    @ResponseBody
    @PostMapping("/upload/cover")
    @ApiOperation(value = "导入PPT封面")
    @RequiresRoles(RoleConstant.ADMIN)
    public R upload(@RequestParam("file") @ValidFile(file = {FileType.JPG, FileType.PNG}) MultipartFile file) {
        return pptCourseBiz.uploadCover(file);
    }

    @ResponseBody
    @GetMapping("/show/cover")
    @ApiOperation(value = "显示封面图片")
    public void showCover(HttpServletResponse response,
                          @RequestParam @ApiParam(value = "封面图片名称", required = true) String cover) {
        pptCourseBiz.showCover(response, cover);
    }


    @ResponseBody
    @PostMapping("/add")
    @ApiOperation(value = "添加PPT课程")
    @RequiresRoles(RoleConstant.ADMIN)
    public R uploadPpt(@RequestParam("file") @ValidFile(file = {FileType.PPT, FileType.PPTX}) MultipartFile file,
                       @RequestParam @Length(max = 100) String intro, @RequestParam @NotNull Integer groupId, @RequestParam String cover,
                       @RequestParam @NotBlank String pptName) {
        PptCourse course = new PptCourse();
        course.setIntro(intro);
        course.setGroupId(groupId);
        course.setPptName(pptName);
        course.setCover(cover);
        course.setUploadUserId(getUserId());
        return pptCourseBiz.uploadPpt(file, course);
    }

    @ResponseBody
    @GetMapping("/show/ppt/{id}")
    @ApiOperation(value = "显示ppt图片，传入页数id")
    public void showCover(HttpServletResponse response,
                          @RequestParam @ApiParam(value = "封面图片名称", required = true) String folderPath, @PathVariable("id") @ApiParam(value = "ppt的页数") int id) {
        pptCourseBiz.showPptById(response, folderPath, id);
    }

    @ResponseBody
    @PostMapping("/teacher/list")
    @ApiOperation(value = "老师查看ppt教程列表")
    @RequiresRoles(RoleConstant.TEACHER)
    public R listByTeacher(@RequestBody @Validated CourseQueryParam param) {
        return pptCourseBiz.listByTeacher(getUserId(), param);
    }
}

