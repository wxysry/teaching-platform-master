package com.xiyou.main.controller.exam;


import com.xiyou.common.controller.BaseController;
import com.xiyou.common.utils.R;
import com.xiyou.main.biz.exam.PptCourseBiz;
import com.xiyou.main.constants.RoleConstant;
import com.xiyou.main.entity.exam.PptGroup;
import com.xiyou.main.groups.Add;
import com.xiyou.main.groups.Update;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.apache.shiro.authz.annotation.Logical;
import org.apache.shiro.authz.annotation.RequiresRoles;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotNull;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author tangcan
 * @since 2019-06-28
 */
@RestController
@RequestMapping("/tp/pptGroup")
@Api(tags = "ppt教程分组管理")
@Validated
public class PptGroupController extends BaseController {
    @Autowired
    private PptCourseBiz pptCourseBiz;

    @ResponseBody
    @PostMapping("/add")
    @ApiOperation(value = "添加ppt分组")
    @RequiresRoles(RoleConstant.ADMIN)
    public R addGroup(@RequestBody @Validated(Add.class) @ApiParam(value = "请求参数", required = true) PptGroup group) {
        return pptCourseBiz.addGroup(group);
    }

    @ResponseBody
    @GetMapping("/get")
    @ApiOperation(value = "通过id查看ppt教程分组")
    public R getGroup(@RequestParam @NotNull @ApiParam(value = "ppt分组id", required = true) Integer id) {
        return pptCourseBiz.getGroupById(id);
    }

    @ResponseBody
    @GetMapping("/list")
    @ApiOperation(value = "管理员查看所有ppt分组")
    @RequiresRoles(RoleConstant.ADMIN)
    public R listGroup() {
        return pptCourseBiz.listGroup();
    }

    @ResponseBody
    @PostMapping("/update")
    @ApiOperation(value = "更新ppt分组")
    @RequiresRoles(RoleConstant.ADMIN)
    public R updateGroup(@RequestBody @Validated(value = Update.class) @ApiParam(value = "请求参数", required = true) PptGroup group) {
        return pptCourseBiz.updateGroup(group);
    }

    @ResponseBody
    @GetMapping("/delete/{id}")
    @ApiOperation(value = "删除ppt分组", notes = "会删去分组下的ppt教程(传多个id时，为批量删除，中间逗号隔开)")
    @RequiresRoles(RoleConstant.ADMIN)
    public R delete(@PathVariable(value = "id") @NotNull @ApiParam(value = "ids", required = true) String[] ids) {
        return pptCourseBiz.deleteGroup(ids);
    }

    @ResponseBody
    @GetMapping("/teacher/list")
    @ApiOperation(value = "老师查看ppt分组")
    @RequiresRoles(RoleConstant.TEACHER)
    public R listGroupByTeacher() {
        return pptCourseBiz.listGroupByTeacher(getUserId());
    }
}

