package com.xiyou.main.controller.exam;


import com.xiyou.common.annotations.ValidFile;
import com.xiyou.common.constants.FileType;
import com.xiyou.common.controller.BaseController;
import com.xiyou.common.log.annotations.OptLog;
import com.xiyou.common.utils.R;
import com.xiyou.main.biz.exam.ProblemBiz;
import com.xiyou.main.constants.RoleConstant;
import com.xiyou.main.groups.Add;
import com.xiyou.main.groups.Update;
import com.xiyou.main.params.exam.ProblemInfo;
import com.xiyou.main.params.exam.ProblemParam;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.apache.shiro.authz.annotation.RequiresRoles;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.NotNull;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author tangcan
 * @since 2019-06-24
 */
@RestController
@RequestMapping("/tp/problem")
@Api(tags = "题目管理")
@Validated
public class ProblemController extends BaseController {
    @Autowired
    private ProblemBiz problemBiz;

    @ResponseBody
    @PostMapping("/upload")
    @ApiOperation(value = "试题导入")
    @RequiresRoles(RoleConstant.ADMIN)
    @OptLog(description = "试题导入")
    public R upload(@RequestParam("file") @ValidFile(file = {FileType.XLS, FileType.XLSX}) MultipartFile file) {
        return problemBiz.upload(getUserId(), file);
    }

    @ResponseBody
    @PostMapping("/add")
    @ApiOperation(value = "新增试题")
    @RequiresRoles(RoleConstant.ADMIN)
    public R add(@RequestBody @Validated({Add.class}) @ApiParam(value = "试题信息", required = true) ProblemInfo problemInfo) {
        return problemBiz.add(getUserId(), problemInfo);
    }

    @ResponseBody
    @PostMapping("/update")
    @ApiOperation(value = "更新试题")
    @RequiresRoles(RoleConstant.ADMIN)
    public R update(@RequestBody @Validated({Update.class}) @ApiParam(value = "试题信息", required = true) ProblemInfo problemInfo) {
        return problemBiz.update(problemInfo);
    }

    @ResponseBody
    @GetMapping("/admin/get")
    @ApiOperation(value = "管理员查看单个试题")
    @RequiresRoles(RoleConstant.ADMIN)
    public R adminGet(@RequestParam @ApiParam(value = "试题id", required = true) @NotNull(message = "试题id不能为空") Integer id) {
        return problemBiz.adminGet(id);
    }

    @ResponseBody
    @GetMapping("/teacher/get")
    @ApiOperation(value = "教师查看单个试题")
    @RequiresRoles(RoleConstant.TEACHER)
    public R teacherGet(@RequestParam @ApiParam(value = "试题id", required = true) @NotNull(message = "试题id不能为空") Integer id) {
        return problemBiz.teacherGet(getUserId(), id);
    }

    @ResponseBody
    @PostMapping("/listByAdmin")
    @ApiOperation(value = "管理员查询试题列表")
    @RequiresRoles(RoleConstant.ADMIN)
    public R listByAdmin(@RequestBody @Validated @ApiParam(value = "试题参数", required = true) ProblemParam problemParam) {
        return problemBiz.listByAdmin(problemParam);
    }

    @ResponseBody
    @PostMapping("/listByTeacher")
    @ApiOperation(value = "教师查询试题列表")
    @RequiresRoles(RoleConstant.TEACHER)
    public R listByTeacher(@RequestBody @Validated @ApiParam(value = "试题参数", required = true) ProblemParam problemParam) {
        return problemBiz.listByTeacher(getUserId(), problemParam);
    }

    @ResponseBody
    @GetMapping("/delete/{id}")
    @ApiOperation(value = "删除试题", notes = "传多个id时，为批量删除，中间逗号隔开")
    @RequiresRoles(RoleConstant.ADMIN)
    @OptLog(description = "删除试题")
    public R delete(@PathVariable(value = "id") @NotNull @ApiParam(value = "ids", required = true) Integer[] ids) {
        return problemBiz.delete(ids);
    }
}

