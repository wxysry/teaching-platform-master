package com.xiyou.main.controller.exam;


import com.xiyou.common.controller.BaseController;
import com.xiyou.common.utils.R;
import com.xiyou.main.biz.exam.ProblemGroupBiz;
import com.xiyou.main.constants.RoleConstant;
import com.xiyou.main.groups.Add;
import com.xiyou.main.groups.Update;
import com.xiyou.main.params.exam.GroupParam;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.apache.shiro.authz.annotation.RequiresRoles;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotNull;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author tangcan
 * @since 2019-07-07
 */
@RestController
@RequestMapping("/tp/problemGroup")
@Api(tags = "题目分组管理")
@Validated
public class ProblemGroupController extends BaseController {
    @Autowired
    private ProblemGroupBiz problemGroupBiz;

    @ResponseBody
    @GetMapping("/list/admin")
    @ApiOperation(value = "管理员获取分组列表")
    @RequiresRoles(RoleConstant.ADMIN)
    public R listByAdmin(@RequestParam(required = false) @ApiParam(value = "父级分组id,为空查找一级分组，否则查找对应的二级分组") Integer fatherId) {
        return problemGroupBiz.listByAdmin(fatherId);
    }

    @ResponseBody
    @GetMapping("/list/teacher")
    @ApiOperation(value = "教师获取分组列表")
    @RequiresRoles(RoleConstant.TEACHER)
    public R listByTeacher(@RequestParam(required = false) @ApiParam(value = "父级分组id,为空查找一级分组，否则查找对应的二级分组") Integer fatherId) {
        return problemGroupBiz.listByTeacher(getUserId(), fatherId);
    }

    @ResponseBody
    @GetMapping("/list/all/admin")
    @ApiOperation(value = "管理员获取分组级联")
    @RequiresRoles(RoleConstant.ADMIN)
    public R allListByAdmin() {
        return problemGroupBiz.allListByAdmin();
    }

    @ResponseBody
    @GetMapping("/list/all/teacher")
    @ApiOperation(value = "教师获取分组级联")
    @RequiresRoles(RoleConstant.TEACHER)
    public R allListByTeacher() {
        return problemGroupBiz.allListByTeacher(getUserId());
    }

    @ResponseBody
    @PostMapping("/add")
    @ApiOperation(value = "添加分组")
    @RequiresRoles(RoleConstant.ADMIN)
    public R add(@RequestBody @Validated(Add.class) @ApiParam(value = "分组信息", required = true) GroupParam groupParam) {
        return problemGroupBiz.add(groupParam);
    }

    @ResponseBody
    @PostMapping("/update")
    @ApiOperation(value = "更新分组")
    @RequiresRoles(RoleConstant.ADMIN)
    public R update(@RequestBody @Validated(Update.class) @ApiParam(value = "分组信息", required = true) GroupParam groupParam) {
        return problemGroupBiz.update(groupParam);
    }

    @ResponseBody
    @GetMapping("/delete/{id}")
    @ApiOperation(value = "删除试题分组", notes = "传多个id时，为批量删除，中间逗号隔开")
    @RequiresRoles(RoleConstant.ADMIN)
    public R delete(@PathVariable(value = "id") @NotNull @ApiParam(value = "ids", required = true) Integer[] ids) {
        return problemGroupBiz.delete(ids);
    }

}

