package com.xiyou.main.controller.exam;


import com.xiyou.common.utils.R;
import com.xiyou.main.biz.exam.ProblemTypeBiz;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author tangcan
 * @since 2019-06-24
 */
@RestController
@RequestMapping("/tp/problemType")
@Api(tags = "题目类型管理")
@Validated
public class ProblemTypeController {
    @Autowired
    private ProblemTypeBiz problemTypeBiz;

    @ResponseBody
    @GetMapping("/list")
    @ApiOperation(value = "获取题型列表")
    public R list() {
        return problemTypeBiz.list();
    }
}

