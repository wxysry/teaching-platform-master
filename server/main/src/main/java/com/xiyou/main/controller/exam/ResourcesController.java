package com.xiyou.main.controller.exam;


import com.xiyou.common.controller.BaseController;
import com.xiyou.common.utils.R;
import com.xiyou.main.biz.exam.ResourcesBiz;
import com.xiyou.main.constants.RoleConstant;
import com.xiyou.main.params.exam.ResourcesParam;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.apache.shiro.authz.annotation.RequiresRoles;
import org.hibernate.validator.constraints.Length;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.NotBlank;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author tangcan
 * @since 2019-07-07
 */
@RestController
@RequestMapping("/tp/resources")
@Api(tags = "资源管理")
@Validated
public class ResourcesController extends BaseController {

    @Autowired
    private ResourcesBiz resourcesBiz;

    @ResponseBody
    @PostMapping("/attachment/upload")
    @ApiOperation(value = "批量上传附件", notes = "批量或单张，返回所有文件名拼接的字符串")
    @RequiresRoles(RoleConstant.ADMIN)
    public R addAttachment(@RequestParam("files") MultipartFile[] files,
                           @RequestParam @ApiParam(value = "附件简介") @Length(max = 200, message = "简介长度不能大于{max}") String intro) {
        return resourcesBiz.addAttachment(getUserId(), files, intro);
    }

    @ResponseBody
    @PostMapping("/pic/upload")
    @ApiOperation(value = "批量上传图片", notes = "批量或单张，返回所有文件名拼接的字符串")
    @RequiresRoles(RoleConstant.ADMIN)
    public R addPic(@RequestParam("files") MultipartFile[] files,
                    @RequestParam @ApiParam(value = "附件简介") @Length(max = 200, message = "简介长度不能大于{max}") String intro) {
        return resourcesBiz.addPic(getUserId(), files, intro);
    }

    @ResponseBody
    @PostMapping("/attachment/list")
    @ApiOperation(value = "查询附件列表")
    @RequiresRoles(RoleConstant.ADMIN)
    public R attachmentList(@RequestBody @Validated @ApiParam(value = "附件参数", required = true) ResourcesParam resourcesParam) {
        return resourcesBiz.attachmentList(resourcesParam);
    }

    @ResponseBody
    @PostMapping("/pic/list")
    @ApiOperation(value = "查询图片列表")
    @RequiresRoles(RoleConstant.ADMIN)
    public R picList(@RequestBody @Validated @ApiParam(value = "附件参数", required = true) ResourcesParam resourcesParam) {
        return resourcesBiz.picList(resourcesParam);
    }

    @ResponseBody
    @GetMapping("/attachment/download")
    @ApiOperation(value = "下载附件")
    public void downloadAttachment(HttpServletResponse response,
                                   @RequestParam @NotBlank(message = "附件名不能为空") @ApiParam(value = "附件名称", required = true) String attachment,
                                   @RequestParam @NotBlank(message = "原始附件名不能为空") @ApiParam(value = "原始附件名", required = true) String originalName) {
        resourcesBiz.downloadAttachment(response, attachment, originalName);
    }

    @ResponseBody
    @GetMapping("/pic/download")
    @ApiOperation(value = "下载图片")
    public void downloadPic(HttpServletResponse response,
                            @RequestParam @ApiParam(value = "图片名称", required = true) String pic) {
        resourcesBiz.downloadPic(response, pic);
    }
}

