package com.xiyou.main.controller.exam;

import com.xiyou.common.annotations.ValidFile;
import com.xiyou.common.constants.FileType;
import com.xiyou.common.controller.BaseController;
import com.xiyou.common.log.annotations.OptLog;
import com.xiyou.common.utils.R;
import com.xiyou.main.biz.exam.StudentBiz;
import com.xiyou.main.constants.RoleConstant;
import com.xiyou.main.entity.exam.SysUser;
import com.xiyou.main.groups.Add;
import com.xiyou.main.groups.Student;
import com.xiyou.main.groups.Update;
import com.xiyou.main.params.exam.UserParam;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.apache.shiro.authz.annotation.Logical;
import org.apache.shiro.authz.annotation.RequiresRoles;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.NotNull;

/**
 * @program: multi-module
 * @description: 学生
 * @author: tangcan
 * @create: 2019-06-25 12:26
 **/
@RestController
@RequestMapping("/tp/student")
@Api(tags = "学生管理")
@Validated
public class StudentController extends BaseController {
    @Autowired
    private StudentBiz studentBiz;

    @ResponseBody
    @PostMapping("/add")
    @ApiOperation(value = "添加账号", notes = "教师添加学生账号")
    @RequiresRoles(RoleConstant.TEACHER)
    public R add(@RequestBody @Validated({Add.class, Student.class}) @ApiParam(value = "学生信息", required = true) SysUser student) {
        return studentBiz.add(getUserId(), student);
    }

    @ResponseBody
    @PostMapping("/list")
    @ApiOperation(value = "教师获取学生列表")
    @RequiresRoles(RoleConstant.TEACHER)
    public R list(@RequestBody @Validated @ApiParam(value = "请求参数", required = true) UserParam userParam) {
        return studentBiz.list(getUserId(), userParam);
    }

    @ResponseBody
    @PostMapping("/list/all")
    @ApiOperation(value = "教师获取全部学生")
    @RequiresRoles(RoleConstant.TEACHER)
    public R listAll() {
        return studentBiz.listAll(getUserId());
    }

    @ResponseBody
    @PostMapping("/update")
    @ApiOperation(value = "更新信息")
    @RequiresRoles(value = {RoleConstant.TEACHER, RoleConstant.STUDENT}, logical = Logical.OR)
    public R update(@RequestBody @Validated({Update.class}) @ApiParam(value = "学生信息", required = true) SysUser student) {
        return studentBiz.update(student);
    }

    @PostMapping("/upload")
    @ApiOperation(value = "上传学生名单")
    @RequiresRoles(RoleConstant.TEACHER)
    public R upload(@RequestParam("file") @ValidFile(file = {FileType.XLS, FileType.XLSX}, maxSize = "1MB") MultipartFile file) {
        return studentBiz.upload(getUserId(), file);
    }

    @ResponseBody
    @GetMapping("/delete/{id}")
    @ApiOperation(value = "批量删除学生", notes = "传多个id时，为批量删除，中间逗号隔开")
    @RequiresRoles(RoleConstant.TEACHER)
    @OptLog(description = "删除学生")
    public R delete(@PathVariable(value = "id") @NotNull @ApiParam(value = "ids", required = true) Integer[] ids) {
        return studentBiz.delete(ids);
    }

}
