package com.xiyou.main.controller.exam;


import com.xiyou.common.controller.BaseController;
import com.xiyou.common.log.annotations.OptLog;
import com.xiyou.common.shiro.utils.MD5Util;
import com.xiyou.common.shiro.utils.ShiroUtil;
import com.xiyou.common.utils.R;
import com.xiyou.main.biz.exam.SysUserBiz;
import com.xiyou.main.constants.RoleConstant;
import com.xiyou.main.entity.exam.SysUser;
import com.xiyou.main.groups.Update;
import com.xiyou.main.params.exam.LoginParam;
import com.xiyou.main.params.exam.ResetPwdParam;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.apache.shiro.authz.annotation.Logical;
import org.apache.shiro.authz.annotation.RequiresRoles;
import org.hibernate.validator.constraints.Range;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.constraints.NotNull;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author tangcan
 * @since 2019-06-24
 */
@RestController
@RequestMapping("/tp/sysUser")
@Api(tags = "用户")
@Validated
public class SysUserController extends BaseController {
    @Autowired
    private SysUserBiz sysUserBiz;

    @ResponseBody
    @PostMapping("/login")
    @ApiOperation(value = "登陆")
    @OptLog(description = "登录")
    public R login(@RequestBody @Validated @ApiParam(value = "登录参数", required = true) LoginParam loginParam) {
        return sysUserBiz.login(loginParam);
    }

    @ResponseBody
    @GetMapping("/logout")
    @ApiOperation(value = "退出登录")
    public R logoutAccount(HttpServletRequest request) {
        logout(request);
        return R.success();
    }

    @ResponseBody
    @PostMapping("/password/reset")
    @ApiOperation(value = "修改密码")
    public R resetPassword(@RequestBody @Validated @ApiParam(value = "修改密码参数", required = true) ResetPwdParam param) {
        return sysUserBiz.resetPassword(param);
    }

    @ResponseBody
    @GetMapping("/roles/get")
    @ApiOperation(value = "获取当前用户角色信息")
    @RequiresRoles(value = {RoleConstant.STUDENT, RoleConstant.TEACHER, RoleConstant.ADMIN}, logical = Logical.OR)
    public R getRoles() {
        return sysUserBiz.getRoles(getUserId(), getAccount(), getName());
    }

    @ResponseBody
    @GetMapping("/get")
    @ApiOperation(value = "查看个人信息")
    @RequiresRoles(value = {RoleConstant.STUDENT, RoleConstant.TEACHER, RoleConstant.ADMIN}, logical = Logical.OR)
    public R getAllInfo() {
        return sysUserBiz.getAllInfo(getUserId());
    }

    @ResponseBody
    @PostMapping("/update")
    @ApiOperation(value = "更新信息")
    public R update(@RequestBody @Validated({Update.class}) SysUser user) {
        return sysUserBiz.update(getUserId(), user);
    }

    @ResponseBody
    @GetMapping("/delete")
    @ApiOperation(value = "删除用户")
    @RequiresRoles(value = {RoleConstant.TEACHER, RoleConstant.ADMIN}, logical = Logical.OR)
    @OptLog(description = "删除用户")
    public R delete(@RequestParam @NotNull(message = "用户id不能为空") @ApiParam(value = "用户id", required = true) Integer userId) {
        return sysUserBiz.delete(userId);
    }

    @ResponseBody
    @GetMapping("/forbid")
    @ApiOperation(value = "禁止用户登录")
    @RequiresRoles(value = {RoleConstant.TEACHER, RoleConstant.ADMIN}, logical = Logical.OR)
    @OptLog(description = "禁止用户登录")
    public R forbid(@RequestParam @NotNull(message = "用户id不能为空") @ApiParam(value = "用户id", required = true) Integer userId,
                    @RequestParam @NotNull(message = "forbid不能为空") @Range(min = 0, max = 1, message = "forbid只能为0或1") @ApiParam(value = "是否禁止登录", required = true) Integer forbid) {
        return sysUserBiz.forbid(userId, forbid);
    }

}

