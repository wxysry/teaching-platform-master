package com.xiyou.main.controller.exam;

import com.xiyou.common.controller.BaseController;
import com.xiyou.common.utils.R;
import com.xiyou.main.biz.exam.TeacherBiz;
import com.xiyou.main.constants.RoleConstant;
import com.xiyou.main.entity.exam.SysUser;
import com.xiyou.main.groups.Add;
import com.xiyou.main.groups.Teacher;
import com.xiyou.main.groups.Update;
import com.xiyou.main.params.exam.GroupUpdateParam;
import com.xiyou.main.params.exam.UserParam;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.apache.shiro.authz.annotation.Logical;
import org.apache.shiro.authz.annotation.RequiresRoles;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * @program: multi-module
 * @description: 教师
 * @author: tangcan
 * @create: 2019-06-25 12:26
 **/
@RestController
@RequestMapping("/tp/teacher")
@Api(tags = "教师管理")
@Validated
public class TeacherController extends BaseController {
    @Autowired
    private TeacherBiz teacherBiz;

    @ResponseBody
    @PostMapping("/add")
    @ApiOperation(value = "添加账号")
    @RequiresRoles(RoleConstant.ADMIN)
    public R add(@RequestBody @Validated({Add.class, Teacher.class}) @ApiParam(value = "教师信息", required = true) SysUser teacher) {
        return teacherBiz.add(getUserId(), teacher);
    }

    @ResponseBody
    @PostMapping("/list")
    @ApiOperation(value = "获取教师列表")
    @RequiresRoles(RoleConstant.ADMIN)
    public R list(@RequestBody @Validated @ApiParam(value = "请求参数", required = true) UserParam userParam) {
        return teacherBiz.list(userParam);
    }

    @ResponseBody
    @PostMapping("/update")
    @ApiOperation(value = "更新信息")
    @RequiresRoles(value = {RoleConstant.TEACHER, RoleConstant.ADMIN}, logical = Logical.OR)
    public R update(@RequestBody @Validated({Update.class}) @ApiParam(value = "教师信息", required = true) SysUser teacher) {
        return teacherBiz.update(getUserId(), teacher);
    }

    @ResponseBody
    @PostMapping("/group/update")
    @ApiOperation(value = "更新分组权限")
    @RequiresRoles(RoleConstant.ADMIN)
    public R updateGroup(@RequestBody @Validated @ApiParam(value = "分组修改信息", required = true) GroupUpdateParam groupUpdateParam) {
        return teacherBiz.updateGroup(groupUpdateParam);
    }

}
