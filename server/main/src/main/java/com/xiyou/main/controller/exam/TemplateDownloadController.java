package com.xiyou.main.controller.exam;

import com.xiyou.main.biz.exam.TemplateDownloadBiz;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;

/**
 * @program: multi-module
 * @description: 模板下载
 * @author: tangcan
 * @create: 2019-07-09 14:21
 **/
@RestController
@RequestMapping("/tp/download/template")
@Api(tags = "模板下载")
public class TemplateDownloadController {
    @Autowired
    private TemplateDownloadBiz templateDownloadBiz;

    @ResponseBody
    @GetMapping("/problem")
    @ApiOperation(value = "下载试题导入模板")
    public void problem(HttpServletResponse response) {
        templateDownloadBiz.problem(response);
    }

    @ResponseBody
    @GetMapping("/student")
    @ApiOperation(value = "下载学生导入模板")
    public void student(HttpServletResponse response) {
        templateDownloadBiz.student(response);
    }

    @ResponseBody
    @GetMapping("/gz")
    @ApiOperation(value = "下载规则表模板")
    public void gzExcel(HttpServletResponse response) {
        templateDownloadBiz.gzExcel(response);
    }


    @ResponseBody
    @GetMapping("/newGz")
    @ApiOperation(value = "下载新平台模拟规则表模板")
    public void gzNewExcel(HttpServletResponse response) {
        templateDownloadBiz.gzNewExcel(response);
    }

    @ResponseBody
    @GetMapping("/gbGz")
    @ApiOperation(value = "下载组间对抗规则模板")
    public void gzGbExcel(HttpServletResponse response) {
        templateDownloadBiz.gzGbExcel(response);
    }


    @ResponseBody
    @GetMapping("/ngbGz")
    @ApiOperation(value = "下载组间对抗(新)规则模板")
    public void gzNGbExcel(HttpServletResponse response) {
        templateDownloadBiz.gzNGbExcel(response);
    }

}
