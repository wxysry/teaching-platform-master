package com.xiyou.main.controller.exam;


import com.xiyou.common.utils.R;
import com.xiyou.main.biz.exam.UserPptBiz;
import com.xiyou.main.constants.RoleConstant;
import com.xiyou.main.entity.exam.UserPpt;
import com.xiyou.main.entity.exam.UserVideo;
import com.xiyou.main.params.exam.CourseQueryParam;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.shiro.authz.annotation.RequiresRoles;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotNull;

import static com.xiyou.common.shiro.utils.ShiroUtil.getUserId;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author tangcan
 * @since 2019-10-28
 */
@RestController
@RequestMapping("/tp/userPpt")
@Api(tags = "学生ppt学习")
public class UserPptController {
    @Autowired
    private UserPptBiz userPptBiz;

    @ResponseBody
    @GetMapping("/student/list")
    @ApiOperation(value = "拥有该ppt权限的学生")
    @RequiresRoles(RoleConstant.TEACHER)
    public R studentList(@RequestParam @NotNull Integer pptId) {
        return userPptBiz.studentList(pptId);
    }

    @ResponseBody
    @PostMapping("/add")
    @ApiOperation(value = "给学生添加ppt教程权限")
    @RequiresRoles(RoleConstant.TEACHER)
    public R add(@RequestBody @Validated UserPpt userPpt) {
        return userPptBiz.add(userPpt);
    }

    @ResponseBody
    @PostMapping("/list")
    @ApiOperation(value = "ppt教程列表")
    @RequiresRoles(RoleConstant.STUDENT)
    public R list(@RequestBody @Validated CourseQueryParam param) {
        param.setStudentId(getUserId());
        return userPptBiz.list(param);
    }

    @ResponseBody
    @GetMapping("/group/list")
    @ApiOperation(value = "ppt分组列表")
    @RequiresRoles(RoleConstant.STUDENT)
    public R group() {
        return userPptBiz.group(getUserId());
    }

}

