package com.xiyou.main.controller.exam;


import com.xiyou.common.utils.R;
import com.xiyou.main.biz.exam.UserVideoBiz;
import com.xiyou.main.constants.RoleConstant;
import com.xiyou.main.entity.exam.UserVideo;
import com.xiyou.main.params.exam.CourseQueryParam;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.shiro.authz.annotation.RequiresRoles;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotNull;

import static com.xiyou.common.shiro.utils.ShiroUtil.getUserId;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author tangcan
 * @since 2019-10-28
 */
@RestController
@RequestMapping("/tp/userVideo")
@Api(tags = "学生视频学习")
public class UserVideoController {

    @Autowired
    private UserVideoBiz userVideoBiz;

    @ResponseBody
    @GetMapping("/student/list")
    @ApiOperation(value = "拥有该视频权限的学生")
    @RequiresRoles(RoleConstant.TEACHER)
    public R studentList(@RequestParam @NotNull Integer videoId) {
        return userVideoBiz.studentList(videoId);
    }

    @ResponseBody
    @PostMapping("/add")
    @ApiOperation(value = "给学生添加视频教程权限")
    @RequiresRoles(RoleConstant.TEACHER)
    public R add(@RequestBody @Validated UserVideo userVideo) {
        return userVideoBiz.add(userVideo);
    }

    @ResponseBody
    @PostMapping("/list")
    @ApiOperation(value = "视频教程列表")
    @RequiresRoles(RoleConstant.STUDENT)
    public R list(@RequestBody @Validated CourseQueryParam param) {
        param.setStudentId(getUserId());
        return userVideoBiz.list(param);
    }

    @ResponseBody
    @GetMapping("/group/list")
    @ApiOperation(value = "视频分组列表")
    @RequiresRoles(RoleConstant.STUDENT)
    public R group() {
        return userVideoBiz.group(getUserId());
    }
}

