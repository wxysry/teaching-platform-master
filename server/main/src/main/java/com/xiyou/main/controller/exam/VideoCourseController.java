package com.xiyou.main.controller.exam;


import com.xiyou.common.annotations.ValidFile;
import com.xiyou.common.constants.FileType;
import com.xiyou.common.controller.BaseController;
import com.xiyou.common.utils.R;
import com.xiyou.main.biz.exam.VideoCourseBiz;
import com.xiyou.main.constants.RoleConstant;
import com.xiyou.main.entity.exam.VideoCourse;
import com.xiyou.main.groups.Update;
import com.xiyou.main.params.exam.CourseQueryParam;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.apache.shiro.authz.annotation.RequiresAuthentication;
import org.apache.shiro.authz.annotation.RequiresRoles;
import org.hibernate.validator.constraints.Length;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author tangcan
 * @since 2019-06-28
 */
@RestController
@RequestMapping("/tp/videoCourse")
@Api(tags = "视频教程管理")
@Validated
public class VideoCourseController extends BaseController {
    @Autowired
    private VideoCourseBiz videoCourseBiz;

    @ResponseBody
    @PostMapping("/add")
    @ApiOperation(value = "添加视频教程")
    @RequiresRoles(RoleConstant.ADMIN)
    public R add(@RequestParam("file") MultipartFile file,
                 @RequestParam @NotNull(message = "分组id不能为空") @ApiParam(value = "分组id", required = true) Integer groupId,
                 @RequestParam @ApiParam(value = "封面") String cover,
                 @RequestParam @NotBlank(message = "视频名称不能为空") @Length(max = 50, message = "视频名称长度不能大于{max}") @ApiParam(value = "视频名称", required = true) String videoName,
                 @RequestParam @Length(max = 500, message = "简介长度不能大于{max}") @ApiParam(value = "简介") String intro) {
        return videoCourseBiz.addCourse(getUserId(), file, groupId, cover, videoName, intro);
    }

    @ResponseBody
    @GetMapping("/delete/{ids}")
    @ApiOperation(value = "删除视频教程")
    @RequiresRoles(RoleConstant.ADMIN)
    public R delete(@PathVariable("ids") @ApiParam(value = "删除的ids,传多个时，用逗号隔开") @NotNull Integer[] ids) {
        return videoCourseBiz.deleteCourse(ids);
    }

    @ResponseBody
    @PostMapping("/update")
    @ApiOperation(value = "更新视频教程")
    @RequiresRoles(RoleConstant.ADMIN)
    public R update(@RequestBody @Validated(Update.class) @ApiParam(value = "视频信息", required = true) VideoCourse course) {
        return videoCourseBiz.updateCourse(course);
    }

    @ResponseBody
    @PostMapping("/list")
    @ApiOperation(value = "查询视频教程")
    @RequiresRoles(RoleConstant.ADMIN)
    public R list(@RequestBody @Validated @ApiParam(value = "请求参数", required = true) CourseQueryParam param) {
        return videoCourseBiz.listCourse(param);
    }

    @ResponseBody
    @PostMapping("/upload/cover")
    @ApiOperation(value = "导入视频封面")
    @RequiresRoles(RoleConstant.ADMIN)
    public R uploadCover(@RequestParam("file") @ValidFile(file = {FileType.JPG, FileType.PNG}) MultipartFile file) {
        return videoCourseBiz.uploadPic(file);
    }

    @ResponseBody
    @PostMapping("/upload/video")
    @ApiOperation(value = "导入视频")
    @RequiresRoles(RoleConstant.ADMIN)
    public R uploadVideo(@RequestParam("file") @ValidFile(file = {FileType.MP4}) MultipartFile file) {
        return videoCourseBiz.uploadVideo(file);
    }

    @ResponseBody
    @PostMapping("/teacher/list")
    @ApiOperation(value = "老师查看视频教程列表")
    @RequiresRoles(RoleConstant.TEACHER)
    public R listByTeacher(@RequestBody @Validated CourseQueryParam param) {
        return videoCourseBiz.listByTeacher(getUserId(), param);
    }

    @ResponseBody
    @GetMapping("/show/cover")
    @ApiOperation(value = "显示封面图片")
    public void showCover(HttpServletResponse response,
                          @RequestParam @ApiParam(value = "封面图片名称", required = true) String cover) {
        videoCourseBiz.showCover(response, cover);
    }

    @ResponseBody
    @GetMapping("/show/video")
    @ApiOperation(value = "显示视频")
    public void showVideo(HttpServletResponse response,
                          @RequestParam @ApiParam(value = "视频名称", required = true) String video) {
        videoCourseBiz.showVideo(response, video);
    }
}

