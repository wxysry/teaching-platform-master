package com.xiyou.main.controller.exam;


import com.xiyou.common.controller.BaseController;
import com.xiyou.common.utils.R;
import com.xiyou.main.biz.exam.VideoCourseBiz;
import com.xiyou.main.constants.RoleConstant;
import com.xiyou.main.entity.exam.VideoGroup;
import com.xiyou.main.groups.Add;
import com.xiyou.main.groups.Update;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.apache.shiro.authz.annotation.RequiresRoles;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotNull;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author tangcan
 * @since 2019-06-28
 */
@RestController
@RequestMapping("/tp/videoGroup")
@Api(tags = "视频教程分组")
@Validated
public class VideoGroupController extends BaseController {
    @Autowired
    private VideoCourseBiz videoCourseBiz;

    @ResponseBody
    @PostMapping("/add")
    @ApiOperation(value = "添加视频分组")
    @RequiresRoles(RoleConstant.ADMIN)
    public R addGroup(@RequestBody @Validated(Add.class) @ApiParam(value = "请求参数", required = true) VideoGroup group) {
        return videoCourseBiz.addGroup(group);
    }

    @ResponseBody
    @GetMapping("/get")
    @ApiOperation(value = "通过id查看视频教程分组")
    public R getGroup(@RequestParam @NotNull @ApiParam(value = "视频分组id", required = true) Integer id) {
        return videoCourseBiz.getGroupById(id);
    }

    @ResponseBody
    @GetMapping("/list")
    @ApiOperation(value = "管理员查看所有视频分组")
    @RequiresRoles(RoleConstant.ADMIN)
    public R listGroup() {
        return videoCourseBiz.listGroup();
    }

    @ResponseBody
    @GetMapping("/teacher/list")
    @ApiOperation(value = "老师查看视频分组")
    @RequiresRoles(RoleConstant.TEACHER)
    public R listGroupByTeacher() {
        return videoCourseBiz.listGroupByTeacher(getUserId());
    }

    @ResponseBody
    @PostMapping("/update")
    @ApiOperation(value = "更新视频分组")
    @RequiresRoles(RoleConstant.ADMIN)
    public R updateGroup(@RequestBody @Validated(value = Update.class) @ApiParam(value = "请求参数", required = true) VideoGroup group) {
        return videoCourseBiz.updateGroup(group);
    }

    @ResponseBody
    @GetMapping("/delete/{id}")
    @ApiOperation(value = "删除视频分组", notes = "会删去分组下的视频教程(传多个id时，为批量删除，中间逗号隔开)")
    @RequiresRoles(RoleConstant.ADMIN)
    public R delete(@PathVariable(value = "id") @NotNull @ApiParam(value = "ids", required = true) String[] ids) {
        return videoCourseBiz.deleteGroup(ids);
    }


}

