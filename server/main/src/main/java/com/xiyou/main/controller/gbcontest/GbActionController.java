package com.xiyou.main.controller.gbcontest;

import com.xiyou.common.controller.BaseController;
import com.xiyou.common.redis.utils.RedisCache;
import com.xiyou.common.utils.R;
import com.xiyou.main.biz.gbcontest.GbActionBiz;
import com.xiyou.main.constants.RoleConstant;
import com.xiyou.main.entity.gbcontest.GbCtFee;
import com.xiyou.main.params.gbcontest.*;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.shiro.authz.annotation.RequiresRoles;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.NotNull;
import java.util.concurrent.TimeUnit;

/**
 * @author dingyoumeng
 * @since 2019/07/25
 */
@RestController
@RequestMapping("/tp/gbContest")
@Api(tags = "p19-p29")
@Validated
public class GbActionController extends BaseController {

    @Autowired
    GbActionBiz actionBiz;

    @Autowired
    RedisCache redisCache;

//    @ResponseBody
//    @GetMapping("/receivables/check")
//    @ApiOperation(value = "应收款项-p19")
//    @RequiresRoles(RoleConstant.STUDENT)
//    public R receivables(@RequestParam @NotNull(message = "contestId不能为空") Integer contestId) {
//        return actionBiz.checkReceivables(getUserId(), contestId);
//    }


    @ResponseBody
    @GetMapping("/order/list")
    @ApiOperation(value = "交货订单-p20")
    @RequiresRoles(RoleConstant.STUDENT)
    public R listOrder(@RequestParam @NotNull(message = "contestId不能为空") Integer contestId,
                       @RequestParam @NotNull(message = "date不能为空") Integer date) {
        return actionBiz.listOrder(getUserId(), contestId, date);
    }


    @ResponseBody
    @GetMapping("/order/delivery")
    @ApiOperation(value = "交货订单-交货-p20")
    @RequiresRoles(RoleConstant.STUDENT)
    public R deliveryOrder(@RequestParam @NotNull(message = "contestId不能为空") Integer contestId,
                           @RequestParam @NotNull(message = "orderId不能为空") Integer orderId,
                           @RequestParam @NotNull(message = "date不能为空") Integer date) {
        return actionBiz.deliveryOrder(getUserId(), contestId, orderId, date);
    }



    @ResponseBody
    @GetMapping("/product/yfList")
    @ApiOperation(value = "产品研发-列表-p22")
    @RequiresRoles(RoleConstant.STUDENT)
    public R yfProductList(@RequestParam @NotNull(message = "contestId不能为空") Integer contestId) {
        return actionBiz.yfProductList(getUserId(), contestId);
    }

    @ResponseBody
    @PostMapping("/product/yf")
    @ApiOperation(value = "产品研发-确认-p22")
    @RequiresRoles(RoleConstant.STUDENT)
    public R yfProduct(@RequestBody @Validated GbYfParam yfParam) {
        return actionBiz.yfProduct(getUserId(), yfParam);
    }

    @ResponseBody
    @GetMapping("/endingSeason")
    @ApiOperation(value = "学生当季结束 -组间对抗")
    @RequiresRoles(RoleConstant.STUDENT)
    public R endingSeason(@RequestParam @NotNull(message = "contestId不能为空") Integer contestId,
                       @RequestParam @NotNull(message = "date不能为空") Integer date) {
        return actionBiz.endingSeason(getUserId(), contestId, date);
    }


    @ResponseBody
    @GetMapping("/show/fee")
    @ApiOperation(value = "更新应收款- 列表 p24")
    @RequiresRoles(RoleConstant.STUDENT)
    public R showFee(@RequestParam @NotNull(message = "contestId不能为空") Integer contestId,
                     @RequestParam @NotNull(message = "账单类型不能为空") Integer currentTime) {
        return actionBiz.showFee(getUserId(), contestId, GbCtFee.TO_RECEIVABLE,currentTime);
    }

    @ResponseBody
    @GetMapping("/show/pay")
    @ApiOperation(value = "更新应付款- 列表")
    @RequiresRoles(RoleConstant.STUDENT)
    public R showPay(@RequestParam @NotNull(message = "contestId不能为空") Integer contestId,
                     @RequestParam @NotNull(message = "账单类型不能为空") Integer currentTime) {
        return actionBiz.showFee(getUserId(), contestId,GbCtFee.TO_PAYMENT,currentTime);
    }

    @ResponseBody
    @GetMapping("/receivables/confirm")
    @ApiOperation(value = "更新应收款 - 收款")
    @RequiresRoles(RoleConstant.STUDENT)
    public R receivablesConfirm(@RequestParam @NotNull(message = "contestId不能为空") Integer contestId,
                                @RequestParam @NotNull(message = "feeId不能为空") Integer feeId,
                                @RequestParam @NotNull(message = "当前时间") Integer currentTime
    ) {
        return actionBiz.confirmReceivables(getUserId(),contestId,feeId,currentTime);
    }

    @ResponseBody
    @PostMapping("/discount")
    @ApiOperation(value = "更新应收款 - 贴现")
    @RequiresRoles(RoleConstant.STUDENT)
    public R discount(@RequestBody @Validated GbDiscountParam discountParam) {
        return actionBiz.discount(discountParam, getUserId());
    }

    @ResponseBody
    @GetMapping("/pay/confirm")
    @ApiOperation(value = "更新应付款 - 付款")
    @RequiresRoles(RoleConstant.STUDENT)
    public R payment(@RequestParam @NotNull(message = "contestId不能为空") Integer contestId,
                     @RequestParam @NotNull(message = "feeId不能为空") Integer feeId,
                     @RequestParam @NotNull(message = "当前时间") Integer currentTime) {
        return actionBiz.payment(getUserId(),contestId,feeId,currentTime);
    }


    @ResponseBody
    @PostMapping("/urgent/material")
    @ApiOperation(value = "紧急采购 - 材料采购")
    @RequiresRoles(RoleConstant.STUDENT)
    public R materialUrgentBuy(@RequestBody @Validated GbUrgentBuyParam param) {
        return actionBiz.buyMaterial(getUserId(), param);
    }

    @ResponseBody
    @PostMapping("/urgent/product")
    @ApiOperation(value = "紧急采购 - 产成品采购")
    @RequiresRoles(RoleConstant.STUDENT)
    public R productUrgentBuy(@RequestBody @Validated GbUrgentBuyParam param) {
        return actionBiz.buyProduct(getUserId(), param);
    }


    @ResponseBody
    @PostMapping("/sell/material")
    @ApiOperation(value = "出售库存 - 出售材料 - p26")
    @RequiresRoles(RoleConstant.STUDENT)
    public R sellMaterial(@RequestBody @Validated GbSellMaterialParam param) {
        return actionBiz.sellMaterial(getUserId(), param);
    }

    @ResponseBody
    @PostMapping("/sell/product")
    @ApiOperation(value = "出售库存 - 出售产品 - p26")
    @RequiresRoles(RoleConstant.STUDENT)
    public R sellProduct(@RequestBody @Validated GbSellProductParam param) {
        return actionBiz.sellProduct(getUserId(), param);
    }



    @ResponseBody
    @GetMapping("/xzorder")
    @ApiOperation(value = "订单信息- p28")
    @RequiresRoles(RoleConstant.STUDENT)
    public R xzorder(@RequestParam @NotNull(message = "contestId不能为空") Integer contestId,
                     @RequestParam @NotNull(message = "当前时间") Integer date) {
        return actionBiz.xzorder(getUserId(), contestId, date);
    }

    @ResponseBody
    @GetMapping("/market/list")
    @ApiOperation(value = "市场开拓 - 显示 - p29")
    @RequiresRoles(RoleConstant.STUDENT)
    public R marketList(@RequestParam @NotNull(message = "contestId不能为空") Integer contestId) {
        return actionBiz.marketList(getUserId(), contestId);
    }

    @ResponseBody
    @PostMapping("/market/yf")
    @ApiOperation(value = "市场开拓 - 确认")
    @RequiresRoles(RoleConstant.STUDENT)
    public R marketYf(@RequestBody @Validated GbMarketYfParam param) {
        return actionBiz.marketYf(getUserId(), param);
    }


    @ResponseBody
    @GetMapping("/social/score")
    @ApiOperation(value = "市场开拓 - 显示")
    @RequiresRoles(RoleConstant.STUDENT)
    public R getCorporateScore(@RequestParam @NotNull(message = "contestId不能为空") Integer contestId) {
        return actionBiz.getCorporateScore(getUserId(), contestId);
    }

    @ResponseBody
    @PostMapping("/add/capital")
    @ApiOperation(value = "注资")
    @RequiresRoles(RoleConstant.TEACHER)
    public R addCapital(@RequestBody @Validated GbCapitalParam gbCapitalParam) {
        return actionBiz.addCapital(gbCapitalParam);
    }


    @ResponseBody
    @GetMapping("/get/other/info")
    @ApiOperation(value = "数据咨询")
    public R getOtherInfo(HttpServletResponse response,
                          @RequestParam @NotNull(message = "contestId不能为空") Integer contestId,
                          @RequestParam @NotNull(message = "contestId不能为空") Integer userId,
                          @RequestParam @NotNull(message = "studentId不能为空") Integer studentId,
                          @RequestParam @NotNull(message = "date不能为空") Integer date,
                          @RequestParam @NotNull(message = "amount不能为空") Integer amount) {
        return actionBiz.getOtherInfo(response,userId,studentId,contestId,date,amount);
    }

    @ResponseBody
    @GetMapping("/redisTest")
    @ApiOperation(value = "redis测试")
    public R redisTest() {
        redisCache.set("111","22222", 20, TimeUnit.SECONDS);
        return  R.success();
    }


    @ResponseBody
    @GetMapping("/getRedisTest")
    @ApiOperation(value = "获取到期时间")
    public R getRedisTest(@RequestParam @NotNull(message = "key不能为空") String  key) {
        long time  = redisCache.getExpire(key);
        return  R.success().put("time",time);
    }


}
