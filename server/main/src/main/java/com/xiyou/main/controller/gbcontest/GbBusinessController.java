package com.xiyou.main.controller.gbcontest;

import com.xiyou.common.controller.BaseController;
import com.xiyou.common.utils.R;
import com.xiyou.main.biz.gbcontest.GbBusinessBiz;
import com.xiyou.main.constants.RoleConstant;
import com.xiyou.main.entity.gbcontest.GbContestStudent;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.apache.shiro.authz.annotation.RequiresRoles;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * @program: multi-module
 * @description: 经营
 * @author: wangxingyu
 * @create: 2023-06-09
 **/
@RestController
@RequestMapping("/tp/gbBusiness")
@Api(tags = "p0-经营")
@Validated
public class GbBusinessController extends BaseController {
    @Autowired
    private GbBusinessBiz gbBusinessBiz;

    @ResponseBody
    @GetMapping("/start")
    @ApiOperation(value = "开始经营")
    @RequiresRoles(RoleConstant.STUDENT)
    public R start(@RequestParam @NotNull @ApiParam(value = "考试id", required = true) Integer contestId) {
        return gbBusinessBiz.start(getUserId(), contestId);
    }



    @ResponseBody
    @GetMapping("/nextStatus")
    @ApiOperation(value = "学生-点击继续")
    @RequiresRoles(RoleConstant.STUDENT)
    public R nextStatus(@RequestParam @NotNull @ApiParam(value = "考试id", required = true) Integer contestId) {
        return gbBusinessBiz.nextStatus(getUserId(), contestId);
    }



    @ResponseBody
    @GetMapping("/progress/save")
    @ApiOperation(value = "保存经营进度-学生")
    @RequiresRoles(RoleConstant.STUDENT)
    public R saveProgress(@RequestParam @NotNull @ApiParam(value = "考试id", required = true) Integer contestId,
                          @RequestParam @NotNull @ApiParam(value = "经营时间进度", required = true) Integer date,
                          @RequestParam @NotBlank @ApiParam(value = "经营进度", required = true) String progress) {
        return gbBusinessBiz.saveProgress(getUserId(), contestId, date, progress);
    }

    @ResponseBody
    @GetMapping("/progress/save/teacher")
    @ApiOperation(value = "保存经营进度-教师")
    @RequiresRoles(RoleConstant.TEACHER)
    public R saveProgressByTeacher(@RequestParam @NotNull @ApiParam(value = "考试id", required = true) Integer contestId,
                                   @RequestParam @NotNull @ApiParam(value = "学生id", required = true) Integer studentId,
                                   @RequestParam @NotNull @ApiParam(value = "经营时间进度", required = true) Integer date,
                                   @RequestParam @NotBlank @ApiParam(value = "经营进度", required = true) String progress) {
        return gbBusinessBiz.saveProgress(studentId, contestId, date, progress);
    }

    @ResponseBody
    @GetMapping("/progress/get")
    @ApiOperation(value = "获取经营进度")
    @RequiresRoles(RoleConstant.STUDENT)
    public R getProgress(@RequestParam @NotNull @ApiParam(value = "考试id", required = true) Integer contestId) {
        return gbBusinessBiz.getProgress(getUserId(), contestId);
    }



    @ResponseBody
    @GetMapping("/end")
    @ApiOperation(value = "结束经营")
    @RequiresRoles(RoleConstant.STUDENT)
    public R end(@RequestParam @NotNull @ApiParam(value = "考试id", required = true) Integer contestId) {
        return gbBusinessBiz.end(getUserId(), contestId);
    }

}
