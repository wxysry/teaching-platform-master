package com.xiyou.main.controller.gbcontest;


import com.xiyou.common.controller.BaseController;
import com.xiyou.common.log.annotations.OptLog;
import com.xiyou.common.utils.R;
import com.xiyou.main.biz.gbcontest.GbContestBiz;
import com.xiyou.main.biz.gbcontest.GbRestoreDataBiz;
import com.xiyou.main.constants.RoleConstant;
import com.xiyou.main.entity.gbcontest.GbContest;
import com.xiyou.main.entity.gbcontest.GbCtAnnouncement;
import com.xiyou.main.params.contest.ContestParam;
import com.xiyou.main.params.contest.ContestStudentParam;
import com.xiyou.main.params.gbcontest.GbCompanyParam;
import com.xiyou.main.params.gbcontest.GbDigitalizeParam;
import com.xiyou.main.params.gbcontest.GbContestParam;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.apache.shiro.authz.annotation.RequiresRoles;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.NotNull;
import java.io.IOException;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author tangcan
 * @since 2019-08-25
 */
@RestController
@RequestMapping("/tp/gbContest")
@Api(tags = "组间对抗")
@Validated
public class GbContestController extends BaseController {
    @Autowired
    private GbContestBiz gbContestBiz;
    @Autowired
    private GbRestoreDataBiz gbRestoreDataBiz;


    @ResponseBody
    @PostMapping("/list/teacher")
    @ApiOperation(value = "教师获取竞赛模拟列表")
    @RequiresRoles(RoleConstant.TEACHER)
    public R listByTeacher(@RequestBody @Validated @ApiParam(value = "竞赛查询参数", required = true) ContestParam contestParam) {
        contestParam.setTeacherId(getUserId());
        return gbContestBiz.listByTeacher(contestParam);
    }

    @ResponseBody
    @PostMapping("/list/admin")
    @ApiOperation(value = "教师获取竞赛模拟列表")
    @RequiresRoles(RoleConstant.ADMIN)
    public R listByAdmin(@RequestBody @Validated @ApiParam(value = "竞赛查询参数", required = true) ContestParam contestParam) {
        return gbContestBiz.listByAdmin(contestParam);
    }

    @ResponseBody
    @PostMapping("/student/list")
    @ApiOperation(value = "获取竞赛学生列表")
    public R studentList(@RequestBody @Validated @ApiParam(value = "学生查询参数", required = true) ContestStudentParam contestStudentParam) {
        contestStudentParam.setTeacherId(getUserId());
        return gbContestBiz.studentList(contestStudentParam);
    }

    @ResponseBody
    @GetMapping("/student/score")
    @ApiOperation(value = "设置学生成绩")
    @RequiresRoles(RoleConstant.TEACHER)
    public R studentScore(@RequestParam @NotNull @ApiParam(value = "列表中的id") Integer id,
                          @RequestParam @NotNull(message = "成绩不能为空") @ApiParam(value = "成绩") Double score) {
        return gbContestBiz.studentScore(id, score);
    }

    @ResponseBody
    @PostMapping("/list/student")
    @ApiOperation(value = "学生获取竞赛模拟列表")
    @RequiresRoles(RoleConstant.STUDENT)
    public R listByStudent(@RequestBody @Validated @ApiParam(value = "竞赛查询参数", required = true) ContestParam contestParam) {
        contestParam.setStudentId(getUserId());
        return gbContestBiz.listByStudent(contestParam);
    }

    @ResponseBody
    @GetMapping("/get")
    @ApiOperation(value = "获取竞赛基本信息")
//    @RequiresRoles(RoleConstant.TEACHER)
    public R get(@RequestParam @NotNull Integer contestId) {
        return gbContestBiz.get(contestId);
    }


    @ResponseBody
    @GetMapping("/marketForecast")
    @ApiOperation(value = "市场预测")
    public R marketForecast(@RequestParam @NotNull Integer contestId) {
        return gbContestBiz.marketForecast(contestId);
    }



    @ResponseBody
    @GetMapping("/equity/get")
    @ApiOperation(value = "获取初始权益")
    @RequiresRoles(RoleConstant.TEACHER)
    public R getEquity(@RequestParam @NotNull @ApiParam(name = "题库号") Integer subjectNumber) {
        return gbContestBiz.getEquity(subjectNumber);
    }

    @ResponseBody
    @PostMapping("/add")
    @ApiOperation(value = "添加竞赛")
    @RequiresRoles(RoleConstant.TEACHER)
    @OptLog(description = "添加竞赛")
    public R add(@RequestBody @Validated GbContest gbContest) {
        gbContest.setTeacherId(getUserId());
        return gbContestBiz.add(gbContest);
    }

    @ResponseBody
    @PostMapping("/update")
    @ApiOperation(value = "更新竞赛基本信息")
    @RequiresRoles(RoleConstant.TEACHER)
    public R update(@RequestBody @Validated GbContest gbContest) {
        gbContest.setTeacherId(getUserId());
        return gbContestBiz.update(gbContest);
    }

    @ResponseBody
    @GetMapping("/publish")
    @ApiOperation(value = "发布竞赛", notes = "只能同时发布一个竞赛")
    @RequiresRoles(RoleConstant.TEACHER)
    public R publish(@RequestParam @NotNull Integer contestId) {
        return gbContestBiz.publish(getUserId(), contestId);
    }

    @ResponseBody
    @GetMapping("/delete")
    @ApiOperation(value = "删除竞赛")
    @RequiresRoles(RoleConstant.TEACHER)
    @OptLog(description = "删除竞赛")
    public R delete(@RequestParam @NotNull Integer contestId) {
        return gbContestBiz.delete(contestId);
    }


    @ResponseBody
    @GetMapping("/attachment/rule/get")
    @ApiOperation(value = "获取规则说明附件")
    public R getRuleAttachment(@RequestParam @ApiParam(value = "竞赛id", required = true) Integer contestId) {
        return gbContestBiz.getRuleAttachment(contestId);
    }

    @ResponseBody
    @GetMapping("/score/download")
    @ApiOperation(value = "竞赛成绩下载")
    public void downloadScore(HttpServletResponse response,
                              @RequestParam @NotNull @ApiParam(value = "contestId", required = true) Integer contestId) {
        gbContestBiz.downloadScore(contestId, response);
    }


    @ResponseBody
    @PostMapping("/start/digitalize")
    @ApiOperation(value = "数字化开启智能生产")
    public R digitalizeStart(@RequestBody @NotNull @Validated GbDigitalizeParam param) {
        return gbContestBiz.digitalizeStart(getUserId(), param);
    }

    @ResponseBody
    @GetMapping("/pause/contest")
    @ApiOperation(value = "暂停比赛")
    public R pauseContest(@RequestParam @ApiParam(value = "竞赛id", required = true) Integer contestId) {
        return gbContestBiz.pause(contestId);
    }

    @ResponseBody
    @GetMapping("/recover/contest")
    @ApiOperation(value = "开始、恢复比赛")
    public R recoverContest(@RequestParam @ApiParam(value = "竞赛id", required = true) Integer contestId,
                            @RequestParam @ApiParam(value = "当前时间", required = true) Integer currentTime) {
        return gbContestBiz.recover(contestId);
    }

    @ResponseBody
    @GetMapping("/last/round")
    @ApiOperation(value = "上一回合")
    public R lastRound(@RequestParam @ApiParam(value = "竞赛id", required = true) Integer contestId,
                       @RequestParam @ApiParam(value = "当前时间", required = true) Integer currentTime) {
        return gbContestBiz.lastRound(contestId,currentTime);
    }



    @ResponseBody
    @GetMapping("/next/round")
    @ApiOperation(value = "下一回合")
    @RequiresRoles(RoleConstant.TEACHER)
    public R nextRound(@RequestParam @ApiParam(value = "竞赛id", required = true) Integer contestId,
                       @RequestParam @ApiParam(value = "当前时间", required = true) Integer currentTime) {
        return gbContestBiz.nextRound(getUserId(),contestId,currentTime);
    }

    @ResponseBody
    @GetMapping("/restore/current")
    @ApiOperation(value = "还原本季")
    @RequiresRoles(RoleConstant.TEACHER)
    public R restoreCurrentSeasonByTeacher(@RequestParam @NotNull(message = "contestId不能为空") Integer contestId,
                                           @RequestParam @NotNull(message = "currentTime不能为空") Integer currentTime) {
        return gbContestBiz.restoreCurrentRound(contestId,currentTime);
    }

    @ResponseBody
    @GetMapping("/restore/time")
    @ApiOperation(value = "还原到某个时间节点的数据")
    @RequiresRoles(RoleConstant.TEACHER)
    public R restore(@RequestParam @NotNull(message = "contestId不能为空") Integer contestId,
                     @RequestParam @NotNull(message = "studentId不能为空") Integer studentId,
                     @RequestParam @NotNull(message = "restoreTime不能为空") Integer restoreTime) {
        return gbRestoreDataBiz.restoreOneData(contestId,studentId,restoreTime);
    }

    @ResponseBody
    @PostMapping("/update/season/time")
    @ApiOperation(value = "设置各季度时间")
    @RequiresRoles(RoleConstant.TEACHER)
    public R setSeasonTime(@RequestBody @NotNull @ApiParam(value = "contestId竞赛id") GbContestParam gbContest
    ){
        return gbContestBiz.setSeasonTime(gbContest.getContestId(),gbContest.getCountDown());
    }

    @ResponseBody
    @GetMapping("/get/restore/time")
    @ApiOperation(value = "获取可还原时间")
    @RequiresRoles(RoleConstant.TEACHER)
    public R getRestoreTime(@RequestParam @NotNull @ApiParam(value = "contestId竞赛id") Integer contestId,
                            @RequestParam @NotNull(message = "studentId不能为空") Integer studentId
    ){
        return gbContestBiz.getRestoreTime(contestId,studentId);
    }


    @ResponseBody
    @GetMapping("/get/public/info")
    @ApiOperation(value = "公共信息")
    @RequiresRoles(RoleConstant.TEACHER)
    public R getPublicInfo(@RequestParam @NotNull @ApiParam(value = "contestId竞赛id") Integer contestId,
                           @RequestParam @NotNull @ApiParam(value = "年份", required = true) Integer year
    ){
        return gbContestBiz.getPublicInfo(contestId,year);
    }


    @ResponseBody
    @PostMapping("/get/company/info")
    @ApiOperation(value = "企业信息")
    @RequiresRoles(RoleConstant.TEACHER)
    public R getCompanyInfo(@RequestBody  @Validated @ApiParam(value = "企业信息参数", required = true) GbCompanyParam gbCompanyParam){
        return gbContestBiz.getCompanyInfo(gbCompanyParam);
    }

    @ResponseBody
    @PostMapping("/send/announcement/info")
    @ApiOperation(value = "系统公告-下发公告信息")
    @RequiresRoles(RoleConstant.TEACHER)
    public R sendAnnouncement(@RequestBody  @Validated @ApiParam(value = "系统公告", required = true) GbCtAnnouncement gbCtAnnouncement) throws IOException {
        return gbContestBiz.addAnnouncement(gbCtAnnouncement);
    }

    @ResponseBody
    @GetMapping("/get/announcement/info")
    @ApiOperation(value = "系统公告-获取公告信息")
    public R getAnnouncement(@RequestParam @NotNull @ApiParam(value = "contestId竞赛id") Integer contestId){
        return gbContestBiz.getAnnouncement(contestId);
    }

    @ResponseBody
    @GetMapping("/generate/subject")
    @ApiOperation(value = "生成复盘数据")
    public void generateSubject(HttpServletResponse response,
                            @RequestParam @NotNull @ApiParam(value = "subjectNumber竞赛题目编号") Integer subjectNumber,
                            @RequestParam @NotNull @ApiParam(value = "contestId竞赛id") Integer contestId){
        gbContestBiz.generateSubject(response,contestId,subjectNumber);
    }













}

