package com.xiyou.main.controller.gbcontest;

import com.xiyou.common.controller.BaseController;
import com.xiyou.common.utils.R;
import com.xiyou.main.biz.gbcontest.GbDeliverAdsBiz;
import com.xiyou.main.constants.RoleConstant;
import com.xiyou.main.entity.gbcontest.GbCtMnAd;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.apache.shiro.authz.annotation.RequiresRoles;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotNull;

/**
 * @program: multi-module
 * @description: 投放广告
 * @author: wangxingyu
 * @create: 2023-06-27 15:01
 **/
@RestController
@RequestMapping("/tp/gbDeliverAds")
@Api(tags = "p37:投放广告")
@Validated
public class GbDeliverAdsController extends BaseController {
    @Autowired
    private GbDeliverAdsBiz gbDeliverAdsBiz;

    @ResponseBody
    @GetMapping("/market/yf/finish")
    @ApiOperation(value = "已开发完成的市场列表")
    @RequiresRoles(RoleConstant.STUDENT)
    public R finishYfMarket(@RequestParam @NotNull @ApiParam(value = "考试id", required = true) Integer contestId) {
        return gbDeliverAdsBiz.finishYfMarket(getUserId(), contestId);
    }

    @ResponseBody
    @PostMapping("/mnads/deliver")
    @ApiOperation(value = "投放广告")
    @RequiresRoles(RoleConstant.STUDENT)
    public R deliverAds(@RequestBody @Validated @ApiParam(value = "模拟广告", required = true) GbCtMnAd mnAd) {
        return gbDeliverAdsBiz.deliverAds(getUserId(), mnAd);
    }

    @ResponseBody
    @GetMapping("/mnads/getFillAd")
    @ApiOperation(value = "获取已填写的广告信息")
    @RequiresRoles(RoleConstant.STUDENT)
    public R getFillAd(Integer contestId,Integer currentTime) {
        return gbDeliverAdsBiz.getFillAd(contestId,getUserId(),currentTime);
    }



}
