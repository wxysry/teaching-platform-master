package com.xiyou.main.controller.gbcontest;

import com.xiyou.common.controller.BaseController;
import com.xiyou.main.biz.gbcontest.GbDownloadBiz;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.NotNull;
import java.io.IOException;

/**
 * @program: multi-module
 * @description: 文件下载
 * @author: tangcan
 * @create: 2019-08-28 14:21
 **/
@RestController
@RequestMapping("/tp/gbContest/download")
@Api(tags = "竞赛模拟-下载")
@Validated
public class GbDownloadController extends BaseController {
    @Autowired
    private GbDownloadBiz gbDownloadBiz;

    @ResponseBody
    @GetMapping("/ad")
    @ApiOperation(value = "下载其他组广告")
    public void ad(HttpServletResponse response,
                   @RequestParam @NotNull @ApiParam(value = "考试id") Integer contestId,
                   @RequestParam @NotNull @ApiParam(value = "学生id") Integer userId,
                   @RequestParam @NotNull @ApiParam(value = "当前时间") Integer date) {
        gbDownloadBiz.ad(response, userId, contestId, date);
    }


    @ResponseBody
    @GetMapping("/result/download")
    @ApiOperation(value = "下载学生竞赛模拟结果")
    public void downloadResult(HttpServletResponse response,
                               @RequestParam @ApiParam(value = "竞赛id", required = true) Integer contestId,
                               @RequestParam @ApiParam(value = "学生id", required = true) Integer studentId) {
        gbDownloadBiz.downloadResult(response, contestId, studentId,"竞赛结果");
    }


    @ResponseBody
    @GetMapping("/announcement/file")
    @ApiOperation(value = "公告信息-下载")
    public void allBalance(HttpServletResponse response,
                      @RequestParam @NotNull @ApiParam(value = "文件名") String fileName) throws IOException {
        gbDownloadBiz.downloadFile(response,fileName);
    }

    @ResponseBody
    @GetMapping("/all/file")
    @ApiOperation(value = "一键导出")
    public void downAllFile(HttpServletResponse response,
                            @RequestParam @NotNull @ApiParam(value = "文件名") Integer contestId) throws IOException {
        gbDownloadBiz.downAllFile(response,contestId);
    }

    @ResponseBody
    @GetMapping("/all/financial")
    @ApiOperation(value = "下载企业报表")
    public void downAllFinancial(HttpServletResponse response,
                                @RequestParam @ApiParam(value = "竞赛id", required = true) Integer contestId,
                                @RequestParam @ApiParam(value = "学生id", required = true) Integer year) throws IOException {
            gbDownloadBiz.downAllFinancial(response, contestId, year);
    }

}
