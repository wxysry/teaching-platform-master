package com.xiyou.main.controller.gbcontest;

import com.xiyou.common.annotations.ValidFile;
import com.xiyou.common.constants.FileType;
import com.xiyou.common.log.annotations.OptLog;
import com.xiyou.common.shiro.utils.ShiroUtil;
import com.xiyou.common.utils.R;
import com.xiyou.main.biz.gbcontest.GbGzBiz;
import com.xiyou.main.constants.RoleConstant;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.shiro.authz.annotation.RequiresRoles;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.NotNull;

/**
 * @author xingzi
 * @date 2019 07 22  21:14
 */
@RestController
@RequestMapping("/tp/gbGz")
@Api(tags = "新平台模拟模拟-题库规则表")
public class GbGzController {

    @Autowired
    private GbGzBiz gbGzBiz;

    @PostMapping("/upLoad")
    @ApiOperation(value = "规则表上传")
//    @RequiresRoles({RoleConstant.ADMIN,RoleConstant.TEACHER})
    @OptLog(description = "竞赛模拟题库规则表上传")
    public R upLoad(@RequestParam @NotNull(message = "题库号不能为空") Integer subjectNumber,
                    @RequestParam("file") @ValidFile(file = {FileType.XLS, FileType.XLSX}) MultipartFile multipartFile) {
        return gbGzBiz.upload(ShiroUtil.getUserId(),multipartFile, subjectNumber);
    }



    @GetMapping("/getRuleBySubjectNumber")
    @ApiOperation(value = "根据题库号获取规则表")
    @OptLog(description = "根据题库号获取规则表")
    public R getRuleBySubjectNumber(@RequestParam("subjectNumber")Integer subjectNumber){
        return gbGzBiz.getRuleBySubjectNumber(subjectNumber);
    }


}
