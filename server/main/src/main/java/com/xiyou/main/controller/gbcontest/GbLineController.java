package com.xiyou.main.controller.gbcontest;

import com.xiyou.common.controller.BaseController;
import com.xiyou.common.utils.R;
import com.xiyou.main.biz.gbcontest.GbLineBiz;
import com.xiyou.main.constants.RoleConstant;
import com.xiyou.main.entity.gbcontest.GbCtLine;
import com.xiyou.main.groups.Add;
import com.xiyou.main.params.gbcontest.GbLineParam;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.apache.shiro.authz.annotation.RequiresRoles;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotNull;

/**
 * @author dingyoumeng
 * @since 2019/07/22
 */
@RestController
@RequestMapping("/tp/gbContest/line")
@Api(tags = "p14生产线")
@Validated
public class GbLineController extends BaseController {

    @Autowired
    GbLineBiz lineBiz;

    @ResponseBody
    @GetMapping("/product/info")
    @ApiOperation(value = "产品信息获取")
    @RequiresRoles(RoleConstant.STUDENT)
    public R proInfo(@RequestParam @NotNull(message = "contestId不能为空") Integer contestId) {
        return lineBiz.proInfo(contestId);
    }

    @ResponseBody
    @GetMapping("/info")
    @ApiOperation(value = "设备管理-设备规则、线型、产品获取")
    @RequiresRoles(RoleConstant.STUDENT)
    public R info(@RequestParam @NotNull(message = "contestId不能为空") Integer contestId) {
        return lineBiz.info(getUserId(), contestId);
    }

    @ResponseBody
    @PostMapping("/add")
    @ApiOperation(value = "设备管理-新增")
    @RequiresRoles(RoleConstant.STUDENT)
    public R add(@RequestBody @Validated({Add.class}) @ApiParam(value = "生产线", required = true) GbCtLine line) {
        line.setStudentId(getUserId());
        return lineBiz.add(line);
    }

    @ResponseBody
    @GetMapping("/listAll")
    @ApiOperation(value = "设备管理-获取生产线列表 - 包含空闲、转产、在产、在建 各个状态的生产线")
    @RequiresRoles(RoleConstant.STUDENT)
    public R listAll(@RequestParam @NotNull(message = "contestId不能为空") Integer contestId) {
        return lineBiz.getAllLineList(contestId,getUserId());
    }


    @ResponseBody
    @PostMapping("/transfer")
    @ApiOperation(value = "设备管理-转产")
    @RequiresRoles(RoleConstant.STUDENT)
    public R lineTransfer(@RequestParam @NotNull(message = "竞赛id不能为空") Integer contestId,
                          @RequestParam @NotNull(message = "产线id") Integer lineId,
                          @RequestParam @NotNull(message = "产品id") Integer cpId,
                          @RequestParam @NotNull(message = "当前时间") Integer currentTime
                            ) {
        return lineBiz.lineTransfer(getUserId(), contestId,lineId,cpId,currentTime);
    }

    @ResponseBody
    @PostMapping("/sell")
    @ApiOperation(value = "设备管理-出售")
    @RequiresRoles(RoleConstant.STUDENT)
    public R lineSell(@RequestBody GbCtLine line) {
        return lineBiz.lineSell(getUserId(), line);
    }



    @ResponseBody
    @PostMapping("/producting")
    @ApiOperation(value = "设备管理-生产")
    @RequiresRoles(RoleConstant.STUDENT)
    public R producting(@RequestBody  GbCtLine line) throws Exception {
        return lineBiz.producting(getUserId(), line);
    }

    @ResponseBody
    @PostMapping("/save")
    @ApiOperation(value = "工人管理-生产线保存")
    @RequiresRoles(RoleConstant.STUDENT)
    public R producting(@RequestBody GbLineParam gbLineParam) {
        return lineBiz.saveWorkerToLine(getUserId(), gbLineParam);
    }




}
