package com.xiyou.main.controller.gbcontest;

import com.xiyou.common.controller.BaseController;
import com.xiyou.common.utils.R;
import com.xiyou.main.biz.gbcontest.GbNumBiz;
import com.xiyou.main.constants.RoleConstant;
import com.xiyou.main.params.gbcontest.GbNumParam;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.shiro.authz.annotation.RequiresRoles;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotNull;

/**
 * @program: multi-module
 * @description: 数字化
 * @author: tangcan
 * @create: 2019-09-01 12:42
 **/
@RestController
@RequestMapping("/tp/gbContest/num")
@Api(tags = "p30 数字化投资")
@Validated
public class GbNumController extends BaseController {
    @Autowired
    private GbNumBiz gbNumBiz;

    @ResponseBody
    @GetMapping("/list")
    @ApiOperation(value = "获取数字化列表")
    @RequiresRoles(RoleConstant.STUDENT)
    public R list(@RequestParam @NotNull(message = "contestId不能为空") Integer contestId) {
        return gbNumBiz.list(contestId, getUserId());
    }

    @ResponseBody
    @PostMapping("/commit")
    @ApiOperation(value = "确认数字化投资")
    @RequiresRoles(RoleConstant.STUDENT)
    public R commit(@RequestBody @Validated GbNumParam param) {
        param.setStudentId(getUserId());
        return gbNumBiz.commit(param);
    }
}
