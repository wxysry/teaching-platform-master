package com.xiyou.main.controller.gbcontest;

import com.xiyou.common.controller.BaseController;
import com.xiyou.common.utils.R;
import com.xiyou.main.biz.gbcontest.GbRestoreDataBiz;
import com.xiyou.main.constants.RoleConstant;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.shiro.authz.annotation.RequiresRoles;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotNull;

/**
 * @program: multi-module
 * @description: 数据还原
 * @author: tangcan
 * @create: 2019-08-31 21:05
 **/
@RestController
@RequestMapping("/tp/gbContest/data")
@Api(tags = "竞赛模拟-还原本年数据")
@Validated
public class GbRestoreDataController extends BaseController {
//    @Autowired
//    private GbRestoreDataBiz gbRestoreDataBiz;
//
//    @ResponseBody
//    @GetMapping("/restore/time")
//    @ApiOperation(value = "还原到某个时间节点的数据")
//    @RequiresRoles(RoleConstant.TEACHER)
//    public R restore(@RequestParam @NotNull(message = "contestId不能为空") Integer contestId,
//                     @RequestParam @NotNull(message = "studentId不能为空") Integer studentId,
//                     @RequestParam @NotNull(message = "currentTime不能为空") Integer currentTime,
//                     @RequestParam @NotNull(message = "restoreTime不能为空") Integer restoreTime) {
//        return gbRestoreDataBiz.restoreDataByTime(contestId,studentId,currentTime,restoreTime);
//    }
//
//
//
//    @ResponseBody
//    @GetMapping("/restore/current")
//    @ApiOperation(value = "还原本季")
//    @RequiresRoles(RoleConstant.TEACHER)
//    public R restoreCurrentSeasonByTeacher(@RequestParam @NotNull(message = "contestId不能为空") Integer contestId,
//                              @RequestParam @NotNull(message = "currentTime不能为空") Integer currentTime) {
//        return gbRestoreDataBiz.restoreCurrentRound(contestId,currentTime);
//    }

}
