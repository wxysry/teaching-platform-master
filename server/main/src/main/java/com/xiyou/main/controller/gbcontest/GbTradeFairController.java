package com.xiyou.main.controller.gbcontest;

import com.xiyou.common.controller.BaseController;
import com.xiyou.common.enums.CodeEnum;
import com.xiyou.common.utils.R;
import com.xiyou.main.biz.gbcontest.GbTradeFairBiz;
import com.xiyou.main.biz.newcontest.NewTradeFairBiz;
import com.xiyou.main.constants.RoleConstant;
import com.xiyou.main.params.contest.UserAndOrderParam;
import com.xiyou.main.params.gbcontest.GbCtMnChooseParam;
import com.xiyou.main.params.gbcontest.GbUserAndOrderParam;
import com.xiyou.main.params.newcontest.NewUserAndOrderParam;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.shiro.authz.annotation.RequiresRoles;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotNull;

/**
 * @program: multi-module
 * @description: 订货会
 * @author: tangcan
 * @create: 2019-07-26 10:04
 **/
@RestController
@RequestMapping("/tp/gbTradeFair")
@Api(tags = "p40:参加订货会")
@Validated
public class GbTradeFairController extends BaseController {
    @Autowired
    private GbTradeFairBiz gbTradeFairBiz;

    @ResponseBody
    @GetMapping("/getOrderDate")
    @ApiOperation(value = "获取可选单的时间")
    public R getOrderDate(@RequestParam @NotNull(message = "contestId不能为空") Integer contestId) {
        return gbTradeFairBiz.getOrderDate(contestId);
    }

    @ResponseBody
    @GetMapping("/getUserAndOrder")
    @ApiOperation(value = "获取用户和订单")
    public R getUserAndOrder(@RequestParam @NotNull(message = "contestId不能为空") Integer contestId,
                             @RequestParam @NotNull(message = "market不能为空")String market) {
        return gbTradeFairBiz.getUserAndOrder(contestId,getUserId(),market);
    }

    @ResponseBody
    @GetMapping("/order/choose")
    @ApiOperation(value = "选单")
    @RequiresRoles(RoleConstant.STUDENT)
    public R chooseOrder(@RequestParam @NotNull(message = "contestId不能为空") Integer contestId,
                         @RequestParam @NotNull(message = "market不能为空")String market,
                         @RequestParam @NotNull(message = "chooseId不能为空") Integer chooseId) {
        if (chooseId == null) {
            return R.error(CodeEnum.PARAM_ERROR.getCode(), "选单id不能为空");
        }
        return gbTradeFairBiz.chooseOrder(contestId,getUserId(),market,chooseId);
    }

    @ResponseBody
    @GetMapping("/order/give/up")
    @ApiOperation(value = "放弃选单")
    @RequiresRoles(RoleConstant.STUDENT)
    public R giveUpOrder(@RequestParam @NotNull(message = "contestId不能为空") Integer contestId,
                         @RequestParam @NotNull(message = "market不能为空")String market) {
        return gbTradeFairBiz.giveUpOrder(contestId,getUserId(),market,false);
    }

    @ResponseBody
    @GetMapping("/order/add")
    @ApiOperation(value = "临时添加订单")
    @RequiresRoles(RoleConstant.TEACHER)
    public R addOrder(@RequestParam @NotNull(message = "contestId不能为空") Integer contestId,
                      @RequestParam @NotNull(message = "stutentId不能为空") Integer studentId,
                      @RequestParam @NotNull(message = "chooseId不能为空") Integer chooseId) {
        if (chooseId == null) {
            return R.error(CodeEnum.PARAM_ERROR.getCode(), "选单id不能为空");
        }
        if (studentId == null) {
            return R.error(CodeEnum.PARAM_ERROR.getCode(), "学生id不能为空");
        }
        return gbTradeFairBiz.addOrder(contestId,studentId,chooseId);
    }




    @ResponseBody
    @GetMapping("/order/restore")
    @ApiOperation(value = "重新选单")
    @RequiresRoles(RoleConstant.TEACHER)
    public R restoreOrder(@RequestParam @NotNull(message = "contestId不能为空") Integer contestId,
                          @RequestParam @NotNull(message = "date不能为空") Integer date) {
        return gbTradeFairBiz.restoreOrder(contestId, date);
    }

    @ResponseBody
    @GetMapping("/start/trade")
    @ApiOperation(value = "开始订货会----开始投放广告")
    @RequiresRoles(RoleConstant.TEACHER)
    public R startTrade(@RequestParam @NotNull(message = "contestId不能为空") Integer contestId,
                          @RequestParam @NotNull(message = "date不能为空") Integer date,
                          @RequestParam @NotNull(message = "订货会状态不能为空") Integer fairOrderState) {
        return gbTradeFairBiz.startTrade(contestId,date,fairOrderState);
    }

    @ResponseBody
    @GetMapping("/pause/start/trade")
    @ApiOperation(value = "暂停--继续订货会")
    @RequiresRoles(RoleConstant.TEACHER)
    public R pauseOrContinue(@RequestParam @NotNull(message = "contestId不能为空") Integer contestId,
                        @RequestParam @NotNull(message = "订货会状态不能为空") Integer fairOrderState) {
        return gbTradeFairBiz.pauseOrContinue(contestId,fairOrderState);
    }

    @ResponseBody
    @PostMapping("/all/order")
    @ApiOperation(value = "选单明细")
    @RequiresRoles(RoleConstant.TEACHER)
    public R getMnOrder(@RequestBody GbCtMnChooseParam gbCtMnChooseParam) {
        return gbTradeFairBiz.getMnOrder(gbCtMnChooseParam);
    }

    @ResponseBody
    @GetMapping("/ad/list")
    @ApiOperation(value = "本季广告")
    @RequiresRoles(RoleConstant.TEACHER)
    public R adList(@RequestParam @NotNull(message = "contestId不能为空") Integer contestId,
                          @RequestParam @NotNull(message = "date不能为空") Integer date) {
        return gbTradeFairBiz.adList(contestId, date);
    }

    @ResponseBody
    @GetMapping("/get/choose/order")
    @ApiOperation(value = "获取当季订单")
    @RequiresRoles(RoleConstant.TEACHER)
    public R getChooseOrder(@RequestParam @NotNull(message = "contestId不能为空") Integer contestId,
                    @RequestParam @NotNull(message = "date不能为空") Integer date) {
        return gbTradeFairBiz.getChooseOrder(contestId, date);
    }


//    @ResponseBody
//    @GetMapping("/check/order/choose")
//    @ApiOperation(value = "校验是否可以选单")
//    @RequiresRoles(RoleConstant.STUDENT)
//    public R checkChooseOrder(@RequestParam @NotNull(message = "contestId不能为空") Integer contestId,
//                              @RequestParam @NotNull(message = "studentId不能为空") Integer studentId,
//                              @RequestParam @NotNull(message = "date不能为空") Integer currentTime) {
//        return gbTradeFairBiz.checkChooseOrder(getUserId(), contestId, studentId,currentTime);
//    }
}
