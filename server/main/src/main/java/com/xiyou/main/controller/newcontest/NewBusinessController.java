package com.xiyou.main.controller.newcontest;

import com.xiyou.common.controller.BaseController;
import com.xiyou.common.utils.R;
import com.xiyou.main.biz.newcontest.NewBusinessBiz;
import com.xiyou.main.constants.RoleConstant;
import com.xiyou.main.entity.contest.ContestStudent;
import com.xiyou.main.entity.newcontest.NewContestStudent;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.apache.shiro.authz.annotation.RequiresRoles;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * @program: multi-module
 * @description: 经营
 * @author: wangxingyu
 * @create: 2023-06-09
 **/
@RestController
@RequestMapping("/tp/newBusiness")
@Api(tags = "p0-经营")
@Validated
public class NewBusinessController extends BaseController {
    @Autowired
    private NewBusinessBiz newBusinessBiz;

    @ResponseBody
    @GetMapping("/start")
    @ApiOperation(value = "开始经营")
    @RequiresRoles(RoleConstant.STUDENT)
    public R start(@RequestParam @NotNull @ApiParam(value = "考试id", required = true) Integer contestId) {
        return newBusinessBiz.start(getUserId(), contestId);
    }

    @ResponseBody
    @GetMapping("/restart")
    @ApiOperation(value = "重新经营-学生操作")
    @RequiresRoles(RoleConstant.STUDENT)
    public R restart(@RequestParam @NotNull @ApiParam(value = "考试id", required = true) Integer contestId) {
        return newBusinessBiz.restart(getUserId(), contestId);
    }

    @ResponseBody
    @GetMapping("/restart/teacher")
    @ApiOperation(value = "重新经营-教师操作")
    @RequiresRoles(RoleConstant.TEACHER)
    public R restart(@RequestParam @NotNull @ApiParam(value = "考试id", required = true) Integer contestId,
                     @RequestParam @NotNull @ApiParam(value = "学生id", required = true) Integer studentId) {
        return newBusinessBiz.restart(studentId, contestId);
    }

    @ResponseBody
    @GetMapping("/progress/save")
    @ApiOperation(value = "保存经营进度-学生")
    @RequiresRoles(RoleConstant.STUDENT)
    public R saveProgress(@RequestParam @NotNull @ApiParam(value = "考试id", required = true) Integer contestId,
                          @RequestParam @NotNull @ApiParam(value = "经营时间进度", required = true) Integer date,
                          @RequestParam @NotBlank @ApiParam(value = "经营进度", required = true) String progress) {
        return newBusinessBiz.saveProgress(getUserId(), contestId, date, progress);
    }

    @ResponseBody
    @GetMapping("/progress/save/teacher")
    @ApiOperation(value = "保存经营进度-教师")
    @RequiresRoles(RoleConstant.TEACHER)
    public R saveProgressByTeacher(@RequestParam @NotNull @ApiParam(value = "考试id", required = true) Integer contestId,
                                   @RequestParam @NotNull @ApiParam(value = "学生id", required = true) Integer studentId,
                                   @RequestParam @NotNull @ApiParam(value = "经营时间进度", required = true) Integer date,
                                   @RequestParam @NotBlank @ApiParam(value = "经营进度", required = true) String progress) {
        return newBusinessBiz.saveProgress(studentId, contestId, date, progress);
    }

    @ResponseBody
    @GetMapping("/progress/get")
    @ApiOperation(value = "获取经营进度")
    @RequiresRoles(RoleConstant.STUDENT)
    public R getProgress(@RequestParam @NotNull @ApiParam(value = "考试id", required = true) Integer contestId) {
        return newBusinessBiz.getProgress(getUserId(), contestId);
    }

    @ResponseBody
    @PostMapping("/xzorder/save")
    @ApiOperation(value = "保存选单进度-学生")
    @RequiresRoles(RoleConstant.STUDENT)
    public R saveXzorder(@RequestBody @Validated NewContestStudent newContestStudent) {
        newContestStudent.setStudentId(getUserId());
        return newBusinessBiz.saveXzorder(newContestStudent);
    }

    @ResponseBody
    @PostMapping("/xzorder/save/teacher")
    @ApiOperation(value = "保存选单进度-教师")
    @RequiresRoles(RoleConstant.TEACHER)
    public R saveXzorderByTeacher(@RequestBody @Validated NewContestStudent newContestStudent) {
        return newBusinessBiz.saveXzorder(newContestStudent);
    }

    @ResponseBody
    @GetMapping("/xzorder/get")
    @ApiOperation(value = "获取选单进度")
    @RequiresRoles(RoleConstant.STUDENT)
    public R getXzorder(@RequestParam @NotNull @ApiParam(value = "考试id", required = true) Integer contestId) {
        return newBusinessBiz.getXzorder(getUserId(), contestId);
    }

    @ResponseBody
    @GetMapping("/end")
    @ApiOperation(value = "结束经营")
    @RequiresRoles(RoleConstant.STUDENT)
    public R end(@RequestParam @NotNull @ApiParam(value = "考试id", required = true) Integer contestId) {
        return newBusinessBiz.end(getUserId(), contestId);
    }

}
