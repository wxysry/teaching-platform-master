package com.xiyou.main.controller.newcontest;


import com.xiyou.common.controller.BaseController;
import com.xiyou.common.log.annotations.OptLog;
import com.xiyou.common.utils.R;
import com.xiyou.main.biz.newcontest.NewContestBiz;
import com.xiyou.main.constants.RoleConstant;
import com.xiyou.main.entity.newcontest.NewContest;
import com.xiyou.main.params.contest.ContestParam;
import com.xiyou.main.params.contest.ContestStudentParam;
import com.xiyou.main.params.newcontest.DigitalizeParam;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.apache.shiro.authz.annotation.RequiresRoles;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.NotNull;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author tangcan
 * @since 2019-08-25
 */
@RestController
@RequestMapping("/tp/newContest")
@Api(tags = "新平台模拟模拟")
@Validated
public class NewContestController extends BaseController {
    @Autowired
    private NewContestBiz newContestBiz;


    @ResponseBody
    @PostMapping("/list/teacher")
    @ApiOperation(value = "教师获取竞赛模拟列表")
    @RequiresRoles(RoleConstant.TEACHER)
    public R listByTeacher(@RequestBody @Validated @ApiParam(value = "竞赛查询参数", required = true) ContestParam contestParam) {
        contestParam.setTeacherId(getUserId());
        return newContestBiz.listByTeacher(contestParam);
    }

    @ResponseBody
    @PostMapping("/student/list")
    @ApiOperation(value = "获取竞赛学生列表")
    @RequiresRoles(RoleConstant.TEACHER)
    public R studentList(@RequestBody @Validated @ApiParam(value = "竞赛查询参数", required = true) ContestStudentParam contestStudentParam) {
        contestStudentParam.setTeacherId(getUserId());
        return newContestBiz.studentList(contestStudentParam);
    }

    @ResponseBody
    @GetMapping("/student/score")
    @ApiOperation(value = "设置学生成绩")
    @RequiresRoles(RoleConstant.TEACHER)
    public R studentScore(@RequestParam @NotNull @ApiParam(value = "列表中的id") Integer id,
                          @RequestParam @NotNull(message = "成绩不能为空") @ApiParam(value = "成绩") Double score) {
        return newContestBiz.studentScore(id, score);
    }

    @ResponseBody
    @PostMapping("/list/student")
    @ApiOperation(value = "学生获取竞赛模拟列表")
    @RequiresRoles(RoleConstant.STUDENT)
    public R listByStudent(@RequestBody @Validated @ApiParam(value = "竞赛查询参数", required = true) ContestParam contestParam) {
        contestParam.setStudentId(getUserId());
        return newContestBiz.listByStudent(contestParam);
    }

    @ResponseBody
    @GetMapping("/get")
    @ApiOperation(value = "获取竞赛基本信息")
    @RequiresRoles(RoleConstant.TEACHER)
    public R get(@RequestParam @NotNull Integer contestId) {
        return newContestBiz.get(contestId);
    }

    @ResponseBody
    @GetMapping("/equity/get")
    @ApiOperation(value = "获取初始权益")
    @RequiresRoles(RoleConstant.TEACHER)
    public R getEquity(@RequestParam @NotNull @ApiParam(name = "题库号") Integer subjectNumber) {
        return newContestBiz.getEquity(subjectNumber);
    }

    @ResponseBody
    @PostMapping("/add")
    @ApiOperation(value = "添加竞赛")
    @RequiresRoles(RoleConstant.TEACHER)
    @OptLog(description = "添加竞赛")
    public R add(@RequestBody @Validated NewContest newContest) {
        newContest.setTeacherId(getUserId());
        return newContestBiz.add(newContest);
    }

    @ResponseBody
    @PostMapping("/update")
    @ApiOperation(value = "更新竞赛基本信息")
    @RequiresRoles(RoleConstant.TEACHER)
    public R update(@RequestBody @Validated NewContest newContest) {
        newContest.setTeacherId(getUserId());
        return newContestBiz.update(newContest);
    }

    @ResponseBody
    @GetMapping("/publish")
    @ApiOperation(value = "发布竞赛", notes = "只能同时发布一个竞赛")
    @RequiresRoles(RoleConstant.TEACHER)
    public R publish(@RequestParam @NotNull Integer contestId) {
        return newContestBiz.publish(getUserId(), contestId);
    }

    @ResponseBody
    @GetMapping("/delete")
    @ApiOperation(value = "删除竞赛")
    @RequiresRoles(RoleConstant.TEACHER)
    @OptLog(description = "删除竞赛")
    public R delete(@RequestParam @NotNull Integer contestId) {
        return newContestBiz.delete(contestId);
    }

    @ResponseBody
    @GetMapping("/attachment/spy/get")
    @ApiOperation(value = "获取间谍附件")
    public R getSpyAttachment(@RequestParam @ApiParam(value = "竞赛id", required = true) Integer contestId,
                              @RequestParam @ApiParam(value = "年份", required = true) Integer year) {
        return newContestBiz.getSpyAttachment(contestId, year);
    }

    @ResponseBody
    @GetMapping("/attachment/rule/get")
    @ApiOperation(value = "获取规则说明附件")
    public R getRuleAttachment(@RequestParam @ApiParam(value = "竞赛id", required = true) Integer contestId) {
        return newContestBiz.getRuleAttachment(contestId);
    }

    @ResponseBody
    @GetMapping("/score/download")
    @ApiOperation(value = "竞赛成绩下载")
    public void downloadScore(HttpServletResponse response,
                              @RequestParam @NotNull @ApiParam(value = "contestId", required = true) Integer contestId) {
        newContestBiz.downloadScore(contestId, response);
    }


    @ResponseBody
    @PostMapping("/start/digitalize")
    @ApiOperation(value = "数字化开启智能生产")
    public R digitalizeStart(@RequestBody @NotNull @Validated DigitalizeParam param) {
        return newContestBiz.digitalizeStart(getUserId(), param);
    }
}

