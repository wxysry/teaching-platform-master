package com.xiyou.main.controller.newcontest;


import com.xiyou.common.controller.BaseController;
import com.xiyou.common.utils.R;
import com.xiyou.main.biz.newcontest.NewContestGroupBiz;
import com.xiyou.main.constants.RoleConstant;
import com.xiyou.main.entity.newcontest.NewContestGroup;
import com.xiyou.main.groups.Add;
import com.xiyou.main.groups.Update;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.apache.shiro.authz.annotation.RequiresRoles;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotNull;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author tangcan
 * @since 2019-08-25
 */
@RestController
@RequestMapping("/tp/newContestGroup")
@Api(tags = "新平台模拟模拟-新平台模拟分组")
@Validated
public class NewContestGroupController extends BaseController {
    @Autowired
    private NewContestGroupBiz newContestGroupBiz;

    @ResponseBody
    @GetMapping("/list")
    @ApiOperation(value = "管理员获取所有新平台模拟分组的列表")
    @RequiresRoles(RoleConstant.ADMIN)
    public R list() {
        return newContestGroupBiz.list();
    }

    @ResponseBody
    @GetMapping("/listByteacher")
    @ApiOperation(value = "教师获取新平台模拟分组的列表")
    @RequiresRoles(RoleConstant.TEACHER)
    public R listByteacher() {
        return newContestGroupBiz.listByteacher(getUserId());
    }

    @ResponseBody
    @PostMapping("/add")
    @ApiOperation(value = "添加分组")
    @RequiresRoles(RoleConstant.ADMIN)
    public R add(@RequestBody @Validated({Add.class}) @ApiParam(value = "分组信息", required = true) NewContestGroup newContestGroup) {
        return newContestGroupBiz.add(newContestGroup);
    }

    @ResponseBody
    @PostMapping("/update")
    @ApiOperation(value = "更新分组信息")
    @RequiresRoles(RoleConstant.ADMIN)
    public R update(@RequestBody @Validated({Update.class}) @ApiParam(value = "分组信息", required = true) NewContestGroup newContestGroup) {
        return newContestGroupBiz.update(newContestGroup);
    }

    @ResponseBody
    @GetMapping("/delete/{id}")
    @ApiOperation(value = "删除新平台模拟分组", notes = "会删去分组下的视频教程(传多个id时，为批量删除，中间逗号隔开)")
    @RequiresRoles(RoleConstant.ADMIN)
    public R delete(@PathVariable(value = "id") @NotNull @ApiParam(value = "ids", required = true) Integer[] ids) {
        return newContestGroupBiz.delete(ids);
    }

}

