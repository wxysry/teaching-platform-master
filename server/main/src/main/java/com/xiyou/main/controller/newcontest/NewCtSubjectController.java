package com.xiyou.main.controller.newcontest;


import com.xiyou.common.controller.BaseController;
import com.xiyou.common.log.annotations.OptLog;
import com.xiyou.common.utils.R;
import com.xiyou.main.biz.newcontest.NewCtSubjectBiz;
import com.xiyou.main.constants.RoleConstant;
import com.xiyou.main.entity.newcontest.NewCtSubject;
import com.xiyou.main.params.contest.SubjectParam;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.apache.shiro.authz.annotation.RequiresRoles;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotNull;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author tangcan
 * @since 2019-08-25
 */
@RestController
@RequestMapping("/tp/newCtSubject")
@Api(tags = "新平台模拟-题库管理")
@Validated
public class NewCtSubjectController extends BaseController {
    @Autowired
    private NewCtSubjectBiz newCtSubjectBiz;

    @ResponseBody
    @PostMapping("/list/admin")
    @ApiOperation(value = "管理员获取题库列表")
    @RequiresRoles(RoleConstant.ADMIN)
    public R listByAdmin(@RequestBody @Validated @ApiParam(value = "题库查询参数", required = true) SubjectParam subjectParam) {
        return newCtSubjectBiz.listByAdmin(subjectParam);
    }

    @ResponseBody
    @GetMapping("/cascade/teacher")
    @ApiOperation(value = "教师获取题库级联")
    @RequiresRoles(RoleConstant.TEACHER)
    public R cascadeByTeacher() {
        return newCtSubjectBiz.cascadeByTeacher(getUserId());
    }

    @ResponseBody
    @GetMapping("/list/teacher")
    @ApiOperation(value = "教师获取题库列表")
    @RequiresRoles(RoleConstant.TEACHER)
    public R listByTeacher(@RequestParam @ApiParam(value = "分组id", required = true) Integer groupId) {
        return newCtSubjectBiz.listByTeacher(groupId);
    }

    @ResponseBody
    @GetMapping("/get")
    @ApiOperation(value = "获取题库基本信息")
    @RequiresRoles(RoleConstant.ADMIN)
    public R get(@RequestParam @NotNull Integer subjectId) {
        return newCtSubjectBiz.get(subjectId);
    }

    @ResponseBody
    @PostMapping("/add")
    @ApiOperation(value = "添加题库")
    @RequiresRoles(RoleConstant.ADMIN)
    @OptLog(description = "添加竞赛模拟题库")
    public R add(@RequestBody @Validated @ApiParam(value = "题库信息", required = true) NewCtSubject newCtSubject) {
        return newCtSubjectBiz.add(newCtSubject);
    }

    @ResponseBody
    @PostMapping("/update")
    @ApiOperation(value = "更新题库基本信息")
    @RequiresRoles(RoleConstant.ADMIN)
    public R update(@RequestBody @Validated @ApiParam(value = "题库信息", required = true) NewCtSubject newCtSubject) {
        return newCtSubjectBiz.update(newCtSubject);
    }

    @ResponseBody
    @GetMapping("/delete")
    @ApiOperation(value = "删除题库")
    @RequiresRoles(RoleConstant.ADMIN)
    @OptLog(description = "删除竞赛模拟题库")
    public R delete(@RequestParam @NotNull @ApiParam(value = "题库id", required = true) Integer subjectId) {
        return newCtSubjectBiz.delete(subjectId);
    }
}

