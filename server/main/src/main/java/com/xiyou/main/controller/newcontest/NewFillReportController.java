package com.xiyou.main.controller.newcontest;

import com.xiyou.common.controller.BaseController;
import com.xiyou.common.utils.R;
import com.xiyou.main.biz.newcontest.NewFillReportBiz;
import com.xiyou.main.constants.RoleConstant;
import com.xiyou.main.entity.contest.CtBalance;
import com.xiyou.main.entity.contest.CtCharges;
import com.xiyou.main.entity.contest.CtProfitChart;
import com.xiyou.main.entity.newcontest.NewCtBalance;
import com.xiyou.main.entity.newcontest.NewCtCharges;
import com.xiyou.main.entity.newcontest.NewCtFinancialTarget;
import com.xiyou.main.entity.newcontest.NewCtProfitChart;
import com.xiyou.main.params.gbcontest.GbFillReportParam;
import com.xiyou.main.params.newcontest.NewFillReportParam;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.apache.shiro.authz.annotation.RequiresRoles;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotNull;

/**
 * @program: multi-module
 * @description: 综合费用表
 * @author: wangxingyu
 * @create: 2023-06-28 21:34
 **/
@RestController
@RequestMapping("/tp/newFillReport")
@Api(tags = "p36:填写报表")
@Validated
public class NewFillReportController extends BaseController {
    @Autowired
    private NewFillReportBiz newFillReportBiz;

    @ResponseBody
    @PostMapping("/batch/fill")
    @ApiOperation(value = "填写报表")
    @RequiresRoles(RoleConstant.STUDENT)
    public R batchFill(@RequestBody @Validated @ApiParam(value = "财务指标表", required = true) NewFillReportParam newFillReportParam) {
        return newFillReportBiz.batchFill(getUserId(), newFillReportParam);
    }



    @ResponseBody
    @GetMapping("/getTemp")
    @ApiOperation(value = "获取暂存数据")
    @RequiresRoles(RoleConstant.STUDENT)
    public R getTemp(Integer contestId,Integer year) {
        return newFillReportBiz.getTemp(getUserId(),contestId,year);
    }

    @ResponseBody
    @GetMapping("/getAutoFill")
    @ApiOperation(value = "获取系统自动生成的数据")
    @RequiresRoles(RoleConstant.STUDENT)
    public R getAutoFill(Integer contestId,Integer year) {
        return newFillReportBiz.getAutoFill(getUserId(),contestId,year);
    }



}
