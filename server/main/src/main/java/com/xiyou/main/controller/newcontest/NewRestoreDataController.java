package com.xiyou.main.controller.newcontest;

import com.xiyou.common.controller.BaseController;
import com.xiyou.common.utils.R;
import com.xiyou.main.biz.newcontest.NewRestoreDataBiz;
import com.xiyou.main.constants.RoleConstant;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.shiro.authz.annotation.RequiresRoles;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotNull;

/**
 * @program: multi-module
 * @description: 数据还原
 * @author: tangcan
 * @create: 2019-08-31 21:05
 **/
@RestController
@RequestMapping("/tp/newContest/data")
@Api(tags = "竞赛模拟-还原本年数据")
@Validated
public class NewRestoreDataController extends BaseController {
    @Autowired
    private NewRestoreDataBiz newRestoreDataBiz;

    @ResponseBody
    @GetMapping("/restore")
    @ApiOperation(value = "还原本年数据到第一季度-学生操作")
    @RequiresRoles(RoleConstant.STUDENT)
    public R restore(@RequestParam @NotNull(message = "contestId不能为空") Integer contestId) {
        return newRestoreDataBiz.restore(contestId, getUserId());
    }

    @ResponseBody
    @GetMapping("/restore/teacher")
    @ApiOperation(value = "还原本年数据到第一季度-教师操作")
    @RequiresRoles(RoleConstant.TEACHER)
    public R restoreByTeacher(@RequestParam @NotNull(message = "contestId不能为空") Integer contestId,
                              @RequestParam @NotNull(message = "studentId不能为空") Integer studentId) {
        return newRestoreDataBiz.restore(contestId, studentId);
    }

    @ResponseBody
    @ApiOperation(value = "还原当季-教师操作")
    @RequiresRoles(RoleConstant.TEACHER)
    @GetMapping("/restore/restoreSeasonByTeacher")
    public R restoreSeasonByTeacher(@RequestParam @NotNull(message = "contestId不能为空") Integer contestId,
                                    @RequestParam @NotNull(message = "studentId不能为空") Integer studentId) {
        return newRestoreDataBiz.restoreSeason(contestId,studentId);
    }

    @ResponseBody
    @ApiOperation(value = "还原当季")
    @GetMapping("/restore/restoreSeason")
    public R restoreSeason(Integer contestId) {
        return newRestoreDataBiz.restoreSeason(contestId,getUserId());
    }


}
