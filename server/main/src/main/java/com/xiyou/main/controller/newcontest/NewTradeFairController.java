package com.xiyou.main.controller.newcontest;

import com.xiyou.common.controller.BaseController;
import com.xiyou.common.enums.CodeEnum;
import com.xiyou.common.utils.R;
import com.xiyou.main.biz.newcontest.NewTradeFairBiz;
import com.xiyou.main.constants.RoleConstant;
import com.xiyou.main.entity.newcontest.NewCtMnChoose;
import com.xiyou.main.params.newcontest.NewUserAndOrderParam;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.authz.annotation.RequiresRoles;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import javax.validation.constraints.NotNull;

/**
 * @program: multi-module
 * @description: 订货会
 * @author: tangcan
 * @create: 2019-07-26 10:04
 **/
@RestController
@RequestMapping("/tp/newTradeFair")
@Api(tags = "p40:参加订货会")
@Validated
public class NewTradeFairController extends BaseController {
    @Autowired
    private NewTradeFairBiz newTradeFairBiz;

    @ResponseBody
    @PostMapping("/getUserAndOrder")
    @ApiOperation(value = "获取用户和订单")
    @RequiresRoles(RoleConstant.STUDENT)
    public R getUserAndOrder(@RequestBody @Validated NewUserAndOrderParam userAndOrderParam) {
        userAndOrderParam.setStudentId(getUserId());
        userAndOrderParam.setYear(userAndOrderParam.getDate() / 10);
        return newTradeFairBiz.getUserAndOrder(userAndOrderParam);
    }

    @ResponseBody
    @PostMapping("/order/choose")
    @ApiOperation(value = "选单")
    @RequiresRoles(RoleConstant.STUDENT)
    public R chooseOrder(@RequestBody @Validated NewUserAndOrderParam userAndOrderParam) {
        if (userAndOrderParam.getChooseId() == null) {
            return R.error(CodeEnum.PARAM_ERROR.getCode(), "选单id不能为空");
        }
        userAndOrderParam.setStudentId(getUserId());
        userAndOrderParam.setYear(userAndOrderParam.getDate() / 10);
        return newTradeFairBiz.chooseOrder(userAndOrderParam);
    }



    @ResponseBody
    @PostMapping("/order/add")
    @ApiOperation(value = "补单")
    @RequiresRoles(RoleConstant.TEACHER)
    public R addOrder(@RequestBody @Validated NewCtMnChoose newCtMnChoose) {
        if (StringUtils.isEmpty(newCtMnChoose.getCoId())) {
            return R.error(CodeEnum.PARAM_ERROR.getCode(), "订单编号不能为空");
        }
        return newTradeFairBiz.addOrder(newCtMnChoose);
    }


    @ResponseBody
    @GetMapping("/end")
    @ApiOperation(value = "结束订货会，备份数据")
    @RequiresRoles(RoleConstant.STUDENT)
    public R end(@RequestParam @NotNull(message = "contestId不能为空") Integer contestId) {
        return newTradeFairBiz.end(contestId, getUserId());
    }

    @ResponseBody
    @GetMapping("/order/restore")
    @ApiOperation(value = "重新选单")
    @RequiresRoles(RoleConstant.STUDENT)
    public R restoreOrder(@RequestParam @NotNull(message = "contestId不能为空") Integer contestId,
                          @RequestParam @NotNull(message = "date不能为空") Integer date) {
        return newTradeFairBiz.restoreOrder(getUserId(), contestId, date);
    }
}
