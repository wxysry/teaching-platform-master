package com.xiyou.main.controller.newcontest;

import com.xiyou.common.controller.BaseController;
import com.xiyou.common.utils.R;
import com.xiyou.main.biz.newcontest.NewYearEndBiz;
import com.xiyou.main.constants.RoleConstant;
import com.xiyou.main.vo.newcontest.NewCashFolowEntity;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.apache.shiro.authz.annotation.RequiresRoles;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotNull;

/**
 * @program: multi-module
 * @description: 当年结束后
 * @author: tangcan
 * @create: 2019-07-22 16:04
 **/
@RestController
@RequestMapping("/tp/newYearEnd")
@Api(tags = "p31~34:当年结束后")
@Validated
public class NewYearEndController extends BaseController {

    @Autowired
    private NewYearEndBiz yearEndBiz;

    @ResponseBody
    @GetMapping("/confirm")
    @ApiOperation(value = "p31:确认当年结束")
    @RequiresRoles(RoleConstant.STUDENT)
    public R confirm(@RequestParam @NotNull @ApiParam(value = "考试id", required = true) Integer contestId,
                        @RequestParam @NotNull @ApiParam(value = "当前时间", required = true) Integer date) {
        return yearEndBiz.confirm(getUserId(), contestId, date);
    }


    @ApiOperation(value = "开始下一个季度")
    @GetMapping("/nextSeason")
    @RequiresRoles(RoleConstant.STUDENT)
    public R currentQuarterInit(@RequestParam @NotNull @ApiParam(value = "竞赛id") Integer contestId,
                                @RequestParam @NotNull @ApiParam(value = "当前时间") Integer currentTime) {
        NewCashFolowEntity cashFolowEntity = new NewCashFolowEntity()
                .setCurrentTime(currentTime)
                .setCurrentYear(currentTime / 10)
                .setStudentId(getUserId())
                .setContestId(contestId);
        return yearEndBiz.currentQuarterNext(cashFolowEntity);
    }

}
