package com.xiyou.main.controller.newcontest;

import com.xiyou.common.controller.BaseController;
import com.xiyou.common.log.annotations.OptLog;
import com.xiyou.common.utils.R;
import com.xiyou.main.biz.contest.*;
import com.xiyou.main.biz.newcontest.NewApplyForLoanBiz;
import com.xiyou.main.biz.newcontest.NewCurrentQuarterBiz;
import com.xiyou.main.biz.newcontest.NewStudentFinanceBiz;
import com.xiyou.main.constants.RoleConstant;
import com.xiyou.main.entity.newcontest.NewCtWorker;
import com.xiyou.main.params.contest.MaterialParam;
import com.xiyou.main.vo.contest.CashFolowEntity;
import com.xiyou.main.vo.newcontest.NewCashFolowEntity;
import com.xiyou.main.vo.newcontest.NewMaterialVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.apache.shiro.authz.annotation.RequiresRoles;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotNull;

/**
 * @author wangxingyu
 * @since 2023-06-11
 * @description: 财务信息统计
 **/
@RestController
@RequestMapping("/tp/newFinance")
@Validated
@Api(tags = "p3-财务信息&综合财务信息")
public class NewZFinanceController extends BaseController {

    @Autowired
    private NewStudentFinanceBiz studentFinanceBiz;
    @Autowired
    private NewApplyForLoanBiz newApplyForLoanBiz;

    @ApiOperation(value = "p3财务信息&综合财务信息查询")
    @GetMapping("/statistical/info")
    @RequiresRoles(RoleConstant.STUDENT)
    public R getStudentFinanceInfor(@RequestParam @NotNull @ApiParam(value = "竞赛id") Integer contestId,
                                    @RequestParam @NotNull @ApiParam(value = "当前时间") Integer currentTime) {

        NewCashFolowEntity cashFolowEntity = new NewCashFolowEntity();
        cashFolowEntity.setStudentId(getUserId());
        cashFolowEntity.setContestId(contestId);
        // 当前时间
        cashFolowEntity.setCurrentTime(currentTime);
        // 当前时间
        cashFolowEntity.setCurrentYear(currentTime / 10);

        return studentFinanceBiz.getStudentFinanceInfo(cashFolowEntity);
    }

    @ApiOperation(value = "P4研发认证信息")
    @GetMapping("/researchAndAuth/info")
    @RequiresRoles(RoleConstant.STUDENT)
    public R researchAndAuthenInfo(@RequestParam @ApiParam(value = "竞赛id") Integer contestId) {

        return studentFinanceBiz.getResearchAndDevelopeInfo(getUserId(), contestId);
    }

    @ApiOperation(value = "P5库存采购信息")
    @GetMapping("/stockPurchase/info")
    @RequiresRoles(RoleConstant.STUDENT)
    public R getStockPurchaseInfo(@RequestParam @ApiParam(value = "竞赛id") Integer contestId, @RequestParam @ApiParam(value = "当前时间") Integer currentTime) {
        return studentFinanceBiz.stockPurchaseInfo(getUserId(), contestId, currentTime);
    }


    @ApiOperation(value = "P8申请贷款-获取最大贷款额度")
    @GetMapping("/loan/info")
    @RequiresRoles(RoleConstant.STUDENT)
    public R applyForLongLoan(@RequestParam @ApiParam(value = "竞赛id") Integer contestId,
                              @RequestParam @ApiParam(value = "当前时间") Integer currentTime) {
        NewCashFolowEntity cashFolowEntity = new NewCashFolowEntity()
                .setStudentId(getUserId())
                .setContestId(contestId)
                .setCurrentTime(currentTime)
                .setCurrentYear(currentTime / 10);
        cashFolowEntity.setBlType(1);
        return newApplyForLoanBiz.getMaxLoanAmountInfo(cashFolowEntity);
    }

    @ApiOperation(value = "P8申请贷款-融资规则")
    @GetMapping("/loan/gz/info")
    @RequiresRoles(RoleConstant.STUDENT)
    public R applyForLongLoanGz(@RequestParam @ApiParam(value = "竞赛id") Integer contestId) {
        return newApplyForLoanBiz.getLoanGz(contestId);
    }

    @ApiOperation(value = "P8申请贷款-融资信息")
    @GetMapping("/loan/bank/info")
    @RequiresRoles(RoleConstant.STUDENT)
    public R applyForLongLoanBank(@RequestParam @ApiParam(value = "竞赛id") Integer contestId) {
        return newApplyForLoanBiz.getLoanBankInfo(contestId,getUserId());
    }

    @ApiOperation(value = "P8申请贷款-确认")
    @PostMapping("/loan/commit")
    @RequiresRoles(RoleConstant.STUDENT)
    public R applyForLongLoanCommit(@RequestBody NewCashFolowEntity cashFolowEntity) {

        cashFolowEntity.setStudentId(getUserId());
        cashFolowEntity.setCurrentYear(cashFolowEntity.getCurrentTime() / 10);
        return newApplyForLoanBiz.applyForLoanCommit(cashFolowEntity);
    }



    @ApiOperation(value = "产线信息")
    @GetMapping("/workshop/info")
    @RequiresRoles(RoleConstant.STUDENT)
    public R getWorkshopInfo(@RequestParam @ApiParam(value = "竞赛id") Integer contestId,
                             @RequestParam @ApiParam(value = "当前时间") Integer currentTime) {
        return newApplyForLoanBiz.getLineGroupInfo(getUserId(),contestId,currentTime);
    }



    @GetMapping("/feeManageData/info")
    @ApiOperation(value = "当季开始-获取费用")
    @OptLog(description = "当季开始-获取费用")
    public R startCurrSeason(@RequestParam("contestId")Integer contestId,
                             @RequestParam("currentTime")Integer currentTime){
        return newApplyForLoanBiz.startCurrSeason(currentTime,contestId,getUserId());
    }

    @GetMapping("/payFee")
    @ApiOperation(value = "当季开始-缴费")
    @OptLog(description = "当季开始-缴费")
    public R payCurrSeason(@RequestParam("contestId")Integer contestId,
                           @RequestParam("currentTime")Integer currentTime,
                           @RequestParam("blAddTime")Integer blAddTime,
                           @RequestParam("payment")String payment,
                           @RequestParam("amount")Integer amount){
        //前端需要传输当前时间节点和借款时间节点和还款类型
        return newApplyForLoanBiz.payCurrSeason(contestId,getUserId(),currentTime,payment,blAddTime,amount);
    }


    @GetMapping("/wokerMarket/list")
    @ApiOperation(value = "招聘管理-人力资源市场-数据")
    @OptLog(description = "招聘管理-人力资源市场-数据")
    public R getWorkerList(@RequestParam("contestId")Integer contestId,
                           @RequestParam("currentTime")Integer currentTime){
        //前端需要传输当前时间节点和借款时间节点和还款类型
        return newApplyForLoanBiz.getWorkerList(contestId,getUserId(),currentTime);
    }

    @PostMapping("/sendOffer")
    @ApiOperation(value = "招聘管理-发送offer")
    @OptLog(description = "招聘管理-发送offer")
    @RequiresRoles(RoleConstant.STUDENT)
    public R sendOffer(@RequestBody NewCtWorker newCtWorker){
        return newApplyForLoanBiz.sendOffer(newCtWorker,getUserId());
    }


    @ApiOperation(value = "更新原料：原料商店")
    @GetMapping("/material/shop")
    @RequiresRoles(RoleConstant.STUDENT)
    public R listMaterialShop(@RequestParam @ApiParam(value = "竞赛id") Integer contestId,
                            @RequestParam @ApiParam(value = "当前时间") Integer currentTime) {

        return newApplyForLoanBiz.getGzMaterialList(contestId,getUserId(),currentTime);
    }

    @ApiOperation(value = "原料商店：确认下单")
    @PostMapping("/material/confirmOrder")
    @RequiresRoles(RoleConstant.STUDENT)
    public R listMaterialShopConfirm(@RequestBody NewMaterialVo NewMaterialVo) {
        return newApplyForLoanBiz.getGzMaterialShopConfirm(NewMaterialVo,getUserId());
    }


    @ApiOperation(value = "更新原料：原料订单")
    @GetMapping("/material/order")
    @RequiresRoles(RoleConstant.STUDENT)
    public R listMaterialOrder(@RequestParam @ApiParam(value = "竞赛id") Integer contestId,
                          @RequestParam @ApiParam(value = "当前时间") Integer currentTime) {

        return newApplyForLoanBiz.getMaterialOrderList(contestId,getUserId());
    }

    @ApiOperation(value = "更新原料：原料库存")
    @GetMapping("/material/inventory")
    @RequiresRoles(RoleConstant.STUDENT)
    public R listMaterialInventory(@RequestParam @ApiParam(value = "竞赛id") Integer contestId,
                               @RequestParam @ApiParam(value = "当前时间") Integer currentTime) {

        return newApplyForLoanBiz.getMaterialInventoryList(contestId,getUserId());
    }

    @ApiOperation(value = "更新原料：产品库存")
    @GetMapping("/product/inventory")
    @RequiresRoles(RoleConstant.STUDENT)
    public R productInventory(@RequestParam @ApiParam(value = "竞赛id") Integer contestId,
                                   @RequestParam @ApiParam(value = "当前时间") Integer currentTime) {

        return newApplyForLoanBiz.getProductInventory(contestId,getUserId());
    }

    @ApiOperation(value = "更新原料：收货")
    @GetMapping("material/to/inventory")
    @RequiresRoles(RoleConstant.STUDENT)
    public R materialToInventory(@RequestParam @ApiParam(value = "订单id") Integer id,
                              @RequestParam @ApiParam(value = "当前时间") Integer currentTime) {

        return newApplyForLoanBiz.materialToInventory(id,currentTime);
    }

    @ApiOperation(value = "工人管理：在职工人数据")
    @GetMapping("worker/onTheJob/list")
    @RequiresRoles(RoleConstant.STUDENT)
    public R getWorkerOnTheJob(@RequestParam @ApiParam(value = "竞赛id") Integer contestId,
                                 @RequestParam @ApiParam(value = "当前时间") Integer currentTime) {

        return newApplyForLoanBiz.getWorkerOnTheJobList(contestId,getUserId());
    }

    @ApiOperation(value = "工人管理：获取班次规则")
    @GetMapping("classes/list")
    @RequiresRoles(RoleConstant.STUDENT)
    public R getClassList(@RequestParam @ApiParam(value = "竞赛id") Integer contestId,
                               @RequestParam @ApiParam(value = "当前时间") Integer currentTime) {
        return newApplyForLoanBiz.getClassList(contestId);
    }
}