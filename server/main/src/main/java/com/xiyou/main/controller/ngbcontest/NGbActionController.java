package com.xiyou.main.controller.ngbcontest;

import com.xiyou.common.controller.BaseController;
import com.xiyou.common.redis.utils.RedisCache;
import com.xiyou.common.utils.R;
import com.xiyou.main.biz.ngbcontest.NGbActionBiz;
import com.xiyou.main.config.Sequential;
import com.xiyou.main.constants.RoleConstant;
import com.xiyou.main.entity.ngbcontest.*;
import com.xiyou.main.params.ngbcontest.*;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.shiro.authz.annotation.RequiresRoles;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.NotNull;
import java.util.concurrent.TimeUnit;

/**
 * @author dingyoumeng
 * @since 2019/07/25
 */
@RestController
@RequestMapping("/tp/ngbContest")
@Api(tags = "p19-p29")
@Validated
public class NGbActionController extends BaseController {

    @Autowired
    NGbActionBiz actionBiz;

    @Autowired
    RedisCache redisCache;



    @ResponseBody
    @GetMapping("/order/list")
    @ApiOperation(value = "交货订单-p20")
    @RequiresRoles(RoleConstant.STUDENT)
    public R listOrder(@RequestParam @NotNull(message = "contestId不能为空") Integer contestId,
                       @RequestParam @NotNull(message = "date不能为空") Integer date) {
        return actionBiz.listOrder(getUserId(), contestId, date);
    }


    @ResponseBody
    @GetMapping("/order/delivery")
    @ApiOperation(value = "交货订单-交货-p20")
    @RequiresRoles(RoleConstant.STUDENT)
    @Sequential(type = "ngbcontest")
    public R deliveryOrder(@RequestParam @NotNull(message = "contestId不能为空") Integer contestId,
                           @RequestParam @NotNull(message = "orderId不能为空") Integer orderId,
                           @RequestParam @NotNull(message = "date不能为空") Integer date) {
        return actionBiz.deliveryOrder(getUserId(), contestId, orderId, date);
    }



    @ResponseBody
    @GetMapping("/product/yfList")
    @ApiOperation(value = "产品研发-列表-p22")
    @RequiresRoles(RoleConstant.STUDENT)
    public R yfProductList(@RequestParam @NotNull(message = "contestId不能为空") Integer contestId) {
        return actionBiz.yfProductList(getUserId(), contestId);
    }

    @ResponseBody
    @PostMapping("/product/yf")
    @ApiOperation(value = "产品研发-确认-p22")
    @RequiresRoles(RoleConstant.STUDENT)
    @Sequential(type = "ngbcontest")
    public R yfProduct(@RequestBody @Validated NGbYfParam yfParam) {
        return actionBiz.yfProduct(getUserId(), yfParam);
    }





    @ResponseBody
    @GetMapping("/design/progress/list")
    @ApiOperation(value = "查看特征研发进度")
    @RequiresRoles(RoleConstant.STUDENT)
    @Sequential(type = "ngbcontest")
    public R designProcessList(@RequestParam @NotNull(message = "contestId不能为空") Integer contestId) {
        return actionBiz.designProcessList(getUserId(), contestId);
    }


    @ResponseBody
    @PostMapping("/design/progress/research")
    @ApiOperation(value = "特征研发")
    @RequiresRoles(RoleConstant.STUDENT)
    @Sequential(type = "ngbcontest")
    public R designProgressResearch(@RequestBody NGbCtDesignProgress nGbCtDesignProgress) {
        return actionBiz.designProgressResearch(getUserId(), nGbCtDesignProgress);
    }


    @ResponseBody
    @GetMapping("/design/feature/list")
    @ApiOperation(value = "查询产品特性")
    @RequiresRoles(RoleConstant.STUDENT)
    @Sequential(type = "ngbcontest")
    public R designFeatureList(@RequestParam @NotNull(message = "contestId不能为空") Integer contestId) {
        return actionBiz.designFeatureList(getUserId(), contestId);
    }



    @ResponseBody
    @PostMapping("/design/feature/add")
    @ApiOperation(value = "添加产品特性")
    @RequiresRoles(RoleConstant.STUDENT)
    @Sequential(type = "ngbcontest")
    public R designFeatureAdd(@RequestBody NGbCtDesignFeature nGbCtDesignFeature) {
        return actionBiz.designFeatureAdd(getUserId(),nGbCtDesignFeature);
    }



    @ResponseBody
    @PostMapping("/feature/getZx")
    @ApiOperation(value = "根据产品ID获取最新的产品特性")
    @RequiresRoles(RoleConstant.STUDENT)
    @Sequential(type = "ngbcontest")
    public R getZxFeature(@RequestBody NGbCtDesignFeature nGbCtDesignFeature) {
        return actionBiz.getZxFeature(getUserId(),nGbCtDesignFeature);
    }



    @ResponseBody
    @GetMapping("/endingSeason")
    @ApiOperation(value = "学生当季结束 -组间对抗")
    @RequiresRoles(RoleConstant.STUDENT)
    @Sequential(type = "ngbcontest")
    public R endingSeason(@RequestParam @NotNull(message = "contestId不能为空") Integer contestId,
                       @RequestParam @NotNull(message = "date不能为空") Integer date) {
        //阻止暂停状态下 和 破产状态下的用户进行操作
        actionBiz.preventAction(contestId,getUserId());
        return actionBiz.endingSeason(getUserId(), contestId, date);
    }


    @ResponseBody
    @GetMapping("/show/fee")
    @ApiOperation(value = "更新应收款- 列表 p24")
    @RequiresRoles(RoleConstant.STUDENT)
    public R showFee(@RequestParam @NotNull(message = "contestId不能为空") Integer contestId,
                     @RequestParam @NotNull(message = "账单类型不能为空") Integer currentTime) {
        return actionBiz.showFee(getUserId(), contestId, NGbCtFee.TO_RECEIVABLE,currentTime);
    }

    @ResponseBody
    @GetMapping("/show/pay")
    @ApiOperation(value = "更新应付款- 列表")
    @RequiresRoles(RoleConstant.STUDENT)
    public R showPay(@RequestParam @NotNull(message = "contestId不能为空") Integer contestId,
                     @RequestParam @NotNull(message = "账单类型不能为空") Integer currentTime) {
        return actionBiz.showFee(getUserId(), contestId,NGbCtFee.TO_PAYMENT,currentTime);
    }

    @ResponseBody
    @GetMapping("/receivables/confirm")
    @ApiOperation(value = "更新应收款 - 收款")
    @RequiresRoles(RoleConstant.STUDENT)
    @Sequential(type = "ngbcontest")
    public R receivablesConfirm(@RequestParam @NotNull(message = "contestId不能为空") Integer contestId,
                                @RequestParam @NotNull(message = "feeId不能为空") Integer feeId,
                                @RequestParam @NotNull(message = "当前时间") Integer currentTime
    ) {
        return actionBiz.confirmReceivables(getUserId(),contestId,feeId,currentTime);
    }



    @ResponseBody
    @GetMapping("/receivables/batchConfirm")
    @ApiOperation(value = "批量收款")
    @RequiresRoles(RoleConstant.STUDENT)
    @Sequential(type = "ngbcontest")
    public R receivablesBatchConfirm(@RequestParam @NotNull(message = "contestId不能为空") Integer contestId,
                                @RequestParam @NotNull(message = "当前时间") Integer currentTime
    ) {
        return actionBiz.receivablesBatchConfirm(getUserId(),contestId,currentTime);
    }



    @ResponseBody
    @PostMapping("/discount")
    @ApiOperation(value = "更新应收款 - 贴现")
    @RequiresRoles(RoleConstant.STUDENT)
    @Sequential(type = "ngbcontest")
    public R discount(@RequestBody @Validated NGbDiscountParam discountParam) {
        return actionBiz.discount(discountParam, getUserId());
    }

    @ResponseBody
    @GetMapping("/pay/confirm")
    @ApiOperation(value = "更新应付款 - 付款")
    @RequiresRoles(RoleConstant.STUDENT)
    @Sequential(type = "ngbcontest")
    public R payment(@RequestParam @NotNull(message = "contestId不能为空") Integer contestId,
                     @RequestParam @NotNull(message = "feeId不能为空") Integer feeId,
                     @RequestParam @NotNull(message = "当前时间") Integer currentTime) {
        return actionBiz.payment(getUserId(),contestId,feeId,currentTime);
    }



    @ResponseBody
    @GetMapping("/pay/batchConfirm")
    @ApiOperation(value = "更新应付款 - 批量付款")
    @RequiresRoles(RoleConstant.STUDENT)
    @Sequential(type = "ngbcontest")
    public R batchPayment(@RequestParam @NotNull(message = "contestId不能为空") Integer contestId,
                     @RequestParam @NotNull(message = "当前时间") Integer currentTime) {
        return actionBiz.batchPayment(getUserId(),contestId,currentTime);
    }


    @ResponseBody
    @PostMapping("/urgent/material")
    @ApiOperation(value = "紧急采购 - 材料采购")
    @RequiresRoles(RoleConstant.STUDENT)
    @Sequential(type = "ngbcontest")
    public R materialUrgentBuy(@RequestBody @Validated NGbUrgentBuyParam param) {
        return actionBiz.buyMaterial(getUserId(), param);
    }

    @ResponseBody
    @PostMapping("/urgent/product")
    @ApiOperation(value = "紧急采购 - 产成品采购")
    @RequiresRoles(RoleConstant.STUDENT)
    @Sequential(type = "ngbcontest")
    public R productUrgentBuy(@RequestBody @Validated NGbUrgentBuyParam param) {
        return actionBiz.buyProduct(getUserId(), param);
    }


    @ResponseBody
    @PostMapping("/sell/material")
    @ApiOperation(value = "出售库存 - 出售材料 - p26")
    @RequiresRoles(RoleConstant.STUDENT)
    @Sequential(type = "ngbcontest")
    public R sellMaterial(@RequestBody @Validated NGbSellMaterialParam param) {
        return actionBiz.sellMaterial(getUserId(), param);
    }

    @ResponseBody
    @PostMapping("/sell/product")
    @ApiOperation(value = "出售库存 - 出售产品 - p26")
    @RequiresRoles(RoleConstant.STUDENT)
    @Sequential(type = "ngbcontest")
    public R sellProduct(@RequestBody @Validated NGbSellProductParam param) {
        return actionBiz.sellProduct(getUserId(), param);
    }



    @ResponseBody
    @GetMapping("/xzorder")
    @ApiOperation(value = "订单信息- p28")
    @RequiresRoles(RoleConstant.STUDENT)
    public R xzorder(@RequestParam @NotNull(message = "contestId不能为空") Integer contestId,
                     @RequestParam @NotNull(message = "当前时间") Integer date) {
        return actionBiz.xzorder(getUserId(), contestId, date);
    }

    @ResponseBody
    @GetMapping("/market/list")
    @ApiOperation(value = "市场开拓 - 显示 - p29")
    @RequiresRoles(RoleConstant.STUDENT)
    public R marketList(@RequestParam @NotNull(message = "contestId不能为空") Integer contestId) {
        return actionBiz.marketList(getUserId(), contestId);
    }

    @ResponseBody
    @PostMapping("/market/yf")
    @ApiOperation(value = "市场开拓 - 确认")
    @RequiresRoles(RoleConstant.STUDENT)
    @Sequential(type = "ngbcontest")
    public R marketYf(@RequestBody @Validated NGbMarketYfParam param) {
        return actionBiz.marketYf(getUserId(), param);
    }


    @ResponseBody
    @GetMapping("/social/score")
    @ApiOperation(value = "社会责任分 - 显示")
    @RequiresRoles(RoleConstant.STUDENT)
    public R getCorporateScore(@RequestParam @NotNull(message = "contestId不能为空") Integer contestId) {
        return actionBiz.getCorporateScore(getUserId(), contestId);
    }


    @ResponseBody
    @GetMapping("/social/scoreDetail")
    @ApiOperation(value = "社会责任分 - 扣分明细")
    @RequiresRoles(RoleConstant.STUDENT)
    public R getCorporateScoreDetail(@RequestParam @NotNull(message = "contestId不能为空") Integer contestId) {
        return actionBiz.getCorporateScoreDetail(getUserId(), contestId);
    }

    @ResponseBody
    @PostMapping("/add/capital")
    @ApiOperation(value = "注资")
    @RequiresRoles(RoleConstant.TEACHER)
    public R addCapital(@RequestBody @Validated NGbCapitalParam gbCapitalParam) {
        return actionBiz.addCapital(gbCapitalParam);
    }


//    @ResponseBody
//    @GetMapping("/get/other/info")
//    @ApiOperation(value = "数据咨询")
//    public R getOtherInfo(HttpServletResponse response,
//                          @RequestParam @NotNull(message = "contestId不能为空") Integer contestId,
//                          @RequestParam @NotNull(message = "contestId不能为空") Integer userId,
//                          @RequestParam @NotNull(message = "studentId不能为空") Integer studentId,
//                          @RequestParam @NotNull(message = "date不能为空") Integer date,
//                          @RequestParam @NotNull(message = "amount不能为空") Integer amount) {
//        return actionBiz.getOtherInfo(response,userId,studentId,contestId,date,amount);
//    }


    @ResponseBody
    @PostMapping("/buy/dataConsult")
    @ApiOperation(value = "数据咨询-情报购买")
    @Sequential(type = "ngbcontest")
    public R buyDataConsult(@RequestBody NGbBuyDataConsult nGbBuyDataConsult) {
        nGbBuyDataConsult.setStudentId(getUserId());
        return actionBiz.buyDataConsult(nGbBuyDataConsult);
    }





    @ResponseBody
    @PostMapping("/carbon/neutraliza")
    @ApiOperation(value = "碳排放-碳中和")
    @Sequential(type = "ngbcontest")
    public R carbonNeutraliza(@RequestBody NGbCtCarbon nGbCtCarbon) {
        nGbCtCarbon.setStudentId(getUserId());
        return actionBiz.carbonNeutraliza(nGbCtCarbon);
    }


    @ResponseBody
    @GetMapping("/carbon/info")
    @ApiOperation(value = "碳排放-碳排放信息查询")
    @Sequential(type = "ngbcontest")
    public R carbonInfo(@RequestParam @NotNull(message = "contestId不能为空") Integer contestId) {
        return actionBiz.carbonInfo(contestId,getUserId());
    }



    @ResponseBody
    @GetMapping("/operateRanking")
    @ApiOperation(value = "企业经营排名")
    @Sequential(type = "ngbcontest")
    public R operateRanking(@RequestParam @NotNull(message = "contestId不能为空") Integer contestId) {
        return actionBiz.operateRanking(contestId);
    }


    @ResponseBody
    @GetMapping("/viewScore")
    @ApiOperation(value = "查看成绩")
    @Sequential(type = "ngbcontest")
    public R viewScore(@RequestParam @NotNull(message = "contestId不能为空") Integer contestId) {
        return actionBiz.viewScore(contestId);
    }


    @ResponseBody
    @PostMapping("/get/dataConsultList")
    @ApiOperation(value = "数据咨询-获取购买情报了的用户")
    public R getDataConsultList(@RequestBody NGbBuyDataConsult nGbBuyDataConsult) {
        nGbBuyDataConsult.setStudentId(getUserId());
        return actionBiz.getDataConsultList(nGbBuyDataConsult);
    }



    @ResponseBody
    @GetMapping("/get/budget")
    @ApiOperation(value = "获取本季度的预算")
    @RequiresRoles(RoleConstant.STUDENT)
    public R getBudget(@RequestParam @NotNull(message = "contestId不能为空") Integer contestId) {
        return actionBiz.getBudget(getUserId(),contestId);
    }


    @ResponseBody
    @PostMapping("/submit/budget")
    @ApiOperation(value = "提交本季度的预算")
    @RequiresRoles(RoleConstant.STUDENT)
    public R submitBudget(@RequestBody NGbCtBudget nGbCtBudget) {
        return actionBiz.submitBudget(getUserId(),nGbCtBudget);
    }


    @ResponseBody
    @PostMapping("/apply/budget")
    @ApiOperation(value = "预算申请")
    @RequiresRoles(RoleConstant.STUDENT)
    @Sequential(type = "ngbcontest")
    public R applyBudget(@RequestBody NGbCtBudgetApply nGbCtBudgetApply) {
        return actionBiz.applyBudget(getUserId(),nGbCtBudgetApply);
    }


    @ResponseBody
    @PostMapping("/getBudgetApplyList")
    @ApiOperation(value = "查询本季度的预算申请记录")
    @RequiresRoles(RoleConstant.STUDENT)
    public R getBudgetApplyList(@RequestBody NGbCtBudgetApply nGbCtBudgetApply) {
        return actionBiz.getBudgetApplyList(getUserId(),nGbCtBudgetApply);
    }



    @ResponseBody
    @PostMapping("/approve/budget")
    @ApiOperation(value = "预算审批")
    @RequiresRoles(RoleConstant.STUDENT)
    @Sequential(type = "ngbcontest")
    public R approveBudget(@RequestBody NGbCtBudgetApply nGbCtBudgetApply) {
        return actionBiz.approveBudget(getUserId(),nGbCtBudgetApply);
    }



    @ResponseBody
    @PostMapping("/pdca/get")
    @ApiOperation(value = "查询本年填写的PDCA")
    @RequiresRoles(RoleConstant.STUDENT)
    public R getPdca(@RequestBody NGbCtPdca nGbCtPdca) {
        nGbCtPdca.setStudentId(getUserId());
        return actionBiz.getPdca(nGbCtPdca);
    }



    @ResponseBody
    @PostMapping("/pdca/submit")
    @ApiOperation(value = "提交本年的PDCA")
    @RequiresRoles(RoleConstant.STUDENT)
    public R submitPdca(@RequestBody @Validated NGbCtPdca nGbCtPdca) {
        nGbCtPdca.setStudentId(getUserId());
        return actionBiz.submitPdca(nGbCtPdca);
    }





    @ResponseBody
    @GetMapping("/redisTest")
    @ApiOperation(value = "redis测试")
    public R redisTest() {
        redisCache.set("111","22222", 20, TimeUnit.SECONDS);
        return  R.success();
    }


    @ResponseBody
    @GetMapping("/getRedisTest")
    @ApiOperation(value = "获取到期时间")
    public R getRedisTest(@RequestParam @NotNull(message = "key不能为空") String  key) {
        long time  = redisCache.getExpire(key);
        return  R.success().put("time",time);
    }


}
