package com.xiyou.main.controller.ngbcontest;


import com.xiyou.common.controller.BaseController;
import com.xiyou.common.log.annotations.OptLog;
import com.xiyou.common.utils.R;
import com.xiyou.main.biz.ngbcontest.NGbCtSubjectBiz;
import com.xiyou.main.constants.RoleConstant;
import com.xiyou.main.entity.ngbcontest.NGbCtSubject;
import com.xiyou.main.params.contest.SubjectParam;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.apache.shiro.authz.annotation.RequiresRoles;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotNull;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author tangcan
 * @since 2019-08-25
 */
@RestController
@RequestMapping("/tp/ngbCtSubject")
@Api(tags = "组件对抗-题库管理")
@Validated
public class NGbCtSubjectController extends BaseController {
    @Autowired
    private NGbCtSubjectBiz gbCtSubjectBiz;

    @ResponseBody
    @PostMapping("/list/admin")
    @ApiOperation(value = "管理员获取题库列表-模糊查询")
    public R listByAdmin(@RequestBody @Validated @ApiParam(value = "题库查询参数", required = true) SubjectParam subjectParam) {
        return gbCtSubjectBiz.listByAdmin(subjectParam);
    }

    @ResponseBody
    @PostMapping("/list/teacher")
    @ApiOperation(value = "教师获取题库列表-模糊查询")
    public R listByTeacher(@RequestBody @Validated @ApiParam(value = "题库查询参数", required = true) SubjectParam subjectParam) {
        subjectParam.setTeacherId(getUserId());
        return gbCtSubjectBiz.listByTeacher(subjectParam);
    }
//
//    @ResponseBody
//    @GetMapping("/list/teacher")
//    @ApiOperation(value = "教师获取题库列表")
//    @RequiresRoles(RoleConstant.TEACHER)
//    public R listByTeacher(@RequestParam @ApiParam(value = "分组id", required = true) Integer groupId) {
//        return gbCtSubjectBiz.listByTeacher(getUserId(),groupId);
//    }

    @ResponseBody
    @GetMapping("/get")
    @ApiOperation(value = "获取题库基本信息")
    public R get(@RequestParam @NotNull Integer subjectId) {
        return gbCtSubjectBiz.get(subjectId);
    }

    @ResponseBody
    @PostMapping("/add")
    @ApiOperation(value = "添加题库")
    @OptLog(description = "添加竞赛模拟题库")
    public R add(@RequestBody @Validated @ApiParam(value = "题库信息", required = true) NGbCtSubject gbCtSubject) {
        gbCtSubject.setCreateUserId(getUserId());
        return gbCtSubjectBiz.add(gbCtSubject);
    }

    @ResponseBody
    @PostMapping("/update")
    @ApiOperation(value = "更新题库基本信息")
    public R update(@RequestBody @Validated @ApiParam(value = "题库信息", required = true) NGbCtSubject gbCtSubject) {
        gbCtSubject.setCreateUserId(getUserId());
        return gbCtSubjectBiz.update(gbCtSubject);
    }


    @ResponseBody
    @GetMapping("/queryContest")
    @ApiOperation(value = "查询当前题库下的比赛")
    @OptLog(description = "查询当前题库下的比赛")
    public R queryContest(@RequestParam @NotNull @ApiParam(value = "题库号", required = true) Integer subjectNumber) {
        return gbCtSubjectBiz.queryContest(subjectNumber);
    }


    @ResponseBody
    @GetMapping("/delete")
    @ApiOperation(value = "删除题库")
    @OptLog(description = "删除竞赛模拟题库")
    public R delete(@RequestParam @NotNull @ApiParam(value = "题库id", required = true) Integer subjectId) {
        return gbCtSubjectBiz.delete(subjectId);
    }
}

