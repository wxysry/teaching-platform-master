package com.xiyou.main.controller.ngbcontest;

import com.xiyou.common.controller.BaseController;
import com.xiyou.common.utils.R;
import com.xiyou.main.biz.ngbcontest.NGbFillReportBiz;
import com.xiyou.main.config.Sequential;
import com.xiyou.main.constants.RoleConstant;
import com.xiyou.main.params.ngbcontest.NGbFillReportParam;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.apache.shiro.authz.annotation.RequiresRoles;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotNull;

/**
 * @program: multi-module
 * @description: 综合费用表
 * @author: wangxingyu
 * @create: 2023-06-28 21:34
 **/
@RestController
@RequestMapping("/tp/ngbFillReport")
@Api(tags = "p36:填写报表")
@Validated
public class NGbFillReportController extends BaseController {

    @Autowired
    private NGbFillReportBiz gbFillReportBiz;


    @ResponseBody
    @PostMapping("/batch/fill")
    @ApiOperation(value = "填写报表")
    @RequiresRoles(RoleConstant.STUDENT)
    @Sequential(type = "ngbcontest")
    public R batchFill(@RequestBody @Validated @ApiParam(value = "财务指标表", required = true) NGbFillReportParam gbFillReportParam) {
        return gbFillReportBiz.batchFill(getUserId(), gbFillReportParam);
    }



    @ResponseBody
    @GetMapping("/getTemp")
    @ApiOperation(value = "获取暂存数据")
    @RequiresRoles(RoleConstant.STUDENT)
    public R getTemp(@RequestParam @NotNull(message = "contestId不能为空")Integer contestId,
                     @RequestParam @NotNull(message = "year不能为空")Integer year) {
        return gbFillReportBiz.getTemp(getUserId(),contestId,year);
    }



    @ResponseBody
    @GetMapping("/getAutoFill")
    @ApiOperation(value = "获取系统自动生成的数据")
    @RequiresRoles(RoleConstant.STUDENT)
    public R getAutoFill(@RequestParam @NotNull(message = "contestId不能为空")Integer contestId,
                         @RequestParam @NotNull(message = "year不能为空")Integer year) {
        return gbFillReportBiz.getAutoFill(getUserId(),contestId,year);
    }


    @ResponseBody
    @GetMapping("/fillReport/result")
    @ApiOperation(value = "获取当年报表")
    @RequiresRoles(RoleConstant.TEACHER)
    public R getFillReportResult(@RequestParam @NotNull(message = "contestId不能为空")Integer contestId,
                                 @RequestParam @NotNull(message = "year不能为空")Integer year) {
        return gbFillReportBiz.getFillReportResult(contestId,year);
    }
}
