package com.xiyou.main.controller.ngbcontest;

import com.xiyou.common.controller.BaseController;
import com.xiyou.common.enums.CodeEnum;
import com.xiyou.common.utils.R;
import com.xiyou.main.biz.ngbcontest.NGbTradeFairBiz;
import com.xiyou.main.config.Sequential;
import com.xiyou.main.constants.RoleConstant;
import com.xiyou.main.entity.ngbcontest.NGbCtOrderApply;
import com.xiyou.main.entity.ngbcontest.NGbCtXzOrder;
import com.xiyou.main.params.ngbcontest.NGbCtMnChooseParam;
import com.xiyou.main.params.ngbcontest.NGbCtNetworkPlanParam;
import com.xiyou.main.params.ngbcontest.NGbCtRetailOrderParam;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.shiro.authz.annotation.RequiresRoles;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotNull;

/**
 * @program: multi-module
 * @description: 订货会
 * @author: tangcan
 * @create: 2019-07-26 10:04
 **/
@RestController
@RequestMapping("/tp/ngbTradeFair")
@Api(tags = "p40:参加订货会")
@Validated
public class NGbTradeFairController extends BaseController {
    @Autowired
    private NGbTradeFairBiz gbTradeFairBiz;


    @ResponseBody
    @PostMapping("/order/add")
    @ApiOperation(value = "临时添加订单")
    @RequiresRoles(RoleConstant.TEACHER)
    public R addOrder(@RequestBody NGbCtXzOrder nGbCtXzOrder) {
        if (nGbCtXzOrder.getContestId() == null) {
            return R.error(CodeEnum.PARAM_ERROR.getCode(), "选单id不能为空");
        }
        if (nGbCtXzOrder.getStudentId() == null) {
            return R.error(CodeEnum.PARAM_ERROR.getCode(), "学生id不能为空");
        }
        if (nGbCtXzOrder.getCoId() == null) {
            return R.error(CodeEnum.PARAM_ERROR.getCode(), "订单选择不能为空");
        }
        if (nGbCtXzOrder.getApplyPrice() == null) {
            return R.error(CodeEnum.PARAM_ERROR.getCode(), "单价不能为空");
        }
        if (nGbCtXzOrder.getAssignedNum() == null) {
            return R.error(CodeEnum.PARAM_ERROR.getCode(), "数量不能为空");
        }
        return gbTradeFairBiz.addOrder(nGbCtXzOrder);
    }


    @ResponseBody
    @GetMapping("/getOrderDate")
    @ApiOperation(value = "获取可选单的时间")
    public R getOrderDate(@RequestParam @NotNull(message = "contestId不能为空") Integer contestId) {
        return gbTradeFairBiz.getOrderDate(contestId);
    }


    @ResponseBody
    @GetMapping("/start/competeOrder")
    @ApiOperation(value = "开始竞单/分配订单")
    @RequiresRoles(RoleConstant.TEACHER)
    public R startCompeteOrder(@RequestParam @NotNull(message = "contestId不能为空") Integer contestId,
                               @RequestParam @NotNull(message = "date不能为空") Integer date,
                               @RequestParam @NotNull(message = "操作类型不能为空，1为申报 2为分配") Integer type) {
        return gbTradeFairBiz.startCompeteOrder(contestId,date,type);
    }


    @ResponseBody
    @GetMapping("/reAssignOrder")
    @ApiOperation(value = "重新分配订单")
    @RequiresRoles(RoleConstant.TEACHER)
    public R reAssignOrder(@RequestParam @NotNull(message = "contestId不能为空") Integer contestId) {
        return gbTradeFairBiz.reAssignOrder(contestId);
    }




    @ResponseBody
    @GetMapping("/getMarketOrderList")
    @ApiOperation(value = "获取当前季度各市场的订单")
    public R getMarketOrderList(@RequestParam @NotNull(message = "contestId不能为空") Integer contestId,
                                @RequestParam @NotNull(message = "date不能为空") Integer date){

        return gbTradeFairBiz.getMarketOrderList(contestId,date,getUserId());
    }


    @ResponseBody
    @GetMapping("/getApplyOrderList")
    @ApiOperation(value = "获取已申报的订单")
    @RequiresRoles(RoleConstant.STUDENT)
    public R getApplyOrderList(@RequestParam @NotNull(message = "contestId不能为空") Integer contestId,
                               @RequestParam @NotNull(message = "date不能为空") Integer date){
        return gbTradeFairBiz.getApplyOrderList(contestId,date,getUserId());
    }


    @ResponseBody
    @PostMapping("/applyOrder")
    @ApiOperation(value = "申请订单")
    @RequiresRoles(RoleConstant.STUDENT)
    @Sequential(type = "ngbcontest")
    public R applyOrder(@RequestBody NGbCtOrderApply nGbCtOrderApply){
        nGbCtOrderApply.setStudentId(getUserId());
        return gbTradeFairBiz.applyOrder(nGbCtOrderApply);
    }


    @ResponseBody
    @PostMapping("/getAssignedOrderList")
    @ApiOperation(value = "查询本季度所有已分配的订单")
    public R getAssignedOrderList(@RequestBody NGbCtXzOrder nGbCtXzOrder){
        nGbCtXzOrder.setStudentId(getUserId());
        return gbTradeFairBiz.getAssignedOrderList(nGbCtXzOrder);
    }



    @ResponseBody
    @PostMapping("/all/order")
    @ApiOperation(value = "选单明细")
    @RequiresRoles(RoleConstant.TEACHER)
    public R getMnOrder(@RequestBody NGbCtMnChooseParam gbCtMnChooseParam) {
        return gbTradeFairBiz.getMnOrder(gbCtMnChooseParam);
    }

    @ResponseBody
    @GetMapping("/ad/list")
    @ApiOperation(value = "本季广告")
    @RequiresRoles(RoleConstant.TEACHER)
    public R adList(@RequestParam @NotNull(message = "contestId不能为空") Integer contestId,
                    @RequestParam @NotNull(message = "date不能为空") Integer date) {
        return gbTradeFairBiz.adList(contestId, date);
    }

    @ResponseBody
    @GetMapping("/get/choose/order")
    @ApiOperation(value = "获取当季订单")
    @RequiresRoles(RoleConstant.TEACHER)
    public R getChooseOrder(@RequestParam @NotNull(message = "contestId不能为空") Integer contestId,
                            @RequestParam @NotNull(message = "date不能为空") Integer date) {
        return gbTradeFairBiz.getChooseOrder(contestId, date);
    }



    @ResponseBody
    @GetMapping("/network/getInfo")
    @ApiOperation(value = "网络营销-查看网络投放以及会员指数")
    @RequiresRoles(RoleConstant.STUDENT)
    @Sequential(type = "ngbcontest")
    public R getNetworkInfo(@RequestParam @NotNull(message = "contestId不能为空") Integer contestId,
                            @RequestParam @NotNull(message = "currentTime不能为空") Integer currentTime){
        return gbTradeFairBiz.getNetworkInfo(contestId,getUserId(),currentTime);
    }



    @ResponseBody
    @PostMapping("/network/submit")
    @ApiOperation(value = "网络营销-提交网络营销")
    @RequiresRoles(RoleConstant.STUDENT)
    @Sequential(type = "ngbcontest")
    public R submitNetworkPlan(@RequestBody NGbCtNetworkPlanParam nGbCtNetworkPlanParam){
        return gbTradeFairBiz.submitNetworkPlan(nGbCtNetworkPlanParam,getUserId());
    }


    @ResponseBody
    @GetMapping("/network/retailOrder")
    @ApiOperation(value = "网络营销-零售订单")
    public R viewRetailOrder(@RequestParam @NotNull(message = "contestId不能为空") Integer contestId,
                             @RequestParam @NotNull(message = "currentTime不能为空") Integer currentTime){
        return gbTradeFairBiz.viewRetailOrder(contestId,getUserId(),currentTime);
    }

    @ResponseBody
    @PostMapping("/network/allOrder")
    @ApiOperation(value = "网络营销-零售订单")
    @RequiresRoles(RoleConstant.TEACHER)
    public R getAllNetWorkOrder(@RequestBody NGbCtRetailOrderParam nGbCtRetailOrderParam) {
        return gbTradeFairBiz.getAllNetWorkOrder(nGbCtRetailOrderParam);
    }


}
