package com.xiyou.main.controller.ngbcontest;

import com.xiyou.common.controller.BaseController;
import com.xiyou.common.utils.R;
import com.xiyou.main.biz.ngbcontest.NGbYearEndBiz;
import com.xiyou.main.config.Sequential;
import com.xiyou.main.constants.RoleConstant;
import com.xiyou.main.vo.ngbcontest.NGbCashFolowEntity;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.apache.shiro.authz.annotation.RequiresRoles;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotNull;

/**
 * @program: multi-module
 * @description: 当年结束后
 * @author: tangcan
 * @create: 2019-07-22 16:04
 **/
@RestController
@RequestMapping("/tp/ngbYearEnd")
@Api(tags = "p31~34:当年结束后")
@Validated
public class NGbYearEndController extends BaseController {

    @Autowired
    private NGbYearEndBiz yearEndBiz;

    @ResponseBody
    @GetMapping("/confirm")
    @ApiOperation(value = "p31:确认当年结束")
    @RequiresRoles(RoleConstant.STUDENT)
    @Sequential(type = "ngbcontest")
    public R confirm(@RequestParam @NotNull @ApiParam(value = "考试id", required = true) Integer contestId,
                        @RequestParam @NotNull @ApiParam(value = "当前时间", required = true) Integer date) {
        return yearEndBiz.confirm(getUserId(), contestId, date);
    }


    @ApiOperation(value = "开始下一个季度")
    @GetMapping("/nextSeason")
    @RequiresRoles(RoleConstant.STUDENT)
    @Sequential(type = "ngbcontest")
    public R currentQuarterInit(@RequestParam @NotNull @ApiParam(value = "竞赛id") Integer contestId,
                                @RequestParam @NotNull @ApiParam(value = "当前时间") Integer currentTime) {
        NGbCashFolowEntity cashFolowEntity = new NGbCashFolowEntity()
                .setCurrentTime(currentTime)
                .setCurrentYear(currentTime / 10)
                .setStudentId(getUserId())
                .setContestId(contestId);
        return yearEndBiz.currentQuarterNext(cashFolowEntity);
    }

}
