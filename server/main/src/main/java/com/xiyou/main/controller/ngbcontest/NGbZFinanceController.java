package com.xiyou.main.controller.ngbcontest;

import com.xiyou.common.controller.BaseController;
import com.xiyou.common.log.annotations.OptLog;
import com.xiyou.common.utils.R;
import com.xiyou.main.biz.ngbcontest.NGbApplyForLoanBiz;
import com.xiyou.main.biz.ngbcontest.NGbStudentFinanceBiz;
import com.xiyou.main.config.Sequential;
import com.xiyou.main.constants.RoleConstant;
import com.xiyou.main.entity.ngbcontest.NGbCtWorker;
import com.xiyou.main.entity.ngbcontest.NGbCtWorkerMarket;
import com.xiyou.main.params.ngbcontest.NGbWorkerIncentiveParam;
import com.xiyou.main.params.ngbcontest.NGbWorkerTrainParam;
import com.xiyou.main.vo.ngbcontest.NGbCashFolowEntity;
import com.xiyou.main.vo.ngbcontest.NGbMaterialVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.apache.shiro.authz.annotation.RequiresRoles;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * @author wangxingyu
 * @since 2023-06-11
 * @description: 财务信息统计
 **/
@RestController
@RequestMapping("/tp/ngbFinance")
@Validated
@Api(tags = "p3-财务信息&综合财务信息")
public class NGbZFinanceController extends BaseController {

    @Autowired
    private NGbStudentFinanceBiz studentFinanceBiz;
    @Autowired
    private NGbApplyForLoanBiz gbApplyForLoanBiz;

    @ApiOperation(value = "p3财务信息&综合财务信息查询")
    @GetMapping("/statistical/info")
    @RequiresRoles(RoleConstant.STUDENT)
    public R getStudentFinanceInfor(@RequestParam @NotNull @ApiParam(value = "竞赛id") Integer contestId,
                                    @RequestParam @NotNull @ApiParam(value = "当前时间") Integer currentTime) {

        NGbCashFolowEntity cashFolowEntity = new NGbCashFolowEntity();
        cashFolowEntity.setStudentId(getUserId());
        cashFolowEntity.setContestId(contestId);
        // 当前时间
        cashFolowEntity.setCurrentTime(currentTime);
        // 当前时间
        cashFolowEntity.setCurrentYear(currentTime / 10);

        return studentFinanceBiz.getStudentFinanceInfo(cashFolowEntity);
    }

    @ApiOperation(value = "P4研发认证信息")
    @GetMapping("/researchAndAuth/info")
    @RequiresRoles(RoleConstant.STUDENT)
    public R researchAndAuthenInfo(@RequestParam @ApiParam(value = "竞赛id") Integer contestId) {

        return studentFinanceBiz.getResearchAndDevelopeInfo(getUserId(), contestId);
    }

    @ApiOperation(value = "P5库存采购信息")
    @GetMapping("/stockPurchase/info")
    @RequiresRoles(RoleConstant.STUDENT)
    public R getStockPurchaseInfo(@RequestParam @ApiParam(value = "竞赛id") Integer contestId, @RequestParam @ApiParam(value = "当前时间") Integer currentTime) {
        return studentFinanceBiz.stockPurchaseInfo(getUserId(), contestId, currentTime);
    }


    @ApiOperation(value = "P8申请贷款-获取最大贷款额度")
    @GetMapping("/loan/info")
    @RequiresRoles(RoleConstant.STUDENT)
    public R applyForLongLoan(@RequestParam @ApiParam(value = "竞赛id") Integer contestId,
                              @RequestParam @ApiParam(value = "当前时间") Integer currentTime) {
        NGbCashFolowEntity cashFolowEntity = new NGbCashFolowEntity()
                .setStudentId(getUserId())
                .setContestId(contestId)
                .setCurrentTime(currentTime)
                .setCurrentYear(currentTime / 10);
        cashFolowEntity.setBlType(1);
        return gbApplyForLoanBiz.getMaxLoanAmountInfo(cashFolowEntity);
    }

    @ApiOperation(value = "P8申请贷款-融资规则")
    @GetMapping("/loan/gz/info")
    @RequiresRoles(RoleConstant.STUDENT)
    public R applyForLongLoanGz(@RequestParam @ApiParam(value = "竞赛id") Integer contestId) {
        return gbApplyForLoanBiz.getLoanGz(contestId);
    }

    @ApiOperation(value = "P8申请贷款-融资信息")
    @GetMapping("/loan/bank/info")
    @RequiresRoles(RoleConstant.STUDENT)
    public R applyForLongLoanBank(@RequestParam @ApiParam(value = "竞赛id") Integer contestId) {
        return gbApplyForLoanBiz.getLoanBankInfo(contestId,getUserId());
    }

    @ApiOperation(value = "P8申请贷款-确认")
    @PostMapping("/loan/commit")
    @RequiresRoles(RoleConstant.STUDENT)
    @Sequential(type = "ngbcontest")
    public R applyForLongLoanCommit(@RequestBody NGbCashFolowEntity cashFolowEntity) {

        cashFolowEntity.setStudentId(getUserId());
        cashFolowEntity.setCurrentYear(cashFolowEntity.getCurrentTime() / 10);
        return gbApplyForLoanBiz.applyForLoanCommit(cashFolowEntity);
    }



    @ApiOperation(value = "产线信息")
    @GetMapping("/workshop/info")
    @RequiresRoles(RoleConstant.STUDENT)
    public R getWorkshopInfo(@RequestParam @ApiParam(value = "竞赛id") Integer contestId,
                             @RequestParam @ApiParam(value = "当前时间") Integer currentTime) {
        return gbApplyForLoanBiz.getLineGroupInfo(getUserId(),contestId,currentTime);
    }



    @GetMapping("/feeManageData/info")
    @ApiOperation(value = "当季开始-获取费用")
    @OptLog(description = "当季开始-获取费用")
    public R startCurrSeason(@RequestParam("contestId")Integer contestId,
                             @RequestParam("currentTime")Integer currentTime){
        return gbApplyForLoanBiz.startCurrSeason(currentTime,contestId,getUserId());
    }

    @GetMapping("/payFee")
    @ApiOperation(value = "当季开始-缴费")
    @OptLog(description = "当季开始-缴费")
    @Sequential(type = "ngbcontest")
    public R payCurrSeason(@RequestParam("contestId")Integer contestId,
                           @RequestParam("currentTime")Integer currentTime,
                           @RequestParam("blAddTime")Integer blAddTime,
                           @RequestParam("payment")String payment,
                           @RequestParam("amount")Integer amount){
        //前端需要传输当前时间节点和借款时间节点和还款类型
        return gbApplyForLoanBiz.payCurrSeason(contestId,getUserId(),currentTime,payment,blAddTime,amount);
    }


    @GetMapping("/batchPayFee")
    @ApiOperation(value = "当季开始-批量缴费")
    @OptLog(description = "当季开始-批量缴费")
    @Sequential(type = "ngbcontest")
    public R batchPayFee(@RequestParam("contestId")Integer contestId,
                           @RequestParam("currentTime")Integer currentTime){
        //前端需要传输当前时间节点和借款时间节点和还款类型
        return gbApplyForLoanBiz.batchPayFee(contestId,getUserId(),currentTime);
    }


    @GetMapping("/wokerMarket/list")
    @ApiOperation(value = "招聘管理-人力资源市场-数据")
    @OptLog(description = "招聘管理-人力资源市场-数据")
    public R getWorkerList(@RequestParam("contestId")Integer contestId,
                           @RequestParam("currentTime")Integer currentTime){
        //前端需要传输当前时间节点和借款时间节点和还款类型
        return gbApplyForLoanBiz.getWorkerList(contestId,getUserId(),currentTime);
    }




    @PostMapping("/sendOffer")
    @ApiOperation(value = "招聘管理-发送offer")
    @OptLog(description = "招聘管理-发送offer")
    @RequiresRoles(RoleConstant.STUDENT)
    @Sequential(type = "ngbcontest")
    public R sendOffer(@RequestBody NGbCtWorkerMarket nGbCtWorkerMarket){
        return gbApplyForLoanBiz.sendOffer(nGbCtWorkerMarket,getUserId());
    }


    @PostMapping("/cancelOffer")
    @ApiOperation(value = "招聘管理-取消招聘")
    @OptLog(description = "招聘管理-取消招聘")
    @RequiresRoles(RoleConstant.STUDENT)
    @Sequential(type = "ngbcontest")
    public R cancelOffer(@RequestBody NGbCtWorkerMarket nGbCtWorkerMarket){
        return gbApplyForLoanBiz.cancelOffer(nGbCtWorkerMarket,getUserId());
    }



    @PostMapping("/worker/train")
    @ApiOperation(value = "工人培训")
    @RequiresRoles(RoleConstant.STUDENT)
    @Sequential(type = "ngbcontest")
    public R workerTrain(@RequestBody NGbWorkerTrainParam nGbWorkerTrainParam){
        return gbApplyForLoanBiz.workerTrain(nGbWorkerTrainParam.getWorkerList(),getUserId(),nGbWorkerTrainParam.getCurrentTime());
    }





    @PostMapping("/worker/dismiss")
    @ApiOperation(value = "解雇员工")
    @RequiresRoles(RoleConstant.STUDENT)
    @Sequential(type = "ngbcontest")
    public R workerDismiss(@RequestBody NGbCtWorker nGbCtWorker){
        return gbApplyForLoanBiz.workerDismiss(nGbCtWorker,getUserId());
    }


    @PostMapping("/worker/pay")
    @ApiOperation(value = "给工人发工资")
    @RequiresRoles(RoleConstant.STUDENT)
    @Sequential(type = "ngbcontest")
    public R workerPay(@RequestBody NGbCtWorker nGbCtWorker){
        return gbApplyForLoanBiz.workerPay(nGbCtWorker,getUserId());
    }



    @GetMapping("/worker/payBatch")
    @ApiOperation(value = "一键发薪")
    @RequiresRoles(RoleConstant.STUDENT)
    @Sequential(type = "ngbcontest")
    public R workerPayBatch(@RequestParam @ApiParam(value = "竞赛id") Integer contestId,
                            @RequestParam @ApiParam(value = "当前时间") Integer currentTime){
        return gbApplyForLoanBiz.workerPayBatch(contestId,getUserId(),currentTime);
    }


    @PostMapping("/worker/incentiveBatch")
    @ApiOperation(value = "一键激励员工")
    @RequiresRoles(RoleConstant.STUDENT)
    @Sequential(type = "ngbcontest")
    public R incentiveBatch(@RequestBody NGbWorkerIncentiveParam nGbWorkerIncentiveParam){
        return gbApplyForLoanBiz.incentiveBatch(nGbWorkerIncentiveParam,getUserId());
    }


    @PostMapping("/worker/incentiveBatchMoney")
    @ApiOperation(value = "一键激励员工-获取所需的预算")
    @RequiresRoles(RoleConstant.STUDENT)
    @Sequential(type = "ngbcontest")
    public R incentiveBatchMoney(@RequestBody NGbWorkerIncentiveParam nGbWorkerIncentiveParam){
        return gbApplyForLoanBiz.incentiveBatchMoney(nGbWorkerIncentiveParam,getUserId());
    }



    @PostMapping("/worker/incentive")
    @ApiOperation(value = "员工激励")
    @RequiresRoles(RoleConstant.STUDENT)
    @Sequential(type = "ngbcontest")
    public R workerIncentive(@RequestBody NGbWorkerIncentiveParam nGbWorkerIncentiveParam){
        return gbApplyForLoanBiz.workerIncentive(nGbWorkerIncentiveParam,getUserId());
    }




    @ApiOperation(value = "更新原料：原料商店")
    @GetMapping("/material/shop")
    @RequiresRoles(RoleConstant.STUDENT)
    public R listMaterialShop(@RequestParam @ApiParam(value = "竞赛id") Integer contestId,
                            @RequestParam @ApiParam(value = "当前时间") Integer currentTime) {

        return gbApplyForLoanBiz.getGzMaterialList(contestId,getUserId(),currentTime);
    }

    @ApiOperation(value = "原料商店：确认下单")
    @PostMapping("/material/confirmOrder")
    @RequiresRoles(RoleConstant.STUDENT)
    @Sequential(type = "ngbcontest")
    public R listMaterialShopConfirm(@RequestBody NGbMaterialVo NGbMaterialVo) {
        return gbApplyForLoanBiz.getGzMaterialShopConfirm(NGbMaterialVo,getUserId());
    }


    @ApiOperation(value = "更新原料：原料订单")
    @GetMapping("/material/order")
    @RequiresRoles(RoleConstant.STUDENT)
    public R listMaterialOrder(@RequestParam @ApiParam(value = "竞赛id") Integer contestId,
                          @RequestParam @ApiParam(value = "当前时间") Integer currentTime) {

        return gbApplyForLoanBiz.getMaterialOrderList(contestId,getUserId());
    }

    @ApiOperation(value = "更新原料：原料库存")
    @GetMapping("/material/inventory")
    @RequiresRoles(RoleConstant.STUDENT)
    public R listMaterialInventory(@RequestParam @ApiParam(value = "竞赛id") Integer contestId,
                               @RequestParam @ApiParam(value = "当前时间") Integer currentTime) {

        return gbApplyForLoanBiz.getMaterialInventoryList(contestId,getUserId());
    }

    @ApiOperation(value = "更新原料：产品库存")
    @GetMapping("/product/inventory")
    @RequiresRoles(RoleConstant.STUDENT)
    public R productInventory(@RequestParam @ApiParam(value = "竞赛id") Integer contestId,
                                   @RequestParam @ApiParam(value = "当前时间") Integer currentTime) {
        return gbApplyForLoanBiz.getProductInventory(contestId,getUserId());
    }

    @ApiOperation(value = "更新原料：收货")
    @GetMapping("material/to/inventory")
    @RequiresRoles(RoleConstant.STUDENT)
    @Sequential(type = "ngbcontest")
    public R materialToInventory(@RequestParam @ApiParam(value = "订单id") Integer id,
                              @RequestParam @ApiParam(value = "当前时间") Integer currentTime) {

        return gbApplyForLoanBiz.materialToInventory(id,currentTime);
    }

    @ApiOperation(value = "工人管理：在职工人数据")
    @GetMapping("worker/onTheJob/list")
    @RequiresRoles(RoleConstant.STUDENT)
    public R getWorkerOnTheJob(@RequestParam @ApiParam(value = "竞赛id") Integer contestId,
                                 @RequestParam @ApiParam(value = "当前时间") Integer currentTime) {

        return gbApplyForLoanBiz.getWorkerOnTheJobList(contestId,getUserId());
    }


    @ApiOperation(value = "工人管理-获取所有的工人")
    @GetMapping("worker/allList")
    @RequiresRoles(RoleConstant.STUDENT)
    public R getAllWorker(@RequestParam @ApiParam(value = "竞赛id") Integer contestId,
                               @RequestParam @ApiParam(value = "当前时间") Integer currentTime) {
        return gbApplyForLoanBiz.getAllWorker(contestId,getUserId());
    }
    


    @ApiOperation(value = "工人管理：获取班次规则")
    @GetMapping("classes/list")
    @RequiresRoles(RoleConstant.STUDENT)
    public R getClassList(@RequestParam @ApiParam(value = "竞赛id") Integer contestId,
                               @RequestParam @ApiParam(value = "当前时间") Integer currentTime) {
        return gbApplyForLoanBiz.getClassList(contestId);
    }
}