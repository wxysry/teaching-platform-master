package com.xiyou.main.dao.contest;

import com.xiyou.main.entity.contest.ContestGroup;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author tangcan
 * @since 2019-08-25
 */
@Repository
public interface ContestGroupMapper extends BaseMapper<ContestGroup> {
    int insert(ContestGroup contestGroup);

    List<ContestGroup> getListByTeacher(Integer teacherId);
}
