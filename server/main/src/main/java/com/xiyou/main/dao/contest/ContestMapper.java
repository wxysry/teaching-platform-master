package com.xiyou.main.dao.contest;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.xiyou.main.entity.contest.Contest;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xiyou.main.params.contest.ContestParam;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author tangcan
 * @since 2019-08-25
 */
@Repository
public interface ContestMapper extends BaseMapper<Contest> {

    Page<Contest> getPage(Page<Contest> page, @Param("param") ContestParam contestParam);

    int getUnEndContest(@Param("teacherId") Integer teacherId);

    int publish(@Param("teacherId") Integer teacherId, @Param("contestId") Integer contestId);

    Page<Contest> getStudentContestPage(Page<Contest> page, @Param("param") ContestParam contestParam);
}
