package com.xiyou.main.dao.contest;

import com.xiyou.main.entity.contest.CtBalance;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xiyou.main.vo.contest.CashFolowEntity;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author tangcan
 * @since 2019-07-22
 */
@Mapper
@Repository
public interface CtBalanceMapper extends BaseMapper<CtBalance> {

    /**
     * 获取最大贷款额度[上一年所有者权益]
     *
     * @param cashFolowEntity
     * @return
     */
    Integer getBalanceMaxLoanLimit(CashFolowEntity cashFolowEntity);


    Integer getRecentYEarHaveEquity(@Param("studentId") Integer studentId, @Param("contestId") Integer contestId, @Param("nowYear") Integer nowYear);

    int maxTotalEquityPreYear(@Param("studentId") Integer studentId, @Param("contestId") Integer contestId, @Param("currentYear") int currentYear);
}
