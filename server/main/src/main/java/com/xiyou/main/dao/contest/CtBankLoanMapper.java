package com.xiyou.main.dao.contest;

import com.xiyou.main.entity.contest.CtBankLoan;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xiyou.main.vo.contest.CashFolowEntity;
import io.swagger.models.auth.In;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author tangcan
 * @since 2019-07-22
 */
@Mapper
@Repository
public interface CtBankLoanMapper extends BaseMapper<CtBankLoan> {

    /**
     * 长贷总额
     * 参数：studentId、contestId、blType{1：长期贷款，2：短期贷款}、currentTime
     *
     * @param cashFolowEntity
     * @param blType
     * @return
     */
    Integer getStudentLongAndShortTermLoansMoney(@Param("cashFolowEntity") CashFolowEntity cashFolowEntity,
                                                 @Param("blType") Integer blType);

    /**
     * bank_loan中还款时间大于等于当前时间的贷款类型为1和2的总额
     *
     * @param cashFolowEntity
     * @return
     */
    Integer getBackLoanAmount(CashFolowEntity cashFolowEntity);

    /**
     * back_loan中贷款类型为2、且归还时间=（当前时间+1）的金额
     * 参数：studentId、contestId、currentTime+1
     *
     * @param cashFolowEntity
     * @return
     */
    Integer getCurrentMoney(CashFolowEntity cashFolowEntity);

    List<CtBankLoan> getStudentLongAndShortTermLoansMoneyList(@Param("cashFolowEntity") CashFolowEntity cashFolowEntity,
                                                              @Param("blType") Integer blType);

    Integer getTotalEquity(CashFolowEntity cashFolowEntity);

    Integer getFee(CashFolowEntity cashFolowEntity);

    List<CtBankLoan> getList(@Param("contestId") Integer contestId, @Param("studentId") Integer studentId);
}
