package com.xiyou.main.dao.contest;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xiyou.main.entity.contest.CtCashflow;
import com.xiyou.main.vo.contest.CashFolowEntity;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;


import java.util.List;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author tangcan
 * @since 2019-07-22
 */
@Mapper
@Repository
public interface CtCashflowMapper extends BaseMapper<CtCashflow> {

    Integer getCash(@Param("studentId") Integer userId, @Param("contestId") Integer contestId);

    /**
     * 当前金额
     *
     * @param cashFolowEntity
     * @return
     */
    Integer getStudentCurrentMoney(CashFolowEntity cashFolowEntity);

    /**
     * 贴息、利息
     * 参数：studentId、contestId、keyword{贴息、归还利息}、
     *
     * @return
     */
    Integer getStudentDiscountMoney(@Param("cashFolowEntity") CashFolowEntity cashFolowEntity,
                                    @Param("keyword") String keyword);

    /**
     * 维修费、转产费、租金、管理费、广告费
     * 参数：studentId、contestId、currentYear
     *
     * @param cashFolowEntity
     * @return
     */
    Integer getRepairCostToAdCost(@Param("cashFolowEntity") CashFolowEntity cashFolowEntity,
                                  @Param("keyword") String keyword);

    /**
     * 损失
     * 参数：studentId、contestId
     *
     * @param cashFolowEntity
     * @return
     */
    Integer getStudentLossCost(CashFolowEntity cashFolowEntity);

    /**
     * ISO认证、产品开发、市场开拓
     *
     * @param cashFolowEntity
     * @return
     */
    Integer getStudentISOTomarketDevCost(@Param("cashFolowEntity") CashFolowEntity cashFolowEntity,
                                         @Param("keyword") String keyword);

    /**
     * p8保存长贷申请
     *
     * @param cashFolowEntity
     */
    void addStudentLongLoanInfo(CashFolowEntity cashFolowEntity);


    List<CtCashflow> list(@Param("studentId") Integer userId,
                          @Param("contestId") Integer contestId,
                          @Param("year") Integer year,
                          @Param("list") List<String> actionList);

    /**
     * P10->上一条现金
     *
     * @param cashFolowEntity
     * @return
     */
    Integer getStudentLastCash(CashFolowEntity cashFolowEntity);

    List<CtCashflow> getList(@Param("contestId") Integer contestId, @Param("studentId") Integer studentId);
}
