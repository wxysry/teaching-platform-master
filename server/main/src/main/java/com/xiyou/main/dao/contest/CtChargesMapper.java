package com.xiyou.main.dao.contest;

import com.xiyou.main.entity.contest.CtCharges;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author tangcan
 * @since 2019-07-22
 */
@Repository
public interface CtChargesMapper extends BaseMapper<CtCharges> {

    int addBySys(CtCharges charges);

    int updateInformation(@Param("chargesId") Integer chargesId);

    Integer getInformation(@Param("studentId") Integer studentId, @Param("contestId") Integer contestId, @Param("year") int year, @Param("isxt") int isxt);
}
