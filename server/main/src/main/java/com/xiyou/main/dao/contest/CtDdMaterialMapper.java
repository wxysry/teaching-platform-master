package com.xiyou.main.dao.contest;

import com.xiyou.main.entity.contest.CtDdMaterial;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xiyou.main.entity.contest.CtGzMaterial;
import com.xiyou.main.vo.contest.CashFolowEntity;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author tangcan
 * @since 2019-07-22
 */
@Mapper
@Repository
public interface CtDdMaterialMapper extends BaseMapper<CtDdMaterial> {

    /**
     * 黑色车 R1-R5
     * 参数：原料编号1-5
     *
     * @param cmId
     * @param remainDate
     * @return
     */
    Integer getddMaterialBlackCar(@Param("cmId") Integer cmId,
                                  @Param("remainDate") Integer remainDate,
                                  @Param("studentId") Integer studentId,
                                  @Param("contestId") Integer contestId);

    /**
     * 原料采购剩余时间为0的数量
     */
    List<CtDdMaterial> getRemainDateISZeroNumb(@Param("contestId") Integer contestId, @Param("studentId") Integer studentId);

    /**
     * 更新dd_material记录，原料编号为相同的，将剩余时间为1的剩余数量更新到剩余时间为0的，
     * 然后将剩余时间为1的剩余数量更新为0
     *
     * @param cashFolowEntity
     */
    void updateDdMaterialRemainDateEqOne(CashFolowEntity cashFolowEntity);


    void updateMaterial(@Param("list") List<CtDdMaterial> materialList);

    List<CtDdMaterial> list(@Param("contestId") Integer contestId, @Param("studentId") Integer studentId);

    Integer getStudentPayMoney(CashFolowEntity cashFolowEntity);

    void insertBatch(@Param("list") List<CtDdMaterial> ctDdMaterialList);

    List<CtGzMaterial> getListBySubjectNumber(Integer contestId);
}
