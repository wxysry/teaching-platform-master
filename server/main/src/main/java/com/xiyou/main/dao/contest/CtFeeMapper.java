package com.xiyou.main.dao.contest;

import com.xiyou.main.entity.contest.CtFee;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xiyou.main.vo.contest.CashFolowEntity;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author tangcan
 * @since 2019-07-22
 */
@Mapper
@Repository
public interface CtFeeMapper extends BaseMapper<CtFee> {

    /**
     * 查询该用户fee的金额合计
     *
     * @param cashFolowEntity
     * @return
     */
    Integer getStudentReceivableMoney(CashFolowEntity cashFolowEntity);

    List<CtFee> getStudentReceivableMoneyList(CashFolowEntity cashFolowEntity);

    List<CtFee> getList(@Param("studentId") Integer userId, @Param("contestId") Integer contestId);

    void insertBatch(@Param("list") List<CtFee> ctFeeList);
}
