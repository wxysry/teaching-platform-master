package com.xiyou.main.dao.contest;

import com.xiyou.main.entity.contest.CtGzAd;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xiyou.main.entity.contest.CtMnAd;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author tangcan
 * @since 2019-07-25
 */
@Repository
public interface CtGzAdMapper extends BaseMapper<CtGzAd> {

    String getGroupNum(@Param("contestId") Integer contestId, @Param("year") Integer year);

    List<CtMnAd> get(Integer contestId);
}
