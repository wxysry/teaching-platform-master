package com.xiyou.main.dao.contest;

import com.xiyou.main.entity.contest.CtGzCs;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xiyou.main.vo.contest.CashFolowEntity;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

import org.apache.ibatis.annotations.Param;


/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author tangcan
 * @since 2019-07-22
 */

@Mapper
@Repository
public interface CtGzCsMapper extends BaseMapper<CtGzCs> {

    /**
     * 查询gz_cs中Loan_Ceiling
     * 参数：contestId
     *
     * @param cashFolowEntity
     * @return
     */
    Integer getCSLoanCeiling(CashFolowEntity cashFolowEntity);

    /**
     * 获取需贷款年限[最大贷款年限]
     *
     * @return
     */
    Integer getCSLoanTerm();


    CtGzCs getByContestId(@Param("contestId") Integer contestId);

    /**
     * 查询gz_cs中Short_Loan_Interests
     * @param cashFolowEntity
     * @return
     */
    Integer getShortLoanInterests(CashFolowEntity cashFolowEntity);

    /**
     * contestId下gz_cs.maxworkshop
     * @param contestId
     * @return
     */
    Integer getMaxWorkshop(Integer contestId);


    Integer getLongLoanMax(Integer contestId);

    CtGzCs getBySubjectNumber(Integer subjectNumber);
}
