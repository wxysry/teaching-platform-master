package com.xiyou.main.dao.contest;

import com.xiyou.main.entity.contest.CtGzIso;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author tangcan
 * @since 2019-07-22
 */
@Repository
public interface CtGzIsoMapper extends BaseMapper<CtGzIso> {

    List<CtGzIso> list(Integer contestId);
}
