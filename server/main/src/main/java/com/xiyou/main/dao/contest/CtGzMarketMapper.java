package com.xiyou.main.dao.contest;

import com.xiyou.main.entity.contest.CtGzMarket;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author tangcan
 * @since 2019-07-22
 */
@Repository
public interface CtGzMarketMapper extends BaseMapper<CtGzMarket> {

    List<CtGzMarket> list(Integer contestId);

    Integer getCmid(@Param("contestId") Integer contestId, @Param("market") String market);
}
