package com.xiyou.main.dao.contest;

import com.xiyou.main.entity.contest.CtGzMaterial;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xiyou.main.vo.contest.CashFolowEntity;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author tangcan
 * @since 2019-07-22
 */
@Repository
public interface CtGzMaterialMapper extends BaseMapper<CtGzMaterial> {

    /**
     * 原料规则表.费用
     * @param contestId
     * @return
     */
    Integer getGzMaterialMoney(Integer contestId);


    List<CtGzMaterial> list(Integer contestId);
}
