package com.xiyou.main.dao.contest;

import com.xiyou.main.entity.contest.CtGzOrder;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xiyou.main.entity.contest.CtMnChoose;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author tangcan
 * @since 2019-07-22
 */
@Repository
public interface CtGzOrderMapper extends BaseMapper<CtGzOrder> {

    List<CtMnChoose> getList(Integer contestId);
}
