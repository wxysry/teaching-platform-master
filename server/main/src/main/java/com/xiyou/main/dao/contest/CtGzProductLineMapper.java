package com.xiyou.main.dao.contest;

import com.xiyou.main.entity.contest.CtGzProductLine;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xiyou.main.vo.contest.CashFolowEntity;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author tangcan
 * @since 2019-07-22
 */
@Repository
public interface CtGzProductLineMapper extends BaseMapper<CtGzProductLine> {

    /**
     * 获取生产线信息P14
     * @param contestId
     * @return
     */
    List<CtGzProductLine> getProductTypeList(Integer contestId);

    /**
     * 该生产线的投资时间
     * @param cashFolowEntity
     * @return
     */
    Integer getProductLineInfo(CashFolowEntity cashFolowEntity);


}
