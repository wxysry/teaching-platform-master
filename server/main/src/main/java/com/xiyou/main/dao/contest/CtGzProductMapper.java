package com.xiyou.main.dao.contest;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xiyou.main.entity.contest.CtGzProduct;
import com.xiyou.main.vo.contest.YfProductVO;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;


/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author tangcan
 * @since 2019-07-22
 */
@Repository
public interface CtGzProductMapper extends BaseMapper<CtGzProduct> {

    List<YfProductVO> listYfProduct(@Param("studentId") Integer userId, @Param("contestId") Integer contestId);

    /**
     * 生产产品名称
     *
     * @param contestId
     * @return
     */
    List<String> getProductCPName(Integer contestId);


    List<CtGzProduct> getList(Integer contestId);

    Integer getCpid(@Param("contestId") Integer contestId, @Param("product") String product);

    CtGzProduct get(@Param("contestId") Integer contestId, @Param("cpId") Integer cpId);
}
