package com.xiyou.main.dao.contest;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xiyou.main.entity.contest.CtGzWorkshop;
import io.swagger.models.auth.In;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author tangcan
 * @since 2019-07-22
 */
@Repository
public interface CtGzWorkshopMapper extends BaseMapper<CtGzWorkshop> {

    /**
     * 厂房类型信息
     * @param contestId
     * @return
     */
    List<CtGzWorkshop> getGzWorkshopInfo(Integer contestId);

}
