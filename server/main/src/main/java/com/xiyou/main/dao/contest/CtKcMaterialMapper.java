package com.xiyou.main.dao.contest;

import com.xiyou.main.entity.contest.CtKcMaterial;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xiyou.main.vo.contest.CashFolowEntity;
import com.xiyou.main.vo.contest.MaterialVo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author tangcan
 * @since 2019-07-22
 */
@Mapper
@Repository
public interface CtKcMaterialMapper extends BaseMapper<CtKcMaterial> {

    /**
     * 原材料 R1-P5
     * 参数：
     *
     * @param cmId
     * @return
     */
    Integer getKcMaterial(@Param("cmId") Integer cmId, @Param("studentId") Integer studentId, @Param("contestId") Integer contestId);


    /**
     * P11-> 原料编号的剩余数量=剩余数量+dd_material中剩余时间为0的原料编号对应的数量
     *
     * @param cashFolowEntity
     */
    void updateKcMaterial(@Param("cashFolowEntity") CashFolowEntity cashFolowEntity);


    int getMaterialSum(@Param("studentId") Integer studentId, @Param("contestId") Integer contestId);

    List<CtKcMaterial> listNeed(@Param("contestId") Integer contestId, @Param("list") List<Integer> lineIds);

    List<MaterialVo> listKc(@Param("contestId") Integer contestId, @Param("studentId") Integer userId);

    List<MaterialVo> listSellKc(@Param("contestId") Integer contestId, @Param("studentId") Integer userId);

    void insertBatch(@Param("list") List<CtKcMaterial> ctKcMaterialList);
}
