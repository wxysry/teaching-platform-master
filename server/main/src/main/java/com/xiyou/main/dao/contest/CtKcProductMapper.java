package com.xiyou.main.dao.contest;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xiyou.main.entity.contest.CtKcProduct;
import com.xiyou.main.vo.contest.ProductVo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author tangcan
 * @since 2019-07-22
 */
@Mapper
@Repository
public interface CtKcProductMapper extends BaseMapper<CtKcProduct> {

    /**
     * 库存采购信息 P1-P5
     * 参数：产品编号
     *
     * @param cpId
     * @return
     */
    Integer getKCProductNumb(@Param("cpId") Integer cpId, @Param("studentId") Integer studentId, @Param("contestId") Integer contestId);


    int getProductSum(@Param("studentId") Integer studentId, @Param("contestId") Integer contestId);

    /**
     * P9
     * 更新产成品表，如果生产线表剩余生产时间为0，则在产成品表中update库存数量=生产线.生产产品相同的+1，如果有多条则增加多个
     */
    int updateKcNum(@Param("studentId") Integer studentId, @Param("contestId") Integer contestId);

    List<ProductVo> listKc(@Param("contestId") Integer contestId, @Param("studentId") Integer userId);

    List<ProductVo> listSellKc(@Param("contestId") Integer contestId, @Param("studentId") Integer userId);

    void insertBatch(@Param("list") List<CtKcProduct> ctKcProductList);

    void updateNum(@Param("contestId") Integer contestId,
                   @Param("studentId") Integer studentId,
                   @Param("cpid") Integer cpid,
                   @Param("num") Integer num);
}
