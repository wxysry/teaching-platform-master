package com.xiyou.main.dao.contest;

import com.xiyou.main.entity.contest.CtGzProductLine;
import com.xiyou.main.entity.contest.CtLine;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xiyou.main.vo.contest.OnlineLine;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author tangcan
 * @since 2019-07-22
 */
@Repository
public interface CtLineMapper extends BaseMapper<CtLine> {

    List<OnlineLine> listOnline(@Param("studentId") Integer userId, @Param("contestId") Integer contestId, @Param("date") Integer date);

    int getMaintenanceFee(CtLine line);

    List<CtGzProductLine> getLineInfo(@Param("list") List<Integer> lineIds);

    List<OnlineLine> listTransfer(@Param("studentId") Integer userId, @Param("contestId") Integer contestId);

    void updateDepFee(@Param("studentId") Integer userId, @Param("contestId") Integer contestId, @Param("date") Integer date);

    Integer getDepTotal(@Param("studentId") Integer userId, @Param("contestId") Integer contestId, @Param("date") Integer date);

    int getProductInProcess(CtLine line);

    int getEquipmentSum(@Param("studentId") Integer userId, @Param("contestId") Integer contestId);

    List<OnlineLine> listNoProducting(@Param("studentId") Integer userId, @Param("contestId") Integer contestId);

    /**
     * 显示line中PL_Remain_Date 不为空的数据
     *
     * @param userId
     * @param contestId
     * @return
     */
    List<OnlineLine> getListOnline(@Param("studentId") Integer userId, @Param("contestId") Integer contestId);

    /*
    如果剩余生产时间为0，则开产时间、剩余生产时间置空，如剩余生产时间>0,则开产时间不变，剩余生产时间=原剩余生产时间-1
     */
    int updateProductAddDateAndProductingDate(@Param("studentId") Integer studentId, @Param("contestId") Integer contestId);

    /*
    如果剩余时间为0，则置空，如>0，则-1；转产剩余时间为0，则置空剩余时间及转产时间，如>0，则-1
     */
    int updateRemainDateAndTransferAddDate(@Param("studentId") Integer studentId, @Param("contestId") Integer contestId, @Param("date") Integer date);

    int add(CtLine line);

    void updateBatch(@Param("list") List<CtLine> lines);

    List<CtLine> getList(@Param("contestId") Integer contestId, @Param("studentId") Integer studentId);
}
