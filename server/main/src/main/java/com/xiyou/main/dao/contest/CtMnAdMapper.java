package com.xiyou.main.dao.contest;

import com.xiyou.main.entity.contest.CtMnAd;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author tangcan
 * @since 2019-07-25
 */
@Repository
public interface CtMnAdMapper extends BaseMapper<CtMnAd> {

    List<CtMnAd> getAdList(@Param("contestId") Integer contestId,
                           @Param("studentId") Integer studentId,
                           @Param("year") Integer year);

    void insertBatch(@Param("list") List<CtMnAd> ctMnAdList);

}
