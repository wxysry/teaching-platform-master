package com.xiyou.main.dao.contest;

import com.xiyou.main.entity.contest.CtMnChoose;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author tangcan
 * @since 2019-08-27
 */
@Repository
public interface CtMnChooseMapper extends BaseMapper<CtMnChoose> {

    List<CtMnChoose> getOrderList(@Param("contestId") Integer contestId,
                                  @Param("studentId") Integer studentId,
                                  @Param("cmid") Integer cmid,
                                  @Param("cpid") Integer cpid,
                                  @Param("year") Integer year);

    void insertBatch(@Param("list") List<CtMnChoose> ctMnChooseList);

    List<CtMnChoose> getSales(@Param("contestId") Integer contestId,
                              @Param("studentId") Integer studentId,
                              @Param("cmid") Integer cmid,
                              @Param("year") int year);
}
