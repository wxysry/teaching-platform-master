package com.xiyou.main.dao.contest;

import com.xiyou.main.entity.contest.CtProfitChart;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.stereotype.Repository;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author tangcan
 * @since 2019-07-22
 */
@Repository
public interface CtProfitChartMapper extends BaseMapper<CtProfitChart> {

}
