package com.xiyou.main.dao.contest;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.xiyou.main.entity.contest.CtSubject;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xiyou.main.entity.exam.Resources;
import com.xiyou.main.params.contest.SubjectParam;
import com.xiyou.main.vo.contest.TeacherSubject;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author tangcan
 * @since 2019-08-25
 */
@Repository
public interface CtSubjectMapper extends BaseMapper<CtSubject> {

    Page<CtSubject> listByAdmin(Page<CtSubject> page, @Param("param") SubjectParam subjectParam);

    Page<CtSubject> listByTeacher(Page<CtSubject> page, @Param("param") SubjectParam subjectParam);

    CtSubject getBySubjectNumber(Integer subjectNumber);

    CtSubject getById(Integer subjectId);

    List<TeacherSubject> getTeacherSubject(Integer teacherId);

    void updateUpload(Integer subjectNumber);

    Resources getSpyAttachment(Integer contestId);

    Resources getRuleAttachment(Integer contestId);

    void removeGz(Integer subjectNumber);

    void removeMainTableData(@Param("contestId") Integer contestId, @Param("studentId") Integer studentId);

    void removeLikeTableData(@Param("contestId") Integer contestId, @Param("studentId") Integer studentId);

    void insertLikeTableDataToMainTable(@Param("contestId") Integer contestId, @Param("studentId") Integer studentId);

    void insertMainTableToLikeTableData(@Param("contestId") Integer contestId, @Param("studentId") Integer studentId);

    void update(CtSubject ctSubject);

    CtSubject getByContestId(Integer contestId);

    void removeOrder(@Param("contestId") Integer contestId, @Param("studentId") Integer studentId);

    void backupOrder(@Param("contestId") Integer contestId, @Param("studentId") Integer studentId);

    void removeSeasonLikeTableData(@Param("contestId") Integer contestId, @Param("studentId") Integer studentId);

    void insertMainTableToSeasonLikeTableData(@Param("contestId") Integer contestId, @Param("studentId") Integer studentId);

    void insertLikeTableDataToReasonTable(@Param("contestId") Integer contestId, @Param("studentId") Integer studentId);

    void insertSeasonLikeTableToMainTable (@Param("contestId") Integer contestId, @Param("studentId") Integer studentId);

    List<Integer> seasonDataSum (@Param("contestId") Integer contestId, @Param("studentId") Integer studentId);
}
