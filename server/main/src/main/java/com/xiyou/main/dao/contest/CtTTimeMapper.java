package com.xiyou.main.dao.contest;

import com.xiyou.main.entity.contest.CtTTime;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import com.xiyou.main.vo.contest.CashFolowEntity;
import org.springframework.stereotype.Repository;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author tangcan
 * @since 2019-07-22
 */
@Repository
public interface CtTTimeMapper extends BaseMapper<CtTTime> {

    /**
     * @Description: 更新time表，当前时间=当前时间+1
     * @Author:zhengxiaodong
     * @Param: [cashFolowEntity]
     * @return: void
     */
    void updateStudentCtTime(CashFolowEntity cashFolowEntity);

    /**
     * @Description: time表的最新date
     * @Author:zhengxiaodong
     * @Param: [cashFolowEntity]
     * @Return: java.lang.Integer
     * @Date: 2019/7/25
     */
    Integer getStudentLatestDate(CashFolowEntity cashFolowEntity);

    int addDate(@Param("studentId") Integer studentId, @Param("contestId") Integer contestId, @Param("time") int time);

}