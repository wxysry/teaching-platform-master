package com.xiyou.main.dao.contest;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xiyou.main.entity.contest.CtWorkshop;
import com.xiyou.main.vo.contest.CashFolowEntity;
import com.xiyou.main.vo.contest.CtWorkshopEntity;
import com.xiyou.main.vo.contest.WorkshopVo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;


/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author tangcan
 * @since 2019-07-22
 */
@Mapper
@Repository
public interface CtWorkshopMapper extends BaseMapper<CtWorkshop> {

    /**
     * 工厂容量减一
     *
     * @param workshopId
     * @return
     */
    boolean tallyDown(Integer workshopId);

    /**
     * PPT6->workshop info[厂房、生产线信息]
     *
     * @param cashFolowEntity
     * @return
     */
    List<CtWorkshopEntity> getWorkShopInfo(CashFolowEntity cashFolowEntity);

    int getRentFee(CtWorkshop workshop);

    /**
     * 工厂容量加一
     *
     * @param plWid
     * @return
     */
    boolean tallyUp(Integer plWid);

    List<WorkshopVo> list(Map<String, Object> queryMap);


    int getWorkshopFeeSum(@Param("studentId") Integer studentId, @Param("contestId") Integer contestId);

    Integer queryWorkshopRentMoney(Map<String, Object> queryMap);


    List<Map<String, Object>> baseInfoList(@Param("studentId") Integer studentId, @Param("contestId") Integer contestId);

    List<WorkshopVo> listWorkshop(List<Integer> integers);


    List<CtWorkshop> getList(@Param("contestId") Integer contestId, @Param("studentId") Integer studentId);

    void updateWpayDate(Map<String, Object> queryMap);
}
