package com.xiyou.main.dao.contest;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xiyou.main.entity.contest.CtXzOrder;
import com.xiyou.main.vo.contest.CashFolowEntity;
import com.xiyou.main.vo.contest.XzOrderVo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author tangcan
 * @since 2019-07-22
 */
@Mapper
@Repository
public interface CtXzOrderMapper extends BaseMapper<CtXzOrder> {


    /**
     * 销售收入
     * 参数：studentId、contestId
     *
     * @param cashFolowEntity
     * @return
     */
    Integer getStudentSalesProfit(CashFolowEntity cashFolowEntity);

    /**
     * 直接成本[本年]
     * 参数：studentId、contestId、currentYear
     *
     * @param cashFolowEntity
     * @return
     */
    Integer getStudentProductDevCost(CashFolowEntity cashFolowEntity);


    List<CtXzOrder> list(CtXzOrder order);

    Map<String, BigDecimal> getSalesAndDirectCost(@Param("studentId") Integer studentId, @Param("contestId") Integer contestId, @Param("year") int year);

    List<XzOrderVo> listDeliveryOrder(Map<String, Object> queryMap);

    List<CtXzOrder> getList(@Param("contestId") Integer contestId, @Param("studentId") Integer studentId);

    Integer getSales(@Param("contestId") Integer contestId, @Param("studentId") Integer studentId, @Param("cmid") Integer cmid, @Param("year") int year);

    void removeNowYear(@Param("contestId") Integer contestId, @Param("studentId") Integer studentId, @Param("year") int year);
}
