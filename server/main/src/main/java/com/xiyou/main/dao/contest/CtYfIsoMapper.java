package com.xiyou.main.dao.contest;

import com.xiyou.main.entity.contest.CtYfIso;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xiyou.main.vo.contest.MarketReturnEntity;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author tangcan
 * @since 2019-07-22
 */
@Mapper
@Repository
public interface CtYfIsoMapper extends BaseMapper<CtYfIso> {

    void update(CtYfIso yfIso);

    /**
     * ISO认证
     *
     * @return
     */
    List<String> getISONameList(Integer marketId);

    /**
     * ISO认证 [ISO9000、ISO14000]
     *
     * @param isoId
     * @return
     */
    MarketReturnEntity getYFISOProportion(@Param("isoId") Integer isoId, @Param("studentId") Integer studentId, @Param("contestId") Integer contestId);


    List<String> getIsoList(@Param("studentId") Integer studentId, @Param("contestId") Integer contestId);

    /**
     * @Author: tangcan
     * @Description: iso投资列表 p30
     * @Param: [studentId, contestId]
     * @date: 2019/9/1
     */
    List<Map<String, Object>> getIsoInvestList(@Param("studentId") Integer studentId, @Param("contestId") Integer contestId);

    Integer getIsoInvestSum(@Param("list") List<Integer> isoIds);

    void updateISOInvest(@Param("studentId") Integer studentId, @Param("contestId") Integer contestId, @Param("list") List<Integer> isoIds);

    void insertBatch(@Param("list") List<CtYfIso> ctYfIsoList);

    List<CtYfIso> getYfFinishList(@Param("contestId") Integer contestId, @Param("studentId") Integer studentId);

    List<CtYfIso> getList(@Param("contestId") Integer contestId, @Param("studentId") Integer studentId);
}
