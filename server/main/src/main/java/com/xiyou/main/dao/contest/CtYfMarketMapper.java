package com.xiyou.main.dao.contest;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xiyou.main.entity.contest.CtYfMarket;
import com.xiyou.main.vo.contest.MarketReturnEntity;
import com.xiyou.main.vo.contest.MarketVo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author tangcan
 * @since 2019-07-22
 */
@Mapper
@Repository
public interface CtYfMarketMapper extends BaseMapper<CtYfMarket> {

    void update(CtYfMarket yfMarket);

    /**
     * 市场准入
     *
     * @return
     */
    List<String> getYFMarketNames(@Param("studentId") Integer studentId, @Param("contestId") Integer contestId);

    /**
     * 本地、区域、国内、亚洲、国际占比
     * 参数：市场编号
     *
     * @param cmId
     * @return
     */
    MarketReturnEntity getYFMarketProportion(@Param("cmId") Integer cmId, @Param("studentId") Integer studentId, @Param("contestId") Integer contestId);

    List<String> getHadYfFinishMarkets(@Param("studentId") Integer studentId, @Param("contestId") Integer contestId);

    List<String> getMarketList(@Param("studentId") Integer studentId, @Param("contestId") Integer contestId);

    List<MarketVo> listYfMarket(@Param("studentId") Integer studentId, @Param("contestId") Integer contestId);

    void insertBatch(@Param("list") List<CtYfMarket> ctYfMarketList);

    List<CtYfMarket> getList(@Param("contestId") Integer contestId, @Param("studentId") Integer studentId);

}
