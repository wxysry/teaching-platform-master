package com.xiyou.main.dao.contest;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xiyou.main.entity.contest.CtYfProduct;
import com.xiyou.main.vo.contest.MarketReturnEntity;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author tangcan
 * @since 2019-07-22
 */
@Mapper
@Repository
public interface CtYfProductMapper extends BaseMapper<CtYfProduct> {

    int update(CtYfProduct yfProduct);

    /**
     * 生产资格
     *
     * @return
     */
    List<String> getProduceStatus();

    /**
     * P1-P5
     *
     * @param productId
     * @return
     */
    MarketReturnEntity getYFProductProportion(@Param("productId") Integer productId, @Param("studentId") Integer studentId, @Param("contestId") Integer contestId);

    void updateEndingSeason(@Param("studentId") Integer userId, @Param("contestId") Integer contestId, @Param("date") Integer date);

    List<String> getProductList(@Param("studentId") Integer studentId, @Param("contestId") Integer contestId);

    void insertBatch(@Param("list") List<CtYfProduct> ctYfProductList);

    List<CtYfProduct> getList(@Param("contestId") Integer contestId, @Param("studentId") Integer studentId);
}
