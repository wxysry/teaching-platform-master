package com.xiyou.main.dao.contest;


import com.xiyou.main.pojo.contest.*;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author xingzi
 * @date 2019 07 23  14:20
 */
@Repository
public interface GzExcelMapper {
    void insertAd(@Param("list") List<CtGzAdModel> list);

    void insertCs(@Param("list") List<CtGzCsModel> list);

    void insertIso(@Param("list") List<CtGzIsoModel> list);

    void insertMarket(@Param("list") List<CtGzMarketModel> list);

    void insertMaterial(@Param("list") List<CtGzMaterialModel> list);

    void insertOrder(@Param("list") List<CtGzOrderModel> list);

    void insertProducing(@Param("list") List<CtGzProducingModel> list);

    void insertProductLine(@Param("list") List<CtGzProductLineModel> list);

    void insertProduct(@Param("list") List<CtGzProductModel> list);

    void insertWorkshop(@Param("list") List<CtGzWorkshopModel> list);

    void removeRestCs(@Param("list") List<CtGzCsModel> ctGzCsModelList);

    void removeRestAd(@Param("list") List<CtGzAdModel> ctGzAdModelList);

    void removeRestIso(@Param("list") List<CtGzIsoModel> ctGzIsoModelList);

    void removeRestOrder(@Param("list") List<CtGzOrderModel> ctGzOrderModelList);

    void removeRestMarket(@Param("list") List<CtGzMarketModel> ctGzMarketModelList);

    void removeRestMaterial(@Param("list") List<CtGzMaterialModel> ctGzMaterialModelList);

    void removeRestProduct(@Param("list") List<CtGzProductModel> ctGzProductModelList);

    void removeRestProducing(@Param("list") List<CtGzProducingModel> ctGzProducingModelList);

    void removeRestProductLine(@Param("list") List<CtGzProductLineModel> ctGzProductLineModelList);

    void removeRestWorkshop(@Param("list") List<CtGzWorkshopModel> ctGzWorkshopModelList);
}
