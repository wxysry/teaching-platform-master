package com.xiyou.main.dao.contest;

import com.xiyou.main.entity.contest.ContestGroup;
import com.xiyou.main.entity.contest.UserContestGroup;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author tangcan
 * @since 2019-07-09
 */
@Repository
public interface UserContestGroupMapper extends BaseMapper<UserContestGroup> {

    List<ContestGroup> listAll();

    List<UserContestGroup> getByUserIdList(@Param("list") List<Integer> userIdList);

    int insertBatch(@Param("list") List<UserContestGroup> userContestGroupList);
}
