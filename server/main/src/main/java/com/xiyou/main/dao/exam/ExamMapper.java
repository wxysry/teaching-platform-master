package com.xiyou.main.dao.exam;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.xiyou.main.entity.exam.Exam;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xiyou.main.params.exam.ExamPaperParam;
import com.xiyou.main.params.exam.ExamStudentParam;
import com.xiyou.main.vo.exam.ExamPaper;
import com.xiyou.main.vo.exam.ExamStudent;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author tangcan
 * @since 2019-07-10
 */
@Repository
public interface ExamMapper extends BaseMapper<Exam> {

    int insertBatch(@Param("list") List<Exam> examList);

    Page<ExamPaper> getExamPaperPage(Page<ExamPaper> page, @Param("param") ExamPaperParam examPaperParam);

    ExamPaper getByExamId(@Param("examId") Integer examId);

    Page<ExamStudent> getStudentList(Page<ExamStudent> page, @Param("param") ExamStudentParam examStudentParam);

    void updateExamTime(@Param("paperId") Integer paperId, @Param("examTime") Integer examTime);
}
