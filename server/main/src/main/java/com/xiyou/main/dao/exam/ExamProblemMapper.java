package com.xiyou.main.dao.exam;

import com.xiyou.main.entity.exam.ExamProblem;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xiyou.main.vo.exam.ExamAnalysis;
import com.xiyou.main.vo.exam.ExamProblemInfo;
import com.xiyou.main.vo.exam.ExamScore;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author tangcan
 * @since 2019-07-10
 */
@Repository
public interface ExamProblemMapper extends BaseMapper<ExamProblem> {

    int insertBatch(@Param("list") List<ExamProblem> examProblemList);

    List<ExamProblemInfo> getByExamId(@Param("examId") Integer examId);

    int autoJudge(@Param("examId") Integer examId);

    void autoJudgeByExamIds(@Param("list") List<Integer> examIdList);

    int updateBatch(@Param("list") List<ExamProblem> examProblemList);

    List<ExamAnalysis> analysis(@Param("paperId") Integer paperId);

    List<ExamScore> getScoreList(@Param("paperId") Integer paperId);
}
