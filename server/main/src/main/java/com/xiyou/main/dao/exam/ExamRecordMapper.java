package com.xiyou.main.dao.exam;

import com.xiyou.main.entity.exam.ExamRecord;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author tangcan
 * @since 2019-07-10
 */
@Repository
public interface ExamRecordMapper extends BaseMapper<ExamRecord> {

    List<ExamRecord> listByPaperId(@Param("paperId") Integer paperId);
}
