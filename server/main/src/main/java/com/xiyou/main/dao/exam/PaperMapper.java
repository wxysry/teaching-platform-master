package com.xiyou.main.dao.exam;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.xiyou.main.entity.exam.Paper;
import com.xiyou.main.params.exam.PaperParam;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author tangcan
 * @since 2019-07-04
 */
@Repository
public interface PaperMapper extends BaseMapper<Paper> {
    int insert(Paper paper);

    Page<Paper> getPage(Page<Paper> page, @Param("param") PaperParam paperParam);
}
