package com.xiyou.main.dao.exam;

import com.xiyou.main.entity.exam.PaperProblem;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xiyou.main.vo.exam.ExamProblemInfo;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author tangcan
 * @since 2019-07-04
 */
@Repository
public interface PaperProblemMapper extends BaseMapper<PaperProblem> {

    int insertBatch(@Param("list") List<PaperProblem> paperProblemList);

    Double countTotalScore(@Param("paperId") Integer paperId);

    List<ExamProblemInfo> listByPaperId(@Param("paperId") Integer paperId);
}
