package com.xiyou.main.dao.exam;

import com.xiyou.main.entity.exam.PaperStudent;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author tangcan
 * @since 2019-07-16
 */
@Repository
public interface PaperStudentMapper extends BaseMapper<PaperStudent> {

    int insertBatch(@Param("list") List<PaperStudent> paperStudentList);

    List<Integer> getStudentIdsByPaperId(@Param("paperId") Integer paperId);
}
