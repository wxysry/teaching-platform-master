package com.xiyou.main.dao.exam;

import com.xiyou.main.entity.exam.PptGroup;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xiyou.main.entity.exam.VideoGroup;

import java.util.List;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author tangcan
 * @since 2019-06-28
 */
public interface PptGroupMapper extends BaseMapper<PptGroup> {

    int insert(PptGroup group);

    List<VideoGroup> listByTeacher(Integer userId);

    List<PptGroup> listByStudent(Integer studentId);
}
