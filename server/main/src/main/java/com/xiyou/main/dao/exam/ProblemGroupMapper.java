package com.xiyou.main.dao.exam;

import com.xiyou.main.entity.exam.ProblemGroup;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author tangcan
 * @since 2019-07-07
 */
@Repository
public interface ProblemGroupMapper extends BaseMapper<ProblemGroup> {

    String getGroupAndSubgroupName(@Param("subgroupId") Integer subgroupId);

    List<ProblemGroup> getGroupAndSubgroupNameList(@Param("list") List<Integer> subgroupIdList);

    List<ProblemGroup> getSubGroupList(@Param("userId") Integer userId,
                                       @Param("fatherId") Integer fatherId,
                                       @Param("subgroupId") Integer subgroupId);

    List<ProblemGroup> getGroupListByUserId(@Param("userId") Integer userId);

    List<ProblemGroup> getAllList();

    int insert(ProblemGroup problemGroup);
}
