package com.xiyou.main.dao.exam;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.xiyou.main.entity.exam.Problem;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xiyou.main.params.exam.ProblemInfo;
import com.xiyou.main.params.exam.ProblemParam;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author tangcan
 * @since 2019-06-24
 */
@Repository
public interface ProblemMapper extends BaseMapper<Problem> {

    int insertBatch(@Param("list") List<Problem> problemList);

    Problem selectById(Integer id);

    Page<ProblemInfo> getList(Page<Problem> page,
                              @Param("teacherId") Integer teacherId,
                              @Param("param") ProblemParam problemParam);

    Problem getByTeacher(@Param("teacherId") Integer teacherId, @Param("problemId") Integer problemId);

    boolean update(Problem problem);
}
