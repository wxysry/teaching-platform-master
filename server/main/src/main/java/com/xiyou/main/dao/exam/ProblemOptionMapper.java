package com.xiyou.main.dao.exam;

import com.xiyou.main.entity.exam.ProblemOption;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author tangcan
 * @since 2019-06-24
 */
@Repository
public interface ProblemOptionMapper extends BaseMapper<ProblemOption> {

    int insertBatch(@Param("list") List<ProblemOption> list);

    List<ProblemOption> getList(Integer problemId);

    List<ProblemOption> getByProblemIdList(@Param("list") List<Integer> problemIdList);
}
