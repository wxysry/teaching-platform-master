package com.xiyou.main.dao.exam;

import com.xiyou.main.entity.exam.ProblemType;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author tangcan
 * @since 2019-06-24
 */
public interface ProblemTypeMapper extends BaseMapper<ProblemType> {

}
