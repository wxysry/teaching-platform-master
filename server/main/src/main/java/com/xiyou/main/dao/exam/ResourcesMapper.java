package com.xiyou.main.dao.exam;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.xiyou.main.entity.exam.Resources;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xiyou.main.params.exam.ResourcesParam;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author tangcan
 * @since 2019-07-07
 */
@Repository
public interface ResourcesMapper extends BaseMapper<Resources> {

    int insertBatch(@Param("list") List<Resources> resourcesList);

    Page<Resources> getPage(Page<Resources> page, @Param("param") ResourcesParam resourcesParam);

    Resources get(String fileName);
}
