package com.xiyou.main.dao.exam;

import com.xiyou.main.entity.exam.SysRole;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author tangcan
 * @since 2019-06-24
 */
@Repository
public interface SysRoleMapper extends BaseMapper<SysRole> {

    SysRole getByUserAccount(@Param("account") String account);
}
