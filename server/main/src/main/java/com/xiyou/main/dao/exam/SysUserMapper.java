package com.xiyou.main.dao.exam;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xiyou.main.entity.exam.SysUser;
import com.xiyou.main.params.exam.UserParam;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author tangcan
 * @since 2019-06-24
 */
@Repository
public interface SysUserMapper extends BaseMapper<SysUser> {
    int insert(SysUser sysUser);

    SysUser getAllInfo(@Param("user") SysUser sysUser);

    Page<SysUser> getTeacherPage(Page<SysUser> page, @Param("param") UserParam userParam);

    int forbid(Integer userId);

    int updatePwd(@Param("userId") Integer userId, @Param("password") String password);

    int insertBatch(@Param("list") List<SysUser> list);

    boolean setForbid(@Param("userId") Integer userId, @Param("forbid") Integer forbid);

    Page<SysUser> getStudentPage(Page<SysUser> page, @Param("param") UserParam userParam);

    List<SysUser> getStudentList(@Param("teacherId") Integer teacherId);

}
