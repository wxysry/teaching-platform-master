package com.xiyou.main.dao.exam;

import com.xiyou.main.entity.exam.TeachClass;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author tangcan
 * @since 2019-06-26
 */
@Repository
public interface TeachClassMapper extends BaseMapper<TeachClass> {

    int insertBatch(@Param("list") List<TeachClass> list);

    List<Integer> getStudentIdList(Integer teacherId);
}
