package com.xiyou.main.dao.exam;

import com.xiyou.main.entity.exam.UserPptGroup;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.io.Serializable;
import java.util.Collection;
import java.util.List;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author tangcan
 * @since 2019-06-28
 */
@Repository
public interface UserPptGroupMapper extends BaseMapper<UserPptGroup> {
    void insertBatch(@Param("list") List<UserPptGroup> userPptGroupList);

    List<UserPptGroup> getByUserIdList(@Param("list") List<Integer> userIdList);

    boolean removeByGroupIds(Collection<? extends Serializable> collection);

}
