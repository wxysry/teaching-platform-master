package com.xiyou.main.dao.exam;

import com.xiyou.main.entity.exam.SysUser;
import com.xiyou.main.entity.exam.UserPpt;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author tangcan
 * @since 2019-10-28
 */
public interface UserPptMapper extends BaseMapper<UserPpt> {

    void insertBatch(@Param("pptId") Integer pptId, @Param("list") List<Integer> studentIdList);

    List<SysUser> getStudents(@Param("pptId") Integer pptId);
}
