package com.xiyou.main.dao.exam;

import com.xiyou.main.entity.exam.UserProblemGroup;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author tangcan
 * @since 2019-06-25
 */
@Repository
public interface UserProblemGroupMapper extends BaseMapper<UserProblemGroup> {

    void insertBatch(@Param("list") List<UserProblemGroup> userProblemGroupList);

    List<UserProblemGroup> getByUserIdList(@Param("list") List<Integer> userIdList);
}
