package com.xiyou.main.dao.exam;

import com.xiyou.main.entity.exam.UserVideoGroup;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.io.Serializable;
import java.util.Collection;
import java.util.List;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author tangcan
 * @since 2019-06-28
 */
@Repository
public interface UserVideoGroupMapper extends BaseMapper<UserVideoGroup> {

    void insertBatch(@Param("list") List<UserVideoGroup> userVideoGroupList);

    List<UserVideoGroup> getByUserIdList(@Param("list") List<Integer> userIdList);

    boolean removeByGroupIds(Collection<? extends Serializable> collection);
}
