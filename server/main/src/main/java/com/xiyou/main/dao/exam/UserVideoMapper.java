package com.xiyou.main.dao.exam;

import com.xiyou.main.entity.exam.SysUser;
import com.xiyou.main.entity.exam.UserVideo;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author tangcan
 * @since 2019-10-28
 */
public interface UserVideoMapper extends BaseMapper<UserVideo> {

    void insertBatch(@Param("videoId") Integer videoId, @Param("list") List<Integer> studentIdList);

    List<SysUser> getStudents(Integer videoId);
}
