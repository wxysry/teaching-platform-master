package com.xiyou.main.dao.exam;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.xiyou.main.entity.exam.VideoCourse;
import com.xiyou.main.params.exam.CourseQueryParam;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author tangcan
 * @since 2019-06-28
 */
@Repository
public interface VideoCourseMapper extends BaseMapper<VideoCourse> {

    Page<VideoCourse> listByUser(Page<VideoCourse> page, @Param("param") CourseQueryParam param);

    boolean removeByGroupId(java.util.Collection<? extends java.io.Serializable> collection);

    Page<VideoCourse> listByStudent(Page<VideoCourse> page, @Param("param") CourseQueryParam param);
}
