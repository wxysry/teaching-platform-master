package com.xiyou.main.dao.exam;

import com.xiyou.main.entity.exam.VideoGroup;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author tangcan
 * @since 2019-06-28
 */
public interface VideoGroupMapper extends BaseMapper<VideoGroup> {

    int insert(VideoGroup group);

    List<VideoGroup> listByTeacher(@Param("userId") Integer userId);


    List<VideoGroup> listByStudent(@Param("studentId") Integer studentId);
}
