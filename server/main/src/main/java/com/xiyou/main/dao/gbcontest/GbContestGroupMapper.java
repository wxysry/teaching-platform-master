package com.xiyou.main.dao.gbcontest;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xiyou.main.entity.gbcontest.GbContestGroup;
import org.springframework.stereotype.Repository;
import java.util.List;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author tangcan
 * @since 2019-08-25
 */
@Repository
public interface GbContestGroupMapper extends BaseMapper<GbContestGroup> {
    int insert(GbContestGroup contestGroup);

    List<GbContestGroup> getListByTeacher(Integer teacherId);
}
