package com.xiyou.main.dao.gbcontest;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.xiyou.main.entity.gbcontest.GbContest;
import com.xiyou.main.params.contest.ContestParam;
import com.xiyou.main.vo.gbcontest.GbMarketForecast;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author tangcan
 * @since 2019-08-25
 */
@Repository
public interface GbContestMapper extends BaseMapper<GbContest> {

    Page<GbContest> getPage(Page<GbContest> page, @Param("param") ContestParam contestParam);

    int getUnEndContest(@Param("teacherId") Integer teacherId);

    int publish(@Param("teacherId") Integer teacherId, @Param("contestId") Integer contestId);

    Page<GbContest> getStudentContestPage(Page<GbContest> page, @Param("param") ContestParam contestParam);

    //根据竞赛contestId获取subjectNum
    GbContest getObjByContestId(@Param("contestId") Integer contestId);

    //暂停竞赛
    void pauseContest(@Param("contestId") Integer contestId);

    //恢复竞赛
    void recoverContest(@Param("contestId") Integer contestId);

    Page<GbContest> getPageAdmin(Page<GbContest> page, @Param("param") ContestParam contestParam);


    /**
     * 删除当前考试的所有数据
     * @param contestId
     */
    void deleteData(Integer contestId);

    /**
     * 删除当前考试的备份数据
     * @param contestId
     */
    void deleteLikeData(Integer contestId);

    /**
     * 市场预测表1 - 总数量
     * @param contestId
     * @return
     */
    List<GbMarketForecast> marketForecast1(Integer contestId);
    /**
     * 市场预测表1 - 总价/总数量
     * @param contestId
     * @return
     */
    List<GbMarketForecast> marketForecast2(Integer contestId);
    /**
     * 市场预测表1 - 订单总量
     * @param contestId
     * @return
     */
    List<GbMarketForecast> marketForecast3(Integer contestId);
}
