package com.xiyou.main.dao.gbcontest;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.xiyou.main.entity.gbcontest.GbContestStudent;
import com.xiyou.main.params.contest.ContestStudentParam;
import com.xiyou.main.vo.contest.ContestScore;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author tangcan
 * @since 2019-08-25
 */
@Repository
public interface GbContestStudentMapper extends BaseMapper<GbContestStudent> {

    int insertBatch(@Param("contestId") Integer contestId, @Param("list") List<Integer> studentIds);

    void updateStart(@Param("contestId") Integer contestId, @Param("studentId") Integer studentId);

    Page<GbContestStudent> getPage(Page<GbContestStudent> page, @Param("param") ContestStudentParam contestStudentParam);

    List<Integer> getStudentIdList(Integer contestId);

    int saveProgress(@Param("studentId") Integer studentId, @Param("contestId") Integer contestId, @Param("date") Integer date, @Param("progress") String progress);

    GbContestStudent get(@Param("contestId") Integer contestId, @Param("studentId") Integer studentId);

    void updateScore(@Param("id") Integer id, @Param("score") Double score);


    void removeProgress(@Param("contestId") Integer contestId, @Param("studentId") Integer studentId);

    List<ContestScore> getScoreList(@Param("contestId") Integer contestId);

    void updateErrorReportYear(GbContestStudent gbContestStudent);


    List<GbContestStudent> getListByContestId(@Param("contestId") Integer contestId);

    List<GbContestStudent> getStudentIdAndAccountList(Integer contestId);

    List<Integer> getRestoreTime(Integer contestId, Integer studentId);
}
