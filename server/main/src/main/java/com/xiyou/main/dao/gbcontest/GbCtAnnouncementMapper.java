package com.xiyou.main.dao.gbcontest;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xiyou.main.entity.gbcontest.GbCtAnnouncement;
import org.springframework.stereotype.Repository;

/**
 * @author 吕辉耀
 * @PROJECT_NAME: teaching-platform-master-new
 * @Description:
 * @creat 2023-07-15 18
 */
@Repository
public interface GbCtAnnouncementMapper extends BaseMapper<GbCtAnnouncement> {
}
