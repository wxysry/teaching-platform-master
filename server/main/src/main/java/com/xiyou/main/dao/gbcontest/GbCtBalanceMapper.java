package com.xiyou.main.dao.gbcontest;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xiyou.main.entity.gbcontest.GbCtBalance;
import com.xiyou.main.vo.gbcontest.GbCashFolowEntity;
import com.xiyou.main.vo.newcontest.NewCashFolowEntity;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author wangxingyu
 * @since 2023-06-11
 */
@Mapper
@Repository
public interface GbCtBalanceMapper extends BaseMapper<GbCtBalance> {

    /**
     * 获取最大贷款额度[上一年所有者权益]
     *
     * @param cashFolowEntity
     * @return
     */
    Integer getBalanceMaxLoanLimit(GbCashFolowEntity cashFolowEntity);


    Integer getRecentYEarHaveEquity(@Param("studentId") Integer studentId, @Param("contestId") Integer contestId, @Param("nowYear") Integer nowYear);

    int maxTotalEquityPreYear(@Param("studentId") Integer studentId, @Param("contestId") Integer contestId, @Param("currentYear") int currentYear);

    //获取企业资产负债表
    List<GbCtBalance>  getListByContestId(@Param("contestId") Integer contestId,@Param("nowYear") Integer nowYear);

}
