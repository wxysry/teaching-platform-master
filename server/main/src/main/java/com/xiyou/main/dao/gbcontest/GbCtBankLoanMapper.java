package com.xiyou.main.dao.gbcontest;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xiyou.main.entity.gbcontest.GbCtBankLoan;
import com.xiyou.main.vo.gbcontest.GbCashFolowEntity;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author wangxingyu
 * @since 2023-06-11
 */
@Mapper
@Repository
public interface GbCtBankLoanMapper extends BaseMapper<GbCtBankLoan> {

    /**
     * 长贷总额
     * 参数：studentId、contestId、blType{1：长期贷款，2：短期贷款}、currentTime
     *
     * @param cashFolowEntity
     * @param blType
     * @return
     */
    Integer getStudentLongAndShortTermLoansMoney(@Param("cashFolowEntity") GbCashFolowEntity cashFolowEntity,
                                                 @Param("blType") Integer blType);

    /**
     * bank_loan中还款时间大于等于当前时间的贷款类型为1和2的总额
     *
     * @param cashFolowEntity
     * @return
     */
    Integer getBackLoanAmount(GbCashFolowEntity cashFolowEntity);

    /**
     * back_loan中贷款类型为2、且归还时间=（当前时间+1）的金额
     * 参数：studentId、contestId、currentTime+1
     *
     * @param cashFolowEntity
     * @return
     */
    Integer getCurrentMoney(GbCashFolowEntity cashFolowEntity);

    List<GbCtBankLoan> getStudentLongAndShortTermLoansMoneyList(@Param("cashFolowEntity") GbCashFolowEntity cashFolowEntity,
                                                              @Param("blType") Integer blType);

    Integer getTotalEquity(GbCashFolowEntity cashFolowEntity);

    Integer getFee(GbCashFolowEntity cashFolowEntity);

    List<GbCtBankLoan> getList(@Param("contestId") Integer contestId, @Param("studentId") Integer studentId);
}
