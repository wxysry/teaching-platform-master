package com.xiyou.main.dao.gbcontest;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xiyou.main.entity.gbcontest.GbCtCapital;
import com.xiyou.main.entity.gbcontest.GbCtYfMarket;
import com.xiyou.main.vo.gbcontest.GbMarketReturnEntity;
import com.xiyou.main.vo.gbcontest.GbMarketVo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author wangxingyu
 * @since 2023-06-11
 */
@Mapper
@Repository
public interface GbCtCapitalMapper extends BaseMapper<GbCtCapital> {
}
