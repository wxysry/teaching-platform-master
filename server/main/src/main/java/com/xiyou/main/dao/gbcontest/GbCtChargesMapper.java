package com.xiyou.main.dao.gbcontest;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xiyou.main.entity.gbcontest.GbCtCharges;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author tangcan
 * @since 2019-07-22
 */
@Repository
public interface GbCtChargesMapper extends BaseMapper<GbCtCharges> {

    int addBySys(GbCtCharges charges);

    int updateTotal(@Param("chargesId") Integer chargesId);

    Integer getTotal(@Param("studentId") Integer studentId, @Param("contestId") Integer contestId, @Param("year") int year, @Param("isxt") int isxt);

    List<GbCtCharges>  getListByContestId(@Param("contestId") Integer contestId, @Param("nowYear") Integer nowYear);
}
