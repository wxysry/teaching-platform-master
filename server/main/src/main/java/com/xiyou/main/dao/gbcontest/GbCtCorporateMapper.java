package com.xiyou.main.dao.gbcontest;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xiyou.main.entity.gbcontest.GbCtCorporate;
import org.springframework.stereotype.Repository;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author tangcan
 * @since 2019-08-25
 */
@Repository
public interface GbCtCorporateMapper extends BaseMapper<GbCtCorporate> {
    int minusScore(Integer studentId,Integer contestId);
    int getScore(Integer studentId,Integer contestId);
}
