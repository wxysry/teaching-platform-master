package com.xiyou.main.dao.gbcontest;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xiyou.main.entity.contest.CtDdMaterial;
import com.xiyou.main.entity.gbcontest.GbCtDdMaterial;
import com.xiyou.main.entity.gbcontest.GbCtGzMaterial;
import com.xiyou.main.entity.newcontest.NewCtGzMaterial;
import com.xiyou.main.entity.ngbcontest.NGbCtDdMaterial;
import com.xiyou.main.vo.gbcontest.GbCashFolowEntity;
import com.xiyou.main.vo.gbcontest.GbKcEntity;
import com.xiyou.main.vo.newcontest.KcEntity;
import com.xiyou.main.vo.newcontest.NewCashFolowEntity;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author wangxingyu
 * @since 2023-06-11
 */
@Mapper
@Repository
public interface GbCtDdMaterialMapper extends BaseMapper<GbCtDdMaterial> {


    /**
     * 原料采购剩余时间为0的数量
     */
    List<GbCtDdMaterial> getRemainDateISZeroNumb(@Param("contestId") Integer contestId, @Param("studentId") Integer studentId);

    /**
     * 更新dd_material记录，原料编号为相同的，将剩余时间为1的剩余数量更新到剩余时间为0的，
     * 然后将剩余时间为1的剩余数量更新为0
     *
     * @param cashFolowEntity
     */
    void updateDdMaterialRemainDateEqOne(GbCashFolowEntity cashFolowEntity);


    void updateMaterial(@Param("list") List<CtDdMaterial> materialList);

    List<GbCtDdMaterial> list(@Param("contestId") Integer contestId, @Param("studentId") Integer studentId);

    Integer getStudentPayMoney(NewCashFolowEntity cashFolowEntity);

    void insertBatch(@Param("list") List<GbCtDdMaterial> ctDdMaterialList);

    List<GbCtGzMaterial> getListBySubjectNumber(Integer contestId);


    //根据竞赛Id和学生Id获取所有原料订单的数量
    List<GbCtDdMaterial> getMaterialOrderEachSum(@Param("contestId")Integer contestId, @Param("studentId")Integer studentId, @Param("currentTime") Integer currentTime);

    /**
     * 获取黑车数量

     * @return
     */
    List<GbKcEntity> getddMaterialBlackCar(Integer studentId, Integer contestId, Integer currentTime);


    /**
     * 获取灰车数量
     * @return
     */
    List<GbKcEntity> getddMaterialGreyCar(Integer studentId, Integer contestId, Integer currentTime);

    List<GbCtDdMaterial> getMaterialOrderRec(Integer studentId, Integer contestId, Integer currentTime);
}
