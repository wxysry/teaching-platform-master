package com.xiyou.main.dao.gbcontest;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xiyou.main.entity.gbcontest.GbCtFee;
import com.xiyou.main.vo.gbcontest.GbCashFolowEntity;
import com.xiyou.main.vo.newcontest.NewCashFolowEntity;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author wangxingyu
 * @since 2023-06-11
 */
@Mapper
@Repository
public interface GbCtFeeMapper extends BaseMapper<GbCtFee> {

    /**
     * 查询该用户fee的金额合计
     *
     * @param cashFolowEntity
     * @return
     */
    Integer getStudentReceivableMoney(GbCashFolowEntity cashFolowEntity);

    List<GbCtFee> getStudentReceivableMoneyList(GbCashFolowEntity cashFolowEntity);
    //应收款
    List<GbCtFee> getRecList(@Param("studentId") Integer userId, @Param("contestId") Integer contestId);
    //应付款
    List<GbCtFee> getPayList(@Param("studentId") Integer userId, @Param("contestId") Integer contestId);

    void insertBatch(@Param("list") List<GbCtFee> ctFeeList);
}
