package com.xiyou.main.dao.gbcontest;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xiyou.main.entity.gbcontest.GbCtFinancialTarget;
import com.xiyou.main.entity.newcontest.NewCtFinancialTarget;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author wangxingyu
 * @since 2023-06-28
 */
@Mapper
@Repository
public interface GbCtFinancialTargetMapper extends BaseMapper<GbCtFinancialTarget> {
}
