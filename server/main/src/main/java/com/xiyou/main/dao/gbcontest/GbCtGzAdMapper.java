package com.xiyou.main.dao.gbcontest;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xiyou.main.entity.gbcontest.GbCtGzAd;
import com.xiyou.main.entity.gbcontest.GbCtMnAd;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author wangxingyu
 * @since 2023-06-06
 */
@Repository
public interface GbCtGzAdMapper extends BaseMapper<GbCtGzAd> {
    String getGroupNum(@Param("contestId") Integer contestId, @Param("year") Integer year);

    List<GbCtMnAd> get(Integer contestId);

}
