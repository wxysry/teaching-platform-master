package com.xiyou.main.dao.gbcontest;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xiyou.main.entity.gbcontest.GbCtGzClasses;
import com.xiyou.main.entity.newcontest.NewCtGzClasses;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author wangxingyu
 * @since 2023-06-06
 */
@Repository
public interface GbCtGzClassesMapper extends BaseMapper<GbCtGzClasses> {
    List<GbCtGzClasses> getClassesByContestId(Integer contestId);
}
