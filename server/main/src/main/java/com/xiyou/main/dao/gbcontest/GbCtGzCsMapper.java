package com.xiyou.main.dao.gbcontest;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xiyou.main.entity.gbcontest.GbCtGzCs;
import com.xiyou.main.entity.newcontest.NewCtGzCs;
import com.xiyou.main.vo.gbcontest.GbCashFolowEntity;
import com.xiyou.main.vo.newcontest.NewCashFolowEntity;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author wangxingyu
 * @since 2023-06-06
 */
@Repository
public interface GbCtGzCsMapper extends BaseMapper<GbCtGzCs> {
    /**
     * 查询gz_cs中Loan_Ceiling
     * 参数：contestId
     *
     * @param cashFolowEntity
     * @return
     */
    Integer getCSLoanCeiling(GbCashFolowEntity cashFolowEntity);



    GbCtGzCs getByContestId(@Param("contestId") Integer contestId);



    GbCtGzCs getBySubjectNumber(Integer subjectNumber);
}
