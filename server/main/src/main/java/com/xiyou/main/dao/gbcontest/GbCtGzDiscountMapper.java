package com.xiyou.main.dao.gbcontest;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xiyou.main.entity.gbcontest.GbCtGzDiscount;
import com.xiyou.main.entity.newcontest.NewCtGzDiscount;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author wangxingyu
 * @since 2023-06-06
 */
@Repository
public interface GbCtGzDiscountMapper extends BaseMapper<GbCtGzDiscount> {

    List<GbCtGzDiscount> getGzDiscountByStudentIdAndContestId(@Param("contestId") Integer contestId,@Param("rRemainDate") Integer rRemainDate);
}
