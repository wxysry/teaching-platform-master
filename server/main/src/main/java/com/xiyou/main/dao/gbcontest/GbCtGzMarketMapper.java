package com.xiyou.main.dao.gbcontest;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xiyou.main.entity.gbcontest.GbCtGzMarket;
import com.xiyou.main.entity.newcontest.NewCtGzMarket;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author wangxingyu
 * @since 2023-06-06
 */
@Repository
public interface GbCtGzMarketMapper extends BaseMapper<GbCtGzMarket> {

    List<GbCtGzMarket> list(Integer contestId);

    Integer getCmid(@Param("contestId") Integer contestId, @Param("market") String market);

}
