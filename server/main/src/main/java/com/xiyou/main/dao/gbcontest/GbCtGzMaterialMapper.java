package com.xiyou.main.dao.gbcontest;

import com.xiyou.main.entity.gbcontest.GbCtGzMaterial;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author wangxingyu
 * @since 2023-06-06
 */
@Repository
public interface GbCtGzMaterialMapper extends BaseMapper<GbCtGzMaterial> {

    /**
     * 原料规则表.费用
     * @param contestId
     * @return
     */
    Integer getGzMaterialMoney(Integer contestId);


    List<GbCtGzMaterial> list(Integer contestId);
}
