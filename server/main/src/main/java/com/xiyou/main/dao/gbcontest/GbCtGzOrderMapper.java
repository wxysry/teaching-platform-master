package com.xiyou.main.dao.gbcontest;
import com.xiyou.main.entity.gbcontest.GbCtGzOrder;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xiyou.main.entity.gbcontest.GbCtMnChoose;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author wangxingyu
 * @since 2023-06-06
 */
@Repository
public interface GbCtGzOrderMapper extends BaseMapper<GbCtGzOrder> {

    List<GbCtMnChoose> getList(Integer contestId);

}
