package com.xiyou.main.dao.gbcontest;

import com.xiyou.main.entity.gbcontest.GbCtGzProducing;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import io.swagger.models.auth.In;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author wangxingyu
 * @since 2023-06-06
 */
@Repository
public interface GbCtGzProducingMapper extends BaseMapper<GbCtGzProducing> {
    List<GbCtGzProducing>  getListByCpId(Integer cpId);
    List<GbCtGzProducing> getListByContestId(Integer contestId);
}
