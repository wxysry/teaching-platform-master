package com.xiyou.main.dao.gbcontest;
import com.xiyou.main.entity.gbcontest.GbCtGzProductLine;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xiyou.main.vo.gbcontest.GbCashFolowEntity;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author wangxingyu
 * @since 2023-06-06
 */
@Repository
public interface GbCtGzProductLineMapper extends BaseMapper<GbCtGzProductLine> {
    /**
     * 获取生产线信息P14
     * @param contestId
     * @return
     */
    List<GbCtGzProductLine> getProductLineTypeList(Integer contestId);

    /**
     * 该生产线的投资时间
     * @param cashFolowEntity
     * @return
     */
    Integer getProductLineInfo(GbCashFolowEntity cashFolowEntity);

}
