package com.xiyou.main.dao.gbcontest;

import com.xiyou.main.entity.contest.CtGzProduct;
import com.xiyou.main.entity.gbcontest.GbCtGzProduct;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xiyou.main.vo.contest.YfProductVO;
import com.xiyou.main.vo.gbcontest.GbYfProductVO;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author wangxingyu
 * @since 2023-06-06
 */
@Repository
public interface GbCtGzProductMapper extends BaseMapper<GbCtGzProduct> {

    List<GbYfProductVO> listYfProduct(@Param("studentId") Integer userId, @Param("contestId") Integer contestId);

    /**
     * 生产产品名称
     *
     * @param contestId
     * @return
     */
    List<String> getProductCPName(Integer contestId);


    List<GbCtGzProduct> getList(Integer contestId);

    Integer getCpid(@Param("contestId") Integer contestId, @Param("product") String product);

    GbCtGzProduct get(@Param("contestId") Integer contestId, @Param("cpId") Integer cpId);
}
