package com.xiyou.main.dao.gbcontest;

import com.xiyou.main.entity.gbcontest.GbCtGzWorkerIncentive;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.stereotype.Repository;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author wangxingyu
 * @since 2023-06-06
 */
@Repository
public interface GbCtGzWorkerIncentiveMapper extends BaseMapper<GbCtGzWorkerIncentive> {

}
