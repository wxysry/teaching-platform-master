package com.xiyou.main.dao.gbcontest;

import com.xiyou.main.entity.gbcontest.GbCtGzWorkerRecruit;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author wangxingyu
 * @since 2023-06-06
 */
@Repository
public interface GbCtGzWorkerRecruitMapper extends BaseMapper<GbCtGzWorkerRecruit> {
    List<GbCtGzWorkerRecruit> getListBySubjectNum(Integer subjectNum);
}
