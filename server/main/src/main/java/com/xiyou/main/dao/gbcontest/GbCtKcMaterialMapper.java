package com.xiyou.main.dao.gbcontest;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xiyou.main.entity.gbcontest.GbCtKcMaterial;
import com.xiyou.main.vo.gbcontest.GbKcEntity;
import com.xiyou.main.vo.gbcontest.GbCashFolowEntity;
import com.xiyou.main.vo.gbcontest.GbCtKcMaterialNumVo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author wangxingyu
 * @since 2023-06-11
 */
@Mapper
@Repository
public interface GbCtKcMaterialMapper extends BaseMapper<GbCtKcMaterial> {

    /**
     * 原材料 R1-P5
     * 参数：
     * @return
     */
    List<GbKcEntity> getKcMaterial(@Param("studentId") Integer studentId, @Param("contestId") Integer contestId);


    /**
     * P11-> 原料编号的剩余数量=剩余数量+dd_material中剩余时间为0的原料编号对应的数量
     *
     * @param cashFolowEntity
     */
    void updateKcMaterial(@Param("cashFolowEntity") GbCashFolowEntity cashFolowEntity);


    int getMaterialSum(@Param("studentId") Integer studentId, @Param("contestId") Integer contestId);

    List<GbCtKcMaterial> listNeed(@Param("contestId") Integer contestId, @Param("list") List<Integer> lineIds);

    List<GbCtKcMaterial> listKc(@Param("contestId") Integer contestId, @Param("studentId") Integer userId);

    List<GbCtKcMaterial> listSellKc(@Param("contestId") Integer contestId, @Param("studentId") Integer userId);

    void insertBatch(@Param("list") List<GbCtKcMaterial> ctKcMaterialList);

    List<GbCtKcMaterialNumVo> listKcNum(@Param("contestId") Integer contestId, @Param("studentId") Integer userId);

    List<GbCtKcMaterial> listByPriceAndDateAndCmId(@Param("contestId") Integer contestId,
                                                  @Param("studentId") Integer studentId,
                                                   @Param("imCmId") Integer imCmId,
                                                  @Param("materialPrice") Integer materialPrice,
                                                  @Param("inInventoryDate") Integer inInventoryDate);

    List<GbCtKcMaterial> list(@Param("contestId") Integer contestId, @Param("studentId") Integer userId);

    List<GbCtKcMaterial> listKcGroupNumber(@Param("contestId") Integer contestId, @Param("studentId") Integer userId);

    List<GbCtKcMaterial> listKcGroup(@Param("contestId") Integer contestId, @Param("studentId") Integer userId);


}
