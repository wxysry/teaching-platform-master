package com.xiyou.main.dao.gbcontest;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xiyou.main.entity.contest.CtGzProductLine;
import com.xiyou.main.entity.contest.CtLine;
import com.xiyou.main.entity.gbcontest.GbCtGzProductLine;
import com.xiyou.main.entity.gbcontest.GbCtLine;
import com.xiyou.main.vo.contest.OnlineLine;
import com.xiyou.main.vo.gbcontest.GbOnlineLine;
import io.swagger.models.auth.In;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author tangcan
 * @since 2019-07-22
 */
@Repository
public interface GbCtLineMapper extends BaseMapper<GbCtLine> {

    List<GbOnlineLine> listOnline(@Param("studentId") Integer userId, @Param("contestId") Integer contestId, @Param("date") Integer date);

    int getMaintenanceFee(Integer studentId, Integer contestId, Integer date);

    List<GbCtGzProductLine> getLineInfo(@Param("list") List<Integer> lineIds);

    List<GbOnlineLine> listTransfer(@Param("studentId") Integer userId, @Param("contestId") Integer contestId);

    void updateDepFee(@Param("studentId") Integer userId, @Param("contestId") Integer contestId, @Param("date") Integer date);

    Integer getDepTotal(@Param("studentId") Integer userId, @Param("contestId") Integer contestId, @Param("date") Integer date);

    int getProductInProcess(GbCtLine line);

    int getEquipmentSum(@Param("studentId") Integer userId, @Param("contestId") Integer contestId);

    List<GbOnlineLine> listNoProducting(@Param("studentId") Integer userId, @Param("contestId") Integer contestId);

    /**
     * 显示line中PL_Remain_Date 不为空的数据
     *
     * @param userId
     * @param contestId
     * @return
     */
    List<GbOnlineLine> getListOnline(@Param("studentId") Integer userId, @Param("contestId") Integer contestId);

    /*
    如果剩余生产时间为0，则开产时间、剩余生产时间置空，如剩余生产时间>0,则开产时间不变，剩余生产时间=原剩余生产时间-1
     */
    int updateProductAddDateAndProductingDate(@Param("studentId") Integer studentId, @Param("contestId") Integer contestId);

    /*
    如果剩余时间为0，则置空，如>0，则-1；转产剩余时间为0，则置空剩余时间及转产时间，如>0，则-1
     */
    int updateRemainDateAndTransferAddDate(@Param("studentId") Integer studentId, @Param("contestId") Integer contestId, @Param("date") Integer date);

    int add(GbCtLine line);

    void updateBatch(@Param("list") List<CtLine> lines);

    List<GbCtLine> getList(Integer contestId, Integer studentId);

    List<GbCtLine> getAllLineList(Integer contestId, Integer studentId);

    //根据产线ID获取产线规则
    GbCtGzProductLine getGzLineByLineId(Integer lineId);

    //获取不同状态的产线
    List<GbCtLine> getLineListOfDifStatus(Integer contestId,Integer studentId,String status);

    //在建-->空闲
    void updateLineStatusBuildToSpace(Integer contestId, Integer studentId, Integer finishDate, String oldStatus, String newStatus);
    //转产-->空闲
    void updateLineStatusTransferToSpace(Integer contestId, Integer studentId, Integer finishDate, String oldStatus, String newStatus);
    //生产-->空闲
    void updateLineStatusProduceToSpace(Integer contestId, Integer studentId, Integer finishDate, String oldStatus, String newStatus);

    //获取所有产线按照建成时间排序
    List<GbCtLine> getLineListOrderByFinishTime(Integer studentId, Integer contestId , Integer currentTime);


    //获取所有在产产线当季结束生产的产线
    List<GbCtLine> getLineListFinishTime(Integer studentId, Integer contestId , Integer finishDate);

    //
    int getCountLine(Integer contestId, Integer studentId );
}
