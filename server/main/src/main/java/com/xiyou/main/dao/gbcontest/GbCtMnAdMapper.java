package com.xiyou.main.dao.gbcontest;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xiyou.main.entity.gbcontest.GbCtMnAd;
import com.xiyou.main.vo.gbcontest.GbCtMnAdVo;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author wangxingyu
 * @since 2023-06-11
 */
@Repository
public interface GbCtMnAdMapper extends BaseMapper<GbCtMnAd> {

    List<GbCtMnAd> getAdList(@Param("contestId") Integer contestId,
                           @Param("studentId") Integer studentId,
                           @Param("year") Integer year,
                           @Param("quarterly") Integer quarterly
    );

    void insertBatch(@Param("list") List<GbCtMnAd> ctMnAdList);

    //获取该竞赛的所有组投放广告的信息
    List<GbCtMnAd> getAdListByContestId(@Param("contestId") Integer contestId,
                             @Param("year") Integer year,
                             @Param("quarterly") Integer quarterly
    );

    //获取该竞赛的所有组投放广告的信息
    List<GbCtMnAdVo> getAdListAndAccountByContestId(@Param("contestId") Integer contestId,
                                                    @Param("year") Integer year,
                                                    @Param("quarterly") Integer quarterly
    );

    List<GbCtMnAd> getAdListAndAccountByYear(@Param("contestId") Integer contestId,
                             @Param("year") Integer year
    );


}
