package com.xiyou.main.dao.gbcontest;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.xiyou.common.utils.R;
import com.xiyou.main.entity.contest.CtMnChoose;
import com.xiyou.main.entity.gbcontest.GbContestStudent;
import com.xiyou.main.entity.gbcontest.GbCtMnChoose;
import com.xiyou.main.params.gbcontest.GbCtMnChooseParam;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author wangxingyu
 * @since 2023-06-11
 */
@Repository
public interface GbCtMnChooseMapper extends BaseMapper<GbCtMnChoose> {

    //获取该竞赛的所有订单 根据时间
    List<GbCtMnChoose> getOrderList(@Param("contestId") Integer contestId,
                                  @Param("cmid") Integer cmid,
                                  @Param("cpid") Integer cpid,
                                  @Param("year") Integer year,
                                  @Param("quarterly") Integer quarterly);

    void insertBatch(@Param("list") List<GbCtMnChoose> ctMnChooseList);



    //获取该竞赛的所有订单 根据时间
    List<GbCtMnChoose> getOrderBySeason(@Param("contestId") Integer contestId,
                                    @Param("year") Integer year,
                                    @Param("quarterly") Integer quarterly);

    Page<GbCtMnChoose> getPage(Page<GbCtMnChoose> page, @Param("param")GbCtMnChooseParam gbCtMnChooseParam);

    /**
     * 获取当前考试的可选单列表
     * @param contestId
     * @return
     */
    List<GbCtMnChoose> getOrderDate(Integer contestId);
}
