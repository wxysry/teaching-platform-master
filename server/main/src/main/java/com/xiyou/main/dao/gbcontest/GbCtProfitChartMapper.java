package com.xiyou.main.dao.gbcontest;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xiyou.main.entity.gbcontest.GbCtProfitChart;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author wangxingyu
 * @since 2023-06-28
 */
@Repository
public interface GbCtProfitChartMapper extends BaseMapper<GbCtProfitChart> {
    List<GbCtProfitChart> getListByContestId(@Param("contestId") Integer contestId, @Param("nowYear") Integer nowYear);
}
