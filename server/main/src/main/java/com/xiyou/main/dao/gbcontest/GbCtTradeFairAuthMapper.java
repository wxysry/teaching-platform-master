package com.xiyou.main.dao.gbcontest;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xiyou.main.entity.gbcontest.GbCtTradeFairAuth;
import org.springframework.stereotype.Repository;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author tangcan
 * @since 2019-08-25
 */
@Repository
public interface GbCtTradeFairAuthMapper extends BaseMapper<GbCtTradeFairAuth> {
    //判断该季订货会是否开启
    GbCtTradeFairAuth getTradeFairByCurrentTime(Integer contestId,Integer currentTime);

    //更新订货会状态
    void updateTradeFairAuth(Integer contestId,Integer status);
}
