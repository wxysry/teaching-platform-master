package com.xiyou.main.dao.gbcontest;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xiyou.main.entity.gbcontest.GbCtWorker;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author tangcan
 * @since 2019-08-25
 */
@Repository
public interface GbCtWorkerMapper extends BaseMapper<GbCtWorker> {
    //查询当前时间招聘的所有工人
    List<GbCtWorker> getGbCtWorkerListByContestIdAndCurrTime(Integer contestId,Integer studentId,Integer currentTime);
    List<GbCtWorker> getGbCtWorkerListOnTheJob(Integer contestId,Integer studentId,Integer state);
    List<GbCtWorker> getAllWorkerListByLineId(Integer lineId);
    List<GbCtWorker> getGjWorkerListByLineId(Integer lineId);
    List<GbCtWorker> getPtWorkerListByLineId(Integer lineId);
    //把当前时间入职的人设置为入职状态
    void updateWorkerToOnboarding(Integer contestId, Integer studentId, Integer currentTime);
    List<GbCtWorker> getAllWorker(Integer contestId,Integer studentId);
    void  updateWorkerListByLineId(Integer lineId);


    /**
     * 更新工人为空闲状态
     * @param lineId
     */
    void updateWorkerListByLineIdToSpace(Integer lineId);
}
