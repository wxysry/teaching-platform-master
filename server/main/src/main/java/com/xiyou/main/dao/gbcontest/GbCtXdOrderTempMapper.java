package com.xiyou.main.dao.gbcontest;

import com.xiyou.main.entity.gbcontest.GbCtXdOrderTemp;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author wangxingyu
 * @since 2023-09-14
 */
@Mapper
@Repository
public interface GbCtXdOrderTempMapper extends BaseMapper<GbCtXdOrderTemp> {

}
