package com.xiyou.main.dao.gbcontest;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xiyou.main.entity.gbcontest.GbCtXzOrder;
import com.xiyou.main.vo.gbcontest.GbCashFolowEntity;
import com.xiyou.main.vo.gbcontest.GbXzOrderVo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author wangxingyu
 * @since 2023-06-11
 */
@Mapper
@Repository
public interface GbCtXzOrderMapper extends BaseMapper<GbCtXzOrder> {


    /**
     * 销售收入
     * 参数：studentId、contestId
     *
     * @param cashFolowEntity
     * @return
     */
    Integer getStudentSalesProfit(GbCashFolowEntity cashFolowEntity);

    /**
     * 直接成本[本年]
     * 参数：studentId、contestId、currentYear
     *
     * @param cashFolowEntity
     * @return
     */
    Integer getStudentProductDevCost(GbCashFolowEntity cashFolowEntity);


    List<GbCtXzOrder> list(GbCtXzOrder order);

    Map<String, BigDecimal> getSalesAndDirectCost(@Param("studentId") Integer studentId, @Param("contestId") Integer contestId, @Param("year") int year);

    List<GbXzOrderVo> listDeliveryOrder(Map<String, Object> queryMap);

    //获取所有订单
    List<GbCtXzOrder> getList(@Param("contestId") Integer contestId, @Param("studentId") Integer studentId);

    Integer getSales(@Param("contestId") Integer contestId, @Param("studentId") Integer studentId, @Param("cmid") Integer cmid, @Param("year") int year);


    /**
     * 删除当前年/季的订单
     * @param contestId
     * @param year
     * @param quarterly
     */
    void removeNowYear(@Param("contestId") Integer contestId, @Param("year") int year,@Param("quarterly") int quarterly);

    //获取当季订单
    List<GbCtXzOrder> getListByDate(@Param("contestId") Integer contestId, @Param("year") Integer year, @Param("quarterly") Integer quarterly);

}
