package com.xiyou.main.dao.gbcontest;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xiyou.main.entity.gbcontest.GbCtYfIso;
import com.xiyou.main.vo.gbcontest.GbMarketReturnEntity;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author wangxingyu
 * @since 2023-06-11
 */
@Mapper
@Repository
public interface GbCtYfIsoMapper extends BaseMapper<GbCtYfIso> {

    void update(GbCtYfIso yfIso);

    /**
     * ISO认证
     *
     * @return
     */
    List<String> getISONameList(Integer marketId);

    /**
     * ISO认证 [ISO9000、ISO14000]
     *
     * @param isoId
     * @return
     */
    GbMarketReturnEntity getYFISOProportion(@Param("isoId") Integer isoId, @Param("studentId") Integer studentId, @Param("contestId") Integer contestId);


    List<String> getIsoList(@Param("studentId") Integer studentId, @Param("contestId") Integer contestId);

    /**
     * @Author: tangcan
     * @Description: iso投资列表 p30
     * @Param: [studentId, contestId]
     * @date: 2019/9/1
     */
    List<Map<String, Object>> getIsoInvestList(@Param("studentId") Integer studentId, @Param("contestId") Integer contestId);

    Integer getIsoInvestSum(@Param("list") List<Integer> isoIds);

    void updateISOInvest(@Param("studentId") Integer studentId, @Param("contestId") Integer contestId, @Param("list") List<Integer> isoIds);

    void insertBatch(@Param("list") List<GbCtYfIso> ctYfIsoList);

    List<GbCtYfIso> getYfFinishList(@Param("contestId") Integer contestId, @Param("studentId") Integer studentId);

    List<GbCtYfIso> getList(@Param("contestId") Integer contestId, @Param("studentId") Integer studentId);

    void updateStateToFinish(Integer studentId,Integer contestId,Integer finishDate);
}
