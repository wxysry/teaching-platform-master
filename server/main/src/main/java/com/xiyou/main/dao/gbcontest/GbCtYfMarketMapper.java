package com.xiyou.main.dao.gbcontest;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xiyou.main.entity.gbcontest.GbCtYfMarket;
import com.xiyou.main.vo.gbcontest.GbMarketReturnEntity;
import com.xiyou.main.vo.gbcontest.GbMarketVo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author wangxingyu
 * @since 2023-06-11
 */
@Mapper
@Repository
public interface GbCtYfMarketMapper extends BaseMapper<GbCtYfMarket> {

    void update(GbCtYfMarket yfMarket);

    /**
     * 市场准入
     *
     * @return
     */
    List<String> getYFMarketNames(@Param("studentId") Integer studentId, @Param("contestId") Integer contestId);

    /**
     * 本地、区域、国内、亚洲、国际占比
     * 参数：市场编号
     *
     * @param cmId
     * @return
     */
    GbMarketReturnEntity getYFMarketProportion(@Param("cmId") Integer cmId, @Param("studentId") Integer studentId, @Param("contestId") Integer contestId);

    List<String> getHadYfFinishMarkets(@Param("studentId") Integer studentId, @Param("contestId") Integer contestId);

    List<String> getMarketList(@Param("studentId") Integer studentId, @Param("contestId") Integer contestId);

    List<GbMarketVo> listYfMarket(@Param("studentId") Integer studentId, @Param("contestId") Integer contestId);

    void insertBatch(@Param("list") List<GbCtYfMarket> ctYfMarketList);

    List<GbCtYfMarket> getList(@Param("contestId") Integer contestId, @Param("studentId") Integer studentId);

    void updateStateToFinish(Integer studentId,Integer contestId,Integer finishDate);
}
