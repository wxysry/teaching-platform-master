package com.xiyou.main.dao.gbcontest;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xiyou.main.entity.gbcontest.GbCtYfNum;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author wangxingyu
 * @since 2023-06-06
 */
@Repository
public interface GbCtYfNumMapper extends BaseMapper<GbCtYfNum> {
    void updateStateToFinish(Integer studentId, Integer contestId, Integer finishDate);


    List<GbCtYfNum> getList(Integer contestId, Integer studentId);
}
