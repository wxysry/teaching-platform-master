package com.xiyou.main.dao.gbcontest;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xiyou.main.entity.gbcontest.GbCtYfProduct;
import com.xiyou.main.vo.gbcontest.GbMarketReturnEntity;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author wangxingyu
 * @since 2023-06-11
 */
@Mapper
@Repository
public interface GbCtYfProductMapper extends BaseMapper<GbCtYfProduct> {

    int update(GbCtYfProduct yfProduct);

    /**
     * 生产资格
     *
     * @return
     */
    List<String> getProduceStatus();

    /**
     * P1-P5
     *
     * @param productId
     * @return
     */
    GbMarketReturnEntity getYFProductProportion(@Param("productId") Integer productId, @Param("studentId") Integer studentId, @Param("contestId") Integer contestId);

    void updateEndingSeason(@Param("studentId") Integer userId, @Param("contestId") Integer contestId, @Param("date") Integer date);

    List<String> getProductList(@Param("studentId") Integer studentId, @Param("contestId") Integer contestId);

    void insertBatch(@Param("list") List<GbCtYfProduct> gbCtYfProductList);

    List<GbCtYfProduct> getList(@Param("contestId") Integer contestId, @Param("studentId") Integer studentId);

    void updateStateToFinish(Integer studentId,Integer contestId,Integer finishDate);

}
