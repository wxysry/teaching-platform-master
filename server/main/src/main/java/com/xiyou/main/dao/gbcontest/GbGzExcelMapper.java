package com.xiyou.main.dao.gbcontest;


import com.xiyou.main.pojo.gbcontest.GbCtGzAdModel;
import com.xiyou.main.pojo.gbcontest.GbCtGzClassesModel;
import com.xiyou.main.pojo.gbcontest.GbCtGzCsModel;
import com.xiyou.main.pojo.gbcontest.GbCtGzDiscountModel;
import com.xiyou.main.pojo.gbcontest.GbCtGzIsoModel;
import com.xiyou.main.pojo.gbcontest.GbCtGzLoanModel;
import com.xiyou.main.pojo.gbcontest.GbCtGzMarketModel;
import com.xiyou.main.pojo.gbcontest.GbCtGzMaterialModel;
import com.xiyou.main.pojo.gbcontest.GbCtGzNumModel;
import com.xiyou.main.pojo.gbcontest.GbCtGzOrderModel;
import com.xiyou.main.pojo.gbcontest.GbCtGzProducingModel;
import com.xiyou.main.pojo.gbcontest.GbCtGzProductDesignModel;
import com.xiyou.main.pojo.gbcontest.GbCtGzProductLineModel;
import com.xiyou.main.pojo.gbcontest.GbCtGzProductModel;
import com.xiyou.main.pojo.gbcontest.GbCtGzWorkerIncentiveModel;
import com.xiyou.main.pojo.gbcontest.GbCtGzWorkerRecruitModel;
import com.xiyou.main.pojo.gbcontest.GbCtGzWorkerTrainModel;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author xingzi
 * @date 2019 07 23  14:20
 */
@Repository
public interface GbGzExcelMapper {

    void insertCs(@Param("list") List<GbCtGzCsModel> list);

    void insertAd(@Param("list") List<GbCtGzAdModel> list);

    void insertIso(@Param("list") List<GbCtGzIsoModel> list);

    void insertOrder(@Param("list") List<GbCtGzOrderModel> list);

    void insertMarket(@Param("list") List<GbCtGzMarketModel> list);

    void insertMaterial(@Param("list") List<GbCtGzMaterialModel> list);

    void insertProduct(@Param("list") List<GbCtGzProductModel> list);

    void insertProducing(@Param("list") List<GbCtGzProducingModel> list);

    void insertProductLine(@Param("list") List<GbCtGzProductLineModel> list);


    //产品设计gz_product_design（新增）

    void insertProductDesign(@Param("list") List<GbCtGzProductDesignModel> list);

    //工人招聘gz_worker_recruit（新增）

    void insertWorkerRecruit(@Param("list") List<GbCtGzWorkerRecruitModel> list);

    //工人培训gz_worker_train(新增)

    void insertWorkerTrain(@Param("list") List<GbCtGzWorkerTrainModel> list);

    //贷款规则gz_loan(新增)

    void insertLoan(@Param("list") List<GbCtGzLoanModel> list);

    //贴现规则gz_discount（新增）

    void insertDiscount(@Param("list") List<GbCtGzDiscountModel> list);

    //班次规则gz_classes(新增)

    void insertClasses(@Param("list") List<GbCtGzClassesModel> list);

    //员工激励gz_worker_incentive(新增)

    void insertWorkerIncentive(@Param("list") List<GbCtGzWorkerIncentiveModel> list);

    //数字化岗位gz_num(新增)

    void insertNum(@Param("list") List<GbCtGzNumModel> list);



    void removeRestAd(@Param("list") List<GbCtGzAdModel> gbCtGzAdModelList);

    void removeRestCs(@Param("list") List<GbCtGzCsModel> gbCtGzCsModelList);

    void removeRestIso(@Param("list") List<GbCtGzIsoModel> gbCtGzIsoModelList);

    void removeRestOrder(@Param("list") List<GbCtGzOrderModel> gbCtGzOrderModelList);

    void removeRestMarket(@Param("list") List<GbCtGzMarketModel> gbCtGzMarketModelList);

    void removeRestMaterial(@Param("list") List<GbCtGzMaterialModel> gbCtGzMaterialModelList);

    void removeRestProduct(@Param("list") List<GbCtGzProductModel> gbCtGzProductModelList);

    void removeRestProducing(@Param("list") List<GbCtGzProducingModel> gbCtGzProducingModelList);

    void removeRestProductLine(@Param("list") List<GbCtGzProductLineModel> gbCtGzProductLineModelList);

    //产品设计gz_product_design（新增）

    void removeRestProductDesign(@Param("list") List<GbCtGzProductDesignModel> list);

    //工人招聘gz_worker_recruit（新增）

    void removeRestWorkerRecruit(@Param("list") List<GbCtGzWorkerRecruitModel> list);

    //工人培训gz_worker_train(新增)

    void removeRestWorkerTrain(@Param("list") List<GbCtGzWorkerTrainModel> list);

    //贷款规则gz_loan(新增)

    void removeRestLoan(@Param("list") List<GbCtGzLoanModel> list);

    //贴现规则gz_discount（新增）

    void removeRestDiscount(@Param("list") List<GbCtGzDiscountModel> list);

    //班次规则gz_classes(新增)

    void removeRestClasses(@Param("list") List<GbCtGzClassesModel> list);

    //员工激励gz_worker_incentive(新增)

    void removeRestWorkerIncentive(@Param("list") List<GbCtGzWorkerIncentiveModel> list);

    //数字化岗位gz_num(新增)

    void removeRestNum(@Param("list") List<GbCtGzNumModel> list);

}
