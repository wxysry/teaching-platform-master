package com.xiyou.main.dao.gbcontest;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xiyou.main.entity.gbcontest.GbRePaymentInfo;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author 吕辉耀
 * @PROJECT_NAME: teaching-platform-master-gb
 * @Description:
 * @creat 2023-07-17 16
 */
@Repository
public interface GbRePaymentInfoMapper extends BaseMapper<GbRePaymentInfo> {
    List<GbRePaymentInfo> getListByTime(Integer contestId,Integer studentId,Integer currentTime);
    GbRePaymentInfo getOverhaul(Integer contestId,Integer studentId,Integer currentTime);
    void deleteByAmountAndBlAddTimeAndPayment(Integer contestId,Integer studentId,Integer currentTime,String payment,Integer blAddTime);
    List<GbRePaymentInfo> selectByAmountAndBlAddTimeAndPayment(Integer contestId,Integer studentId,Integer currentTime,String payment,Integer blAddTime);
}
