package com.xiyou.main.dao.newcontest;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.xiyou.main.entity.newcontest.NewContest;
import com.xiyou.main.params.contest.ContestParam;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author tangcan
 * @since 2019-08-25
 */
@Repository
public interface NewContestMapper extends BaseMapper<NewContest> {

    Page<NewContest> getPage(Page<NewContest> page, @Param("param") ContestParam contestParam);

    int getUnEndContest(@Param("teacherId") Integer teacherId);

    int publish(@Param("teacherId") Integer teacherId, @Param("contestId") Integer contestId);

    Page<NewContest> getStudentContestPage(Page<NewContest> page, @Param("param") ContestParam contestParam);

    //根据竞赛contestId获取subjectNum
    NewContest getObjByContestId(@Param("contestId") Integer contestId);
}
