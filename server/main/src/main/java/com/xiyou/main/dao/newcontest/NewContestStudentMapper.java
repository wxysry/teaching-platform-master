package com.xiyou.main.dao.newcontest;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.xiyou.main.entity.contest.ContestStudent;
import com.xiyou.main.entity.newcontest.NewContestStudent;
import com.xiyou.main.params.contest.ContestStudentParam;
import com.xiyou.main.vo.contest.ContestScore;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author tangcan
 * @since 2019-08-25
 */
@Repository
public interface NewContestStudentMapper extends BaseMapper<NewContestStudent> {

    int insertBatch(@Param("contestId") Integer contestId, @Param("list") List<Integer> studentIds);

    void updateStart(@Param("contestId") Integer contestId, @Param("studentId") Integer studentId);

    Page<NewContestStudent> getPage(Page<NewContestStudent> page, @Param("param") ContestStudentParam contestStudentParam);

    List<Integer> getStudentIdList(Integer contestId);

    int saveProgress(@Param("studentId") Integer studentId, @Param("contestId") Integer contestId, @Param("date") Integer date, @Param("progress") String progress);

    NewContestStudent get(@Param("contestId") Integer contestId, @Param("studentId") Integer studentId);

    void updateScore(@Param("id") Integer id, @Param("score") Double score);

    void saveXzorder(NewContestStudent newContestStudent);

    void removeProgress(@Param("contestId") Integer contestId, @Param("studentId") Integer studentId);

    List<ContestScore> getScoreList(@Param("contestId") Integer contestId);

    void updateErrorReportYear(NewContestStudent newContestStudent);
}
