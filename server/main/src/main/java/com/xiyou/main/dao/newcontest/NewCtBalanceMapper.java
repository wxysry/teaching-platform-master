package com.xiyou.main.dao.newcontest;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xiyou.main.entity.newcontest.NewCtBalance;
import com.xiyou.main.vo.newcontest.NewCashFolowEntity;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author wangxingyu
 * @since 2023-06-11
 */
@Mapper
@Repository
public interface NewCtBalanceMapper extends BaseMapper<NewCtBalance> {

    /**
     * 获取最大贷款额度[上一年所有者权益]
     *
     * @param cashFolowEntity
     * @return
     */
    Integer getBalanceMaxLoanLimit(NewCashFolowEntity cashFolowEntity);


    Integer getRecentYEarHaveEquity(@Param("studentId") Integer studentId, @Param("contestId") Integer contestId, @Param("nowYear") Integer nowYear);

    int maxTotalEquityPreYear(@Param("studentId") Integer studentId, @Param("contestId") Integer contestId, @Param("currentYear") int currentYear);
}
