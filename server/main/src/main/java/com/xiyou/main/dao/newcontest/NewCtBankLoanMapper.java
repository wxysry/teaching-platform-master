package com.xiyou.main.dao.newcontest;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xiyou.main.entity.newcontest.NewCtBankLoan;
import com.xiyou.main.vo.newcontest.NewCashFolowEntity;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author wangxingyu
 * @since 2023-06-11
 */
@Mapper
@Repository
public interface NewCtBankLoanMapper extends BaseMapper<NewCtBankLoan> {

    /**
     * 长贷总额
     * 参数：studentId、contestId、blType{1：长期贷款，2：短期贷款}、currentTime
     *
     * @param cashFolowEntity
     * @param blType
     * @return
     */
    Integer getStudentLongAndShortTermLoansMoney(@Param("cashFolowEntity") NewCashFolowEntity cashFolowEntity,
                                                 @Param("blType") Integer blType);

    /**
     * bank_loan中还款时间大于等于当前时间的贷款类型为1和2的总额
     *
     * @param cashFolowEntity
     * @return
     */
    Integer getBackLoanAmount(NewCashFolowEntity cashFolowEntity);

    /**
     * back_loan中贷款类型为2、且归还时间=（当前时间+1）的金额
     * 参数：studentId、contestId、currentTime+1
     *
     * @param cashFolowEntity
     * @return
     */
    Integer getCurrentMoney(NewCashFolowEntity cashFolowEntity);

    List<NewCtBankLoan> getStudentLongAndShortTermLoansMoneyList(@Param("cashFolowEntity") NewCashFolowEntity cashFolowEntity,
                                                              @Param("blType") Integer blType);

    Integer getTotalEquity(NewCashFolowEntity cashFolowEntity);

    Integer getFee(NewCashFolowEntity cashFolowEntity);

    List<NewCtBankLoan> getList(@Param("contestId") Integer contestId, @Param("studentId") Integer studentId);

    //获取本季度需要偿还的贷款
    List<NewCtBankLoan> getListNeedPay(@Param("contestId") Integer contestId,
                                       @Param("studentId") Integer studentId,@Param("currentTime") Integer currentTime);

    //获取本季度需要偿还的贷款
    List<NewCtBankLoan> getListOverCurrentTime(@Param("contestId") Integer contestId,
                                       @Param("studentId") Integer studentId,@Param("currentTime") Integer currentTime);
}
