package com.xiyou.main.dao.newcontest;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xiyou.main.entity.newcontest.NewCtCashflow;
import com.xiyou.main.vo.newcontest.NewCashFolowEntity;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author wangxingyu
 * @since 2023-06-11
 */
@Mapper
@Repository
public interface NewCtCashflowMapper extends BaseMapper<NewCtCashflow> {

    Integer getCash(@Param("studentId") Integer userId, @Param("contestId") Integer contestId);

    /**
     * 当前金额
     *
     * @param cashFolowEntity
     * @return
     */
    Integer getStudentCurrentMoney(NewCashFolowEntity cashFolowEntity);

    /**
     * 贴息、利息
     * 参数：studentId、contestId、keyword{贴息、归还利息}、
     *
     * @return
     */
    Integer getStudentDiscountMoney(@Param("cashFolowEntity") NewCashFolowEntity cashFolowEntity,
                                    @Param("keyword") String keyword);

    /**
     * 维修费、转产费、租金、管理费、广告费
     * 参数：studentId、contestId、currentYear
     *
     * @param cashFolowEntity
     * @return
     */
    Integer getRepairCostToAdCost(@Param("cashFolowEntity") NewCashFolowEntity cashFolowEntity,
                                  @Param("keyword") String keyword);

    /**
     * 损失
     * 参数：studentId、contestId
     *
     * @param cashFolowEntity
     * @return
     */
    Integer getStudentLossCost(NewCashFolowEntity cashFolowEntity);

    /**
     * ISO认证、产品开发、市场开拓
     *
     * @param cashFolowEntity
     * @return
     */
    Integer getStudentISOTomarketDevCost(@Param("cashFolowEntity") NewCashFolowEntity cashFolowEntity,
                                         @Param("keyword") String keyword);

    /**
     * p8保存长贷申请
     *
     * @param cashFolowEntity
     */
    void addStudentLongLoanInfo(NewCashFolowEntity cashFolowEntity);


    List<NewCtCashflow> list(@Param("studentId") Integer userId,
                          @Param("contestId") Integer contestId,
                          @Param("year") Integer year,
                          @Param("list") List<String> actionList);

    /**
     * P10->上一条现金
     *
     * @param cashFolowEntity
     * @return
     */
    Integer getStudentLastCash(NewCashFolowEntity cashFolowEntity);

    List<NewCtCashflow> getList(@Param("contestId") Integer contestId, @Param("studentId") Integer studentId);

    List<NewCtCashflow> getRepaymentList(@Param("contestId") Integer contestId, @Param("studentId") Integer studentId,
                                         @Param("currentTime") Integer currentTime,@Param("cAction") String cAction);
}
