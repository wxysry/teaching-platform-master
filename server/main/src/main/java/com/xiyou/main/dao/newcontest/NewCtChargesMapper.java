package com.xiyou.main.dao.newcontest;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xiyou.main.entity.contest.CtCharges;
import com.xiyou.main.entity.newcontest.NewCtCharges;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author tangcan
 * @since 2019-07-22
 */
@Repository
public interface NewCtChargesMapper extends BaseMapper<NewCtCharges> {

    int addBySys(NewCtCharges charges);

    int updateTotal(@Param("chargesId") Integer chargesId);

    Integer getTotal(@Param("studentId") Integer studentId, @Param("contestId") Integer contestId, @Param("year") int year, @Param("isxt") int isxt);
}
