package com.xiyou.main.dao.newcontest;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.xiyou.main.entity.newcontest.NewContest;
import com.xiyou.main.entity.newcontest.NewCtCorporate;
import com.xiyou.main.params.contest.ContestParam;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author tangcan
 * @since 2019-08-25
 */
@Repository
public interface NewCtCorporateMapper extends BaseMapper<NewCtCorporate> {
    int minusScore(Integer studentId,Integer contestId);
    int getScore(Integer studentId,Integer contestId);
}
