package com.xiyou.main.dao.newcontest;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xiyou.main.entity.contest.CtFee;
import com.xiyou.main.entity.newcontest.NewCtFee;
import com.xiyou.main.vo.newcontest.NewCashFolowEntity;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author wangxingyu
 * @since 2023-06-11
 */
@Mapper
@Repository
public interface NewCtFeeMapper extends BaseMapper<NewCtFee> {

    /**
     * 查询该用户fee的金额合计
     *
     * @param cashFolowEntity
     * @return
     */
    Integer getStudentReceivableMoney(NewCashFolowEntity cashFolowEntity);

    List<NewCtFee> getStudentReceivableMoneyList(NewCashFolowEntity cashFolowEntity);

    List<NewCtFee> getList(@Param("studentId") Integer userId, @Param("contestId") Integer contestId);

    void insertBatch(@Param("list") List<NewCtFee> ctFeeList);
}
