package com.xiyou.main.dao.newcontest;

import com.xiyou.main.entity.newcontest.NewCtFinancialTarget;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author wangxingyu
 * @since 2023-06-28
 */
public interface NewCtFinancialTargetMapper extends BaseMapper<NewCtFinancialTarget> {

}
