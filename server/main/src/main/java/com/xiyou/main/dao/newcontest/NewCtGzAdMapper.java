package com.xiyou.main.dao.newcontest;

import com.xiyou.main.entity.contest.CtMnAd;
import com.xiyou.main.entity.newcontest.NewCtGzAd;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xiyou.main.entity.newcontest.NewCtMnAd;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author wangxingyu
 * @since 2023-06-06
 */
@Repository
public interface NewCtGzAdMapper extends BaseMapper<NewCtGzAd> {
    String getGroupNum(@Param("contestId") Integer contestId, @Param("year") Integer year);

    List<NewCtMnAd> get(Integer contestId);

}
