package com.xiyou.main.dao.newcontest;

import com.xiyou.main.entity.newcontest.NewCtGzClasses;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author wangxingyu
 * @since 2023-06-06
 */
@Repository
public interface NewCtGzClassesMapper extends BaseMapper<NewCtGzClasses> {
    List<NewCtGzClasses> getClassesByContestId(Integer contestId);
}
