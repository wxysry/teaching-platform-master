package com.xiyou.main.dao.newcontest;

import com.xiyou.main.entity.newcontest.NewCtGzDiscount;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author wangxingyu
 * @since 2023-06-06
 */
@Repository
public interface NewCtGzDiscountMapper extends BaseMapper<NewCtGzDiscount> {

    List<NewCtGzDiscount> getGzDiscountByStudentIdAndContestId(@Param("contestId") Integer contestId,@Param("rRemainDate") Integer rRemainDate);
}
