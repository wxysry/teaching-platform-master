package com.xiyou.main.dao.newcontest;

import com.xiyou.main.entity.contest.CtGzIso;
import com.xiyou.main.entity.newcontest.NewCtGzIso;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author wangxingyu
 * @since 2023-06-06
 */
@Repository
public interface NewCtGzIsoMapper extends BaseMapper<NewCtGzIso> {
    List<NewCtGzIso> list(Integer contestId);
}
