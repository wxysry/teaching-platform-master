package com.xiyou.main.dao.newcontest;

import com.xiyou.main.entity.contest.CtGzMarket;
import com.xiyou.main.entity.newcontest.NewCtGzMarket;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author wangxingyu
 * @since 2023-06-06
 */
@Repository
public interface NewCtGzMarketMapper extends BaseMapper<NewCtGzMarket> {

    List<NewCtGzMarket> list(Integer contestId);

    Integer getCmid(@Param("contestId") Integer contestId, @Param("market") String market);

}
