package com.xiyou.main.dao.newcontest;

import com.xiyou.main.entity.contest.CtGzMaterial;
import com.xiyou.main.entity.newcontest.NewCtGzMaterial;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author wangxingyu
 * @since 2023-06-06
 */
@Repository
public interface NewCtGzMaterialMapper extends BaseMapper<NewCtGzMaterial> {

    /**
     * 原料规则表.费用
     * @param contestId
     * @return
     */
    Integer getGzMaterialMoney(Integer contestId);


    List<NewCtGzMaterial> list(Integer contestId);
}
