package com.xiyou.main.dao.newcontest;

import com.xiyou.main.entity.newcontest.NewCtGzNum;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xiyou.main.entity.newcontest.NewCtGzProduct;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author wangxingyu
 * @since 2023-06-06
 */
@Repository
public interface NewCtGzNumMapper extends BaseMapper<NewCtGzNum> {
    List<NewCtGzNum> getListByContestId(Integer contestId);

    List<NewCtGzNum> getList(Integer contestId);
}
