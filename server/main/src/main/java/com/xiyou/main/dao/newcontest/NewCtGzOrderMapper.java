package com.xiyou.main.dao.newcontest;

import com.xiyou.main.entity.contest.CtMnChoose;
import com.xiyou.main.entity.newcontest.NewCtGzOrder;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xiyou.main.entity.newcontest.NewCtMnChoose;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author wangxingyu
 * @since 2023-06-06
 */
@Repository
public interface NewCtGzOrderMapper extends BaseMapper<NewCtGzOrder> {

    List<NewCtMnChoose> getList(Integer contestId);

}
