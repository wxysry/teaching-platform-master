package com.xiyou.main.dao.newcontest;

import com.xiyou.main.entity.newcontest.NewCtGzProducing;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import io.swagger.models.auth.In;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author wangxingyu
 * @since 2023-06-06
 */
@Repository
public interface NewCtGzProducingMapper extends BaseMapper<NewCtGzProducing> {
    List<NewCtGzProducing>  getListByCpId(Integer cpId);
    List<NewCtGzProducing> getListByContestId(Integer contestId);
}
