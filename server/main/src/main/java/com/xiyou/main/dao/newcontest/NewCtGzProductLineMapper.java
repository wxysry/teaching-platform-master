package com.xiyou.main.dao.newcontest;
import com.xiyou.main.entity.newcontest.NewCtGzProductLine;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xiyou.main.vo.newcontest.NewCashFolowEntity;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author wangxingyu
 * @since 2023-06-06
 */
@Repository
public interface NewCtGzProductLineMapper extends BaseMapper<NewCtGzProductLine> {
    /**
     * 获取生产线信息P14
     * @param contestId
     * @return
     */
    List<NewCtGzProductLine> getProductLineTypeList(Integer contestId);

    /**
     * 该生产线的投资时间
     * @param cashFolowEntity
     * @return
     */
    Integer getProductLineInfo(NewCashFolowEntity cashFolowEntity);

}
