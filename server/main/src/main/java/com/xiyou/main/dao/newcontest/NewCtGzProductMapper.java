package com.xiyou.main.dao.newcontest;

import com.xiyou.main.entity.contest.CtGzProduct;
import com.xiyou.main.entity.newcontest.NewCtGzProduct;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xiyou.main.vo.contest.YfProductVO;
import com.xiyou.main.vo.newcontest.NewYfProductVO;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author wangxingyu
 * @since 2023-06-06
 */
@Repository
public interface NewCtGzProductMapper extends BaseMapper<NewCtGzProduct> {

    List<NewYfProductVO> listYfProduct(@Param("studentId") Integer userId, @Param("contestId") Integer contestId);

    /**
     * 生产产品名称
     *
     * @param contestId
     * @return
     */
    List<String> getProductCPName(Integer contestId);


    List<NewCtGzProduct> getList(Integer contestId);

    Integer getCpid(@Param("contestId") Integer contestId, @Param("product") String product);

    NewCtGzProduct get(@Param("contestId") Integer contestId, @Param("cpId") Integer cpId);
}
