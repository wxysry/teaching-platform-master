package com.xiyou.main.dao.newcontest;

import com.xiyou.main.entity.newcontest.NewCtGzWorkerTrain;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.stereotype.Repository;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author wangxingyu
 * @since 2023-06-06
 */
@Repository
public interface NewCtGzWorkerTrainMapper extends BaseMapper<NewCtGzWorkerTrain> {

}
