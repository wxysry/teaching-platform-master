package com.xiyou.main.dao.newcontest;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xiyou.main.entity.newcontest.NewCtKcMaterial;
import com.xiyou.main.vo.newcontest.KcEntity;
import com.xiyou.main.vo.newcontest.NewCashFolowEntity;
import com.xiyou.main.vo.newcontest.NewCtKcMaterialNumVo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author wangxingyu
 * @since 2023-06-11
 */
@Mapper
@Repository
public interface NewCtKcMaterialMapper extends BaseMapper<NewCtKcMaterial> {

    /**
     * 原材料 R1-P5
     * 参数：
     * @return
     */
    List<KcEntity> getKcMaterial(@Param("studentId") Integer studentId, @Param("contestId") Integer contestId);


    /**
     * P11-> 原料编号的剩余数量=剩余数量+dd_material中剩余时间为0的原料编号对应的数量
     *
     * @param cashFolowEntity
     */
    void updateKcMaterial(@Param("cashFolowEntity") NewCashFolowEntity cashFolowEntity);


    int getMaterialSum(@Param("studentId") Integer studentId, @Param("contestId") Integer contestId);

    List<NewCtKcMaterial> listNeed(@Param("contestId") Integer contestId, @Param("list") List<Integer> lineIds);

    List<NewCtKcMaterial> listKc(@Param("contestId") Integer contestId, @Param("studentId") Integer userId);

    List<NewCtKcMaterial> listSellKc(@Param("contestId") Integer contestId, @Param("studentId") Integer userId);

    void insertBatch(@Param("list") List<NewCtKcMaterial> ctKcMaterialList);

    List<NewCtKcMaterialNumVo> listKcNum(@Param("contestId") Integer contestId, @Param("studentId") Integer userId);

    List<NewCtKcMaterial> listByPriceAndDateAndCmId(@Param("contestId") Integer contestId,
                                                  @Param("studentId") Integer studentId,
                                                  @Param("imCmId") Integer imCmId,
                                                  @Param("materialPrice") Integer materialPrice,
                                                  @Param("inInventoryDate") Integer inInventoryDate);

    List<NewCtKcMaterial> list(@Param("contestId") Integer contestId, @Param("studentId") Integer userId);

    List<NewCtKcMaterial> listKcGroupNumber(@Param("contestId") Integer contestId, @Param("studentId") Integer userId);

}
