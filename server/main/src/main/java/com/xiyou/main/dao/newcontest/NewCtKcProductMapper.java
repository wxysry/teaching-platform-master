package com.xiyou.main.dao.newcontest;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xiyou.main.entity.newcontest.NewCtKcProduct;
import com.xiyou.main.vo.newcontest.KcEntity;
import com.xiyou.main.vo.newcontest.NewProductVo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author wangxingyu
 * @since 2023-06-11
 */
@Mapper
@Repository
public interface NewCtKcProductMapper extends BaseMapper<NewCtKcProduct> {

    /**
     * 库存采购信息 P1-P5
     * 参数：产品编号
     * @return
     */
    List<KcEntity> getKCProductNumb(@Param("studentId") Integer studentId, @Param("contestId") Integer contestId);


    int getProductSum(@Param("studentId") Integer studentId, @Param("contestId") Integer contestId);

    /**
     * P9
     * 更新产成品表，如果生产线表剩余生产时间为0，则在产成品表中update库存数量=生产线.生产产品相同的+1，如果有多条则增加多个
     */
    int updateKcNum(@Param("studentId") Integer studentId, @Param("contestId") Integer contestId);

    List<NewCtKcProduct> listKc(@Param("contestId") Integer contestId, @Param("studentId") Integer userId);

    List<NewProductVo> listSellKc(@Param("contestId") Integer contestId, @Param("studentId") Integer userId);

    void insertBatch(@Param("list") List<NewCtKcProduct> ctKcProductList);

    void updateNum(@Param("contestId") Integer contestId,
                   @Param("studentId") Integer studentId,
                   @Param("cpid") Integer cpid,
                   @Param("num") Integer num);


    List<NewCtKcProduct> getKcProductByPriceAndInDate(@Param("contestId") Integer contestId,
                                                      @Param("studentId") Integer studentId,
                                                      @Param("ipCpId") Integer ipCpId,
                                                      @Param("inventoryDate")Integer inventoryDate,
                                                      @Param("realCost")Integer realCost
                                                      );

    //
    List<NewCtKcProduct> getAllKcProductAsc(@Param("contestId") Integer contestId,
                                         @Param("studentId") Integer studentId,
                                         @Param("cpId") Integer cpId
                                         );

    //根据入库时间和价格分组展示
    List<NewCtKcProduct> listKcGroupByPriceAndInventory(@Param("contestId") Integer contestId, @Param("studentId") Integer userId,Integer isInventory );

    //更新产品
    void updateProductToFinish(Integer contestId ,Integer studentId,Integer finishDate);

    //获取库存

}
