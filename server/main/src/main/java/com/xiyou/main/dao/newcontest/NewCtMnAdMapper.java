package com.xiyou.main.dao.newcontest;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xiyou.main.entity.contest.CtMnAd;
import com.xiyou.main.entity.newcontest.NewCtMnAd;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author wangxingyu
 * @since 2023-06-11
 */
@Repository
public interface NewCtMnAdMapper extends BaseMapper<NewCtMnAd> {

    List<NewCtMnAd> getAdList(@Param("contestId") Integer contestId,
                           @Param("studentId") Integer studentId,
                           @Param("year") Integer year,
                           @Param("quarterly") Integer quarterly
    );

    void insertBatch(@Param("list") List<NewCtMnAd> ctMnAdList);

}
