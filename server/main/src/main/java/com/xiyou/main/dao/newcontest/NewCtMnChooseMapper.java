package com.xiyou.main.dao.newcontest;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xiyou.main.entity.contest.CtMnChoose;
import com.xiyou.main.entity.newcontest.NewCtMnChoose;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author wangxingyu
 * @since 2023-06-11
 */
@Repository
public interface NewCtMnChooseMapper extends BaseMapper<NewCtMnChoose> {

    List<NewCtMnChoose> getOrderList(@Param("contestId") Integer contestId,
                                  @Param("studentId") Integer studentId,
                                  @Param("cmid") Integer cmid,
                                  @Param("cpid") Integer cpid,
                                  @Param("year") Integer year,
                                  @Param("quarterly") Integer quarterly);

    void insertBatch(@Param("list") List<NewCtMnChoose> ctMnChooseList);

    List<NewCtMnChoose> getSales(@Param("contestId") Integer contestId,
                              @Param("studentId") Integer studentId,
                              @Param("cmid") Integer cmid,
                              @Param("year") int year);
}
