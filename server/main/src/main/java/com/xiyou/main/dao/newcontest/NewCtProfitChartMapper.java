package com.xiyou.main.dao.newcontest;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xiyou.main.entity.contest.CtProfitChart;
import com.xiyou.main.entity.newcontest.NewCtProfitChart;
import org.springframework.stereotype.Repository;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author wangxingyu
 * @since 2023-06-28
 */
@Repository
public interface NewCtProfitChartMapper extends BaseMapper<NewCtProfitChart> {

}
