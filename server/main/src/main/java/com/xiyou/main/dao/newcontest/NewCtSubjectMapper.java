package com.xiyou.main.dao.newcontest;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.xiyou.main.entity.exam.Resources;
import com.xiyou.main.entity.newcontest.NewCtSubject;
import com.xiyou.main.params.contest.SubjectParam;
import com.xiyou.main.vo.contest.TeacherSubject;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author wangxingyu
 * @since 2023-06-05
 */
@Repository
public interface NewCtSubjectMapper extends BaseMapper<NewCtSubject> {

    Page<NewCtSubject> listByAdmin(Page<NewCtSubject> page, @Param("param") SubjectParam subjectParam);

    Page<NewCtSubject> listByTeacher(Page<NewCtSubject> page, @Param("param") SubjectParam subjectParam);

    NewCtSubject getBySubjectNumber(Integer subjectNumber);

    NewCtSubject getById(Integer subjectId);

    List<TeacherSubject> getTeacherSubject(Integer teacherId);

    void updateUpload(Integer subjectNumber);

    Resources getSpyAttachment(Integer contestId);

    Resources getRuleAttachment(Integer contestId);

    void removeGz(Integer subjectNumber);

    /**
     * 删除主表数据，含new_contest_student
     * @param contestId
     * @param studentId
     */
    void removeMainTableData(@Param("contestId") Integer contestId, @Param("studentId") Integer studentId);

    /**
     * 删除主表数据，不含new_contest_student
     * @param contestId
     * @param studentId
     */
    void removeMainTableDataNS(@Param("contestId") Integer contestId, @Param("studentId") Integer studentId);



    void removeLikeTableData(@Param("contestId") Integer contestId, @Param("studentId") Integer studentId);


    void insertLikeTableDataToMainTable(@Param("contestId") Integer contestId, @Param("studentId") Integer studentId);

    void insertMainTableToLikeTableData(@Param("contestId") Integer contestId, @Param("studentId") Integer studentId);

    void update(NewCtSubject newCtSubject);

    NewCtSubject getByContestId(Integer contestId);

    void removeOrder(@Param("contestId") Integer contestId, @Param("studentId") Integer studentId);

    void backupOrder(@Param("contestId") Integer contestId, @Param("studentId") Integer studentId);

    void removeSeasonLikeTableData(@Param("contestId") Integer contestId, @Param("studentId") Integer studentId);

    void insertMainTableToSeasonLikeTableData(@Param("contestId") Integer contestId, @Param("studentId") Integer studentId);

    void insertLikeTableDataToReasonTable(@Param("contestId") Integer contestId, @Param("studentId") Integer studentId);

    void insertSeasonLikeTableToMainTable (@Param("contestId") Integer contestId, @Param("studentId") Integer studentId);

    List<Integer> seasonDataSum (@Param("contestId") Integer contestId, @Param("studentId") Integer studentId);
}
