package com.xiyou.main.dao.newcontest;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xiyou.main.entity.contest.CtTTime;
import com.xiyou.main.entity.newcontest.NewCtTTime;
import com.xiyou.main.vo.contest.CashFolowEntity;
import com.xiyou.main.vo.newcontest.NewCashFolowEntity;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author wangxingyu
 * @since 2023-06-11
 */
@Repository
public interface NewCtTTimeMapper extends BaseMapper<NewCtTTime> {

    /**
     * @Description: 更新time表，当前时间=当前时间+1
     * @Author:zhengxiaodong
     * @Param: [cashFolowEntity]
     * @return: void
     */
    void updateStudentCtTime(NewCashFolowEntity cashFolowEntity);

    /**
     * @Description: time表的最新date
     * @Author:zhengxiaodong
     * @Param: [cashFolowEntity]
     * @Return: java.lang.Integer
     * @Date: 2019/7/25
     */
    Integer getStudentLatestDate(NewCashFolowEntity cashFolowEntity);

    int addDate(@Param("studentId") Integer studentId, @Param("contestId") Integer contestId, @Param("time") int time);

}