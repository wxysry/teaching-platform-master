package com.xiyou.main.dao.newcontest;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xiyou.main.entity.newcontest.NewCtWorker;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author tangcan
 * @since 2019-08-25
 */
@Repository
public interface NewCtWorkerMapper extends BaseMapper<NewCtWorker> {
    //查询当前时间招聘的所有工人
    List<NewCtWorker> getNewCtWorkerListByContestIdAndCurrTime(Integer contestId,Integer studentId,Integer currentTime);
    List<NewCtWorker> getNewCtWorkerListOnTheJob(Integer contestId,Integer studentId,Integer state);
    List<NewCtWorker> getAllWorkerListByLineId(Integer lineId);
    List<NewCtWorker> getGjWorkerListByLineId(Integer lineId);
    List<NewCtWorker> getPtWorkerListByLineId(Integer lineId);
    //把当前时间入职的人设置为入职状态
    void updateWorkerToOnboarding(Integer contestId, Integer studentId, Integer currentTime);
    List<NewCtWorker> getAllWorker(Integer contestId,Integer studentId);

    /**
     * 更新工人为空闲状态
     * @param lineId
     */
    void  updateWorkerListByLineIdToSpace(Integer lineId);


}
