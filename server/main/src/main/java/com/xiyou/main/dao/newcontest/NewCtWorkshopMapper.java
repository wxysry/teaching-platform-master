package com.xiyou.main.dao.newcontest;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xiyou.main.entity.newcontest.NewCtWorkshop;
import com.xiyou.main.vo.newcontest.NewCashFolowEntity;
import com.xiyou.main.vo.newcontest.NewCtWorkshopEntity;
import com.xiyou.main.vo.newcontest.NewWorkshopVo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;


/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author wangxingyu
 * @since 2023-06-11
 */
@Mapper
@Repository
public interface NewCtWorkshopMapper extends BaseMapper<NewCtWorkshop> {

//    /**
//     * 工厂容量减一
//     *
//     * @param workshopId
//     * @return
//     */
//    boolean tallyDown(Integer workshopId);
//
//    /**
//     * PPT6->workshop info[厂房、生产线信息]
//     *
//     * @param cashFolowEntity
//     * @return
//     */
    List<NewCtWorkshopEntity> getWorkShopInfo(NewCashFolowEntity cashFolowEntity);

    int getRentFee(NewCtWorkshop workshop);

//    /**
//     * 工厂容量加一
//     *
//     * @param plWid
//     * @return
//     */
//    boolean tallyUp(Integer plWid);

    List<NewWorkshopVo> list(Map<String, Object> queryMap);


    int getWorkshopFeeSum(@Param("studentId") Integer studentId, @Param("contestId") Integer contestId);

    Integer queryWorkshopRentMoney(Map<String, Object> queryMap);


    List<Map<String, Object>> baseInfoList(@Param("studentId") Integer studentId, @Param("contestId") Integer contestId);

    List<NewWorkshopVo> listWorkshop(List<Integer> integers);


    List<NewCtWorkshop> getList(@Param("contestId") Integer contestId, @Param("studentId") Integer studentId);

    void updateWpayDate(Map<String, Object> queryMap);
}
