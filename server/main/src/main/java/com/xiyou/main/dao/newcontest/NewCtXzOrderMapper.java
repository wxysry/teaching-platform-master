package com.xiyou.main.dao.newcontest;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xiyou.main.entity.newcontest.NewCtXzOrder;
import com.xiyou.main.vo.newcontest.NewCashFolowEntity;
import com.xiyou.main.vo.newcontest.NewXzOrderVo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author wangxingyu
 * @since 2023-06-11
 */
@Mapper
@Repository
public interface NewCtXzOrderMapper extends BaseMapper<NewCtXzOrder> {


    /**
     * 销售收入
     * 参数：studentId、contestId
     *
     * @param cashFolowEntity
     * @return
     */
    Integer getStudentSalesProfit(NewCashFolowEntity cashFolowEntity);

    /**
     * 直接成本[本年]
     * 参数：studentId、contestId、currentYear
     *
     * @param cashFolowEntity
     * @return
     */
    Integer getStudentProductDevCost(NewCashFolowEntity cashFolowEntity);


    List<NewCtXzOrder> list(NewCtXzOrder order);

    Map<String, BigDecimal> getSalesAndDirectCost(@Param("studentId") Integer studentId, @Param("contestId") Integer contestId, @Param("year") int year);

    List<NewXzOrderVo> listDeliveryOrder(Map<String, Object> queryMap);

    //获取所有订单
    List<NewCtXzOrder> getList(@Param("contestId") Integer contestId, @Param("studentId") Integer studentId);

    Integer getSales(@Param("contestId") Integer contestId, @Param("studentId") Integer studentId, @Param("cmid") Integer cmid, @Param("year") int year);

    void removeNowYear(@Param("contestId") Integer contestId, @Param("studentId") Integer studentId, @Param("year") int year,@Param("quarterly") int quarterly);

}
