package com.xiyou.main.dao.newcontest;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xiyou.main.entity.newcontest.NewCtYfIso;
import com.xiyou.main.vo.newcontest.NewMarketReturnEntity;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author wangxingyu
 * @since 2023-06-11
 */
@Mapper
@Repository
public interface NewCtYfIsoMapper extends BaseMapper<NewCtYfIso> {

    void update(NewCtYfIso yfIso);

    /**
     * ISO认证
     *
     * @return
     */
    List<String> getISONameList(Integer marketId);

    /**
     * ISO认证 [ISO9000、ISO14000]
     *
     * @param isoId
     * @return
     */
    NewMarketReturnEntity getYFISOProportion(@Param("isoId") Integer isoId, @Param("studentId") Integer studentId, @Param("contestId") Integer contestId);


    List<String> getIsoList(@Param("studentId") Integer studentId, @Param("contestId") Integer contestId);

    /**
     * @Author: tangcan
     * @Description: iso投资列表 p30
     * @Param: [studentId, contestId]
     * @date: 2019/9/1
     */
    List<Map<String, Object>> getIsoInvestList(@Param("studentId") Integer studentId, @Param("contestId") Integer contestId);

    Integer getIsoInvestSum(@Param("list") List<Integer> isoIds);

    void updateISOInvest(@Param("studentId") Integer studentId, @Param("contestId") Integer contestId, @Param("list") List<Integer> isoIds);

    void insertBatch(@Param("list") List<NewCtYfIso> ctYfIsoList);

    List<NewCtYfIso> getYfFinishList(@Param("contestId") Integer contestId, @Param("studentId") Integer studentId);

    List<NewCtYfIso> getList(@Param("contestId") Integer contestId, @Param("studentId") Integer studentId);

    NewCtYfIso getISOInvest(@Param("studentId") Integer studentId, @Param("contestId") Integer contestId, @Param("isoId") Integer isoId);

    void updateStateToFinish(Integer studentId,Integer contestId,Integer finishDate);
}
