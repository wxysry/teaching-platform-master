package com.xiyou.main.dao.newcontest;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xiyou.main.entity.contest.CtYfMarket;
import com.xiyou.main.entity.newcontest.NewCtYfMarket;
import com.xiyou.main.vo.newcontest.NewMarketReturnEntity;
import com.xiyou.main.vo.newcontest.NewMarketVo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author wangxingyu
 * @since 2023-06-11
 */
@Mapper
@Repository
public interface NewCtYfMarketMapper extends BaseMapper<NewCtYfMarket> {

    void update(NewCtYfMarket yfMarket);

    /**
     * 市场准入
     *
     * @return
     */
    List<String> getYFMarketNames(@Param("studentId") Integer studentId, @Param("contestId") Integer contestId);

    /**
     * 本地、区域、国内、亚洲、国际占比
     * 参数：市场编号
     *
     * @param cmId
     * @return
     */
    NewMarketReturnEntity getYFMarketProportion(@Param("cmId") Integer cmId, @Param("studentId") Integer studentId, @Param("contestId") Integer contestId);

    List<String> getHadYfFinishMarkets(@Param("studentId") Integer studentId, @Param("contestId") Integer contestId);

    List<String> getMarketList(@Param("studentId") Integer studentId, @Param("contestId") Integer contestId);

    List<NewMarketVo> listYfMarket(@Param("studentId") Integer studentId, @Param("contestId") Integer contestId);

    void insertBatch(@Param("list") List<NewCtYfMarket> ctYfMarketList);

    List<NewCtYfMarket> getList(@Param("contestId") Integer contestId, @Param("studentId") Integer studentId);

    void updateStateToFinish(Integer studentId,Integer contestId,Integer finishDate);
}
