package com.xiyou.main.dao.newcontest;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xiyou.main.entity.newcontest.NewCtYfNum;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author wangxingyu
 * @since 2023-06-06
 */
@Repository
public interface NewCtYfNumMapper extends BaseMapper<NewCtYfNum> {
    void updateStateToFinish(Integer studentId, Integer contestId, Integer finishDate);


    List<NewCtYfNum> getList(Integer contestId, Integer studentId);
}
