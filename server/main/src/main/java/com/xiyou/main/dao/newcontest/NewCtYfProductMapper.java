package com.xiyou.main.dao.newcontest;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xiyou.main.entity.contest.CtYfProduct;
import com.xiyou.main.entity.newcontest.NewCtYfProduct;
import com.xiyou.main.vo.newcontest.NewMarketReturnEntity;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author wangxingyu
 * @since 2023-06-11
 */
@Mapper
@Repository
public interface NewCtYfProductMapper extends BaseMapper<NewCtYfProduct> {

    int update(NewCtYfProduct yfProduct);

    /**
     * 生产资格
     *
     * @return
     */
    List<String> getProduceStatus();

    /**
     * P1-P5
     *
     * @param productId
     * @return
     */
    NewMarketReturnEntity getYFProductProportion(@Param("productId") Integer productId, @Param("studentId") Integer studentId, @Param("contestId") Integer contestId);

    void updateEndingSeason(@Param("studentId") Integer userId, @Param("contestId") Integer contestId, @Param("date") Integer date);

    List<String> getProductList(@Param("studentId") Integer studentId, @Param("contestId") Integer contestId);

    void insertBatch(@Param("list") List<NewCtYfProduct> newCtYfProductList);

    List<NewCtYfProduct> getList(@Param("contestId") Integer contestId, @Param("studentId") Integer studentId);

    void updateStateToFinish(Integer studentId,Integer contestId,Integer finishDate);

}
