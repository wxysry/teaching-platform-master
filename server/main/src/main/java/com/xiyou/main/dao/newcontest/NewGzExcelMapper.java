package com.xiyou.main.dao.newcontest;


import com.xiyou.main.entity.newcontest.NewCtGzProductDesign;
import com.xiyou.main.pojo.contest.*;
import com.xiyou.main.pojo.newcontest.*;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author xingzi
 * @date 2019 07 23  14:20
 */
@Repository
public interface NewGzExcelMapper {

    void insertCs(@Param("list") List<NewCtGzCsModel> list);

    void insertAd(@Param("list") List<NewCtGzAdModel> list);

    void insertIso(@Param("list") List<NewCtGzIsoModel> list);

    void insertOrder(@Param("list") List<NewCtGzOrderModel> list);

    void insertMarket(@Param("list") List<NewCtGzMarketModel> list);

    void insertMaterial(@Param("list") List<NewCtGzMaterialModel> list);

    void insertProduct(@Param("list") List<NewCtGzProductModel> list);

    void insertProducing(@Param("list") List<NewCtGzProducingModel> list);

    void insertProductLine(@Param("list") List<NewCtGzProductLineModel> list);


    //产品设计gz_product_design（新增）

    void insertProductDesign(@Param("list") List<NewCtGzProductDesignModel> list);

    //工人招聘gz_worker_recruit（新增）

    void insertWorkerRecruit(@Param("list") List<NewCtGzWorkerRecruitModel> list);

    //工人培训gz_worker_train(新增)

    void insertWorkerTrain(@Param("list") List<NewCtGzWorkerTrainModel> list);

    //贷款规则gz_loan(新增)

    void insertLoan(@Param("list") List<NewCtGzLoanModel> list);

    //贴现规则gz_discount（新增）

    void insertDiscount(@Param("list") List<NewCtGzDiscountModel> list);

    //班次规则gz_classes(新增)

    void insertClasses(@Param("list") List<NewCtGzClassesModel> list);

    //员工激励gz_worker_incentive(新增)

    void insertWorkerIncentive(@Param("list") List<NewCtGzWorkerIncentiveModel> list);

    //数字化岗位gz_num(新增)

    void insertNum(@Param("list") List<NewCtGzNumModel> list);



    void removeRestAd(@Param("list") List<NewCtGzAdModel> newCtGzAdModelList);

    void removeRestCs(@Param("list") List<NewCtGzCsModel> newCtGzCsModelList);

    void removeRestIso(@Param("list") List<NewCtGzIsoModel> newCtGzIsoModelList);

    void removeRestOrder(@Param("list") List<NewCtGzOrderModel> newCtGzOrderModelList);

    void removeRestMarket(@Param("list") List<NewCtGzMarketModel> newCtGzMarketModelList);

    void removeRestMaterial(@Param("list") List<NewCtGzMaterialModel> newCtGzMaterialModelList);

    void removeRestProduct(@Param("list") List<NewCtGzProductModel> newCtGzProductModelList);

    void removeRestProducing(@Param("list") List<NewCtGzProducingModel> newCtGzProducingModelList);

    void removeRestProductLine(@Param("list") List<NewCtGzProductLineModel> newCtGzProductLineModelList);

    //产品设计gz_product_design（新增）

    void removeRestProductDesign(@Param("list") List<NewCtGzProductDesignModel> list);

    //工人招聘gz_worker_recruit（新增）

    void removeRestWorkerRecruit(@Param("list") List<NewCtGzWorkerRecruitModel> list);

    //工人培训gz_worker_train(新增)

    void removeRestWorkerTrain(@Param("list") List<NewCtGzWorkerTrainModel> list);

    //贷款规则gz_loan(新增)

    void removeRestLoan(@Param("list") List<NewCtGzLoanModel> list);

    //贴现规则gz_discount（新增）

    void removeRestDiscount(@Param("list") List<NewCtGzDiscountModel> list);

    //班次规则gz_classes(新增)

    void removeRestClasses(@Param("list") List<NewCtGzClassesModel> list);

    //员工激励gz_worker_incentive(新增)

    void removeRestWorkerIncentive(@Param("list") List<NewCtGzWorkerIncentiveModel> list);

    //数字化岗位gz_num(新增)

    void removeRestNum(@Param("list") List<NewCtGzNumModel> list);

}
