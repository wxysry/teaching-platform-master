package com.xiyou.main.dao.newcontest;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xiyou.main.entity.newcontest.NewRePaymentInfo;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author 吕辉耀
 * @PROJECT_NAME: teaching-platform-master-new
 * @Description:
 * @creat 2023-07-17 16
 */
@Repository
public interface NewRePaymentInfoMapper extends BaseMapper<NewRePaymentInfo> {
    List<NewRePaymentInfo> getListByTime(Integer contestId,Integer studentId,Integer currentTime);
    NewRePaymentInfo getOverhaul(Integer contestId,Integer studentId,Integer currentTime);
    void deleteByAmountAndBlAddTimeAndPayment(Integer contestId,Integer studentId,Integer currentTime,String payment,Integer blAddTime);
    List<NewRePaymentInfo> selectByAmountAndBlAddTimeAndPayment(Integer contestId,Integer studentId,Integer currentTime,String payment,Integer blAddTime);
}
