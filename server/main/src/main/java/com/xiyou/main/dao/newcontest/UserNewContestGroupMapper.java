package com.xiyou.main.dao.newcontest;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xiyou.main.entity.newcontest.NewContestGroup;
import com.xiyou.main.entity.newcontest.UserNewContestGroup;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author tangcan
 * @since 2019-07-09
 */
@Repository
public interface UserNewContestGroupMapper extends BaseMapper<UserNewContestGroup> {

    List<NewContestGroup> listAll();

    List<UserNewContestGroup> getByUserIdList(@Param("list") List<Integer> userIdList);

    int insertBatch(@Param("list") List<UserNewContestGroup> userNewContestGroupList);
}
