package com.xiyou.main.dao.ngbcontest;

import com.xiyou.main.entity.ngbcontest.NGbBuyDataConsult;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.stereotype.Repository;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author wangxingyu
 * @since 2024-04-12
 */
@Repository
public interface NGbBuyDataConsultMapper extends BaseMapper<NGbBuyDataConsult> {

}
