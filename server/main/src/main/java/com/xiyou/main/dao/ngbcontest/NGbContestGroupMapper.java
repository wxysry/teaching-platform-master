package com.xiyou.main.dao.ngbcontest;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xiyou.main.entity.ngbcontest.NGbContestGroup;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author tangcan
 * @since 2019-08-25
 */
@Repository
public interface NGbContestGroupMapper extends BaseMapper<NGbContestGroup> {
    int insert(NGbContestGroup contestGroup);

    List<NGbContestGroup> getListByTeacher(Integer teacherId);

    List<NGbContestGroup> getListByAdmin();
}
