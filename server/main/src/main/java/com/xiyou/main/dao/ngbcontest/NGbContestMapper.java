package com.xiyou.main.dao.ngbcontest;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.xiyou.main.entity.ngbcontest.NGbContest;
import com.xiyou.main.params.contest.ContestParam;
import com.xiyou.main.vo.ngbcontest.NGbMarketForecast;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author tangcan
 * @since 2019-08-25
 */
@Repository
public interface NGbContestMapper extends BaseMapper<NGbContest> {

    Page<NGbContest> getPage(Page<NGbContest> page, @Param("param") ContestParam contestParam);

    int getUnEndContest(@Param("teacherId") Integer teacherId);

    int publish(@Param("teacherId") Integer teacherId, @Param("contestId") Integer contestId);

    Page<NGbContest> getStudentContestPage(Page<NGbContest> page, @Param("param") ContestParam contestParam);

    //根据竞赛contestId获取subjectNum
    NGbContest getObjByContestId(@Param("contestId") Integer contestId);

    //暂停竞赛
    void pauseContest(@Param("contestId") Integer contestId);

    //恢复竞赛
    void recoverContest(@Param("contestId") Integer contestId);

    Page<NGbContest> getPageAdmin(Page<NGbContest> page, @Param("param") ContestParam contestParam);


    /**
     * 删除当前考试的所有数据
     * @param contestId
     */
    void deleteData(Integer contestId);

    /**
     * 删除当前考试的备份数据
     * @param contestId
     */
    void deleteLikeData(Integer contestId);


    /**
     * 删除当前考试的备份数据
     * @param contestId
     */
    void deleteSingleLikeData(Integer contestId);


    /**
     * 获取市场调研结果
     * @param contestId
     * @return
     */
    List<NGbMarketForecast> marketForecast(Integer contestId);
}
