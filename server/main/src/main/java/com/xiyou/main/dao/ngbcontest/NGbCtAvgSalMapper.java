package com.xiyou.main.dao.ngbcontest;

import com.xiyou.main.entity.ngbcontest.NGbCtAvgSal;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xiyou.main.vo.ngbcontest.NGbBigDataAvgSalVo;
import com.xiyou.main.vo.ngbcontest.NGbBigDataSaleAmountVo;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author wangxingyu
 * @since 2024-05-17
 */
@Repository
public interface NGbCtAvgSalMapper extends BaseMapper<NGbCtAvgSal> {

    /**
     * 获取各组当前季度的平均工资
     * @param contestId
     * @param currentTime
     * @return
     */
    List<NGbBigDataAvgSalVo> getAverageSal(Integer contestId, Integer currentTime);

    /**
     * 各组累计销售额
     * @param contestId
     * @param currentTime
     * @return
     */
    List<NGbBigDataSaleAmountVo> bigDataSumSaleAmount(Integer contestId, Integer currentTime);
}
