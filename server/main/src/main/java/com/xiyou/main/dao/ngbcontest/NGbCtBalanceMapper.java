package com.xiyou.main.dao.ngbcontest;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xiyou.main.entity.ngbcontest.NGbCtBalance;
import com.xiyou.main.vo.ngbcontest.NGbCashFolowEntity;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author wangxingyu
 * @since 2023-06-11
 */
@Mapper
@Repository
public interface NGbCtBalanceMapper extends BaseMapper<NGbCtBalance> {

    /**
     * 获取最大贷款额度[上一年所有者权益]
     *
     * @param cashFolowEntity
     * @return
     */
    Integer getBalanceMaxLoanLimit(NGbCashFolowEntity cashFolowEntity);


    Integer getRecentYEarHaveEquity(@Param("studentId") Integer studentId, @Param("contestId") Integer contestId, @Param("nowYear") Integer nowYear);

    int maxTotalEquityPreYear(@Param("studentId") Integer studentId, @Param("contestId") Integer contestId, @Param("currentYear") int currentYear);

    //获取企业资产负债表
    List<NGbCtBalance>  getListByContestId(@Param("contestId") Integer contestId,@Param("nowYear") Integer nowYear);

}
