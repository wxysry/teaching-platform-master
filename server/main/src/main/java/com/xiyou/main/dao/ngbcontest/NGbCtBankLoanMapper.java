package com.xiyou.main.dao.ngbcontest;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xiyou.main.entity.ngbcontest.NGbCtBankLoan;
import com.xiyou.main.vo.ngbcontest.NGbCashFolowEntity;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author wangxingyu
 * @since 2023-06-11
 */
@Mapper
@Repository
public interface NGbCtBankLoanMapper extends BaseMapper<NGbCtBankLoan> {

    /**
     * 长贷总额
     * 参数：studentId、contestId、blType{1：长期贷款，2：短期贷款}、currentTime
     *
     * @param cashFolowEntity
     * @param blType
     * @return
     */
    Integer getStudentLongAndShortTermLoansMoney(@Param("cashFolowEntity") NGbCashFolowEntity cashFolowEntity,
                                                 @Param("blType") Integer blType);

    /**
     * bank_loan中还款时间大于等于当前时间的贷款类型为1和2的总额
     *
     * @param cashFolowEntity
     * @return
     */
    Integer getBackLoanAmount(NGbCashFolowEntity cashFolowEntity);

//    /**
//     * back_loan中贷款类型为2、且归还时间=（当前时间+1）的金额
//     * 参数：studentId、contestId、currentTime+1
//     *
//     * @param cashFolowEntity
//     * @return
//     */
//    Integer getCurrentMoney(NGbCashFolowEntity cashFolowEntity);

    List<NGbCtBankLoan> getStudentLongAndShortTermLoansMoneyList(@Param("cashFolowEntity") NGbCashFolowEntity cashFolowEntity,
                                                              @Param("blType") Integer blType);

    Integer getTotalEquity(NGbCashFolowEntity cashFolowEntity);

//    Integer getFee(NGbCashFolowEntity cashFolowEntity);

    List<NGbCtBankLoan> getList(@Param("contestId") Integer contestId, @Param("studentId") Integer studentId);

    /**
     * 获取本年贷款金额
     * @param contestId
     * @param studentId
     * @param year
     * @return
     */
    List<NGbCtBankLoan> getYearDkList(Integer contestId, Integer studentId, Integer year);
}
