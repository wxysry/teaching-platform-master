package com.xiyou.main.dao.ngbcontest;

import com.xiyou.main.entity.ngbcontest.NGbCtBudgetApply;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author wangxingyu
 * @since 2024-02-18
 */
public interface NGbCtBudgetApplyMapper extends BaseMapper<NGbCtBudgetApply> {

}
