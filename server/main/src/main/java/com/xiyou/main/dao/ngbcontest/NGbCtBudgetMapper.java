package com.xiyou.main.dao.ngbcontest;

import com.xiyou.main.entity.ngbcontest.NGbCtBudget;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author wangxingyu
 * @since 2024-02-18
 */
public interface NGbCtBudgetMapper extends BaseMapper<NGbCtBudget> {

}
