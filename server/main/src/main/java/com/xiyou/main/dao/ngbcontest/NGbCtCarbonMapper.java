package com.xiyou.main.dao.ngbcontest;

import com.xiyou.main.entity.ngbcontest.NGbCtCarbon;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.stereotype.Repository;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author wangxingyu
 * @since 2024-04-19
 */
@Repository
public interface NGbCtCarbonMapper extends BaseMapper<NGbCtCarbon> {

}
