package com.xiyou.main.dao.ngbcontest;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xiyou.main.entity.ngbcontest.NGbCtCorporate;
import org.springframework.stereotype.Repository;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author tangcan
 * @since 2019-08-25
 */
@Repository
public interface NGbCtCorporateMapper extends BaseMapper<NGbCtCorporate> {
//    int minusScore(Integer studentId,Integer contestId);


    int getScore(Integer studentId,Integer contestId);
}
