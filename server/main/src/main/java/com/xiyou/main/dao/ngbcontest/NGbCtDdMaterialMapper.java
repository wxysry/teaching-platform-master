package com.xiyou.main.dao.ngbcontest;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xiyou.main.entity.contest.CtDdMaterial;
import com.xiyou.main.entity.ngbcontest.NGbCtDdMaterial;
import com.xiyou.main.entity.ngbcontest.NGbCtGzMaterial;
import com.xiyou.main.vo.ngbcontest.NGbCashFolowEntity;
import com.xiyou.main.vo.ngbcontest.NGbKcEntity;
import com.xiyou.main.vo.newcontest.NewCashFolowEntity;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author wangxingyu
 * @since 2023-06-11
 */
@Mapper
@Repository
public interface NGbCtDdMaterialMapper extends BaseMapper<NGbCtDdMaterial> {


    /**
     * 原料采购剩余时间为0的数量
     */
    List<NGbCtDdMaterial> getRemainDateISZeroNumb(@Param("contestId") Integer contestId, @Param("studentId") Integer studentId);

    /**
     * 更新dd_material记录，原料编号为相同的，将剩余时间为1的剩余数量更新到剩余时间为0的，
     * 然后将剩余时间为1的剩余数量更新为0
     *
     * @param cashFolowEntity
     */
    void updateDdMaterialRemainDateEqOne(NGbCashFolowEntity cashFolowEntity);


    void updateMaterial(@Param("list") List<CtDdMaterial> materialList);

    List<NGbCtDdMaterial> list(@Param("contestId") Integer contestId, @Param("studentId") Integer studentId);

    Integer getStudentPayMoney(NewCashFolowEntity cashFolowEntity);

    void insertBatch(@Param("list") List<NGbCtDdMaterial> ctDdMaterialList);

    List<NGbCtGzMaterial> getListBySubjectNumber(Integer contestId);


    //根据竞赛Id和学生Id获取所有原料订单的数量
    List<NGbCtDdMaterial> getMaterialOrderEachSum(@Param("contestId")Integer contestId,@Param("currentTime") Integer currentTime);


    /**
     * 获取黑车数量

     * @return
     */
    List<NGbKcEntity> getddMaterialBlackCar(Integer studentId, Integer contestId, Integer currentTime);


    /**
     * 获取灰车数量
     * @return
     */
    List<NGbKcEntity> getddMaterialGreyCar(Integer studentId, Integer contestId, Integer currentTime);

    List<NGbCtDdMaterial> getMaterialOrderRec(Integer studentId, Integer contestId, Integer currentTime);
}
