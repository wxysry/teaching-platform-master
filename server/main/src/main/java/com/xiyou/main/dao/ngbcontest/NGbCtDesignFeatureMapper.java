package com.xiyou.main.dao.ngbcontest;

import com.xiyou.main.entity.ngbcontest.NGbCtDesignFeature;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xiyou.main.entity.ngbcontest.NGbCtGzProduct;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author wangxingyu
 * @since 2024-03-26
 */
@Repository
public interface NGbCtDesignFeatureMapper extends BaseMapper<NGbCtDesignFeature> {


    List<NGbCtGzProduct> getProductList(Integer userId, Integer contestId);
}
