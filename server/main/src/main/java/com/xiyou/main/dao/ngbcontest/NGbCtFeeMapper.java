package com.xiyou.main.dao.ngbcontest;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xiyou.main.entity.ngbcontest.NGbCtFee;
import com.xiyou.main.vo.ngbcontest.NGbCashFolowEntity;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author wangxingyu
 * @since 2023-06-11
 */
@Mapper
@Repository
public interface NGbCtFeeMapper extends BaseMapper<NGbCtFee> {

    /**
     * 查询该用户fee的金额合计
     *
     * @param cashFolowEntity
     * @return
     */
    Integer getStudentReceivableMoney(NGbCashFolowEntity cashFolowEntity);

    List<NGbCtFee> getStudentReceivableMoneyList(NGbCashFolowEntity cashFolowEntity);
    //应收款
    List<NGbCtFee> getRecList(@Param("studentId") Integer userId, @Param("contestId") Integer contestId);
    //应付款
    List<NGbCtFee> getPayList(@Param("studentId") Integer userId, @Param("contestId") Integer contestId);

    void insertBatch(@Param("list") List<NGbCtFee> ctFeeList);
}
