package com.xiyou.main.dao.ngbcontest;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xiyou.main.entity.ngbcontest.NGbCtGzCs;
import com.xiyou.main.vo.ngbcontest.NGbCashFolowEntity;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author wangxingyu
 * @since 2023-06-06
 */
@Repository
public interface NGbCtGzCsMapper extends BaseMapper<NGbCtGzCs> {
    /**
     * 查询gz_cs中Loan_Ceiling
     * 参数：contestId
     *
     * @param cashFolowEntity
     * @return
     */
    Integer getCSLoanCeiling(NGbCashFolowEntity cashFolowEntity);



    NGbCtGzCs getByContestId(@Param("contestId") Integer contestId);



    NGbCtGzCs getBySubjectNumber(Integer subjectNumber);
}
