package com.xiyou.main.dao.ngbcontest;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xiyou.main.entity.ngbcontest.NGbCtGzLoan;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author wangxingyu
 * @since 2023-06-06
 */
@Repository
public interface NGbCtGzLoanMapper extends BaseMapper<NGbCtGzLoan> {
    List<NGbCtGzLoan> listCtGzLoan(Integer newContestId);
}
