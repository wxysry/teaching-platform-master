package com.xiyou.main.dao.ngbcontest;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xiyou.main.entity.ngbcontest.NGbCtGzMarket;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author wangxingyu
 * @since 2023-06-06
 */
@Repository
public interface NGbCtGzMarketMapper extends BaseMapper<NGbCtGzMarket> {

    List<NGbCtGzMarket> list(Integer contestId);

    Integer getCmid(@Param("contestId") Integer contestId, @Param("market") String market);

}
