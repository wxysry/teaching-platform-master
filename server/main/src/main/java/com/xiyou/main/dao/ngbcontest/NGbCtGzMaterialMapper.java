package com.xiyou.main.dao.ngbcontest;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xiyou.main.entity.ngbcontest.NGbCtGzMaterial;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author wangxingyu
 * @since 2023-06-06
 */
@Repository
public interface NGbCtGzMaterialMapper extends BaseMapper<NGbCtGzMaterial> {

    /**
     * 原料规则表.费用
     * @param contestId
     * @return
     */
    Integer getGzMaterialMoney(Integer contestId);


    List<NGbCtGzMaterial> list(Integer contestId);
}
