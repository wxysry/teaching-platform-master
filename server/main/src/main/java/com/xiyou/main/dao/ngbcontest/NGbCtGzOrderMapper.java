package com.xiyou.main.dao.ngbcontest;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xiyou.main.entity.ngbcontest.NGbCtGzOrder;
import com.xiyou.main.entity.ngbcontest.NGbCtMnChoose;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author wangxingyu
 * @since 2023-06-06
 */
@Repository
public interface NGbCtGzOrderMapper extends BaseMapper<NGbCtGzOrder> {

    List<NGbCtMnChoose> getList(Integer contestId);

    List<NGbCtGzOrder> getMarketOrderList(Integer contestId, Integer studentId, int year, int quarterly);

    List<NGbCtGzOrder> getApplyOrderList(Integer contestId, Integer studentId, int year, int quarterly);

    /**
     * 获取有订单的时间
     * @param subjectNumber
     * @return
     */
    List<NGbCtGzOrder> getOrderDate(Integer subjectNumber);
}
