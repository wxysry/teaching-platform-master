package com.xiyou.main.dao.ngbcontest;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xiyou.main.entity.ngbcontest.NGbCtGzProducing;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author wangxingyu
 * @since 2023-06-06
 */
@Repository
public interface NGbCtGzProducingMapper extends BaseMapper<NGbCtGzProducing> {
    List<NGbCtGzProducing>  getListByCpId(Integer cpId);
    List<NGbCtGzProducing> getListByContestId(Integer contestId);
}
