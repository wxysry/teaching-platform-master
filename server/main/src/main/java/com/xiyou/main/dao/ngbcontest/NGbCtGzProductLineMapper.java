package com.xiyou.main.dao.ngbcontest;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xiyou.main.entity.ngbcontest.NGbCtGzProductLine;
import com.xiyou.main.vo.ngbcontest.NGbCashFolowEntity;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author wangxingyu
 * @since 2023-06-06
 */
@Repository
public interface NGbCtGzProductLineMapper extends BaseMapper<NGbCtGzProductLine> {
    /**
     * 获取生产线信息P14
     * @param contestId
     * @return
     */
    List<NGbCtGzProductLine> getProductLineTypeList(Integer contestId);

    /**
     * 该生产线的投资时间
     * @param cashFolowEntity
     * @return
     */
    Integer getProductLineInfo(NGbCashFolowEntity cashFolowEntity);

}
