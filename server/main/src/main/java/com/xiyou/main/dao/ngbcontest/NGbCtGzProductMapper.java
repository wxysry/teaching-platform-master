package com.xiyou.main.dao.ngbcontest;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xiyou.main.entity.ngbcontest.NGbCtGzProduct;
import com.xiyou.main.vo.ngbcontest.NGbYfProductVO;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author wangxingyu
 * @since 2023-06-06
 */
@Repository
public interface NGbCtGzProductMapper extends BaseMapper<NGbCtGzProduct> {

    List<NGbYfProductVO> listYfProduct(@Param("studentId") Integer userId, @Param("contestId") Integer contestId);

    /**
     * 生产产品名称
     *
     * @param contestId
     * @return
     */
    List<String> getProductCPName(Integer contestId);


    List<NGbCtGzProduct> getList(Integer contestId);

    Integer getCpid(@Param("contestId") Integer contestId, @Param("product") String product);

    NGbCtGzProduct get(@Param("contestId") Integer contestId, @Param("cpId") String cpId);
}
