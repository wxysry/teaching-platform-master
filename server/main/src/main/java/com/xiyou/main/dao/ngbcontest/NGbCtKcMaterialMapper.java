package com.xiyou.main.dao.ngbcontest;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xiyou.main.entity.ngbcontest.NGbCtKcMaterial;
import com.xiyou.main.vo.ngbcontest.NGbCashFolowEntity;
import com.xiyou.main.vo.ngbcontest.NGbCtKcMaterialNumVo;
import com.xiyou.main.vo.ngbcontest.NGbKcEntity;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author wangxingyu
 * @since 2023-06-11
 */
@Mapper
@Repository
public interface NGbCtKcMaterialMapper extends BaseMapper<NGbCtKcMaterial> {

    /**
     * 原材料 R1-P5
     * 参数：
     * @return
     */
    List<NGbKcEntity> getKcMaterial(@Param("studentId") Integer studentId, @Param("contestId") Integer contestId);



    int getMaterialSum(@Param("studentId") Integer studentId, @Param("contestId") Integer contestId);


    List<NGbCtKcMaterial> listKc(@Param("contestId") Integer contestId, @Param("studentId") Integer userId);



    List<NGbCtKcMaterialNumVo> listKcNum(@Param("contestId") Integer contestId, @Param("studentId") Integer userId);

    List<NGbCtKcMaterial> listByPriceAndDateAndCmId(@Param("contestId") Integer contestId,
                                                  @Param("studentId") Integer studentId,
                                                   @Param("imCmId") Integer imCmId,
                                                  @Param("materialPrice") Integer materialPrice,
                                                  @Param("inInventoryDate") Integer inInventoryDate);

    List<NGbCtKcMaterial> list(@Param("contestId") Integer contestId, @Param("studentId") Integer userId);

    List<NGbCtKcMaterial> listKcGroupNumber(@Param("contestId") Integer contestId, @Param("studentId") Integer userId);


}
