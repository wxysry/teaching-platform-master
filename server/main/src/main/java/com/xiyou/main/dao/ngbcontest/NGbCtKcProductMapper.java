package com.xiyou.main.dao.ngbcontest;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xiyou.main.entity.ngbcontest.NGbCtKcProduct;
import com.xiyou.main.vo.ngbcontest.NGbKcEntity;
import com.xiyou.main.vo.ngbcontest.NGbProductVo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author wangxingyu
 * @since 2023-06-11
 */
@Mapper
@Repository
public interface NGbCtKcProductMapper extends BaseMapper<NGbCtKcProduct> {

    /**
     * 库存采购信息 P1-P5
     * 参数：产品编号
     * @return
     */
    List<NGbKcEntity> getKCProductNumb(@Param("studentId") Integer studentId, @Param("contestId") Integer contestId);


    int getProductSum(@Param("studentId") Integer studentId, @Param("contestId") Integer contestId);

    /**
     * P9
     * 更新产成品表，如果生产线表剩余生产时间为0，则在产成品表中update库存数量=生产线.生产产品相同的+1，如果有多条则增加多个
     */
    int updateKcNum(@Param("studentId") Integer studentId, @Param("contestId") Integer contestId);

    List<NGbCtKcProduct> listKc(@Param("contestId") Integer contestId, @Param("studentId") Integer userId);

//    List<NGbProductVo> listSellKc(@Param("contestId") Integer contestId, @Param("studentId") Integer userId);

    void insertBatch(@Param("list") List<NGbCtKcProduct> ctKcProductList);

    void updateNum(@Param("contestId") Integer contestId,
                   @Param("studentId") Integer studentId,
                   @Param("cpid") Integer cpid,
                   @Param("num") Integer num);


    List<NGbCtKcProduct> getKcProductByPriceAndInDate(@Param("contestId") Integer contestId,
                                                      @Param("studentId") Integer studentId,
                                                      @Param("ipCpId") Integer ipCpId,
                                                      @Param("inventoryDate")Integer inventoryDate,
                                                      @Param("realCost")Integer realCost,
                                                      @Param("featureNum")String featureNum
                                                      );

    //
    List<NGbCtKcProduct> getAllKcProductAsc(@Param("contestId") Integer contestId,
                                         @Param("studentId") Integer studentId,
                                         @Param("cpId") Integer cpId,
                                            @Param("designNum") String designNum
                                         );

    //根据入库时间和价格分组展示
    List<NGbCtKcProduct> listKcGroupByPriceAndInventory(@Param("contestId") Integer contestId, @Param("studentId") Integer userId,Integer isInventory );

    //更新产品
    void updateProductToFinish(Integer contestId ,Integer studentId,Integer finishDate);


    /**
     * 获取本年入库的产品
     * @param contestId
     * @param studentId
     * @param year
     * @return
     */
    List<NGbCtKcProduct> getYearRkList(Integer contestId, Integer studentId, Integer year);

}
