package com.xiyou.main.dao.ngbcontest;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xiyou.main.entity.contest.CtLine;
import com.xiyou.main.entity.ngbcontest.NGbCtGzProductLine;
import com.xiyou.main.entity.ngbcontest.NGbCtLine;
import com.xiyou.main.vo.ngbcontest.NGbBigDataLineNumVo;
import com.xiyou.main.vo.ngbcontest.NGbBigDataSaleAmountVo;
import com.xiyou.main.vo.ngbcontest.NGbOnlineLine;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author tangcan
 * @since 2019-07-22
 */
@Repository
public interface NGbCtLineMapper extends BaseMapper<NGbCtLine> {

    List<NGbOnlineLine> listOnline(@Param("studentId") Integer userId, @Param("contestId") Integer contestId, @Param("date") Integer date);

    int getMaintenanceFee(Integer studentId, Integer contestId, Integer date);

    List<NGbCtGzProductLine> getLineInfo(@Param("list") List<Integer> lineIds);

    List<NGbOnlineLine> listTransfer(@Param("studentId") Integer userId, @Param("contestId") Integer contestId);

    void updateDepFee(@Param("studentId") Integer userId, @Param("contestId") Integer contestId, @Param("date") Integer date);

    Integer getDepTotal(@Param("studentId") Integer userId, @Param("contestId") Integer contestId, @Param("date") Integer date);

    int getProductInProcess(NGbCtLine line);

    int getEquipmentSum(@Param("studentId") Integer userId, @Param("contestId") Integer contestId);

    List<NGbOnlineLine> listNoProducting(@Param("studentId") Integer userId, @Param("contestId") Integer contestId);

    /**
     * 显示line中PL_Remain_Date 不为空的数据
     *
     * @param userId
     * @param contestId
     * @return
     */
    List<NGbOnlineLine> getListOnline(@Param("studentId") Integer userId, @Param("contestId") Integer contestId);

    /*
    如果剩余生产时间为0，则开产时间、剩余生产时间置空，如剩余生产时间>0,则开产时间不变，剩余生产时间=原剩余生产时间-1
     */
    int updateProductAddDateAndProductingDate(@Param("studentId") Integer studentId, @Param("contestId") Integer contestId);

    /*
    如果剩余时间为0，则置空，如>0，则-1；转产剩余时间为0，则置空剩余时间及转产时间，如>0，则-1
     */
    int updateRemainDateAndTransferAddDate(@Param("studentId") Integer studentId, @Param("contestId") Integer contestId, @Param("date") Integer date);

    int add(NGbCtLine line);

    void updateBatch(@Param("list") List<CtLine> lines);

    List<NGbCtLine> getList(Integer contestId, Integer studentId);

    List<NGbCtLine> getAllLineList(Integer contestId, Integer studentId);

    //根据产线ID获取产线规则
    NGbCtGzProductLine getGzLineByLineId(Integer lineId);

    //获取不同状态的产线
    List<NGbCtLine> getLineListOfDifStatus(Integer contestId,Integer studentId,String status);

    //在建-->空闲
    void updateLineStatusBuildToSpace(Integer contestId, Integer studentId, Integer finishDate, String oldStatus, String newStatus);
    //转产-->空闲
    void updateLineStatusTransferToSpace(Integer contestId, Integer studentId, Integer finishDate, String oldStatus, String newStatus);
    //生产-->空闲
    void updateLineStatusProduceToSpace(Integer contestId, Integer studentId, Integer finishDate, String oldStatus, String newStatus);

    //获取所有产线按照建成时间排序
    List<NGbCtLine> getLineListOrderByFinishTime(Integer studentId, Integer contestId , Integer currentTime);


    //获取所有在产产线当季结束生产的产线
    List<NGbCtLine> getLineListFinishTime(Integer studentId, Integer contestId , Integer finishDate);

    //
    int getCountLine(Integer contestId, Integer studentId );


    /**
     * 获取本年建成的产线
     * @param contestId
     * @param studentId
     * @param year
     * @return
     */
    List<NGbCtLine> getYearJcLineList(Integer contestId, Integer studentId, Integer year);

    List<NGbBigDataLineNumVo> bigDataLineNum(Integer contestId, Integer currentTime);
}
