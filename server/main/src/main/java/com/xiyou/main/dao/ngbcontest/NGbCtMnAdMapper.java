package com.xiyou.main.dao.ngbcontest;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xiyou.main.entity.ngbcontest.NGbCtMnAd;
import com.xiyou.main.vo.ngbcontest.NGbBigDataAdTotalVo;
import com.xiyou.main.vo.ngbcontest.NGbCtMnAdVo;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author wangxingyu
 * @since 2023-06-11
 */
@Repository
public interface NGbCtMnAdMapper extends BaseMapper<NGbCtMnAd> {

    List<NGbCtMnAd> getAdList(@Param("contestId") Integer contestId,
                           @Param("studentId") Integer studentId,
                           @Param("year") Integer year,
                           @Param("quarterly") Integer quarterly
    );

    void insertBatch(@Param("list") List<NGbCtMnAd> ctMnAdList);

    //获取该竞赛的所有组投放广告的信息
    List<NGbCtMnAd> getAdListByContestId(@Param("contestId") Integer contestId,
                             @Param("year") Integer year,
                             @Param("quarterly") Integer quarterly
    );

//    //获取该竞赛的所有组投放广告的信息
//    List<NGbCtMnAdVo> getAdListAndAccountByContestId(@Param("contestId") Integer contestId,
//                                                    @Param("year") Integer year,
//                                                    @Param("quarterly") Integer quarterly
//    );

    List<NGbCtMnAd> getAdListAndAccountByYear(@Param("contestId") Integer contestId,
                             @Param("year") Integer year
    );


    List<NGbCtMnAd> getAdRank(Integer contestId, Integer year, Integer quarterly, Integer cmId);


    /**
     * 获取所有组的累计广告
     * @param contestId
     * @return
     */
    List<NGbBigDataAdTotalVo> getAllAdTotal(Integer contestId);
}
