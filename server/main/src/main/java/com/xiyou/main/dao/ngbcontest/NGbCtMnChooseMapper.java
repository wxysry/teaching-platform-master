package com.xiyou.main.dao.ngbcontest;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xiyou.main.entity.ngbcontest.NGbCtMnChoose;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author wangxingyu
 * @since 2023-06-11
 */
@Repository
public interface NGbCtMnChooseMapper extends BaseMapper<NGbCtMnChoose> {

    //获取该竞赛的所有订单 根据时间
    List<NGbCtMnChoose> getOrderList(@Param("contestId") Integer contestId,
                                  @Param("cmid") Integer cmid,
                                  @Param("cpid") Integer cpid,
                                  @Param("year") Integer year,
                                  @Param("quarterly") Integer quarterly);

    void insertBatch(@Param("list") List<NGbCtMnChoose> ctMnChooseList);



//    //获取该竞赛的所有订单 根据时间
//    List<NGbCtMnChoose> getOrderBySeason(@Param("contestId") Integer contestId,
//                                    @Param("year") Integer year,
//                                    @Param("quarterly") Integer quarterly);

//    Page<NGbCtXzOrder> getPage(Page<NGbCtXzOrder> page, @Param("param")NGbCtMnChooseParam gbCtMnChooseParam);

}
