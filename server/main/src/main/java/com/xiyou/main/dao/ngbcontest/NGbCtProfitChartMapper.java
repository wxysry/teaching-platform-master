package com.xiyou.main.dao.ngbcontest;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xiyou.main.entity.ngbcontest.NGbCtProfitChart;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author wangxingyu
 * @since 2023-06-28
 */
@Repository
public interface NGbCtProfitChartMapper extends BaseMapper<NGbCtProfitChart> {
    List<NGbCtProfitChart> getListByContestId(@Param("contestId") Integer contestId, @Param("nowYear") Integer nowYear);
}
