package com.xiyou.main.dao.ngbcontest;

import com.xiyou.main.entity.ngbcontest.NGbCtRetailApply;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.stereotype.Repository;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author wangxingyu
 * @since 2024-05-25
 */
@Repository
public interface NGbCtRetailApplyMapper extends BaseMapper<NGbCtRetailApply> {

}
