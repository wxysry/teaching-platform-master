package com.xiyou.main.dao.ngbcontest;

import com.xiyou.main.entity.ngbcontest.NGbCtScore;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.stereotype.Repository;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author wangxingyu
 * @since 2024-05-08
 */
@Repository
public interface NGbCtScoreMapper extends BaseMapper<NGbCtScore> {

}
