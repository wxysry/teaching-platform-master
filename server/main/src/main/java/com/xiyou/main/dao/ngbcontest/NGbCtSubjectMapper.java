package com.xiyou.main.dao.ngbcontest;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.xiyou.main.entity.exam.Resources;
import com.xiyou.main.entity.ngbcontest.NGbCtSubject;
import com.xiyou.main.params.contest.SubjectParam;
import com.xiyou.main.vo.contest.TeacherSubject;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author wangxingyu
 * @since 2023-06-05
 */
@Repository
public interface NGbCtSubjectMapper extends BaseMapper<NGbCtSubject> {

    Page<NGbCtSubject> listByAdmin(Page<NGbCtSubject> page, @Param("param") SubjectParam subjectParam);

    Page<NGbCtSubject> listByTeacher(Page<NGbCtSubject> page, @Param("param") SubjectParam subjectParam);

    NGbCtSubject getBySubjectNumber(Integer subjectNumber);

    NGbCtSubject getById(Integer subjectId);

    List<TeacherSubject> getTeacherSubject(Integer teacherId);

    void updateUpload(Integer subjectNumber);

    /**
     * 获取规则附件
     * @param contestId
     * @return
     */
    Resources getRuleAttachment(Integer contestId);



    Resources getRuleAttachmentBySubjectNumber(Integer subjectNumber);


    /**
     * 更新题目
     * @param gbCtSubject
     */
    void update(NGbCtSubject gbCtSubject);

    /**
     * 获取试题信息
     * @param contestId
     * @return
     */
    NGbCtSubject getByContestId(Integer contestId);





    /**
     * 删除题库时-顺便删除规则表的规则
     * @param subjectNumber
     */
    void removeGz(Integer subjectNumber);

    /**
     * 删除当前的数据
     * @param contestId
     * @param studentId
     */
    void removeMainTableData(@Param("contestId") Integer contestId, @Param("studentId") Integer studentId);


    /**
     * 删除某个时间段的备份数据
     * @param contestId
     * @param studentId
     * @param time
     */
    void removeLikeTableDataByTime(@Param("contestId") Integer contestId, @Param("studentId") Integer studentId,@Param("time") Integer time);



    /**
     * 删除某个时间段的备份的single_like数据
     * @param contestId
     * @param studentId
     * @param time
     */
    void removeSingleLikeTableDataByTime(@Param("contestId") Integer contestId, @Param("studentId") Integer studentId,@Param("time") Integer time);



    /**
     * 删除某个时间段（后）的备份数据
     * @param contestId
     * @param studentId
     * @param date
     */
    void removeLikeTableDataAfterRestoreTime(@Param("contestId") Integer contestId, @Param("studentId") Integer studentId,@Param("date") Integer date);




    /**
     * 删除某个时间段（后）的备份数据
     * @param contestId
     * @param studentId
     * @param date
     */
    void removeSingleLikeTableDataAfterRestoreTime(@Param("contestId") Integer contestId, @Param("studentId") Integer studentId,@Param("date") Integer date);





    /**
     * 还原数据到竞赛表
     * @param contestId
     * @param studentId
     * @param date
     */
    void insertLikeTableDataToMainTable(@Param("contestId") Integer contestId, @Param("studentId") Integer studentId,@Param("date") Integer date);




    /**
     * 还原数据到竞赛表
     * @param contestId
     * @param studentId
     * @param date
     */
    void insertSingleLikeTableDataToMainTable(@Param("contestId") Integer contestId, @Param("studentId") Integer studentId,@Param("date") Integer date);




    /**
     * 竞赛表备份
     * @param contestId
     * @param studentId

     */
    void insertMainTableToLikeTableData(@Param("contestId") Integer contestId, @Param("studentId") Integer studentId);


    /**
     * 竞赛表备份-备份到single_like
     * @param contestId
     * @param studentId

     */
    void insertMainTableToSingleLikeTableData(@Param("contestId") Integer contestId, @Param("studentId") Integer studentId);



    /**
     * 更新TABLE的backUpdate
     * @param contestId
     * @param studentId
     * @param date
     */
    void updateTableBackUpDate(@Param("contestId")Integer contestId, @Param("studentId")Integer studentId,@Param("date") Integer date);



    /**
     * 删除系统公告数据重置选单信息
     * @param contestId
     * @param date
     */
    void removeSystemNoticeAfterRestoreTime(@Param("contestId")Integer contestId, @Param("date") Integer date);

}
