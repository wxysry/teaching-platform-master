package com.xiyou.main.dao.ngbcontest;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xiyou.main.entity.ngbcontest.NGbCtWorker;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author tangcan
 * @since 2019-08-25
 */
@Repository
public interface NGbCtWorkerMapper extends BaseMapper<NGbCtWorker> {
    //查询当前时间招聘的所有工人
//    List<NGbCtWorker> getNGbCtWorkerListByContestIdAndCurrTime(Integer contestId,Integer studentId,Integer currentTime);

    List<NGbCtWorker> getNGbCtWorkerListOnTheJob(Integer contestId,Integer studentId,Integer state);
    List<NGbCtWorker> getAllWorkerListByLineId(Integer lineId);
    List<NGbCtWorker> getGjWorkerListByLineId(Integer lineId);
    List<NGbCtWorker> getPtWorkerListByLineId(Integer lineId);
    //把当前时间入职的人设置为入职状态
    void updateWorkerToOnboarding(Integer contestId, Integer studentId);

    void  updateWorkerListByLineId(Integer lineId);


    /**
     * 更新工人为空闲状态
     * @param lineId
     */
    void updateWorkerListByLineIdToSpace(Integer lineId);


    /**
     * 更新员工薪水
     * @param contestId
     * @param studentId
     */
    void updateWorkerSal(Integer contestId, Integer studentId);


    /**
     * 获取本年入职的工人
     * @param contestId
     * @param studentId
     * @param year
     * @return
     */
    List<NGbCtWorker> getYearRzList(Integer contestId, Integer studentId, Integer year);
}
