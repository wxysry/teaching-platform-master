package com.xiyou.main.dao.ngbcontest;

import com.xiyou.main.entity.ngbcontest.NGbCtWorkerMarket;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author wangxingyu
 * @since 2024-03-21
 */
public interface NGbCtWorkerMarketMapper extends BaseMapper<NGbCtWorkerMarket> {

}
