package com.xiyou.main.dao.ngbcontest;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.xiyou.main.entity.ngbcontest.NGbCtXzOrder;
import com.xiyou.main.params.ngbcontest.NGbCtMnChooseParam;
import com.xiyou.main.vo.ngbcontest.NGbCashFolowEntity;
import com.xiyou.main.vo.ngbcontest.NGbXzOrderVo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author wangxingyu
 * @since 2023-06-11
 */
@Mapper
@Repository
public interface NGbCtXzOrderMapper extends BaseMapper<NGbCtXzOrder> {


    /**
     * 销售收入
     * 参数：studentId、contestId
     *
     * @param cashFolowEntity
     * @return
     */
    Integer getStudentSalesProfit(NGbCashFolowEntity cashFolowEntity);

    /**
     * 直接成本[本年]
     * 参数：studentId、contestId、currentYear
     *
     * @param cashFolowEntity
     * @return
     */
    Integer getStudentProductDevCost(NGbCashFolowEntity cashFolowEntity);


    List<NGbCtXzOrder> list(NGbCtXzOrder order);

    Map<String, BigDecimal> getSalesAndDirectCost(@Param("studentId") Integer studentId, @Param("contestId") Integer contestId, @Param("year") int year);

    List<NGbXzOrderVo> listDeliveryOrder(Map<String, Object> queryMap);

    //获取所有订单
    List<NGbCtXzOrder> getList(@Param("contestId") Integer contestId, @Param("studentId") Integer studentId);


    /**
     * 获取某学生在当年的该市场的年销售额
     * @param contestId
     * @param studentId
     * @param cmid
     * @param year
     * @return
     */
    Integer getStudentYearSales(@Param("contestId") Integer contestId, @Param("studentId") Integer studentId, @Param("cmid") Integer cmid, @Param("year") int year);

    /**
     * 获取所有学生在当年的该市场的年销售额
     * @param contestId
     * @param cmid
     * @param year
     * @return
     */
    Integer getAllYearSales(@Param("contestId") Integer contestId, @Param("cmid") Integer cmid, @Param("year") int year);



    /**
     * 删除当前年/季的订单
     * @param contestId
     * @param year
     * @param quarterly
     */
    void removeNowYear(@Param("contestId") Integer contestId, @Param("year") int year,@Param("quarterly") int quarterly);

    //获取当季订单
    List<NGbCtXzOrder> getListByDate(@Param("contestId") Integer contestId, @Param("year") Integer year, @Param("quarterly") Integer quarterly);


    Page<NGbCtXzOrder> getPage(Page<NGbCtXzOrder> page, @Param("param")NGbCtMnChooseParam gbCtMnChooseParam);
}
