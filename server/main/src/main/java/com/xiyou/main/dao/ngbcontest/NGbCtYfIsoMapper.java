package com.xiyou.main.dao.ngbcontest;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xiyou.main.entity.ngbcontest.NGbCtYfIso;
import com.xiyou.main.vo.ngbcontest.NGbMarketReturnEntity;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author wangxingyu
 * @since 2023-06-11
 */
@Mapper
@Repository
public interface NGbCtYfIsoMapper extends BaseMapper<NGbCtYfIso> {

    void update(NGbCtYfIso yfIso);

    /**
     * ISO认证
     *
     * @return
     */
    List<String> getISONameList(Integer marketId);

    /**
     * ISO认证 [ISO9000、ISO14000]
     *
     * @param isoId
     * @return
     */
    NGbMarketReturnEntity getYFISOProportion(@Param("isoId") Integer isoId, @Param("studentId") Integer studentId, @Param("contestId") Integer contestId);


    List<String> getIsoList(@Param("studentId") Integer studentId, @Param("contestId") Integer contestId);

    /**
     * @Author: tangcan
     * @Description: iso投资列表 p30
     * @Param: [studentId, contestId]
     * @date: 2019/9/1
     */
    List<Map<String, Object>> getIsoInvestList(@Param("studentId") Integer studentId, @Param("contestId") Integer contestId);

    Integer getIsoInvestSum(@Param("list") List<Integer> isoIds);

    void updateISOInvest(@Param("studentId") Integer studentId, @Param("contestId") Integer contestId, @Param("list") List<Integer> isoIds);

    void insertBatch(@Param("list") List<NGbCtYfIso> ctYfIsoList);

    List<NGbCtYfIso> getYfFinishList(@Param("contestId") Integer contestId, @Param("studentId") Integer studentId);

    List<NGbCtYfIso> getList(@Param("contestId") Integer contestId, @Param("studentId") Integer studentId);

    void updateStateToFinish(Integer studentId,Integer contestId,Integer finishDate);
}
