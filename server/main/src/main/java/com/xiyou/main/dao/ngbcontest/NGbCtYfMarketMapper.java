package com.xiyou.main.dao.ngbcontest;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xiyou.main.entity.ngbcontest.NGbCtYfMarket;
import com.xiyou.main.vo.ngbcontest.NGbMarketReturnEntity;
import com.xiyou.main.vo.ngbcontest.NGbMarketVo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author wangxingyu
 * @since 2023-06-11
 */
@Mapper
@Repository
public interface NGbCtYfMarketMapper extends BaseMapper<NGbCtYfMarket> {

    void update(NGbCtYfMarket yfMarket);

    /**
     * 市场准入
     *
     * @return
     */
    List<String> getYFMarketNames(@Param("studentId") Integer studentId, @Param("contestId") Integer contestId);

    /**
     * 本地、区域、国内、亚洲、国际占比
     * 参数：市场编号
     *
     * @param cmId
     * @return
     */
    NGbMarketReturnEntity getYFMarketProportion(@Param("cmId") Integer cmId, @Param("studentId") Integer studentId, @Param("contestId") Integer contestId);

    List<String> getHadYfFinishMarkets(@Param("studentId") Integer studentId, @Param("contestId") Integer contestId);

    List<String> getMarketList(@Param("studentId") Integer studentId, @Param("contestId") Integer contestId);

    List<NGbMarketVo> listYfMarket(@Param("studentId") Integer studentId, @Param("contestId") Integer contestId);

    void insertBatch(@Param("list") List<NGbCtYfMarket> ctYfMarketList);

    List<NGbCtYfMarket> getList(@Param("contestId") Integer contestId, @Param("studentId") Integer studentId);

    void updateStateToFinish(Integer studentId,Integer contestId,Integer finishDate);

    /**
     * 获取本年研发完成的市场
     * @param contestId
     * @param studentId
     * @param year
     * @return
     */
    List<NGbCtYfMarket> getYearYfList(Integer contestId, Integer studentId, Integer year);
}
