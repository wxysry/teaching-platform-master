package com.xiyou.main.dao.ngbcontest;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xiyou.main.entity.ngbcontest.NGbCtYfNum;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author wangxingyu
 * @since 2023-06-06
 */
@Repository
public interface NGbCtYfNumMapper extends BaseMapper<NGbCtYfNum> {
    void updateStateToFinish(Integer studentId, Integer contestId, Integer finishDate);


    List<NGbCtYfNum> getList(Integer contestId, Integer studentId);
}
