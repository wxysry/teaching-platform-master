package com.xiyou.main.dao.ngbcontest;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xiyou.main.entity.ngbcontest.NGbCtYfProduct;
import com.xiyou.main.vo.ngbcontest.NGbMarketReturnEntity;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author wangxingyu
 * @since 2023-06-11
 */
@Mapper
@Repository
public interface NGbCtYfProductMapper extends BaseMapper<NGbCtYfProduct> {

    int update(NGbCtYfProduct yfProduct);

    /**
     * 生产资格
     *
     * @return
     */
    List<String> getProduceStatus();

    /**
     * P1-P5
     *
     * @param productId
     * @return
     */
    NGbMarketReturnEntity getYFProductProportion(@Param("productId") Integer productId, @Param("studentId") Integer studentId, @Param("contestId") Integer contestId);

    void updateEndingSeason(@Param("studentId") Integer userId, @Param("contestId") Integer contestId, @Param("date") Integer date);

    List<String> getProductList(@Param("studentId") Integer studentId, @Param("contestId") Integer contestId);

    void insertBatch(@Param("list") List<NGbCtYfProduct> gbCtYfProductList);

    List<NGbCtYfProduct> getList(@Param("contestId") Integer contestId, @Param("studentId") Integer studentId);

    void updateStateToFinish(Integer studentId,Integer contestId,Integer finishDate);

    /**
     * 获取本年的产品研发列表
     * @param contestId
     * @param studentId
     * @param year
     * @return
     */
    List<NGbCtYfProduct> getYearYfList(Integer contestId, Integer studentId, Integer year);
}
