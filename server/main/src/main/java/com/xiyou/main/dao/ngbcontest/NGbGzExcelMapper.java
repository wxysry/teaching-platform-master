package com.xiyou.main.dao.ngbcontest;


import com.xiyou.main.pojo.ngbcontest.*;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author xingzi
 * @date 2019 07 23  14:20
 */
@Repository
public interface NGbGzExcelMapper {

    void insertCs(@Param("list") List<NGbCtGzCsModel> list);

    void insertAd(@Param("list") List<NGbCtGzAdModel> list);

    void insertIso(@Param("list") List<NGbCtGzIsoModel> list);

    void insertOrder(@Param("list") List<NGbCtGzOrderModel> list);

    void insertMarket(@Param("list") List<NGbCtGzMarketModel> list);

    void insertMaterial(@Param("list") List<NGbCtGzMaterialModel> list);

    void insertProduct(@Param("list") List<NGbCtGzProductModel> list);

    void insertProducing(@Param("list") List<NGbCtGzProducingModel> list);

    void insertProductLine(@Param("list") List<NGbCtGzProductLineModel> list);


    //产品设计gz_product_design（新增）

    void insertProductDesign(@Param("list") List<NGbCtGzProductDesignModel> list);

    //工人招聘gz_worker_recruit（新增）

    void insertWorkerRecruit(@Param("list") List<NGbCtGzWorkerRecruitModel> list);

    //工人培训gz_worker_train(新增)

    void insertWorkerTrain(@Param("list") List<NGbCtGzWorkerTrainModel> list);

    //贷款规则gz_loan(新增)

    void insertLoan(@Param("list") List<NGbCtGzLoanModel> list);

    //贴现规则gz_discount（新增）

    void insertDiscount(@Param("list") List<NGbCtGzDiscountModel> list);

    //班次规则gz_classes(新增)

    void insertClasses(@Param("list") List<NGbCtGzClassesModel> list);

    //员工激励gz_worker_incentive(新增)

    void insertWorkerIncentive(@Param("list") List<NGbCtGzWorkerIncentiveModel> list);

    //数字化岗位gz_num(新增)

    void insertNum(@Param("list") List<NGbCtGzNumModel> list);


    //数字化岗位gz_drainage(新增)

    void insertDrainage(@Param("list") List<NGbCtGzDrainageModel> list);


    //数字化岗位gz_retail_market(新增)

    void insertRetailMarket(@Param("list") List<NGbCtGzRetailMarketModel> list);



    void removeRestAd(@Param("list") List<NGbCtGzAdModel> gbCtGzAdModelList);

    void removeRestCs(@Param("list") List<NGbCtGzCsModel> gbCtGzCsModelList);

    void removeRestIso(@Param("list") List<NGbCtGzIsoModel> gbCtGzIsoModelList);

    void removeRestOrder(@Param("list") List<NGbCtGzOrderModel> gbCtGzOrderModelList);

    void removeRestMarket(@Param("list") List<NGbCtGzMarketModel> gbCtGzMarketModelList);

    void removeRestMaterial(@Param("list") List<NGbCtGzMaterialModel> gbCtGzMaterialModelList);

    void removeRestProduct(@Param("list") List<NGbCtGzProductModel> gbCtGzProductModelList);

    void removeRestProducing(@Param("list") List<NGbCtGzProducingModel> gbCtGzProducingModelList);

    void removeRestProductLine(@Param("list") List<NGbCtGzProductLineModel> gbCtGzProductLineModelList);

    //产品设计gz_product_design（新增）

    void removeRestProductDesign(@Param("list") List<NGbCtGzProductDesignModel> list);

    //工人招聘gz_worker_recruit（新增）

    void removeRestWorkerRecruit(@Param("list") List<NGbCtGzWorkerRecruitModel> list);

    //工人培训gz_worker_train(新增)

    void removeRestWorkerTrain(@Param("list") List<NGbCtGzWorkerTrainModel> list);

    //贷款规则gz_loan(新增)

    void removeRestLoan(@Param("list") List<NGbCtGzLoanModel> list);

    //贴现规则gz_discount（新增）

    void removeRestDiscount(@Param("list") List<NGbCtGzDiscountModel> list);

    //班次规则gz_classes(新增)

    void removeRestClasses(@Param("list") List<NGbCtGzClassesModel> list);

    //员工激励gz_worker_incentive(新增)

    void removeRestWorkerIncentive(@Param("list") List<NGbCtGzWorkerIncentiveModel> list);

    //数字化岗位gz_num(新增)

    void removeRestNum(@Param("list") List<NGbCtGzNumModel> list);

    //引流参数gz_drainage(新增)

    void removeRestDrainage(@Param("list") List<NGbCtGzDrainageModel> list);

    //零售市场gz_retail_market(新增)

    void removeRestRetailMarket(@Param("list") List<NGbCtGzRetailMarketModel> list);

}
