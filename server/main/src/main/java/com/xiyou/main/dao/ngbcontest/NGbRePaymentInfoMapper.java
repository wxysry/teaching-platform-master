package com.xiyou.main.dao.ngbcontest;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xiyou.main.entity.ngbcontest.NGbRePaymentInfo;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author 吕辉耀
 * @PROJECT_NAME: teaching-platform-master-gb
 * @Description:
 * @creat 2023-07-17 16
 */
@Repository
public interface NGbRePaymentInfoMapper extends BaseMapper<NGbRePaymentInfo> {
    List<NGbRePaymentInfo> getListByTime(Integer contestId,Integer studentId,Integer currentTime);
    NGbRePaymentInfo getOverhaul(Integer contestId,Integer studentId,Integer currentTime);
    int deleteByAmountAndBlAddTimeAndPayment(Integer contestId,Integer studentId,Integer currentTime,String payment,Integer blAddTime);
    List<NGbRePaymentInfo> selectByAmountAndBlAddTimeAndPayment(Integer contestId,Integer studentId,Integer currentTime,String payment,Integer blAddTime);
}
