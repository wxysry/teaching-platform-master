package com.xiyou.main.dao.ngbcontest;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xiyou.main.entity.ngbcontest.UserNGbContestGroup;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author tangcan
 * @since 2019-07-09
 */
@Repository
public interface UserNGbContestGroupMapper extends BaseMapper<UserNGbContestGroup> {


    List<UserNGbContestGroup> getByUserIdList(@Param("list") List<Integer> userIdList);

    int insertBatch(@Param("list") List<UserNGbContestGroup> userNGbContestGroupList);
}
