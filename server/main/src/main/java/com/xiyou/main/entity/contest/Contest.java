package com.xiyou.main.entity.contest;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.baomidou.mybatisplus.annotation.TableId;

import java.time.LocalDateTime;
import java.io.Serializable;
import java.util.List;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.Range;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * <p>
 *
 * </p>
 *
 * @author tangcan
 * @since 2019-08-25
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value = "Contest对象", description = "")
public class Contest extends Model<Contest> {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "竞赛id")
    @TableId(value = "contest_id", type = IdType.AUTO)
    private Integer contestId;

    @ApiModelProperty(value = "题库号")
    @NotNull(message = "题库号不能为空")
    private Integer subjectNumber;

    @ApiModelProperty(value = "竞赛标题")
    @NotBlank(message = "竞赛标题不能为空")
    @Length(max = 100, message = "竞赛标题长度不能超过{max}")
    private String title;

    @ApiModelProperty(value = "竞赛起始时间")
    @NotNull(message = "请选择竞赛起始时间")
    private LocalDateTime openTimeStart;

    @ApiModelProperty(value = "竞赛结束时间")
    @NotNull(message = "请选择竞赛结束时间")
    private LocalDateTime openTimeEnd;

    @ApiModelProperty(value = "教师id")
    private Integer teacherId;

    @ApiModelProperty(value = "是否发布竞赛")
    private Integer isPublish;

    @ApiModelProperty(value = "0表示向指定考生发布，1表示向全部考生发布")
    @NotNull(message = "请选择考生范围")
    private Integer publishAll;

    private LocalDateTime createTime;

    @ApiModelProperty(value = "学生人数", hidden = true)
    private Integer studentNum;

    @ApiModelProperty(value = "初始权益，即初始资金")
    @NotNull(message = "请填写初始权益")
    @Range(min = 0, max = 8000000, message = "初始权益只能在{min}~{max}之间")
    private Integer equity;

    @ApiModelProperty(value = "是否可以重新经营")
    private Integer restart;

    @ApiModelProperty(value = "是否可以还原本年")
    private Integer restore;

    @ApiModelProperty(value = "是否可以还原本季")
    private Integer restoreSeason;

    @ApiModelProperty(value = "是否可以重新选单")
    private Integer restoreOrder;

    @ApiModelProperty(value = "是否可以导出成绩")
    private Integer exportScore;

    @ApiModelProperty("试卷状态:0未开始，1竞赛中，2已结束")
    @TableField(exist = false)
    private Integer status;

    @ApiModelProperty(value = "学生id列表")
    @TableField(exist = false)
    private List<Integer> studentList;

    @ApiModelProperty(value = "分组id")
    @TableField(exist = false)
    private Integer groupId;

    @ApiModelProperty(value = "分组名称")
    @TableField(exist = false)
    private String groupName;

    @ApiModelProperty(value = "题库名称")
    @TableField(exist = false)
    private String subjectName;

    @ApiModelProperty(value = "是否已开始经营")
    @TableField(exist = false)
    private Integer start;

    @ApiModelProperty(value = "分数")
    @TableField(exist = false)
    private Integer score;

    @ApiModelProperty(value = "经营进度，前端用于显示按钮")
    @TableField(exist = false)
    private String progress;

    @ApiModelProperty(value = "完成时间，为空表示未完成")
    @TableField(exist = false)
    private LocalDateTime finishTime;

    @ApiModelProperty(value = "经营时间进度")
    @TableField(exist = false)
    private Integer date;

    @Override
    protected Serializable pkVal() {
        return this.contestId;
    }

}
