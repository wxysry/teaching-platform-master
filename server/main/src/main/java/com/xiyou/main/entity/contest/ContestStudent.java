package com.xiyou.main.entity.contest;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.baomidou.mybatisplus.annotation.TableId;

import java.io.Serializable;
import java.time.LocalDateTime;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.hibernate.validator.constraints.Length;

/**
 * <p>
 *
 * </p>
 *
 * @author tangcan
 * @since 2019-08-25
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value = "ContestStudent对象", description = "")
public class ContestStudent extends Model<ContestStudent> {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "竞赛模拟的学生")
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty(value = "学生id")
    private Integer studentId;

    @ApiModelProperty(value = "竞赛id")
    private Integer contestId;

    @ApiModelProperty(value = "是否已开始经营")
    private Integer start;

    @ApiModelProperty(value = "经营进度，前端用于显示按钮")
    private String progress;

    @ApiModelProperty(value = "选单进度，前端需要")
    @Length(max = 255, message = "选单进度长度不能超过{max}")
    private String marketList;

    @ApiModelProperty(value = "选单进度，前端需要")
    @Length(max = 255, message = "选单进度长度不能超过{max}")
    private String activeName;

    @ApiModelProperty(value = "经营时间进度")
    private Integer date;

    @ApiModelProperty(value = "分数")
    private Double score;

    @ApiModelProperty(value = "报表填写错误年数，1,2,3表示第1、2、3年填写错误")
    private String errorReportYear;

    @ApiModelProperty(value = "完成时间，为空表示未完成")
    private LocalDateTime finishTime;

    @ApiModelProperty(value = "学生姓名")
    @TableField(exist = false)
    private String studentName;

    @ApiModelProperty(value = "账号")
    @TableField(exist = false)
    private String account;

    @ApiModelProperty(value = "所有者权益")
    @TableField(exist = false)
    private String bsTotalEquity;

    @TableField(exist = false)
    private String contestName;

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
