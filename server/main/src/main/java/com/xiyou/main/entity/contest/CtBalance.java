package com.xiyou.main.entity.contest;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.baomidou.mybatisplus.annotation.TableId;

import java.io.Serializable;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotNull;

/**
 * <p>
 *
 * </p>
 *
 * @author tangcan
 * @since 2019-07-22
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value = "CtBalance对象", description = "")
public class CtBalance extends Model<CtBalance> {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "资产负债表id")
    @TableId(value = "balance_id", type = IdType.AUTO)
    private Integer balanceId;

    @ApiModelProperty(value = "学生id")
    private Integer studentId;

    @ApiModelProperty(value = "考试id")
    private Integer contestId;

    @ApiModelProperty(value = "填写人：1代表系统自动生成，0代表学生录入")
    private Integer bsIsxt;

    @ApiModelProperty(value = "年份")
    private Integer bsYear;

    @ApiModelProperty(value = "现金")
    @NotNull
    private Integer bsCash;

    @ApiModelProperty(value = "应收款")
    @NotNull
    private Integer bsReceivable;

    @ApiModelProperty(value = "在制品")
    @NotNull
    private Integer bsProductInProcess;

    @ApiModelProperty(value = "产成品")
    @NotNull
    private Integer bsProduct;

    @ApiModelProperty(value = "原材料")
    @NotNull
    private Integer bsMaterial;

    @ApiModelProperty(value = "流动资产合计")
    @NotNull
    private Integer bsTotalCurrentAsset;

    @ApiModelProperty(value = "厂房")
    @NotNull
    private Integer bsWorkshop;

    @ApiModelProperty(value = "生产线")
    @NotNull
    private Integer bsEquipment;

    @ApiModelProperty(value = "在建")
    @NotNull
    private Integer bsProjectOnConstruction;

    @ApiModelProperty(value = "固定资产合计")
    @NotNull
    private Integer bsTotalFixedAsset;

    @ApiModelProperty(value = "资产合计")
    @NotNull
    private Integer bsTotalAsset;

    @ApiModelProperty(value = "长期贷款")
    @NotNull
    private Integer bsLongLoan;

    @ApiModelProperty(value = "短期贷款")
    @NotNull
    private Integer bsShortLoan;

    @ApiModelProperty(value = "应交税费")
    @NotNull
    private Integer bsTax;

    @ApiModelProperty(value = "负债合计")
    @NotNull
    private Integer bsTotalLiability;

    @ApiModelProperty(value = "股东资本")
    @NotNull
    private Integer bsEquity = 0;

    @ApiModelProperty(value = "利润留存")
    @NotNull
    private Integer bsRetainedEarning;

    @ApiModelProperty(value = "年度净利")
    @NotNull
    private Integer bsAnnualNetProfit;

    @ApiModelProperty(value = "所有者权益")
    @NotNull
    private Integer bsTotalEquity;

    @ApiModelProperty(value = "负债所有者权益合计")
    @NotNull
    private Integer bsTotal;


    @Override
    protected Serializable pkVal() {
        return this.balanceId;
    }

    public void setZero() {
        bsCash = 0;
        bsReceivable = 0;
        bsProductInProcess = 0;
        bsProduct = 0;
        bsMaterial = 0;
        bsTotalCurrentAsset = 0;
        bsWorkshop = 0;
        bsEquipment = 0;
        bsProjectOnConstruction = 0;
        bsTotalFixedAsset = 0;
        bsTotalAsset = 0;
        bsLongLoan = 0;
        bsShortLoan = 0;
        bsTax = 0;
        bsTotalLiability = 0;
        bsEquity = 0;
        bsRetainedEarning = 0;
        bsAnnualNetProfit = 0;
        bsTotalEquity = 0;
        bsTotal = 0;
    }

    public void add(CtBalance ctBalance) {
        this.bsCash += ctBalance.getBsCash();
        bsReceivable += ctBalance.getBsReceivable();
        bsProductInProcess += ctBalance.getBsProductInProcess();
        bsProduct += ctBalance.getBsProduct();
        bsMaterial += ctBalance.getBsMaterial();
        bsTotalCurrentAsset += ctBalance.getBsTotalCurrentAsset();
        bsWorkshop += ctBalance.getBsWorkshop();
        bsEquipment += ctBalance.getBsEquipment();
        bsProjectOnConstruction += ctBalance.getBsProjectOnConstruction();
        bsTotalFixedAsset += ctBalance.getBsTotalFixedAsset();
        bsTotalAsset += ctBalance.getBsTotalAsset();
        bsLongLoan += ctBalance.getBsLongLoan();
        bsShortLoan += ctBalance.getBsShortLoan();
        bsTax += ctBalance.getBsTax();
        bsTotalLiability += ctBalance.getBsTotalLiability();
        bsEquity += ctBalance.getBsEquity();
        bsRetainedEarning += ctBalance.getBsRetainedEarning();
        bsAnnualNetProfit += ctBalance.getBsAnnualNetProfit();
        bsTotalEquity += ctBalance.getBsTotalEquity();
        bsTotal += ctBalance.getBsTotal();
    }
}
