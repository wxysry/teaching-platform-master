package com.xiyou.main.entity.contest;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 * @author tangcan
 * @since 2019-07-22
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="CtBankLoan对象", description="")
public class CtBankLoan extends Model<CtBankLoan> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "bl_id", type = IdType.AUTO)
    private Integer blId;

    private Integer studentId;

    private Integer contestId;

    @ApiModelProperty(value = "贷款类型")
    private Integer blType;

    @ApiModelProperty(value = "贷款金额")
    private Integer blFee;

    @ApiModelProperty(value = "贷款时间")
    private Integer blAddTime;

    @ApiModelProperty(value = "贷款时长")
    private Integer blRemainTime;

    @ApiModelProperty(value = "还款时间")
    private Integer blRepaymentDate;


    @Override
    protected Serializable pkVal() {
        return this.blId;
    }

}
