package com.xiyou.main.entity.contest;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.baomidou.mybatisplus.annotation.TableId;

import java.io.Serializable;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.hibernate.validator.constraints.Range;

import javax.validation.constraints.NotNull;

/**
 * <p>
 *
 * </p>
 *
 * @author tangcan
 * @since 2019-07-22
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value = "CtCharges对象", description = "")
public class CtCharges extends Model<CtCharges> {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "费用表id")
    @TableId(value = "charges_id", type = IdType.AUTO)
    private Integer chargesId;

    @ApiModelProperty(value = "学生id")
    private Integer studentId;

    @ApiModelProperty(value = "考试id")
    @NotNull
    private Integer contestId;

    @ApiModelProperty(value = "填写人：1代表系统自动生成，0代表学生录入")
    private Integer bsIsxt;

    @ApiModelProperty(value = "年份")
    @NotNull
    private Integer cDate;

    @ApiModelProperty(value = "管理费用")
    @Range(min = 0, message = "管理费用不能小于{min}")
    @NotNull(message = "管理费不能为空")
    private Integer cOverhaul;

    @ApiModelProperty(value = "广告费用")
    @Range(min = 0, message = "广告费用不能小于{min}")
    @NotNull(message = "广告费不能为空")
    private Integer cAd;

    @ApiModelProperty(value = "维护费")
    @Range(min = 0, message = "维护费不能小于{min}")
    @NotNull(message = "设备维护费不能为空")
    private Integer cMaintenance;

    @ApiModelProperty(value = "损失")
    @NotNull(message = "其他不能为空")
    @Range(min = 0, message = "损失不能小于{min}")
    private Integer cDamage;

    @ApiModelProperty(value = "转产费")
    @Range(min = 0, message = "转产费不能小于{min}")
    @NotNull(message = "转产费不能为空")
    private Integer cTransfer;

    @ApiModelProperty(value = "租金")
    @Range(min = 0, message = "租金不能小于{min}")
    @NotNull(message = "厂房租金不能为空")
    private Integer cRent;

    @ApiModelProperty(value = "市场开拓")
    @Range(min = 0, message = "市场开拓不能小于{min}")
    @NotNull(message = "市场准入不能为空")
    private Integer cDevelopMarket;

    @ApiModelProperty(value = "ISO开拓")
    @Range(min = 0, message = "ISO开拓不能小于{min}")
    @NotNull(message = "ISO开发不能为空")
    private Integer cDevelopIso;

    @ApiModelProperty(value = "产品研发")
    @Range(min = 0, message = "产品研发费不能小于{min}")
    @NotNull(message = "产品研发不能为空")
    private Integer cDevelopProduct;

    @ApiModelProperty(value = "合计")
    @NotNull(message = "合计不能为空")
    @Range(min = 0, message = "合计不能小于{min}")
    private Integer cInformation;

    public void setZero() {
        cOverhaul = 0;
        cAd = 0;
        cMaintenance = 0;
        cDamage = 0;
        cTransfer = 0;
        cRent = 0;
        cDevelopMarket = 0;
        cDevelopIso = 0;
        cDevelopProduct = 0;
        cInformation = 0;
    }

    public void add(CtCharges ctCharges) {
        cOverhaul += ctCharges.getCOverhaul();
        cAd += ctCharges.getCAd();
        cMaintenance += ctCharges.getCMaintenance();
        cDamage += ctCharges.getCDamage();
        cTransfer += ctCharges.getCTransfer();
        cRent += ctCharges.getCRent();
        cDevelopMarket += ctCharges.getCDevelopMarket();
        cDevelopIso += ctCharges.getCDevelopIso();
        cDevelopProduct += ctCharges.getCDevelopProduct();
        cInformation += ctCharges.getCInformation();
    }

    @Override
    protected Serializable pkVal() {
        return this.chargesId;
    }

}
