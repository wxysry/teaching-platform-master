package com.xiyou.main.entity.contest;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.baomidou.mybatisplus.annotation.TableId;

import java.io.Serializable;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 *
 * </p>
 *
 * @author tangcan
 * @since 2019-07-22
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value = "CtDdMaterial对象", description = "")
public class CtDdMaterial extends Model<CtDdMaterial> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "dd_material_id", type = IdType.AUTO)
    private Integer ddMaterialId;

    @ApiModelProperty(value = "原料编号")
    private Integer omCmid;

    @ApiModelProperty(value = "原料名称")
    @TableField(exist = false)
    private String cmName;

    @ApiModelProperty(value = "数量")
    private Integer omNum;

    private Integer studentId;

    private Integer contestId;

    @ApiModelProperty(value = "剩余时间")
    private Integer remainDate;

    @ApiModelProperty(value = "剩余时间")
    private Integer purchaseDate;


    @Override
    protected Serializable pkVal() {
        return this.omCmid;
    }

}
