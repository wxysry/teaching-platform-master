package com.xiyou.main.entity.contest;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 * @author tangcan
 * @since 2019-07-22
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="CtFee对象", description="")
public class CtFee extends Model<CtFee> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "fee_id", type = IdType.AUTO)
    private Integer feeId;

    private Integer studentId;

    private Integer contestId;

    @ApiModelProperty(value = "金额")
    private Integer rFee;

    @ApiModelProperty(value = "账期")
    private Integer rRemainDate;


    @Override
    protected Serializable pkVal() {
        return this.feeId;
    }

}
