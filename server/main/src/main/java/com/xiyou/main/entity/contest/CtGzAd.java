package com.xiyou.main.entity.contest;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 * @author tangcan
 * @since 2019-07-25
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="CtGzAd对象", description="")
public class CtGzAd extends Model<CtGzAd> {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "初始广告")
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty(value = "题号")
    private String subjectNum;

    @ApiModelProperty(value = "年份")
    private Integer year;

    @ApiModelProperty(value = "组号")
    private Integer groupNum;

    @ApiModelProperty(value = "本地P1")
    private Double localP1;

    @ApiModelProperty(value = "区域P1")
    private Double regionalP1;

    @ApiModelProperty(value = "国内P1")
    private Double nationalP1;

    @ApiModelProperty(value = "亚洲P1")
    private Double asianP1;

    @ApiModelProperty(value = "国际P1")
    private Double internationalP1;

    @ApiModelProperty(value = "本地P2")
    private Double localP2;

    @ApiModelProperty(value = "区域P2")
    private Double regionalP2;

    @ApiModelProperty(value = "国内P2")
    private Double nationalP2;

    @ApiModelProperty(value = "亚洲P2")
    private Double asianP2;

    @ApiModelProperty(value = "国际P2")
    private Double internationalP2;

    @ApiModelProperty(value = "本地P3")
    private Double localP3;

    @ApiModelProperty(value = "区域P3")
    private Double regionalP3;

    @ApiModelProperty(value = "国内P3")
    private Double nationalP3;

    @ApiModelProperty(value = "亚洲P3")
    private Double asianP3;

    @ApiModelProperty(value = "国际P3")
    private Double internationalP3;

    @ApiModelProperty(value = "本地P4")
    private Double localP4;

    @ApiModelProperty(value = "区域P4")
    private Double regionalP4;

    @ApiModelProperty(value = "国内P4")
    private Double nationalP4;

    @ApiModelProperty(value = "亚洲P4")
    private Double asianP4;

    @ApiModelProperty(value = "国际P4")
    private Double internationalP4;

    @ApiModelProperty(value = "本地P5")
    private Double localP5;

    @ApiModelProperty(value = "区域P5")
    private Double regionalP5;

    @ApiModelProperty(value = "国内P5")
    private Double nationalP5;

    @ApiModelProperty(value = "亚洲P5")
    private Double asianP5;

    @ApiModelProperty(value = "国际P5")
    private Double internationalP5;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
