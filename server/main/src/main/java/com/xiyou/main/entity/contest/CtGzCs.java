package com.xiyou.main.entity.contest;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * <p>
 * 
 * </p>
 *
 * @author tangcan
 * @since 2019-07-22
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="CtGzCs对象", description="")
public class CtGzCs extends Model<CtGzCs> {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "规则id")
    @TableId(value = "cs_id", type = IdType.AUTO)
    private Integer csId;

    @ApiModelProperty(value = "subject_number")
    private Integer subjectNumber;

    @ApiModelProperty(value = "违约金比例")
    private Integer punish;

    @ApiModelProperty(value = "出售产成品")
    private Integer inventoryDiscountProduct;

    @ApiModelProperty(value = "出售原材料")
    private Integer inventoryDiscountMaterial;

    @ApiModelProperty(value = "贷款倍数")
    private Integer loanCeiling;

    @ApiModelProperty(value = "长贷利息")
    private Double longLoanInterests;

    @ApiModelProperty(value = "短贷利息")
    private Double shortLoanInterests;

    @ApiModelProperty(value = "1、2账期贴现")
    @TableField("discount_12")
    private Double discount12;

    @TableField("discount_34")
    @ApiModelProperty(value = "3、4账期贴现")
    private Double discount34;

    @ApiModelProperty(value = "初始现金")
    private Integer cash;

    @ApiModelProperty(value = "管理费")
    private Integer overhaul;

    @ApiModelProperty(value = "最小得单金额")
    private Integer min;

    @ApiModelProperty(value = "信息费")
    private Integer information;

    @ApiModelProperty(value = "紧急采购原材料")
    private Integer emergenceMultipleMaterial;

    @ApiModelProperty(value = "紧急采购产成品")
    private Integer emergenceMultipleProduct;

    @ApiModelProperty(value = "所得税率")
    private Integer incomeTax;

    @ApiModelProperty(value = "最大经营年限")
    private Integer manageMax;

    @ApiModelProperty(value = "最大长贷年限")
    private Integer longLoanMax;

    @ApiModelProperty(value = "选单时间")
    private Integer orderTimeout;

    @ApiModelProperty(value = "间谍时间")
    private Integer spyPeriod;

    @ApiModelProperty(value = "间谍等待")
    private Integer spyInterval;

    @ApiModelProperty(value = "首单增加时间")
    private Integer orderFixTime;

    @ApiModelProperty(value = "拍卖时间")
    private Integer auctionTimeout;

    @ApiModelProperty(value = "拍卖张数")
    private Integer auctionThread;

    @ApiModelProperty(value = "厂房最大数")
    private Integer maxWorkshop;


    @Override
    protected Serializable pkVal() {
        return this.csId;
    }

}
