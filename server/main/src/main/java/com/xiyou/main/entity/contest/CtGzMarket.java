package com.xiyou.main.entity.contest;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 * @author tangcan
 * @since 2019-07-22
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="CtGzMarket对象", description="")
public class CtGzMarket extends Model<CtGzMarket> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty(value = "市场规则表编号")
    private Integer cmId;

    @ApiModelProperty(value = "题库号")
    private Integer subjectNumber;

    @ApiModelProperty(value = "市场名称")
    private String cmName;

    @ApiModelProperty(value = "研发费用")
    private Integer cmDevelopFee;

    @ApiModelProperty(value = "研发周期")
    private Integer cmDevelopDate;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
