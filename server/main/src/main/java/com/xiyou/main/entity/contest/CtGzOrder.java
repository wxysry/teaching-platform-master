package com.xiyou.main.entity.contest;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 * @author tangcan
 * @since 2019-07-22
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="CtGzOrder对象", description="")
public class CtGzOrder extends Model<CtGzOrder> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty(value = "市场订单 订单编号")
    private String coId;

    @ApiModelProperty(value = "题库号")
    private Integer subjectNumber;

    @ApiModelProperty(value = "年份")
    private Integer date;

    @ApiModelProperty(value = "市场")
    private Integer cmId;

    @ApiModelProperty(value = "产品")
    private Integer cpId;

    @ApiModelProperty(value = "数量")
    private Integer num;

    @ApiModelProperty(value = "总价")
    private Integer totalPrice;

    @ApiModelProperty(value = "交货期")
    private Integer deliveryDate;

    @ApiModelProperty(value = "账期")
    private Integer paymentDate;

    @ApiModelProperty(value = "ISO")
    private Integer ciId;

    @ApiModelProperty(value = "选走轮次")
    private Integer xzRound;

    @ApiModelProperty(value = "选走组号")
    private String xzGroup;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
