package com.xiyou.main.entity.contest;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author tangcan
 * @since 2019-07-22
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="CtGzProductLine对象", description="")
public class CtGzProductLine extends Model<CtGzProductLine> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty(value = "生产线编号")
    private Integer cplId;

    @ApiModelProperty(value = "题库号")
    private Integer subjectNum;

    @ApiModelProperty(value = "生产线名称")
    private String cplName;

    @ApiModelProperty(value = "每次投资费用")
    private Integer cplBuyFee;

    @ApiModelProperty(value = "投资周期")
    private Integer cplInstallDate;

    @ApiModelProperty(value = "生产时间")
    private Integer cplProduceDate;

    @ApiModelProperty(value = "转产周期")
    private Integer cplTransferDate;

    @ApiModelProperty(value = "转产费用")
    private Integer cplTransferFee;

    @ApiModelProperty(value = "维护费")
    private Integer cplMaintenanceFee;

    @ApiModelProperty(value = "折旧费")
    private Integer cplScarpFee;

    @ApiModelProperty(value = "折旧年限")
    private Integer cplDepDate;

    @ApiModelProperty(value = "残值")
    private Integer cplDepFee;

    @ApiModelProperty(value = "价格=CPL_Buy_Fee * CPL_Install_Date")
    @TableField(exist = false)
    private Integer cost;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
