package com.xiyou.main.entity.contest;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 * @author tangcan
 * @since 2019-07-22
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="CtGzWorkshop对象", description="")
public class CtGzWorkshop extends Model<CtGzWorkshop> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    private Integer subjectNum;

    @ApiModelProperty(value = "编号")
    private Integer cwId;

    @ApiModelProperty(value = "厂房名称")
    private String cwName;

    @ApiModelProperty(value = "购买费用")
    private Integer cwBuyFee;

    @ApiModelProperty(value = "租金")
    private Integer cwRentFee;

    @ApiModelProperty(value = "售价")
    private Integer cwSellFee;

    @ApiModelProperty(value = "生产线容量")
    private Integer cwCapacity;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
