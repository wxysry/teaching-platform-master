package com.xiyou.main.entity.contest;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 * @author tangcan
 * @since 2019-07-22
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="CtKcMaterial对象", description="")
public class CtKcMaterial extends Model<CtKcMaterial> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "material_id", type = IdType.AUTO)
    private Integer materialId;

    private Integer studentId;

    private Integer contestId;

    @ApiModelProperty(value = "原料编号")
    private Integer imCmId;

    @ApiModelProperty(value = "剩余数量")
    private Integer imNum;


    @Override
    protected Serializable pkVal() {
        return this.materialId;
    }

}
