package com.xiyou.main.entity.contest;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 * @author tangcan
 * @since 2019-07-22
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="CtKcProduct对象", description="")
public class CtKcProduct extends Model<CtKcProduct> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "kc_product_id", type = IdType.AUTO)
    private Integer kcProductId;

    private Integer studentId;

    private Integer contestId;

    @ApiModelProperty(value = "产品编号")
    private Integer ipCpId;

    @ApiModelProperty(value = "库存数量")
    private Integer ipNum;


    @Override
    protected Serializable pkVal() {
        return this.kcProductId;
    }

}
