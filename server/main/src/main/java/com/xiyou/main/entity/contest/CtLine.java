package com.xiyou.main.entity.contest;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.xiyou.main.groups.Add;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * <p>
 *
 * </p>
 *
 * @author tangcan
 * @since 2019-07-22
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value = "CtLine对象", description = "")
public class CtLine extends Model<CtLine> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "line_id", type = IdType.AUTO)
    private Integer lineId;

    private Integer studentId;
    @NotNull(message = "竞赛id不能为空", groups = {Add.class})
    private Integer contestId;

    @NotNull(message = "请选择厂房", groups = {Add.class})
    @ApiModelProperty(value = "所属厂房id（ct_workshop）")
    private Integer workshopId;

    @ApiModelProperty(value = "所属厂房类型id(ct_gz_workshop)")
    private Integer plWid;

    @NotNull(message = "请选择生产线类型", groups = {Add.class})
    @ApiModelProperty(value = "生产线类型id")
    private Integer plCplid;

    @NotNull(message = "请选择生产产品", groups = {Add.class})
    @ApiModelProperty(value = "生产产品id")
    private Integer plCpid;

    @ApiModelProperty(value = "原值")
    private Integer plInvest;

    @ApiModelProperty(value = "累计折旧")
    private Integer plDepTotal;

    @ApiModelProperty(value = "开产时间")
    private Integer plProductAddDate;

    @ApiModelProperty(value = "剩余生产时间")
    private Integer plProductingDate;

    @ApiModelProperty(value = "建成时间")
    private Integer plFinishDate;

    @ApiModelProperty(value = "开建剩余时间")
    private Integer plRemainDate;

    @NotNull(message = "开建时间不能为空", groups = {Add.class})
    @ApiModelProperty(value = "开建时间")
    private Integer plAddTime;

    @ApiModelProperty(value = "转产时间")
    private Integer plTransferAddDate;

    @ApiModelProperty(value = "转产剩余时间")
    private Integer plTransferDate;

    @ApiModelProperty(value = "厂房名称")
    @TableField(exist = false)
    private String cwName;

    /**
     * gz_product_name P1-P4
     */
    @ApiModelProperty(value = "产品名称")
    @TableField(exist = false)
    private String cpName;

    /**
     * 生产线名称
     */
    @ApiModelProperty(value = "生产线名称")
    @TableField(exist = false)
    private String cplName;

    /**
     * 生产线状态：空闲、转产、在产、在建
     */
    @ApiModelProperty(value = "生产线状态")
    @TableField(exist = false)
    private String status;


    @Override
    protected Serializable pkVal() {
        return this.lineId;
    }

}
