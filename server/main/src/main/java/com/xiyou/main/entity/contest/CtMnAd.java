package com.xiyou.main.entity.contest;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.baomidou.mybatisplus.annotation.TableId;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotNull;

/**
 * <p>
 *
 * </p>
 *
 * @author tangcan
 * @since 2019-07-25
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value = "CtMnAd对象", description = "")
public class CtMnAd extends Model<CtMnAd> {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "模拟广告")
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty(value = "学生id")
    private Integer studentId;

    @ApiModelProperty(value = "考试id")
    @NotNull(message = "考试id不能为空")
    private Integer contestId;

    @ApiModelProperty(value = "年份")
    @NotNull(message = "年份不能为空")
    private Integer year;

    @ApiModelProperty(value = "组号")
    private String groupNum;

    @ApiModelProperty(value = "本地P1")
    @NotNull
    private Double localP1;

    @ApiModelProperty(value = "区域P1")
    @NotNull
    private Double regionalP1;

    @ApiModelProperty(value = "国内P1")
    @NotNull
    private Double nationalP1;

    @ApiModelProperty(value = "亚洲P1")
    @NotNull
    private Double asianP1;

    @ApiModelProperty(value = "国际P1")
    @NotNull
    private Double internationalP1;

    @ApiModelProperty(value = "本地P2")
    @NotNull
    private Double localP2;

    @ApiModelProperty(value = "区域P2")
    @NotNull
    private Double regionalP2;

    @ApiModelProperty(value = "国内P2")
    @NotNull
    private Double nationalP2;

    @ApiModelProperty(value = "亚洲P2")
    @NotNull
    private Double asianP2;

    @ApiModelProperty(value = "国际P2")
    @NotNull
    private Double internationalP2;

    @ApiModelProperty(value = "本地P3")
    @NotNull
    private Double localP3;

    @ApiModelProperty(value = "区域P3")
    @NotNull
    private Double regionalP3;

    @ApiModelProperty(value = "国内P3")
    @NotNull
    private Double nationalP3;

    @ApiModelProperty(value = "亚洲P3")
    @NotNull
    private Double asianP3;

    @ApiModelProperty(value = "国际P3")
    @NotNull
    private Double internationalP3;

    @ApiModelProperty(value = "本地P4")
    @NotNull
    private Double localP4;

    @ApiModelProperty(value = "区域P4")
    @NotNull
    private Double regionalP4;

    @ApiModelProperty(value = "国内P4")
    @NotNull
    private Double nationalP4;

    @ApiModelProperty(value = "亚洲P4")
    @NotNull
    private Double asianP4;

    @ApiModelProperty(value = "国际P4")
    @NotNull
    private Double internationalP4;

    @ApiModelProperty(value = "本地P5")
    @NotNull
    private Double localP5;

    @ApiModelProperty(value = "区域P5")
    @NotNull
    private Double regionalP5;

    @ApiModelProperty(value = "国内P5")
    @NotNull
    private Double nationalP5;

    @ApiModelProperty(value = "亚洲P5")
    @NotNull
    private Double asianP5;

    @ApiModelProperty(value = "国际P5")
    @NotNull
    private Double internationalP5;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    public Double getSum() {
        return localP1 + localP2 + localP3 + localP4 + localP5
                + regionalP1 + regionalP2 + regionalP3 + regionalP4 + regionalP5
                + nationalP1 + nationalP2 + nationalP3 + nationalP4 + nationalP5
                + asianP1 + asianP2 + asianP3 + asianP4 + asianP5
                + internationalP1 + internationalP2 + internationalP3 + internationalP4 + internationalP5;
    }

    /*
    根据字段名称获取值，如：本地P1 -> localP1
     */
    public Double getByName(String name) {
        Map<String, Double> map = new HashMap<>();
        map.put("本地P1", this.localP1);
        map.put("区域P1", this.regionalP1);
        map.put("国内P1", this.nationalP1);
        map.put("亚洲P1", this.asianP1);
        map.put("国际P1", this.internationalP1);
        map.put("本地P2", this.localP2);
        map.put("区域P2", this.regionalP2);
        map.put("国内P2", this.nationalP2);
        map.put("亚洲P2", this.asianP2);
        map.put("国际P2", this.internationalP2);
        map.put("本地P3", this.localP3);
        map.put("区域P3", this.regionalP3);
        map.put("国内P3", this.nationalP3);
        map.put("亚洲P3", this.asianP3);
        map.put("国际P3", this.internationalP3);
        map.put("本地P4", this.localP4);
        map.put("区域P4", this.regionalP4);
        map.put("国内P4", this.nationalP4);
        map.put("亚洲P4", this.asianP4);
        map.put("国际P4", this.internationalP4);
        map.put("本地P5", this.localP5);
        map.put("区域P5", this.regionalP5);
        map.put("国内P5", this.nationalP5);
        map.put("亚洲P5", this.asianP5);
        map.put("国际P5", this.internationalP5);
        Double ad = map.get(name);
        return ad == null ? 0 : ad;
    }

    public void setValue() {
        double localSum = (localP1 + localP2 + localP3 + localP4 + localP5);
        double regionalSum = (regionalP1 + regionalP2 + regionalP3 + regionalP4 + regionalP5);
        double nationalSum = (nationalP1 + nationalP2 + nationalP3 + nationalP4 + nationalP5);
        double asianSum = (asianP1 + asianP2 + asianP3 + asianP4 + asianP5);
        double internationalSum = (internationalP1 + internationalP2 + internationalP3 + internationalP4 + internationalP5);
        if (localP1 > 0) {
            this.localP1 += localSum * 0.01;
        }
        if (regionalP1 > 0) {
            this.regionalP1 += regionalSum * 0.01;
        }
        if (nationalP1 > 0) {
            this.nationalP1 += nationalSum * 0.01;
        }
        if (asianP1 > 0) {
            this.asianP1 += asianSum * 0.01;
        }
        if (internationalP1 > 0) {
            this.internationalP1 += internationalSum * 0.01;
        }
        if (localP2 > 0) {
            this.localP2 += localSum * 0.01;
        }
        if (regionalP2 > 0) {
            this.regionalP2 += regionalSum * 0.01;
        }
        if (nationalP2 > 0) {
            this.nationalP2 += nationalSum * 0.01;
        }
        if (asianP2 > 0) {
            this.asianP2 += asianSum * 0.01;
        }
        if (internationalP2 > 0) {
            this.internationalP2 += internationalSum * 0.01;
        }
        if (localP3 > 0) {
            this.localP3 += localSum * 0.01;
        }
        if (regionalP3 > 0) {
            this.regionalP3 += regionalSum * 0.01;
        }
        if (nationalP3 > 0) {
            this.nationalP3 += nationalSum * 0.01;
        }
        if (asianP3 > 0) {
            this.asianP3 += asianSum * 0.01;
        }
        if (internationalP3 > 0) {
            this.internationalP3 += internationalSum * 0.01;
        }
        if (localP4 > 0) {
            this.localP4 += localSum * 0.01;
        }
        if (regionalP4 > 0) {
            this.regionalP4 += regionalSum * 0.01;
        }
        if (nationalP4 > 0) {
            this.nationalP4 += nationalSum * 0.01;
        }
        if (asianP4 > 0) {
            this.asianP4 += asianSum * 0.01;
        }
        if (internationalP4 > 0) {
            this.internationalP4 += internationalSum * 0.01;
        }
        if (localP5 > 0) {
            this.localP5 += localSum * 0.01;
        }
        if (regionalP5 > 0) {
            this.regionalP5 += regionalSum * 0.01;
        }
        if (nationalP5 > 0) {
            this.nationalP5 += nationalSum * 0.01;
        }
        if (asianP5 > 0) {
            this.asianP5 += asianSum * 0.01;
        }
        if (internationalP5 > 0) {
            this.internationalP5 += internationalSum * 0.01;
        }
    }

}
