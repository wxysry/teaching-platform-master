package com.xiyou.main.entity.contest;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.baomidou.mybatisplus.annotation.TableId;

import java.io.Serializable;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 *
 * </p>
 *
 * @author tangcan
 * @since 2019-08-27
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value = "CtMnChoose对象", description = "")
public class CtMnChoose extends Model<CtMnChoose> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "choose_id", type = IdType.AUTO)
    private Integer chooseId;

    private Integer studentId;

    private Integer contestId;

    @ApiModelProperty(value = "定单编号")
    private String coId;

    @ApiModelProperty(value = "年份")
    private Integer date;

    @ApiModelProperty(value = "市场")
    private Integer cmId;

    @ApiModelProperty(value = "产品")
    private Integer cpId;

    @ApiModelProperty(value = "数量")
    private Integer num;

    @ApiModelProperty(value = "总价")
    private Integer totalPrice;

    @ApiModelProperty(value = "交货期")
    private Integer deliveryDate;

    @ApiModelProperty(value = "账期")
    private Integer paymentDay;

    @ApiModelProperty(value = "ISO编号，为空代表不需要任何iso认证，为3代表需要全部iso认证，为1或2代表需要1或2认证")
    private Integer ciId;

    @ApiModelProperty(value = "选走轮次")
    private Integer xzRound;

    @ApiModelProperty(value = "选走组号")
    private String xzGroup;

    @ApiModelProperty(value = "该订单是否可选，根据iso研发完成情况判断")
    @TableField(exist = false)
    private Integer choosable;

    @ApiModelProperty(value = "销售额，本组本市场所有产品总价之和")
    @TableField(exist = false)
    private Integer sales;


    @Override
    protected Serializable pkVal() {
        return this.chooseId;
    }

}
