package com.xiyou.main.entity.contest;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.extension.activerecord.Model;

import java.time.LocalDateTime;
import java.io.Serializable;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * <p>
 *
 * </p>
 *
 * @author tangcan
 * @since 2019-08-25
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value = "CtSubject对象", description = "")
public class CtSubject extends Model<CtSubject> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "subject_id", type = IdType.AUTO)
    private Integer subjectId;

    @ApiModelProperty(value = "题库号")
    @NotNull(message = "题库号不能为空")
    private Integer subjectNumber;

    @ApiModelProperty(value = "题库名称")
    @NotBlank(message = "题库名称不能为空")
    @Length(max = 100, message = "题库名称长度不能超过{max}")
    private String subjectName;

    @ApiModelProperty(value = "题库所属分组")
    @NotNull(message = "请选择分组")
    private Integer groupId;

    @ApiModelProperty(value = "是否已导入题库的规则表")
    private Integer upload;

    @ApiModelProperty(value = "间谍附件")
    private String spyAttachment;

    @ApiModelProperty(value = "规则说明附件")
    private String ruleAttachment;

    @ApiModelProperty(value = "创建时间")
    private LocalDateTime createTime;

    @ApiModelProperty(value = "题库所属分组名称", hidden = true)
    @TableField(exist = false)
    private String groupName;


    @Override
    protected Serializable pkVal() {
        return this.subjectNumber;
    }

}
