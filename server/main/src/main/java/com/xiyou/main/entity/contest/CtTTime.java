package com.xiyou.main.entity.contest;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 * @author tangcan
 * @since 2019-07-22
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="CtTTime对象", description="")
public class CtTTime extends Model<CtTTime> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "time_id", type = IdType.AUTO)
    private Integer timeId;

    @ApiModelProperty(value = "学生id")
    private Integer studentId;

    @ApiModelProperty(value = "竞赛id")
    private Integer contestId;

    @ApiModelProperty(value = "考试时间")
    private Integer date;


    @Override
    protected Serializable pkVal() {
        return this.timeId;
    }

}
