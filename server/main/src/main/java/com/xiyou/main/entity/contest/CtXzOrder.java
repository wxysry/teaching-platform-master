package com.xiyou.main.entity.contest;

import java.math.BigDecimal;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.baomidou.mybatisplus.annotation.TableId;

import java.io.Serializable;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 *
 * </p>
 *
 * @author tangcan
 * @since 2019-07-22
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value = "CtXzOrder对象", description = "")
public class CtXzOrder extends Model<CtXzOrder> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "order_id", type = IdType.AUTO)
    private Integer orderId;

    @ApiModelProperty(value = "订单id")
    private String coId;

    private Integer studentId;

    private Integer contestId;

    @ApiModelProperty(value = "年份")
    private Integer date;

    @ApiModelProperty(value = "市场")
    private Integer cmId;

    @ApiModelProperty(value = "产品")
    private Integer cpId;

    @ApiModelProperty(value = "数量")
    private Integer num;

    @ApiModelProperty(value = "总价")
    private Integer totalPrice;

    @ApiModelProperty(value = "交货期")
    private Integer deliveryDate;

    @ApiModelProperty(value = "账期")
    private Integer paymentDate;

    @ApiModelProperty(value = "ISO")
    private Integer isoId;

    @ApiModelProperty(value = "交单时间")
    private Integer commitDate;

    @ApiModelProperty(value = "订单编号")
    @TableField(exist = false)
    private String coNum;

    @ApiModelProperty(value = "市场名称")
    @TableField(exist = false)
    private String cmName;

    @ApiModelProperty(value = "产品名称")
    @TableField(exist = false)
    private String cpName;

    @ApiModelProperty(value = "状态")
    @TableField(exist = false)
    private String status;


    @Override
    protected Serializable pkVal() {
        return this.orderId;
    }

}
