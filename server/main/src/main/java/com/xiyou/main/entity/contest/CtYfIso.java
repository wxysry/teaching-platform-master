package com.xiyou.main.entity.contest;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.baomidou.mybatisplus.annotation.TableId;

import java.io.Serializable;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 *
 * </p>
 *
 * @author tangcan
 * @since 2019-07-22
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value = "CtYfIso对象", description = "")
public class CtYfIso extends Model<CtYfIso> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "iso_id", type = IdType.AUTO)
    private Integer isoId;

    private Integer studentId;

    private Integer contestId;

    @ApiModelProperty(value = "ISO研发规则表id")
    private Integer diCiid;

    @ApiModelProperty(value = "ISO编号")
    @TableField(exist = false)
    private Integer isoNum;

    @ApiModelProperty(value = "总研发时间")
    private Integer diTotalDate;

    @ApiModelProperty(value = "已研发时间")
    private Integer diNowDate;

    @ApiModelProperty(value = "剩余研发时间")
    private Integer diRemainDate;

    @ApiModelProperty(value = "研发完成时间")
    private Integer diFinishDate;

    @ApiModelProperty(value = "名称")
    @TableField(exist = false)
    private String ciName;

    @ApiModelProperty(value = "开发费用")
    @TableField(exist = false)
    private Integer ciDevelopFee;

    @ApiModelProperty(value = "开发周期")
    @TableField(exist = false)
    private Integer ciDevelopDate;


    @Override
    protected Serializable pkVal() {
        return this.isoId;
    }

}
