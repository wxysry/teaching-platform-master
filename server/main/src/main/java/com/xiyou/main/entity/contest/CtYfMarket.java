package com.xiyou.main.entity.contest;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.baomidou.mybatisplus.annotation.TableId;

import java.io.Serializable;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 *
 * </p>
 *
 * @author tangcan
 * @since 2019-07-22
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value = "CtYfMarket对象", description = "")
public class CtYfMarket extends Model<CtYfMarket> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "market_id", type = IdType.AUTO)
    private Integer marketId;

    private Integer studentId;

    private Integer contestId;

    @ApiModelProperty(value = "市场编号")
    private Integer dmCmId;

    @ApiModelProperty(value = "总研发时间")
    private Integer dmTotalDate;

    @ApiModelProperty(value = "已研发时间")
    private Integer dmNowDate;

    @ApiModelProperty(value = "剩余研发时间")
    private Integer dmRemainDate;

    @ApiModelProperty(value = "研发完成时间")
    private Integer dmFinishDate;

    @ApiModelProperty(value = "研发费用")
    @TableField(exist = false)
    private Integer cmDevelopFee;

    @ApiModelProperty(value = "市场名称")
    @TableField(exist = false)
    private String cmName;

    @ApiModelProperty(value = "研发周期")
    @TableField(exist = false)
    private Integer cmDevelopDate;


    @Override
    protected Serializable pkVal() {
        return this.marketId;
    }

}
