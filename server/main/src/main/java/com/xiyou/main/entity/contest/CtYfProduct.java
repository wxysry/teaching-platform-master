package com.xiyou.main.entity.contest;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 * @author tangcan
 * @since 2019-07-22
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="CtYfProduct对象", description="")
public class CtYfProduct extends Model<CtYfProduct> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "product_id", type = IdType.AUTO)
    private Integer productId;

    private Integer studentId;

    private Integer contestId;

    @ApiModelProperty(value = "产品编号")
    private Integer dpCpId;

    @ApiModelProperty(value = "总研发时间")
    private Integer dpTotalDate;

    @ApiModelProperty(value = "已研发时间")
    private Integer dpNowDate;

    @ApiModelProperty(value = "剩余研发时间")
    private Integer dpRemainDate;

    @ApiModelProperty(value = "研发完成时间")
    private Integer dpFinishDate;

    @ApiModelProperty(value = "名称")
    @TableField(exist = false)
    private String cpName;

    @ApiModelProperty(value = "开发费用")
    @TableField(exist = false)
    private Integer cpProcessingFee;

    @ApiModelProperty(value = "开发周期")
    @TableField(exist = false)
    private Integer cpDevelopDate;


    @Override
    protected Serializable pkVal() {
        return this.productId;
    }

}
