package com.xiyou.main.entity.exam;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.baomidou.mybatisplus.annotation.TableId;

import java.time.LocalDateTime;
import java.io.Serializable;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 *
 * </p>
 *
 * @author tangcan
 * @since 2019-07-10
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value = "Exam对象", description = "")
public class Exam extends Model<Exam> {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "学生的考试的id")
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty(value = "试卷id")
    private Integer paperId;

    @ApiModelProperty(value = "考生id")
    private Integer studentId;

    @ApiModelProperty(value = "教师id")
    private Integer teacherId;

    @ApiModelProperty(value = "考试成绩")
    @TableField(exist = false)
    private Integer grade;

    @ApiModelProperty(value = "考试时间（分钟）")
    private Integer examTime;

    @ApiModelProperty(value = "是否已交卷,0表示未交卷，1表示已交卷")
    private Integer isHandIn;

    @ApiModelProperty(value = "试题总数")
    private Integer problemNum;

    @ApiModelProperty(value = "已批卷题数")
    @TableField(exist = false)
    private Integer correctProblemNumber;

    @ApiModelProperty(value = "疲倦完成时间，为空表示未完成")
    private LocalDateTime correctCompleteTime;

    private LocalDateTime createTime;

    @ApiModelProperty(value = "剩余时间（秒）")
    @TableField(exist = false)
    private Integer restTime;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
