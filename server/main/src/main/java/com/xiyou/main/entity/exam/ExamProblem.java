package com.xiyou.main.entity.exam;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.baomidou.mybatisplus.annotation.TableId;

import java.time.LocalDateTime;
import java.io.Serializable;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import javax.validation.constraints.DecimalMax;
import javax.validation.constraints.DecimalMin;

/**
 * <p>
 *
 * </p>
 *
 * @author tangcan
 * @since 2019-07-10
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value = "ExamProblem对象", description = "")
public class ExamProblem extends Model<ExamProblem> {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "学生的考题答案")
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty(value = "学生的考试的id")
    private Integer examId;

    @ApiModelProperty(value = "试卷id")
    private Integer paperId;

    @ApiModelProperty(value = "考题id")
    private Integer problemId;

    @ApiModelProperty(value = "考题答案，选择题多个选项从小到大排序后逗号隔开")
    private String studentAnswer;

    @ApiModelProperty(value = "本题分数")
    @DecimalMin(value = "0.00", message = "试题分数不能小于{value}")
    @DecimalMax(value = "999.99", message = "试题分数不能大于{value}")
    private Double score;

    @ApiModelProperty(value = "学生分数")
    @DecimalMin(value = "0.00", message = "试题分数不能小于{value}")
    @DecimalMax(value = "999.99", message = "试题分数不能大于{value}")
    private Double studentScore;

    private LocalDateTime createTime;

    @ApiModelProperty(value = "试题答案")
    @TableField(exist = false)
    private String answer;

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
