package com.xiyou.main.entity.exam;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 * @author tangcan
 * @since 2019-07-10
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="ExamRecord对象", description="")
public class ExamRecord extends Model<ExamRecord> {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "每次答题记录的id")
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty(value = "考题")
    private Integer examId;

    @ApiModelProperty(value = "开始答题时间")
    private LocalDateTime startTime;

    @ApiModelProperty(value = "结束答题时间（默认为理论上的结束时间，如果提前退出就更新这个时间，便于计算）")
    private LocalDateTime endTime;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
