package com.xiyou.main.entity.exam;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.Range;

import javax.validation.constraints.*;
import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 *
 * </p>
 *
 * @author tangcan
 * @since 2019-07-04
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value = "Paper对象", description = "")
public class Paper extends Model<Paper> {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "试卷id")
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty(value = "试卷标题")
    @NotBlank(message = "试卷标题不能为空")
    @Length(max = 100, message = "试卷标题长度不能大于{max}")
    private String title;

    @ApiModelProperty(value = "试卷封面图")
    private String cover;

    @ApiModelProperty(value = "考试时间（分钟）")
    @NotNull(message = "请填写开始时间")
    @Min(value = 1, message = "考试时间不能小于{value}")
    @Max(value = 10000, message = "考试时间不能大于{value}")
    private Integer examTime;

    @ApiModelProperty(value = "试卷开放起始时间")
    private LocalDateTime openTimeStart;

    @ApiModelProperty(value = "试卷开放结束时间（为空表示永久开放）")
    private LocalDateTime openTimeEnd;

    @ApiModelProperty(value = "及格分数")
    @Min(value = 0, message = "及格分数不能小于{value}")
    @Max(value = 10000, message = "及格分数不能大于{value}")
    private Double passGrade;

    @ApiModelProperty(value = "考试说明")
    @Length(max = 500, message = "考试说明长度不能大于{max}")
    private String examExplain;

    @ApiModelProperty(value = "教师的user_id")
    private Integer teacherId;

    @ApiModelProperty(value = "是否下发试卷")
    @Range(min = 0, max = 1, message = "是否下发试卷状态只能为{min}和{max}")
    private Integer isPublish;

    @ApiModelProperty(value = "0表示向指定考生发布，1表示向全部考生发布")
    private Integer publishAll;

    @ApiModelProperty(value = "试卷总分")
    @DecimalMin(value = "0.00", message = "试题分数不能小于{value}")
    @DecimalMax(value = "999.99", message = "试题分数不能大于{value}")
    private Double totalScore;

    @ApiModelProperty(value = "试题数量")
    private Integer problemNum;

    private LocalDateTime updateTime;

    private LocalDateTime createTime;

    @ApiModelProperty(value = "学生数量")
    private Integer studentNum;

    @ApiModelProperty(value = "阅卷完成后，是否向学生公开答案，0表示不公开，1表示公开")
    @Range(min = 0, max = 1, message = "是否向学生公开答案的状态只能为{min}和{max}")
    private Integer openAnswer;

    @ApiModelProperty(value = "阅卷完成后，学生查看试题范围，0表示只查看错题，1表示查看全部")
    @Range(min = 0, max = 1, message = "学生查看试题范围的状态只能为{min}和{max}")
    private Integer showAllProblem;

    @ApiModelProperty(value = "已批卷数量")
    @TableField(exist = false)
    private Integer correctNum;

    @ApiModelProperty(value = "已交卷数量")
    @TableField(exist = false)
    private Integer handInNum;

    @ApiModelProperty(value = "试卷状态")
    @TableField(exist = false)
    private Integer status;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
