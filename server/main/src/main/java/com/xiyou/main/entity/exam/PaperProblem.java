package com.xiyou.main.entity.exam;


import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.baomidou.mybatisplus.annotation.TableId;

import java.time.LocalDateTime;
import java.io.Serializable;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import javax.validation.constraints.DecimalMax;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotNull;

/**
 * <p>
 *
 * </p>
 *
 * @author tangcan
 * @since 2019-07-04
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value = "PaperProblem对象", description = "")
public class PaperProblem extends Model<PaperProblem> {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "试卷的考题")
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty(value = "试卷id")
    private Integer paperId;

    @ApiModelProperty(value = "试题id")
    @NotNull(message = "试题id不能为空")
    private Integer problemId;

    @ApiModelProperty(value = "试题总分")
    @NotNull(message = "试题总分不能为空")
    @DecimalMin(value = "0.00", message = "试题分数不能小于{value}")
    @DecimalMax(value = "999.99", message = "试题分数不能大于{value}")
    private Double score;

    private LocalDateTime createTime;

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
