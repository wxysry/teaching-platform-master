package com.xiyou.main.entity.exam;

import com.baomidou.mybatisplus.extension.activerecord.Model;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 * @author tangcan
 * @since 2019-07-16
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="PaperStudent对象", description="")
public class PaperStudent extends Model<PaperStudent> {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "考生")
    private Integer id;

    @ApiModelProperty(value = "试卷id")
    private Integer paperId;

    @ApiModelProperty(value = "学生id")
    private Integer studentId;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
