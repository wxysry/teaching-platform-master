package com.xiyou.main.entity.exam;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.xiyou.main.groups.Add;
import com.xiyou.main.groups.Update;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 *
 * </p>
 *
 * @author tangcan
 * @since 2019-06-28
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value = "PptCourse对象", description = "")
public class PptCourse extends Model<PptCourse> {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "ppt教程")
    @TableId(value = "id", type = IdType.AUTO)
    @NotNull(message = "id不能为空", groups = {Update.class})
    private Integer id;

    @ApiModelProperty(value = "ppt分组的id")
    @NotNull(message = "groupId不能为空", groups = {Update.class, Add.class})
    @Min(value = 1, message = "分组id不能为空", groups = {Update.class, Add.class})
    private Integer groupId;

    @ApiModelProperty(value = "ppt分组名称")
    @TableField(exist = false)
    private String groupName;

    @ApiModelProperty(value = "上传者id")
    private Integer uploadUserId;

    @ApiModelProperty(value = "封面")
    private String cover;

    @ApiModelProperty(value = "ppt名称")
    @NotBlank(message = "ppt教程名称不能为空", groups = {Update.class, Add.class})
    private String pptName;

    @ApiModelProperty(value = "ppt文件名")
    private String fileName;

    @ApiModelProperty(value = "ppt转成图片后存储的文件夹")
    private String folderName;

    @ApiModelProperty(value = "ppt转成图片的数量（所有图片根据编号命名）")
    private Integer picNum;

    @ApiModelProperty(value = "简介")
    @NotBlank(message = "简介不能为空", groups = {Update.class, Add.class})
    private String intro;
    @ApiModelProperty(hidden = true)
    private LocalDateTime updateTime;
    @ApiModelProperty(hidden = true)
    private LocalDateTime createTime;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
