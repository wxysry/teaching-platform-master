package com.xiyou.main.entity.exam;

import com.baomidou.mybatisplus.annotation.IdType;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.baomidou.mybatisplus.annotation.TableId;

import java.io.Serializable;
import java.time.LocalDateTime;

import com.xiyou.main.groups.Add;
import com.xiyou.main.groups.Update;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotNull;

/**
 * <p>
 *
 * </p>
 *
 * @author tangcan
 * @since 2019-06-24
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value = "Problem对象", description = "")
public class Problem extends Model<Problem> {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "考题")
    @TableId(value = "id", type = IdType.AUTO)
    @NotNull(message = "id不能为空", groups = {Update.class})
    private Integer id;

    @ApiModelProperty(value = "考题类型的id")
    @NotNull(message = "typeId不能为空", groups = {Add.class, Update.class})
    private Integer typeId;

    @ApiModelProperty(value = "题型", hidden = true)
    @TableField(exist = false)
    private String typeName;

    @ApiModelProperty(value = "考题分组的id(一级分组)")
    @NotNull(message = "请选择一级分组", groups = {Add.class, Update.class})
    private Integer groupId;

    private String groupName;

    @ApiModelProperty(value = "考题分组的id(二级分组)")
    @NotNull(message = "请选择二级分组", groups = {Add.class, Update.class})
    private Integer subgroupId;

    private String subgroupName;

    @ApiModelProperty(value = "是否是选择题，0表示非选择题，1表示单选题，2表示多选题")
    private Integer isChoice;

    @ApiModelProperty(value = "题目描述")
    private String title;

    @ApiModelProperty(value = "题目图片名称，用|隔开")
    private String pic;

    @ApiModelProperty(value = "考题答案，选择题多个选项从小到大排序后逗号隔开，填空题每个空之间用|隔开，表格题同一表之间逗号隔开，不同表之间|隔开")
    private String answer;

    @ApiModelProperty(value = "考题解析")
    private String analysis;

    @ApiModelProperty(value = "评分要点")
    private String scorePoint;

    @ApiModelProperty(value = "附件,多个附近用|隔开")
    private String attachment;

    @ApiModelProperty(value = "简介")
    private String intro;

    @ApiModelProperty(value = "添加考题的人员id")
    private Integer createUserId;

    @ApiModelProperty(value = "创建人", hidden = true)
    @TableField(exist = false)
    private String createUserName;

    @ApiModelProperty(value = "更新时间", hidden = true)
    private LocalDateTime updateTime;

    private LocalDateTime createTime;

    // 导入辅助字段,便于把插入后的problem_id定位到表格中每条数据
    @TableField(exist = false)
    private Integer rowNum;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
