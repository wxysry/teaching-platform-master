package com.xiyou.main.entity.exam;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.baomidou.mybatisplus.annotation.TableId;

import java.time.LocalDateTime;
import java.io.Serializable;
import java.util.List;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 *
 * </p>
 *
 * @author tangcan
 * @since 2019-07-07
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value = "ProblemGroup对象", description = "")
public class ProblemGroup extends Model<ProblemGroup> {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "考题分组id")
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty(value = "父级分组的id，第一级为0")
    private Integer fatherId;

    @ApiModelProperty(value = "分组名称")
    private String groupName;

    @ApiModelProperty(value = "是否删除", hidden = true)
    @TableLogic
    private Integer deleted;

    private LocalDateTime updateTime;

    private LocalDateTime createTime;

    @ApiModelProperty(value = "二级分组列表", hidden = true)
    @TableField(exist = false)
    List<ProblemGroup> subgroups;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
