package com.xiyou.main.entity.exam;

import com.baomidou.mybatisplus.annotation.IdType;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.baomidou.mybatisplus.annotation.TableId;

import java.io.Serializable;
import java.time.LocalDateTime;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 * @author tangcan
 * @since 2019-06-24
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="ProblemOption对象", description="")
public class ProblemOption extends Model<ProblemOption> {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "考题选项的id")
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty(value = "考题id")
    private Integer problemId;

    @ApiModelProperty(value = "选项描述")
    private String option;

    @ApiModelProperty(value = "选项序号")
    private Integer number;

    @ApiModelProperty(value = "更新时间", hidden = true)
    private LocalDateTime updateTime;

    private LocalDateTime createTime;

    // 导入辅助字段,便于把插入后的problem_id定位到表格中每条数据
    @TableField(exist = false)
    private Integer rowNum;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
