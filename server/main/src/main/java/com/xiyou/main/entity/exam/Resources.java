package com.xiyou.main.entity.exam;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.baomidou.mybatisplus.annotation.TableId;

import java.time.LocalDateTime;
import java.io.Serializable;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 *
 * </p>
 *
 * @author tangcan
 * @since 2019-07-07
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value = "Resources对象", description = "")
public class Resources extends Model<Resources> {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "资源id")
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty(value = "文件原始名称（用户上传时的名称）")
    private String originalName;

    @ApiModelProperty(value = "服务器文件名")
    private String fileName;

    @ApiModelProperty(value = "上传者id")
    private Integer uploadUserId;

    @ApiModelProperty(value = "上传者")
    @TableField(exist = false)
    private String uploadUserName;

    @ApiModelProperty(value = "文件类型，0表示附件，1表示图片")
    private Integer type;

    @ApiModelProperty(value = "简介")
    private String intro;

    private LocalDateTime updateTime;

    private LocalDateTime createTime;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
