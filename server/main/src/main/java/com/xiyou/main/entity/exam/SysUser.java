package com.xiyou.main.entity.exam;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;

import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.baomidou.mybatisplus.annotation.TableId;


import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;

import com.xiyou.main.groups.Add;
import com.xiyou.main.groups.Teacher;
import com.xiyou.main.groups.Update;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.*;

/**
 * <p>
 *
 * </p>
 *
 * @author tangcan
 * @since 2019-06-24
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value = "SysUser对象", description = "")
public class SysUser extends Model<SysUser> {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "角色id")
    private Integer roleId;

    @ApiModelProperty(value = "用户id")
    @TableId(value = "id", type = IdType.AUTO)
    @NotNull(message = "id不能为空", groups = {Update.class})
    private Integer id;

    @ApiModelProperty(value = "账号")
    @NotBlank(message = "账号不能为空", groups = {Add.class})
    @Length(max = 50, message = "账号长度不能超过{max}", groups = {Add.class})
    private String account;

    @ApiModelProperty(value = "密码")
    @NotBlank(message = "密码不能为空", groups = {Add.class})
    @Length(min = 1, max = 50, message = "密码长度在{min} ~ {max}之间", groups = {Add.class})
    private String password;

    @ApiModelProperty(value = "姓名")
    @NotBlank(message = "姓名不能为空", groups = {Add.class, Update.class})
    @Length(max = 50, message = "姓名长度不能超过{max}", groups = {Add.class, Update.class})
    private String name;

    @ApiModelProperty(value = "邮箱")
//    @NotBlank(message = "邮箱不能为空", groups = {Add.class, Update.class})
//    @Length(max = 100, message = "邮箱长度不能超过{max}", groups = {Add.class, Update.class})
    private String email;

    @ApiModelProperty(value = "电话")
    @Length(max = 50, message = "电话长度不能超过{max}", groups = {Add.class, Update.class})
    private String phone;

    @ApiModelProperty(value = "所属学校名称")
    @NotBlank(message = "学校名称不能为空", groups = {Add.class})
    @Length(max = 50, message = "学校名称长度不能大于{max}", groups = {Add.class})
    private String schoolName;

    @ApiModelProperty(value = "可开设学生账号数")
    @Min(value = 0, message = "可开设学生账号数不能小于{value}", groups = {Teacher.class})
    @Max(value = 30000, message = "可开设学生账号数不能超过{value}", groups = {Teacher.class})
    private Integer memberNum;

    @ApiModelProperty(value = "可同时发布竞赛模拟数")
    @Min(value = 0, message = "可同时发布竞赛模拟数{value}", groups = {Teacher.class})
    @Max(value = 30000, message = "可同时发布竞赛模拟数不能超过{value}", groups = {Teacher.class})
    private Integer contestNum;

    @ApiModelProperty(value = "添加该账号的用户id", hidden = true)
    private Integer createUserId;

    @ApiModelProperty(value = "是否禁止登录", hidden = true)
    private Integer forbid;

    @ApiModelProperty(value = "是否删除", hidden = true)
    @TableLogic
    private Integer deleted;

    @ApiModelProperty(value = "更新时间", hidden = true)
    private LocalDateTime updateTime;

    @ApiModelProperty(value = "创建时间", hidden = true)
    private LocalDateTime createTime;


    /*
    新增辅助字段
     */

    @ApiModelProperty(value = "角色标识", hidden = true)
    @TableField(exist = false)
    private String roleCode;

    @ApiModelProperty(value = "角色名称", hidden = true)
    @TableField(exist = false)
    private String roleName;

    @ApiModelProperty(value = "添加该账号的管理人员姓名", hidden = true)
    @TableField(exist = false)
    private String createUserName;

    // 题库分组
    @ApiModelProperty(value = "可查看的题库分组")
    @TableField(exist = false)
    List<Integer> problemGroups;

    // 视频教程分组
    @ApiModelProperty(value = "可查看的视频教程的分组")
    @TableField(exist = false)
    List<Integer> videoGroups;

    // PPT教程分组
    @ApiModelProperty(value = "可查看的PPT教程的分组")
    @TableField(exist = false)
    List<Integer> pptGroups;

    // 竞赛模拟分组
    @ApiModelProperty(value = "可查看的竞赛模拟分组")
    @TableField(exist = false)
    List<Integer> contestGroups;

    // 新平台模拟分组
    @ApiModelProperty(value = "可查看的新平台模拟分组")
    @TableField(exist = false)
    List<Integer> newContestGroups;


    // 组间对抗
    @ApiModelProperty(value = "可查看的组间对抗分组")
    @TableField(exist = false)
    List<Integer> gbContestGroups;


    // 组间对抗(新)
    @ApiModelProperty(value = "可查看的组件对抗分组")
    @TableField(exist = false)
    List<Integer> ngbContestGroups;

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
