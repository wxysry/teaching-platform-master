package com.xiyou.main.entity.exam;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import java.io.Serializable;
import java.util.List;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 * @author tangcan
 * @since 2019-10-28
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="UserPpt对象", description="")
public class UserPpt extends Model<UserPpt> {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "学生可查看的ppt")
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty(value = "用户id")
    private Integer userId;

    @ApiModelProperty(value = "ppt教程的id")
    private Integer pptId;

    private LocalDateTime createTime;

    @ApiModelProperty(value = "获得查看权限的学生id列表")
    @TableField(exist = false)
    private List<Integer> studentIdList;

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
