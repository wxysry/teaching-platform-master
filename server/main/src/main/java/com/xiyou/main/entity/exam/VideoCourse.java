package com.xiyou.main.entity.exam;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.xiyou.main.groups.Add;
import com.xiyou.main.groups.Update;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;

/**
 * <p>
 *
 * </p>
 *
 * @author tangcan
 * @since 2019-06-28
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value = "VideoCourse对象", description = "")
public class VideoCourse extends Model<VideoCourse> {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "视频教程的id")
    @TableId(value = "id", type = IdType.AUTO)
    @NotNull(message = "id不能为空", groups = {Update.class})
    private Integer id;

    @ApiModelProperty(value = "分组id")
    @NotNull(message = "分组id不能为空", groups = {Add.class, Update.class})
    private Integer groupId;

    @ApiModelProperty(value = "分组名称")
    @TableField(exist = false)
    private String groupName;

    @ApiModelProperty(value = "上传者id")
    private Integer uploadUserId;

    @ApiModelProperty(value = "封面")
    private String cover;

    @ApiModelProperty(value = "视频名称")
    @NotBlank(message = "视频名称不能为空", groups = {Add.class, Update.class})
    @Length(max = 50, message = "视频名称长度不能大于{max}", groups = {Add.class, Update.class})
    private String videoName;

    @ApiModelProperty(value = "视频文件名")
    private String fileName;

    @ApiModelProperty(value = "简介")
    @Length(max = 500, message = "简介长度不能大于{max}", groups = {Add.class, Update.class})
    private String intro;

    @ApiModelProperty(hidden = true)
    private LocalDateTime updateTime;

    @ApiModelProperty(hidden = true)
    private LocalDateTime createTime;

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
