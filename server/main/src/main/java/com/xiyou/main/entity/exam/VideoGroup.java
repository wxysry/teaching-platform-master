package com.xiyou.main.entity.exam;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.xiyou.main.groups.Add;
import com.xiyou.main.groups.Update;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 *
 * </p>
 *
 * @author tangcan
 * @since 2019-06-28
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value = "VideoGroup对象", description = "")
public class VideoGroup extends Model<VideoGroup> {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "视频分组的id")
    @TableId(value = "id", type = IdType.AUTO)
    @NotNull(message = "id不能为空", groups = Update.class)
    private Integer id;

    @ApiModelProperty(value = "分组名称")
    @NotBlank(message = "分组名称不能为空", groups = {Add.class, Update.class})
    @Length(max = 50, message = "分组名称长度不能大于{max}", groups = {Add.class, Update.class})
    private String groupName;

    @ApiModelProperty(value = "是否删除", hidden = true)
    @TableLogic
    private Integer deleted;

    @ApiModelProperty(hidden = true)
    private LocalDateTime updateTime;
    @ApiModelProperty(hidden = true)
    private LocalDateTime createTime;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
