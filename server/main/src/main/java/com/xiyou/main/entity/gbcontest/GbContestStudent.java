package com.xiyou.main.entity.gbcontest;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.hibernate.validator.constraints.Length;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 *
 * </p>
 *
 * @author tangcan
 * @since 2019-08-25
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value = "ContestStudent对象", description = "")
public class GbContestStudent extends Model<GbContestStudent> {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "组间对抗的学生")
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty(value = "学生id")
    private Integer studentId;

    @ApiModelProperty(value = "竞赛id")
    private Integer contestId;

    @ApiModelProperty(value = "是否已开始经营")
    private Integer start;

    @ApiModelProperty(value = "经营进度，前端用于显示按钮")
    private String progress;


    @ApiModelProperty(value = "经营时间进度")
    private Integer date;

    @ApiModelProperty(value = "分数")
    private Double score;

    @ApiModelProperty(value = "报表填写错误年数，1,2,3表示第1、2、3年填写错误")
    private String errorReportYear;

    @ApiModelProperty(value = "完成时间，为空表示未完成")
    private LocalDateTime finishTime;

    @ApiModelProperty(value = "每个季度结束时间")
    private String everySeasonEndTime;

    @ApiModelProperty(value = "是否破产,0未破产，1破产")
    private Integer isBankruptcy;

    @ApiModelProperty(value = "最近一次还原时间")
    private Integer lastRestoreTime;

    @ApiModelProperty(value = "学生姓名")
    @TableField(exist = false)
    private String studentName;

    @ApiModelProperty(value = "账号")
    @TableField(exist = false)
    private String account;

    @ApiModelProperty(value = "所有者权益")
    @TableField(exist = false)
    private String bsTotalEquity;

    @TableField(exist = false)
    private String contestName;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
