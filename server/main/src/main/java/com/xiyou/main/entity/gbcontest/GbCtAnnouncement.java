package com.xiyou.main.entity.gbcontest;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.models.auth.In;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.time.LocalDateTime;

/**
 * @author 吕辉耀
 * @PROJECT_NAME: teaching-platform-master-new
 * @Description:
 * @creat 2023-07-15 18
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value = "公告信息", description = "")
public class GbCtAnnouncement extends Model<GbCtAnnouncement> {
    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "公告信息id")
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty(value = "竞赛id")
    private Integer contestId;

    @ApiModelProperty(value = "公告时间")
    private Integer anDate;

    @ApiModelProperty(value = "消息内容")
    private String info;

    @ApiModelProperty(value = "企业报表")
    private String financial;//企业报表

    @ApiModelProperty(value = "广告下发")
    private String advertising;//广告下发

    @ApiModelProperty(value = "一键下载")
    private String allData;//一键下载（各组数组）

    @ApiModelProperty(value = "创建时间")
    private LocalDateTime createTime;


}
