package com.xiyou.main.entity.gbcontest;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author tangcan
 * @since 2019-07-22
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="CtCashflow对象", description="")
public class GbCtCashflow extends Model<GbCtCashflow> {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "现金流量表id")
    @TableId(value = "cashflow_id", type = IdType.AUTO)
    private Integer cashflowId;

    @ApiModelProperty(value = "学生id")
    private Integer studentId;

    @ApiModelProperty(value = "考试id")
    private Integer contestId;

    @ApiModelProperty(value = "操作类型")
    private String cAction;

    @ApiModelProperty(value = "资金增加")
    private Integer cIn;

    @ApiModelProperty(value = "资金减少")
    private Integer cOut;

    @ApiModelProperty(value = "现金")
    private Integer cSurplus;

    @ApiModelProperty(value = "详情")
    private String cComment;

    @ApiModelProperty(value = "操作时间")
    private Integer cDate;


    @Override
    protected Serializable pkVal() {
        return this.cashflowId;
    }

}
