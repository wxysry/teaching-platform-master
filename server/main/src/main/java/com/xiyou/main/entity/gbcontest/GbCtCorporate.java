package com.xiyou.main.entity.gbcontest;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.xiyou.main.entity.newcontest.NewCtCharges;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * @author 吕辉耀
 * @PROJECT_NAME: teaching-platform-master-new
 * @Description:
 * @creat 2023-06-30 14
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value = "社会责任", description = "")
public class GbCtCorporate extends Model<NewCtCharges> {
    private static final long serialVersionUID = 1L;
    public static final Integer DEFAULT_SCORE = 100;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty(value = "分数")
    private Integer score;

    private Integer studentId;

    private Integer contestId;

    private String action;//惩罚原因
}
