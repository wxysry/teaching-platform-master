package com.xiyou.main.entity.gbcontest;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * <p>
 * 
 * </p>
 *应收款
 * @author tangcan
 * @since 2019-07-22
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="CtFee对象", description="")
public class GbCtFee extends Model<GbCtFee> {

    private static final long serialVersionUID = 1L;
    public static final Integer TO_RECEIVABLE = 1;
    public static final Integer TO_PAYMENT = 0;

    @TableId(value = "fee_id", type = IdType.AUTO)
    private Integer feeId;

    private Integer studentId;

    private Integer contestId;

    @ApiModelProperty(value = "借方")
    private String borrower;

    @ApiModelProperty(value = "贷方")
    private String lender;

    @ApiModelProperty(value = "金额")
    private Integer rFee;

    @ApiModelProperty(value = "备注")
    private String remarks;

    @ApiModelProperty(value = "账期")
    private Integer rRemainDate;

    @ApiModelProperty(value = "收款(付款)日期")
    private Integer paymentDate;

    @ApiModelProperty(value = "类型")
    private Integer type; //1为应收款 ，0 为应付款



    @ApiModelProperty(value = "贴现率%")
    @TableField(exist = false)
    private BigDecimal rate;






    @Override
    protected Serializable pkVal() {
        return this.feeId;
    }

}
