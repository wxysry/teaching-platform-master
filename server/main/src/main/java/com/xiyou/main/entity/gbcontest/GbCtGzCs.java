package com.xiyou.main.entity.gbcontest;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author wangxingyu
 * @since 2023-06-06
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="NewCtGzCs对象", description="")
public class GbCtGzCs extends Model<GbCtGzCs> {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "规则id")
    @TableId(value = "cs_id", type = IdType.AUTO)
    private Integer csId;

    @ApiModelProperty(value = "subject_number")
    private Integer subjectNumber;

    @ApiModelProperty(value = "违约金比例")
    private Integer punish;

    @ApiModelProperty(value = "出售产成品")
    private Integer inventoryDiscountProduct;

    @ApiModelProperty(value = "出售原材料")
    private Integer inventoryDiscountMaterial;

    @ApiModelProperty(value = "贷款倍数")
    private Integer loanCeiling;

    @ApiModelProperty(value = "初始现金")
    private Integer cash;

    @ApiModelProperty(value = "管理费")
    private Integer overhaul;

    @ApiModelProperty(value = "最小得单金额")
    private Integer min;

    @ApiModelProperty(value = "得单公差")
    private Integer tolerance;

    @ApiModelProperty(value = "信息费")
    private Integer information;

    @ApiModelProperty(value = "紧急采购原材料")
    private Integer emergenceMultipleMaterial;

    @ApiModelProperty(value = "紧急采购产成品")
    private Integer emergenceMultipleProduct;

    @ApiModelProperty(value = "所得税率")
    private Integer incomeTax;

    @ApiModelProperty(value = "选单时间")
    private Integer orderTimeout;

    @ApiModelProperty(value = "首单增加时间")
    private Integer orderFixTime;

    @ApiModelProperty(value = "拍卖时间")
    private Integer auctionTimeout;

    @ApiModelProperty(value = "拍卖张数")
    private Integer auctionThread;

    @ApiModelProperty(value = "生产线最大数量")
    private Integer maxLine;


    @Override
    protected Serializable pkVal() {
        return this.csId;
    }

}
