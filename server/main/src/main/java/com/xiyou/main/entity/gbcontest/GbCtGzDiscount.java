package com.xiyou.main.entity.gbcontest;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * <p>
 * 
 * </p>
 *
 * @author wangxingyu
 * @since 2023-06-06
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="NewCtGzDiscount对象", description="")
public class GbCtGzDiscount extends Model<GbCtGzDiscount> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty(value = "题库号")
    private Integer subjectNum;

    @ApiModelProperty(value = "贷款名称")
    private String discountName;

    @ApiModelProperty(value = "贷款编码")
    private String discountNum;

    @ApiModelProperty(value = "收款期(季)")
    private Integer discountTime;

    @ApiModelProperty(value = "贴息(%)")
    private BigDecimal discountInterest;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
