package com.xiyou.main.entity.gbcontest;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author wangxingyu
 * @since 2023-06-06
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="NewCtGzProducing对象", description="")
public class GbCtGzProducing extends Model<GbCtGzProducing> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty(value = "题库号")
    private Integer subjectNumber;

    @ApiModelProperty(value = "产品编号")
    private Integer cpId;


    @ApiModelProperty(value = "产品名称")
    private String cpName;

    @ApiModelProperty(value = "原材料规则id")
    private Integer cpMid;

    @ApiModelProperty(value = "所需数量")
    private Integer cpNum;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
