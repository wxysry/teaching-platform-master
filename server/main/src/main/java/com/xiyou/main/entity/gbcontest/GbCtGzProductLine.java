package com.xiyou.main.entity.gbcontest;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author wangxingyu
 * @since 2023-06-06
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="NewCtGzProductLine对象", description="")
public class GbCtGzProductLine extends Model<GbCtGzProductLine> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty(value = "题库号")
    private Integer subjectNum;

    @ApiModelProperty(value = "生产线编号")
    private Integer cplId;


    @ApiModelProperty(value = "生产线名称")
    private String cplName;

    @ApiModelProperty(value = "总投资费用")
    private Integer cplBuyFee;

    @ApiModelProperty(value = "投资周期")
    private Integer cplInstallDate;

    @ApiModelProperty(value = "生产时间")
    private Integer cplProduceDate;

    @ApiModelProperty(value = "转产周期")
    private Integer cplTransferDate;

    @ApiModelProperty(value = "转产费用")
    private Integer cplTransferFee;

    @ApiModelProperty(value = "维修费（W/每年）")
    private Integer cplMaintenanceFee;

    @ApiModelProperty(value = "折旧费")
    private Integer cplScarpFee;

    @ApiModelProperty(value = "折旧年限")
    private Integer cplDepDate;

    @ApiModelProperty(value = "残值")
    private Integer cplDepFee;

    @ApiModelProperty(value = "需要高级工人")
    private Integer cplSeniorWorker;

    @ApiModelProperty(value = "需要普通工人")
    private Integer cplOrdinaryWorker;

    @ApiModelProperty(value = "基础产量")
    private Integer cplProduction;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
