package com.xiyou.main.entity.gbcontest;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.xiyou.main.groups.Add;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.List;

/**
 * <p>
 *
 * </p>
 *
 * @author tangcan
 * @since 2019-07-22
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value = "NewCtLine对象", description = "")
public class GbCtLine extends Model<GbCtLine> {

    private static final long serialVersionUID = 1L;

    public final static String ON_BUILD= "在建";
    public  final static String ON_PRODUCE= "在产";
    public  final static String ON_SPACE= "空闲";
    public  final static String ON_TRANSFER= "转产";

    @TableId(value = "line_id", type = IdType.AUTO)
    private Integer lineId;

    private Integer studentId;
    @NotNull(message = "竞赛id不能为空", groups = {Add.class})
    private Integer contestId;

    @NotNull(message = "请选择生产线类型", groups = {Add.class})
    @ApiModelProperty(value = "生产线类型id")
    private Integer plCplid;

    @NotNull(message = "请选择生产产品", groups = {Add.class})
    @ApiModelProperty(value = "生产产品id")
    private Integer plCpid;

    @ApiModelProperty(value = "原值")
    private Integer plInvest;

    @ApiModelProperty(value = "累计折旧")
    private Integer plDepTotal;

    @ApiModelProperty(value = "开产时间")
    private Integer plProductAddDate;

    @ApiModelProperty(value = "结束生产时间")
    private Integer plProductingDate;

    @ApiModelProperty(value = "建成时间")
    private Integer plFinishDate;

    @ApiModelProperty(value = "开建剩余时间")
    private Integer plRemainDate;

    @NotNull(message = "开建时间不能为空", groups = {Add.class})
    @ApiModelProperty(value = "开建时间")
    private Integer plAddTime;

    @ApiModelProperty(value = "转产开始时间")
    private Integer plTransferAddDate;

    @ApiModelProperty(value = "转产结束时间")
    private Integer plTransferDate;

    @ApiModelProperty(value = "班次id")
    private Integer classesId;
    /**
     * gz_product_name P1-P4
     */
    @ApiModelProperty(value = "产品名称")
    @TableField(exist = false)
    private String cpName;

    /**
     * 生产线名称
     */
    @ApiModelProperty(value = "生产线名称")
    @TableField(exist = false)
    private String cplName;

    /**
     * 生产线投资费用
     */
    @ApiModelProperty(value = "生产线投资费用")
    @TableField(exist = false)
    private String cplDepFee;

    /**
     * 生产线状态：空闲、转产、生产、在建
     */
    @ApiModelProperty(value = "生产线状态")
    private String status;


    @ApiModelProperty(value = "实际产量")
    private Integer realProduction;
    /**
     * 操作时间
     */
    @ApiModelProperty(value = "操作时间-用于VO")
    @TableField(exist = false)
    private Integer actionTime;


    @ApiModelProperty(value = "实际产量")
    @TableField(exist = false)
    private Integer cplProduction;

    @ApiModelProperty(value = "工人集合-用于VO")
    @TableField(exist = false)
    private List<Integer> ordinaryWorkerList;

    @ApiModelProperty(value = "工人集合-用于VO")
    @TableField(exist = false)
    private List<Integer> seniorWorkerList;


    @ApiModelProperty(value = "需要高级工人")
    @TableField(exist = false)
    private Integer cplSeniorWorker;


    @ApiModelProperty(value = "需要普通工人")
    @TableField(exist = false)
    private Integer cplOrdinaryWorker;


    @Override
    protected Serializable pkVal() {
        return this.lineId;
    }

}
