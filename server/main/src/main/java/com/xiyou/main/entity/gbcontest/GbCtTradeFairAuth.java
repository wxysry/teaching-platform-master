package com.xiyou.main.entity.gbcontest;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *应收款
 * @author tangcan
 * @since 2019-07-22
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="订货会权限", description="")
public class GbCtTradeFairAuth extends Model<GbCtTradeFairAuth> {

    private static final long serialVersionUID = 1L;
    public static final Integer DIS_OPEN = 0;
    public static final Integer IS_OPEN = 1;
    public static final Integer IS_CHOOSE = 2;
    public static final Integer IS_OVER = 3;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    private Integer studentId;

    private Integer contestId;

    @ApiModelProperty(value = "订货会开启时间")
    private String tradeOpenTime;

    @ApiModelProperty(value = "订货会是否开启")
    private String isTradeOpen;//0为未开启 1为开启投放广发 2为广告结束开始选单 3为结束选单

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
