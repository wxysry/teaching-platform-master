package com.xiyou.main.entity.gbcontest;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * <p>
 *订单信息
 * </p>
 *
 * @author tangcan
 * @since 2019-07-22
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value = "NewCtXzOrder对象", description = "")
public class GbCtXzOrder extends Model<GbCtXzOrder> {

    private static final long serialVersionUID = 1L;
    public static final String ORDER_UNDELIVERED = "未到期";
    public static final String ORDER_DELIVERED = "已交货";
    public static final String ORDER_VIOLATION = "已违约";

    @TableId(value = "order_id", type = IdType.AUTO)
    private Integer orderId;

    @ApiModelProperty(value = "订单id")
    private String coId;

    private Integer studentId;

    private Integer contestId;

    @ApiModelProperty(value = "年份")
    private Integer date;

    @ApiModelProperty(value = "季度")
    private Integer quarterly;

    @ApiModelProperty(value = "市场")
    private Integer cmId;

    @ApiModelProperty(value = "产品")
    private Integer cpId;

    @ApiModelProperty(value = "数量")
    private Integer num;

    @ApiModelProperty(value = "总价")
    private Integer totalPrice;

    @ApiModelProperty(value = "交货期")
    private Integer deliveryDate;

    @ApiModelProperty(value = "账期")
    private Integer paymentDate;

    @ApiModelProperty(value = "ISO")
    private Integer isoId;

    @ApiModelProperty(value = "交单时间")
    private Integer commitDate;

    @ApiModelProperty(value = "订单编号")
    @TableField(exist = false)
    private String coNum;

    @ApiModelProperty(value = "市场名称")
    @TableField(exist = false)
    private String cmName;

    @ApiModelProperty(value = "产品名称")
    @TableField(exist = false)
    private String cpName;

    @ApiModelProperty(value = "ISO名字")
    @TableField(exist = false)
    private String ciName;

    @ApiModelProperty(value = "状态")
    @TableField(exist = false)
    private String status;

    @ApiModelProperty(value = "成本")
    private Integer totalCost;


    @Override
    protected Serializable pkVal() {
        return this.orderId;
    }

}
