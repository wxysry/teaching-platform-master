package com.xiyou.main.entity.newcontest;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.models.auth.In;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * <p>
 * 
 * </p>
 *
 * @author tangcan
 * @since 2019-07-22
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="CtBankLoan对象", description="")
public class NewCtBankLoan extends Model<NewCtBankLoan> {

    private static final long serialVersionUID = 1L;
    public static final Integer LONG_LOAN = 1;
    public static final Integer SHORT_LOAN = 2;

    @TableId(value = "bl_id", type = IdType.AUTO)
    private Integer blId;

    private Integer studentId;

    private Integer contestId;

    @ApiModelProperty(value = "贷款类型")
    private Integer blType;//1是长贷，2是短贷

    @ApiModelProperty(value = "贷款名称")
    private String blName;

    @ApiModelProperty(value = "贷款金额")
    private Integer blFee;

    @ApiModelProperty(value = "贷款时间")
    private Integer blAddTime;

    @ApiModelProperty(value = "贷款时长")
    private Integer blRemainTime;

    @ApiModelProperty(value = "还款时间")
    private Integer blRepaymentDate;

    @ApiModelProperty(value = "还款方式")
    private String repaymentType;

    @ApiModelProperty(value = "利息")
    private BigDecimal interest;

    @ApiModelProperty(value = "利率")
    private BigDecimal rate;
    @Override
    protected Serializable pkVal() {
        return this.blId;
    }

}
