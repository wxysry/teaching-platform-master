package com.xiyou.main.entity.newcontest;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.hibernate.validator.constraints.Range;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * <p>
 *
 * </p>
 *
 * @author tangcan
 * @since 2019-07-22
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value = "CtCharges对象", description = "")
public class NewCtCharges extends Model<NewCtCharges> {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "费用表id")
    @TableId(value = "charges_id", type = IdType.AUTO)
    private Integer chargesId;

    @ApiModelProperty(value = "学生id")
    private Integer studentId;

    @ApiModelProperty(value = "考试id")
    @NotNull
    private Integer contestId;

    @ApiModelProperty(value = "填写人：1代表系统自动生成，0代表学生录入")
    private Integer bsIsxt;

    @NotNull
    @ApiModelProperty(value = "年份")
    private Integer cDate;


    @ApiModelProperty(value = "管理费用")
    @Range(min = 0, message = "管理费用不能小于{min}")
    @NotNull(message = "管理费不能为空")
    private Integer cOverhaul;

    @ApiModelProperty(value = "广告费用")
    @Range(min = 0, message = "广告费用不能小于{min}")
    @NotNull(message = "广告费不能为空")
    private Integer cAd;

    @ApiModelProperty(value = "维护费")
    @Range(min = 0, message = "维护费不能小于{min}")
    @NotNull(message = "设备维护费不能为空")
    private Integer cMaintenance;

    @ApiModelProperty(value = "转产费")
    @Range(min = 0, message = "转产费不能小于{min}")
    @NotNull(message = "转产费不能为空")
    private Integer cTransfer;


    @ApiModelProperty(value = "市场开拓")
    @Range(min = 0, message = "市场开拓不能小于{min}")
    @NotNull(message = "市场准入不能为空")
    private Integer cDevelopMarket;

    @ApiModelProperty(value = "ISO开拓")
    @Range(min = 0, message = "ISO开拓不能小于{min}")
    @NotNull(message = "ISO开发不能为空")
    private Integer cDevelopIso;

    @ApiModelProperty(value = "产品研发")
    @Range(min = 0, message = "产品研发费不能小于{min}")
    @NotNull(message = "产品研发不能为空")
    private Integer cDevelopProduct;

    @ApiModelProperty(value = "信息费")
    @Range(min = 0, message = "信息费不能小于{min}")
    @NotNull(message = "信息费不能为空")
    private Integer cInformation;


    @ApiModelProperty(value = "人力费")
    @Range(min = 0, message = "人力费不能小于{min}")
    @NotNull(message = "人力费不能为空")
    private Integer cHr;


    @ApiModelProperty(value = "数字化研发费")
    @Range(min = 0, message = "数字化研发费不能小于{min}")
    @NotNull(message = "数字化研发费不能为空")
    private Integer cDigitalization;



    @ApiModelProperty(value = "合计")
    @NotNull(message = "合计不能为空")
    @Range(min = 0, message = "合计不能小于{min}")
    private Integer cTotal;

    @ApiModelProperty(value = "是否提交")
    private String isSubmit;


    public void setZero() {
        cOverhaul = 0;
        cAd = 0;
        cMaintenance = 0;
        cTransfer = 0;
        cDevelopMarket = 0;
        cDevelopIso = 0;
        cDevelopProduct = 0;
        cInformation = 0;
        cHr = 0;
        cDigitalization = 0;
        cTotal = 0;
    }

    public void add(NewCtCharges ctCharges) {
        cOverhaul += ctCharges.getCOverhaul();
        cAd += ctCharges.getCAd();
        cMaintenance += ctCharges.getCMaintenance();
        cTransfer += ctCharges.getCTransfer();
        cDevelopMarket += ctCharges.getCDevelopMarket();
        cDevelopIso += ctCharges.getCDevelopIso();
        cDevelopProduct += ctCharges.getCDevelopProduct();
        cInformation += ctCharges.getCInformation();
        cHr += ctCharges.getCHr();
        cDigitalization += ctCharges.getCDigitalization();
        cTotal += ctCharges.getCTotal();
    }

    @Override
    protected Serializable pkVal() {
        return this.chargesId;
    }

}
