package com.xiyou.main.entity.newcontest;

import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 * @author wangxingyu
 * @since 2023-06-28
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="NewCtFinancialTarget对象", description="")
public class NewCtFinancialTarget extends Model<NewCtFinancialTarget> {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "财务指标表id")
    @TableId(value = "ft_id", type = IdType.AUTO)
    private Integer ftId;

    @ApiModelProperty(value = "学生id")
    private Integer studentId;

    @ApiModelProperty(value = "考试id")
    private Integer contestId;

    @ApiModelProperty(value = "填写人：1代表系统自动生成，0代表学生录入")
    private Integer ftIsxt;

    @ApiModelProperty(value = "年份")
    private Integer ftYear;

    @ApiModelProperty(value = "流动比率")
    private BigDecimal ftCurrentRate;

    @ApiModelProperty(value = "速动比率")
    private BigDecimal ftQuickRate;

    @ApiModelProperty(value = "资产负债率")
    private BigDecimal ftDebtRate;

    @ApiModelProperty(value = "产权比率")
    private BigDecimal ftEquityRate;

    @ApiModelProperty(value = "营业净利润率")
    private BigDecimal ftNetProfitRate;

    @ApiModelProperty(value = "成本费用率")
    private BigDecimal ftCostExpenseRate;

    @ApiModelProperty(value = "资产报酬率")
    private BigDecimal ftReturnAssetsRate;

    @ApiModelProperty(value = "净资产收益率")
    private BigDecimal ftReturnEquityRate;

    @ApiModelProperty(value = "营业收入增长率")
    private BigDecimal ftRevenueGrowthRate;

    @ApiModelProperty(value = "资本保值增值率")
    private BigDecimal ftAppreciationRate;

    @ApiModelProperty(value = "总资产增长率")
    private BigDecimal ftTotalGrowthRate;

    @ApiModelProperty(value = "库存周转率")
    private BigDecimal ftInventoryRate;

    @ApiModelProperty(value = "库存周转天数")
    private BigDecimal ftInventoryDays;

    @ApiModelProperty(value = "应收账款周转率")
    private BigDecimal ftReceivableRate;

    @ApiModelProperty(value = "应收账款周转天数")
    private BigDecimal ftReceivableDays;

    @ApiModelProperty(value = "现金周转期")
    private BigDecimal ftCashPeriod;


    @ApiModelProperty(value = "是否提交")
    private String isSubmit;


    @Override
    protected Serializable pkVal() {
        return this.ftId;
    }

}
