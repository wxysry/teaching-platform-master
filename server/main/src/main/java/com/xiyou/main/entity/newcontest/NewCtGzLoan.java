package com.xiyou.main.entity.newcontest;

import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 * @author wangxingyu
 * @since 2023-06-06
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="NewCtGzLoan对象", description="")
public class NewCtGzLoan extends Model<NewCtGzLoan> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty(value = "题库号")
    private Integer subjectNum;

    @ApiModelProperty(value = "贷款名称")
    private String loanName;

    @ApiModelProperty(value = "贷款编码")
    private String loanNum;

    @ApiModelProperty(value = "额度上限(倍)")
    private BigDecimal loanMax;

    @ApiModelProperty(value = "贷款时间(季)")
    private Integer loanTime;

    @ApiModelProperty(value = "还款方式")
    private String loanRepayment;

    @ApiModelProperty(value = "利率(%)")
    private BigDecimal loanRate;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
