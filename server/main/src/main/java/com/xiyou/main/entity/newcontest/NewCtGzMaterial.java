package com.xiyou.main.entity.newcontest;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 * @author wangxingyu
 * @since 2023-06-06
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="NewCtGzMaterial对象", description="")
public class NewCtGzMaterial extends Model<NewCtGzMaterial> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;


    @ApiModelProperty(value = "题库号")
    private Integer subjectNumber;

    @ApiModelProperty(value = "原料规则表编号")
    private Integer cmId;

    @ApiModelProperty(value = "原料名称")
    private String cnName;

    @ApiModelProperty(value = "送货期")
    private Integer cmLeadDate;

    @ApiModelProperty(value = "价格")
    private Integer cmBuyFee;

    @ApiModelProperty(value = "付款周期（账期）")
    private Integer cmPayDate;

    @ApiModelProperty(value = "数量")
    private Integer num;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
