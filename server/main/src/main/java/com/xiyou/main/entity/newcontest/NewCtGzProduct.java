package com.xiyou.main.entity.newcontest;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 * @author wangxingyu
 * @since 2023-06-06
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="NewCtGzProduct对象", description="")
public class NewCtGzProduct extends Model<NewCtGzProduct> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty(value = "题库号")
    private Integer subjectNumber;

    @ApiModelProperty(value = "产品规则表编号")
    private Integer cpId;


    @ApiModelProperty(value = "名称")
    private String cpName;

    @ApiModelProperty(value = "开发费用")
    private Integer cpProcessingFee;

    @ApiModelProperty(value = "开发周期")
    private Integer cpDevelopDate;

    @ApiModelProperty(value = "加工费")
    private Integer cpDevelopFee;

    @ApiModelProperty(value = "直接成本")
    private Integer cpDirectCost;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
