package com.xiyou.main.entity.newcontest;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 * @author wangxingyu
 * @since 2023-06-06
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="NewCtGzProductDesign对象", description="")
public class NewCtGzProductDesign extends Model<NewCtGzProductDesign> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty(value = "题库号")
    private Integer subjectNum;

    @ApiModelProperty(value = "特征名称")
    private String featureName;

    @ApiModelProperty(value = "编码")
    private String designNum;

    @ApiModelProperty(value = "成本加成")
    private Integer costMarkup;

    @ApiModelProperty(value = "设计费用")
    private Integer designCost;

    @ApiModelProperty(value = "升级单位成本")
    private Integer unitCostUpgrade;

    @ApiModelProperty(value = "初始值")
    private Integer initialValue;

    @ApiModelProperty(value = "上限")
    private Integer upperLimit;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
