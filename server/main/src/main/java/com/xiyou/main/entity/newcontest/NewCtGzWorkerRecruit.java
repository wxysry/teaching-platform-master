package com.xiyou.main.entity.newcontest;

import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 * @author wangxingyu
 * @since 2023-06-06
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="NewCtGzWorkerRecruit对象", description="")
public class NewCtGzWorkerRecruit extends Model<NewCtGzWorkerRecruit> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty(value = "题库号")
    private Integer subjectNum;

    @ApiModelProperty(value = "名称")
    private String recruitName;

    @ApiModelProperty(value = "编码")
    private String recruitNum;

    @ApiModelProperty(value = "初始期望工资(元)")
    private Integer initSalExpect;

    @ApiModelProperty(value = "计件")
    private Integer piece;

    @ApiModelProperty(value = "每季度数量")
    private Integer qtyPerQtr;

    @ApiModelProperty(value = "倍数加成(%)")
    private Integer multBonus;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
