package com.xiyou.main.entity.newcontest;

import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 * @author wangxingyu
 * @since 2023-06-06
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="NewCtGzWorderTrain对象", description="")
public class NewCtGzWorkerTrain extends Model<NewCtGzWorkerTrain> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty(value = "题库号")
    private Integer subjectNum;

    @ApiModelProperty(value = "培训名称")
    private String trainingName;

    @ApiModelProperty(value = "消耗现金(元)")
    private Integer cashCost;

    @ApiModelProperty(value = "消耗时间(季)")
    private Integer timeCostQuarter;

    @ApiModelProperty(value = "原岗位")
    private String originalPosition;

    @ApiModelProperty(value = "培训后岗位")
    private String trainedPosition;

    @ApiModelProperty(value = "工资涨幅(%)")
    private BigDecimal salaryIncreasePercent;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
