package com.xiyou.main.entity.newcontest;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author tangcan
 * @since 2019-07-22
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="CtKcMaterial对象", description="")
public class NewCtKcMaterial extends Model<NewCtKcMaterial> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "material_id", type = IdType.AUTO)
    private Integer materialId;

    private Integer studentId;

    private Integer contestId;

    @ApiModelProperty(value = "原料编号")
    private Integer imCmId;

    @ApiModelProperty(value = "剩余数量")
    private Integer imNum;

    @ApiModelProperty(value = "原料名称")
    private String materialName;

    @ApiModelProperty(value = "入库日期")
    private Integer inInventoryDate;

    @ApiModelProperty(value = "成本")
    private Integer materialPrice;

    @Override
    protected Serializable pkVal() {
        return this.materialId;
    }

}
