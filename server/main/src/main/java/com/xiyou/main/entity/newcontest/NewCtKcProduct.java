package com.xiyou.main.entity.newcontest;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author tangcan
 * @since 2019-07-22
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="CtKcProduct对象", description="庫存")
public class NewCtKcProduct extends Model<NewCtKcProduct> {

    private static final long serialVersionUID = 1L;
    public static final Integer IS_IN = 1;
    public static final Integer NOT_IN = 0;

    @TableId(value = "kc_product_id", type = IdType.AUTO)
    private Integer kcProductId;

    private Integer studentId;

    private Integer contestId;

    @ApiModelProperty(value = "产品编号")
    private Integer ipCpId;

    @ApiModelProperty(value = "库存数量")
    private Integer ipNum;

    @ApiModelProperty(value = "生产线Id")
    private Integer lineId;

    @ApiModelProperty(value = "是否入库")
    private Integer isInventory;//  1代表入库    0或null代表未入库

    @ApiModelProperty(value = "入库日期")
    private Integer inventoryDate;

    @ApiModelProperty(value = "真实成本")
    private Integer realCost;

    @ApiModelProperty(value = "产品名称")
    @TableField(exist = false)
    private String cpName;
    @Override
    protected Serializable pkVal() {
        return this.kcProductId;
    }

}
