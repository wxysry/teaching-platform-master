package com.xiyou.main.entity.newcontest;

import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.metadata.BaseRowModel;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.math.BigDecimal;

/**
 * @author wangxy
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@ApiModel(value = "NewCtWorker对象", description = "")
public class NewCtWorker extends Model<NewCtWorker> {

    private static final long serialVersionUID = 1L;
    public static final Integer ON_BUSI = 1;
    public static final Integer ON_SPACE = 2;
    public static final Integer ON_WORK = 3;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty(value = "学生id")
    private Integer studentId;

    @ApiModelProperty(value = "竞赛ID")
    private Integer contestId;

    @ApiModelProperty(value = "产线ID")
    private Integer lineId;

    @ApiModelProperty(value = "姓名")
    private String workerName;

    @ApiModelProperty(value = "序号")
    private Integer workerNum;

    @ApiModelProperty(value = "等级")
    private String recruitName;

    @ApiModelProperty(value = "等级编码")
    private String recruitNum;

    @ApiModelProperty(value = "倍数加成(%)")
    private Integer multBonus;

    @ApiModelProperty(value = "工资(元)")
    private Integer initSal;

    @ApiModelProperty(value = "招聘时间")
    private Integer startWorkTime;

    @ApiModelProperty(value = "是否在职")
    private  Integer isWork;//招聘1 在职(空闲)2 上岗3

    @ApiModelProperty(value = "计件")
    private  Integer piece;

}