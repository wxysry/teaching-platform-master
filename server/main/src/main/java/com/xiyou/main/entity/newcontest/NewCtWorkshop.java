package com.xiyou.main.entity.newcontest;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * <p>
 *
 * </p>
 *
 * @author tangcan
 * @since 2019-07-22
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value = "CtWorkshop对象", description = "工廠")
public class NewCtWorkshop extends Model<NewCtWorkshop> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "workshop_id", type = IdType.AUTO)
    private Integer workshopId;

    private Integer studentId;

    private Integer contestId;

    @ApiModelProperty(value = "厂房规则表里的id")
    private Integer wCwid;

    @ApiModelProperty(value = "租售类型:0表示BUY,1表示RENT")
    private Integer wStatus;

    @ApiModelProperty(value = "剩余容量")
    private Integer wSurplusCapacity;

    @ApiModelProperty(value = "最后付租")
    private Integer wPayDate;

    @ApiModelProperty(value = "置办时间")
    private Integer getDate;

    @TableField(exist = false)
    private String cwName;

    @TableField(exist = false)
    private Integer cwBuyFee;

    @TableField(exist = false)
    private Integer cwRentFee;

    @TableField(exist = false)
    private Integer cwSellFee;

    @TableField(exist = false)
    private Integer cwCapacity;


    @Override
    protected Serializable pkVal() {
        return this.workshopId;
    }

}
