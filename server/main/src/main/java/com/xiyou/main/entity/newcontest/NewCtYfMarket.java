package com.xiyou.main.entity.newcontest;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * <p>
 *
 * </p>
 *
 * @author tangcan
 * @since 2019-07-22
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value = "CtYfMarket对象", description = "")
public class NewCtYfMarket extends Model<NewCtYfMarket> {

    private static final long serialVersionUID = 1L;

    public static final String SC_YF_FINISH = "已完成";
    public static final String SC_YF_ING = "开拓中";
    public static final String SC_YF_NONE = "未开拓";

    @TableId(value = "market_id", type = IdType.AUTO)
    private Integer marketId;

    private Integer studentId;

    private Integer contestId;

    @ApiModelProperty(value = "市场编号")
    private Integer dmCmId;

    @ApiModelProperty(value = "总研发时间")
    private Integer dmTotalDate;

    @ApiModelProperty(value = "已研发时间")
    private Integer dmNowDate;

    @ApiModelProperty(value = "剩余研发时间")
    private Integer dmRemainDate;

    @ApiModelProperty(value = "申请时间")
    private Integer dmStartDate;

    @ApiModelProperty(value = "研发完成时间")
    private Integer dmFinishDate;

    @ApiModelProperty(value = "状态")
    private String dmState;

    @ApiModelProperty(value = "研发费用")
    @TableField(exist = false)
    private Integer cmDevelopFee;

    @ApiModelProperty(value = "市场名称")
    @TableField(exist = false)
    private String cmName;

    @ApiModelProperty(value = "研发周期")
    @TableField(exist = false)
    private Integer cmDevelopDate;


    @Override
    protected Serializable pkVal() {
        return this.marketId;
    }

}
