package com.xiyou.main.entity.newcontest;

import com.alibaba.excel.annotation.ExcelProperty;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * @author 吕辉耀
 * @PROJECT_NAME: teaching-platform-master-new
 * @Description:
 * @creat 2023-07-01 11
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value = "数字化-竞赛", description = "数字化-竞赛")
public class NewCtYfNum extends Model<NewCtYfNum> {
    private static final long serialVersionUID = 1L;
    public static final String NUM_YF_FINISH = "已完成";
    public static final String NUM_YF_ING = "研发中";
    public static final String NUM_YF_NONE = "未研发";

    @ApiModelProperty(value = "id")
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty(value = "学生id")
    private Integer studentId;

    @ApiModelProperty(value = "考试id")
    private Integer contestId;

    @ApiModelProperty(value = "数字化id")
    private Integer numId;

    @ApiModelProperty(value = "开始时间")
    private Integer startDate;

    @ApiModelProperty(value = "结束时间")
    private Integer finishDate;

    @ApiModelProperty(value = "消耗金钱(元)")
    private Integer consumeMoney;

    @ApiModelProperty(value = "状态")
    private String state;


    @ApiModelProperty(value = "研发周期")
    @TableField(exist = false)
    private Integer timeCostQuarter;

}
