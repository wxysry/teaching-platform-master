package com.xiyou.main.entity.ngbcontest;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 * @author wangxingyu
 * @since 2024-04-12
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="NgbBuyDataConsult对象", description="")
@TableName("ngb_buy_data_consult")
public class NGbBuyDataConsult extends Model<NGbBuyDataConsult> {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "主键ID")
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty(value = "学生id")
    private Integer studentId;

    @ApiModelProperty(value = "考试id")
    private Integer contestId;

    @ApiModelProperty(value = "咨询时间")
    private Integer date;

    @ApiModelProperty(value = "咨询学生id")
    private Integer zxStudentId;

    @ApiModelProperty(value = "咨询学生组号")
    @TableField(exist = false)
    private String zxGroupNum;

    @ApiModelProperty(value = "备份时间")
    private Integer backUpDate;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
