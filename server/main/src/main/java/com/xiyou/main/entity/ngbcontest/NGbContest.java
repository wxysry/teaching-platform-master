package com.xiyou.main.entity.ngbcontest;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.Range;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;

/**
 * <p>
 *
 * </p>
 *
 * @author wangxingyu
 * @since 2023-06-05
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value = "GbContest对象", description = "")
@TableName("ngb_contest")
public class NGbContest extends Model<NGbContest> {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "竞赛id")
    @TableId(value = "contest_id", type = IdType.AUTO)
    private Integer contestId;

    @ApiModelProperty(value = "题库号")
    @NotNull(message = "题库号不能为空")
    private Integer subjectNumber;

    @ApiModelProperty(value = "竞赛标题")
    @NotBlank(message = "竞赛标题不能为空")
    @Length(max = 100, message = "竞赛标题长度不能超过{max}")
    private String title;

    @ApiModelProperty(value = "教师id")
    private Integer teacherId;


    @ApiModelProperty(value = "分组id")
    @TableField(exist = false)
    private Integer groupId;

    @ApiModelProperty(value = "分组名称")
    @TableField(exist = false)
    private String groupName;

    @ApiModelProperty(value = "题库名称")
    @TableField(exist = false)
    private String subjectName;


    @ApiModelProperty(value = "初始权益，即初始资金")
    @NotNull(message = "请填写初始权益")
    @Range(min = 0, max = 1000000000, message = "初始权益只能在{min}~{max}之间")
    private Integer equity;


    @ApiModelProperty(value = "考试类型 高职 0 本科 1")
    private Integer contestType;

    @ApiModelProperty(value = "是否限制原材料订购不得高于资产 否 0 是 1")
    private Integer limitMaterial;

    @ApiModelProperty(value = "是否限制最大获取订单不得超过订单30% 否 0 是 1")
    private Integer limitOrder;

    @ApiModelProperty(value = "市场的选单状态")
    @Length(max = 500, message = "市场的选单状态{max}")
    private String marketList;//

    @ApiModelProperty(value = "选单市场排序")
    @Length(max = 255, message = "选单市场排序{max}")
    private String marketSort;//本地->区域->国内->国际

    @ApiModelProperty(value = "选单产品排序")
    @Length(max = 255, message = "选单产品排序{max}")
    private String productSort;//P1->P2->P3->P4

    @ApiModelProperty(value = "0表示不统一时间进度，1表示统一时间进度")
    @NotNull(message = "请选择是否统一时间进度")
    private Integer isSyn;

    @ApiModelProperty(value = "是否传统模式")
    private Integer traditionalMode;

    @ApiModelProperty(value = "0表示向指定考生发布，1表示向全部考生发布")
    @NotNull(message = "请选择考生范围")
    private Integer publishAll;

    @ApiModelProperty(value = "学生id列表")
    @TableField(exist = false)
    private List<Integer> studentList;

    @ApiModelProperty(value = "学生人数", hidden = true)
    private Integer studentNum;

    @ApiModelProperty(value = "是否发布竞赛")
    private Integer isPublish;


    @ApiModelProperty(value = "创建时间")
    private LocalDateTime createTime;

    @ApiModelProperty(value = "0未暂停，1暂停")
    private Integer isSuspend;

    @ApiModelProperty(value = "教师时间")
    private Integer teacherDate;

    @ApiModelProperty(value = "最后一次订货会时间")
    private Integer lastFairOrderDate;


    //0未开启订货会   1竞单环节
    @ApiModelProperty(value = "订货会阶段")
    private Integer fairOrderState;


    @ApiModelProperty(value = "订货会开始时间爱你")
    private LocalDateTime orderStartTime;


    @ApiModelProperty(value = "回合倒计时，用于设置各个回合的时间")
    private String countDown;


    @ApiModelProperty(value = "当前回合剩余时间,单位秒")
    private Integer remainTime;



    @ApiModelProperty("试卷状态:0未开始，1竞赛中，2已结束")
    @TableField(exist = false)
    private Integer status;


    @ApiModelProperty(value = "是否已开始经营")
    @TableField(exist = false)
    private Integer start;

    @ApiModelProperty(value = "分数")
    @TableField(exist = false)
    private Integer score;

    @ApiModelProperty(value = "经营进度，前端用于显示按钮")
    @TableField(exist = false)
    private String progress;

    @ApiModelProperty(value = "完成时间，为空表示未完成")
    @TableField(exist = false)
    private LocalDateTime finishTime;

    @ApiModelProperty(value = "经营时间进度")
    @TableField(exist = false)
    private Integer date;

    @Override
    protected Serializable pkVal() {
        return this.contestId;
    }

}
