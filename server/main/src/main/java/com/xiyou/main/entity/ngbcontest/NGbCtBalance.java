package com.xiyou.main.entity.ngbcontest;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * <p>
 *
 * </p>
 *
 * @author wangxingyu
 * @since 2023-06-11
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value = "CtBalance对象", description = "")
@TableName("ngb_ct_balance")
public class NGbCtBalance extends Model<NGbCtBalance> {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "资产负债表id")
    @TableId(value = "balance_id", type = IdType.AUTO)
    private Integer balanceId;

    @ApiModelProperty(value = "学生id")
    private Integer studentId;

    @ApiModelProperty(value = "学生账号")
    @TableField(exist = false)
    private String account;

    @ApiModelProperty(value = "考试id")
    private Integer contestId;

    @ApiModelProperty(value = "填写人：1代表系统自动生成，0代表学生录入")
    private Integer bsIsxt;

    @ApiModelProperty(value = "年份")
    private Integer bsYear;

    @ApiModelProperty(value = "现金")
    @NotNull
    private Integer bsCash;

    @ApiModelProperty(value = "应收款")
    @NotNull
    private Integer bsReceivable;

    @ApiModelProperty(value = "在制品")
    @NotNull
    private Integer bsProductInProcess;

    @ApiModelProperty(value = "产成品")
    @NotNull
    private Integer bsProduct;

    @ApiModelProperty(value = "原材料")
    @NotNull
    private Integer bsMaterial;

    @ApiModelProperty(value = "流动资产合计")
    @NotNull
    private Integer bsTotalCurrentAsset;


    @ApiModelProperty(value = "机器和设备")
    @NotNull
    private Integer bsEquipment;

    @ApiModelProperty(value = "在建")
    @NotNull
    private Integer bsProjectOnConstruction;

    @ApiModelProperty(value = "固定资产合计")
    @NotNull
    private Integer bsTotalFixedAsset;

    @ApiModelProperty(value = "资产合计")
    @NotNull
    private Integer bsTotalAsset;

    @ApiModelProperty(value = "长期贷款")
    @NotNull
    private Integer bsLongLoan;

    @ApiModelProperty(value = "短期贷款")
    @NotNull
    private Integer bsShortLoan;

    @ApiModelProperty(value = "其他应付款")
    @NotNull
    private Integer bsOtherPay;


    @ApiModelProperty(value = "应交税费")
    @NotNull
    private Integer bsTax;

    @ApiModelProperty(value = "负债合计")
    @NotNull
    private Integer bsTotalLiability;

    @ApiModelProperty(value = "股东资本")
    @NotNull
    private Integer bsEquity = 0;

    @ApiModelProperty(value = "利润留存")
    @NotNull
    private Integer bsRetainedEarning;

    @ApiModelProperty(value = "年度净利")
    @NotNull
    private Integer bsAnnualNetProfit;

    @ApiModelProperty(value = "所有者权益")
    @NotNull
    private Integer bsTotalEquity;

    @ApiModelProperty(value = "负债所有者权益合计")
    @NotNull
    private Integer bsTotal;

    @ApiModelProperty(value = "是否提交")
    private String isSubmit;

    @ApiModelProperty(value = "第一季结束时间")
    @TableField(exist = false)
    private String endTime1;

    @ApiModelProperty(value = "第二季结束时间")
    @TableField(exist = false)
    private String endTime2;

    @ApiModelProperty(value = "第二季结束时间")
    @TableField(exist = false)
    private String endTime3;

    @ApiModelProperty(value = "本年结束时间")
    @TableField(exist = false)
    private String endTime;


    @Override
    protected Serializable pkVal() {
        return this.balanceId;
    }

    public NGbCtBalance setZero() {
        bsCash = 0;
        bsReceivable = 0;
        bsProductInProcess = 0;
        bsProduct = 0;
        bsMaterial = 0;
        bsTotalCurrentAsset = 0;
        bsEquipment = 0;
        bsProjectOnConstruction = 0;
        bsTotalFixedAsset = 0;
        bsTotalAsset = 0;
        bsLongLoan = 0;
        bsShortLoan = 0;
        bsOtherPay = 0;
        bsTax = 0;
        bsTotalLiability = 0;
        bsEquity = 0;
        bsRetainedEarning = 0;
        bsAnnualNetProfit = 0;
        bsTotalEquity = 0;
        bsTotal = 0;
        return this;
    }

}
