package com.xiyou.main.entity.ngbcontest;

import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 * @author wangxingyu
 * @since 2024-02-18
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="NgbCtBudget对象", description="")
@TableName("ngb_ct_budget")
public class NGbCtBudget extends Model<NGbCtBudget> {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "预算id")
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty(value = "学生id")
    private Integer studentId;

    @ApiModelProperty(value = "考试id")
    private Integer contestId;

    @ApiModelProperty(value = "年份")
    private Integer year;

    @ApiModelProperty(value = "季度")
    private Integer quarterly;



    @ApiModelProperty(value = "销售预算")
    private BigDecimal initSaleBudget;

    @ApiModelProperty(value = "生产预算")
    private BigDecimal initProductBudget;

    @ApiModelProperty(value = "人力预算")
    private BigDecimal initHrBudget;



    @ApiModelProperty(value = "销售预算")
    private BigDecimal saleBudget;

    @ApiModelProperty(value = "生产预算")
    private BigDecimal productBudget;

    @ApiModelProperty(value = "人力预算")
    private BigDecimal hrBudget;

    @ApiModelProperty(value = "备份时间")
    private Integer backUpDate;


    public static NGbCtBudget getDefaultBudget(Integer contestId,Integer studentId,Integer year,Integer quarterly){
        NGbCtBudget nGbCtBudget  = new NGbCtBudget();
        nGbCtBudget.setContestId(contestId);
        nGbCtBudget.setStudentId(studentId);
        nGbCtBudget.setYear(year);
        nGbCtBudget.setQuarterly(quarterly);
        nGbCtBudget.setInitHrBudget(new BigDecimal(0));
        nGbCtBudget.setInitSaleBudget(new BigDecimal(0));
        nGbCtBudget.setInitProductBudget(new BigDecimal(0));

        nGbCtBudget.setHrBudget(new BigDecimal(0));
        nGbCtBudget.setSaleBudget(new BigDecimal(0));
        nGbCtBudget.setProductBudget(new BigDecimal(0));

        return nGbCtBudget;
    }


    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
