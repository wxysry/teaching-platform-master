package com.xiyou.main.entity.ngbcontest;

import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 * @author wangxingyu
 * @since 2024-02-18
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="NgbCtBudgetApply对象", description="")
@TableName("ngb_ct_budget_apply")
public class NGbCtBudgetApply extends Model<NGbCtBudgetApply> {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "申请id")
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty(value = "学生id")
    private Integer studentId;

    @ApiModelProperty(value = "考试id")
    private Integer contestId;

    @ApiModelProperty(value = "年份")
    private Integer year;

    @ApiModelProperty(value = "季度")
    private Integer quarterly;

    @ApiModelProperty(value = "预审申请金额")
    private BigDecimal amount;

    @ApiModelProperty(value = "预算类型,1销售 2生产 3人力")
    private String type;

    @ApiModelProperty(value = "审批结果 1未审批 2审批通过 3审批驳回")
    private String approveResult;

    @ApiModelProperty(value = "申请内容")
    private String comment;

    @ApiModelProperty(value = "备份时间")
    private Integer backUpDate;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
