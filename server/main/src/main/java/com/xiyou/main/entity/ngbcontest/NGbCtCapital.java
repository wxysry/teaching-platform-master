package com.xiyou.main.entity.ngbcontest;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * @author wangxy
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@ApiModel(value = "GbCtCapital对象", description = "注资")
@TableName("ngb_ct_capital")
public class NGbCtCapital extends Model<NGbCtCapital> {

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty(value = "学生id")
    private Integer studentId;

    @ApiModelProperty(value = "竞赛ID")
    private Integer contestId;

    @ApiModelProperty(value = "当前时间")
    private Integer caDate;

    @ApiModelProperty(value = "注资金额")
    private Integer addCapital;

    @ApiModelProperty(value = "材料信息")
    private String materialInfo;

}