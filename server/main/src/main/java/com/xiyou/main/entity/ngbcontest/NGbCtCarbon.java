package com.xiyou.main.entity.ngbcontest;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import java.math.BigDecimal;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 * @author wangxingyu
 * @since 2024-04-19
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="NgbCtCarbon对象", description="")
@TableName("ngb_ct_carbon")
public class NGbCtCarbon extends Model<NGbCtCarbon> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    private Integer studentId;

    private Integer contestId;

    @ApiModelProperty(value = "年份")
    private Integer year;

    @ApiModelProperty(value = "碳排放分配")
    private Integer assignedNum;

    @ApiModelProperty(value = "碳排放")
    private Integer emissionNum;

    @ApiModelProperty(value = "碳中和量")
    private Integer neutralizaNum;


    @ApiModelProperty(value = "碳中和率")
    @TableField(exist = false)
    private BigDecimal neutralizaRate;


    @ApiModelProperty(value = "上年碳中和率")
    @TableField(exist = false)
    private BigDecimal lastYearNeutralizaRate;




    @ApiModelProperty(value = "备份时间")
    private Integer backUpDate;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
