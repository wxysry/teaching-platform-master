package com.xiyou.main.entity.ngbcontest;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.xiyou.main.enums.CashFlowActionEnum;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * 
 * </p>
 *
 * @author tangcan
 * @since 2019-07-22
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="CtCashflow对象", description="")
@TableName("ngb_ct_cashflow")
public class NGbCtCashflow extends Model<NGbCtCashflow> {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "现金流量表id")
    @TableId(value = "cashflow_id", type = IdType.AUTO)
    private Integer cashflowId;

    @ApiModelProperty(value = "学生id")
    private Integer studentId;

    @ApiModelProperty(value = "考试id")
    private Integer contestId;

    @ApiModelProperty(value = "操作类型")
    private String cAction;

    @ApiModelProperty(value = "资金增加")
    private Integer cIn;

    @ApiModelProperty(value = "资金减少")
    private Integer cOut;

    @ApiModelProperty(value = "现金")
    private Integer cSurplus;

    @ApiModelProperty(value = "详情")
    private String cComment;

    @ApiModelProperty(value = "操作时间")
    private Integer cDate;


    /**
     * 根据部门类型获取对应的款项
     * @param type
     * @return
     */
    public static List<String> getActionListByDeptType(String type){
        List<String> actionList = new ArrayList<>();
        //市场营销部预算
        if("1".equals(type)){
            //市场营销部费用
            actionList.add(CashFlowActionEnum.PAY_AD.getAction()); //广告费
            actionList.add(CashFlowActionEnum.DEVELOP_MARKET.getAction()); //市场开拓
            actionList.add(CashFlowActionEnum.DEVELOP_ISO.getAction()); //ISO开拓
            actionList.add(CashFlowActionEnum.URGENT_PRODUCT_COST.getAction()); //紧急采购产成品
            actionList.add(CashFlowActionEnum.DEVELOP_PRODUCT.getAction()); //产品研发
            actionList.add(CashFlowActionEnum.YXSZH.getAction()); //生产数字化研发

//            actionList.add(CashFlowActionEnum.INFO_OTHER.getAction()); //数据咨询-信息费
//            actionList.add(CashFlowActionEnum.ORDERWYJ.getAction()); //违约金
        }
        //生产设计部预算
        else if("2".equals(type)){
            //生产设计部费用
            actionList.add(CashFlowActionEnum.BUY_PRODUCT_LINE.getAction()); //新建生产线
            actionList.add(CashFlowActionEnum.PRODUCT_LINE_TRANSFER.getAction()); //生产线转产费
            actionList.add(CashFlowActionEnum.PAY_MATERIAL.getAction()); //原材料付款
            actionList.add(CashFlowActionEnum.PRODUCT_PRODUCING.getAction()); //下一批生产 加工费
            actionList.add(CashFlowActionEnum.JJGZ.getAction()); //计件工资
            actionList.add(CashFlowActionEnum.URGENT_MATERIAL_COST.getAction()); //紧急采购原材料
            actionList.add(CashFlowActionEnum.CPSJ.getAction()); //产品设计费用
            actionList.add(CashFlowActionEnum.TZYF.getAction()); //特征研发费用
            actionList.add(CashFlowActionEnum.PAY_MAINTENANCE.getAction()); //产线维修费
            actionList.add(CashFlowActionEnum.SCSZH.getAction()); //人力数字化研发
        }
        //人力资源部预算
        else if("3".equals(type)){
            //人力资源部费用
            actionList.add(CashFlowActionEnum.GRPX.getAction()); //工人培训
            actionList.add(CashFlowActionEnum.GRPCK.getAction()); //工人赔偿款
            actionList.add(CashFlowActionEnum.GRGZ.getAction()); //工人工资
            actionList.add(CashFlowActionEnum.RLF.getAction()); //人力费
            actionList.add(CashFlowActionEnum.GRJL.getAction()); //工人激励
            actionList.add(CashFlowActionEnum.RLSZH.getAction()); //营销数字化研发
        }
        return actionList;
    }

    @Override
    protected Serializable pkVal() {
        return this.cashflowId;
    }

}
