package com.xiyou.main.entity.ngbcontest;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * <p>
 *
 * </p>
 *
 * @author tangcan
 * @since 2019-07-22
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value = "CtDdMaterial对象", description = "")
@TableName("ngb_ct_dd_material")
public class NGbCtDdMaterial extends Model<NGbCtDdMaterial> {

    private static final long serialVersionUID = 1L;
    public static final Integer IS_INVENTORY = 1;
    public static final Integer NO_INVENTORY = 0;
    public static final Integer IS_PAY = 1;
    public static final Integer NO_PAY = 0;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty(value = "原料规则表id")
    private Integer omCmid;

    @ApiModelProperty(value = "原料名称")
    private String cmName;

    private Integer studentId;

    private Integer contestId;

    @ApiModelProperty(value = "采购时间")
    private Integer purchaseDate;

    @ApiModelProperty(value = "收货期")
    private Integer remainDate;

    @ApiModelProperty(value = "是否入库")
    private Integer isInventory;//0代表未入库 1代表入库

    @ApiModelProperty(value = "付款周期（账期）")
    private Integer payDate;

    @ApiModelProperty(value = "是否付款")
    private Integer isPay;//0代表未付款 1代表付款

    @ApiModelProperty(value = "价格")
    private Integer price;

    @ApiModelProperty(value = "数量")
    private Integer num;


    @Override
    protected Serializable pkVal() {
        return this.omCmid;
    }

}
