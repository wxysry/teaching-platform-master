package com.xiyou.main.entity.ngbcontest;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 * @author wangxingyu
 * @since 2024-03-26
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="NgbCtDesignFeature对象", description="")
@TableName("ngb_ct_design_feature")
public class NGbCtDesignFeature extends Model<NGbCtDesignFeature> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    private Integer studentId;

    private Integer contestId;

    @ApiModelProperty(value = "产品规则表编号")
    private Integer cpId;

    @ApiModelProperty(value = "产品名称")
    private String cpName;

    @ApiModelProperty(value = "产品特征编号")
    private String featureNum;

    @ApiModelProperty(value = "产品特征名称")
    private String featureName;

    @ApiModelProperty(value = "备份时间")
    private Integer backUpDate;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
