package com.xiyou.main.entity.ngbcontest;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 * @author wangxingyu
 * @since 2024-03-26
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="NgbCtDesignProgress对象", description="")
@TableName("ngb_ct_design_progress")
public class NGbCtDesignProgress extends Model<NGbCtDesignProgress> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    private Integer studentId;

    private Integer contestId;

    @ApiModelProperty(value = "特征名称")
    private String featureName;

    @ApiModelProperty(value = "编码")
    private String designNum;

    @ApiModelProperty(value = "成本加成")
    private Integer costMarkup;

    @ApiModelProperty(value = "设计费用")
    private Integer designCost;

    @ApiModelProperty(value = "升级单位成本")
    private Integer unitCostUpgrade;

    @ApiModelProperty(value = "初始值")
    private Integer initialValue;

    @ApiModelProperty(value = "当前只")
    private Integer nowValue;

    @ApiModelProperty(value = "上限")
    private Integer upperLimit;

    @ApiModelProperty(value = "备份时间")
    private Integer backUpDate;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
