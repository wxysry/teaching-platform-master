package com.xiyou.main.entity.ngbcontest;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * <p>
 * 
 * </p>
 *
 * @author wangxingyu
 * @since 2023-06-06
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="NewCtGzAd对象", description="")
@TableName("ngb_ct_gz_ad")
public class NGbCtGzAd extends Model<NGbCtGzAd> {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "初始广告")
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty(value = "题号")
    private Integer subjectNum;

    @ApiModelProperty(value = "年份")
    private Integer year;

    @ApiModelProperty(value = "季度")
    private Integer quarterly;

    @ApiModelProperty(value = "组号")
    private String groupNum;

    @ApiModelProperty(value = "本地P1")
    private BigDecimal localP1;

    @ApiModelProperty(value = "区域P1")
    private BigDecimal regionalP1;

    @ApiModelProperty(value = "国内P1")
    private BigDecimal nationalP1;

    @ApiModelProperty(value = "亚洲P1")
    private BigDecimal asianP1;

    @ApiModelProperty(value = "国际P1")
    private BigDecimal internationalP1;

    @ApiModelProperty(value = "本地P2")
    private BigDecimal localP2;

    @ApiModelProperty(value = "区域P2")
    private BigDecimal regionalP2;

    @ApiModelProperty(value = "国内P2")
    private BigDecimal nationalP2;

    @ApiModelProperty(value = "亚洲P2")
    private BigDecimal asianP2;

    @ApiModelProperty(value = "国际P2")
    private BigDecimal internationalP2;

    @ApiModelProperty(value = "本地P3")
    private BigDecimal localP3;

    @ApiModelProperty(value = "区域P3")
    private BigDecimal regionalP3;

    @ApiModelProperty(value = "国内P3")
    private BigDecimal nationalP3;

    @ApiModelProperty(value = "亚洲P3")
    private BigDecimal asianP3;

    @ApiModelProperty(value = "国际P3")
    private BigDecimal internationalP3;

    @ApiModelProperty(value = "本地P4")
    private BigDecimal localP4;

    @ApiModelProperty(value = "区域P4")
    private BigDecimal regionalP4;

    @ApiModelProperty(value = "国内P4")
    private BigDecimal nationalP4;

    @ApiModelProperty(value = "亚洲P4")
    private BigDecimal asianP4;

    @ApiModelProperty(value = "国际P4")
    private BigDecimal internationalP4;

    @ApiModelProperty(value = "本地P5")
    private BigDecimal localP5;

    @ApiModelProperty(value = "区域P5")
    private BigDecimal regionalP5;

    @ApiModelProperty(value = "国内P5")
    private BigDecimal nationalP5;

    @ApiModelProperty(value = "亚洲P5")
    private BigDecimal asianP5;

    @ApiModelProperty(value = "国际P5")
    private BigDecimal internationalP5;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
