package com.xiyou.main.entity.ngbcontest;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * <p>
 * 
 * </p>
 *
 * @author wangxingyu
 * @since 2023-06-06
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="NewCtGzClasses对象", description="")
@TableName("ngb_ct_gz_classes")
public class NGbCtGzClasses extends Model<NGbCtGzClasses> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty(value = "题库号")
    private Integer subjectNum;

    @ApiModelProperty(value = "班次名称")
    private String classesName;

    @ApiModelProperty(value = "班次编码")
    private String classesNum;

    @ApiModelProperty(value = "产量加成(倍)")
    private Double outputMulti;

    @ApiModelProperty(value = "效率损失(%)")
    private BigDecimal efficiencyLoss;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
