package com.xiyou.main.entity.ngbcontest;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * <p>
 * 
 * </p>
 *
 * @author wangxingyu
 * @since 2023-06-06
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="NewCtGzLoan对象", description="")
@TableName("ngb_ct_gz_loan")
public class NGbCtGzLoan extends Model<NGbCtGzLoan> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty(value = "题库号")
    private Integer subjectNum;

    @ApiModelProperty(value = "贷款名称")
    private String loanName;

    @ApiModelProperty(value = "贷款编码")
    private String loanNum;

    @ApiModelProperty(value = "额度上限(倍)")
    private BigDecimal loanMax;

    @ApiModelProperty(value = "贷款时间(季)")
    private Integer loanTime;

    @ApiModelProperty(value = "还款方式")
    private String loanRepayment;

    @ApiModelProperty(value = "利率(%)")
    private BigDecimal loanRate;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
