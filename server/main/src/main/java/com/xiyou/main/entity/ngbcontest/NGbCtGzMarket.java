package com.xiyou.main.entity.ngbcontest;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author wangxingyu
 * @since 2023-06-06
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="NewCtGzMarket对象", description="")
@TableName("ngb_ct_gz_market")
public class NGbCtGzMarket extends Model<NGbCtGzMarket> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty(value = "题库号")
    private Integer subjectNumber;

    @ApiModelProperty(value = "市场规则表编号")
    private Integer cmId;

    @ApiModelProperty(value = "市场名称")
    private String cmName;

    @ApiModelProperty(value = "研发费用")
    private Integer cmDevelopFee;

    @ApiModelProperty(value = "研发周期")
    private Integer cmDevelopDate;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
