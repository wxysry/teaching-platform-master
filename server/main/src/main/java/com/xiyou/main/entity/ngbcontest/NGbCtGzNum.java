package com.xiyou.main.entity.ngbcontest;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author wangxingyu
 * @since 2023-06-06
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="NewCtGzNum对象", description="")
@TableName("ngb_ct_gz_num")
public class NGbCtGzNum extends Model<NGbCtGzNum> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty(value = "题库号")
    private Integer subjectNum;

    @ApiModelProperty(value = "数字化名称")
    private String postName;

    @ApiModelProperty(value = "数字化编码")
    private String postNum;

    @ApiModelProperty(value = "消耗金钱(元)")
    private Integer consumeMoney;

    @ApiModelProperty(value = "消耗时间(季)")
    private Integer timeCostQuarter;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
