package com.xiyou.main.entity.ngbcontest;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author wangxingyu
 * @since 2023-06-06
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="NewCtGzOrder对象", description="")
@TableName("ngb_ct_gz_order")
public class NGbCtGzOrder extends Model<NGbCtGzOrder> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty(value = "题库号")
    private Integer subjectNumber;


    @ApiModelProperty(value = "市场订单 订单编号")
    private String coId;


    @ApiModelProperty(value = "年份")
    private Integer date;

    @ApiModelProperty(value = "季度")
    private Integer quarterly;

    @ApiModelProperty(value = "市场")
    private Integer cmId;

    @ApiModelProperty(value = "市场名称")
    @TableField(exist = false)
    private String cmName;

    @ApiModelProperty(value = "产品")
    private String cpId;

    @ApiModelProperty(value = "产品名称")
    @TableField(exist = false)
    private String cpName;

    @ApiModelProperty(value = "特性编码")
    private String designNum;

    @ApiModelProperty(value = "特性编码")
    @TableField(exist = false)
    private String featureName;

    @ApiModelProperty(value = "数量")
    private Integer num;

    @ApiModelProperty(value = "参考价")
    private Integer price;

    @ApiModelProperty(value = "交货期")
    private Integer deliveryDate;

    @ApiModelProperty(value = "账期")
    private Integer paymentDate;

    @ApiModelProperty(value = "ISO")
    private Integer ciId;

    @ApiModelProperty(value = "ISO名字")
    @TableField(exist = false)
    private String ciName;

    @ApiModelProperty(value = "申报数量")
    @TableField(exist = false)
    private Integer applyNum;

    @ApiModelProperty(value = "报价")
    @TableField(exist = false)
    private Integer applyPrice;

    @ApiModelProperty(value = "是否可选")
    @TableField(exist = false)
    private Boolean disabled ;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
