package com.xiyou.main.entity.ngbcontest;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author wangxingyu
 * @since 2023-06-06
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="NewCtGzProduct对象", description="")
@TableName("ngb_ct_gz_product")
public class NGbCtGzProduct extends Model<NGbCtGzProduct> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty(value = "题库号")
    private Integer subjectNumber;

    @ApiModelProperty(value = "产品规则表编号")
    private String cpId;


    @ApiModelProperty(value = "名称")
    private String cpName;

    @ApiModelProperty(value = "特征编号")
    @TableField(exist = false)
    private String featureNum;

    @ApiModelProperty(value = "特征名称")
    @TableField(exist = false)
    private String featureName;


    @ApiModelProperty(value = "开发费用")
    private Integer cpProcessingFee;

    @ApiModelProperty(value = "开发周期")
    private Integer cpDevelopDate;

    @ApiModelProperty(value = "加工费")
    private Integer cpDevelopFee;

    @ApiModelProperty(value = "直接成本")
    private Integer cpDirectCost;

    @ApiModelProperty(value = "单次生产碳排放")
    private Integer cpCarbon;


    @ApiModelProperty(value = "R1所需数量")
    @TableField(exist = false)
    private Integer r1;

    @ApiModelProperty(value = "R2所需数量")
    @TableField(exist = false)
    private Integer r2;

    @ApiModelProperty(value = "R3所需数量")
    @TableField(exist = false)
    private Integer r3;

    @ApiModelProperty(value = "R4所需数量")
    @TableField(exist = false)
    private Integer r4;

    @ApiModelProperty(value = "R4所需数量")
    @TableField(exist = false)
    private Integer r5;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
