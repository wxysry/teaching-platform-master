package com.xiyou.main.entity.ngbcontest;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 * @author wangxingyu
 * @since 2024-05-25
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="NgbCtGzRetailMarket对象", description="")
@TableName("ngb_ct_gz_retail_market")
public class NGbCtGzRetailMarket extends Model<NGbCtGzRetailMarket> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty(value = "题库号")
    private Integer subjectNumber;

    @ApiModelProperty(value = "年份")
    private Integer year;

    @ApiModelProperty(value = "季度")
    private Integer quarterly;

    @ApiModelProperty(value = "产品")
    private String cpId;

    @ApiModelProperty(value = "特性编码")
    private String designNum;

    @ApiModelProperty(value = "承受单价")
    private Integer supportPrice;

    @ApiModelProperty(value = "购买数量")
    private Integer num;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
