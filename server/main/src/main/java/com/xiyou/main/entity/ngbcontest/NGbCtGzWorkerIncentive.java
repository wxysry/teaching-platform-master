package com.xiyou.main.entity.ngbcontest;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * <p>
 * 
 * </p>
 *
 * @author wangxingyu
 * @since 2023-06-06
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="NewCtGzWorkerIncentive对象", description="")
@TableName("ngb_ct_gz_worker_incentive")
public class NGbCtGzWorkerIncentive extends Model<NGbCtGzWorkerIncentive> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty(value = "题库号")
    private Integer subjectNum;

    @ApiModelProperty(value = "激励名称")
    private String incentiveName;

    @ApiModelProperty(value = "编码")
    private String incentiveNum;

    @ApiModelProperty(value = "提升效率比例(%)")
    private BigDecimal efficiencyPercent;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
