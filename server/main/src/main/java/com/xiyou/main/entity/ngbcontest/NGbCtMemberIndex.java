package com.xiyou.main.entity.ngbcontest;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 * @author wangxingyu
 * @since 2024-05-25
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="NgbCtMemberIndex对象", description="")
@TableName("ngb_ct_member_index")
public class NGbCtMemberIndex extends Model<NGbCtMemberIndex> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    private Integer contestId;

    private Integer studentId;

    @ApiModelProperty(value = "年份")
    private Integer year;

    @ApiModelProperty(value = "季度")
    private Integer quarterly;

    @ApiModelProperty(value = "会员指数")
    private Integer memberIndex;

    @ApiModelProperty(value = "热度")
    private Integer heatNum;

    @ApiModelProperty(value = "备份时间")
    private Integer backUpDate;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
