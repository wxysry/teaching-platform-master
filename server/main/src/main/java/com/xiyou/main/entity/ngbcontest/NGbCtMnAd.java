package com.xiyou.main.entity.ngbcontest;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.xiyou.main.utils.NumberUtils;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 *
 * </p>
 *
 * @author wangxingyu
 * @since 2023-06-11
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value = "CtMnAd对象", description = "")
@TableName("ngb_ct_mn_ad")
public class NGbCtMnAd extends Model<NGbCtMnAd> {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "广告")
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty(value = "学生id")
    private Integer studentId;

    @ApiModelProperty(value = "考试id")
    @NotNull(message = "考试id不能为空")
    private Integer contestId;

    @ApiModelProperty(value = "年份")
    @NotNull(message = "年份不能为空")
    private Integer year;


    @ApiModelProperty(value = "季度")
    @NotNull(message = "季度不能为空")
    private Integer quarterly;

    @ApiModelProperty(value = "广告投放时间")
    private LocalDateTime createTime;

    @ApiModelProperty(value = "组号")
    private String groupNum;


    @ApiModelProperty(value = "市场id")
    private Integer cmId;


    @ApiModelProperty(value = "市场名称")
    private String cmName;


    @ApiModelProperty(value = "投入金额")
    private Integer amount;


    @ApiModelProperty(value = "排名")
    @TableField(exist = false)
    private Integer rank;



    @ApiModelProperty(value = "本地P1")
    @TableField(exist = false)
    private Double localP1;

    @ApiModelProperty(value = "区域P1")
    @TableField(exist = false)
    private Double regionalP1;

    @ApiModelProperty(value = "国内P1")
    @TableField(exist = false)
    private Double nationalP1;

    @ApiModelProperty(value = "亚洲P1")
    @TableField(exist = false)
    private Double asianP1;

    @ApiModelProperty(value = "国际P1")
    @TableField(exist = false)
    private Double internationalP1;

    @ApiModelProperty(value = "本地P2")
    @TableField(exist = false)
    private Double localP2;

    @ApiModelProperty(value = "区域P2")
    @TableField(exist = false)
    private Double regionalP2;

    @ApiModelProperty(value = "国内P2")
    @TableField(exist = false)
    private Double nationalP2;

    @ApiModelProperty(value = "亚洲P2")
    @TableField(exist = false)
    private Double asianP2;

    @ApiModelProperty(value = "国际P2")
    @TableField(exist = false)
    private Double internationalP2;

    @ApiModelProperty(value = "本地P3")
    @TableField(exist = false)
    private Double localP3;

    @ApiModelProperty(value = "区域P3")
    @TableField(exist = false)
    private Double regionalP3;

    @ApiModelProperty(value = "国内P3")
    @TableField(exist = false)
    private Double nationalP3;

    @ApiModelProperty(value = "亚洲P3")
    @TableField(exist = false)
    private Double asianP3;

    @ApiModelProperty(value = "国际P3")
    @TableField(exist = false)
    private Double internationalP3;

    @ApiModelProperty(value = "本地P4")
    @TableField(exist = false)
    private Double localP4;

    @ApiModelProperty(value = "区域P4")
    @TableField(exist = false)
    private Double regionalP4;

    @ApiModelProperty(value = "国内P4")
    @TableField(exist = false)
    private Double nationalP4;

    @ApiModelProperty(value = "亚洲P4")
    @TableField(exist = false)
    private Double asianP4;

    @ApiModelProperty(value = "国际P4")
    @TableField(exist = false)
    private Double internationalP4;

    @ApiModelProperty(value = "本地P5")
    @TableField(exist = false)
    private Double localP5;

    @ApiModelProperty(value = "区域P5")
    @TableField(exist = false)
    private Double regionalP5;

    @ApiModelProperty(value = "国内P5")
    @TableField(exist = false)
    private Double nationalP5;

    @ApiModelProperty(value = "亚洲P5")
    @TableField(exist = false)
    private Double asianP5;

    @ApiModelProperty(value = "国际P5")
    @TableField(exist = false)
    private Double internationalP5;

    @ApiModelProperty(value = "是否提交")
    @TableField(exist = false)
    private String isSubmit;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    public Double calSum() {
        return localP1 + localP2 + localP3 + localP4 + localP5
                + regionalP1 + regionalP2 + regionalP3 + regionalP4 + regionalP5
                + nationalP1 + nationalP2 + nationalP3 + nationalP4 + nationalP5
                + asianP1 + asianP2 + asianP3 + asianP4 + asianP5
                + internationalP1 + internationalP2 + internationalP3 + internationalP4 + internationalP5;
    }

    /*
    根据字段名称获取值，如：本地P1 -> localP1
     */
    public Double getByName(String name) {
        Map<String, Double> map = new HashMap<>();
        map.put("本地P1", this.localP1);
        map.put("区域P1", this.regionalP1);
        map.put("国内P1", this.nationalP1);
        map.put("亚洲P1", this.asianP1);
        map.put("国际P1", this.internationalP1);
        map.put("本地P2", this.localP2);
        map.put("区域P2", this.regionalP2);
        map.put("国内P2", this.nationalP2);
        map.put("亚洲P2", this.asianP2);
        map.put("国际P2", this.internationalP2);
        map.put("本地P3", this.localP3);
        map.put("区域P3", this.regionalP3);
        map.put("国内P3", this.nationalP3);
        map.put("亚洲P3", this.asianP3);
        map.put("国际P3", this.internationalP3);
        map.put("本地P4", this.localP4);
        map.put("区域P4", this.regionalP4);
        map.put("国内P4", this.nationalP4);
        map.put("亚洲P4", this.asianP4);
        map.put("国际P4", this.internationalP4);
        map.put("本地P5", this.localP5);
        map.put("区域P5", this.regionalP5);
        map.put("国内P5", this.nationalP5);
        map.put("亚洲P5", this.asianP5);
        map.put("国际P5", this.internationalP5);
        Double ad = map.get(name);
        return ad == null ? 0 : ad;
    }


    /*
  根据字段名称获取值，如：本地P1 -> localP1
   */
    public Double getSumByName(String name) {
        Map<String, Double> map = new HashMap<>();
        Double local= NumberUtils.doubleTransform(localP1) + NumberUtils.doubleTransform(localP2) + NumberUtils.doubleTransform(localP3) + NumberUtils.doubleTransform(localP4) + NumberUtils.doubleTransform(localP5);
        Double regional= NumberUtils.doubleTransform(regionalP1) + NumberUtils.doubleTransform(regionalP2) + NumberUtils.doubleTransform(regionalP3) + NumberUtils.doubleTransform(regionalP4) + NumberUtils.doubleTransform(regionalP5);
        Double national=  NumberUtils.doubleTransform(nationalP1) + NumberUtils.doubleTransform(nationalP2) + NumberUtils.doubleTransform(nationalP3) + NumberUtils.doubleTransform(nationalP4) + NumberUtils.doubleTransform(nationalP5);
        Double asian= NumberUtils.doubleTransform(asianP1) + NumberUtils.doubleTransform(asianP2) + NumberUtils.doubleTransform(asianP3) + NumberUtils.doubleTransform(asianP4) + NumberUtils.doubleTransform(asianP5);
        Double international= NumberUtils.doubleTransform(internationalP1) + NumberUtils.doubleTransform(internationalP2) + NumberUtils.doubleTransform(internationalP3) + NumberUtils.doubleTransform(internationalP4) + NumberUtils.doubleTransform(internationalP5);

        map.put("本地P1", local);
        map.put("本地P2", local);
        map.put("本地P3", local);
        map.put("本地P4", local);
        map.put("本地P5", local);


        map.put("区域P1", regional);
        map.put("区域P2", regional);
        map.put("区域P3", regional);
        map.put("区域P4", regional);
        map.put("区域P5", regional);


        map.put("国内P1", national);
        map.put("国内P2", national);
        map.put("国内P3", national);
        map.put("国内P4", national);
        map.put("国内P5", national);

        map.put("亚洲P1", asian);
        map.put("亚洲P2", asian);
        map.put("亚洲P3", asian);
        map.put("亚洲P4", asian);
        map.put("亚洲P5", asian);

        map.put("国际P1", international);
        map.put("国际P2", international);
        map.put("国际P3", international);
        map.put("国际P4", international);
        map.put("国际P5", international);
        Double ad = map.get(name);
        return ad == null ? 0 : ad;
    }


}
