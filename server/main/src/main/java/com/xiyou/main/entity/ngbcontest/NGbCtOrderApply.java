package com.xiyou.main.entity.ngbcontest;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import java.math.BigDecimal;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 * @author wangxingyu
 * @since 2024-03-30
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="NgbCtOrderApply对象", description="")
@TableName("ngb_ct_order_apply")
public class NGbCtOrderApply extends Model<NGbCtOrderApply> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    private Integer contestId;

    private Integer studentId;

    @ApiModelProperty(value = "订单编号")
    private String coId;

    @ApiModelProperty(value = "申报数量")
    private Integer applyNum;

    @ApiModelProperty(value = "报价")
    private Integer applyPrice;

    @ApiModelProperty(value = "年份")
    private Integer year;

    @ApiModelProperty(value = "季度")
    private Integer quarterly;

    @ApiModelProperty(value = "备份时间")
    private Integer backUpDate;


    @ApiModelProperty(value = "Y值")
    @TableField(exist = false)
    private BigDecimal yValue;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
