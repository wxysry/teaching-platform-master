package com.xiyou.main.entity.ngbcontest;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotNull;

/**
 * <p>
 * 
 * </p>
 *
 * @author wangxingyu
 * @since 2024-04-10
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="NgbCtPdca对象", description="")
@TableName("ngb_ct_pdca")
public class NGbCtPdca extends Model<NGbCtPdca> {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "主键ID")
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty(value = "学生id")
    private Integer studentId;

    @ApiModelProperty(value = "考试id")
    private Integer contestId;

    @ApiModelProperty(value = "年份")
    private Integer year;

    @ApiModelProperty(value = "收入目标")
    @NotNull(message = "收入目标不能为空")
    private Integer srmb;


    @ApiModelProperty(value = "收入目标实际值")
    @TableField(exist = false)
    private Integer sjzSrmb;


    @ApiModelProperty(value = "收入目标得分")
    @TableField(exist = false)
    private Integer scoreSrmb;



    @ApiModelProperty(value = "产线建设数量")
    @NotNull(message = "产线建设数量不能为空")
    private Integer cxjsNum;


    @ApiModelProperty(value = "产线建设数量实际值")
    @TableField(exist = false)
    private Integer sjzCxjsNum;


    @ApiModelProperty(value = "产线建设数量得分")
    @TableField(exist = false)
    private Integer scoreCxjsNum;


    @ApiModelProperty(value = "产品入库数量")
    @NotNull(message = "产品入库数量不能为空")
    private Integer cprkNum;

    @ApiModelProperty(value = "产品入库数量数量实际值")
    @TableField(exist = false)
    private Integer sjzCprkNum;

    @ApiModelProperty(value = "产品入库数量数量得分")
    @TableField(exist = false)
    private Integer scoreCprkNum;

    @ApiModelProperty(value = "工人招聘数量")
    @NotNull(message = "工人招聘数量不能为空")
    private Integer grzpNum;

    @ApiModelProperty(value = "工人招聘数量实际值")
    @TableField(exist = false)
    private Integer sjzGrzpNum;

    @ApiModelProperty(value = "工人招聘数量得分")
    @TableField(exist = false)
    private Integer scoreGrzpNum;

    @ApiModelProperty(value = "市场开拓数量")
    @NotNull(message = "市场开拓数量不能为空")
    private Integer scktNum;

    @ApiModelProperty(value = "市场开拓数量实际值")
    @TableField(exist = false)
    private Integer sjzScktNum;

    @ApiModelProperty(value = "市场开拓数量得分")
    @TableField(exist = false)
    private Integer scoreScktNum;

    @ApiModelProperty(value = "产品研发数量")
    @NotNull(message = "市场研发数量不能为空")
    private Integer cpyfNum;

    @ApiModelProperty(value = "产品研发数量实际值")
    @TableField(exist = false)
    private Integer sjzCpyfNum;

    @ApiModelProperty(value = "产品研发数量得分")
    @TableField(exist = false)
    private Integer scoreCpyfNum;


    @ApiModelProperty(value = "贷款额度")
    @NotNull(message = "贷款额度不能为空")
    private Integer dked;

    @ApiModelProperty(value = "贷款额度实际值")
    @TableField(exist = false)
    private Integer sjzDked;

    @ApiModelProperty(value = "贷款额度得分")
    @TableField(exist = false)
    private Integer scoreDked;

    @ApiModelProperty(value = "广告投放金额")
    @NotNull(message = "广告投放金额不能为空")
    private Integer ggtf;

    @ApiModelProperty(value = "广告投放金额实际值")
    @TableField(exist = false)
    private Integer sjzGgtf;


    @ApiModelProperty(value = "广告投放金额得分")
    @TableField(exist = false)
    private Integer scoreGgtf;

    @ApiModelProperty(value = "生产部门预算")
    @NotNull(message = "生产部门预算不能为空")
    private Integer scbmys;

    @ApiModelProperty(value = "生产部门预算实际值")
    @TableField(exist = false)
    private Integer sjzScbmys;

    @ApiModelProperty(value = "生产部门预算得分")
    @TableField(exist = false)
    private Integer scoreScbmys;

    @ApiModelProperty(value = "人力部门预算")
    @NotNull(message = "人力部门预算不能为空")
    private Integer rlbmys;

    @ApiModelProperty(value = "人力部门预算实际值")
    @TableField(exist = false)
    private Integer sjzRlbmys;

    @ApiModelProperty(value = "人力部门预算得分")
    @TableField(exist = false)
    private Integer scoreRlbmys;

    @ApiModelProperty(value = "营销部门预算")
    @NotNull(message = "营销部门预算不能为空")
    private Integer yxbmys;

    @ApiModelProperty(value = "营销部门预算实际值")
    @TableField(exist = false)
    private Integer sjzYxbmys;

    @ApiModelProperty(value = "营销部门预算得分")
    @TableField(exist = false)
    private Integer scoreYxbmys;


    @ApiModelProperty(value = "是否显示得分")
    @TableField(exist = false)
    private boolean showScore;

    @ApiModelProperty(value = "备份时间")
    private Integer backUpDate;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }


    public void setDefaultValue(Integer contestId, Integer studentId, Integer year) {
        this.setContestId(contestId);
        this.setStudentId(studentId);
        this.setYear(year);
        this.setSrmb(0);
        this.setCxjsNum(0);
        this.setCprkNum(0);
        this.setGrzpNum(0);
        this.setScktNum(0);
        this.setCpyfNum(0);
        this.setDked(0);
        this.setGgtf(0);
        this.setScbmys(0);
        this.setRlbmys(0);
        this.setYxbmys(0);
    }
}
