package com.xiyou.main.entity.ngbcontest;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * <p>
 *
 * </p>
 *
 * @author tangcan
 * @since 2019-07-22
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value = "CtProfitChart对象", description = "")
@TableName("ngb_ct_profit_chart")
public class NGbCtProfitChart extends Model<NGbCtProfitChart> {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "利润表id")
    @TableId(value = "profit_chart_id", type = IdType.AUTO)
    private Integer profitChartId;

    @ApiModelProperty(value = "学生id")
    private Integer studentId;

    @ApiModelProperty(value = "学生账号")
    @TableField(exist = false)
    private String account;

    @ApiModelProperty(value = "考试id")
    private Integer contestId;

    @ApiModelProperty(value = "填写人：1代表系统自动生成，0代表学生录入")
    private Integer bsIsxt;

    @ApiModelProperty(value = "年份")
    private Integer pcDate;

    @ApiModelProperty(value = "销售额")
    @NotNull
    private Integer pcSales;

    @ApiModelProperty(value = "成本")
    @NotNull
    private Integer pcDirectCost;

    @ApiModelProperty(value = "毛利")
    @NotNull
    private Integer pcGoodsProfit;

    @ApiModelProperty(value = "综合费用")
    @NotNull
    private Integer pcTotal;

    @ApiModelProperty(value = "折旧前利润")
    @NotNull
    private Integer pcProfitBeforeDep;

    @ApiModelProperty(value = "折旧")
    @NotNull
    private Integer pcDep;

    @ApiModelProperty(value = "支付利息前利润")
    @NotNull
    private Integer pcProfitBeforeInterests;

    @ApiModelProperty(value = "财务费用")
    @NotNull
    private Integer pcFinanceFee;


    @ApiModelProperty(value = "营业外收支")
    @NotNull
    private Integer pcNonOperating;


    @ApiModelProperty(value = "税前利润")
    @NotNull
    private Integer pcProfitBeforeTax;

    @ApiModelProperty(value = "所得税")
    @NotNull
    private Integer pcTax;

    @ApiModelProperty(value = "净利润")
    @NotNull
    private Integer pcAnnualNetProfit;

    @ApiModelProperty(value = "是否提交,0 未提交 1，已提交")
    private String isSubmit;

    @Override
    protected Serializable pkVal() {
        return this.profitChartId;
    }

    public NGbCtProfitChart setZero() {
        pcSales = 0;
        pcDirectCost = 0;
        pcGoodsProfit = 0;
        pcTotal = 0;
        pcProfitBeforeDep = 0;
        pcDep = 0;
        pcProfitBeforeInterests = 0;
        pcFinanceFee = 0;
        pcNonOperating= 0;
        pcProfitBeforeTax = 0;
        pcTax = 0;
        pcAnnualNetProfit = 0;
        return this;
    }

}
