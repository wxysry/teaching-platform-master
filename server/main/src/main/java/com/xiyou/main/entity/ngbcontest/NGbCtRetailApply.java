package com.xiyou.main.entity.ngbcontest;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 * @author wangxingyu
 * @since 2024-05-25
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="NgbCtRetailApply对象", description="")
@TableName("ngb_ct_retail_apply")
public class NGbCtRetailApply extends Model<NGbCtRetailApply> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    private Integer contestId;

    private Integer studentId;

    @ApiModelProperty(value = "年份")
    private Integer year;

    @ApiModelProperty(value = "季度")
    private Integer quarterly;

    @ApiModelProperty(value = "零售市场ID")
    private Integer retailId;

    @ApiModelProperty(value = "产品")
    private String cpId;


    @ApiModelProperty(value = "产品名称")
    @TableField(exist = false)
    private String cpName;

    @ApiModelProperty(value = "特性编码")
    private String designNum;

    @ApiModelProperty(value = "特征名称")
    @TableField(exist = false)
    private String designName;

    @ApiModelProperty(value = "报价")
    private Integer applyPrice;

    @ApiModelProperty(value = "投放数量")
    private Integer applyNum;

    @ApiModelProperty(value = "备份时间")
    private Integer backUpDate;

    @ApiModelProperty(value = "会员指数")
    @TableField(exist = false)
    private Integer memberIndex;

    @ApiModelProperty(value = "零售指数")
    @TableField(exist = false)
    private Integer retailIndex;

    @ApiModelProperty(value = "竞争指数")
    @TableField(exist = false)
    private Integer competeIndex;

    @ApiModelProperty(value = "分配量")
    @TableField(exist = false)
    private Integer assignedNum;


    @ApiModelProperty(value = "承受单价")
    @TableField(exist = false)
    private Integer supportPrice;

    @ApiModelProperty(value = "M")
    @TableField(exist = false)
    private Integer m;




    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
