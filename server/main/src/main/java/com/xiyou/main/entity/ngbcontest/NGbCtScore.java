package com.xiyou.main.entity.ngbcontest;

import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 * @author wangxingyu
 * @since 2024-05-08
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="NgbCtScore对象", description="")
@TableName("ngb_ct_score")
public class NGbCtScore extends Model<NGbCtScore> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    private Integer studentId;

    private Integer contestId;

    @ApiModelProperty(value ="排名")
    @TableField(exist = false)
    Integer rank;

    @ApiModelProperty(value ="企业名")
    @TableField(exist = false)
    String groupNum;


    @ApiModelProperty(value = "年份")
    private Integer year;

    @ApiModelProperty(value = "季度")
    private Integer quarterly;

    @ApiModelProperty(value = "权益值")
    private Integer equity;

    @ApiModelProperty(value = "商誉")
    private Integer sy;

    @ApiModelProperty(value = "数字化得分")
    private Integer szh;

    @ApiModelProperty(value = "扣分")
    private Integer kf;

    @ApiModelProperty(value = "碳中和率")
    private BigDecimal neutralizaRate;

    @ApiModelProperty(value = "总分")
    private Long score;

    @ApiModelProperty(value = "备份时间")
    private Integer backUpDate;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
