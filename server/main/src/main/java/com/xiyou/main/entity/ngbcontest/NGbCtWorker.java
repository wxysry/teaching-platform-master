package com.xiyou.main.entity.ngbcontest;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * @author wangxy
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@ApiModel(value = "NewCtWorker对象", description = "")
@TableName("ngb_ct_worker")
public class NGbCtWorker extends Model<NGbCtWorker> {

    private static final long serialVersionUID = 1L;
    public static final Integer ON_BUSI = 1;  //招聘中
    public static final Integer ON_SPACE = 2; //空闲中
    public static final Integer ON_WORK = 3;  //在岗
    public static final Integer ON_PRODUCE = 6;  //生产中
    public static final Integer ON_TRAIN = 4;  //培训中
    public static final Integer ON_DISMISS = 5;  //已辞退


    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty(value = "竞赛ID")
    private Integer contestId;

    @ApiModelProperty(value = "学生id")
    private Integer studentId;

    @ApiModelProperty(value = "姓名")
    private String workerName;

//    @ApiModelProperty(value = "序号")
//    private Integer workerNum;

    @ApiModelProperty(value = "等级")
    private String recruitName;

    @ApiModelProperty(value = "等级编码")
    private String recruitNum;


    @ApiModelProperty(value = "基本工资(元)")
    private Integer initSal;

    @ApiModelProperty(value = "下季度基本工资(元)")
    private Integer nextInitSal;

    @ApiModelProperty(value = "倍数加成(%)")
    private Integer multBonus;

    @ApiModelProperty(value = "计件工资")
    private  Integer piece;


    @ApiModelProperty(value = "正式入职时间")
    private Integer startWorkTime;

    @ApiModelProperty(value = "员工状态")
    private  Integer isWork;//1 发送offer等待入职  2.在职(空闲)  3.在岗  4.培训中  5.已解聘


    @ApiModelProperty(value = "能否成功招聘")
    private  Integer isRecruit;//1是  2否


    @ApiModelProperty(value = "是否欠薪")
    private  Integer isBackPay;//1是  2否


    @ApiModelProperty(value = "薪资类型")
    private  Integer salType;//1 人力费  2 基本工资

    @ApiModelProperty(value = "产线ID")
    private Integer lineId;


    @ApiModelProperty(value = "培训结束时间")
    private Integer trainEndTime;


    @ApiModelProperty(value = "人才市场工人ID")
    private Integer wokerMarketId;
    

















}