package com.xiyou.main.entity.ngbcontest;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 * @author wangxingyu
 * @since 2024-03-21
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="NgbCtWorkerMarket对象", description="")
@TableName("ngb_ct_worker_market")
public class NGbCtWorkerMarket extends Model<NGbCtWorkerMarket> {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "市场工人ID")
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty(value = "竞赛ID")
    private Integer contestId;

    @ApiModelProperty(value = "招聘时间")
    private Integer date;

    @ApiModelProperty(value = "姓名")
    private String workerName;

    @ApiModelProperty(value = "等级")
    private String recruitName;

    @ApiModelProperty(value = "等级编码")
    private String recruitNum;

    @ApiModelProperty(value = "倍数加成(%)")
    private Integer multBonus;

    @ApiModelProperty(value = "初始期望工资(元)")
    private Integer initSal;

    @ApiModelProperty(value = "计件工资")
    private Integer piece;


    @ApiModelProperty(value = "offer薪资")
    @TableField(exist = false)
    private Integer offerSal;


    @ApiModelProperty(value = "是否已发放offer")
    @TableField(exist = false)
    private boolean sendOffer;



    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
