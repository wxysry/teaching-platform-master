package com.xiyou.main.entity.ngbcontest;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author wangxingyu
 * @since 2023-09-14
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="GbCtXdOrderTemp对象", description="")
@TableName("ngb_ct_xd_order_temp")
public class NGbCtXdOrderTemp extends Model<NGbCtXdOrderTemp> {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "id")
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty(value = "学生id")
    private Integer studentId;

    @ApiModelProperty(value = "考试id")
    private Integer contestId;

    @ApiModelProperty(value = "年份")
    private Integer year;

    @ApiModelProperty(value = "季度")
    private Integer quarterly;

    @ApiModelProperty(value = "组号")
    private String groupNum;

    @ApiModelProperty(value = "轮次")
    private Integer round;

    @ApiModelProperty(value = "市场")
    private String market;

    @ApiModelProperty(value = "产品")
    private String product;

    @ApiModelProperty(value = "产品广告")
    @TableField("productAd")
    private Integer productAd;

    @ApiModelProperty(value = "市场广告")
    @TableField("marketAd")
    private Integer marketAd;

    @ApiModelProperty(value = "上一年销售额")
    private Integer sales;

    @ApiModelProperty(value = "剩余选单次数")
    @TableField("remainNum")
    private Integer remainNum;

    @ApiModelProperty(value = "删除状态，0未删除，1已删除")
    @TableField("deleteStatus")
    private Integer deleteStatus;

    @ApiModelProperty(value = "备份（已选单，放弃，无单可选）")
    private String remark;

    @ApiModelProperty(value = "该用户是否是自己")
    @TableField(exist = false)
    private Integer isMe;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
