package com.xiyou.main.entity.ngbcontest;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author tangcan
 * @since 2019-07-22
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="CtYfProduct对象", description="")
@TableName("ngb_ct_yf_product")
public class NGbCtYfProduct extends Model<NGbCtYfProduct> {

    private static final long serialVersionUID = 1L;
    public static final String CP_YF_FINISH = "已完成";
    public static final String CP_YF_ING = "研发中";
    public static final String CP_YF_NONE = "未研发";

    @TableId(value = "product_id", type = IdType.AUTO)
    private Integer productId;

    private Integer studentId;

    private Integer contestId;

    @ApiModelProperty(value = "产品编号")
    private Integer dpCpId;

    @ApiModelProperty(value = "总研发时间")
    private Integer dpTotalDate;

    @ApiModelProperty(value = "已研发时间")
    private Integer dpNowDate;

    @ApiModelProperty(value = "剩余研发时间")
    private Integer dpRemainDate;

    @ApiModelProperty(value = "申请时间")
    private Integer dpStartDate;

    @ApiModelProperty(value = "研发完成时间")
    private Integer dpFinishDate;

    @ApiModelProperty(value = "状态")
    private String dpState;

    @ApiModelProperty(value = "名称")
    @TableField(exist = false)
    private String cpName;

    @ApiModelProperty(value = "开发费用")
    @TableField(exist = false)
    private Integer cpProcessingFee;

    @ApiModelProperty(value = "开发周期")
    @TableField(exist = false)
    private Integer cpDevelopDate;


    @Override
    protected Serializable pkVal() {
        return this.productId;
    }

}
