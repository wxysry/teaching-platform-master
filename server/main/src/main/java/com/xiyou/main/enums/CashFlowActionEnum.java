package com.xiyou.main.enums;

/**
 * @author dingyoumeng
 * @since 2019/07/22
 */
public enum CashFlowActionEnum {
    PAY_CAPITAL("开始经营"),
    PAY_AD("支付广告费"),
    PAY_INCOME_TAX("支付所得税"),
    RETURN_LONG_LOAN("归还长期贷款"),
    RETURN_LONG_LOAN_INTEREST("归还长期贷款利息"),
    RETURN_SHORT_LOAN("归还短期贷款"),
    RETURN_SHORT_LOAN_INTEREST("归还短期贷款利息"),
    SHORT_LOAN("短期贷款"),
    LONG_LOAN("长期贷款"),
    ORDER_MATERIAL("下原料订单"),
    BUY_WORKSHOP("购买厂房"),
    RENT_WORKSHOP("租用厂房"),
    BUY_TO_RENT_WORKSHOP("厂房买转租"),
    BUY_PRODUCT_LINE("新建生产线"),
    PRODUCT_LINE_BUILDING("在建生产线"),
    PRODUCT_PRODUCING("下一批生产"),
    PRODUCT_LINE_PROCESS("更新生产/完工入库"),
    SELL_PRODUCT_LINE("变卖生产线"),
    PRODUCT_LINE_TRANSFER("生产线转产"),
    UPDATE_RECEIVABLE("更新应收款"),
    UPDATE_PAY("更新应付款"),
    DEVELOP_PRODUCT("产品研发"),
    DELIVERY_ORDER("交货订单"),
    DISCOUNT("贴现"),
    MATERICAL_PROCESS("更新原料订单/原料入库"),
    WORKSHOP_DISCOUNT("厂房贴现"),
    PAY_OVERHAUL("支付管理费"),
    INVEST_PRODUCT_LINE("在建生产线投资"),
    PAY_MAINTENANCE("维修费"),
    DEVELOP_MARKET("市场开拓"),
    DEVELOP_ISO("ISO开拓"),
    URGENT_MATERIAL_COST("原材料紧急采购成本"),
    URGENT_MATERIAL_LOSS("原材料紧急采购损失"),
    URGENT_PRODUCT_COST("产成品紧急采购成本"),
    URGENT_PRODUCT_LOSS("产成品紧急采购损失"),
    PAY_MATERIAL("原材料付款"),
    SELL_MATERIAL("出售原材料"),
    SELL_PRODUCT("出售库存"),
    REPAYMENT_INTEREST_R("本息同还-本金"),
    REPAYMENT_INTEREST_I("本息同还-利息"),
    INTEREST_R("每季付息-本金"),
    INTEREST_I("每季付息-利息"),
    DIGITALIZATION("数字化"),
    YXSZH("营销数字化"),
    SCSZH("生产数字化"),
    RLSZH("人力数字化"),
    CWSZH("财务数字化"),
    ADD_CAPITAL("注入资本"),
    INFO_OTHER("信息费"),
    GRPX("培训费"),
    GRPCK("辞退福利"),

    GRGZ("工人工资"),
    RLF("人力费"),

    GRJL("激励费"),
    JJGZ("计件工资"),
    TZYF("特性研发"),
    CPSJ("产品设计"),
    ORDERWYJ("违约金"),
    TZH("碳中和"),
    XMTGG("新媒体广告费"),
    LSDDJH("零售订单交货");



    String action;

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    CashFlowActionEnum(String action) {
        this.action = action;
    }
}
