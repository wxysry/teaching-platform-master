package com.xiyou.main.enums;

import com.xiyou.common.utils.R;

/**
 * @author dingyoumeng
 * @since 2019/07/08
 */
public enum ErrorEnum{
    /*
    业务错误
    */
    COURSE_GROUP_EXIST(1000, "分组名称已经存在"),
    CASH_NOT_ENOUGH(1101, "现金不足"),
    INVENTORY_NOT_ENOUGH(1102, "库存不足"),

    LOAN_ERROR(1102, "贷款错误"),
    MATERIAL_NOT_ENOUGH(1103, "原料不足"),
    PRODUCT_NOT_ENOUGH(1104, "产品不足"),
    PRODUCT_NOT_EXIST(1105, "产品不存在"),
    MATERIAL_NOT_EXIST(1106, "材料不存在"),
    WORKER_WORKING(1107, "工人已在其他产线"),
    TRANSFER_WORING(1108, "转产失败"),
    AUTO_PRODUCT_NO_LINE(1109, "智能化生产失败,无可用产线"),
    AUTO_PRODUCT_ERROR_LINE(1110, "智能化生产失败,部分产线无法正常生产"),
    DISCOUNT_ERROR(1111, "应收款不足"),
    NO_LAST_ROUND(1112, "没有上一回合"),
    LINE_MULTI(1113, "产线数量超出限制"),
    RESTORE_TIME_ERROR(1114, "还原时间错误"),
    MARKET_AND_PRODUCT_INIT_ERROR(1115, "市场或产品初始化错误"),
    FAIR_ORDER_STATE_ERROR(1116, "订货会状态错误！"),
    CHOOSE_TYEP_WRONG(1117, "查询信息错误"),
    YTF_GG(1118, "本季已投放广告"),

    GRYZP(1119, "该工人已招聘"),


    ;


    int code;
    String msg;


    ErrorEnum(int code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public R getR(){
        return R.error(this.code, this.msg);
    }
}
