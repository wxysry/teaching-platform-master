package com.xiyou.main.enums;

/**
 * @author wangxy
 */

public enum RedisEnum {
    HHDJS("回合倒计时", "HHDJS"),
    XDDJS("选单倒计时", "XDDJS"),
    GZHHDJS("高职回合倒计时", "GZHHDJS");

    private String label;
    private String value;

    // 构造函数
    RedisEnum(String label, String value) {
        this.label = label;
        this.value = value;
    }

    // 获取标签
    public String getLabel() {
        return label;
    }

    // 获取值
    public String getValue() {
        return value;
    }
}