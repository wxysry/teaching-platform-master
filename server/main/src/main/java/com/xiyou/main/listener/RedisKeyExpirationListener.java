package com.xiyou.main.listener;

import org.apache.commons.lang3.StringUtils;
import org.springframework.data.redis.connection.Message;
import org.springframework.data.redis.listener.KeyExpirationEventMessageListener;
import org.springframework.data.redis.listener.RedisMessageListenerContainer;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

@Transactional
@Component
public class RedisKeyExpirationListener extends KeyExpirationEventMessageListener {


    //回合倒计时   value 存剩余时间
    public static final String HHDJS="HHDJS";

    //选单倒计时  value存剩余时间
    public static final String XDDJS="XDDJS";



    public RedisKeyExpirationListener(RedisMessageListenerContainer listenerContainer) {
        super(listenerContainer);
    }


    @Override
    public void onMessage(Message message, byte[] pattern) {
        String key = message.toString();
        if (!StringUtils.isBlank(key)) {
            //TODO 回合倒计时逻辑处理
            if(key.startsWith(HHDJS)){

            }

            //TODO 选单倒计时逻辑处理
            if(key.startsWith(XDDJS)){

            }

        }

    }
}
