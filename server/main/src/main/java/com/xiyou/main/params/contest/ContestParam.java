package com.xiyou.main.params.contest;

import com.xiyou.main.params.exam.PageParam;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.hibernate.validator.constraints.Range;

/**
 * @program: multi-module
 * @description: 竞赛查询参数
 * @author: tangcan
 * @create: 2019-08-25 15:27
 **/
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value = "竞赛查询参数")
public class ContestParam extends PageParam {
    @ApiModelProperty(hidden = true)
    private Integer teacherId;

    @ApiModelProperty(hidden = true)
    private Integer studentId;

    @ApiModelProperty(hidden = true)
    private Integer groupId;

    @ApiModelProperty(value = "是否发布竞赛")
    @Range(min = 0, max = 1, message = "是否发布竞赛只能为{min}和{max}")
    private Integer isPublish;

    @ApiModelProperty(value = "竞赛标题关键字")
    private String keyword;
}
