package com.xiyou.main.params.contest;

import com.xiyou.main.params.exam.PageParam;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * @program: multi-module
 * @description:
 * @author: tangcan
 * @create: 2019-08-26 21:20
 **/
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value = "请求参数")
public class ContestStudentParam extends PageParam {
    @ApiModelProperty(hidden = true)
    private Integer teacherId;

    @ApiModelProperty(value = "竞赛id")
    private Integer contestId;

    @ApiModelProperty(value = "是否完成：0表示未完成，1表示已完成")
    private Integer finish;

    @ApiModelProperty(value = "学生姓名关键字")
    private String keyword;
}
