package com.xiyou.main.params.contest;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotNull;

/**
 * @author dingyoumeng
 * @since 2019/08/18
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value = "贴现参数")
public class DiscountParam {

    @NotNull
    @ApiModelProperty(value ="竞赛Id")
    Integer contestId;

    @NotNull
    @ApiModelProperty(value ="当前时间")
    Integer date;

    @NotNull
    @ApiModelProperty(value ="1季贴现额")
    Integer fee1;

    @NotNull
    @ApiModelProperty(value ="2季贴现额")
    Integer fee2;

    @NotNull
    @ApiModelProperty(value ="3季贴现额")
    Integer fee3;

    @NotNull
    @ApiModelProperty(value ="4季贴现额")
    Integer fee4;
}
