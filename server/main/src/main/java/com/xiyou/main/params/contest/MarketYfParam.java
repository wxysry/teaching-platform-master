package com.xiyou.main.params.contest;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * @author dingyoumeng
 * @since 2019/09/01
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value = "市场研发")
public class MarketYfParam {

    @ApiModelProperty(value = "竞赛id")
    @NotNull(message = "竞赛id不能为空")
    private Integer contestId;

    @ApiModelProperty(value = "当前时间")
    @NotNull(message = "当前时间不能为空")
    private Integer date;

    @ApiModelProperty(value = "选择的iso的id列表")
    private List<Integer> marketIds;
}
