package com.xiyou.main.params.contest;

import com.xiyou.main.entity.contest.CtDdMaterial;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * @program: multi-module
 * @description: 订购原料参数
 * @author: tangcan
 * @create: 2019-08-25 19:49
 **/
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value = "订购原料参数")
public class MaterialParam {
    @ApiModelProperty(hidden = true)
    private Integer studentId;

    @ApiModelProperty(value = "竞赛id")
    @NotNull(message = "竞赛id不能为空")
    private Integer contestId;

    @ApiModelProperty(value = "当期时间")
    @NotNull(message = "当期时间不能为空")
    private Integer currentTime;

    @ApiModelProperty(value = "原料清单")
    private List<CtDdMaterial> materialList;
}
