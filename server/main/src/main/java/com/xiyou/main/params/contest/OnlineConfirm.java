package com.xiyou.main.params.contest;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.util.List;

/**
 * @author dingyoumeng
 * @since 2019/07/22
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value = "在建生产线确认参数 / 生产线转产确认参数")
public class OnlineConfirm {

    @ApiModelProperty(value ="竞赛Id")
    Integer contestId;

    @ApiModelProperty(value ="选择的生产线id")
    List<Integer> lineIds;

    @ApiModelProperty(value ="当前时间")
    Integer date;

    @ApiModelProperty(value ="所属工厂")
    Integer plWid;

    @ApiModelProperty(value = "转产产品的id")
    Integer cpId;


}
