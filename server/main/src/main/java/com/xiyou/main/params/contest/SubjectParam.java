package com.xiyou.main.params.contest;

import com.xiyou.main.params.exam.PageParam;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.time.LocalDateTime;

/**
 * @program: multi-module
 * @description: 题库请求参数
 * @author: tangcan
 * @create: 2019-08-25 13:07
 **/
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value = "题库查询参数")
public class SubjectParam extends PageParam {
    @ApiModelProperty(hidden = true)
    private Integer teacherId;

    @ApiModelProperty(value = "题库号")
    private Integer subjectNumber;

    @ApiModelProperty(value = "题库所属分组id")
    private Integer groupId;

    @ApiModelProperty(value = "题库名称关键字")
    private String keyword;
}
