package com.xiyou.main.params.contest;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * @author dingyoumeng
 * @since 2019/08/29
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value = "紧急采购参数")
public class UrgentBuyParam {

    @NotNull
    @ApiModelProperty(value ="竞赛Id")
    Integer contestId;

    @NotNull
    @ApiModelProperty(value ="当前时间")
    Integer date;

    @ApiModelProperty(value = "采购的材料")
    List<Material> materials;

    @ApiModelProperty(value = "采购的产品")
    List<Product> products;

    @Data
    public class Product {
        Integer ipCpId;
        //采购数量
        Integer num;
    }

    @Data
    public class Material {
        Integer imCmId;
        //采购数量
        Integer num;
    }
}
