package com.xiyou.main.params.contest;

import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.util.List;

/**
 * @author dingyoumeng
 * @since 2019/09/01
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value = "厂房贴现")
public class WsDiscountParam {
    Integer contestId;

    List<Integer> workshopIds;

    Integer date;
}
