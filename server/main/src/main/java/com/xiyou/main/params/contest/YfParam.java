package com.xiyou.main.params.contest;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * @author dingyoumeng
 * @since 2019/07/26
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value = "产品研发参数")
public class YfParam {
    @NotNull
    @ApiModelProperty(value ="竞赛Id")
    Integer contestId;

    @NotNull
    @ApiModelProperty(value ="当前时间")
    Integer date;

    @NotNull
    @ApiModelProperty(value ="选择的产品id")
    List<Integer> cpIds;
}
