package com.xiyou.main.params.exam;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

/**
 * @author dingyoumeng
 * @since 2019/07/08
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value = "课程查询参数")
public class CourseQueryParam extends PageParam {
    @ApiModelProperty(value = "教师id", hidden = true)
    private Integer teacherId;

    @ApiModelProperty(value = "学生id", hidden = true)
    private Integer studentId;

    @ApiModelProperty(value = "分组id，查找所有分组时，传值为0")
    private Integer groupId;

    @ApiModelProperty(value = "课程名称查询")
    private String value;

    @ApiModelProperty(hidden = true)
    public Page getIPage() {
        return new Page(this.getPage(), this.getLimit());
    }
}
