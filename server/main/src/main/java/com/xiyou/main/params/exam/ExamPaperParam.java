package com.xiyou.main.params.exam;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.hibernate.validator.constraints.Range;

/**
 * @program: multi-module
 * @description: 考卷查询
 * @author: tangcan
 * @create: 2019-07-10 13:58
 **/
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value = "学生的考卷信息")
public class ExamPaperParam extends PageParam {
    @ApiModelProperty(value = "学生id", hidden = true)
    private Integer studentId;

    @ApiModelProperty(value = "试卷状态", notes = "0表示全部，1表示即将开始，2表示正在进行，3表示已经结束")
    @Range(min = 0, max = 3, message = "status只能在{min}~{max}之间")
    private Integer status;

    @ApiModelProperty(value = "是否已交卷")
    private Integer isHandIn;

    @ApiModelProperty(value = "试卷标题关键字")
    private String keyword;
}
