package com.xiyou.main.params.exam;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.hibernate.validator.constraints.Range;

import javax.validation.constraints.NotNull;

/**
 * @program: multi-module
 * @description: 考卷查询
 * @author: tangcan
 * @create: 2019-07-10 13:58
 **/
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value = "考卷的学生信息")
public class ExamStudentParam extends PageParam {
    @ApiModelProperty(value = "教师id", hidden = true)
    private Integer teacherId;

    @ApiModelProperty(value = "paperId")
    @NotNull(message = "paperId不能为空")
    private Integer paperId;

    @ApiModelProperty(value = "是否批改完成", notes = "0表示待批改，1表示已批改")
    @Range(min = 0, max = 1, message = "correctComplete只能为{min}和{max}")
    private Integer correct;

    @ApiModelProperty(value = "是否已交卷")
    @Range(min = 0, max = 1, message = "isHandIn只能为{min}和{max}")
    private Integer isHandIn;

    @ApiModelProperty(value = "学生姓名或账号关键字")
    private String keyword;
}
