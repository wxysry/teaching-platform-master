package com.xiyou.main.params.exam;

import com.xiyou.main.groups.Add;
import com.xiyou.main.groups.Update;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * @program: multi-module
 * @description:
 * @author: tangcan
 * @create: 2019-07-08 17:07
 **/
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value = "分组信息")
public class GroupParam {
    @ApiModelProperty(value = "分组的id")
    @NotNull(message = "id不能为空", groups = {Update.class})
    @Min(value = 1, message = "id不能小于{value}", groups = {Update.class})
    private Integer id;

    @ApiModelProperty(value = "父级分组的id，等于0或者null表示第一级")
    private Integer fatherId;

    @ApiModelProperty(value = "分组名称")
    @NotBlank(message = "分组名称不能为空")
    @Length(max = 50, message = "分组名称长度不能超过{max}", groups = {Add.class, Update.class})
    private String groupName;
}
