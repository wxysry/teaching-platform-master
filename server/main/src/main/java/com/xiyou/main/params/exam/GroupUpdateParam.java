package com.xiyou.main.params.exam;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.hibernate.validator.constraints.Range;

import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * @program: multi-module
 * @description: 分组修改
 * @author: tangcan
 * @create: 2019-07-08 16:07
 **/
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value = "分组修改")
public class GroupUpdateParam {
    @ApiModelProperty(value = "用户id")
    @NotNull(message = "请选择用户")
    private Integer userId;

    @ApiModelProperty(value = "分组类型，0表示题库分组，1表示视频分组，2表示PPT分组，3表示竞赛模拟分组，4表示新平台模拟分组，5表示组间对抗，6表示新组间对抗")
    @NotNull(message = "分组类型不能为空")
    @Range(min = 0, max = 6, message = "分组类型只能在{min}~{max}之内")
    private Integer type;

    @ApiModelProperty(value = "分组列表")
    @NotNull(message = "分组列表不能为空")
    private List<Integer> groups;
}
