package com.xiyou.main.params.exam;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiParam;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.springframework.web.bind.annotation.RequestParam;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * @program: multi-module
 * @description: 学生提交答案
 * @author: tangcan
 * @create: 2019-07-17 18:53
 **/
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value = "答案信息")
public class HandInAnswerParam {
    @ApiModelProperty(value = "学生考卷id")
    @NotNull(message = "examId不能为空")
    private Integer examId;

    @ApiModelProperty(value = "答案列表")
    @Valid
    private List<Answer> answerList;

    @Valid
    @Data
    public class Answer {
        @NotNull(message = "试题id不能为空")
        private Integer problemId;

        private String answer;
    }
}
