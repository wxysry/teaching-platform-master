package com.xiyou.main.params.exam;

import cn.afterturn.easypoi.excel.annotation.Excel;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;

/**
 * @program: multi-module
 * @description: 登录参数
 * @author: tangcan
 * @create: 2019-06-24 10:35
 **/
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value = "登录参数")
public class LoginParam {
    @ApiModelProperty(value = "账号")
    @NotBlank(message = "账号不能为空")
    @Length(max = 50, message = "账号长度不能超过{max}")
    private String account;

    @NotBlank(message = "密码不能为空")
    @Length(min = 1, max = 50, message = "密码长度必须在{min} ~ {max}之间")
    @ApiModelProperty(value = "密码")
    private String password;

    @ApiModelProperty(value = "验证码")
    @NotBlank(message = "验证码不能为空")
    @Length(min = 4, max = 4, message = "验证码只能是{min}位")
    private String code;

    @ApiModelProperty(value = "验证码token")
    @NotBlank(message = "验证码token不能为空")
    private String captchaToken;
}
