package com.xiyou.main.params.exam;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

/**
 * @program: multi-module
 * @description: 分页的参数
 * @author: tangcan
 * @create: 2019-06-24 20:38
 **/
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value = "分页参数")
public class PageParam {
    @ApiModelProperty(value = "当前页数")
    @NotNull(message = "当前页数不能为空")
    @Min(value = 1, message = "当前页数不能小于{value}")
    private Integer page;


    @ApiModelProperty(value = "每页显示条数")
    @NotNull(message = "每页显示条数不能为空")
    @Min(value = 1, message = "每页显示条数不能小于{value}")
    @Max(value = 2000, message = "每页显示条数不能大于{value}")
    private Integer limit;

    @ApiModelProperty(hidden = true)
    public Page getIPage() {
        Page p = new Page(this.page, this.limit);
        return p;
    }
}
