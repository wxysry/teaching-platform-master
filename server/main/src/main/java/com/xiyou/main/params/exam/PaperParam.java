package com.xiyou.main.params.exam;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.hibernate.validator.constraints.Range;

/**
 * @program: multi-module
 * @description: 试卷请求参数
 * @author: tangcan
 * @create: 2019-07-14 17:07
 **/
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value = "试卷请求参数")
public class PaperParam extends PageParam {
    @ApiModelProperty(value = "教师id", hidden = true)
    private Integer teacherId;

    @ApiModelProperty(value = "是否发布")
    @Range(min = 0, max = 1, message = "publish只能为{min}和{max}")
    private Integer publish;

    @ApiModelProperty(value = "试卷标题关键字")
    private String keyword;
}
