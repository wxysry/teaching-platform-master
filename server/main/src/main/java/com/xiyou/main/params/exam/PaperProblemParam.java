package com.xiyou.main.params.exam;

import com.xiyou.main.entity.exam.PaperProblem;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * @program: multi-module
 * @description: 试卷试题
 * @author: tangcan
 * @create: 2019-07-09 16:29
 **/
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value = "试卷试题")
public class PaperProblemParam {
    @ApiModelProperty(value = "试卷id")
    @NotNull(message = "试卷id不能为空")
    private Integer paperId;

    @ApiModelProperty(value = "试题列表")
    @NotNull(message = "试题列表不能为空")
    @Valid
    private List<PaperProblem> problems;
}
