package com.xiyou.main.params.exam;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.hibernate.validator.constraints.Range;

import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * @program: multi-module
 * @description: 试卷试题
 * @author: tangcan
 * @create: 2019-07-09 16:29
 **/
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value = "学生列表")
public class PaperStudentParam {
    @ApiModelProperty(value = "试卷id")
    @NotNull(message = "试卷id不能为空")
    private Integer paperId;

    @ApiModelProperty(value = "0表示向指定考试发布，1表示向全部考试发布")
    @Range(min = 0, max = 1, message = "publishAll只能为0或1")
    private Integer publishAll;

    @ApiModelProperty(value = "学生id列表")
    @NotNull(message = "列表不能为空")
    private List<Integer> studentList;
}
