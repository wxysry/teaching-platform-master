package com.xiyou.main.params.exam;

import com.xiyou.main.entity.exam.Resources;
import com.xiyou.main.groups.Add;
import com.xiyou.main.groups.Update;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;
import java.util.List;

/**
 * @program: multi-module
 * @description: 试题参数
 * @author: tangcan
 * @create: 2019-07-07 14:49
 **/
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value = "试题信息")
public class ProblemInfo {
    @ApiModelProperty(value = "试题id")
    @NotNull(message = "请选择试题", groups = {Update.class})
    private Integer id;

    @ApiModelProperty(value = "是否是选择题", hidden = true)
    private Integer isChoice;

    @ApiModelProperty(value = "题型id")
    @NotNull(message = "请选择题型", groups = {Add.class, Update.class})
    private Integer typeId;

    @ApiModelProperty(value = "题型名称", hidden = true)
    private String typeName;

    @ApiModelProperty(value = "创建人", hidden = true)
    private String createUserName;

    @ApiModelProperty(value = "考题分组的id(一级分组)")
    @NotNull(message = "请选择分组", groups = {Add.class, Update.class})
    private Integer groupId;

    @ApiModelProperty(value = "考题分组的id(二级分组)")
    @NotNull(message = "请选择分组", groups = {Add.class, Update.class})
    private Integer subgroupId;

    @ApiModelProperty(value = "考题分组的id(一级分组和二级分组)")
    private List<Integer> groupIds;

    @ApiModelProperty(value = "分组名称，一级和二级", hidden = true)
    private String groupName;

    @ApiModelProperty(value = "题目")
    private String title;

    @ApiModelProperty(value = "题目简介")
    @Length(max = 100, message = "简介长度不能超过{max}", groups = {Add.class, Update.class})
    private String intro;

    @ApiModelProperty(value = "答案")
    private String answer;

    @ApiModelProperty(value = "解析")
    private String analysis;

    @ApiModelProperty(value = "评分要点")
    private String scorePoint;

    @ApiModelProperty(value = "图片")
    private String pic;

    @ApiModelProperty(value = "附件")
    private String attachment;

    @ApiModelProperty(value = "选项", hidden = true)
    private List<String> optionList;

    @ApiModelProperty(value = "图片", hidden = true)
    private List<Resources> picList;

    @ApiModelProperty(value = "附件", hidden = true)
    private List<Resources> attachmentList;

    @ApiModelProperty(value = "创建时间", hidden = true)
    private LocalDateTime createTime;
}
