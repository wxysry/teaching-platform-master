package com.xiyou.main.params.exam;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * @program: multi-module
 * @description:
 * @author: tangcan
 * @create: 2019-07-07 20:44
 **/
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value = "试题参数")
public class ProblemParam extends PageParam {
    @ApiModelProperty(value = "题型id", required = false)
    private Integer typeId;

    @ApiModelProperty(value = "一级分组id", required = false)
    private Integer groupId;

    @ApiModelProperty(value = "二级分组id", required = false)
    private Integer subgroupId;

    @ApiModelProperty(value = "创建人id", required = false)
    private Integer createUserId;

    @ApiModelProperty(value = "简介关键字")
    private String keyword;
}
