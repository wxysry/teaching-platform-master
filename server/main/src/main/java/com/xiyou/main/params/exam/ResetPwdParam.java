package com.xiyou.main.params.exam;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;

/**
 * @program: multi-module
 * @description: 修改密码
 * @author: tangcan
 * @create: 2019-06-25 20:27
 **/
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value = "修改密码")
public class ResetPwdParam {
    @ApiModelProperty(value = "账号")
    @NotBlank(message = "账号不能为空")
    @Length(max = 50, message = "账号长度不能超过{max}")
    private String account;

    @ApiModelProperty(value = "邮箱")
    @NotBlank(message = "邮箱不能为空")
    @Email(message = "邮箱格式错误")
    private String email;

    @NotBlank(message = "密码不能为空")
    @Length(min = 6, max = 50, message = "密码长度必须在{min} ~ {max}之间")
    @ApiModelProperty(value = "新密码")
    private String password;

    @ApiModelProperty(value = "验证码")
    @NotBlank(message = "验证码不能为空")
    private String code;
}
