package com.xiyou.main.params.exam;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * @program: multi-module
 * @description:
 * @author: tangcan
 * @create: 2019-07-10 20:23
 **/
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value = "资源参数")
public class ResourcesParam extends PageParam {

    @ApiModelProperty(value = "资源类型", hidden = true)
    private Integer type;

    @ApiModelProperty(value = "文件名")
    private String keyword;
}
