package com.xiyou.main.params.exam;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.hibernate.validator.constraints.Range;

/**
 * @program: multi-module
 * @description: 用户信息
 * @author: tangcan
 * @create: 2019-06-24 19:20
 **/
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value = "用户参数")
public class UserParam extends PageParam {
    @ApiModelProperty(value = "执行查询操作的用户id", hidden = true)
    private Integer userId;

    @ApiModelProperty(value = "获取的用户角色的id", hidden = true)
    private Integer roleId;

    @ApiModelProperty(value = "是否禁止登录")
    @Range(min = 0, max = 1, message = "禁止登录状态只能为{min}或{max}")
    private Integer forbid;

    @ApiModelProperty(value = "学校名称")
    private String schoolName;

    @ApiModelProperty(value = "账号或姓名的关键字")
    private String keyword;
}
