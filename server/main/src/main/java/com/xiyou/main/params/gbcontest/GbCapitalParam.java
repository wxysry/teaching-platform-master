package com.xiyou.main.params.gbcontest;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * @author 吕辉耀
 * @PROJECT_NAME: teaching-platform-master-new
 * @Description:
 * @creat 2023-07-26 17
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value = "注资参数")
public class GbCapitalParam {

    @ApiModelProperty(value = "竞赛id")
    @NotNull(message = "竞赛id不能为空")
    private Integer contestId;

    @ApiModelProperty(value = "学生id")
    @NotNull(message = "学生id")
    private Integer studentId;

    @ApiModelProperty(value = "注资金额")
    @NotNull(message = "注资金额")
    private Integer addCapital;

    List<MaterialIdAndMaterialNum> materialIdAndMaterialNumList;

    @NotNull
    @Data
    public static class MaterialIdAndMaterialNum{
        @NotNull
        @ApiModelProperty(value ="材料id")
        Integer materialId;
        @NotNull
        @ApiModelProperty(value ="材料数量")
        Integer materialNum;

        String cnName;
    }
}
