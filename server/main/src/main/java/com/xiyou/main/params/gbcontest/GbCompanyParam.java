package com.xiyou.main.params.gbcontest;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import java.util.ArrayList;
import java.util.List;

/**
 * @author dingyoumeng
 * @since 2019/08/18
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value = "企业信息参数")
public class GbCompanyParam {

    @ApiModelProperty(value ="竞赛id")
    Integer contestId;

    @ApiModelProperty(value ="组号")
    Integer studentId;

    @ApiModelProperty(value ="查询类型")
    String type;

}
