package com.xiyou.main.params.gbcontest;


import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;


@Data
@Accessors(chain = true)
@ApiModel(value = "用于设置各回合时间", description = "")
public class GbContestParam {

    @ApiModelProperty(value = "竞赛id")
    private Integer contestId;

    @ApiModelProperty(value = "回合倒计时，用于设置各个回合的时间")
    private String countDown;
}
