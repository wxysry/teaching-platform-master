package com.xiyou.main.params.gbcontest;


import com.xiyou.main.entity.gbcontest.GbCtBalance;
import com.xiyou.main.entity.gbcontest.GbCtCharges;
import com.xiyou.main.entity.gbcontest.GbCtFinancialTarget;
import com.xiyou.main.entity.gbcontest.GbCtProfitChart;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="填写四张报表", description="")
public class GbFillReportParam {
    private GbCtCharges charges;
    private GbCtProfitChart profitChart;
    private GbCtBalance balance;
    private GbCtFinancialTarget financialTarget;
}
