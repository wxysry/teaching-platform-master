package com.xiyou.main.params.gbcontest;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotNull;

/**
 * @program: multi-module
 * @description: iso投资
 * @author: tangcan
 * @create: 2019-09-01 13:16
 **/
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value = "iso投资")
public class GbISOInvestParam {
    @ApiModelProperty(value = "竞赛id")
    @NotNull(message = "竞赛id不能为空")
    private Integer contestId;

    @ApiModelProperty(value = "当前时间")
    @NotNull(message = "当前时间不能为空")
    private Integer date;

    @ApiModelProperty(value = "竞赛id", hidden = true)
    private Integer studentId;

    @ApiModelProperty(value = "选择的iso的id")
    private Integer isoId;
}
