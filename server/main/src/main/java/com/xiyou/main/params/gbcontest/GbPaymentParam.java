package com.xiyou.main.params.gbcontest;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotNull;

/**
 * @author dingyoumeng
 * @since 2019/08/18
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value = "付款参数")
public class GbPaymentParam {

    @NotNull
    @ApiModelProperty(value ="数据id")
    Integer id;

    @NotNull
    @ApiModelProperty(value ="竞赛Id")
    Integer contestId;

    @NotNull
    @ApiModelProperty(value ="当前时间")
    Integer date;

}
