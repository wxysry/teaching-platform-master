package com.xiyou.main.params.newcontest;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotNull;

/**
 * @author dingyoumeng
 * @since 2019/08/18
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value = "贴现参数")
public class NewDiscountParam {

    @NotNull(message = "id不能为null")
    @ApiModelProperty(value ="数据id")
    Integer id;

    @NotNull(message = "竞赛id不能为null")
    @ApiModelProperty(value ="竞赛Id")
    Integer contestId;

    @NotNull(message = "时间不能为null")
    @ApiModelProperty(value ="当前时间")
    Integer date;

    @NotNull(message = "贴现金额不能为null")
    @ApiModelProperty(value ="贴现额")
    Integer fee;

    @NotNull(message = "账单类型不能为null")
    @ApiModelProperty(value ="账单类型")
    Integer type;

}
