package com.xiyou.main.params.newcontest;


import com.xiyou.main.entity.contest.CtCharges;
import com.xiyou.main.entity.gbcontest.GbCtBalance;
import com.xiyou.main.entity.gbcontest.GbCtCharges;
import com.xiyou.main.entity.gbcontest.GbCtFinancialTarget;
import com.xiyou.main.entity.gbcontest.GbCtProfitChart;
import com.xiyou.main.entity.newcontest.NewCtBalance;
import com.xiyou.main.entity.newcontest.NewCtCharges;
import com.xiyou.main.entity.newcontest.NewCtFinancialTarget;
import com.xiyou.main.entity.newcontest.NewCtProfitChart;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="填写四张报表", description="")
public class NewFillReportParam {
    private NewCtCharges charges;
    private NewCtProfitChart profitChart;
    private NewCtBalance balance;
    private NewCtFinancialTarget financialTarget;
}
