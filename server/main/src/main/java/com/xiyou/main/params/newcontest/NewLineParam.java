package com.xiyou.main.params.newcontest;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.models.auth.In;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * @author 吕辉耀
 * @PROJECT_NAME: teaching-platform-master-new
 * @Description:
 * @creat 2023-06-29 13
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value = "市场研发")
public class NewLineParam {
    @ApiModelProperty(value = "竞赛id")
    @NotNull(message = "竞赛id不能为空")
    private Integer contestId;

    @ApiModelProperty(value = "班次id")
    @NotNull(message = "班次id")
    private Integer classesId;

    @ApiModelProperty(value = "当前时间")
    @NotNull(message = "当前时间不能为空")
    private Integer currentTime;

    @ApiModelProperty(value = "生产线id")
    private Integer lineId;

    @ApiModelProperty(value = "实际产量")
    private Integer realProduction;

    List<Integer> ordinaryWorkerList;

    List<Integer> seniorWorkerList;
}
