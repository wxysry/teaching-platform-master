package com.xiyou.main.params.newcontest;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotNull;

/**
 * @author 吕辉耀
 * @PROJECT_NAME: teaching-platform-master-new
 * @Description:
 * @creat 2023-06-26 17
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value = "产品出售参数")
public class NewSellMaterialParam {

    @NotNull
    @ApiModelProperty(value ="竞赛Id")
    Integer contestId;

    @NotNull
    @ApiModelProperty(value ="当前时间")
    Integer date;

    @ApiModelProperty(value = "材料id")
    Integer imCmId;

    @ApiModelProperty(value = "出售数量")
    Integer imNum;

    @ApiModelProperty(value = "入库日期")
    Integer inInventoryDate;

    @ApiModelProperty(value = "真实成本")
    Integer materialPrice;


}

