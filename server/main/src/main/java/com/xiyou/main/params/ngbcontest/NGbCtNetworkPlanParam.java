package com.xiyou.main.params.ngbcontest;

import com.xiyou.main.entity.ngbcontest.NGbCtRetailApply;
import com.xiyou.main.params.exam.PageParam;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * @author 吕辉耀
 * @PROJECT_NAME: teaching-platform-master-new
 * @Description:
 * @creat 2023-07-26 17
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value = "提交网络营销方案参数")
public class NGbCtNetworkPlanParam extends PageParam {

    @ApiModelProperty(value = "竞赛ID")
    private Integer contestId;

    @ApiModelProperty(value = "时间")
    private Integer currentTime;


    @ApiModelProperty(value = "新媒体广告投放金额")
    private Integer adAmount;


    @ApiModelProperty(value = "零售市场投放")
    private List<NGbCtRetailApply> applyList;




}
