package com.xiyou.main.params.ngbcontest;

import com.xiyou.main.params.exam.PageParam;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * @author 吕辉耀
 * @PROJECT_NAME: teaching-platform-master-new
 * @Description:
 * @creat 2023-07-26 17
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value = "选单查询参数")
public class NGbCtRetailOrderParam extends PageParam {

    @ApiModelProperty(value = "竞赛id")
    @NotNull(message = "竞赛id不能为空")
    private Integer contestId;

    @ApiModelProperty(value = "季度")
    private Integer date;

}
