package com.xiyou.main.params.ngbcontest;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * @author 吕辉耀
 * @PROJECT_NAME: teaching-platform-master-new
 * @Description:
 * @creat 2023-06-28 11
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value = "数字化参数")
public class NGbDigitalizeParam {

    @NotNull
    @ApiModelProperty(value ="竞赛id")
    Integer contestId;

    @ApiModelProperty(value ="竞赛id")
    Integer userId;

    @NotNull
    List<LineIdAndCpId> list;

    @NotNull
    @ApiModelProperty(value ="当前时间")
    Integer currentTime;

    @NotNull
    @ApiModelProperty(value = "班次id")
    private Integer classesId;

    @NotNull
    @Data
    public static class LineIdAndCpId{
        @NotNull
        @ApiModelProperty(value ="产线id")
        Integer lineId;
        @NotNull
        @ApiModelProperty(value ="产品id")
        Integer cpId;
    }
}
