package com.xiyou.main.params.ngbcontest;


import com.xiyou.main.entity.ngbcontest.NGbCtBalance;
import com.xiyou.main.entity.ngbcontest.NGbCtCharges;
import com.xiyou.main.entity.ngbcontest.NGbCtFinancialTarget;
import com.xiyou.main.entity.ngbcontest.NGbCtProfitChart;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="填写四张报表", description="")
public class NGbFillReportParam {
    private NGbCtCharges charges;
    private NGbCtProfitChart profitChart;
    private NGbCtBalance balance;
    private NGbCtFinancialTarget financialTarget;
}
