package com.xiyou.main.params.ngbcontest;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotNull;

/**
 * @author dingyoumeng
 * @since 2019/09/01
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value = "市场研发")
public class NGbMarketYfParam {

    @ApiModelProperty(value = "竞赛id")
    @NotNull(message = "竞赛id不能为空")
    private Integer contestId;

    @ApiModelProperty(value = "当前时间")
    @NotNull(message = "当前时间不能为空")
    private Integer date;

    @ApiModelProperty(value = "选择的market的id")
    private Integer marketId;
}
