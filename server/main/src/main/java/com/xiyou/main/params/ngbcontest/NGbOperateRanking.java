package com.xiyou.main.params.ngbcontest;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.math.BigDecimal;

/**
 * @author dingyoumeng
 * @since 2019/08/18
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value = "企业经营排行")
public class NGbOperateRanking {

    @ApiModelProperty(value ="竞赛id")
    Integer contestId;

    @ApiModelProperty(value ="学生ID")
    Integer studentId;


    @ApiModelProperty(value ="排名")
    Integer rank;

    @ApiModelProperty(value ="企业名")
    String groupNum;

    @ApiModelProperty(value ="权益值")
    Integer bsTotalEquity;

    @ApiModelProperty(value ="商誉")
    Integer sy;

    @ApiModelProperty(value ="数字化得分")
    Integer szh;

    @ApiModelProperty(value ="扣分")
    Integer kf;

    @ApiModelProperty(value ="碳中和率")
    BigDecimal neutralizaRate;

    @ApiModelProperty(value ="得分")
    Integer totalScore;

}
