package com.xiyou.main.params.ngbcontest;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotNull;

/**
 * @author 吕辉耀
 * @PROJECT_NAME: teaching-platform-master-new
 * @Description:
 * @creat 2023-06-26 17
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value = "产品出售参数")
public class NGbSellProductParam {

    @NotNull
    @ApiModelProperty(value ="竞赛Id")
    Integer contestId;

    @NotNull
    @ApiModelProperty(value ="当前时间")
    Integer date;

    @ApiModelProperty(value = "产品id")
    Integer ipCpId;

    @ApiModelProperty(value = "出售数量")
    Integer num;

    @ApiModelProperty(value = "入库日期")
    Integer inventoryDate;


    @ApiModelProperty(value = "特征编号")
    String featureNum;


    @ApiModelProperty(value = "真实成本")
    Integer realCost;


}

