package com.xiyou.main.params.ngbcontest;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotNull;

/**
 * @author dingyoumeng
 * @since 2019/08/29
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value = "紧急采购参数")
public class NGbUrgentBuyParam {

    @ApiModelProperty(value ="数据id")
    Integer id;

    @NotNull
    @ApiModelProperty(value ="竞赛Id")
    Integer contestId;

    @NotNull
    @ApiModelProperty(value ="当前时间")
    Integer date;

    @ApiModelProperty(value = "采购的材料id")
    Integer ipCpId;

    @ApiModelProperty(value = "采购的产品id")
    Integer imCmId;

    @ApiModelProperty(value = "采购数量")
    Integer num;



    @ApiModelProperty(value = "特征编码")
    String featureNum;

    @ApiModelProperty(value = "特征名称")
    String featureName;


    @ApiModelProperty(value = "紧急采购价格")
    private Integer urgentPrice;


}
