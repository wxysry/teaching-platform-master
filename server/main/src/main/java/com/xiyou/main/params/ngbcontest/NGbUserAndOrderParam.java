package com.xiyou.main.params.ngbcontest;

import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * @program: multi-module
 * @description: 用户和订单
 * @author: tangcan
 * @create: 2019-08-29 15:11
 **/
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value = "用户和订单")
public class NGbUserAndOrderParam {
//    @ApiModelProperty(value = "考试id")
//    @NotNull(message = "考试id不能为空")
//    private Integer contestId;
//
//    @ApiModelProperty(value = "学生id", hidden = true)
//    private Integer studentId;
//
//    @ApiModelProperty(value = "学生账号", hidden = true)
//    private String account;
//
//    @ApiModelProperty(value = "当前时间")
//    private Integer date;
//
//    @ApiModelProperty(value = "当前年份", hidden = true)
//    private Integer year;
//
//    @ApiModelProperty(value = "当前季度", hidden = true)
//    private Integer quarterly;
//
//    @ApiModelProperty(value = "回合数，为空表示第一回合")
//    private Integer round;
//
//    @ApiModelProperty(value = "市场+产品，如：本地P1")
//    private String marketProduct;
//
//    @ApiModelProperty(value = "选单id")
//    private Integer chooseId;
}
