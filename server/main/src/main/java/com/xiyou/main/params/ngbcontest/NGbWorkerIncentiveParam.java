package com.xiyou.main.params.ngbcontest;

import com.xiyou.main.entity.ngbcontest.NGbCtWorker;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotNull;
import java.util.List;


@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value = "工人培训")
public class NGbWorkerIncentiveParam {

    @ApiModelProperty(value = "竞赛id")
    private Integer contestId;

    @ApiModelProperty(value = "时间")
    private Integer currentTime;


    @ApiModelProperty(value = "工人")
    private NGbCtWorker nGbCtWorker;


    @ApiModelProperty(value = "奖励类型,JL1 激励 , JL2 涨薪")
    private String type;


    @ApiModelProperty(value = "激励金额")
    private Integer amount;


    @ApiModelProperty(value = "工人类型")
    private String  recruitNum;;

    @ApiModelProperty(value = "效率")
    private Integer multBonus;

}
