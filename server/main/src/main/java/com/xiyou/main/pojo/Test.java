package com.xiyou.main.pojo;

import lombok.Data;
import org.hibernate.validator.constraints.Length;
import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.*;


/**
 * @program: multi-module
 * @description:
 * @author: tangcan
 * @create: 2019-06-16 18:10
 **/
@Data
public class Test {
    @NotBlank(message = "userName不允许为空")
    @Length(min = 2, max = 5, message = "userName长度必须在{min}-{max}之间")
    private String userName;

    @NotBlank(message="年龄不能为空")
    @Pattern(regexp="^[0-9]{1,2}$",message="年龄不正确")
    private String userAge;

    @NotNull(message = "price不允许为空")
    @DecimalMin(value = "15.5", message = "价格不能低于 {value}")
    private Double price;

    @AssertFalse(message = "必须为false")
    private Boolean isFalse;
    /**
     * 如果是空，则不校验，如果不为空，则校验
     */
    @Pattern(regexp="^[0-9]{4}-[0-9]{2}-[0-9]{2}$",message="出生日期格式不正确")
    private String birthday;
}
