package com.xiyou.main.pojo.contest;

import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.metadata.BaseRowModel;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 *
 * </p>
 *
 * @author tangcan
 * @since 2019-07-22
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class CtGzAdModel extends BaseRowModel {

    @ExcelProperty(value = "题号", index = 0)
    private Integer subjectNum;

    @ExcelProperty(value = "年份", index = 1)
    private Integer year;

    @ExcelProperty(value = "组号", index = 2)
    private String groupNum;

    @ExcelProperty(value = "本地区P1", index = 3)
    private String localP1;

    @ExcelProperty(value = "区域P1", index = 4)
    private String regionalP1;

    @ExcelProperty(value = "国内P1", index = 5)
    private String nationalP1;

    @ExcelProperty(value = "亚洲P1", index = 6)
    private String asianP1;

    @ExcelProperty(value = "国际P1", index = 7)
    private String internationalP1;

    @ExcelProperty(value = "本地P2", index = 8)
    private String localP2;

    @ExcelProperty(value = "区域P2", index = 9)
    private String regionalP2;

    @ExcelProperty(value = "国内P2", index = 10)
    private String nationalP2;

    @ExcelProperty(value = "亚洲P2", index = 11)
    private String asianP2;

    @ExcelProperty(value = "国际P2", index = 12)
    private String internationalP2;

    @ExcelProperty(value = "本地P3", index = 13)
    private String localP3;

    @ExcelProperty(value = "区域P3", index = 14)
    private String regionalP3;

    @ExcelProperty(value = "国内P3", index = 15)
    private String nationalP3;

    @ExcelProperty(value = "亚洲P3", index = 16)
    private String asianP3;

    @ExcelProperty(value = "国际P3", index = 17)
    private String internationalP3;

    @ExcelProperty(value = "本地P4", index = 18)
    private String localP4;

    @ExcelProperty(value = "区域P4", index = 19)
    private String regionalP4;

    @ExcelProperty(value = "国内P4", index = 20)
    private String nationalP4;

    @ExcelProperty(value = "亚洲P4", index = 21)
    private String asianP4;

    @ExcelProperty(value = "国际P4", index = 22)
    private String internationalP4;

    @ExcelProperty(value = "本地P5", index = 23)
    private String localP5;

    @ExcelProperty(value = "区域P5", index = 24)
    private String regionalP5;

    @ExcelProperty(value = "国内P4", index = 25)
    private String nationalP5;

    @ExcelProperty(value = "亚洲P5", index = 26)
    private String asianP5;

    @ExcelProperty(value = "国际P5", index = 27)
    private String internationalP5;
}
