package com.xiyou.main.pojo.contest;

import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.metadata.BaseRowModel;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * @author xingzi
 * @date 2019 07 22  21:30
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class CtGzCsModel extends BaseRowModel {

    @ExcelProperty(value = "题库号", index = 0)
    private Integer subjectNumber;

    @ExcelProperty(value = "违约金比例", index = 1)
    private String punish;

    @ExcelProperty(value = "出售产成品", index = 2)
    private String inventoryDiscountProduct;

    @ExcelProperty(value = "出售原材料", index = 3)
    private String inventoryDiscountMaterial;

    @ExcelProperty(value = "贷款倍数", index = 4)
    private String loanCeiling;

    @ExcelProperty(value = "长贷利息", index = 5)
    private String longLoanInterests;

    @ExcelProperty(value = "短贷利息", index = 6)
    private String shortLoanInterests;

    @ExcelProperty(value = "1、2账期贴现", index = 7)
    private String discount12;

    @ExcelProperty(value = "3、4账期贴现", index = 8)
    private String discount34;

    @ExcelProperty(value = "初始现金", index = 9)
    private String cash;

    @ExcelProperty(value = "管理费", index = 10)
    private String overhaul;

    @ExcelProperty(value = "最小得单金额", index = 11)
    private String min;

    @ExcelProperty(value = "信息费", index = 12)
    private String information;

    @ExcelProperty(value = "紧急采购原材料", index = 13)
    private String emergenceMultipleMaterial;

    @ExcelProperty(value = "紧急采购产品", index = 14)
    private String emergenceMultipleProduct;

    @ExcelProperty(value = "所得税率", index = 15)
    private String incomeTax;

    @ExcelProperty(value = "最大经营年限", index = 16)
    private String manageMax;

    @ExcelProperty(value = "最大长贷年限", index = 17)
    private String longLoanMax;

    @ExcelProperty(value = "选单时间", index = 18)
    private String orderTimeout;

    @ExcelProperty(value = "间谍时间", index = 19)
    private String spyPeriod;

    @ExcelProperty(value = "间谍等待", index = 20)
    private Integer spyInterval;

    @ExcelProperty(value = "首单增加时间", index = 21)
    private Integer orderFixTime;

    @ExcelProperty(value = "拍卖时间", index = 22)
    private Integer auctionTimeout;

    @ExcelProperty(value = "拍卖张数", index = 23)
    private Integer auctionThread;

    @ExcelProperty(value = "厂房最大数", index = 24)
    private Integer maxWorkshop;
}
