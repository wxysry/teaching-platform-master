package com.xiyou.main.pojo.contest;

import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.metadata.BaseRowModel;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 *
 * </p>
 *
 * @author tangcan
 * @since 2019-07-22
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class CtGzIsoModel extends BaseRowModel {

    @ExcelProperty(value = "题库号", index = 0)
    private Integer subjectNumber;

    @ExcelProperty(value = "ISO规则表 ISO类型", index = 1)
    private Integer ciId;

    @ExcelProperty(value = "ISO名称", index = 2)
    private String ciName;

    @ExcelProperty(value = "研发费用", index = 3)
    private Integer ciDevelopFee;

    @ExcelProperty(value = "研发周期", index = 4)
    private Integer ciDevelopDate;
}
