package com.xiyou.main.pojo.contest;

import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.metadata.BaseRowModel;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 *
 * </p>
 *
 * @author tangcan
 * @since 2019-07-22
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class CtGzMaterialModel extends BaseRowModel {


    @ExcelProperty(value = "题库号", index = 0)
    private Integer subjectNumber;

    @ExcelProperty(value = "原料规则表编号", index = 1)
    private Integer cmId;

    @ExcelProperty(value = "原料名称", index = 2)
    private String cnName;

    @ExcelProperty(value = "采购周期", index = 3)
    private Integer cmLeadDate;

    @ExcelProperty(value = "费用", index = 4)
    private Integer cmBuyFee;


}
