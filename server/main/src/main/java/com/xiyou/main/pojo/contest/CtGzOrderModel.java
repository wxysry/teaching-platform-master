package com.xiyou.main.pojo.contest;

import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.metadata.BaseRowModel;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 *
 * </p>
 *
 * @author tangcan
 * @since 2019-07-22
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class CtGzOrderModel extends BaseRowModel {

    private static final long serialVersionUID = 1L;

    @ExcelProperty(value = "题库号", index = 0)
    private Integer subjectNumber;

    @ExcelProperty(value = "市场订单 订单编号", index = 1)
    private String coId;

    @ExcelProperty(value = "年份", index = 2)
    private Integer date;

    @ExcelProperty(value = "市场", index = 3)
    private Integer cmId;

    @ExcelProperty(value = "产品", index = 4)
    private Integer cpId;

    @ExcelProperty(value = "数量", index = 5)
    private Integer num;

    @ExcelProperty(value = "总价", index = 6)
    private Integer totalPrice;

    @ExcelProperty(value = "交货期", index = 7)
    private Integer deliveryDate;

    @ExcelProperty(value = "账期", index = 8)
    private Integer paymentDate;

    @ExcelProperty(value = "ISO", index = 9)
    private String ciId;

    @ExcelProperty(value = "选走轮次", index = 10)
    private Integer xzRound;

    @ExcelProperty(value = "选走组号", index = 11)
    private String xzGroup;

}
