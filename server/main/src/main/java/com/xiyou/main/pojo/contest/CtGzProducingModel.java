package com.xiyou.main.pojo.contest;

import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.metadata.BaseRowModel;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 *
 * </p>
 *
 * @author tangcan
 * @since 2019-07-22
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class CtGzProducingModel extends BaseRowModel {

    @ExcelProperty(value = "题库号", index = 0)
    private Integer subjectNumber;

    @ExcelProperty(value = "生产规则表 产品编号", index = 1)
    private Integer cpId;

    @ExcelProperty(value = "产品名称", index = 2)
    private String cpName;

    @ExcelProperty(value = "原材料编号", index = 3)
    private Integer cpMid;

    @ExcelProperty(value = "所需数量", index = 4)
    private Integer cpNum;

}
