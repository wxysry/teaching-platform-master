package com.xiyou.main.pojo.contest;

import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.metadata.BaseRowModel;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 *
 * </p>
 *
 * @author tangcan
 * @since 2019-07-22
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class CtGzWorkshopModel extends BaseRowModel {

    @ExcelProperty(value = "题库号", index = 0)
    private Integer subjectNum;

    @ExcelProperty(value = "编号", index = 1)
    private Integer cwId;

    @ExcelProperty(value = "厂房名称", index = 2)
    private String cwName;

    @ExcelProperty(value = "购买费用", index = 3)
    private Integer cwBuyFee;

    @ExcelProperty(value = "租金", index = 4)
    private Integer cwRentFee;

    @ExcelProperty(value = "售价", index = 5)
    private Integer cwSellFee;

    @ExcelProperty(value = "生产线容量", index = 6)
    private Integer cwCapacity;
}
