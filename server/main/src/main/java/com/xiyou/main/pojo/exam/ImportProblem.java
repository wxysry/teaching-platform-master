package com.xiyou.main.pojo.exam;

import cn.afterturn.easypoi.excel.annotation.Excel;
import com.xiyou.common.office.pojo.ImportExcel;
import com.xiyou.main.groups.Add;
import com.xiyou.main.groups.Import;
import com.xiyou.main.groups.Update;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

/**
 * @program: multi-module
 * @description: 试题导入
 * @author: tangcan
 * @create: 2019-07-06 21:21
 **/
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class ImportProblem extends ImportExcel {

    @Excel(name = "题型")
    @NotBlank(message = "题型不能为空")
    @Pattern(regexp = "^[0-9]{1,2}$", message = "题型填写不正确")
    private String problemTypeCode;

    @Excel(name = "题目")
    private String problemTitle;

    @Excel(name = "题目简介")
    @Length(max = 100, message = "简介长度不能超过{max}")
    private String intro;

    @Excel(name = "一级分组")
    @NotBlank(message = "一级分组不能为空")
    @Length(max = 50, message = "一级分组长度不能超过{max}")
    private String groupName;

    @Excel(name = "二级分组")
    @NotBlank(message = "二级分组不能为空")
    @Length(max = 50, message = "二级分组长度不能超过{max}")
    private String subgroupName;

    @Excel(name = "选项A")
    private String optionA;

    @Excel(name = "选项B")
    private String optionB;

    @Excel(name = "选项C")
    private String optionC;

    @Excel(name = "选项D")
    private String optionD;

    @Excel(name = "选项E")
    private String optionE;

    @Excel(name = "选项F")
    private String optionF;

    @Excel(name = "答案")
    private String answer;

    @Excel(name = "解析")
    private String analysis;

    @Excel(name = "评分要点")
    private String scorePoint;

    @Excel(name = "图片")
    private String pic;

    @Excel(name = "附件")
    private String attachment;
}
