package com.xiyou.main.pojo.exam;

import cn.afterturn.easypoi.excel.annotation.Excel;
import com.xiyou.common.office.pojo.ImportExcel;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;

/**
 * @program: multi-module
 * @description: 导入学生的信息
 * @author: tangcan
 * @create: 2019-06-26 10:12
 **/
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class ImportStudent extends ImportExcel {

    @Excel(name = "账号")
    @NotBlank(message = "账号不能为空")
    @Length(max = 50, message = "账号长度不能超过{max}")
    private String account;

    @Excel(name = "初始密码")
    @NotBlank(message = "初始密码不能为空")
    @Length(max = 50, message = "初始密码长度不能超过{max}")
    private String password;

    @Excel(name = "姓名")
    @NotBlank(message = "姓名不能为空")
    @Length(max = 50, message = "姓名长度不能超过{max}")
    private String name;

    @Excel(name = "邮箱")
//    @NotBlank(message = "邮箱不能为空")
//    @Email(message = "邮箱格式错误")
//    @Length(max = 100, message = "邮箱长度不能超过{max}")
    private String email;

    @Excel(name = "学校")
    @NotBlank(message = "学校名称不能为空")
    @Length(max = 50, message = "学校名称长度不能超过{max}")
    private String schoolName;
}
