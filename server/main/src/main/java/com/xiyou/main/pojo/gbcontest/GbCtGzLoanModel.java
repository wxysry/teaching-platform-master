package com.xiyou.main.pojo.gbcontest;

import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.metadata.BaseRowModel;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.math.BigDecimal;

@Data
@EqualsAndHashCode(callSuper = true)
public class GbCtGzLoanModel extends BaseRowModel {

    @ExcelProperty(value = "题库号", index = 0)
    private Integer subjectNum;

    @ExcelProperty(value = "贷款名称", index = 1)
    private String loanName;

    @ExcelProperty(value = "贷款编码", index = 2)
    private String loanNum;

    @ExcelProperty(value = "额度上限(倍)", index = 3)
    private BigDecimal loanMax;

    @ExcelProperty(value = "贷款时间(季)", index = 4)
    private Integer loanTime;

    @ExcelProperty(value = "还款方式", index = 5)
    private String loanRepayment;

    @ExcelProperty(value = "利率(%)", index = 6)
    private BigDecimal loanRate;
}