package com.xiyou.main.pojo.gbcontest;

import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.metadata.BaseRowModel;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

@Data
@EqualsAndHashCode(callSuper = true)
public class GbCtGzNumModel extends BaseRowModel {

    @ExcelProperty(value = "题库号", index = 0)
    private Integer subjectNum;

    @ExcelProperty(value = "岗位编码", index = 1)
    private String postNum;

    @ExcelProperty(value = "消耗金钱(元)", index = 2)
    private Integer consumeMoney;

    @ExcelProperty(value = "消耗时间(季)", index = 3)
    private Integer timeCostQuarter;
}