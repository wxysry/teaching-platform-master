package com.xiyou.main.pojo.gbcontest;

import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.metadata.BaseRowModel;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
public class GbCtGzProducingModel extends BaseRowModel {

    @ExcelProperty(value = "题库号", index = 0)
    private Integer subjectNumber;

    @ExcelProperty(value = "生产规则表id", index = 1)
    private Integer cpId;

    @ExcelProperty(value = "产品名称", index = 2)
    private String cpName;

    @ExcelProperty(value = "原材料规则id", index = 3)
    private Integer cpMid;

    @ExcelProperty(value = "所需数量", index = 4)
    private Integer cpNum;

}