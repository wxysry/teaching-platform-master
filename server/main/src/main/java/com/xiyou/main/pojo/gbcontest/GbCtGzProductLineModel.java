package com.xiyou.main.pojo.gbcontest;

import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.metadata.BaseRowModel;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

@Data
@EqualsAndHashCode(callSuper = true)
public class GbCtGzProductLineModel extends BaseRowModel {

    @ExcelProperty(value = "题库号", index = 0)
    private Integer subjectNum;

    @ExcelProperty(value = "生产线编号", index = 1)
    private Integer cplId;


    @ExcelProperty(value = "生产线名称", index = 2)
    private String cplName;

    @ExcelProperty(value = "总投资费用", index = 3)
    private Integer cplBuyFee;

    @ExcelProperty(value = "投资周期", index = 4)
    private Integer cplInstallDate;

    @ExcelProperty(value = "生产时间", index = 5)
    private Integer cplProduceDate;

    @ExcelProperty(value = "转产费用", index = 6)
    private Integer cplTransferFee;

    @ExcelProperty(value = "转产周期", index = 7)
    private Integer cplTransferDate;


    @ExcelProperty(value = "维修费（W/每年）", index = 8)
    private Integer cplMaintenanceFee;

    @ExcelProperty(value = "折旧费", index = 9)
    private Integer cplScarpFee;

    @ExcelProperty(value = "折旧年限", index = 10)
    private Integer cplDepDate;

    @ExcelProperty(value = "残值", index = 11)
    private Integer cplDepFee;

    @ExcelProperty(value = "需要高级工人", index = 12)
    private Integer cplSeniorWorker;

    @ExcelProperty(value = "需要普通工人", index = 13)
    private Integer cplOrdinaryWorker;

    @ExcelProperty(value = "基础产量", index = 14)
    private Integer cplProduction;

}
