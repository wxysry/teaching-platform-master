package com.xiyou.main.pojo.gbcontest;

import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.metadata.BaseRowModel;
import lombok.Data;
import lombok.EqualsAndHashCode;


import java.math.BigDecimal;

/**
 * @author wangxy
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class GbCtGzWorkerRecruitModel extends BaseRowModel {
    @ExcelProperty(value = "题库号", index = 0)
    private Integer subjectNum;

    @ExcelProperty(value = "名称", index = 1)
    private String recruitName;

    @ExcelProperty(value = "编码", index = 2)
    private String recruitNum;

    @ExcelProperty(value = "初始期望工资(元)", index = 3)
    private Integer initSalExpect;

    @ExcelProperty(value = "计件", index = 4)
    private Integer piece;

    @ExcelProperty(value = "每季度数量", index = 5)
    private Integer qtyPerQtr;

    @ExcelProperty(value = "倍数加成(%)", index = 6)
    private BigDecimal multBonus;
}