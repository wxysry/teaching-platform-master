package com.xiyou.main.pojo.newcontest;

import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.metadata.BaseRowModel;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.math.BigDecimal;

/**
 * <p>
 *
 * </p>
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class NewCtGzAdModel extends BaseRowModel {


    @ExcelProperty(value = "题号", index = 0)
    private Integer subjectNum;

    @ExcelProperty(value = "年份", index = 1)
    private Integer year;

    @ExcelProperty(value = "季度", index = 2)
    private Integer quarterly;

    @ExcelProperty(value = "组号", index = 3)
    private String groupNum;

    @ExcelProperty(value = "本地P1", index = 4)
    private BigDecimal localP1;

    @ExcelProperty(value = "区域P1", index = 5)
    private BigDecimal regionalP1;

    @ExcelProperty(value = "国内P1", index = 6)
    private BigDecimal nationalP1;

    @ExcelProperty(value = "亚洲P1", index = 7)
    private BigDecimal asianP1;

    @ExcelProperty(value = "国际P1", index = 8)
    private BigDecimal internationalP1;

    @ExcelProperty(value = "本地P2", index = 9)
    private BigDecimal localP2;

    @ExcelProperty(value = "区域P2", index = 10)
    private BigDecimal regionalP2;

    @ExcelProperty(value = "国内P2", index = 11)
    private BigDecimal nationalP2;

    @ExcelProperty(value = "亚洲P2", index = 12)
    private BigDecimal asianP2;

    @ExcelProperty(value = "国际P2", index = 13)
    private BigDecimal internationalP2;

    @ExcelProperty(value = "本地P3", index = 14)
    private BigDecimal localP3;

    @ExcelProperty(value = "区域P3", index = 15)
    private BigDecimal regionalP3;

    @ExcelProperty(value = "国内P3", index = 16)
    private BigDecimal nationalP3;

    @ExcelProperty(value = "亚洲P3", index = 17)
    private BigDecimal asianP3;

    @ExcelProperty(value = "国际P3", index = 18)
    private BigDecimal internationalP3;

    @ExcelProperty(value = "本地P4", index = 19)
    private BigDecimal localP4;

    @ExcelProperty(value = "区域P4", index = 20)
    private BigDecimal regionalP4;

    @ExcelProperty(value = "国内P4", index = 21)
    private BigDecimal nationalP4;

    @ExcelProperty(value = "亚洲P4", index = 22)
    private BigDecimal asianP4;

    @ExcelProperty(value = "国际P4", index = 23)
    private BigDecimal internationalP4;

    @ExcelProperty(value = "本地P5", index = 24)
    private BigDecimal localP5;

    @ExcelProperty(value = "区域P5", index = 25)
    private BigDecimal regionalP5;

    @ExcelProperty(value = "国内P5", index = 26)
    private BigDecimal nationalP5;

    @ExcelProperty(value = "亚洲P5", index = 27)
    private BigDecimal asianP5;

    @ExcelProperty(value = "国际P5", index = 28)
    private BigDecimal internationalP5;


}
