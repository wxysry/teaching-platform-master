package com.xiyou.main.pojo.newcontest;

import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.metadata.BaseRowModel;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.math.BigDecimal;

/**
 * @author wangxy
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class NewCtGzClassesModel extends BaseRowModel {

    @ExcelProperty(value = "题库号", index = 0)
    private Integer subjectNum;

    @ExcelProperty(value = "班次名称", index = 1)
    private String classesName;

    @ExcelProperty(value = "班次编码", index = 2)
    private String classesNum;

    @ExcelProperty(value = "产量加成(倍)", index = 3)
    private BigDecimal outputMulti;

    @ExcelProperty(value = "效率损失(%)", index = 4)
    private BigDecimal efficiencyLoss;

}
