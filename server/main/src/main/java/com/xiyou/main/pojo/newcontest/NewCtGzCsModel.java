package com.xiyou.main.pojo.newcontest;

import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.metadata.BaseRowModel;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 *
 * </p>
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class NewCtGzCsModel extends BaseRowModel {

    @ExcelProperty(value = "题号", index = 0)
    private Integer subjectNumber;

    @ExcelProperty(value = "违约金比例", index = 1)
    private Integer punish;

    @ExcelProperty(value = "出售产成品", index = 2)
    private Integer inventoryDiscountProduct;

    @ExcelProperty(value = "出售原材料", index = 3)
    private Integer inventoryDiscountMaterial;

    @ExcelProperty(value = "贷款倍数", index = 4)
    private Integer loanCeiling;

    @ExcelProperty(value = "初始现金", index = 5)
    private Integer cash;

    @ExcelProperty(value = "管理费", index = 6)
    private Integer overhaul;

    @ExcelProperty(value = "最小得单金额", index = 7)
    private Integer min;

    @ExcelProperty(value = "得单公差", index = 8)
    private Integer tolerance;

    @ExcelProperty(value = "信息费", index = 9)
    private Integer information;

    @ExcelProperty(value = "紧急采购原材料", index = 10)
    private Integer emergenceMultipleMaterial;

    @ExcelProperty(value = "紧急采购产成品", index = 11)
    private Integer emergenceMultipleProduct;

    @ExcelProperty(value = "所得税率", index = 12)
    private Integer incomeTax;

    @ExcelProperty(value = "选单时间", index = 13)
    private Integer orderTimeout;

    @ExcelProperty(value = "首单增加时间", index = 14)
    private Integer orderFixTime;

    @ExcelProperty(value = "拍卖时间", index = 15)
    private Integer auctionTimeout;

    @ExcelProperty(value = "拍卖张数", index = 16)
    private Integer auctionThread;

    @ExcelProperty(value = "生产线最大数量", index = 17)
    private Integer maxLine;

}
