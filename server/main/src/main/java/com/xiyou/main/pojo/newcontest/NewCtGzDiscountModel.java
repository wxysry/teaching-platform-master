package com.xiyou.main.pojo.newcontest;

import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.metadata.BaseRowModel;
import lombok.Data;
import lombok.EqualsAndHashCode;


import java.math.BigDecimal;

@Data
@EqualsAndHashCode(callSuper = true)
public class NewCtGzDiscountModel extends BaseRowModel {

    @ExcelProperty(value = "题库号", index = 0)
    private Integer subjectNum;

    @ExcelProperty(value = "贷款名称", index = 1)
    private String discountName;

    @ExcelProperty(value = "贷款编码", index = 2)
    private String discountNum;

    @ExcelProperty(value = "收款期(季)", index = 3)
    private Integer discountTime;

    @ExcelProperty(value = "贴息(%)", index = 4)
    private BigDecimal discountInterest;
}