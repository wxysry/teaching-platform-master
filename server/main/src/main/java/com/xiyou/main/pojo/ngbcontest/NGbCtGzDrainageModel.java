package com.xiyou.main.pojo.ngbcontest;

import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.metadata.BaseRowModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.math.BigDecimal;

@Data
@EqualsAndHashCode(callSuper = true)
public class NGbCtGzDrainageModel extends BaseRowModel {

    @ExcelProperty(value = "题库号", index = 0)
    private Integer subjectNum;


    @ExcelProperty(value = "引流名称", index = 1)
    private String drainageName;

    @ExcelProperty(value = "引流参数", index = 2)
    private BigDecimal drainageParam;

}