package com.xiyou.main.pojo.ngbcontest;

import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.metadata.BaseRowModel;
import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = true)
@Data
public class NGbCtGzMarketModel extends BaseRowModel {

    @ExcelProperty(value = "题库号", index = 0)
    private Integer subjectNumber;

    @ExcelProperty(value = "市场规则表编号", index = 1)
    private Integer cmId;

    @ExcelProperty(value = "市场名称", index = 2)
    private String cmName;

    @ExcelProperty(value = "研发费用", index = 3)
    private Integer cmDevelopFee;

    @ExcelProperty(value = "研发周期", index = 4)
    private Integer cmDevelopDate;
}