package com.xiyou.main.pojo.ngbcontest;

import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.metadata.BaseRowModel;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class NGbCtGzNumModel extends BaseRowModel {

    @ExcelProperty(value = "题库号", index = 0)
    private Integer subjectNum;

    @ExcelProperty(value = "数字化名称", index = 1)
    private String postName;

    @ExcelProperty(value = "数字化类型", index = 2)
    private String postNum;

    @ExcelProperty(value = "消耗金钱(元)", index = 3)
    private Integer consumeMoney;

    @ExcelProperty(value = "消耗时间(季)", index = 4)
    private Integer timeCostQuarter;
}