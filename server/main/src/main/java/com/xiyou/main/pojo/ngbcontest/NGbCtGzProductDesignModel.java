package com.xiyou.main.pojo.ngbcontest;

import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.metadata.BaseRowModel;
import lombok.Data;
import lombok.EqualsAndHashCode;


/**
 * @author wangxy
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class NGbCtGzProductDesignModel extends BaseRowModel {
    @ExcelProperty(value = "题库号", index = 0)
    private Integer subjectNum;

    @ExcelProperty(value = "特征名称", index = 1)
    private String featureName;

    @ExcelProperty(value = "编码", index = 2)
    private String designNum;

    @ExcelProperty(value = "成本加成", index = 3)
    private Integer costMarkup;

    @ExcelProperty(value = "设计费用", index = 4)
    private Integer designCost;

    @ExcelProperty(value = "升级单位成本", index = 5)
    private Integer unitCostUpgrade;

    @ExcelProperty(value = "初始值", index = 6)
    private Integer initialValue;

    @ExcelProperty(value = "上限", index = 7)
    private Integer upperLimit;
}
