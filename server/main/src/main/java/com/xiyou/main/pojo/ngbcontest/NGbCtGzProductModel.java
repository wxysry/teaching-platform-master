package com.xiyou.main.pojo.ngbcontest;

import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.metadata.BaseRowModel;
import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = true)
@Data
public class NGbCtGzProductModel extends BaseRowModel {

    @ExcelProperty(value = "题库号", index = 0)
    private Integer subjectNumber;

    @ExcelProperty(value = "产品规则表编号", index = 1)
    private Integer cpId;

    @ExcelProperty(value = "名称", index = 2)
    private String cpName;

    @ExcelProperty(value = "开发费用", index = 3)
    private Integer cpProcessingFee;

    @ExcelProperty(value = "开发周期", index = 4)
    private Integer cpDevelopDate;

    @ExcelProperty(value = "加工费", index = 5)
    private Integer cpDevelopFee;

    @ExcelProperty(value = "直接成本", index = 6)
    private Integer cpDirectCost;

    @ExcelProperty(value = "单次生产碳排放", index = 7)
    private Integer cpCarbon;

}