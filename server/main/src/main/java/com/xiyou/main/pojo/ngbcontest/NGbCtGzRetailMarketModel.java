package com.xiyou.main.pojo.ngbcontest;

import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.metadata.BaseRowModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class NGbCtGzRetailMarketModel extends BaseRowModel {

    @ExcelProperty(value = "题库号", index = 0)
    private Integer subjectNumber;

    @ExcelProperty(value = "年份", index = 1)
    private Integer year;

    @ExcelProperty(value = "季度", index = 2)
    private Integer quarterly;

    @ExcelProperty(value = "产品", index = 3)
    private String cpId;

    @ExcelProperty(value = "特性编码", index = 4)
    private String designNum;

    @ExcelProperty(value = "承受单价", index = 5)
    private Integer supportPrice;

    @ExcelProperty(value = "购买数量", index = 6)
    private Integer num;

}