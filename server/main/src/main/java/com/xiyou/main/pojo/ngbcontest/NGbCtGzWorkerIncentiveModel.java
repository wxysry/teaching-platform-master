package com.xiyou.main.pojo.ngbcontest;

import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.metadata.BaseRowModel;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.math.BigDecimal;

/**
 * @author wangxy
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class NGbCtGzWorkerIncentiveModel extends BaseRowModel {
    @ExcelProperty(value = "题库号", index = 0)
    private Integer subjectNum;

    @ExcelProperty(value = "激励名称", index = 1)
    private String incentiveName;

    @ExcelProperty(value = "编码", index = 2)
    private String incentiveNum;

    @ExcelProperty(value = "提升效率比例(%)", index = 3)
    private BigDecimal efficiencyPercent;
}
