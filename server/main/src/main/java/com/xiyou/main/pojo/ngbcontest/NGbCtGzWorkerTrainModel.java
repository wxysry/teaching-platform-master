package com.xiyou.main.pojo.ngbcontest;

import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.metadata.BaseRowModel;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.math.BigDecimal;

@EqualsAndHashCode(callSuper = true)
@Data
public class NGbCtGzWorkerTrainModel extends BaseRowModel {
    @ExcelProperty(value = "题库号", index = 0)
    private Integer subjectNum;

    @ExcelProperty(value = "培训名称", index = 1)
    private String trainingName;

    @ExcelProperty(value = "消耗现金(元)", index = 2)
    private Integer cashCost;

    @ExcelProperty(value = "消耗时间(季)", index = 3)
    private Integer timeCostQuarter;

    @ExcelProperty(value = "原岗位", index = 4)
    private String originalPosition;

    @ExcelProperty(value = "培训后岗位", index = 5)
    private String trainedPosition;

    @ExcelProperty(value = "工资涨幅(%)", index = 6)
    private BigDecimal salaryIncreasePercent;
}
