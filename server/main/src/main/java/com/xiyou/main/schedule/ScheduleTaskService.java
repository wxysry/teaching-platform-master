package com.xiyou.main.schedule;


import com.xiyou.common.redis.utils.RedisCache;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;
import org.springframework.stereotype.Component;

import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.concurrent.*;

/**
 * @author 吕辉耀
 * @PROJECT_NAME: teaching-platform-master-new
 * @Description:
 * @creat 2023-07-23 17
 */
@Component
public class ScheduleTaskService {
    @Autowired
    private RedisCache redisCache;
//    private ScheduledExecutorService executorService = (ScheduledExecutorService) Executors.newFixedThreadPool(2);
    public Map<String, ScheduledFuture<?>> taskMap = new ConcurrentHashMap<>();
    public List<String> taskList = new CopyOnWriteArrayList<>();
    private final ThreadPoolTaskScheduler syncScheduler;
    public ScheduleTaskService(ThreadPoolTaskScheduler syncScheduler) {
        this.syncScheduler = syncScheduler;
    }



    public boolean add(String taskName,Runnable runnable,Integer time){
        //开始之前先将之前的任务删除
        this.stop(taskName);

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date());
        calendar.add(Calendar.SECOND,time);
        Date date = calendar.getTime();
        ScheduledFuture<?> schedule = syncScheduler.schedule(runnable, date);
        if(null != redisCache.get(taskName)){
            redisCache.delete(taskName);
        }
        redisCache.set(taskName,taskName,time,TimeUnit.SECONDS);
        taskMap.put(taskName,schedule);
        taskList.add(taskName);
        return true;
    }

    public boolean stop(String taskName){
        if(null == taskMap.get(taskName)){
            return false;
        }
        ScheduledFuture<?> scheduledFuture = taskMap.get(taskName);
        if(scheduledFuture!=null){
            scheduledFuture.cancel(true);
        }
        redisCache.delete(taskName);
        taskMap.remove(taskName);
        taskList.remove(taskName);
        return true;
    }
}
