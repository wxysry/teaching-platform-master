package com.xiyou.main.service.contest;

import com.xiyou.main.entity.contest.ContestGroup;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author tangcan
 * @since 2019-07-09
 */
public interface ContestGroupService extends IService<ContestGroup> {

    int insert(ContestGroup contestGroup);

    boolean checkNameExist(ContestGroup contestGroup);

    List<ContestGroup> getListByTeacher(Integer teacherId);
}
