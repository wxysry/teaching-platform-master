package com.xiyou.main.service.contest;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.xiyou.main.entity.contest.Contest;
import com.baomidou.mybatisplus.extension.service.IService;
import com.xiyou.main.params.contest.ContestParam;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author tangcan
 * @since 2019-08-25
 */
public interface ContestService extends IService<Contest> {

    Page<Contest> getPage(ContestParam contestParam);

    int getUnEndContest(Integer teacherId);

    int publish(Integer teacherId, Integer contestId);

    Page<Contest> getStudentContestPage(ContestParam contestParam);
}
