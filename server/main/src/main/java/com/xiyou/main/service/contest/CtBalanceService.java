package com.xiyou.main.service.contest;

import com.xiyou.main.entity.contest.CtBalance;
import com.baomidou.mybatisplus.extension.service.IService;
import com.xiyou.main.vo.contest.CashFolowEntity;

import java.util.List;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author tangcan
 * @since 2019-07-22
 */
public interface CtBalanceService extends IService<CtBalance> {

    /**
     * P8&P9 申请长贷款&申请短贷-获取最大贷款额度
     *
     * @param cashFolowEntity
     * @return
     */
    Integer getMaxLoanAmount(CashFolowEntity cashFolowEntity);

    CtBalance get(CtBalance balance);

    /**
     * @Author: tangcan
     * @Description: 最近一个所得税>0的年份的权益
     * @Param: [studentId, contestId]
     * @date: 2019/7/24
     */
    Integer getRecentYEarHaveEquity(Integer studentId, Integer contestId, Integer nowYear);

    /**
     * @Author: tangcan
     * @Description: max(之前年份所有者权益 ）
     * @Param: [studentId, contestId, currentYear]
     * @date: 2019/7/24
     */
    int maxTotalEquityPreYear(Integer studentId, Integer contestId, int currentYear);

    List<CtBalance> getCurrentYear(CtBalance balance);

    CtBalance getOne(CtBalance ctBalance);
}
