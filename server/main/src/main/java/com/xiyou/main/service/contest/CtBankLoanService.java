package com.xiyou.main.service.contest;

import com.xiyou.main.entity.contest.CtBalance;
import com.xiyou.main.entity.contest.CtBankLoan;
import com.baomidou.mybatisplus.extension.service.IService;
import com.xiyou.main.vo.contest.CashFolowEntity;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author tangcan
 * @since 2019-07-22
 */
public interface CtBankLoanService extends IService<CtBankLoan> {

    /**
     * bank_loan表中贷款类型为2且归还时间=当前时间的金额
     * @param cashFolowEntity
     * @return
     */
    Integer getCurrentMoney(CashFolowEntity cashFolowEntity);


    List<CtBankLoan> list(Integer studentId, Integer contestId);
}
