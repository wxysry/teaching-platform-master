package com.xiyou.main.service.contest;

import com.baomidou.mybatisplus.extension.service.IService;
import com.xiyou.main.entity.contest.CtCashflow;
import com.xiyou.main.vo.contest.CashFolowEntity;

import java.util.List;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author tangcan
 * @since 2019-07-22
 */
public interface CtCashflowService extends IService<CtCashflow> {


    /**
     * 用户当前现金
     *
     * @param studentId
     * @param contestId
     * @return
     */
    CashFolowEntity getStudentFinanceInfor(Integer studentId, Integer contestId);

    /**
     * student上一条现金
     *
     * @param cashFolowEntity
     * @return
     */
    Integer getStudentLastCash(CashFolowEntity cashFolowEntity);

    /**
     * 查出用户当前现金余额
     *
     * @param userId
     * @param contestId
     * @return
     */
    Integer getCash(Integer userId, Integer contestId);

    List<CtCashflow> list(Integer studentId, Integer contestId, Integer year, List<String> actionList);

}
