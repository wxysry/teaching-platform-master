package com.xiyou.main.service.contest;

import com.xiyou.main.entity.contest.CtCharges;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author tangcan
 * @since 2019-07-22
 */
public interface CtChargesService extends IService<CtCharges> {

    int addBySys(CtCharges charges);

    int updateInformation(Integer chargesId);

    /**
     * @Author: tangcan
     * @Description: 获取系统填入的
     * @Param: [studentId, contestId, year]
     * @date: 2019/9/10
    */
    CtCharges getSys(Integer studentId, Integer contestId, int year);

    List<CtCharges> getCurrentYear(CtCharges charges);

    Integer getInformation(Integer studentId, Integer contestId, int year, int isxt);

    CtCharges getOne(CtCharges ctCharges);
}
