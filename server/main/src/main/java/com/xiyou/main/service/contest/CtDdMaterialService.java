package com.xiyou.main.service.contest;

import com.xiyou.main.entity.contest.CtDdMaterial;
import com.baomidou.mybatisplus.extension.service.IService;
import com.xiyou.main.vo.contest.CashFolowEntity;

import java.util.List;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author tangcan
 * @since 2019-07-22
 */
public interface CtDdMaterialService extends IService<CtDdMaterial> {

    /**
     * 获取付现金额
     *
     * @param cashFolowEntity
     * @return
     */
    Integer getStudentPayMoney(CashFolowEntity cashFolowEntity);

    /**
     * 原料编号为相同的，将剩余时间为1的剩余数量更新到剩余时间为0的
     *
     * @param cashFolowEntity
     */
    void updateStudentDdMaterialListInfo(CashFolowEntity cashFolowEntity);

    void updateMaterial(List<CtDdMaterial> materialList);
}
