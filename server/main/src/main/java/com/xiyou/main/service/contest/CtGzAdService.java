package com.xiyou.main.service.contest;

import com.xiyou.main.entity.contest.CtGzAd;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author tangcan
 * @since 2019-07-25
 */
public interface CtGzAdService extends IService<CtGzAd> {

    String getGroupNum(Integer contestId, Integer year);
}
