package com.xiyou.main.service.contest;

import com.xiyou.main.entity.contest.CtGzCs;
import com.baomidou.mybatisplus.extension.service.IService;
import com.xiyou.main.vo.contest.CashFolowEntity;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author tangcan
 * @since 2019-07-22
 */
public interface CtGzCsService extends IService<CtGzCs> {

    CtGzCs getByContestId(Integer contestId);

    /**
     * gz_cs中的Short_Loan_Interests
     * @param cashFolowEntity
     * @return
     */
    Integer getShortLoanInterests(CashFolowEntity cashFolowEntity);

    /**
     * 查询contestId下 gz_cs.maxworkshop
     * @param contestId
     * @return
     */
    Integer getMaxWorkshop(Integer contestId);

    Integer getLongLoanMax(Integer contestId);

    CtGzCs getBySubjectNumber(Integer subjectNumber);
}
