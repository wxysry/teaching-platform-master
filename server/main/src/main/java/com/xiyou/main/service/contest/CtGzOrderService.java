package com.xiyou.main.service.contest;

import com.xiyou.main.entity.contest.CtGzOrder;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author tangcan
 * @since 2019-07-22
 */
public interface CtGzOrderService extends IService<CtGzOrder> {

}
