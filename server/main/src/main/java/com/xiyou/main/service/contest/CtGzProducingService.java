package com.xiyou.main.service.contest;

import com.xiyou.main.entity.contest.CtGzProducing;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author tangcan
 * @since 2019-07-22
 */
public interface CtGzProducingService extends IService<CtGzProducing> {


}
