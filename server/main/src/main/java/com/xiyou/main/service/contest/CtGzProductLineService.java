package com.xiyou.main.service.contest;

import com.xiyou.main.entity.contest.CtGzProductLine;
import com.baomidou.mybatisplus.extension.service.IService;
import com.xiyou.main.vo.contest.CashFolowEntity;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author tangcan
 * @since 2019-07-22
 */
public interface CtGzProductLineService extends IService<CtGzProductLine> {

    /**
     * 选择生产线类型
     * @param contestId
     * @return
     */
    List<CtGzProductLine> getProductTypeList(Integer contestId);

    /**
     * 该生产线的投资时间
     * @param cashFolowEntity
     * @return
     */
    Integer getProductLineInfo(CashFolowEntity cashFolowEntity);

    List<CtGzProductLine> getListByCplids(List<Integer> cplids);
}
