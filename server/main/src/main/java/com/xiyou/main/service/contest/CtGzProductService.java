package com.xiyou.main.service.contest;

import com.baomidou.mybatisplus.extension.service.IService;
import com.xiyou.main.entity.contest.CtGzProduct;
import com.xiyou.main.vo.contest.YfProductVO;
import java.util.List;


/**
 * <p>
 *  服务类
 * </p>
 *
 * @author tangcan
 * @since 2019-07-22
 */
public interface CtGzProductService extends IService<CtGzProduct> {

    /**
     * 查出产品研发列表
     * @param userId
     * @param contestId
     * @return
     */
    List<YfProductVO> listYfProduct(Integer userId, Integer contestId);

    /**
     * 产品名称
     * @param contestId
     * @return
     */
    List<String> getProductCPName(Integer contestId);


    List<CtGzProduct> getList(Integer contestId);

    List<CtGzProduct> getListByIds(List<Integer> cpIds);

}
