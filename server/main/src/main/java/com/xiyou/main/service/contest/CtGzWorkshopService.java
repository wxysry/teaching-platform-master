package com.xiyou.main.service.contest;

import com.xiyou.main.entity.contest.CtGzWorkshop;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author tangcan
 * @since 2019-07-22
 */
public interface CtGzWorkshopService extends IService<CtGzWorkshop> {

    List<CtGzWorkshop> getWorkshopInfo(Integer contestId);

}
