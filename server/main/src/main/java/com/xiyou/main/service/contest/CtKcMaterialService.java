package com.xiyou.main.service.contest;

import com.xiyou.main.entity.contest.CtKcMaterial;
import com.baomidou.mybatisplus.extension.service.IService;
import com.xiyou.main.vo.contest.CashFolowEntity;
import com.xiyou.main.vo.contest.MaterialVo;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author tangcan
 * @since 2019-07-22
 */
public interface CtKcMaterialService extends IService<CtKcMaterial> {


    void updateKcMaterial(CashFolowEntity cashFolowEntity);

    int getMaterialSum(Integer studentId, Integer contestId);

    //查看生产线所需要的材料
    List<CtKcMaterial> listNeed(Integer contestId, List<Integer> lineIds);

    //查出库存材料数量
    List<MaterialVo> listKc(Integer contestId, Integer userId);

    //查出材料库存的售出价格
    List<MaterialVo> listSellKc(Integer contestId, Integer userId);
}
