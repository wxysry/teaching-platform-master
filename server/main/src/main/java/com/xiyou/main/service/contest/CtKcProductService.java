package com.xiyou.main.service.contest;

import com.baomidou.mybatisplus.extension.service.IService;
import com.xiyou.main.entity.contest.CtKcProduct;
import com.xiyou.main.vo.contest.ProductVo;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author tangcan
 * @since 2019-07-22
 */
public interface CtKcProductService extends IService<CtKcProduct> {

    int getProductSum(Integer studentId, Integer contestId);

    List<ProductVo> listKc(Integer contestId, Integer userId);

    List<ProductVo> listSellKc(Integer contestId, Integer userId);
}
