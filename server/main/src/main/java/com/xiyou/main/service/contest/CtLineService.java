package com.xiyou.main.service.contest;

import com.baomidou.mybatisplus.extension.service.IService;
import com.xiyou.main.entity.contest.CtGzProductLine;
import com.xiyou.main.entity.contest.CtLine;
import com.xiyou.main.vo.contest.OnlineLine;

import java.util.List;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author tangcan
 * @since 2019-07-22
 */
public interface CtLineService extends IService<CtLine> {

    /**
     * 列出所有在建中的生产线
     * @param userId
     * @param contestId
     * @return
     */
    List<OnlineLine> listOnline(Integer userId, Integer contestId, Integer date);

    /**
     * 通过生产线的id查生产线的类型信息
     * @param lineIds
     * @return
     */
    List<CtGzProductLine> getLineInfo(List<Integer> lineIds);

    /**
     * 列出所有可以转产的生产线
     * @param userId
     * @param contestId
     * @return
     */
    List<OnlineLine> listTransfer(Integer userId, Integer contestId);

    int getMaintenanceFee(CtLine line);

    void updateDepFee(Integer studentId, Integer contestId, Integer date);

    Integer getDepTotal(Integer studentId, Integer contestId, int date);

    int getProductInProcess(CtLine line);

    int getEquipmentSum(Integer studentId, Integer contestId);

    List<CtLine> list(Integer studentId, Integer contestId);


    /**
     * 查出所有空闲的可以生产的生产线
     * @param userId
     * @param contestId
     * @return
     */
    List<OnlineLine> listNoProducting(Integer userId, Integer contestId);

    /**
     * 显示line中PL_Remain_Date 不为空的数据
     * @param userId
     * @param contestId
     * @return
     */
    List<OnlineLine> getListOnlineInfo(Integer userId, Integer contestId);
}
