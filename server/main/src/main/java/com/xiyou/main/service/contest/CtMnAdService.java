package com.xiyou.main.service.contest;

import com.xiyou.main.entity.contest.CtMnAd;
import com.baomidou.mybatisplus.extension.service.IService;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author tangcan
 * @since 2019-07-25
 */
public interface CtMnAdService extends IService<CtMnAd> {

    List<CtMnAd> getAdList(Integer contestId, Integer studentId, Integer year);

    List<CtMnAd> getAdListByGroupNum(Integer contestId, Integer studentId, String name);
}
