package com.xiyou.main.service.contest;

import com.xiyou.main.entity.contest.CtMnChoose;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author tangcan
 * @since 2019-08-27
 */
public interface CtMnChooseService extends IService<CtMnChoose> {

}
