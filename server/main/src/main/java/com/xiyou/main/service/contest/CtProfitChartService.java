package com.xiyou.main.service.contest;

import com.xiyou.main.entity.contest.CtProfitChart;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author tangcan
 * @since 2019-07-22
 */
public interface CtProfitChartService extends IService<CtProfitChart> {

    List<CtProfitChart> list(CtProfitChart profitChart);

    CtProfitChart getSys(CtProfitChart profitChart);

    List<CtProfitChart> getCurrentYear(CtProfitChart profitChart);

    CtProfitChart getOne(CtProfitChart ctProfitChart);
}
