package com.xiyou.main.service.contest;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.xiyou.main.entity.contest.CtSubject;
import com.baomidou.mybatisplus.extension.service.IService;
import com.xiyou.main.params.contest.SubjectParam;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author tangcan
 * @since 2019-08-25
 */
public interface CtSubjectService extends IService<CtSubject> {

    Page<CtSubject> listByAdmin(SubjectParam subjectParam);

    Page<CtSubject> listByTeacher(SubjectParam subjectParam);

    List<CtSubject> listByTeacher(Integer groupId);

}
