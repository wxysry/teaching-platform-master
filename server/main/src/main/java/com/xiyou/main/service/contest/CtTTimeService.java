package com.xiyou.main.service.contest;

import com.xiyou.main.entity.contest.CtTTime;
import com.baomidou.mybatisplus.extension.service.IService;
import com.xiyou.main.vo.contest.CashFolowEntity;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author tangcan
 * @since 2019-07-22
 */
public interface CtTTimeService extends IService<CtTTime> {


    /**
     * P9->更新时间表
     * @param cashFolowEntity
     */
    void updateCtime(CashFolowEntity cashFolowEntity);

    /**
     * 获取Time表最新时间
     * @param cashFolowEntity
     * @return
     */
    Integer getStudentLatestDate(CashFolowEntity cashFolowEntity);
    int addDate(Integer studentId, Integer contestId, int time);
}
