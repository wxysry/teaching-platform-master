package com.xiyou.main.service.contest;

import com.baomidou.mybatisplus.extension.service.IService;
import com.xiyou.main.entity.contest.CtWorkshop;
import com.xiyou.main.vo.contest.WorkshopVo;

import java.util.List;
import java.util.Map;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author tangcan
 * @since 2019-07-22
 */
public interface CtWorkshopService extends IService<CtWorkshop> {

    /**
     * 工厂容量减一
     * @param workshopId
     * @return
     */
    boolean tallyDown(Integer workshopId);

    int getRentFee(CtWorkshop workshop);

    int getWorkshopFeeSum(Integer studentId, Integer contestId);

    /**
     * 工厂容量加一
     * @param plWid
     */
    boolean tallyUp(Integer plWid);

    /**
     * 工厂列表
     * @param queryMap
     * @return
     */
    List<WorkshopVo> list(Map<String, Object> queryMap);

    /**
     * 查询当季需要结算的工厂租用费
     * @param queryMap
     * @return
     */
    Integer queryWorkshopRentMoney(Map<String, Object> queryMap);

    List<Map<String, Object>> baseInfoList(Integer studentId, Integer contestId);

    List<WorkshopVo> listWorkshop(List<Integer> integers);

    List<CtWorkshop> getList(Integer contestId, Integer studentId);

    void updateWpayDate(Map<String, Object> queryMap);
}
