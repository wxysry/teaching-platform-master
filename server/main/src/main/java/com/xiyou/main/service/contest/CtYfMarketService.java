package com.xiyou.main.service.contest;

import com.baomidou.mybatisplus.extension.service.IService;
import com.xiyou.main.entity.contest.CtYfMarket;
import com.xiyou.main.vo.contest.MarketVo;

import java.util.List;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author tangcan
 * @since 2019-07-22
 */
public interface CtYfMarketService extends IService<CtYfMarket> {

    void update(CtYfMarket yfMarket);

    /**
     * @Author: tangcan
     * @Description: 获取已经开发结束的市场名称
     * @Param: [studentId, contestId]
     * @date: 2019/7/25
     */
    List<String> getHadYfFinishMarkets(Integer studentId, Integer contestId);

    List<MarketVo> listYfMarket(Integer studentId, Integer contestId);
}
