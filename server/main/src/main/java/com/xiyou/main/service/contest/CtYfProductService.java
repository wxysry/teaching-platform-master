package com.xiyou.main.service.contest;

import com.xiyou.main.entity.contest.CtYfProduct;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author tangcan
 * @since 2019-07-22
 */
public interface CtYfProductService extends IService<CtYfProduct> {

    void update(CtYfProduct yfProduct);

    /**
     * 当季结束更新产品研发
     * @param userId
     * @param contestId
     */
    void updateEndingSeason(Integer userId, Integer contestId, Integer date);
}
