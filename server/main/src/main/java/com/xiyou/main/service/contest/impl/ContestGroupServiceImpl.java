package com.xiyou.main.service.contest.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.xiyou.main.entity.contest.ContestGroup;
import com.xiyou.main.dao.contest.ContestGroupMapper;
import com.xiyou.main.service.contest.ContestGroupService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author tangcan
 * @since 2019-07-09
 */
@Service
public class ContestGroupServiceImpl extends ServiceImpl<ContestGroupMapper, ContestGroup> implements ContestGroupService {
    @Autowired
    private ContestGroupMapper contestGroupMapper;

    @Override
    public int insert(ContestGroup contestGroup) {
        return contestGroupMapper.insert(contestGroup);
    }

    @Override
    public boolean checkNameExist(ContestGroup contestGroup) {
        QueryWrapper<ContestGroup> wrapper = new QueryWrapper<>();
        wrapper.ne("id", contestGroup.getId())
                .eq("group_name", contestGroup.getGroupName());
        return this.getOne(wrapper) != null;
    }

    @Override
    public List<ContestGroup> getListByTeacher(Integer teacherId) {
        return contestGroupMapper.getListByTeacher(teacherId);
    }
}
