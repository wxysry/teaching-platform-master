package com.xiyou.main.service.contest.impl;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.xiyou.main.entity.contest.Contest;
import com.xiyou.main.dao.contest.ContestMapper;
import com.xiyou.main.params.contest.ContestParam;
import com.xiyou.main.service.contest.ContestService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author tangcan
 * @since 2019-08-25
 */
@Service
public class ContestServiceImpl extends ServiceImpl<ContestMapper, Contest> implements ContestService {
    @Autowired
    private ContestMapper contestMapper;

    @Override
    public Page<Contest> getPage(ContestParam contestParam) {
        Page<Contest> page = new Page<>(contestParam.getPage(), contestParam.getLimit());
        return contestMapper.getPage(page, contestParam);
    }

    @Override
    public int getUnEndContest(Integer teacherId) {
        return contestMapper.getUnEndContest(teacherId);
    }

    @Override
    public int publish(Integer teacherId, Integer contestId) {
        return contestMapper.publish(teacherId, contestId);
    }

    @Override
    public Page<Contest> getStudentContestPage(ContestParam contestParam) {
        Page<Contest> page = new Page<>(contestParam.getPage(), contestParam.getLimit());
        return contestMapper.getStudentContestPage(page, contestParam);
    }
}
