package com.xiyou.main.service.contest.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.xiyou.main.entity.contest.Contest;
import com.xiyou.main.entity.contest.ContestStudent;
import com.xiyou.main.dao.contest.ContestStudentMapper;
import com.xiyou.main.params.contest.ContestStudentParam;
import com.xiyou.main.service.contest.ContestStudentService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.xiyou.main.vo.contest.ContestScore;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author tangcan
 * @since 2019-08-25
 */
@Service
public class ContestStudentServiceImpl extends ServiceImpl<ContestStudentMapper, ContestStudent> implements ContestStudentService {
    @Autowired
    private ContestStudentMapper contestStudentMapper;

    @Override
    public int insertBatch(Integer contestId, List<Integer> studentIds) {
        if (studentIds == null || studentIds.size() == 0) {
            return 0;
        }
        return contestStudentMapper.insertBatch(contestId, studentIds);
    }

    @Override
    public void remove(Integer contestId) {
        QueryWrapper<ContestStudent> wrapper = new QueryWrapper<>();
        wrapper.eq("contest_id", contestId);
        this.remove(wrapper);
    }

    @Override
    public ContestStudent get(Integer contestId, Integer studentId) {
        QueryWrapper<ContestStudent> wrapper = new QueryWrapper<>();
        wrapper.eq("contest_id", contestId).eq("student_id", studentId);
        return this.getOne(wrapper);
    }

    @Override
    public Page<ContestStudent> getPage(ContestStudentParam contestStudentParam) {
        Page<ContestStudent> page = new Page<>(contestStudentParam.getPage(), contestStudentParam.getLimit());
        return contestStudentMapper.getPage(page, contestStudentParam);
    }

    @Override
    public List<Integer> getStudentIdList(Integer contestId) {
        return contestStudentMapper.getStudentIdList(contestId);
    }

    @Override
    public void updateScore(Integer id, Double score) {
        contestStudentMapper.updateScore(id, score);
    }

    @Override
    public List<ContestScore> getScoreList(Integer contestId) {
        return contestStudentMapper.getScoreList(contestId);
    }

    @Override
    public void updateErrorReportYear(ContestStudent contestStudent, Integer errorYear) {
        if (contestStudent != null && errorYear != null) {
            String errorReportYear = contestStudent.getErrorReportYear() == null ? "" : contestStudent.getErrorReportYear();
            if (!errorReportYear.contains(String.valueOf(errorYear))) {
                // 不重复统计错误年份
                if (errorReportYear.length() > 0) {
                    errorReportYear += "," + errorYear;
                } else {
                    errorReportYear += errorYear;
                }
                contestStudent.setErrorReportYear(errorReportYear);
                this.updateErrorReportYear(contestStudent);
            }
        }
    }

    @Override
    public void updateErrorReportYear(ContestStudent contestStudent) {
        contestStudentMapper.updateErrorReportYear(contestStudent);
    }
}
