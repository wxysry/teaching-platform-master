package com.xiyou.main.service.contest.impl;


import com.xiyou.main.dao.contest.CtBankLoanMapper;
import com.xiyou.main.dao.contest.CtGzCsMapper;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;

import com.xiyou.main.entity.contest.CtBalance;
import com.xiyou.main.dao.contest.CtBalanceMapper;
import com.xiyou.main.service.contest.CtBalanceService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import com.xiyou.main.vo.contest.CashFolowEntity;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author tangcan
 * @since 2019-07-22
 */
@Service
public class CtBalanceServiceImpl extends ServiceImpl<CtBalanceMapper, CtBalance> implements CtBalanceService {
    @Autowired
    private CtBalanceMapper ctBalanceMapper;
    @Autowired
    CtGzCsMapper ctGzCsMapper;
    @Autowired
    CtBankLoanMapper ctBankLoanMapper;

    @Override
    public CtBalance get(CtBalance balance) {
        QueryWrapper<CtBalance> wrapper = new QueryWrapper<>();
        if (balance.getStudentId() != null) {
            wrapper.eq("student_id", balance.getStudentId());
        }
        if (balance.getContestId() != null) {
            wrapper.eq("contest_id", balance.getContestId());
        }
        if (balance.getBsYear() != null) {
            wrapper.eq("bs_year", balance.getBsYear());
        }
        wrapper.eq("bs_isxt", 1);
        return this.getOne(wrapper);
    }

    @Override
    public Integer getRecentYEarHaveEquity(Integer studentId, Integer contestId, Integer nowYear) {
        return ctBalanceMapper.getRecentYEarHaveEquity(studentId, contestId, nowYear);
    }

    @Override
    public int maxTotalEquityPreYear(Integer studentId, Integer contestId, int currentYear) {
        return ctBalanceMapper.maxTotalEquityPreYear(studentId, contestId, currentYear);
    }


    @Override
    public Integer getMaxLoanAmount(CashFolowEntity cashFolowEntity) {
        Integer userRights = ctBalanceMapper.getBalanceMaxLoanLimit(cashFolowEntity);
        CashFolowEntity entity = new CashFolowEntity()
                .setStudentId(cashFolowEntity.getStudentId())
                .setContestId(cashFolowEntity.getContestId())
                .setCurrentTime(cashFolowEntity.getCurrentTime())
                .setCurrentYear(cashFolowEntity.getCurrentYear());
        // gz_cs中Loan_Ceiling
        Integer loadCeiling = ctGzCsMapper.getCSLoanCeiling(entity);
        // bank_loan type=2
        entity.setBlType(2);
        Integer loanType2 = ctBankLoanMapper.getBackLoanAmount(entity);
        // bank_loan type=1
        entity.setBlType(1);
        Integer loanType1 = ctBankLoanMapper.getBackLoanAmount(entity);
        // 最大贷款额度
        return Math.max(0, userRights * loadCeiling - loanType2 - loanType1);
    }

    @Override
    public List<CtBalance> getCurrentYear(CtBalance balance) {
        if (balance == null) {
            return new ArrayList<>();
        }
        QueryWrapper<CtBalance> wrapper = new QueryWrapper<>();
        wrapper.eq("student_id", balance.getStudentId())
                .eq("contest_id", balance.getContestId())
                .eq("bs_year", balance.getBsYear());
        return this.list(wrapper);
    }

    @Override
    public CtBalance getOne(CtBalance ctBalance) {
        QueryWrapper<CtBalance> wrapper = new QueryWrapper<>();
        if (ctBalance.getStudentId() != null) {
            wrapper.eq("student_id", ctBalance.getStudentId());
        }
        if (ctBalance.getContestId() != null) {
            wrapper.eq("contest_id", ctBalance.getContestId());
        }
        if (ctBalance.getBsIsxt() != null) {
            wrapper.eq("bs_isxt", ctBalance.getBsIsxt());
        }
        if (ctBalance.getBsYear() != null) {
            wrapper.eq("bs_year", ctBalance.getBsYear());
        }
        return this.getOne(wrapper);
    }
}
