package com.xiyou.main.service.contest.impl;


import com.xiyou.main.dao.contest.CtGzCsMapper;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.xiyou.main.entity.contest.CtBankLoan;
import com.xiyou.main.dao.contest.CtBankLoanMapper;
import com.xiyou.main.service.contest.CtBankLoanService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.xiyou.main.vo.contest.CashFolowEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author tangcan
 * @since 2019-07-22
 */
@Service
public class CtBankLoanServiceImpl extends ServiceImpl<CtBankLoanMapper, CtBankLoan> implements CtBankLoanService {
    @Autowired
    CtBankLoanMapper ctBankLoanMapper;
    @Autowired
    CtGzCsMapper ctGzCsMapper;

    @Override
    public List<CtBankLoan> list(Integer studentId, Integer contestId) {
        QueryWrapper<CtBankLoan> wrapper = new QueryWrapper<>();
        wrapper.eq("student_id", studentId).eq("contest_id", contestId);
        return this.list(wrapper);
    }

    @Override
    public Integer getCurrentMoney(CashFolowEntity cashFolowEntity) {
        return ctBankLoanMapper.getCurrentMoney(cashFolowEntity);
    }

}
