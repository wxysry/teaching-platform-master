package com.xiyou.main.service.contest.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.xiyou.main.entity.contest.CtCashflow;
import com.xiyou.main.dao.contest.CtCashflowMapper;
import com.xiyou.main.service.contest.CtCashflowService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import com.xiyou.main.vo.contest.CashFolowEntity;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

import java.util.Map;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author tangcan
 * @since 2019-07-22
 */
@Service
public class CtCashflowServiceImpl extends ServiceImpl<CtCashflowMapper, CtCashflow> implements CtCashflowService {
    @Autowired
    CtCashflowMapper mapper;
    @Autowired
    CtCashflowMapper ctCashflowMapper;


    @Override
    public CashFolowEntity getStudentFinanceInfor(Integer studentId, Integer contestId) {
        return null;
    }

    @Override
    public Integer getStudentLastCash(CashFolowEntity cashFolowEntity) {
        return ctCashflowMapper.getStudentLastCash(cashFolowEntity);
    }


    @Override
    public Integer getCash(Integer userId, Integer contestId) {
        return mapper.getCash(userId, contestId);
    }

    @Override
    public List<CtCashflow> list(Integer studentId, Integer contestId, Integer year, List<String> actionList) {
        return mapper.list(studentId, contestId, year, actionList);
    }
}
