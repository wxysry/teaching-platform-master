package com.xiyou.main.service.contest.impl;

import com.xiyou.main.dao.contest.CtGzMaterialMapper;
import com.xiyou.main.dao.contest.CtKcMaterialMapper;
import com.xiyou.main.entity.contest.CtDdMaterial;
import com.xiyou.main.dao.contest.CtDdMaterialMapper;
import com.xiyou.main.service.contest.CtDdMaterialService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.xiyou.main.service.contest.CtKcMaterialService;
import com.xiyou.main.vo.contest.CashFolowEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author tangcan
 * @since 2019-07-22
 */
@Service
public class CtDdMaterialServiceImpl extends ServiceImpl<CtDdMaterialMapper, CtDdMaterial> implements CtDdMaterialService {

    @Autowired
    CtDdMaterialMapper ctDdMaterialMapper;
    @Autowired
    CtGzMaterialMapper ctGzMaterialMapper;


    @Override
    public Integer getStudentPayMoney(CashFolowEntity cashFolowEntity) {

        // // 原料采购剩余时间为0的数量
        // Integer number = ctDdMaterialMapper.getRemainDateISZeroNumb(cashFolowEntity);
        // // 原料规则表.费用
        // Integer cost = ctGzMaterialMapper.getGzMaterialMoney(cashFolowEntity.getContestId());

        return ctDdMaterialMapper.getStudentPayMoney(cashFolowEntity);
    }

    @Override
    public void updateStudentDdMaterialListInfo(CashFolowEntity cashFolowEntity) {

        ctDdMaterialMapper.updateDdMaterialRemainDateEqOne(cashFolowEntity);
    }

    @Override
    public void updateMaterial(List<CtDdMaterial> materialList) {
        if (materialList == null || materialList.size() == 0) {
            return;
        }
        ctDdMaterialMapper.updateMaterial(materialList);
    }
}
