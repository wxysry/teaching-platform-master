package com.xiyou.main.service.contest.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.xiyou.main.entity.contest.CtFee;
import com.xiyou.main.dao.contest.CtFeeMapper;
import com.xiyou.main.service.contest.CtFeeService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author tangcan
 * @since 2019-07-22
 */
@Service
public class CtFeeServiceImpl extends ServiceImpl<CtFeeMapper, CtFee> implements CtFeeService {

    @Autowired
    CtFeeMapper ctFeeMapper;

    @Override
    public int getFeeSum(CtFee fee) {
        if (fee == null) {
            return 0;
        }
        QueryWrapper<CtFee> wrapper = new QueryWrapper<>();
        wrapper.eq("student_id", fee.getStudentId())
                .eq("contest_id", fee.getContestId());
        if (fee.getRRemainDate() != null) {
            wrapper.eq("r_remain_date", fee.getRRemainDate());
        }
        List<CtFee> feeList = this.list(wrapper);
        return feeList.stream().mapToInt(CtFee::getRFee).sum();
    }
}
