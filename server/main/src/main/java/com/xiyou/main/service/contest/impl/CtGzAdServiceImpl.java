package com.xiyou.main.service.contest.impl;

import com.xiyou.main.entity.contest.CtGzAd;
import com.xiyou.main.dao.contest.CtGzAdMapper;
import com.xiyou.main.service.contest.CtGzAdService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author tangcan
 * @since 2019-07-25
 */
@Service
public class CtGzAdServiceImpl extends ServiceImpl<CtGzAdMapper, CtGzAd> implements CtGzAdService {
    @Autowired
    private CtGzAdMapper ctGzAdMapper;

    @Override
    public String getGroupNum(Integer contestId, Integer year) {
        return ctGzAdMapper.getGroupNum(contestId, year);
    }
}
