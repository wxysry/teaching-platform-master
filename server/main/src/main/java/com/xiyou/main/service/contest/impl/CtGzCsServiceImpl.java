package com.xiyou.main.service.contest.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.xiyou.main.entity.contest.CtGzCs;
import com.xiyou.main.dao.contest.CtGzCsMapper;
import com.xiyou.main.service.contest.CtGzCsService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.xiyou.main.vo.contest.CashFolowEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author tangcan
 * @since 2019-07-22
 */
@Service
public class CtGzCsServiceImpl extends ServiceImpl<CtGzCsMapper, CtGzCs> implements CtGzCsService {
    @Autowired
    private CtGzCsMapper ctGzCsMapper;

    @Override
    public CtGzCs getByContestId(Integer contestId) {
        return ctGzCsMapper.getByContestId(contestId);
    }

    @Override
    public Integer getShortLoanInterests(CashFolowEntity cashFolowEntity) {
        return ctGzCsMapper.getShortLoanInterests(cashFolowEntity);
    }

    @Override
    public Integer getMaxWorkshop(Integer contestId) {

        return ctGzCsMapper.getMaxWorkshop(contestId);
    }

    @Override
    public Integer getLongLoanMax(Integer contestId) {
        return ctGzCsMapper.getLongLoanMax(contestId);
    }

    @Override
    public CtGzCs getBySubjectNumber(Integer subjectNumber) {
        return ctGzCsMapper.getBySubjectNumber(subjectNumber);
    }
}
