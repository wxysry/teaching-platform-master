package com.xiyou.main.service.contest.impl;

import com.xiyou.main.entity.contest.CtGzIso;
import com.xiyou.main.dao.contest.CtGzIsoMapper;
import com.xiyou.main.service.contest.CtGzIsoService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author tangcan
 * @since 2019-07-22
 */
@Service
public class CtGzIsoServiceImpl extends ServiceImpl<CtGzIsoMapper, CtGzIso> implements CtGzIsoService {

}
