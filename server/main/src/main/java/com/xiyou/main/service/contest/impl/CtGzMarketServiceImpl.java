package com.xiyou.main.service.contest.impl;

import com.xiyou.main.entity.contest.CtGzMarket;
import com.xiyou.main.dao.contest.CtGzMarketMapper;
import com.xiyou.main.service.contest.CtGzMarketService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author tangcan
 * @since 2019-07-22
 */
@Service
public class CtGzMarketServiceImpl extends ServiceImpl<CtGzMarketMapper, CtGzMarket> implements CtGzMarketService {

}
