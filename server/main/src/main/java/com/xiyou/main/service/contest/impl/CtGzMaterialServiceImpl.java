package com.xiyou.main.service.contest.impl;

import com.xiyou.main.entity.contest.CtGzMaterial;
import com.xiyou.main.dao.contest.CtGzMaterialMapper;
import com.xiyou.main.service.contest.CtGzMaterialService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author tangcan
 * @since 2019-07-22
 */
@Service
public class CtGzMaterialServiceImpl extends ServiceImpl<CtGzMaterialMapper, CtGzMaterial> implements CtGzMaterialService {

}
