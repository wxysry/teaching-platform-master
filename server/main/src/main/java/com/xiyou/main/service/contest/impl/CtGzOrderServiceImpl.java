package com.xiyou.main.service.contest.impl;

import com.xiyou.main.entity.contest.CtGzOrder;
import com.xiyou.main.dao.contest.CtGzOrderMapper;
import com.xiyou.main.service.contest.CtGzOrderService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author tangcan
 * @since 2019-07-22
 */
@Service
public class CtGzOrderServiceImpl extends ServiceImpl<CtGzOrderMapper, CtGzOrder> implements CtGzOrderService {

}
