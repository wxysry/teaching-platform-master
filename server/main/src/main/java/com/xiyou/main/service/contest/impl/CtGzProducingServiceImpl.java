package com.xiyou.main.service.contest.impl;

import com.xiyou.main.entity.contest.CtGzProducing;
import com.xiyou.main.dao.contest.CtGzProducingMapper;
import com.xiyou.main.service.contest.CtGzProducingService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author tangcan
 * @since 2019-07-22
 */
@Service
public class CtGzProducingServiceImpl extends ServiceImpl<CtGzProducingMapper, CtGzProducing> implements CtGzProducingService {


}
