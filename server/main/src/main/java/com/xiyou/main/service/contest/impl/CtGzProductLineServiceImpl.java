package com.xiyou.main.service.contest.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.xiyou.main.entity.contest.CtGzProductLine;
import com.xiyou.main.dao.contest.CtGzProductLineMapper;
import com.xiyou.main.service.contest.CtGzProductLineService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.xiyou.main.vo.contest.CashFolowEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author tangcan
 * @since 2019-07-22
 */
@Service
public class CtGzProductLineServiceImpl extends ServiceImpl<CtGzProductLineMapper, CtGzProductLine> implements CtGzProductLineService {

    @Autowired
    CtGzProductLineMapper ctGzProductLineMapper;

    @Override
    public List<CtGzProductLine> getProductTypeList(Integer contestId) {
        return ctGzProductLineMapper.getProductTypeList(contestId);
    }

    @Override
    public Integer getProductLineInfo(CashFolowEntity cashFolowEntity) {

        return ctGzProductLineMapper.getProductLineInfo(cashFolowEntity);
    }

    @Override
    public List<CtGzProductLine> getListByCplids(List<Integer> cplids) {
        QueryWrapper<CtGzProductLine> wrapper = new QueryWrapper<>();
        wrapper.in("id", cplids);
        return this.list(wrapper);
    }
}
