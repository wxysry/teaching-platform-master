package com.xiyou.main.service.contest.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.xiyou.main.dao.contest.CtGzProductMapper;
import com.xiyou.main.entity.contest.CtGzProduct;
import com.xiyou.main.service.contest.CtGzProductService;
import com.xiyou.main.vo.contest.YfProductVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author tangcan
 * @since 2019-07-22
 */
@Service
public class CtGzProductServiceImpl extends ServiceImpl<CtGzProductMapper, CtGzProduct> implements CtGzProductService {

    @Autowired
    CtGzProductMapper mapper;

    @Override
    public List<String> getProductCPName(Integer contestId) {
        return mapper.getProductCPName(contestId);
    }

    @Override
    public List<YfProductVO> listYfProduct(Integer userId, Integer contestId) {
        return mapper.listYfProduct(userId, contestId);
    }

    @Override
    public List<CtGzProduct> getList(Integer contestId) {
        return mapper.getList(contestId);
    }

    @Override
    public List<CtGzProduct> getListByIds(List<Integer> cpIds) {
        if (cpIds == null || cpIds.size() == 0) {
            return new ArrayList<>();
        }
        QueryWrapper<CtGzProduct> wrapper = new QueryWrapper<>();
        wrapper.in("id", cpIds);
        return this.list(wrapper);
    }
}
