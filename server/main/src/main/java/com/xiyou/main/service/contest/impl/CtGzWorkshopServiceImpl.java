package com.xiyou.main.service.contest.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.xiyou.main.entity.contest.CtGzWorkshop;
import com.xiyou.main.dao.contest.CtGzWorkshopMapper;
import com.xiyou.main.entity.contest.CtWorkshop;
import com.xiyou.main.service.contest.CtGzWorkshopService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author tangcan
 * @since 2019-07-22
 */
@Service
public class CtGzWorkshopServiceImpl extends ServiceImpl<CtGzWorkshopMapper, CtGzWorkshop> implements CtGzWorkshopService {

    @Autowired
    CtGzWorkshopMapper ctGzWorkshopMapper;

    @Override
    public List<CtGzWorkshop> getWorkshopInfo(Integer contestId) {

        return ctGzWorkshopMapper.getGzWorkshopInfo(contestId);
    }

}
