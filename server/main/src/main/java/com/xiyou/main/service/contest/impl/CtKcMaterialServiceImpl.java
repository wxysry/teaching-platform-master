package com.xiyou.main.service.contest.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.xiyou.main.dao.contest.CtKcMaterialMapper;
import com.xiyou.main.entity.contest.CtGzCs;
import com.xiyou.main.entity.contest.CtKcMaterial;
import com.xiyou.main.service.contest.CtGzCsService;
import com.xiyou.main.service.contest.CtKcMaterialService;
import com.xiyou.main.vo.contest.CashFolowEntity;
import com.xiyou.main.vo.contest.MaterialVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author tangcan
 * @since 2019-07-22
 */
@Service
public class CtKcMaterialServiceImpl extends ServiceImpl<CtKcMaterialMapper, CtKcMaterial> implements CtKcMaterialService {
    @Autowired
    private CtKcMaterialMapper ctKcMaterialMapper;
    @Autowired
    private CtGzCsService ctGzCsService;

    @Override
    public int getMaterialSum(Integer studentId, Integer contestId) {
        return ctKcMaterialMapper.getMaterialSum(studentId, contestId);
    }

    @Override
    public List<CtKcMaterial> listNeed(Integer contestId, List<Integer> lineIds) {
        if (lineIds.size() == 0) {
            return new ArrayList<>();
        }
        return ctKcMaterialMapper.listNeed(contestId, lineIds);
    }


    @Override
    public void updateKcMaterial(CashFolowEntity cashFolowEntity) {
        ctKcMaterialMapper.updateKcMaterial(cashFolowEntity);
    }

    @Override
    public List<MaterialVo> listKc(Integer contestId, Integer userId) {
        List<MaterialVo> list = ctKcMaterialMapper.listKc(contestId, userId);
        CtGzCs ctGzCs = ctGzCsService.getByContestId(contestId);
        if (list.size() > 0) {
            for (MaterialVo l : list) {
                l.setUrgentPrice(l.getUrgentPrice() * ctGzCs.getEmergenceMultipleMaterial());
            }
        }
        return list;
    }

    @Override
    public List<MaterialVo> listSellKc(Integer contestId, Integer userId) {
        List<MaterialVo> list = ctKcMaterialMapper.listSellKc(contestId, userId);
        CtGzCs ctGzCs = ctGzCsService.getByContestId(contestId);
        if (list.size() > 0) {
            for (MaterialVo l : list) {
                l.setUrgentPrice(l.getDirectCost() * ctGzCs.getEmergenceMultipleMaterial());
                l.setSellPrice((double) l.getDirectCost() * ((double) ctGzCs.getInventoryDiscountMaterial() / 100.0));
            }
        }
        return list;
    }
}
