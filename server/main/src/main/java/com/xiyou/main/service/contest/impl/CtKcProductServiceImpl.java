package com.xiyou.main.service.contest.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.xiyou.main.dao.contest.CtKcProductMapper;
import com.xiyou.main.entity.contest.CtGzCs;
import com.xiyou.main.entity.contest.CtKcProduct;
import com.xiyou.main.service.contest.CtGzCsService;
import com.xiyou.main.service.contest.CtKcProductService;
import com.xiyou.main.vo.contest.ProductVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author tangcan
 * @since 2019-07-22
 */
@Service
public class CtKcProductServiceImpl extends ServiceImpl<CtKcProductMapper, CtKcProduct> implements CtKcProductService {
    @Autowired
    private CtKcProductMapper ctKcProductMapper;
    @Autowired
    private CtGzCsService ctGzCsService;


    @Override
    public int getProductSum(Integer studentId, Integer contestId) {
        return ctKcProductMapper.getProductSum(studentId, contestId);
    }

    @Override
    public List<ProductVo> listKc(Integer contestId, Integer userId) {
        List<ProductVo> list = ctKcProductMapper.listKc(contestId, userId);
        CtGzCs ctGzCs = ctGzCsService.getByContestId(contestId);
        if (list.size() > 0) {
            for (ProductVo l : list) {
                l.setUrgentPrice(l.getUrgentPrice() * ctGzCs.getEmergenceMultipleProduct());
            }
        }
        return list;
    }

    @Override
    public List<ProductVo> listSellKc(Integer contestId, Integer userId) {
        List<ProductVo> list = ctKcProductMapper.listSellKc(contestId, userId);
        CtGzCs ctGzCs = ctGzCsService.getByContestId(contestId);
        if (list.size() > 0) {
            for (ProductVo l : list) {
                l.setUrgentPrice(l.getDirectCost() * ctGzCs.getEmergenceMultipleProduct());
                l.setSellPrice((double) l.getDirectCost() * ((double) ctGzCs.getInventoryDiscountProduct() / 100.0));
            }
        }
        return list;
    }
}
