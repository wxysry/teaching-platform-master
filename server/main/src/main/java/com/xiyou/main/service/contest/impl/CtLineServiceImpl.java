package com.xiyou.main.service.contest.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.xiyou.main.dao.contest.CtLineMapper;
import com.xiyou.main.entity.contest.CtGzProductLine;
import com.xiyou.main.entity.contest.CtLine;
import com.xiyou.main.service.contest.CtLineService;
import com.xiyou.main.vo.contest.OnlineLine;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author tangcan
 * @since 2019-07-22
 */
@Service
public class CtLineServiceImpl extends ServiceImpl<CtLineMapper, CtLine> implements CtLineService {
    @Autowired
    CtLineMapper ctLineMapper;

    @Override
    public List<OnlineLine> listOnline(Integer userId, Integer contestId, Integer date) {
        return ctLineMapper.listOnline(userId, contestId, date);
    }

    @Override
    public List<CtGzProductLine> getLineInfo(List<Integer> lineIds) {
        return ctLineMapper.getLineInfo(lineIds);
    }

    @Override
    public List<OnlineLine> listTransfer(Integer userId, Integer contestId) {
        return ctLineMapper.listTransfer(userId, contestId);
    }

    @Override
    public int getMaintenanceFee(CtLine line) {
        return ctLineMapper.getMaintenanceFee(line);
    }

    @Override
    public void updateDepFee(Integer studentId, Integer contestId, Integer date) {
        ctLineMapper.updateDepFee(studentId, contestId, date);
    }

    @Override
    public Integer getDepTotal(Integer studentId, Integer contestId, int date) {
        return ctLineMapper.getDepTotal(studentId, contestId, date);
    }

    @Override
    public int getProductInProcess(CtLine line) {
        if (line == null) {
            return 0;
        }
        return ctLineMapper.getProductInProcess(line);
    }

    @Override
    public int getEquipmentSum(Integer studentId, Integer contestId) {
        return ctLineMapper.getEquipmentSum(studentId, contestId);
    }

    @Override
    public List<CtLine> list(Integer studentId, Integer contestId) {
        QueryWrapper<CtLine> wrapper = new QueryWrapper<>();
        wrapper.eq("student_id", studentId).eq("contest_id", contestId);
        return this.list(wrapper);
    }

    @Override
    public List<OnlineLine> listNoProducting(Integer userId, Integer contestId) {
        return ctLineMapper.listNoProducting(userId, contestId);
    }

    @Override
    public List<OnlineLine> getListOnlineInfo(Integer userId, Integer contestId) {
        return ctLineMapper.getListOnline(userId, contestId);
    }


}
