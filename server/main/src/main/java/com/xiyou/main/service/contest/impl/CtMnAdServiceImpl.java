package com.xiyou.main.service.contest.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.xiyou.main.entity.contest.CtMnAd;
import com.xiyou.main.dao.contest.CtMnAdMapper;
import com.xiyou.main.service.contest.CtMnAdService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author tangcan
 * @since 2019-07-25
 */
@Service
public class CtMnAdServiceImpl extends ServiceImpl<CtMnAdMapper, CtMnAd> implements CtMnAdService {
    @Autowired
    private CtMnAdMapper ctMnAdMapper;

    @Override
    public List<CtMnAd> getAdList(Integer contestId, Integer studentId, Integer year) {
        return ctMnAdMapper.getAdList(contestId, studentId, year);
    }

    @Override
    public List<CtMnAd> getAdListByGroupNum(Integer contestId, Integer studentId, String name) {
        QueryWrapper<CtMnAd> wrapper = new QueryWrapper<>();
        wrapper.eq("student_id", studentId)
                .eq("contest_id", contestId)
                .eq("group_num", name)
                .orderByAsc("year");
        return this.list(wrapper);
    }
}
