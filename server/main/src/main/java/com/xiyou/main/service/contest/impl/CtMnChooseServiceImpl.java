package com.xiyou.main.service.contest.impl;

import com.xiyou.main.entity.contest.CtMnChoose;
import com.xiyou.main.dao.contest.CtMnChooseMapper;
import com.xiyou.main.service.contest.CtMnChooseService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author tangcan
 * @since 2019-08-27
 */
@Service
public class CtMnChooseServiceImpl extends ServiceImpl<CtMnChooseMapper, CtMnChoose> implements CtMnChooseService {

}
