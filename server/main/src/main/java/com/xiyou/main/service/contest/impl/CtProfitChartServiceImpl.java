package com.xiyou.main.service.contest.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.xiyou.main.entity.contest.CtProfitChart;
import com.xiyou.main.dao.contest.CtProfitChartMapper;
import com.xiyou.main.service.contest.CtProfitChartService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author tangcan
 * @since 2019-07-22
 */
@Service
public class CtProfitChartServiceImpl extends ServiceImpl<CtProfitChartMapper, CtProfitChart> implements CtProfitChartService {
    @Override
    public List<CtProfitChart> list(CtProfitChart profitChart) {
        if (profitChart == null) {
            return new ArrayList<>();
        }
        QueryWrapper<CtProfitChart> wrapper = new QueryWrapper<>();
        wrapper.eq("student_id", profitChart.getStudentId())
                .eq("contest_id", profitChart.getContestId())
                .eq("bs_isxt", 1);
        if (profitChart.getPcDate() != null) {
            wrapper.eq("pc_date", profitChart.getPcDate());
        }
        return this.list(wrapper);
    }

    @Override
    public CtProfitChart getSys(CtProfitChart profitChart) {
        if (profitChart == null) {
            return null;
        }
        QueryWrapper<CtProfitChart> wrapper = new QueryWrapper<>();
        wrapper.eq("student_id", profitChart.getStudentId())
                .eq("contest_id", profitChart.getContestId())
                .eq("pc_date", profitChart.getPcDate())
                .eq("bs_isxt", 1);
        return this.getOne(wrapper);
    }

    @Override
    public List<CtProfitChart> getCurrentYear(CtProfitChart profitChart) {
        if (profitChart == null) {
            return new ArrayList<>();
        }
        QueryWrapper<CtProfitChart> wrapper = new QueryWrapper<>();
        wrapper.eq("student_id", profitChart.getStudentId())
                .eq("contest_id", profitChart.getContestId())
                .eq("pc_date", profitChart.getPcDate());
        return this.list(wrapper);
    }

    @Override
    public CtProfitChart getOne(CtProfitChart ctProfitChart) {
        QueryWrapper<CtProfitChart> wrapper = new QueryWrapper<>();
        if (ctProfitChart.getStudentId() != null) {
            wrapper.eq("student_id", ctProfitChart.getStudentId());
        }
        if (ctProfitChart.getContestId() != null) {
            wrapper.eq("contest_id", ctProfitChart.getContestId());
        }
        if (ctProfitChart.getBsIsxt() != null) {
            wrapper.eq("bs_isxt", ctProfitChart.getBsIsxt());
        }
        if (ctProfitChart.getPcDate() != null) {
            wrapper.eq("pc_date", ctProfitChart.getPcDate());
        }
        return this.getOne(wrapper);
    }
}
