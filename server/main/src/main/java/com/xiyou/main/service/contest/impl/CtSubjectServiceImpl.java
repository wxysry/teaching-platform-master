package com.xiyou.main.service.contest.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.xiyou.main.entity.contest.CtSubject;
import com.xiyou.main.dao.contest.CtSubjectMapper;
import com.xiyou.main.params.contest.SubjectParam;
import com.xiyou.main.service.contest.CtSubjectService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author tangcan
 * @since 2019-08-25
 */
@Service
public class CtSubjectServiceImpl extends ServiceImpl<CtSubjectMapper, CtSubject> implements CtSubjectService {
    @Autowired
    private CtSubjectMapper ctSubjectMapper;

    @Override
    public Page<CtSubject> listByAdmin(SubjectParam subjectParam) {
        Page<CtSubject> page = new Page<>(subjectParam.getPage(), subjectParam.getLimit());
        return ctSubjectMapper.listByAdmin(page, subjectParam);
    }

    @Override
    public Page<CtSubject> listByTeacher(SubjectParam subjectParam) {
        Page<CtSubject> page = new Page<>(subjectParam.getPage(), subjectParam.getLimit());
        return ctSubjectMapper.listByTeacher(page, subjectParam);
    }

    @Override
    public List<CtSubject> listByTeacher(Integer groupId) {
        QueryWrapper<CtSubject> wrapper = new QueryWrapper<>();
        wrapper.eq("group_id", groupId)
                .eq("upload", 1);
        return this.list(wrapper);
    }
}
