package com.xiyou.main.service.contest.impl;

import com.xiyou.main.entity.contest.CtTTime;
import com.xiyou.main.dao.contest.CtTTimeMapper;
import com.xiyou.main.service.contest.CtTTimeService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.xiyou.main.vo.contest.CashFolowEntity;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author tangcan
 * @since 2019-07-22
 */
@Service
public class CtTTimeServiceImpl extends ServiceImpl<CtTTimeMapper, CtTTime> implements CtTTimeService {
    @Autowired
    private CtTTimeMapper ctTTimeMapper;


    @Override
    public void updateCtime(CashFolowEntity cashFolowEntity) {
        ctTTimeMapper.updateStudentCtTime(cashFolowEntity);
    }

    @Override
    public Integer getStudentLatestDate(CashFolowEntity cashFolowEntity) {

        return ctTTimeMapper.getStudentLatestDate(cashFolowEntity);
    }

    @Override
    public int addDate(Integer studentId, Integer contestId, int time) {
        return ctTTimeMapper.addDate(studentId, contestId, time);
    }
}
