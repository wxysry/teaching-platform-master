package com.xiyou.main.service.contest.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.xiyou.main.entity.contest.CtWorkshop;
import com.xiyou.main.dao.contest.CtWorkshopMapper;
import com.xiyou.main.service.contest.CtWorkshopService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.xiyou.main.vo.contest.WorkshopVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author tangcan
 * @since 2019-07-22
 */
@Service
public class CtWorkshopServiceImpl extends ServiceImpl<CtWorkshopMapper, CtWorkshop> implements CtWorkshopService {

    @Autowired
    private CtWorkshopMapper ctWorkshopMapper;

    @Override
    public int getRentFee(CtWorkshop workshop) {
        return ctWorkshopMapper.getRentFee(workshop);
    }

    @Override
    public int getWorkshopFeeSum(Integer studentId, Integer contestId) {
        return ctWorkshopMapper.getWorkshopFeeSum(studentId, contestId);
    }

    @Override
    public boolean tallyDown(Integer workshopId) {
        return ctWorkshopMapper.tallyDown(workshopId);
    }


    @Override
    public boolean tallyUp(Integer plWid) {
        return ctWorkshopMapper.tallyUp(plWid);
    }

    @Override
    public List<WorkshopVo> list(Map<String, Object> queryMap) {
        return ctWorkshopMapper.list(queryMap);
    }

    @Override
    public List<Map<String, Object>> baseInfoList(Integer studentId, Integer contestId) {
        return ctWorkshopMapper.baseInfoList(studentId, contestId);
    }

    @Override
    public Integer queryWorkshopRentMoney(Map<String, Object> queryMap) {
        return ctWorkshopMapper.queryWorkshopRentMoney(queryMap);
    }

    @Override
    public List<WorkshopVo> listWorkshop(List<Integer> integers) {
        return ctWorkshopMapper.listWorkshop(integers);
    }

    @Override
    public List<CtWorkshop> getList(Integer contestId, Integer studentId) {
        QueryWrapper<CtWorkshop> wrapper = new QueryWrapper<>();
        wrapper.eq("contest_id", contestId).eq("student_id", studentId);
        return this.list(wrapper);
    }

    @Override
    public void updateWpayDate(Map<String, Object> queryMap) {
        ctWorkshopMapper.updateWpayDate(queryMap);
    }
}
