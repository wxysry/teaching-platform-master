package com.xiyou.main.service.contest.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.xiyou.main.dao.contest.CtXzOrderMapper;
import com.xiyou.main.entity.contest.CtXzOrder;
import com.xiyou.main.service.contest.CtXzOrderService;
import com.xiyou.main.vo.contest.XzOrderVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author tangcan
 * @since 2019-07-22
 */
@Service
public class CtXzOrderServiceImpl extends ServiceImpl<CtXzOrderMapper, CtXzOrder> implements CtXzOrderService {
    @Autowired
    private CtXzOrderMapper ctXzOrderMapper;

    @Override
    public List<CtXzOrder> list(CtXzOrder order) {
        return ctXzOrderMapper.list(order);
    }

    @Override
    public Map<String, BigDecimal> getSalesAndDirectCost(Integer studentId, Integer contestId, int year) {
        return ctXzOrderMapper.getSalesAndDirectCost(studentId, contestId, year);
    }

    @Override
    public List<XzOrderVo> listDeliveryOrder(Map<String, Object> queryMap) {
        return ctXzOrderMapper.listDeliveryOrder(queryMap);
    }
}
