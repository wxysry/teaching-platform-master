package com.xiyou.main.service.contest.impl;

import com.xiyou.main.entity.contest.CtYfIso;
import com.xiyou.main.dao.contest.CtYfIsoMapper;
import com.xiyou.main.service.contest.CtYfIsoService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author tangcan
 * @since 2019-07-22
 */
@Service
public class CtYfIsoServiceImpl extends ServiceImpl<CtYfIsoMapper, CtYfIso> implements CtYfIsoService {
    @Autowired
    private CtYfIsoMapper ctYfIsoMapper;

    @Override
    public void update(CtYfIso yfIso) {
        ctYfIsoMapper.update(yfIso);
    }
}
