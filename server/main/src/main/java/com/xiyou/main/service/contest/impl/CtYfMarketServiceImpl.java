package com.xiyou.main.service.contest.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.xiyou.main.dao.contest.CtYfMarketMapper;
import com.xiyou.main.entity.contest.CtYfMarket;
import com.xiyou.main.service.contest.CtYfMarketService;
import com.xiyou.main.vo.contest.MarketVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author tangcan
 * @since 2019-07-22
 */
@Service
public class CtYfMarketServiceImpl extends ServiceImpl<CtYfMarketMapper, CtYfMarket> implements CtYfMarketService {
    @Autowired
    private CtYfMarketMapper ctYfMarketMapper;

    @Override
    public void update(CtYfMarket yfMarket) {
        ctYfMarketMapper.update(yfMarket);
    }

    @Override
    public List<String> getHadYfFinishMarkets(Integer studentId, Integer contestId) {
        return ctYfMarketMapper.getHadYfFinishMarkets(studentId, contestId);
    }

    @Override
    public List<MarketVo> listYfMarket(Integer studentId, Integer contestId) {
        return ctYfMarketMapper.listYfMarket(studentId, contestId);
    }
}
