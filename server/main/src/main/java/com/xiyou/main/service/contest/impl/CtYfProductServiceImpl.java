package com.xiyou.main.service.contest.impl;

import com.xiyou.main.entity.contest.CtYfProduct;
import com.xiyou.main.dao.contest.CtYfProductMapper;
import com.xiyou.main.service.contest.CtYfProductService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author tangcan
 * @since 2019-07-22
 */
@Service
public class CtYfProductServiceImpl extends ServiceImpl<CtYfProductMapper, CtYfProduct> implements CtYfProductService {
    @Autowired
    private CtYfProductMapper ctYfProductMapper;

    @Override
    public void update(CtYfProduct yfProduct) {
        ctYfProductMapper.update(yfProduct);
    }

    @Override
    public void updateEndingSeason(Integer userId, Integer contestId, Integer date) {
        ctYfProductMapper.updateEndingSeason(userId, contestId, date);
    }
}
