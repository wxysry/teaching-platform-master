package com.xiyou.main.service.exam;

import com.xiyou.main.entity.exam.ExamProblem;
import com.baomidou.mybatisplus.extension.service.IService;
import com.xiyou.main.vo.exam.ExamAnalysis;
import com.xiyou.main.vo.exam.ExamProblemInfo;
import com.xiyou.main.vo.exam.ExamScore;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author tangcan
 * @since 2019-07-10
 */
public interface ExamProblemService extends IService<ExamProblem> {

    int insertBatch(List<ExamProblem> examProblemList);

    List<ExamProblemInfo> getByExamId(Integer examId);

    void removeByExamIds(List<Integer> examIdList);

    boolean update(ExamProblem examProblem);

    int autoJudge(Integer examId);

    int updateBatch(List<ExamProblem> examProblemList);

    ExamProblem getByExamIdAndProblemId(Integer examId, Integer problemId);

    List<ExamAnalysis> analysis(Integer paperId);

    void removeByPaperId(Integer paperId);

    void autoJudgeByExamIds(List<Integer> examIdList);

    List<ExamScore> getScoreList(Integer paperId);
}
