package com.xiyou.main.service.exam;

import com.xiyou.main.entity.exam.ExamRecord;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author tangcan
 * @since 2019-07-10
 */
public interface ExamRecordService extends IService<ExamRecord> {

    List<ExamRecord> getByExamId(Integer examId);

    ExamRecord getCurrent(Integer examId);

    void removeByExamIds(List<Integer> examIdList);

    List<ExamRecord> listByPaperId(Integer paperId);
}
