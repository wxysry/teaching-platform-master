package com.xiyou.main.service.exam;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.xiyou.main.entity.exam.Exam;
import com.baomidou.mybatisplus.extension.service.IService;
import com.xiyou.main.params.exam.ExamPaperParam;
import com.xiyou.main.params.exam.ExamStudentParam;
import com.xiyou.main.vo.exam.ExamPaper;
import com.xiyou.main.vo.exam.ExamStudent;

import java.util.List;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author tangcan
 * @since 2019-07-10
 */
public interface ExamService extends IService<Exam> {

    int insertBatch(List<Exam> examList);

    Page<ExamPaper> getPage(ExamPaperParam examPaperParam);

    ExamPaper getByExamId(Integer examId);

    boolean update(Exam exam);

    Page<ExamStudent> getStudentList(ExamStudentParam examStudentParam);

    List<Exam> getByPaperId(Integer paperId);

    void updateExamTime(Integer paperId, Integer examTime);

    int getHandInStudentNum(Integer paperId);

    void handInByExamIds(List<Integer> examIdList);
}
