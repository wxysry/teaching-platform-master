package com.xiyou.main.service.exam;

import com.xiyou.main.entity.exam.PaperProblem;
import com.baomidou.mybatisplus.extension.service.IService;
import com.xiyou.main.vo.exam.ExamProblemInfo;

import java.util.List;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author tangcan
 * @since 2019-07-04
 */
public interface PaperProblemService extends IService<PaperProblem> {

    int insertBatch(List<PaperProblem> paperProblemList);

    void deleteAllProblem(Integer paperId);

    Double countTotalScore(Integer paperId);

    List<PaperProblem> get(PaperProblem paperProblem);

    void removeByPaperId(Integer paperId);

    List<ExamProblemInfo> listByPaperId(Integer paperId);
}
