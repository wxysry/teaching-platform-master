package com.xiyou.main.service.exam;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.xiyou.main.entity.exam.Paper;
import com.baomidou.mybatisplus.extension.service.IService;
import com.xiyou.main.params.exam.PaperParam;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author tangcan
 * @since 2019-07-04
 */
public interface PaperService extends IService<Paper> {

    int insert(Paper paper);

    boolean update(Paper paper);

    Page<Paper> getPage(PaperParam paperParam);

    Paper getByPaperIdAndTeacherId(Integer paperId, Integer teacherId);
}
