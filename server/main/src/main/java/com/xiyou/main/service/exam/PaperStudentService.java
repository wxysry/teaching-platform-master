package com.xiyou.main.service.exam;

import com.xiyou.main.entity.exam.PaperStudent;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author tangcan
 * @since 2019-07-16
 */
public interface PaperStudentService extends IService<PaperStudent> {

    boolean removeByPaperId(Integer paperId);

    int insertBatch(List<PaperStudent> paperStudentList);

    List<Integer> getStudentIdsByPaperId(Integer paperId);
}
