package com.xiyou.main.service.exam;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.xiyou.main.entity.exam.PptCourse;
import com.xiyou.main.entity.exam.VideoCourse;
import com.xiyou.main.params.exam.CourseQueryParam;

import java.io.Serializable;
import java.util.Collection;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author tangcan
 * @since 2019-06-28
 */
public interface PptCourseService extends IService<PptCourse> {

    Page<PptCourse> listByUser(CourseQueryParam param);

    boolean removeByGroupIds(Collection<? extends Serializable> collection);

    Page<PptCourse> listByStudent(CourseQueryParam param);
}
