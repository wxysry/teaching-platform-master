package com.xiyou.main.service.exam;

import com.xiyou.main.entity.exam.PptGroup;
import com.baomidou.mybatisplus.extension.service.IService;
import com.xiyou.main.entity.exam.VideoGroup;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author tangcan
 * @since 2019-06-28
 */
public interface PptGroupService extends IService<PptGroup> {

    List<VideoGroup> listByTeacher(Integer userId);

    void insert(PptGroup group);

    List<PptGroup> listByStudent(Integer studentId);
}
