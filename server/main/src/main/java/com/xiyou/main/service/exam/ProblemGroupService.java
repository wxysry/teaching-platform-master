package com.xiyou.main.service.exam;

import com.xiyou.main.entity.exam.ProblemGroup;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author tangcan
 * @since 2019-07-07
 */
public interface ProblemGroupService extends IService<ProblemGroup> {
    /**
     * @Author: tangcan
     * @Description: 获取一级目录和二级目录名称的拼接形式
     * @Param: [subgroupIdList]
     * @date: 2019/7/7
     */
    String getGroupAndSubgroupName(Integer subgroupId);

    /**
     * @Author: tangcan
     * @Description: 获取一级目录和二级目录名称的拼接形式
     * @Param: [subgroupIdList]
     * @date: 2019/7/7
     */
    List<ProblemGroup> getGroupAndSubgroupNameList(List<Integer> subgroupIdList);

    List<ProblemGroup> getSubGroupList(Integer userId, Integer fatherId, Integer subgroupId);

    List<ProblemGroup> getList(Integer fatherId);

    List<ProblemGroup> getGroupListByUserId(Integer userId);

    /**
     * @Author: tangcan
     * @Description: 获取试题分组级联
     * @Param: [userId]
     * @date: 2019/7/8
    */
    List<ProblemGroup> getAllList(Integer teacherId);

    int insert(ProblemGroup problemGroup);

    boolean checkNameExist(ProblemGroup group);

    void deleteSubGroup(List<Integer> fatherIdList);
}
