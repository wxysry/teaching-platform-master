package com.xiyou.main.service.exam;

import com.xiyou.main.entity.exam.ProblemOption;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author tangcan
 * @since 2019-06-24
 */
public interface ProblemOptionService extends IService<ProblemOption> {

    int insertBatch(List<ProblemOption> optionList);

    List<ProblemOption> getList(Integer problemId);

    List<ProblemOption> getByProblemIdList(List<Integer> problemIdList);

    void removeByProblemIds(List<Integer> problemIdList);
}
