package com.xiyou.main.service.exam;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.xiyou.main.entity.exam.Problem;
import com.baomidou.mybatisplus.extension.service.IService;
import com.xiyou.main.params.exam.ProblemInfo;
import com.xiyou.main.params.exam.ProblemParam;

import java.util.List;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author tangcan
 * @since 2019-06-24
 */
public interface ProblemService extends IService<Problem> {

    int insertBatch(List<Problem> problemList);

    Problem selectById(Integer id);

    Page<ProblemInfo> getList(Integer teacherId, ProblemParam problemParam);

    Problem getByTeacher(Integer teacherId, Integer problemId);

    boolean update(Problem problem);
}
