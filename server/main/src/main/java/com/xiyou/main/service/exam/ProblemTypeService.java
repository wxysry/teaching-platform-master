package com.xiyou.main.service.exam;

import com.xiyou.main.entity.exam.ProblemType;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author tangcan
 * @since 2019-06-24
 */
public interface ProblemTypeService extends IService<ProblemType> {

    List<ProblemType> getList();
}
