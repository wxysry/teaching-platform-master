package com.xiyou.main.service.exam;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.xiyou.main.entity.exam.Resources;
import com.baomidou.mybatisplus.extension.service.IService;
import com.xiyou.main.params.exam.ResourcesParam;

import java.util.List;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author tangcan
 * @since 2019-07-07
 */
public interface ResourcesService extends IService<Resources> {

    List<Resources> getList(List<String> fileList, Integer type);

    List<Resources> getByFileNameList(List<String> fileNameList);

    int insertBatch(List<Resources> resourcesList);

    Page<Resources> getPage(ResourcesParam resourcesParam);

    boolean check(String spyAttachment);
}
