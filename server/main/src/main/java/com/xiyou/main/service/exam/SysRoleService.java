package com.xiyou.main.service.exam;

import com.xiyou.main.entity.exam.SysRole;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author tangcan
 * @since 2019-06-24
 */
public interface SysRoleService extends IService<SysRole> {

    SysRole get(String roleCode);

    SysRole getByUserAccount(String account);
}
