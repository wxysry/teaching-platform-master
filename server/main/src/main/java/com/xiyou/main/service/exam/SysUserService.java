package com.xiyou.main.service.exam;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.xiyou.main.entity.exam.SysUser;
import com.baomidou.mybatisplus.extension.service.IService;
import com.xiyou.main.params.exam.UserParam;

import java.util.List;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author tangcan
 * @since 2019-06-24
 */
public interface SysUserService extends IService<SysUser> {

    /**
     * @Author: tangcan
     * @Description: 查询全部信息，包括学校名称，角色列表等
     * @Param: [sysUser]
     * @date: 2019/6/25
     */
    SysUser getAllInfo(SysUser sysUser);

    /**
     * @Author: tangcan
     * @Description: 查询简单信息（单表）
     * @Param: [sysUser]
     * @date: 2019/6/25
     */
    SysUser get(SysUser sysUser);

    boolean insert(SysUser sysUser);


    Page<SysUser> getTeacherPage(UserParam userParam);

    boolean forbid(Integer userId);

    boolean updatePwd(Integer userId, String password);

    List<SysUser> getByEmailList(List<String> emailList);

    int insertBatch(List<SysUser> studentList);

    boolean setForbid(Integer userId, Integer forbid);

    Page<SysUser> getStudentPage(UserParam userParam);

    List<SysUser> getStudentList(Integer teacherId);

    List<SysUser> getListByAccountList(List<String> accountList);

    List<SysUser> getByAccountList(List<String> needIdUserAccountList);

}
