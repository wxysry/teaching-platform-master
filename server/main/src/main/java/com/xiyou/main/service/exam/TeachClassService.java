package com.xiyou.main.service.exam;

import com.xiyou.main.entity.exam.TeachClass;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author tangcan
 * @since 2019-06-26
 */
public interface TeachClassService extends IService<TeachClass> {
    int count(TeachClass teachClass);

    int insertBatch(List<TeachClass> teachClassList);

    List<Integer> getStudentIdList(Integer teacherId);
}
