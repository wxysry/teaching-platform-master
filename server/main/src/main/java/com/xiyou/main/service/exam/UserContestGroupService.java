package com.xiyou.main.service.exam;

import com.xiyou.main.entity.contest.ContestGroup;
import com.xiyou.main.entity.contest.UserContestGroup;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author tangcan
 * @since 2019-07-09
 */
public interface UserContestGroupService extends IService<UserContestGroup> {

    List<ContestGroup> listAll();

    List<UserContestGroup> getByUserIdList(List<Integer> userIdList);

    void save(Integer teacherId, List<Integer> groups);
}
