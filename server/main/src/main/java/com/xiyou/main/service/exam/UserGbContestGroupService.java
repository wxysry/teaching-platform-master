package com.xiyou.main.service.exam;

import com.baomidou.mybatisplus.extension.service.IService;
import com.xiyou.main.entity.gbcontest.GbContestGroup;
import com.xiyou.main.entity.gbcontest.UserGbContestGroup;
import com.xiyou.main.entity.newcontest.NewContestGroup;
import com.xiyou.main.entity.newcontest.UserNewContestGroup;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author tangcan
 * @since 2019-07-09
 */
public interface UserGbContestGroupService extends IService<UserGbContestGroup> {

    List<UserGbContestGroup> getByUserIdList(List<Integer> userIdList);

    void save(Integer teacherId, List<Integer> groups);
}
