package com.xiyou.main.service.exam;

import com.baomidou.mybatisplus.extension.service.IService;
import com.xiyou.main.entity.gbcontest.UserGbContestGroup;
import com.xiyou.main.entity.ngbcontest.UserNGbContestGroup;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author tangcan
 * @since 2019-07-09
 */
public interface UserNGbContestGroupService extends IService<UserNGbContestGroup> {

    List<UserNGbContestGroup> getByUserIdList(List<Integer> userIdList);

    void save(Integer teacherId, List<Integer> groups);
}
