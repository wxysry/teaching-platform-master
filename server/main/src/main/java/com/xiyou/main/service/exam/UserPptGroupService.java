package com.xiyou.main.service.exam;

import com.xiyou.main.entity.exam.UserPptGroup;
import com.baomidou.mybatisplus.extension.service.IService;

import java.io.Serializable;
import java.util.Collection;
import java.util.List;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author tangcan
 * @since 2019-06-28
 */
public interface UserPptGroupService extends IService<UserPptGroup> {
    void insertBatch(List<UserPptGroup> userPptGroupList);

    boolean deleteByUserId(Integer userId);

    void save(Integer userId, List<Integer> groups);

    List<UserPptGroup> getByUserIdList(List<Integer> userIdList);

    boolean removeByGroupIds(Collection<? extends Serializable> collection);
}
