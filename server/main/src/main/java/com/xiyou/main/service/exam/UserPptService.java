package com.xiyou.main.service.exam;

import com.xiyou.main.entity.exam.SysUser;
import com.xiyou.main.entity.exam.UserPpt;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author tangcan
 * @since 2019-10-28
 */
public interface UserPptService extends IService<UserPpt> {

    void removeByPptId(Integer pptId);

    void insertBatch(Integer pptId, List<Integer> studentIdList);

    List<SysUser> getStudents(Integer pptId);
}
