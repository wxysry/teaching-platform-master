package com.xiyou.main.service.exam;

import com.xiyou.main.entity.exam.UserProblemGroup;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author tangcan
 * @since 2019-06-25
 */
public interface UserProblemGroupService extends IService<UserProblemGroup> {

    void insertBatch(List<UserProblemGroup> userProblemGroupList);

    boolean deleteByUserId(Integer userId);

    List<UserProblemGroup> getByUserId(Integer userId);

    void save(Integer userId, List<Integer> groups);

    List<UserProblemGroup> getByUserIdList(List<Integer> userIdList);
}
