package com.xiyou.main.service.exam;

import com.xiyou.main.entity.exam.UserVideoGroup;
import com.baomidou.mybatisplus.extension.service.IService;

import java.io.Serializable;
import java.util.Collection;
import java.util.List;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author tangcan
 * @since 2019-06-28
 */
public interface UserVideoGroupService extends IService<UserVideoGroup> {
    void insertBatch(List<UserVideoGroup> userVideoGroupList);

    boolean deleteByUserId(Integer userId);

    void save(Integer userId, List<Integer> groups);

    List<UserVideoGroup> getByUserIdList(List<Integer> userIdList);

    boolean removeByGroupIds(Collection<? extends Serializable> collection);
}
