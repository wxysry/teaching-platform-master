package com.xiyou.main.service.exam;

import com.xiyou.main.entity.exam.SysUser;
import com.xiyou.main.entity.exam.UserVideo;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author tangcan
 * @since 2019-10-28
 */
public interface UserVideoService extends IService<UserVideo> {

    void removeByVideoId(Integer videoId);

    void insertBatch(Integer videoId, List<Integer> studentIdList);

    List<SysUser> getStudents(Integer videoId);
}
