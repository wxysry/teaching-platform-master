package com.xiyou.main.service.exam;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.xiyou.main.entity.exam.VideoCourse;
import com.xiyou.main.params.exam.CourseQueryParam;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author tangcan
 * @since 2019-06-28
 */
public interface VideoCourseService extends IService<VideoCourse> {

    Page<VideoCourse> listByUser(CourseQueryParam param);

    void removeByGroupIds(java.util.Collection<? extends java.io.Serializable> collection);

    Page<VideoCourse> listByStudent(CourseQueryParam param);
}
