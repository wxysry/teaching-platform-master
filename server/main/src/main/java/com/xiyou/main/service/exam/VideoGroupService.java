package com.xiyou.main.service.exam;

import com.xiyou.main.entity.exam.VideoGroup;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author tangcan
 * @since 2019-06-28
 */
public interface VideoGroupService extends IService<VideoGroup> {

    List<VideoGroup> listByTeacher(Integer userId);

    void insert(VideoGroup group);

    List<VideoGroup> listByStudent(Integer studentId);
}
