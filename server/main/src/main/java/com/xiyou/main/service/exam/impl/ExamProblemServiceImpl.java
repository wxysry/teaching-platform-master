package com.xiyou.main.service.exam.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.xiyou.main.entity.exam.ExamProblem;
import com.xiyou.main.dao.exam.ExamProblemMapper;
import com.xiyou.main.service.exam.ExamProblemService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.xiyou.main.vo.exam.ExamAnalysis;
import com.xiyou.main.vo.exam.ExamProblemInfo;
import com.xiyou.main.vo.exam.ExamScore;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author tangcan
 * @since 2019-07-10
 */
@Service
public class ExamProblemServiceImpl extends ServiceImpl<ExamProblemMapper, ExamProblem> implements ExamProblemService {
    @Autowired
    private ExamProblemMapper examProblemMapper;

    @Override
    public int insertBatch(List<ExamProblem> examProblemList) {
        if (examProblemList == null || examProblemList.size() == 0) {
            return 0;
        }
        return examProblemMapper.insertBatch(examProblemList);
    }

    @Override
    public List<ExamProblemInfo> getByExamId(Integer examId) {
        return examProblemMapper.getByExamId(examId);
    }

    @Override
    public void removeByExamIds(List<Integer> examIdList) {
        QueryWrapper<ExamProblem> wrapper = new QueryWrapper<>();
        wrapper.in("exam_id", examIdList);
        this.remove(wrapper);
    }

    @Override
    public boolean update(ExamProblem examProblem) {
        return this.updateById(examProblem);
    }

    @Override
    public int autoJudge(Integer examId) {
        return examProblemMapper.autoJudge(examId);
    }

    @Override
    public void autoJudgeByExamIds(List<Integer> examIdList) {
        examProblemMapper.autoJudgeByExamIds(examIdList);
    }

    @Override
    public int updateBatch(List<ExamProblem> examProblemList) {
        if (examProblemList == null || examProblemList.size() == 0) {
            return 0;
        }
        return examProblemMapper.updateBatch(examProblemList);
    }

    @Override
    public ExamProblem getByExamIdAndProblemId(Integer examId, Integer problemId) {
        QueryWrapper<ExamProblem> wrapper = new QueryWrapper<>();
        wrapper.eq("exam_id", examId).eq("problem_id", problemId);
        return this.getOne(wrapper);
    }

    @Override
    public List<ExamAnalysis> analysis(Integer paperId) {
        return examProblemMapper.analysis(paperId);
    }

    @Override
    public void removeByPaperId(Integer paperId) {
        QueryWrapper<ExamProblem> wrapper = new QueryWrapper<>();
        wrapper.eq("paper_id", paperId);
        this.remove(wrapper);
    }

    @Override
    public List<ExamScore> getScoreList(Integer paperId) {
        return examProblemMapper.getScoreList(paperId);
    }
}
