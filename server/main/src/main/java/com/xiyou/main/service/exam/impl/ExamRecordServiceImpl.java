package com.xiyou.main.service.exam.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.xiyou.main.entity.exam.ExamRecord;
import com.xiyou.main.dao.exam.ExamRecordMapper;
import com.xiyou.main.service.exam.ExamRecordService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author tangcan
 * @since 2019-07-10
 */
@Service
public class ExamRecordServiceImpl extends ServiceImpl<ExamRecordMapper, ExamRecord> implements ExamRecordService {
    @Autowired
    private ExamRecordMapper examRecordMapper;

    @Override
    public List<ExamRecord> getByExamId(Integer examId) {
        QueryWrapper<ExamRecord> wrapper = new QueryWrapper<>();
        wrapper.eq("exam_id", examId);
        return this.list(wrapper);
    }

    @Override
    public ExamRecord getCurrent(Integer examId) {
        QueryWrapper<ExamRecord> wrapper = new QueryWrapper<>();
        wrapper.eq("exam_id", examId)
                .orderByDesc("id");
        return this.getOne(wrapper);
    }

    @Override
    public void removeByExamIds(List<Integer> examIdList) {
        QueryWrapper<ExamRecord> wrapper = new QueryWrapper<>();
        wrapper.in("exam_id", examIdList);
        this.remove(wrapper);
    }

    @Override
    public List<ExamRecord> listByPaperId(Integer paperId) {
        return examRecordMapper.listByPaperId(paperId);
    }
}
