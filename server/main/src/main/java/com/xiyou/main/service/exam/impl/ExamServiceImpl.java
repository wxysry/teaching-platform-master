package com.xiyou.main.service.exam.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.xiyou.main.entity.exam.Exam;
import com.xiyou.main.dao.exam.ExamMapper;
import com.xiyou.main.params.exam.ExamPaperParam;
import com.xiyou.main.params.exam.ExamStudentParam;
import com.xiyou.main.service.exam.ExamService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.xiyou.main.vo.exam.ExamPaper;
import com.xiyou.main.vo.exam.ExamStudent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author tangcan
 * @since 2019-07-10
 */
@Service
public class ExamServiceImpl extends ServiceImpl<ExamMapper, Exam> implements ExamService {
    @Autowired
    private ExamMapper examMapper;

    @Override
    public int insertBatch(List<Exam> examList) {
        if (examList == null || examList.size() == 0) {
            return 0;
        }
        return examMapper.insertBatch(examList);
    }

    @Override
    public Page<ExamPaper> getPage(ExamPaperParam examPaperParam) {
        Page<ExamPaper> page = new Page<>(examPaperParam.getPage(), examPaperParam.getLimit());
        return examMapper.getExamPaperPage(page, examPaperParam);
    }

    @Override
    public ExamPaper getByExamId(Integer examId) {
        return examMapper.getByExamId(examId);
    }

    @Override
    public boolean update(Exam exam) {
        return this.updateById(exam);
    }

    @Override
    public Page<ExamStudent> getStudentList(ExamStudentParam examStudentParam) {
        Page<ExamStudent> page = new Page<>(examStudentParam.getPage(), examStudentParam.getLimit());
        return examMapper.getStudentList(page, examStudentParam);
    }

    @Override
    public void updateExamTime(Integer paperId, Integer examTime) {
        examMapper.updateExamTime(paperId, examTime);
    }

    @Override
    public List<Exam> getByPaperId(Integer paperId) {
        QueryWrapper<Exam> wrapper = new QueryWrapper<>();
        wrapper.eq("paper_id", paperId);
        return this.list(wrapper);
    }

    @Override
    public int getHandInStudentNum(Integer paperId) {
        QueryWrapper<Exam> wrapper = new QueryWrapper<>();
        wrapper.eq("paper_id", paperId).eq("is_hand_in", 1);
        return this.count(wrapper);
    }

    @Override
    public void handInByExamIds(List<Integer> examIdList) {
        if (examIdList == null || examIdList.size() == 0) {
            return;
        }
        Exam exam = new Exam().setIsHandIn(1);
        UpdateWrapper<Exam> wrapper = new UpdateWrapper<>();
        wrapper.in("id", examIdList);
        this.update(exam, wrapper);
    }
}
