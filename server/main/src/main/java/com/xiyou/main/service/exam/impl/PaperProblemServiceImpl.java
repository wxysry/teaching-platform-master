package com.xiyou.main.service.exam.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.xiyou.main.entity.exam.PaperProblem;
import com.xiyou.main.dao.exam.PaperProblemMapper;
import com.xiyou.main.service.exam.PaperProblemService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.xiyou.main.vo.exam.ExamProblemInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author tangcan
 * @since 2019-07-04
 */
@Service
public class PaperProblemServiceImpl extends ServiceImpl<PaperProblemMapper, PaperProblem> implements PaperProblemService {
    @Autowired
    private PaperProblemMapper paperProblemMapper;

    @Override
    public int insertBatch(List<PaperProblem> paperProblemList) {
        return paperProblemMapper.insertBatch(paperProblemList);
    }

    @Override
    public void deleteAllProblem(Integer paperId) {
        QueryWrapper<PaperProblem> wrapper = new QueryWrapper<>();
        wrapper.eq("paper_id", paperId);
        this.remove(wrapper);
    }

    @Override
    public Double countTotalScore(Integer paperId) {
        return paperProblemMapper.countTotalScore(paperId);
    }

    @Override
    public List<PaperProblem> get(PaperProblem paperProblem) {
        QueryWrapper<PaperProblem> wrapper = new QueryWrapper<>();
        if (paperProblem.getId() != null) {
            wrapper.eq("id", paperProblem.getId());
        }
        if (paperProblem.getPaperId() != null) {
            wrapper.eq("paper_id", paperProblem.getPaperId());
        }
        if (paperProblem.getProblemId() != null) {
            wrapper.eq("problem_id", paperProblem.getProblemId());
        }
        return this.list(wrapper);
    }

    @Override
    public void removeByPaperId(Integer paperId) {
        QueryWrapper<PaperProblem> wrapper = new QueryWrapper<>();
        wrapper.eq("paper_id", paperId);
        this.remove(wrapper);
    }

    @Override
    public List<ExamProblemInfo> listByPaperId(Integer paperId) {
        return paperProblemMapper.listByPaperId(paperId);
    }
}
