package com.xiyou.main.service.exam.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.xiyou.main.entity.exam.Paper;
import com.xiyou.main.dao.exam.PaperMapper;
import com.xiyou.main.params.exam.PaperParam;
import com.xiyou.main.service.exam.PaperService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author tangcan
 * @since 2019-07-04
 */
@Service
public class PaperServiceImpl extends ServiceImpl<PaperMapper, Paper> implements PaperService {
    @Autowired
    private PaperMapper paperMapper;

    @Override
    public int insert(Paper paper) {
        return paperMapper.insert(paper);
    }


    @Override
    public boolean update(Paper paper) {
        return this.updateById(paper);
    }

    @Override
    public Page<Paper> getPage(PaperParam paperParam) {
        Page<Paper> page = new Page<>(paperParam.getPage(), paperParam.getLimit());
        return paperMapper.getPage(page, paperParam);
    }

    @Override
    public Paper getByPaperIdAndTeacherId(Integer paperId, Integer teacherId) {
        QueryWrapper<Paper> wrapper = new QueryWrapper<>();
        wrapper.eq("id", paperId).eq("teacher_id", teacherId);
        return this.getOne(wrapper);
    }
}
