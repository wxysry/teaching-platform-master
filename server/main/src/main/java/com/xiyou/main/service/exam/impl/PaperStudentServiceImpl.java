package com.xiyou.main.service.exam.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.xiyou.main.entity.exam.PaperStudent;
import com.xiyou.main.dao.exam.PaperStudentMapper;
import com.xiyou.main.service.exam.PaperStudentService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author tangcan
 * @since 2019-07-16
 */
@Service
public class PaperStudentServiceImpl extends ServiceImpl<PaperStudentMapper, PaperStudent> implements PaperStudentService {
    @Autowired
    private PaperStudentMapper paperStudentMapper;

    @Override
    public boolean removeByPaperId(Integer paperId) {
        QueryWrapper<PaperStudent> wrapper = new QueryWrapper<>();
        wrapper.eq("paper_id", paperId);
        return this.remove(wrapper);
    }

    @Override
    public int insertBatch(List<PaperStudent> paperStudentList) {
        if (paperStudentList == null || paperStudentList.size() == 0) {
            return 0;
        }
        return paperStudentMapper.insertBatch(paperStudentList);
    }

    @Override
    public List<Integer> getStudentIdsByPaperId(Integer paperId) {
        return paperStudentMapper.getStudentIdsByPaperId(paperId);
    }
}
