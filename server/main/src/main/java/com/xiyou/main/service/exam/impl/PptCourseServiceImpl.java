package com.xiyou.main.service.exam.impl;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.xiyou.main.entity.exam.PptCourse;
import com.xiyou.main.dao.exam.PptCourseMapper;
import com.xiyou.main.entity.exam.VideoCourse;
import com.xiyou.main.params.exam.CourseQueryParam;
import com.xiyou.main.service.exam.PptCourseService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author tangcan
 * @since 2019-06-28
 */
@Service
public class PptCourseServiceImpl extends ServiceImpl<PptCourseMapper, PptCourse> implements PptCourseService {

    @Autowired
    PptCourseMapper pptCourseMapper;

    @Override
    public Page<PptCourse> listByUser(CourseQueryParam param) {
        Page<PptCourse> page = new Page<>(param.getPage(), param.getLimit());
        return pptCourseMapper.listByUser(page, param);
    }

    @Override
    public boolean removeByGroupIds(java.util.Collection<? extends java.io.Serializable> collection) {
        return pptCourseMapper.removeByGroupIds(collection);
    }

    @Override
    public Page<PptCourse> listByStudent(CourseQueryParam param) {
        Page<VideoCourse> page = new Page<>(param.getPage(), param.getLimit());
        return pptCourseMapper.listByStudent(page, param);
    }
}
