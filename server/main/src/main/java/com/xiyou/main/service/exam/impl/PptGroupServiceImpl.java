package com.xiyou.main.service.exam.impl;

import com.xiyou.main.entity.exam.PptGroup;
import com.xiyou.main.dao.exam.PptGroupMapper;
import com.xiyou.main.entity.exam.VideoGroup;
import com.xiyou.main.service.exam.PptGroupService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author tangcan
 * @since 2019-06-28
 */
@Service
public class PptGroupServiceImpl extends ServiceImpl<PptGroupMapper, PptGroup> implements PptGroupService {
    @Autowired
    PptGroupMapper pptGroupMapper;

    @Override
    public List<VideoGroup> listByTeacher(Integer userId) {
        return pptGroupMapper.listByTeacher(userId);
    }

    @Override
    public void insert(PptGroup group) {
        pptGroupMapper.insert(group);
    }

    @Override
    public List<PptGroup> listByStudent(Integer studentId) {
        return pptGroupMapper.listByStudent(studentId);
    }
}
