package com.xiyou.main.service.exam.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.xiyou.main.entity.exam.ProblemGroup;
import com.xiyou.main.dao.exam.ProblemGroupMapper;
import com.xiyou.main.service.exam.ProblemGroupService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author tangcan
 * @since 2019-07-07
 */
@Service
public class ProblemGroupServiceImpl extends ServiceImpl<ProblemGroupMapper, ProblemGroup> implements ProblemGroupService {
    @Autowired
    private ProblemGroupMapper problemGroupMapper;

    @Override
    public String getGroupAndSubgroupName(Integer subgroupId) {
        if (subgroupId == null) {
            return "";
        }
        return problemGroupMapper.getGroupAndSubgroupName(subgroupId);
    }

    @Override
    public List<ProblemGroup> getGroupAndSubgroupNameList(List<Integer> subgroupIdList) {
        if (subgroupIdList == null || subgroupIdList.size() == 0) {
            return new ArrayList<>();
        }
        return problemGroupMapper.getGroupAndSubgroupNameList(subgroupIdList);
    }

    @Override
    public List<ProblemGroup> getSubGroupList(Integer userId, Integer fatherId, Integer subgroupId) {
        return problemGroupMapper.getSubGroupList(userId, fatherId, subgroupId);
    }

    @Override
    public List<ProblemGroup> getList(Integer fatherId) {
        QueryWrapper<ProblemGroup> wrapper = new QueryWrapper<>();
        if (fatherId != null) {
            wrapper.eq("father_id", fatherId);
        } else {
            wrapper.eq("father_id", 0);
        }
        return this.list(wrapper);
    }

    @Override
    public List<ProblemGroup> getGroupListByUserId(Integer userId) {
        return problemGroupMapper.getGroupListByUserId(userId);
    }

    @Override
    public List<ProblemGroup> getAllList(Integer teacherId) {
        List<ProblemGroup> list = problemGroupMapper.getAllList();
        if (teacherId == null) {
            return list;
        }
        List<ProblemGroup> groupList = this.getGroupListByUserId(teacherId);
        Map<Integer, Integer> groupMap = groupList.stream().collect(Collectors.toMap(ProblemGroup::getId, ProblemGroup::getId, (k1, k2) -> k1));
        return list.stream().filter(p -> groupMap.get(p.getId()) != null).collect(Collectors.toList());
    }

    @Override
    public int insert(ProblemGroup problemGroup) {
        return problemGroupMapper.insert(problemGroup);
    }

    @Override
    public boolean checkNameExist(ProblemGroup group) {
        QueryWrapper<ProblemGroup> wrapper = new QueryWrapper<>();
        wrapper.ne("id", group.getId())
                .eq("father_id", group.getFatherId())
                .eq("group_name", group.getGroupName());
        return this.getOne(wrapper) != null;
    }

    @Override
    public void deleteSubGroup(List<Integer> fatherIdList) {
        QueryWrapper<ProblemGroup> wrapper = new QueryWrapper<>();
        wrapper.in("father_id", fatherIdList);
        this.remove(wrapper);
    }
}
