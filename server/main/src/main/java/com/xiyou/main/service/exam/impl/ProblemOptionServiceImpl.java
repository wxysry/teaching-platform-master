package com.xiyou.main.service.exam.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.google.common.collect.Lists;
import com.xiyou.main.constants.Constant;
import com.xiyou.main.entity.exam.ProblemOption;
import com.xiyou.main.dao.exam.ProblemOptionMapper;
import com.xiyou.main.service.exam.ProblemOptionService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author tangcan
 * @since 2019-06-24
 */
@Service
public class ProblemOptionServiceImpl extends ServiceImpl<ProblemOptionMapper, ProblemOption> implements ProblemOptionService {
    @Autowired
    private ProblemOptionMapper problemOptionMapper;

    @Override
    public int insertBatch(List<ProblemOption> optionList) {
        if (optionList == null || optionList.size() == 0) {
            return 0;
        }
        List<List<ProblemOption>> lists = Lists.partition(optionList, Constant.BATCH_SIZE);
        int success = 0;
        for (List<ProblemOption> list : lists) {
            success += problemOptionMapper.insertBatch(list);
        }
        return success;
    }

    @Override
    public List<ProblemOption> getList(Integer problemId) {
        return problemOptionMapper.getList(problemId);
    }

    @Override
    public List<ProblemOption> getByProblemIdList(List<Integer> problemIdList) {
        if (problemIdList == null || problemIdList.size() == 0) {
            return new ArrayList<>();
        }
        return problemOptionMapper.getByProblemIdList(problemIdList);
    }

    @Override
    public void removeByProblemIds(List<Integer> problemIdList) {
        QueryWrapper<ProblemOption> wrapper = new QueryWrapper<>();
        wrapper.in("problem_id", problemIdList);
        this.remove(wrapper);
    }
}
