package com.xiyou.main.service.exam.impl;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.google.common.collect.Lists;
import com.xiyou.main.constants.Constant;
import com.xiyou.main.entity.exam.Problem;
import com.xiyou.main.dao.exam.ProblemMapper;
import com.xiyou.main.params.exam.ProblemInfo;
import com.xiyou.main.params.exam.ProblemParam;
import com.xiyou.main.service.exam.ProblemService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author tangcan
 * @since 2019-06-24
 */
@Service
public class ProblemServiceImpl extends ServiceImpl<ProblemMapper, Problem> implements ProblemService {
    @Autowired
    private ProblemMapper problemMapper;

    @Override
    public int insertBatch(List<Problem> problemList) {
        if (problemList == null || problemList.size() == 0) {
            return 0;
        }
        List<List<Problem>> lists = Lists.partition(problemList, Constant.BATCH_SIZE);
        int success = 0;
        for (List<Problem> list : lists) {
            success += problemMapper.insertBatch(list);
        }
        return success;
    }

    @Override
    public Problem selectById(Integer id) {
        return problemMapper.selectById(id);
    }

    @Override
    public Page<ProblemInfo> getList(Integer teacherId, ProblemParam problemParam) {
        Page<Problem> page = new Page<>(problemParam.getPage(), problemParam.getLimit());
        return problemMapper.getList(page, teacherId, problemParam);
    }

    @Override
    public Problem getByTeacher(Integer teacherId, Integer problemId) {
        return problemMapper.getByTeacher(teacherId, problemId);
    }

    @Override
    public boolean update(Problem problem) {
        if (problem == null) {
            return false;
        }
        return problemMapper.update(problem);
    }
}
