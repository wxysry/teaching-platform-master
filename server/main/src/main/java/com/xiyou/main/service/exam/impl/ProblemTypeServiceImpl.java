package com.xiyou.main.service.exam.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.xiyou.main.entity.exam.ProblemType;
import com.xiyou.main.dao.exam.ProblemTypeMapper;
import com.xiyou.main.service.exam.ProblemTypeService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author tangcan
 * @since 2019-06-24
 */
@Service
public class ProblemTypeServiceImpl extends ServiceImpl<ProblemTypeMapper, ProblemType> implements ProblemTypeService {
    @Override
    public List<ProblemType> getList() {
        QueryWrapper<ProblemType> wrapper = new QueryWrapper<>();
        wrapper.orderByAsc("type_code");
        return this.list(wrapper);
    }
}
