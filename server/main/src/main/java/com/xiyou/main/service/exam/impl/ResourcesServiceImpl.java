package com.xiyou.main.service.exam.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.xiyou.main.entity.exam.Resources;
import com.xiyou.main.dao.exam.ResourcesMapper;
import com.xiyou.main.params.exam.ResourcesParam;
import com.xiyou.main.service.exam.ResourcesService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author tangcan
 * @since 2019-07-07
 */
@Service
public class ResourcesServiceImpl extends ServiceImpl<ResourcesMapper, Resources> implements ResourcesService {
    @Autowired
    private ResourcesMapper resourcesMapper;

    @Override
    public List<Resources> getList(List<String> fileList, Integer type) {
        QueryWrapper<Resources> wrapper = new QueryWrapper<>();
        if (type != null) {
            wrapper.eq("type", type);
        }
        if (fileList != null && fileList.size() > 0) {
            wrapper.in("file_name", fileList);
        }
        return this.list(wrapper);
    }

    @Override
    public List<Resources> getByFileNameList(List<String> fileNameList) {
        if (fileNameList == null || fileNameList.size() == 0) {
            return new ArrayList<>();
        }
        QueryWrapper<Resources> wrapper = new QueryWrapper<>();
        wrapper.select("file_name", "id", "intro", "original_name", "upload_user_id");
        wrapper.in("file_name", fileNameList);
        return this.list(wrapper);
    }

    @Override
    public int insertBatch(List<Resources> resourcesList) {
        if (resourcesList == null || resourcesList.size() == 0) {
            return 0;
        }
        return resourcesMapper.insertBatch(resourcesList);
    }

    @Override
    public Page<Resources> getPage(ResourcesParam resourcesParam) {
        Page<Resources> page = new Page<>(resourcesParam.getPage(), resourcesParam.getLimit());
        return resourcesMapper.getPage(page, resourcesParam);
    }

    @Override
    public boolean check(String attachment) {
        QueryWrapper<Resources> wrapper = new QueryWrapper<>();
        wrapper.eq("file_name", attachment);
        return this.getOne(wrapper) != null;
    }
}
