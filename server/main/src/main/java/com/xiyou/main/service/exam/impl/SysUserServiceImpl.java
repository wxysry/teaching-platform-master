package com.xiyou.main.service.exam.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.google.common.collect.Lists;
import com.xiyou.common.enums.CodeEnum;
import com.xiyou.common.exception.CustomException;
import com.xiyou.common.utils.CommonUtil;
import com.xiyou.main.async.exam.SysUserAsyncService;
import com.xiyou.main.constants.Constant;
import com.xiyou.main.dao.exam.SysUserMapper;
import com.xiyou.main.entity.exam.SysUser;
import com.xiyou.main.params.exam.UserParam;
import com.xiyou.main.service.exam.SysUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author tangcan
 * @since 2019-06-24
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class SysUserServiceImpl extends ServiceImpl<SysUserMapper, SysUser> implements SysUserService {
    @Autowired
    private SysUserMapper sysUserMapper;
    @Autowired
    private SysUserAsyncService sysUserAsyncService;

    @Override
    public boolean insert(SysUser sysUser) {
        if (sysUserMapper.insert(sysUser) > 0) {
            return true;
        }
        return false;
    }

    @Override
    public Page<SysUser> getTeacherPage(UserParam userParam) {
        Page<SysUser> page = new Page<>(userParam.getPage(), userParam.getLimit());
        return sysUserMapper.getTeacherPage(page, userParam);
    }

    @Override
    public SysUser getAllInfo(SysUser sysUser) {
        return sysUserMapper.getAllInfo(sysUser);
    }

    @Override
    public SysUser get(SysUser sysUser) {
        QueryWrapper<SysUser> wrapper = new QueryWrapper<>();
        if (sysUser.getId() != null) {
            wrapper.eq("id", sysUser.getId());
        }
        if (sysUser.getAccount() != null) {
            wrapper.eq("account", sysUser.getAccount());
        }
        if (sysUser.getEmail() != null) {
            wrapper.eq("email", sysUser.getEmail());
        }
        if (sysUser.getPhone() != null) {
            wrapper.eq("phone", sysUser.getPhone());
        }
        return this.getOne(wrapper);
    }

    @Override
    public boolean forbid(Integer userId) {
        if (sysUserMapper.forbid(userId) > 0) {
            return true;
        }
        return false;
    }

    @Override
    public boolean updatePwd(Integer userId, String password) {
        if (sysUserMapper.updatePwd(userId, password) > 0) {
            return true;
        }
        return false;
    }

    @Override
    public List<SysUser> getByEmailList(List<String> emailList) {
        if (emailList == null || emailList.size() == 0) {
            return new ArrayList<>();
        }
        List<SysUser> sysUserList = new ArrayList<>(CommonUtil.getListInitCap(emailList.size()));
        List<List<String>> lists = Lists.partition(emailList, 200);
        int size = lists.size();
        // 异步查找
        Future<List<SysUser>>[] futures = new Future[size];
        for (int i = 0; i < size; i++) {
            List<String> list = lists.get(i);
            futures[i] = sysUserAsyncService.getByEmailList(list);
        }
        // 等待执行结束
        while (true) {
            boolean done = true;
            for (int i = 0; i < size; i++) {
                if (!futures[i].isDone()) {
                    done = false;
                    break;
                }
            }
            if (done) {
                break;
            }
        }
        for (int i = 0; i < size; i++) {
            List<SysUser> users = null;
            try {
                users = futures[i].get();
            } catch (InterruptedException | ExecutionException e) {
                throw new CustomException(CodeEnum.OTHER_ERROR.getCode(), e.getMessage());
            }
            if (users == null || users.size() == 0) {
                continue;
            }
            sysUserList.addAll(users);
        }
        return sysUserList;
    }



    @Override
    public List<SysUser> getByAccountList(List<String> accountList) {
        QueryWrapper<SysUser> wrapper = new QueryWrapper<>();
        wrapper.select("id", "account", "email").in("account", accountList);
        List<SysUser> sysUserList = this.list(wrapper);
        return sysUserList;
    }



    @Override
    public int insertBatch(List<SysUser> studentList) {
        if (studentList == null || studentList.size() == 0) {
            return 0;
        }
        List<List<SysUser>> lists = Lists.partition(studentList, Constant.BATCH_SIZE);
        int success = 0;
        for (List<SysUser> list : lists) {
            success += sysUserMapper.insertBatch(list);
        }
        return success;
    }

    @Override
    public boolean setForbid(Integer userId, Integer forbid) {
        return sysUserMapper.setForbid(userId, forbid);
    }

    @Override
    public Page<SysUser> getStudentPage(UserParam userParam) {
        Page<SysUser> page = new Page<>(userParam.getPage(), userParam.getLimit());
        return sysUserMapper.getStudentPage(page, userParam);
    }

    @Override
    public List<SysUser> getStudentList(Integer teacherId) {
        return sysUserMapper.getStudentList(teacherId);
    }


    @Override
    public List<SysUser> getListByAccountList(List<String> accountList) {
        if (accountList == null || accountList.isEmpty()) {
            return new ArrayList<>();
        }
        QueryWrapper<SysUser> wrapper = new QueryWrapper<>();
        wrapper.in("account", accountList);
        return this.list(wrapper);
    }

}
