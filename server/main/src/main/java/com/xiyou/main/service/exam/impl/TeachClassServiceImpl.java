package com.xiyou.main.service.exam.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.google.common.collect.Lists;
import com.xiyou.main.constants.Constant;
import com.xiyou.main.entity.exam.TeachClass;
import com.xiyou.main.dao.exam.TeachClassMapper;
import com.xiyou.main.service.exam.TeachClassService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author tangcan
 * @since 2019-06-26
 */
@Service
public class TeachClassServiceImpl extends ServiceImpl<TeachClassMapper, TeachClass> implements TeachClassService {
    @Autowired
    private TeachClassMapper teachClassMapper;

    @Override
    public int count(TeachClass teachClass) {
        if (teachClass == null) {
            return 0;
        }
        QueryWrapper<TeachClass> wrapper = new QueryWrapper<>();
        wrapper.eq("teacher_id", teachClass.getTeacherId());
        return this.count(wrapper);
    }

    @Override
    public int insertBatch(List<TeachClass> teachClassList) {
        if (teachClassList == null || teachClassList.size() == 0) {
            return 0;
        }
        List<List<TeachClass>> lists = Lists.partition(teachClassList, Constant.BATCH_SIZE);
        int success = 0;
        for (List<TeachClass> list : lists) {
            success += teachClassMapper.insertBatch(list);
        }
        return success;
    }

    @Override
    public List<Integer> getStudentIdList(Integer teacherId) {
        return teachClassMapper.getStudentIdList(teacherId);
    }
}
