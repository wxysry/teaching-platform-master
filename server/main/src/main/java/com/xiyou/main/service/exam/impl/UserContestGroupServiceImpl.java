package com.xiyou.main.service.exam.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.xiyou.main.entity.contest.ContestGroup;
import com.xiyou.main.entity.contest.UserContestGroup;
import com.xiyou.main.dao.contest.UserContestGroupMapper;
import com.xiyou.main.entity.exam.UserPptGroup;
import com.xiyou.main.service.exam.UserContestGroupService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author tangcan
 * @since 2019-07-09
 */
@Service
public class UserContestGroupServiceImpl extends ServiceImpl<UserContestGroupMapper, UserContestGroup> implements UserContestGroupService {
    @Autowired
    private UserContestGroupMapper userContestGroupMapper;

    @Override
    public List<ContestGroup> listAll() {
        return userContestGroupMapper.listAll();
    }

    @Override
    public List<UserContestGroup> getByUserIdList(List<Integer> userIdList) {
        if (userIdList == null || userIdList.size() == 0) {
            return new ArrayList<>();
        }
        return userContestGroupMapper.getByUserIdList(userIdList);
    }

    @Override
    public void save(Integer teacherId, List<Integer> groups) {
        // 用户可查看的分组
        List<UserContestGroup> userContestGroupList = new ArrayList<>();

        // PPT教程
        if (groups != null) {
            // 遍历分组id列表
            for (Integer groupId : groups) {
                // 添加
                userContestGroupList.add(new UserContestGroup()
                        .setUserId(teacherId)
                        .setGroupId(groupId));
            }
        }
        // 先删除以前的分组权限
        this.deleteByUserId(teacherId);
        // 存库
        if (userContestGroupList.size() > 0) {
            // 再加入最新分组权限
            this.insertBatch(userContestGroupList);
        }
    }

    private int insertBatch(List<UserContestGroup> userContestGroupList) {
        if (userContestGroupList == null || userContestGroupList.size() == 0) {
            return 0;
        }
        return userContestGroupMapper.insertBatch(userContestGroupList);
    }

    private void deleteByUserId(Integer teacherId) {
        QueryWrapper<UserContestGroup> wrapper = new QueryWrapper<>();
        wrapper.eq("user_id", teacherId);
        this.remove(wrapper);
    }
}
