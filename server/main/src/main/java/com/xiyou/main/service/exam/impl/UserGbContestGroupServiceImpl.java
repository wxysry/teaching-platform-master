package com.xiyou.main.service.exam.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.xiyou.main.dao.gbcontest.UserGbContestGroupMapper;
import com.xiyou.main.entity.gbcontest.GbContestGroup;
import com.xiyou.main.entity.gbcontest.UserGbContestGroup;
import com.xiyou.main.service.exam.UserGbContestGroupService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author wangxingyu
 * @since 2023-06-05
 */
@Service
public class UserGbContestGroupServiceImpl extends ServiceImpl<UserGbContestGroupMapper, UserGbContestGroup> implements UserGbContestGroupService {
    @Autowired
    private UserGbContestGroupMapper userGbContestGroupMapper;


    @Override
    public List<UserGbContestGroup> getByUserIdList(List<Integer> userIdList) {
        if (userIdList == null || userIdList.size() == 0) {
            return new ArrayList<>();
        }
        return userGbContestGroupMapper.getByUserIdList(userIdList);
    }

    @Override
    public void save(Integer teacherId, List<Integer> groups) {
        // 用户可查看的分组
        List<UserGbContestGroup> userGbContestGroupList = new ArrayList<>();

        // PPT教程
        if (groups != null) {
            // 遍历分组id列表
            for (Integer groupId : groups) {
                // 添加
                userGbContestGroupList.add(new UserGbContestGroup()
                        .setUserId(teacherId)
                        .setGroupId(groupId));
            }
        }
        // 先删除以前的分组权限
        this.deleteByUserId(teacherId);
        // 存库
        if (userGbContestGroupList.size() > 0) {
            // 再加入最新分组权限
            this.insertBatch(userGbContestGroupList);
        }
    }

    private void insertBatch(List<UserGbContestGroup> userGbContestGroupList) {
        if (userGbContestGroupList == null || userGbContestGroupList.size() == 0) {
            return;
        }
        userGbContestGroupMapper.insertBatch(userGbContestGroupList);
    }

    private void deleteByUserId(Integer teacherId) {
        QueryWrapper<UserGbContestGroup> wrapper = new QueryWrapper<>();
        wrapper.eq("user_id", teacherId);
        this.remove(wrapper);
    }
}
