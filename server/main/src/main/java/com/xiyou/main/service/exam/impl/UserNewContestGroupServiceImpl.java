package com.xiyou.main.service.exam.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.xiyou.main.dao.newcontest.UserNewContestGroupMapper;
import com.xiyou.main.entity.newcontest.NewContestGroup;
import com.xiyou.main.entity.newcontest.UserNewContestGroup;
import com.xiyou.main.service.exam.UserNewContestGroupService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author wangxingyu
 * @since 2023-06-05
 */
@Service
public class UserNewContestGroupServiceImpl extends ServiceImpl<UserNewContestGroupMapper, UserNewContestGroup> implements UserNewContestGroupService {
    @Autowired
    private UserNewContestGroupMapper userNewContestGroupMapper;

    @Override
    public List<NewContestGroup> listAll() {
        return userNewContestGroupMapper.listAll();
    }

    @Override
    public List<UserNewContestGroup> getByUserIdList(List<Integer> userIdList) {
        if (userIdList == null || userIdList.size() == 0) {
            return new ArrayList<>();
        }
        return userNewContestGroupMapper.getByUserIdList(userIdList);
    }

    @Override
    public void save(Integer teacherId, List<Integer> groups) {
        // 用户可查看的分组
        List<UserNewContestGroup> userNewContestGroupList = new ArrayList<>();

        // PPT教程
        if (groups != null) {
            // 遍历分组id列表
            for (Integer groupId : groups) {
                // 添加
                userNewContestGroupList.add(new UserNewContestGroup()
                        .setUserId(teacherId)
                        .setGroupId(groupId));
            }
        }
        // 先删除以前的分组权限
        this.deleteByUserId(teacherId);
        // 存库
        if (userNewContestGroupList.size() > 0) {
            // 再加入最新分组权限
            this.insertBatch(userNewContestGroupList);
        }
    }

    private int insertBatch(List<UserNewContestGroup> userNewContestGroupList) {
        if (userNewContestGroupList == null || userNewContestGroupList.size() == 0) {
            return 0;
        }
        return userNewContestGroupMapper.insertBatch(userNewContestGroupList);
    }

    private void deleteByUserId(Integer teacherId) {
        QueryWrapper<UserNewContestGroup> wrapper = new QueryWrapper<>();
        wrapper.eq("user_id", teacherId);
        this.remove(wrapper);
    }
}
