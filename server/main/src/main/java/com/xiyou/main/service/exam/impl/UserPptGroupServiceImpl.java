package com.xiyou.main.service.exam.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.xiyou.main.dao.exam.UserPptGroupMapper;
import com.xiyou.main.entity.exam.UserPptGroup;
import com.xiyou.main.service.exam.UserPptGroupService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author tangcan
 * @since 2019-06-28
 */
@Service
public class UserPptGroupServiceImpl extends ServiceImpl<UserPptGroupMapper, UserPptGroup> implements UserPptGroupService {
    @Autowired
    private UserPptGroupMapper userPptGroupMapper;

    @Override
    public void insertBatch(List<UserPptGroup> userPptGroupList) {
        if (userPptGroupList == null || userPptGroupList.size() == 0) {
            return;
        }
        userPptGroupMapper.insertBatch(userPptGroupList);
    }

    @Override
    public boolean deleteByUserId(Integer userId) {
        QueryWrapper<UserPptGroup> wrapper = new QueryWrapper<>();
        wrapper.eq("user_id", userId);
        return this.remove(wrapper);
    }

    @Override
    public void save(Integer userId, List<Integer> groups) {
        // 用户可查看的分组
        List<UserPptGroup> userPptGroupList = new ArrayList<>();

        // PPT教程
        if (groups != null) {
            // 遍历分组id列表
            for (Integer groupId : groups) {
                // 添加
                userPptGroupList.add(new UserPptGroup()
                        .setUserId(userId)
                        .setGroupId(groupId));
            }
        }
        // 先删除以前的分组权限
        this.deleteByUserId(userId);
        // 存库
        if (userPptGroupList.size() > 0) {
            // 再加入最新分组权限
            this.insertBatch(userPptGroupList);
        }
    }

    @Override
    public List<UserPptGroup> getByUserIdList(List<Integer> userIdList) {
        if (userIdList == null || userIdList.size() == 0) {
            return new ArrayList<>();
        }
        return userPptGroupMapper.getByUserIdList(userIdList);
 }

    @Override
    public boolean removeByGroupIds(Collection<? extends Serializable> collection) {
        return userPptGroupMapper.removeByGroupIds(collection);
    }
}
