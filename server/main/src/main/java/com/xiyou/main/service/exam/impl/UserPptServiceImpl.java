package com.xiyou.main.service.exam.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.xiyou.main.entity.exam.SysUser;
import com.xiyou.main.entity.exam.UserPpt;
import com.xiyou.main.dao.exam.UserPptMapper;
import com.xiyou.main.service.exam.UserPptService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author tangcan
 * @since 2019-10-28
 */
@Service
public class UserPptServiceImpl extends ServiceImpl<UserPptMapper, UserPpt> implements UserPptService {
    @Autowired
    private UserPptMapper userPptMapper;

    @Override
    public void removeByPptId(Integer pptId) {
        QueryWrapper<UserPpt> wrapper = new QueryWrapper<>();
        wrapper.eq("ppt_id", pptId);
        this.remove(wrapper);
    }

    @Override
    public void insertBatch(Integer pptId, List<Integer> studentIdList) {
        if (studentIdList == null || studentIdList.isEmpty()) {
            return;
        }
        userPptMapper.insertBatch(pptId, studentIdList);
    }

    @Override
    public List<SysUser> getStudents(Integer pptId) {
        return userPptMapper.getStudents(pptId);
    }
}
