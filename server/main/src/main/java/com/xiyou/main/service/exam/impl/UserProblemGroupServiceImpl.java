package com.xiyou.main.service.exam.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.xiyou.main.entity.exam.UserProblemGroup;
import com.xiyou.main.dao.exam.UserProblemGroupMapper;
import com.xiyou.main.service.exam.UserProblemGroupService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author tangcan
 * @since 2019-06-25
 */
@Service
public class UserProblemGroupServiceImpl extends ServiceImpl<UserProblemGroupMapper, UserProblemGroup> implements UserProblemGroupService {
    @Autowired
    private UserProblemGroupMapper userProblemGroupMapper;

    @Override
    public void insertBatch(List<UserProblemGroup> userProblemGroupList) {
        if (userProblemGroupList == null || userProblemGroupList.size() == 0) {
            return;
        }
        userProblemGroupMapper.insertBatch(userProblemGroupList);
    }

    @Override
    public boolean deleteByUserId(Integer userId) {
        QueryWrapper<UserProblemGroup> wrapper = new QueryWrapper<>();
        wrapper.eq("user_id", userId);
        return this.remove(wrapper);
    }

    @Override
    public List<UserProblemGroup> getByUserId(Integer userId) {
        QueryWrapper<UserProblemGroup> wrapper = new QueryWrapper<>();
        wrapper.eq("user_id", userId);
        return this.list(wrapper);
    }

    @Override
    public void save(Integer userId, List<Integer> groups) {
        if (groups != null) {
            List<UserProblemGroup> userProblemGroupList = new ArrayList<>();
            // 遍历考题的分组id列表
            for (Integer groupId : groups) {
                // 添加
                userProblemGroupList.add(new UserProblemGroup()
                        .setGroupId(groupId)
                        .setUserId(userId));
            }
            // 先删除以前的分组权限
            this.deleteByUserId(userId);
            if (userProblemGroupList.size() > 0) {
                // 再加入最新分组权限
                this.insertBatch(userProblemGroupList);
            }
        }
    }

    @Override
    public List<UserProblemGroup> getByUserIdList(List<Integer> userIdList) {
        if (userIdList == null || userIdList.size() == 0) {
            return new ArrayList<>();
        }
        return userProblemGroupMapper.getByUserIdList(userIdList);
    }
}
