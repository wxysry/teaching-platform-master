package com.xiyou.main.service.exam.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.xiyou.main.entity.exam.UserVideoGroup;
import com.xiyou.main.dao.exam.UserVideoGroupMapper;
import com.xiyou.main.service.exam.UserVideoGroupService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author tangcan
 * @since 2019-06-28
 */
@Service
public class UserVideoGroupServiceImpl extends ServiceImpl<UserVideoGroupMapper, UserVideoGroup> implements UserVideoGroupService {
    @Autowired
    private UserVideoGroupMapper userVideoGroupMapper;

    @Override
    public void insertBatch(List<UserVideoGroup> userVideoGroupList) {
        if (userVideoGroupList == null || userVideoGroupList.size() == 0) {
            return;
        }
        userVideoGroupMapper.insertBatch(userVideoGroupList);
    }

    @Override
    public boolean deleteByUserId(Integer userId) {
        QueryWrapper<UserVideoGroup> wrapper = new QueryWrapper<>();
        wrapper.eq("user_id", userId);
        return this.remove(wrapper);
    }

    @Override
    public void save(Integer userId, List<Integer> groups) {
        List<UserVideoGroup> userVideoGroupList = new ArrayList<>();
        // 视频教程
        if (groups != null) {
            // 遍历分组id列表
            for (Integer groupId : groups) {
                // 添加
                userVideoGroupList.add(new UserVideoGroup()
                        .setUserId(userId)
                        .setGroupId(groupId));
            }
        }
        // 先删除以前的分组权限
        this.deleteByUserId(userId);
        if (userVideoGroupList.size() > 0) {
            // 再加入最新分组权限
            this.insertBatch(userVideoGroupList);
        }
    }

    @Override
    public List<UserVideoGroup> getByUserIdList(List<Integer> userIdList) {
        if (userIdList == null || userIdList.size() == 0) {
            return new ArrayList<>();
        }
        return userVideoGroupMapper.getByUserIdList(userIdList);
    }

    @Override
    public boolean removeByGroupIds(Collection<? extends Serializable> collection) {
        return userVideoGroupMapper.removeByGroupIds(collection);
    }
}
