package com.xiyou.main.service.exam.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.xiyou.main.entity.exam.SysUser;
import com.xiyou.main.entity.exam.UserVideo;
import com.xiyou.main.dao.exam.UserVideoMapper;
import com.xiyou.main.service.exam.UserVideoService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author tangcan
 * @since 2019-10-28
 */
@Service
public class UserVideoServiceImpl extends ServiceImpl<UserVideoMapper, UserVideo> implements UserVideoService {
    @Autowired
    private UserVideoMapper userVideoMapper;

    @Override
    public void removeByVideoId(Integer videoId) {
        QueryWrapper<UserVideo> wrapper = new QueryWrapper<>();
        wrapper.eq("video_id", videoId);
        this.remove(wrapper);
    }

    @Override
    public void insertBatch(Integer videoId, List<Integer> studentIdList) {
        if (studentIdList == null || studentIdList.isEmpty()) {
            return;
        }
        userVideoMapper.insertBatch(videoId, studentIdList);
    }

    @Override
    public List<SysUser> getStudents(Integer videoId) {
        return userVideoMapper.getStudents(videoId);
    }
}
