package com.xiyou.main.service.exam.impl;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.xiyou.main.entity.exam.VideoCourse;
import com.xiyou.main.dao.exam.VideoCourseMapper;
import com.xiyou.main.params.exam.CourseQueryParam;
import com.xiyou.main.service.exam.VideoCourseService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author tangcan
 * @since 2019-06-28
 */
@Service
public class VideoCourseServiceImpl extends ServiceImpl<VideoCourseMapper, VideoCourse> implements VideoCourseService {
    @Autowired
    VideoCourseMapper videoCourseMapper;

    @Override
    public Page<VideoCourse> listByUser(CourseQueryParam param) {
        Page<VideoCourse> page = new Page<>(param.getPage(), param.getLimit());
        return videoCourseMapper.listByUser(page, param);
    }

    @Override
    public void removeByGroupIds(java.util.Collection<? extends java.io.Serializable> collection) {
        videoCourseMapper.removeByGroupId(collection);
    }

    @Override
    public Page<VideoCourse> listByStudent(CourseQueryParam param) {
        Page<VideoCourse> page = new Page<>(param.getPage(), param.getLimit());
        return videoCourseMapper.listByStudent(page, param);
    }
}
