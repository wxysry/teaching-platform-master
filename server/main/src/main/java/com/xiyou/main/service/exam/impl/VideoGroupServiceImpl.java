package com.xiyou.main.service.exam.impl;

import com.xiyou.main.entity.exam.VideoGroup;
import com.xiyou.main.dao.exam.VideoGroupMapper;
import com.xiyou.main.service.exam.VideoGroupService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author tangcan
 * @since 2019-06-28
 */
@Service
public class VideoGroupServiceImpl extends ServiceImpl<VideoGroupMapper, VideoGroup> implements VideoGroupService {
    @Autowired
    VideoGroupMapper videoGroupMapper;

    @Override
    public List<VideoGroup> listByTeacher(Integer userId) {
        return videoGroupMapper.listByTeacher(userId);
    }

    @Override
    public void insert(VideoGroup group) {
        videoGroupMapper.insert(group);
    }

    @Override
    public List<VideoGroup> listByStudent(Integer studentId) {
        return videoGroupMapper.listByStudent(studentId);
    }
}
