package com.xiyou.main.service.gbcontest;
import com.baomidou.mybatisplus.extension.service.IService;
import com.xiyou.main.entity.gbcontest.GbContestGroup;
import org.springframework.stereotype.Service;
import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author tangcan
 * @since 2019-07-09
 */
@Service
public interface GbContestGroupService extends IService<GbContestGroup> {

    int insert(GbContestGroup gbContestGroup);

    boolean checkNameExist(GbContestGroup gbContestGroup);

    List<GbContestGroup> getListByTeacher(Integer teacherId);
}
