package com.xiyou.main.service.gbcontest;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.xiyou.main.entity.gbcontest.GbContest;
import com.xiyou.main.params.contest.ContestParam;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author tangcan
 * @since 2019-08-25
 */
public interface GbContestService extends IService<GbContest> {

    Page<GbContest> getPage(ContestParam contestParam);

    Page<GbContest> getPageAdmin(ContestParam contestParam);

    int getUnEndContest(Integer teacherId);

    int publish(Integer teacherId, Integer contestId);

    Page<GbContest> getStudentContestPage(ContestParam contestParam);

    boolean isSyn(Integer contestId);
}
