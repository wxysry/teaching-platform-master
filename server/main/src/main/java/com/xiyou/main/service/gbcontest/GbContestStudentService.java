package com.xiyou.main.service.gbcontest;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.xiyou.main.entity.gbcontest.GbContestStudent;
import com.xiyou.main.params.contest.ContestStudentParam;
import com.xiyou.main.vo.contest.ContestScore;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author tangcan
 * @since 2019-08-25
 */
public interface GbContestStudentService extends IService<GbContestStudent> {

    int insertBatch(Integer contestId, List<Integer> studentIds);

    void remove(Integer contestId);

    GbContestStudent get(Integer contestId, Integer studentId);

    Page<GbContestStudent> getPage(ContestStudentParam contestStudentParam);

    List<Integer> getStudentIdList(Integer contestId);

    void updateScore(Integer id, Double score);

    List<ContestScore> getScoreList(Integer contestId);

    void updateErrorReportYear(GbContestStudent gbContestStudent, Integer errorYear);

    void updateErrorReportYear(GbContestStudent gbContestStudent);

    void insertStudentEndTime(Integer contestId,Integer studentId,Integer date);

    String getEndTime(Integer contestId,Integer studentId,Integer date);

    String getEndTimeByStr(String endTimeStr,Integer date);

    int getMaxDate(Integer contestId);

    Map<Integer,String> getStudentIdAndAccountMap(Integer contestId);

    /**
     * 获取最大的完成年份
     * @param contestId
     * @return
     */
    Integer getMaxFinishDate(Integer contestId);

    /**
     * 获取最大的广告投放年份
     * @param contestId
     * @return
     */
    Integer getMaxAdDate(Integer contestId);


    /**
     * 获取学生的经营进度
     */
    Integer getStudentDate(Integer contestId,Integer studentId);
}
