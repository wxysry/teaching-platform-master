package com.xiyou.main.service.gbcontest;

import com.baomidou.mybatisplus.extension.service.IService;
import com.xiyou.main.entity.gbcontest.GbCtBalance;
import com.xiyou.main.vo.gbcontest.GbCashFolowEntity;
import com.xiyou.main.vo.newcontest.NewCashFolowEntity;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author wangxingyu
 * @since 2023-06-11
 */
public interface GbCtBalanceService extends IService<GbCtBalance> {

    /**
     * P8&P9 申请长贷款&申请短贷-获取最大贷款额度和当前贷款总和
     *
     * @param cashFolowEntity
     * @return
     */
    Map<String,Integer> getAllMaxLoanAmount(GbCashFolowEntity cashFolowEntity);

    /**
     * P8&P9 申请长贷款&申请短贷-获取最大贷款额度和当前贷款总和
     *
     * @param cashFolowEntity
     * @return
     */
    Integer getMaxLoanAmount(GbCashFolowEntity cashFolowEntity);

    GbCtBalance get(GbCtBalance balance);

    GbCtBalance getTemp(Integer studentId, Integer contestId, Integer year);

    /**
     * @Author: tangcan
     * @Description: 最近一个所得税>0的年份的权益
     * @Param: [studentId, contestId]
     * @date: 2019/7/24
     */
    Integer getRecentYEarHaveEquity(Integer studentId, Integer contestId, Integer nowYear);

    /**
     * @Author: tangcan
     * @Description: max(之前年份所有者权益 ）
     * @Param: [studentId, contestId, currentYear]
     * @date: 2019/7/24
     */
    int maxTotalEquityPreYear(Integer studentId, Integer contestId, int currentYear);

    List<GbCtBalance> getCurrentYear(GbCtBalance balance);

    GbCtBalance getOne(GbCtBalance ctBalance);
}
