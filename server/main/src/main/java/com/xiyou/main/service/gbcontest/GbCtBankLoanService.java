package com.xiyou.main.service.gbcontest;

import com.baomidou.mybatisplus.extension.service.IService;
import com.xiyou.main.entity.gbcontest.GbCtBankLoan;
import com.xiyou.main.vo.gbcontest.GbCashFolowEntity;
import com.xiyou.main.vo.newcontest.NewCashFolowEntity;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author tangcan
 * @since 2019-07-22
 */
public interface GbCtBankLoanService extends IService<GbCtBankLoan> {

    /**
     * bank_loan表中贷款类型为2且归还时间=当前时间的金额
     * @param cashFolowEntity
     * @return
     */
    Integer getCurrentMoney(GbCashFolowEntity cashFolowEntity);


    List<GbCtBankLoan> list(Integer studentId, Integer contestId);
}
