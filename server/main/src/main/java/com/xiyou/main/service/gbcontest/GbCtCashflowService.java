package com.xiyou.main.service.gbcontest;

import com.baomidou.mybatisplus.extension.service.IService;
import com.xiyou.main.entity.gbcontest.GbCtCashflow;
import com.xiyou.main.vo.gbcontest.GbCashFolowEntity;
import com.xiyou.main.vo.newcontest.NewCashFolowEntity;

import java.util.List;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author wangxingyu
 * @since 2023-06-11
 */
public interface GbCtCashflowService extends IService<GbCtCashflow> {


    /**
     * 用户当前现金
     *
     * @param studentId
     * @param contestId
     * @return
     */
    GbCashFolowEntity getStudentFinanceInfor(Integer studentId, Integer contestId);

    /**
     * student上一条现金
     *
     * @param gbCashFolowEntity
     * @return
     */
    Integer getStudentLastCash(GbCashFolowEntity gbCashFolowEntity);

    /**
     * 查出用户当前现金余额
     *
     * @param userId
     * @param contestId
     * @return
     */
    Integer getCash(Integer userId, Integer contestId);

    List<GbCtCashflow> list(Integer studentId, Integer contestId, Integer year, List<String> actionList);

}
