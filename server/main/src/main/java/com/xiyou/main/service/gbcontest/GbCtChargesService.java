package com.xiyou.main.service.gbcontest;

import com.baomidou.mybatisplus.extension.service.IService;
import com.xiyou.main.entity.gbcontest.GbCtCharges;
import java.util.List;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author tangcan
 * @since 2019-07-22
 */
public interface GbCtChargesService extends IService<GbCtCharges> {

    int addBySys(GbCtCharges charges);

    int updateTotal(Integer chargesId);

    /**
     * @Author: tangcan
     * @Description: 获取系统填入的
     * @Param: [studentId, contestId, year]
     * @date: 2019/9/10
    */
    GbCtCharges getSys(Integer studentId, Integer contestId, int year);


    GbCtCharges getTemp(Integer studentId, Integer contestId, int year);

    List<GbCtCharges> getCurrentYear(GbCtCharges charges);

    Integer getTotal(Integer studentId, Integer contestId, int year, int isxt);

    GbCtCharges getOne(GbCtCharges ctCharges);
}
