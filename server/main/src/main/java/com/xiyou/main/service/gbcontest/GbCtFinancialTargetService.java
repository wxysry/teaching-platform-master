package com.xiyou.main.service.gbcontest;

import com.xiyou.main.entity.gbcontest.GbCtFinancialTarget;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author wangxingyu
 * @since 2023-06-28
 */
public interface GbCtFinancialTargetService extends IService<GbCtFinancialTarget> {

    GbCtFinancialTarget get(GbCtFinancialTarget ctFinancialTarget);

    /**
     * 获取暂存数据
     * @return
     */
    GbCtFinancialTarget getTemp(Integer studentId, Integer contestId, int year);
}
