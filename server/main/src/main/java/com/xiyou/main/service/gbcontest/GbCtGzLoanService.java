package com.xiyou.main.service.gbcontest;

import com.xiyou.main.entity.gbcontest.GbCtGzLoan;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author wangxingyu
 * @since 2023-06-06
 */
public interface GbCtGzLoanService extends IService<GbCtGzLoan> {
    List<GbCtGzLoan> listCtGzLoan(Integer newContestId);
}
