package com.xiyou.main.service.gbcontest;

import com.xiyou.main.entity.gbcontest.GbCtGzMarket;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author wangxingyu
 * @since 2023-06-06
 */
public interface GbCtGzMarketService extends IService<GbCtGzMarket> {

}
