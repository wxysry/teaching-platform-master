package com.xiyou.main.service.gbcontest;

import com.xiyou.main.entity.gbcontest.GbCtGzProductLine;
import com.baomidou.mybatisplus.extension.service.IService;
import com.xiyou.main.vo.gbcontest.GbCashFolowEntity;
import com.xiyou.main.vo.newcontest.NewCashFolowEntity;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author wangxingyu
 * @since 2023-06-06
 */
public interface GbCtGzProductLineService extends IService<GbCtGzProductLine> {
    /**
     * 选择生产线类型
     * @param contestId
     * @return
     */
    List<GbCtGzProductLine> getProductLineTypeList(Integer contestId);

    /**
     * 该生产线的投资时间
     * @param cashFolowEntity
     * @return
     */
    Integer getProductLineInfo(GbCashFolowEntity cashFolowEntity);

    List<GbCtGzProductLine> getListByCplids(List<Integer> cplids);

}
