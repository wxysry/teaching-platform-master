package com.xiyou.main.service.gbcontest;

import com.baomidou.mybatisplus.extension.service.IService;
import com.xiyou.main.entity.gbcontest.GbCtGzProduct;
import com.xiyou.main.vo.gbcontest.GbYfProductVO;
import com.xiyou.main.vo.newcontest.NewYfProductVO;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author wangxingyu
 * @since 2023-06-06
 */
public interface GbCtGzProductService extends IService<GbCtGzProduct> {
    /**
     * 查出产品研发列表
     * @param userId
     * @param contestId
     * @return
     */
    List<GbYfProductVO> listYfProduct(Integer userId, Integer contestId);

    /**
     * 产品名称
     * @param contestId
     * @return
     */
    List<String> getProductCPName(Integer contestId);


    List<GbCtGzProduct> getList(Integer contestId);

    List<GbCtGzProduct> getListByIds(List<Integer> cpIds);

}
