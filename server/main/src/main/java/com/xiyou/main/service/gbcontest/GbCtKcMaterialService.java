package com.xiyou.main.service.gbcontest;

import com.baomidou.mybatisplus.extension.service.IService;
import com.xiyou.main.entity.gbcontest.GbCtKcMaterial;
import com.xiyou.main.vo.gbcontest.GbCashFolowEntity;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author tangcan
 * @since 2019-07-22
 */
public interface GbCtKcMaterialService extends IService<GbCtKcMaterial> {


    void updateKcMaterial(GbCashFolowEntity cashFolowEntity);

    int getMaterialSum(Integer studentId, Integer contestId);

    //查看生产线所需要的材料
    List<GbCtKcMaterial> listNeed(Integer contestId, List<Integer> lineIds);

    //查出库存材料数量
    List<GbCtKcMaterial> listKc(Integer contestId, Integer userId);

    //查出材料库存的售出价格
    List<GbCtKcMaterial> listSellKc(Integer contestId, Integer userId);
}
