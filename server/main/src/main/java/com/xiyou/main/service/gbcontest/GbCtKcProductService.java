package com.xiyou.main.service.gbcontest;

import com.baomidou.mybatisplus.extension.service.IService;
import com.xiyou.main.entity.gbcontest.GbCtKcProduct;
import com.xiyou.main.vo.gbcontest.GbProductVo;
import com.xiyou.main.vo.newcontest.NewProductVo;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author tangcan
 * @since 2019-07-22
 */
public interface GbCtKcProductService extends IService<GbCtKcProduct> {

    int getProductSum(Integer studentId, Integer contestId);

    List<GbProductVo> listSellKc(Integer contestId, Integer userId);
}
