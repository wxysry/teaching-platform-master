package com.xiyou.main.service.gbcontest;

import com.baomidou.mybatisplus.extension.service.IService;
import com.xiyou.main.entity.gbcontest.GbCtGzProductLine;
import com.xiyou.main.entity.gbcontest.GbCtLine;
import com.xiyou.main.vo.gbcontest.GbOnlineLine;

import java.util.List;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author tangcan
 * @since 2019-07-22
 */
public interface GbCtLineService extends IService<GbCtLine> {

    /**
     * 列出所有在建中的生产线
     * @param userId
     * @param contestId
     * @return
     */
    List<GbOnlineLine> listOnline(Integer userId, Integer contestId, Integer date);

    /**
     * 通过生产线的id查生产线的类型信息
     * @param lineIds
     * @return
     */
    List<GbCtGzProductLine> getLineInfo(List<Integer> lineIds);

    /**
     * 列出所有可以转产的生产线
     * @param userId
     * @param contestId
     * @return
     */
    List<GbOnlineLine> listTransfer(Integer userId, Integer contestId);

    int getMaintenanceFee(Integer studentId, Integer contestId, Integer season);

    void updateDepFee(Integer studentId, Integer contestId, Integer date);

    Integer getDepTotal(Integer studentId, Integer contestId, int date);

    int getProductInProcess(GbCtLine line);

    int getEquipmentSum(Integer studentId, Integer contestId);

    List<GbCtLine> list(Integer studentId, Integer contestId);


    /**
     * 查出所有空闲的可以生产的生产线
     * @param userId
     * @param contestId
     * @return
     */
    List<GbOnlineLine> listNoProducting(Integer userId, Integer contestId);

    /**
     * 显示line中PL_Remain_Date 不为空的数据
     * @param userId
     * @param contestId
     * @return
     */
    List<GbOnlineLine> getListOnlineInfo(Integer userId, Integer contestId);
}
