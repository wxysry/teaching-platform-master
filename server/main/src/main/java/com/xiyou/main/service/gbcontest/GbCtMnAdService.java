package com.xiyou.main.service.gbcontest;

import com.baomidou.mybatisplus.extension.service.IService;
import com.xiyou.main.entity.gbcontest.GbCtMnAd;

import java.util.List;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author tangcan
 * @since 2019-07-25
 */
public interface GbCtMnAdService extends IService<GbCtMnAd> {

    List<GbCtMnAd> getAdList(Integer contestId, Integer studentId, Integer year,Integer quarterly);

    List<GbCtMnAd> getAdListByGroupNum(Integer contestId, Integer studentId, String name);
}
