package com.xiyou.main.service.gbcontest;

import com.baomidou.mybatisplus.extension.service.IService;
import com.xiyou.main.entity.gbcontest.GbCtProfitChart;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author tangcan
 * @since 2019-07-22
 */
public interface GbCtProfitChartService extends IService<GbCtProfitChart> {

    List<GbCtProfitChart> list(GbCtProfitChart profitChart);

    GbCtProfitChart getSys(GbCtProfitChart profitChart);


    GbCtProfitChart getTemp(Integer studentId, Integer contestId, Integer year);


    List<GbCtProfitChart> getCurrentYear(GbCtProfitChart profitChart);

    GbCtProfitChart getOne(GbCtProfitChart ctProfitChart);
}
