package com.xiyou.main.service.gbcontest;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.xiyou.main.entity.gbcontest.GbCtSubject;
import com.xiyou.main.params.contest.SubjectParam;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author tangcan
 * @since 2019-08-25
 */
public interface GbCtSubjectService extends IService<GbCtSubject> {

    Page<GbCtSubject> listByAdmin(SubjectParam subjectParam);

    Page<GbCtSubject> listByTeacher(SubjectParam subjectParam);

    List<GbCtSubject> listByTeacher(Integer teacherId,Integer groupId);

}
