package com.xiyou.main.service.gbcontest;

import com.xiyou.main.entity.gbcontest.GbCtXdOrderTemp;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author wangxingyu
 * @since 2023-09-14
 */
public interface GbCtXdOrderTempService extends IService<GbCtXdOrderTemp> {

}
