package com.xiyou.main.service.gbcontest;

import com.baomidou.mybatisplus.extension.service.IService;
import com.xiyou.main.entity.gbcontest.GbCtXzOrder;
import com.xiyou.main.vo.gbcontest.GbXzOrderVo;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author tangcan
 * @since 2019-07-22
 */
public interface GbCtXzOrderService extends IService<GbCtXzOrder> {

    List<GbCtXzOrder> list(GbCtXzOrder order);

    Map<String, BigDecimal> getSalesAndDirectCost(Integer studentId, Integer contestId, int year);

    /**
     * 查出交货订单
     * @param queryMap
     * @return
     */
    List<GbXzOrderVo> listDeliveryOrder(Map<String, Object> queryMap);
}
