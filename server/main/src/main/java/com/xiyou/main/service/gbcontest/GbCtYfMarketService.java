package com.xiyou.main.service.gbcontest;

import com.baomidou.mybatisplus.extension.service.IService;
import com.xiyou.main.entity.gbcontest.GbCtYfMarket;
import com.xiyou.main.vo.gbcontest.GbMarketVo;

import java.util.List;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author tangcan
 * @since 2019-07-22
 */
public interface GbCtYfMarketService extends IService<GbCtYfMarket> {

    void update(GbCtYfMarket yfMarket);

    /**
     * @Author: tangcan
     * @Description: 获取已经开发结束的市场名称
     * @Param: [studentId, contestId]
     * @date: 2019/7/25
     */
    List<String> getHadYfFinishMarkets(Integer studentId, Integer contestId);

    List<GbMarketVo> listYfMarket(Integer studentId, Integer contestId);
}
