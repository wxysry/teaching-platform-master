package com.xiyou.main.service.gbcontest;

import com.baomidou.mybatisplus.extension.service.IService;
import com.xiyou.main.entity.gbcontest.GbCtYfProduct;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author tangcan
 * @since 2019-07-22
 */
public interface GbCtYfProductService extends IService<GbCtYfProduct> {

    void update(GbCtYfProduct yfProduct);

    /**
     * 当季结束更新产品研发
     * @param userId
     * @param contestId
     */
    void updateEndingSeason(Integer userId, Integer contestId, Integer date);
}
