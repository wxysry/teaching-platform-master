package com.xiyou.main.service.gbcontest.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.xiyou.main.dao.gbcontest.GbContestGroupMapper;
import com.xiyou.main.entity.gbcontest.GbContestGroup;
import com.xiyou.main.service.gbcontest.GbContestGroupService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author tangcan
 * @since 2019-07-09
 */
@Service
public class GbContestGroupServiceImpl extends ServiceImpl<GbContestGroupMapper, GbContestGroup> implements GbContestGroupService {
    @Autowired
    private GbContestGroupMapper gbContestGroupMapper;

    @Override
    public int insert(GbContestGroup gbContestGroup) {
        return gbContestGroupMapper.insert(gbContestGroup);
    }

    @Override
    public boolean checkNameExist(GbContestGroup contestGroup) {
        QueryWrapper<GbContestGroup> wrapper = new QueryWrapper<>();
        wrapper.ne("id", contestGroup.getId())
                .eq("group_name", contestGroup.getGroupName());
        return this.getOne(wrapper) != null;
    }

    @Override
    public List<GbContestGroup> getListByTeacher(Integer teacherId) {
        return gbContestGroupMapper.getListByTeacher(teacherId);
    }
}
