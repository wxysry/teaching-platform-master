package com.xiyou.main.service.gbcontest.impl;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.xiyou.main.biz.gbcontest.GbActionBiz;
import com.xiyou.main.biz.gbcontest.GbBusinessBiz;
import com.xiyou.main.biz.gbcontest.GbContestBiz;
import com.xiyou.main.dao.gbcontest.GbContestMapper;
import com.xiyou.main.dao.gbcontest.GbCtGzOrderMapper;
import com.xiyou.main.dao.gbcontest.GbCtMnChooseMapper;
import com.xiyou.main.entity.gbcontest.GbContest;
import com.xiyou.main.entity.gbcontest.GbCtMnChoose;
import com.xiyou.main.params.contest.ContestParam;
import com.xiyou.main.service.exam.TeachClassService;
import com.xiyou.main.service.gbcontest.GbContestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author tangcan
 * @since 2019-08-25
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class GbContestServiceImpl extends ServiceImpl<GbContestMapper, GbContest> implements GbContestService {
    @Autowired
    private GbContestMapper gbContestMapper;
    @Autowired
    private TeachClassService teachClassService;
    @Autowired
    private GbCtMnChooseMapper gbCtMnChooseMapper;
    @Autowired
    private GbCtGzOrderMapper gbCtGzOrderMapper;
    @Autowired
    private GbActionBiz gbActionBiz;
    @Autowired
    private GbContestBiz gbContestBiz;

    @Override
    public Page<GbContest> getPage(ContestParam contestParam) {
        Page<GbContest> page = new Page<>(contestParam.getPage(), contestParam.getLimit());
        return gbContestMapper.getPage(page, contestParam);
    }

    @Override
    public Page<GbContest> getPageAdmin(ContestParam contestParam) {
        Page<GbContest> page = new Page<>(contestParam.getPage(), contestParam.getLimit());
        return gbContestMapper.getPageAdmin(page, contestParam);
    }

    @Override
    public int getUnEndContest(Integer teacherId) {
        return gbContestMapper.getUnEndContest(teacherId);
    }


    @Autowired
    GbBusinessBiz gbBusinessBiz;

    @Override
    public int publish(Integer teacherId, Integer contestId) {
        List<Integer> studentIdList = teachClassService.getStudentIdList(teacherId);
        // 选单mn_choose新增数据=市场定单gz_order为该场考试的
        List<GbCtMnChoose> ctMnChooseList = gbCtGzOrderMapper.getList(contestId);
        if (ctMnChooseList != null) {
            for (GbCtMnChoose ctMnChoose : ctMnChooseList) {
                //选中轮次和选走组号都为空
                ctMnChoose.setXzGroup(null);
                ctMnChoose.setXzRound(null);
                ctMnChoose.setContestId(contestId);
            }
            if (ctMnChooseList.size() > 0) {
                gbCtMnChooseMapper.insertBatch(ctMnChooseList);
            }
        }
        //每个学生生成一份数据
        for (Integer studentId : studentIdList) {
            gbBusinessBiz.start(studentId,contestId);
        }

        GbContest gbContest  = new GbContest();
        gbContest.setIsPublish(1);
        gbContest.setContestId(contestId);
        //剩余时间
        int seasonTime =gbContestBiz.getSeasonTime(contestId, 11);
        gbContest.setRemainTime(seasonTime);
        return gbContestMapper.updateById(gbContest);
    }

    @Override
    public Page<GbContest> getStudentContestPage(ContestParam contestParam) {
        Page<GbContest> page = new Page<>(contestParam.getPage(), contestParam.getLimit());
        return gbContestMapper.getStudentContestPage(page, contestParam);
    }

    @Override
    public boolean isSyn(Integer contestId) {
        GbContest contest = gbContestMapper.getObjByContestId(contestId);
        return contest.getIsSyn() == 1;
    }


}
