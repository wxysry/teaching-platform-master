package com.xiyou.main.service.gbcontest.impl;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.xiyou.main.dao.gbcontest.GbCtBankLoanMapper;
import com.xiyou.main.dao.gbcontest.GbCtGzCsMapper;
import com.xiyou.main.entity.gbcontest.GbCtBankLoan;
import com.xiyou.main.service.gbcontest.GbCtBankLoanService;
import com.xiyou.main.vo.gbcontest.GbCashFolowEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author tangcan
 * @since 2019-07-22
 */
@Service
public class GbCtBankLoanServiceImpl extends ServiceImpl<GbCtBankLoanMapper, GbCtBankLoan> implements GbCtBankLoanService {
    @Autowired
    GbCtBankLoanMapper gbCtBankLoanMapper;
    @Autowired
    GbCtGzCsMapper gbCtGzCsMapper;

    @Override
    public List<GbCtBankLoan> list(Integer studentId, Integer contestId) {
        QueryWrapper<GbCtBankLoan> wrapper = new QueryWrapper<>();
        wrapper.eq("student_id", studentId).eq("contest_id", contestId);
        return this.list(wrapper);
    }

    @Override
    public Integer getCurrentMoney(GbCashFolowEntity cashFolowEntity) {
        return gbCtBankLoanMapper.getCurrentMoney(cashFolowEntity);
    }

}
