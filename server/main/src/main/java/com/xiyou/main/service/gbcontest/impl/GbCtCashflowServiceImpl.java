package com.xiyou.main.service.gbcontest.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.xiyou.main.dao.gbcontest.GbCtCashflowMapper;
import com.xiyou.main.entity.gbcontest.GbCtCashflow;
import com.xiyou.main.service.gbcontest.GbCtCashflowService;
import com.xiyou.main.vo.gbcontest.GbCashFolowEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author wangxingyu
 * @since 2023-06-11
 */
@Service
public class GbCtCashflowServiceImpl extends ServiceImpl<GbCtCashflowMapper, GbCtCashflow> implements GbCtCashflowService {

    @Autowired
    GbCtCashflowMapper gbCtCashflowMapper;


    @Override
    public GbCashFolowEntity getStudentFinanceInfor(Integer studentId, Integer contestId) {
        return null;
    }

    @Override
    public Integer getStudentLastCash(GbCashFolowEntity gbCashFolowEntity) {
        return gbCtCashflowMapper.getStudentLastCash(gbCashFolowEntity);
    }


    @Override
    public Integer getCash(Integer userId, Integer contestId) {
        return gbCtCashflowMapper.getCash(userId, contestId);
    }

    @Override
    public List<GbCtCashflow> list(Integer studentId, Integer contestId, Integer year, List<String> actionList) {
        return gbCtCashflowMapper.list(studentId, contestId, year, actionList);
    }
}
