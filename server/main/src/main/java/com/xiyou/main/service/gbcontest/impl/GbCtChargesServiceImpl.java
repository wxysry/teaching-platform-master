package com.xiyou.main.service.gbcontest.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.xiyou.main.dao.gbcontest.GbCtChargesMapper;
import com.xiyou.main.entity.gbcontest.GbCtCharges;
import com.xiyou.main.service.gbcontest.GbCtChargesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author tangcan
 * @since 2019-07-22
 */
@Service
public class GbCtChargesServiceImpl extends ServiceImpl<GbCtChargesMapper, GbCtCharges> implements GbCtChargesService {
    @Autowired
    private GbCtChargesMapper gbCtChargesMapper;

    @Override
    public int addBySys(GbCtCharges charges) {
        return gbCtChargesMapper.addBySys(charges);
    }

    @Override
    public int updateTotal(Integer chargesId) {
        return gbCtChargesMapper.updateTotal(chargesId);
    }

    @Override
    public GbCtCharges getSys(Integer studentId, Integer contestId, int year) {
        QueryWrapper<GbCtCharges> wrapper = new QueryWrapper<>();
        wrapper.eq("student_id", studentId)
                .eq("contest_id", contestId)
                .eq("c_date", year)
                .eq("bs_isxt", 1);
        return this.getOne(wrapper);
    }


    @Override
    public GbCtCharges getTemp(Integer studentId, Integer contestId, int year) {
        QueryWrapper<GbCtCharges> wrapper = new QueryWrapper<>();
        wrapper.eq("student_id", studentId)
                .eq("contest_id", contestId)
                .eq("c_date", year)
                .eq("bs_isxt", 0);
        return this.getOne(wrapper);
    }


    @Override
    public List<GbCtCharges> getCurrentYear(GbCtCharges charges) {
        if (charges == null) {
            return new ArrayList<>();
        }
        QueryWrapper<GbCtCharges> wrapper = new QueryWrapper<>();
        wrapper.eq("student_id", charges.getStudentId())
                .eq("contest_id", charges.getContestId())
                .eq("c_date", charges.getCDate());
        return this.list(wrapper);
    }

    @Override
    public Integer getTotal(Integer studentId, Integer contestId, int year, int isxt) {
        return gbCtChargesMapper.getTotal(studentId, contestId, year, isxt);
    }

    @Override
    public GbCtCharges getOne(GbCtCharges ctCharges) {
        QueryWrapper<GbCtCharges> wrapper = new QueryWrapper<>();
        if (ctCharges.getStudentId() != null) {
            wrapper.eq("student_id", ctCharges.getStudentId());
        }
        if (ctCharges.getContestId() != null) {
            wrapper.eq("contest_id", ctCharges.getContestId());
        }
        if (ctCharges.getBsIsxt() != null) {
            wrapper.eq("bs_isxt", ctCharges.getBsIsxt());
        }
        if (ctCharges.getCDate() != null) {
            wrapper.eq("c_date", ctCharges.getCDate());
        }
        return this.getOne(wrapper);
    }
}
