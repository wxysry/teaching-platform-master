package com.xiyou.main.service.gbcontest.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.xiyou.main.dao.contest.CtFeeMapper;
import com.xiyou.main.dao.gbcontest.GbCtFeeMapper;
import com.xiyou.main.dao.gbcontest.GbCtFeeMapper;
import com.xiyou.main.entity.gbcontest.GbCtFee;
import com.xiyou.main.service.gbcontest.GbCtFeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author tangcan
 * @since 2019-07-22
 */
@Service
public class GbCtFeeServiceImpl extends ServiceImpl<GbCtFeeMapper, GbCtFee> implements GbCtFeeService {

    @Autowired
    GbCtFeeMapper ctFeeMapper;

    @Override
    public int getFeeSum(GbCtFee fee) {
        if (fee == null) {
            return 0;
        }
        QueryWrapper<GbCtFee> wrapper = new QueryWrapper<>();
        wrapper.eq("student_id", fee.getStudentId())
                .eq("contest_id", fee.getContestId());
        if (fee.getType() != null) {
            wrapper.eq("type", fee.getType());
        }
        List<GbCtFee> feeList = this.list(wrapper);
        return feeList.stream().mapToInt(GbCtFee::getRFee).sum();
    }
}
