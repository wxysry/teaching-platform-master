package com.xiyou.main.service.gbcontest.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.xiyou.main.entity.gbcontest.GbCtFinancialTarget;
import com.xiyou.main.dao.gbcontest.GbCtFinancialTargetMapper;
import com.xiyou.main.service.gbcontest.GbCtFinancialTargetService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author wangxingyu
 * @since 2023-06-28
 */
@Service
public class GbCtFinancialTargetServiceImpl extends ServiceImpl<GbCtFinancialTargetMapper, GbCtFinancialTarget> implements GbCtFinancialTargetService {


    @Override
    public GbCtFinancialTarget get(GbCtFinancialTarget ctFinancialTarget) {
        QueryWrapper<GbCtFinancialTarget> wrapper = new QueryWrapper<>();
        if (ctFinancialTarget.getStudentId() != null) {
            wrapper.eq("student_id", ctFinancialTarget.getStudentId());
        }
        if (ctFinancialTarget.getContestId() != null) {
            wrapper.eq("contest_id", ctFinancialTarget.getContestId());
        }
        if (ctFinancialTarget.getFtYear() != null) {
            wrapper.eq("ft_year", ctFinancialTarget.getFtYear());
        }
        wrapper.eq("ft_isxt", 1);
        return this.getOne(wrapper);
    }



    @Override
    public GbCtFinancialTarget getTemp(Integer studentId, Integer contestId, int year) {
        QueryWrapper<GbCtFinancialTarget> wrapper = new QueryWrapper<>();
        wrapper.eq("student_id", studentId);
        wrapper.eq("contest_id", contestId);
        wrapper.eq("ft_year", year);
        wrapper.eq("ft_isxt", 0);
        return this.getOne(wrapper);
    }
}
