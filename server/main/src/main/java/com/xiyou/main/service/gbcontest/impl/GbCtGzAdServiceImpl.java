package com.xiyou.main.service.gbcontest.impl;

import com.xiyou.main.entity.gbcontest.GbCtGzAd;
import com.xiyou.main.dao.gbcontest.GbCtGzAdMapper;
import com.xiyou.main.service.gbcontest.GbCtGzAdService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author wangxingyu
 * @since 2023-06-06
 */
@Service
public class GbCtGzAdServiceImpl extends ServiceImpl<GbCtGzAdMapper, GbCtGzAd> implements GbCtGzAdService {

}
