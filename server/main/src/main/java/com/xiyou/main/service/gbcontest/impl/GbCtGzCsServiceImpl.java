package com.xiyou.main.service.gbcontest.impl;

import com.xiyou.main.entity.gbcontest.GbCtGzCs;
import com.xiyou.main.dao.gbcontest.GbCtGzCsMapper;
import com.xiyou.main.service.gbcontest.GbCtGzCsService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author wangxingyu
 * @since 2023-06-06
 */
@Service
public class GbCtGzCsServiceImpl extends ServiceImpl<GbCtGzCsMapper, GbCtGzCs> implements GbCtGzCsService {

    @Override
    public GbCtGzCs getByContestId(Integer contestId) {
        return baseMapper.getByContestId(contestId);
    }


    @Override
    public GbCtGzCs getBySubjectNumber(Integer subjectNumber) {

        return baseMapper.getBySubjectNumber(subjectNumber);
    }
}
