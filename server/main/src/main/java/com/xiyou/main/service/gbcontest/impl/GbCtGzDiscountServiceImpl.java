package com.xiyou.main.service.gbcontest.impl;

import com.xiyou.main.entity.gbcontest.GbCtGzDiscount;
import com.xiyou.main.dao.gbcontest.GbCtGzDiscountMapper;
import com.xiyou.main.service.gbcontest.GbCtGzDiscountService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author wangxingyu
 * @since 2023-06-06
 */
@Service
public class GbCtGzDiscountServiceImpl extends ServiceImpl<GbCtGzDiscountMapper, GbCtGzDiscount> implements GbCtGzDiscountService {

}
