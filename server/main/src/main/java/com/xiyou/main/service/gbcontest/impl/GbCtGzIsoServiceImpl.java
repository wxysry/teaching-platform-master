package com.xiyou.main.service.gbcontest.impl;

import com.xiyou.main.entity.gbcontest.GbCtGzIso;
import com.xiyou.main.dao.gbcontest.GbCtGzIsoMapper;
import com.xiyou.main.service.gbcontest.GbCtGzIsoService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author wangxingyu
 * @since 2023-06-06
 */
@Service
public class GbCtGzIsoServiceImpl extends ServiceImpl<GbCtGzIsoMapper, GbCtGzIso> implements GbCtGzIsoService {

}
