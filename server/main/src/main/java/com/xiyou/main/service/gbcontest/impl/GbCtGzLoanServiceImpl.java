package com.xiyou.main.service.gbcontest.impl;

import com.xiyou.main.entity.gbcontest.GbCtGzLoan;
import com.xiyou.main.dao.gbcontest.GbCtGzLoanMapper;
import com.xiyou.main.service.gbcontest.GbCtGzLoanService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author wangxingyu
 * @since 2023-06-06
 */
@Service
public class GbCtGzLoanServiceImpl extends ServiceImpl<GbCtGzLoanMapper, GbCtGzLoan> implements GbCtGzLoanService {


    @Autowired
    GbCtGzLoanMapper gbCtGzLoanMapper;
    @Override
    public List<GbCtGzLoan> listCtGzLoan(Integer gbContestId) {
        return gbCtGzLoanMapper.listCtGzLoan(gbContestId);
    }
}
