package com.xiyou.main.service.gbcontest.impl;

import com.xiyou.main.entity.gbcontest.GbCtGzMarket;
import com.xiyou.main.dao.gbcontest.GbCtGzMarketMapper;
import com.xiyou.main.service.gbcontest.GbCtGzMarketService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author wangxingyu
 * @since 2023-06-06
 */
@Service
public class GbCtGzMarketServiceImpl extends ServiceImpl<GbCtGzMarketMapper, GbCtGzMarket> implements GbCtGzMarketService {

}
