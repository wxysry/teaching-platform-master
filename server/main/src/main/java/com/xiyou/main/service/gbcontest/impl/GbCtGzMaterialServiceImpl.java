package com.xiyou.main.service.gbcontest.impl;

import com.xiyou.main.entity.gbcontest.GbCtGzMaterial;
import com.xiyou.main.dao.gbcontest.GbCtGzMaterialMapper;
import com.xiyou.main.service.gbcontest.GbCtGzMaterialService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author wangxingyu
 * @since 2023-06-06
 */
@Service
public class GbCtGzMaterialServiceImpl extends ServiceImpl<GbCtGzMaterialMapper, GbCtGzMaterial> implements GbCtGzMaterialService {

}
