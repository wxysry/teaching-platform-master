package com.xiyou.main.service.gbcontest.impl;

import com.xiyou.main.entity.gbcontest.GbCtGzNum;
import com.xiyou.main.dao.gbcontest.GbCtGzNumMapper;
import com.xiyou.main.service.gbcontest.GbCtGzNumService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author wangxingyu
 * @since 2023-06-06
 */
@Service
public class GbCtGzNumServiceImpl extends ServiceImpl<GbCtGzNumMapper, GbCtGzNum> implements GbCtGzNumService {

}
