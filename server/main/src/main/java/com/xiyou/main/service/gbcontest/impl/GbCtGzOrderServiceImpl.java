package com.xiyou.main.service.gbcontest.impl;

import com.xiyou.main.entity.gbcontest.GbCtGzOrder;
import com.xiyou.main.dao.gbcontest.GbCtGzOrderMapper;
import com.xiyou.main.service.gbcontest.GbCtGzOrderService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author wangxingyu
 * @since 2023-06-06
 */
@Service
public class GbCtGzOrderServiceImpl extends ServiceImpl<GbCtGzOrderMapper, GbCtGzOrder> implements GbCtGzOrderService {

}
