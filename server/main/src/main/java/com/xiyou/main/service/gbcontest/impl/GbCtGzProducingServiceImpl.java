package com.xiyou.main.service.gbcontest.impl;

import com.xiyou.main.entity.gbcontest.GbCtGzProducing;
import com.xiyou.main.dao.gbcontest.GbCtGzProducingMapper;
import com.xiyou.main.service.gbcontest.GbCtGzProducingService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author wangxingyu
 * @since 2023-06-06
 */
@Service
public class GbCtGzProducingServiceImpl extends ServiceImpl<GbCtGzProducingMapper, GbCtGzProducing> implements GbCtGzProducingService {

}
