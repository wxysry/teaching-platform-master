package com.xiyou.main.service.gbcontest.impl;

import com.xiyou.main.entity.gbcontest.GbCtGzProductDesign;
import com.xiyou.main.dao.gbcontest.GbCtGzProductDesignMapper;
import com.xiyou.main.service.gbcontest.GbCtGzProductDesignService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author wangxingyu
 * @since 2023-06-06
 */
@Service
public class GbCtGzProductDesignServiceImpl extends ServiceImpl<GbCtGzProductDesignMapper, GbCtGzProductDesign> implements GbCtGzProductDesignService {

}
