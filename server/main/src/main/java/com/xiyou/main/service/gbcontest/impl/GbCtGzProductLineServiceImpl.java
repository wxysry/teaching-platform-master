package com.xiyou.main.service.gbcontest.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.xiyou.main.entity.gbcontest.GbCtGzProductLine;
import com.xiyou.main.dao.gbcontest.GbCtGzProductLineMapper;
import com.xiyou.main.service.gbcontest.GbCtGzProductLineService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.xiyou.main.vo.gbcontest.GbCashFolowEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author wangxingyu
 * @since 2023-06-06
 */
@Service
public class GbCtGzProductLineServiceImpl extends ServiceImpl<GbCtGzProductLineMapper, GbCtGzProductLine> implements GbCtGzProductLineService {

    @Autowired
    GbCtGzProductLineMapper ctGzProductLineMapper;

    @Override
    public List<GbCtGzProductLine> getProductLineTypeList(Integer contestId) {
        return ctGzProductLineMapper.getProductLineTypeList(contestId);
    }

    @Override
    public Integer getProductLineInfo(GbCashFolowEntity cashFolowEntity) {

        return ctGzProductLineMapper.getProductLineInfo(cashFolowEntity);
    }

    @Override
    public List<GbCtGzProductLine> getListByCplids(List<Integer> cplids) {
        QueryWrapper<GbCtGzProductLine> wrapper = new QueryWrapper<>();
        wrapper.in("id", cplids);
        return this.list(wrapper);
    }

}
