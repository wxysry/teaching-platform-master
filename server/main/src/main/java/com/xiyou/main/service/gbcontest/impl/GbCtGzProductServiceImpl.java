package com.xiyou.main.service.gbcontest.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.xiyou.main.entity.gbcontest.GbCtGzProduct;
import com.xiyou.main.dao.gbcontest.GbCtGzProductMapper;
import com.xiyou.main.service.gbcontest.GbCtGzProductService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.xiyou.main.vo.gbcontest.GbYfProductVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author wangxingyu
 * @since 2023-06-06
 */
@Service
public class GbCtGzProductServiceImpl extends ServiceImpl<GbCtGzProductMapper, GbCtGzProduct> implements GbCtGzProductService {

    @Autowired
    GbCtGzProductMapper mapper;

    @Override
    public List<String> getProductCPName(Integer contestId) {
        return mapper.getProductCPName(contestId);
    }

    @Override
    public List<GbYfProductVO> listYfProduct(Integer userId, Integer contestId) {
        return mapper.listYfProduct(userId, contestId);
    }

    @Override
    public List<GbCtGzProduct> getList(Integer contestId) {
        return mapper.getList(contestId);
    }

    @Override
    public List<GbCtGzProduct> getListByIds(List<Integer> cpIds) {
        if (cpIds == null || cpIds.size() == 0) {
            return new ArrayList<>();
        }
        QueryWrapper<GbCtGzProduct> wrapper = new QueryWrapper<>();
        wrapper.in("id", cpIds);
        return this.list(wrapper);
    }
}
