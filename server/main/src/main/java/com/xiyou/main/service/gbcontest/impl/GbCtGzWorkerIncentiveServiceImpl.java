package com.xiyou.main.service.gbcontest.impl;

import com.xiyou.main.entity.gbcontest.GbCtGzWorkerIncentive;
import com.xiyou.main.dao.gbcontest.GbCtGzWorkerIncentiveMapper;
import com.xiyou.main.service.gbcontest.GbCtGzWorkerIncentiveService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author wangxingyu
 * @since 2023-06-06
 */
@Service
public class GbCtGzWorkerIncentiveServiceImpl extends ServiceImpl<GbCtGzWorkerIncentiveMapper, GbCtGzWorkerIncentive> implements GbCtGzWorkerIncentiveService {

}
