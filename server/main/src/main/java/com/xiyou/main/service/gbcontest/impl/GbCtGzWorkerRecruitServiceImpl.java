package com.xiyou.main.service.gbcontest.impl;

import com.xiyou.main.entity.gbcontest.GbCtGzWorkerRecruit;
import com.xiyou.main.dao.gbcontest.GbCtGzWorkerRecruitMapper;
import com.xiyou.main.service.gbcontest.GbCtGzWorkerRecruitService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author wangxingyu
 * @since 2023-06-06
 */
@Service
public class GbCtGzWorkerRecruitServiceImpl extends ServiceImpl<GbCtGzWorkerRecruitMapper, GbCtGzWorkerRecruit> implements GbCtGzWorkerRecruitService {

}
