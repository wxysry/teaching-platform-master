package com.xiyou.main.service.gbcontest.impl;

import com.xiyou.main.entity.gbcontest.GbCtGzWorkerTrain;
import com.xiyou.main.dao.gbcontest.GbCtGzWorkerTrainMapper;
import com.xiyou.main.service.gbcontest.GbCtGzWorkerTrainService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author wangxingyu
 * @since 2023-06-06
 */
@Service
public class GbCtGzWorkerTrainServiceImpl extends ServiceImpl<GbCtGzWorkerTrainMapper, GbCtGzWorkerTrain> implements GbCtGzWorkerTrainService {

}
