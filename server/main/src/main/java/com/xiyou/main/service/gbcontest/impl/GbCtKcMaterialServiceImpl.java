package com.xiyou.main.service.gbcontest.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.xiyou.main.dao.gbcontest.GbCtKcMaterialMapper;
import com.xiyou.main.entity.gbcontest.GbCtGzCs;
import com.xiyou.main.entity.gbcontest.GbCtKcMaterial;
import com.xiyou.main.service.gbcontest.GbCtGzCsService;
import com.xiyou.main.service.gbcontest.GbCtKcMaterialService;
import com.xiyou.main.vo.gbcontest.GbCashFolowEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author tangcan
 * @since 2019-07-22
 */
@Service
public class GbCtKcMaterialServiceImpl extends ServiceImpl<GbCtKcMaterialMapper, GbCtKcMaterial> implements GbCtKcMaterialService {
    @Autowired
    private GbCtKcMaterialMapper ctKcMaterialMapper;
    @Autowired
    private GbCtGzCsService ctGzCsService;

    @Override
    public int getMaterialSum(Integer studentId, Integer contestId) {
        return ctKcMaterialMapper.getMaterialSum(studentId, contestId);
    }

    @Override
    public List<GbCtKcMaterial> listNeed(Integer contestId, List<Integer> lineIds) {
        if (lineIds.size() == 0) {
            return new ArrayList<>();
        }
        return ctKcMaterialMapper.listNeed(contestId, lineIds);
    }


    @Override
    public void updateKcMaterial(GbCashFolowEntity cashFolowEntity) {
        ctKcMaterialMapper.updateKcMaterial(cashFolowEntity);
    }

    @Override
    public List<GbCtKcMaterial> listKc(Integer contestId, Integer userId) {
        List<GbCtKcMaterial> list = ctKcMaterialMapper.listKc(contestId, userId);
        GbCtGzCs ctGzCs = ctGzCsService.getByContestId(contestId);
        if (list.size() > 0) {
            for (GbCtKcMaterial l : list) {
                l.setMaterialPrice(l.getMaterialPrice() * ctGzCs.getEmergenceMultipleMaterial());
            }
        }
        return list;
    }

    @Override
    public List<GbCtKcMaterial> listSellKc(Integer contestId, Integer userId) {
        List<GbCtKcMaterial> list = ctKcMaterialMapper.listSellKc(contestId, userId);
        GbCtGzCs ctGzCs = ctGzCsService.getByContestId(contestId);
        if (list.size() > 0) {
//            for (GbCtKcMaterial l : list) {
//                l.setMaterialPrice(l.getMaterialPrice() * ctGzCs.getEmergenceMultipleMaterial());
//                l.setSellPrice((double) l.getDirectCost() * ((double) ctGzCs.getInventoryDiscountMaterial() / 100.0));
//            }
        }
        return list;
    }
}
