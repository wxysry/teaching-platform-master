package com.xiyou.main.service.gbcontest.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.xiyou.main.dao.gbcontest.GbCtKcProductMapper;
import com.xiyou.main.entity.gbcontest.GbCtGzCs;
import com.xiyou.main.entity.gbcontest.GbCtKcProduct;
import com.xiyou.main.service.gbcontest.GbCtGzCsService;
import com.xiyou.main.service.gbcontest.GbCtKcProductService;
import com.xiyou.main.vo.gbcontest.GbProductVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author tangcan
 * @since 2019-07-22
 */
@Service
public class GbCtKcProductServiceImpl extends ServiceImpl<GbCtKcProductMapper, GbCtKcProduct> implements GbCtKcProductService {
    @Autowired
    private GbCtKcProductMapper ctKcProductMapper;
    @Autowired
    private GbCtGzCsService ctGzCsService;


    @Override
    public int getProductSum(Integer studentId, Integer contestId) {
        return ctKcProductMapper.getProductSum(studentId, contestId);
    }



    @Override
    public List<GbProductVo> listSellKc(Integer contestId, Integer userId) {
        List<GbProductVo> list = ctKcProductMapper.listSellKc(contestId, userId);
        GbCtGzCs ctGzCs = ctGzCsService.getByContestId(contestId);
        if (list.size() > 0) {
            for (GbProductVo l : list) {
                l.setUrgentPrice(l.getDirectCost() * ctGzCs.getEmergenceMultipleProduct());
                l.setSellPrice((double) l.getDirectCost() * ((double) ctGzCs.getInventoryDiscountProduct() / 100.0));
            }
        }
        return list;
    }
}
