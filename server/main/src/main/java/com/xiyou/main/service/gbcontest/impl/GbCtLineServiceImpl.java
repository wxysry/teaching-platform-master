package com.xiyou.main.service.gbcontest.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.xiyou.main.dao.gbcontest.GbCtLineMapper;
import com.xiyou.main.entity.gbcontest.GbCtGzProductLine;
import com.xiyou.main.entity.gbcontest.GbCtLine;
import com.xiyou.main.service.gbcontest.GbCtLineService;
import com.xiyou.main.vo.gbcontest.GbOnlineLine;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author tangcan
 * @since 2019-07-22
 */
@Service
public class GbCtLineServiceImpl extends ServiceImpl<GbCtLineMapper, GbCtLine> implements GbCtLineService {
    @Autowired
    GbCtLineMapper ctLineMapper;

    @Override
    public List<GbOnlineLine> listOnline(Integer userId, Integer contestId, Integer date) {
        return ctLineMapper.listOnline(userId, contestId, date);
    }

    @Override
    public List<GbCtGzProductLine> getLineInfo(List<Integer> lineIds) {
        return ctLineMapper.getLineInfo(lineIds);
    }

    @Override
    public List<GbOnlineLine> listTransfer(Integer userId, Integer contestId) {
        return ctLineMapper.listTransfer(userId, contestId);
    }

    @Override
    public int getMaintenanceFee(Integer studentId, Integer contestId, Integer date) {
        return ctLineMapper.getMaintenanceFee(studentId, contestId, date);
    }

    @Override
    public void updateDepFee(Integer studentId, Integer contestId, Integer date) {
        ctLineMapper.updateDepFee(studentId, contestId, date);
    }

    @Override
    public Integer getDepTotal(Integer studentId, Integer contestId, int date) {
        return ctLineMapper.getDepTotal(studentId, contestId, date);
    }

    @Override
    public int getProductInProcess(GbCtLine line) {
        if (line == null) {
            return 0;
        }
        return ctLineMapper.getProductInProcess(line);
    }

    @Override
    public int getEquipmentSum(Integer studentId, Integer contestId) {
        return ctLineMapper.getEquipmentSum(studentId, contestId);
    }

    @Override
    public List<GbCtLine> list(Integer studentId, Integer contestId) {
        QueryWrapper<GbCtLine> wrapper = new QueryWrapper<>();
        wrapper.eq("student_id", studentId).eq("contest_id", contestId);
        return this.list(wrapper);
    }

    @Override
    public List<GbOnlineLine> listNoProducting(Integer userId, Integer contestId) {
        return ctLineMapper.listNoProducting(userId, contestId);
    }

    @Override
    public List<GbOnlineLine> getListOnlineInfo(Integer userId, Integer contestId) {
        return ctLineMapper.getListOnline(userId, contestId);
    }


}
