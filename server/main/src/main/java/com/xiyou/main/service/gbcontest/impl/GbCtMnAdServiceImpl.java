package com.xiyou.main.service.gbcontest.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.xiyou.main.dao.gbcontest.GbCtMnAdMapper;
import com.xiyou.main.entity.gbcontest.GbCtMnAd;
import com.xiyou.main.service.gbcontest.GbCtMnAdService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author tangcan
 * @since 2019-07-25
 */
@Service
public class GbCtMnAdServiceImpl extends ServiceImpl<GbCtMnAdMapper, GbCtMnAd> implements GbCtMnAdService {
    @Autowired
    private GbCtMnAdMapper gbCtMnAdMapper;

    @Override
    public List<GbCtMnAd> getAdList(Integer contestId, Integer studentId, Integer year ,Integer quarterly) {
        return gbCtMnAdMapper.getAdList(contestId, studentId, year,quarterly);
    }

    @Override
    public List<GbCtMnAd> getAdListByGroupNum(Integer contestId, Integer studentId, String name) {
        QueryWrapper<GbCtMnAd> wrapper = new QueryWrapper<>();
        wrapper.eq("student_id", studentId)
                .eq("contest_id", contestId)
                .eq("group_num", name)
                .eq("is_submit","Y")
                .orderByAsc("year","quarterly");
        return this.list(wrapper);
    }
}
