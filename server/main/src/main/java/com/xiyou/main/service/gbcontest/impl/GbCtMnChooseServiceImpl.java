package com.xiyou.main.service.gbcontest.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.xiyou.main.dao.gbcontest.GbCtMnChooseMapper;
import com.xiyou.main.entity.gbcontest.GbCtMnChoose;
import com.xiyou.main.service.gbcontest.GbCtMnChooseService;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author tangcan
 * @since 2019-08-27
 */
@Service
public class GbCtMnChooseServiceImpl extends ServiceImpl<GbCtMnChooseMapper, GbCtMnChoose> implements GbCtMnChooseService {

}
