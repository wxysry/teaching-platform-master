package com.xiyou.main.service.gbcontest.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.xiyou.main.dao.gbcontest.GbCtProfitChartMapper;
import com.xiyou.main.entity.gbcontest.GbCtProfitChart;
import com.xiyou.main.service.gbcontest.GbCtProfitChartService;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author tangcan
 * @since 2019-07-22
 */
@Service
public class GbCtProfitChartServiceImpl extends ServiceImpl<GbCtProfitChartMapper, GbCtProfitChart> implements GbCtProfitChartService {
    @Override
    public List<GbCtProfitChart> list(GbCtProfitChart profitChart) {
        if (profitChart == null) {
            return new ArrayList<>();
        }
        QueryWrapper<GbCtProfitChart> wrapper = new QueryWrapper<>();
        wrapper.eq("student_id", profitChart.getStudentId())
                .eq("contest_id", profitChart.getContestId())
                .eq("bs_isxt", 1);
        if (profitChart.getPcDate() != null) {
            wrapper.eq("pc_date", profitChart.getPcDate());
        }
        return this.list(wrapper);
    }

    @Override
    public GbCtProfitChart getSys(GbCtProfitChart profitChart) {
        if (profitChart == null) {
            return null;
        }
        QueryWrapper<GbCtProfitChart> wrapper = new QueryWrapper<>();
        wrapper.eq("student_id", profitChart.getStudentId())
                .eq("contest_id", profitChart.getContestId())
                .eq("pc_date", profitChart.getPcDate())
                .eq("bs_isxt", 1);
        return this.getOne(wrapper);
    }


    @Override
    public GbCtProfitChart getTemp(Integer studentId, Integer contestId, Integer year) {
        QueryWrapper<GbCtProfitChart> wrapper = new QueryWrapper<>();
        wrapper.eq("student_id", studentId)
                .eq("contest_id", contestId)
                .eq("pc_date", year)
                .eq("bs_isxt", 0);
        return this.getOne(wrapper);
    }


    @Override
    public List<GbCtProfitChart> getCurrentYear(GbCtProfitChart profitChart) {
        if (profitChart == null) {
            return new ArrayList<>();
        }
        QueryWrapper<GbCtProfitChart> wrapper = new QueryWrapper<>();
        wrapper.eq("student_id", profitChart.getStudentId())
                .eq("contest_id", profitChart.getContestId())
                .eq("pc_date", profitChart.getPcDate());
        return this.list(wrapper);
    }

    @Override
    public GbCtProfitChart getOne(GbCtProfitChart ctProfitChart) {
        QueryWrapper<GbCtProfitChart> wrapper = new QueryWrapper<>();
        if (ctProfitChart.getStudentId() != null) {
            wrapper.eq("student_id", ctProfitChart.getStudentId());
        }
        if (ctProfitChart.getContestId() != null) {
            wrapper.eq("contest_id", ctProfitChart.getContestId());
        }
        if (ctProfitChart.getBsIsxt() != null) {
            wrapper.eq("bs_isxt", ctProfitChart.getBsIsxt());
        }
        if (ctProfitChart.getPcDate() != null) {
            wrapper.eq("pc_date", ctProfitChart.getPcDate());
        }
        return this.getOne(wrapper);
    }
}
