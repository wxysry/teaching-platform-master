package com.xiyou.main.service.gbcontest.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.xiyou.main.dao.gbcontest.GbCtSubjectMapper;
import com.xiyou.main.entity.gbcontest.GbCtSubject;
import com.xiyou.main.params.contest.SubjectParam;
import com.xiyou.main.service.gbcontest.GbCtSubjectService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author tangcan
 * @since 2019-08-25
 */
@Service
public class GbCtSubjectServiceImpl extends ServiceImpl<GbCtSubjectMapper, GbCtSubject> implements GbCtSubjectService {
    @Autowired
    private GbCtSubjectMapper gbCtSubjectMapper;

    @Override
    public Page<GbCtSubject> listByAdmin(SubjectParam subjectParam) {
        Page<GbCtSubject> page = new Page<>(subjectParam.getPage(), subjectParam.getLimit());
        return gbCtSubjectMapper.listByAdmin(page, subjectParam);
    }

    @Override
    public Page<GbCtSubject> listByTeacher(SubjectParam subjectParam) {
        Page<GbCtSubject> page = new Page<>(subjectParam.getPage(), subjectParam.getLimit());
        return gbCtSubjectMapper.listByTeacher(page, subjectParam);
    }

    @Override
    public List<GbCtSubject> listByTeacher(Integer teacherId,Integer groupId) {
        QueryWrapper<GbCtSubject> wrapper = new QueryWrapper<>();
        wrapper.eq("group_id", groupId)
                .eq("create_user_id",teacherId)
                .eq("upload", 1);
        return this.list(wrapper);
    }
}
