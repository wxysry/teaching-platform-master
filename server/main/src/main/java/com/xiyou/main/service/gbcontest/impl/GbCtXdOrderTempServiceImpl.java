package com.xiyou.main.service.gbcontest.impl;

import com.xiyou.main.entity.gbcontest.GbCtXdOrderTemp;
import com.xiyou.main.dao.gbcontest.GbCtXdOrderTempMapper;
import com.xiyou.main.service.gbcontest.GbCtXdOrderTempService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author wangxingyu
 * @since 2023-09-14
 */
@Service
public class GbCtXdOrderTempServiceImpl extends ServiceImpl<GbCtXdOrderTempMapper, GbCtXdOrderTemp> implements GbCtXdOrderTempService {

}
