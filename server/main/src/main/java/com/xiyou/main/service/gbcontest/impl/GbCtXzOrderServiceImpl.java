package com.xiyou.main.service.gbcontest.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.xiyou.main.dao.gbcontest.GbCtXzOrderMapper;
import com.xiyou.main.entity.gbcontest.GbCtXzOrder;
import com.xiyou.main.service.gbcontest.GbCtXzOrderService;
import com.xiyou.main.vo.gbcontest.GbXzOrderVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author tangcan
 * @since 2019-07-22
 */
@Service
public class GbCtXzOrderServiceImpl extends ServiceImpl<GbCtXzOrderMapper, GbCtXzOrder> implements GbCtXzOrderService {
    @Autowired
    private GbCtXzOrderMapper ctXzOrderMapper;

    @Override
    public List<GbCtXzOrder> list(GbCtXzOrder order) {
        return ctXzOrderMapper.list(order);
    }

    @Override
    public Map<String, BigDecimal> getSalesAndDirectCost(Integer studentId, Integer contestId, int year) {
        return ctXzOrderMapper.getSalesAndDirectCost(studentId, contestId, year);
    }

    @Override
    public List<GbXzOrderVo> listDeliveryOrder(Map<String, Object> queryMap) {
        return ctXzOrderMapper.listDeliveryOrder(queryMap);
    }
}
