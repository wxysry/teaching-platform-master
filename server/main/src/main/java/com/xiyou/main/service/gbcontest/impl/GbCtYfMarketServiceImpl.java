package com.xiyou.main.service.gbcontest.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.xiyou.main.dao.gbcontest.GbCtYfMarketMapper;
import com.xiyou.main.entity.gbcontest.GbCtYfMarket;
import com.xiyou.main.service.gbcontest.GbCtYfMarketService;
import com.xiyou.main.vo.gbcontest.GbMarketVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author tangcan
 * @since 2019-07-22
 */
@Service
public class GbCtYfMarketServiceImpl extends ServiceImpl<GbCtYfMarketMapper, GbCtYfMarket> implements GbCtYfMarketService {
    @Autowired
    private GbCtYfMarketMapper ctYfMarketMapper;

    @Override
    public void update(GbCtYfMarket yfMarket) {
        ctYfMarketMapper.update(yfMarket);
    }

    @Override
    public List<String> getHadYfFinishMarkets(Integer studentId, Integer contestId) {
        return ctYfMarketMapper.getHadYfFinishMarkets(studentId, contestId);
    }

    @Override
    public List<GbMarketVo> listYfMarket(Integer studentId, Integer contestId) {
        return ctYfMarketMapper.listYfMarket(studentId, contestId);
    }
}
