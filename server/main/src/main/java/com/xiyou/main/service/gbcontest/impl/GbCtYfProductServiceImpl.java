package com.xiyou.main.service.gbcontest.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.xiyou.main.dao.gbcontest.GbCtYfProductMapper;
import com.xiyou.main.entity.gbcontest.GbCtYfProduct;
import com.xiyou.main.service.gbcontest.GbCtYfProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author tangcan
 * @since 2019-07-22
 */
@Service
public class GbCtYfProductServiceImpl extends ServiceImpl<GbCtYfProductMapper, GbCtYfProduct> implements GbCtYfProductService {
    @Autowired
    private GbCtYfProductMapper ctYfProductMapper;

    @Override
    public void update(GbCtYfProduct yfProduct) {
        ctYfProductMapper.update(yfProduct);
    }

    @Override
    public void updateEndingSeason(Integer userId, Integer contestId, Integer date) {
        ctYfProductMapper.updateEndingSeason(userId, contestId, date);
    }
}
