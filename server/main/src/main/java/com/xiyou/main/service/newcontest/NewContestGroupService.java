package com.xiyou.main.service.newcontest;

import com.baomidou.mybatisplus.extension.service.IService;
import com.xiyou.main.entity.newcontest.NewContestGroup;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author tangcan
 * @since 2019-07-09
 */
public interface NewContestGroupService extends IService<NewContestGroup> {

    int insert(NewContestGroup newContestGroup);

    boolean checkNameExist(NewContestGroup newContestGroup);

    List<NewContestGroup> getListByTeacher(Integer teacherId);
}
