package com.xiyou.main.service.newcontest;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.xiyou.main.entity.newcontest.NewContest;
import com.xiyou.main.params.contest.ContestParam;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author tangcan
 * @since 2019-08-25
 */
public interface NewContestService extends IService<NewContest> {

    Page<NewContest> getPage(ContestParam contestParam);

    int getUnEndContest(Integer teacherId);

    int publish(Integer teacherId, Integer contestId);

    Page<NewContest> getStudentContestPage(ContestParam contestParam);
}
