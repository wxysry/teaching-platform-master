package com.xiyou.main.service.newcontest;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.xiyou.main.entity.contest.ContestStudent;
import com.xiyou.main.entity.newcontest.NewContestStudent;
import com.xiyou.main.params.contest.ContestStudentParam;
import com.xiyou.main.vo.contest.ContestScore;

import java.util.List;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author tangcan
 * @since 2019-08-25
 */
public interface NewContestStudentService extends IService<NewContestStudent> {

    int insertBatch(Integer contestId, List<Integer> studentIds);

    void remove(Integer contestId);

    NewContestStudent get(Integer contestId, Integer studentId);

    Page<NewContestStudent> getPage(ContestStudentParam contestStudentParam);

    List<Integer> getStudentIdList(Integer contestId);

    void updateScore(Integer id, Double score);

    List<ContestScore> getScoreList(Integer contestId);

    void updateErrorReportYear(NewContestStudent newContestStudent, Integer errorYear);

    void updateErrorReportYear(NewContestStudent newContestStudent);
}
