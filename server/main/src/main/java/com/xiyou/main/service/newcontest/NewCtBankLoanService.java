package com.xiyou.main.service.newcontest;

import com.baomidou.mybatisplus.extension.service.IService;
import com.xiyou.main.entity.contest.CtBankLoan;
import com.xiyou.main.entity.newcontest.NewCtBankLoan;
import com.xiyou.main.vo.contest.CashFolowEntity;
import com.xiyou.main.vo.newcontest.NewCashFolowEntity;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author tangcan
 * @since 2019-07-22
 */
public interface NewCtBankLoanService extends IService<NewCtBankLoan> {

    /**
     * bank_loan表中贷款类型为2且归还时间=当前时间的金额
     * @param cashFolowEntity
     * @return
     */
    Integer getCurrentMoney(NewCashFolowEntity cashFolowEntity);


    List<NewCtBankLoan> list(Integer studentId, Integer contestId);
}
