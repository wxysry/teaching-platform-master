package com.xiyou.main.service.newcontest;

import com.baomidou.mybatisplus.extension.service.IService;
import com.xiyou.main.entity.newcontest.NewCtCashflow;
import com.xiyou.main.vo.newcontest.NewCashFolowEntity;

import java.util.List;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author wangxingyu
 * @since 2023-06-11
 */
public interface NewCtCashflowService extends IService<NewCtCashflow> {


    /**
     * 用户当前现金
     *
     * @param studentId
     * @param contestId
     * @return
     */
    NewCashFolowEntity getStudentFinanceInfor(Integer studentId, Integer contestId);

    /**
     * student上一条现金
     *
     * @param newCashFolowEntity
     * @return
     */
    Integer getStudentLastCash(NewCashFolowEntity newCashFolowEntity);

    /**
     * 查出用户当前现金余额
     *
     * @param userId
     * @param contestId
     * @return
     */
    Integer getCash(Integer userId, Integer contestId);

    List<NewCtCashflow> list(Integer studentId, Integer contestId, Integer year, List<String> actionList);

}
