package com.xiyou.main.service.newcontest;

import com.baomidou.mybatisplus.extension.service.IService;
import com.xiyou.main.entity.contest.CtCharges;
import com.xiyou.main.entity.newcontest.NewCtCharges;

import java.util.List;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author tangcan
 * @since 2019-07-22
 */
public interface NewCtChargesService extends IService<NewCtCharges> {

    int addBySys(NewCtCharges charges);

    int updateTotal(Integer chargesId);

    /**
     * @Author: tangcan
     * @Description: 获取系统填入的
     * @Param: [studentId, contestId, year]
     * @date: 2019/9/10
    */
    NewCtCharges getSys(Integer studentId, Integer contestId, int year);


    NewCtCharges getTemp(Integer studentId, Integer contestId, int year);

    List<NewCtCharges> getCurrentYear(NewCtCharges charges);

    Integer getTotal(Integer studentId, Integer contestId, int year, int isxt);

    NewCtCharges getOne(NewCtCharges ctCharges);
}
