package com.xiyou.main.service.newcontest;

import com.baomidou.mybatisplus.extension.service.IService;
import com.xiyou.main.entity.contest.CtFee;
import com.xiyou.main.entity.newcontest.NewCtFee;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author tangcan
 * @since 2019-07-22
 */
public interface NewCtFeeService extends IService<NewCtFee> {

    int getFeeSum(NewCtFee fee);

}
