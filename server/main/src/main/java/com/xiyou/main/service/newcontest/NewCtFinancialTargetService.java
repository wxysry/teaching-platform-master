package com.xiyou.main.service.newcontest;

import com.xiyou.main.entity.newcontest.NewCtFinancialTarget;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author wangxingyu
 * @since 2023-06-28
 */
public interface NewCtFinancialTargetService extends IService<NewCtFinancialTarget> {

    NewCtFinancialTarget get(NewCtFinancialTarget ctFinancialTarget);

    /**
     * 获取暂存数据
     * @return
     */
    NewCtFinancialTarget getTemp(Integer studentId, Integer contestId, int year);
}
