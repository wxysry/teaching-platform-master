package com.xiyou.main.service.newcontest;

import com.xiyou.main.entity.contest.CtGzCs;
import com.xiyou.main.entity.newcontest.NewCtGzCs;
import com.baomidou.mybatisplus.extension.service.IService;
import com.xiyou.main.vo.contest.CashFolowEntity;
import com.xiyou.main.vo.newcontest.NewCashFolowEntity;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author wangxingyu
 * @since 2023-06-06
 */
public interface NewCtGzCsService extends IService<NewCtGzCs> {
    NewCtGzCs getByContestId(Integer contestId);




    NewCtGzCs getBySubjectNumber(Integer subjectNumber);
}
