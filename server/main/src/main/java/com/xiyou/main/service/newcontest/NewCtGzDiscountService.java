package com.xiyou.main.service.newcontest;

import com.xiyou.main.entity.newcontest.NewCtGzDiscount;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author wangxingyu
 * @since 2023-06-06
 */
public interface NewCtGzDiscountService extends IService<NewCtGzDiscount> {

}
