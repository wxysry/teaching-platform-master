package com.xiyou.main.service.newcontest;

import com.xiyou.main.entity.newcontest.NewCtGzLoan;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author wangxingyu
 * @since 2023-06-06
 */
public interface NewCtGzLoanService extends IService<NewCtGzLoan> {
    List<NewCtGzLoan> listCtGzLoan(Integer newContestId);
}
