package com.xiyou.main.service.newcontest;

import com.xiyou.main.entity.contest.CtGzProductLine;
import com.xiyou.main.entity.newcontest.NewCtGzProductLine;
import com.baomidou.mybatisplus.extension.service.IService;
import com.xiyou.main.vo.contest.CashFolowEntity;
import com.xiyou.main.vo.newcontest.NewCashFolowEntity;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author wangxingyu
 * @since 2023-06-06
 */
public interface NewCtGzProductLineService extends IService<NewCtGzProductLine> {
    /**
     * 选择生产线类型
     * @param contestId
     * @return
     */
    List<NewCtGzProductLine> getProductLineTypeList(Integer contestId);

    /**
     * 该生产线的投资时间
     * @param cashFolowEntity
     * @return
     */
    Integer getProductLineInfo(NewCashFolowEntity cashFolowEntity);

    List<NewCtGzProductLine> getListByCplids(List<Integer> cplids);

}
