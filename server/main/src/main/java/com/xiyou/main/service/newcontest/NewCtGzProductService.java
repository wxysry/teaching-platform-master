package com.xiyou.main.service.newcontest;

import com.xiyou.main.entity.contest.CtGzProduct;
import com.xiyou.main.entity.newcontest.NewCtGzProduct;
import com.baomidou.mybatisplus.extension.service.IService;
import com.xiyou.main.vo.contest.YfProductVO;
import com.xiyou.main.vo.newcontest.NewYfProductVO;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author wangxingyu
 * @since 2023-06-06
 */
public interface NewCtGzProductService extends IService<NewCtGzProduct> {
    /**
     * 查出产品研发列表
     * @param userId
     * @param contestId
     * @return
     */
    List<NewYfProductVO> listYfProduct(Integer userId, Integer contestId);

    /**
     * 产品名称
     * @param contestId
     * @return
     */
    List<String> getProductCPName(Integer contestId);


    List<NewCtGzProduct> getList(Integer contestId);

    List<NewCtGzProduct> getListByIds(List<Integer> cpIds);

}
