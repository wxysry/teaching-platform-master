package com.xiyou.main.service.newcontest;

import com.baomidou.mybatisplus.extension.service.IService;
import com.xiyou.main.entity.contest.CtKcMaterial;
import com.xiyou.main.entity.newcontest.NewCtKcMaterial;
import com.xiyou.main.vo.contest.CashFolowEntity;
import com.xiyou.main.vo.contest.MaterialVo;
import com.xiyou.main.vo.newcontest.NewCashFolowEntity;
import com.xiyou.main.vo.newcontest.NewMaterialVo;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author tangcan
 * @since 2019-07-22
 */
public interface NewCtKcMaterialService extends IService<NewCtKcMaterial> {


    void updateKcMaterial(NewCashFolowEntity cashFolowEntity);

    int getMaterialSum(Integer studentId, Integer contestId);

    //查看生产线所需要的材料
    List<NewCtKcMaterial> listNeed(Integer contestId, List<Integer> lineIds);

    //查出库存材料数量
    List<NewCtKcMaterial> listKc(Integer contestId, Integer userId);

    //查出材料库存的售出价格
    List<NewCtKcMaterial> listSellKc(Integer contestId, Integer userId);
}
