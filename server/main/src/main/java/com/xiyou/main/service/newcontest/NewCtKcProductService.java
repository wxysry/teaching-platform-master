package com.xiyou.main.service.newcontest;

import com.baomidou.mybatisplus.extension.service.IService;
import com.xiyou.main.entity.contest.CtKcProduct;
import com.xiyou.main.entity.newcontest.NewCtKcProduct;
import com.xiyou.main.vo.contest.ProductVo;
import com.xiyou.main.vo.newcontest.NewProductVo;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author tangcan
 * @since 2019-07-22
 */
public interface NewCtKcProductService extends IService<NewCtKcProduct> {

    int getProductSum(Integer studentId, Integer contestId);

    List<NewProductVo> listSellKc(Integer contestId, Integer userId);
}
