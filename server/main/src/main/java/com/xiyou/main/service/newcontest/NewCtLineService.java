package com.xiyou.main.service.newcontest;

import com.baomidou.mybatisplus.extension.service.IService;
import com.xiyou.main.entity.contest.CtLine;
import com.xiyou.main.entity.newcontest.NewCtGzProductLine;
import com.xiyou.main.entity.newcontest.NewCtLine;
import com.xiyou.main.vo.newcontest.NewOnlineLine;

import java.util.List;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author tangcan
 * @since 2019-07-22
 */
public interface NewCtLineService extends IService<NewCtLine> {

    /**
     * 列出所有在建中的生产线
     * @param userId
     * @param contestId
     * @return
     */
    List<NewOnlineLine> listOnline(Integer userId, Integer contestId, Integer date);

    /**
     * 通过生产线的id查生产线的类型信息
     * @param lineIds
     * @return
     */
    List<NewCtGzProductLine> getLineInfo(List<Integer> lineIds);

    /**
     * 列出所有可以转产的生产线
     * @param userId
     * @param contestId
     * @return
     */
    List<NewOnlineLine> listTransfer(Integer userId, Integer contestId);

    int getMaintenanceFee(Integer studentId, Integer contestId, Integer season);

    void updateDepFee(Integer studentId, Integer contestId, Integer date);

    Integer getDepTotal(Integer studentId, Integer contestId, int date);

    int getProductInProcess(NewCtLine line);

    int getEquipmentSum(Integer studentId, Integer contestId);

    List<NewCtLine> list(Integer studentId, Integer contestId);


    /**
     * 查出所有空闲的可以生产的生产线
     * @param userId
     * @param contestId
     * @return
     */
    List<NewOnlineLine> listNoProducting(Integer userId, Integer contestId);

    /**
     * 显示line中PL_Remain_Date 不为空的数据
     * @param userId
     * @param contestId
     * @return
     */
    List<NewOnlineLine> getListOnlineInfo(Integer userId, Integer contestId);
}
