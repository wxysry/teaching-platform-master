package com.xiyou.main.service.newcontest;

import com.baomidou.mybatisplus.extension.service.IService;
import com.xiyou.main.entity.contest.CtMnAd;
import com.xiyou.main.entity.newcontest.NewCtMnAd;

import java.util.List;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author tangcan
 * @since 2019-07-25
 */
public interface NewCtMnAdService extends IService<NewCtMnAd> {

    List<NewCtMnAd> getAdList(Integer contestId, Integer studentId, Integer year,Integer quarterly);

    List<NewCtMnAd> getAdListByGroupNum(Integer contestId, Integer studentId, String name);
}
