package com.xiyou.main.service.newcontest;

import com.baomidou.mybatisplus.extension.service.IService;
import com.xiyou.main.entity.contest.CtMnChoose;
import com.xiyou.main.entity.newcontest.NewCtMnChoose;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author tangcan
 * @since 2019-08-27
 */
public interface NewCtMnChooseService extends IService<NewCtMnChoose> {

}
