package com.xiyou.main.service.newcontest;

import com.baomidou.mybatisplus.extension.service.IService;
import com.xiyou.main.entity.contest.CtProfitChart;
import com.xiyou.main.entity.newcontest.NewCtProfitChart;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author tangcan
 * @since 2019-07-22
 */
public interface NewCtProfitChartService extends IService<NewCtProfitChart> {

    List<NewCtProfitChart> list(NewCtProfitChart profitChart);

    NewCtProfitChart getSys(NewCtProfitChart profitChart);


    NewCtProfitChart getTemp(Integer studentId, Integer contestId, Integer year);


    List<NewCtProfitChart> getCurrentYear(NewCtProfitChart profitChart);

    NewCtProfitChart getOne(NewCtProfitChart ctProfitChart);
}
