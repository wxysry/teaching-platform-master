package com.xiyou.main.service.newcontest;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.xiyou.main.entity.newcontest.NewCtSubject;
import com.xiyou.main.params.contest.SubjectParam;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author tangcan
 * @since 2019-08-25
 */
public interface NewCtSubjectService extends IService<NewCtSubject> {

    Page<NewCtSubject> listByAdmin(SubjectParam subjectParam);

    Page<NewCtSubject> listByTeacher(SubjectParam subjectParam);

    List<NewCtSubject> listByTeacher(Integer groupId);

}
