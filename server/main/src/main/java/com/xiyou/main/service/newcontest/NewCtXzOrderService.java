package com.xiyou.main.service.newcontest;

import com.baomidou.mybatisplus.extension.service.IService;
import com.xiyou.main.entity.contest.CtXzOrder;
import com.xiyou.main.entity.newcontest.NewCtXzOrder;
import com.xiyou.main.vo.contest.XzOrderVo;
import com.xiyou.main.vo.newcontest.NewXzOrderVo;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author tangcan
 * @since 2019-07-22
 */
public interface NewCtXzOrderService extends IService<NewCtXzOrder> {

    List<NewCtXzOrder> list(NewCtXzOrder order);

    Map<String, BigDecimal> getSalesAndDirectCost(Integer studentId, Integer contestId, int year);

    /**
     * 查出交货订单
     * @param queryMap
     * @return
     */
    List<NewXzOrderVo> listDeliveryOrder(Map<String, Object> queryMap);
}
