package com.xiyou.main.service.newcontest;

import com.baomidou.mybatisplus.extension.service.IService;
import com.xiyou.main.entity.contest.CtYfProduct;
import com.xiyou.main.entity.newcontest.NewCtYfProduct;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author tangcan
 * @since 2019-07-22
 */
public interface NewCtYfProductService extends IService<NewCtYfProduct> {

    void update(NewCtYfProduct yfProduct);

    /**
     * 当季结束更新产品研发
     * @param userId
     * @param contestId
     */
    void updateEndingSeason(Integer userId, Integer contestId, Integer date);
}
