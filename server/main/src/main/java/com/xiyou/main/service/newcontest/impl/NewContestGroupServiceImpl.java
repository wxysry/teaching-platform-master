package com.xiyou.main.service.newcontest.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.xiyou.main.dao.newcontest.NewContestGroupMapper;
import com.xiyou.main.entity.newcontest.NewContestGroup;
import com.xiyou.main.service.newcontest.NewContestGroupService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author tangcan
 * @since 2019-07-09
 */
@Service
public class NewContestGroupServiceImpl extends ServiceImpl<NewContestGroupMapper, NewContestGroup> implements NewContestGroupService {
    @Autowired
    private NewContestGroupMapper newContestGroupMapper;

    @Override
    public int insert(NewContestGroup newContestGroup) {
        return newContestGroupMapper.insert(newContestGroup);
    }

    @Override
    public boolean checkNameExist(NewContestGroup contestGroup) {
        QueryWrapper<NewContestGroup> wrapper = new QueryWrapper<>();
        wrapper.ne("id", contestGroup.getId())
                .eq("group_name", contestGroup.getGroupName());
        return this.getOne(wrapper) != null;
    }

    @Override
    public List<NewContestGroup> getListByTeacher(Integer teacherId) {
        return newContestGroupMapper.getListByTeacher(teacherId);
    }
}
