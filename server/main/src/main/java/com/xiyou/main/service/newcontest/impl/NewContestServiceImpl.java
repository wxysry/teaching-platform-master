package com.xiyou.main.service.newcontest.impl;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.xiyou.main.dao.newcontest.NewContestMapper;
import com.xiyou.main.entity.newcontest.NewContest;
import com.xiyou.main.params.contest.ContestParam;
import com.xiyou.main.service.newcontest.NewContestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author tangcan
 * @since 2019-08-25
 */
@Service
public class NewContestServiceImpl extends ServiceImpl<NewContestMapper, NewContest> implements NewContestService {
    @Autowired
    private NewContestMapper newContestMapper;

    @Override
    public Page<NewContest> getPage(ContestParam contestParam) {
        Page<NewContest> page = new Page<>(contestParam.getPage(), contestParam.getLimit());
        return newContestMapper.getPage(page, contestParam);
    }

    @Override
    public int getUnEndContest(Integer teacherId) {
        return newContestMapper.getUnEndContest(teacherId);
    }

    @Override
    public int publish(Integer teacherId, Integer contestId) {
        return newContestMapper.publish(teacherId, contestId);
    }

    @Override
    public Page<NewContest> getStudentContestPage(ContestParam contestParam) {
        Page<NewContest> page = new Page<>(contestParam.getPage(), contestParam.getLimit());
        return newContestMapper.getStudentContestPage(page, contestParam);
    }
}
