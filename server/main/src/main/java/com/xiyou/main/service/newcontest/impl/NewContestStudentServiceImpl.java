package com.xiyou.main.service.newcontest.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.xiyou.main.dao.newcontest.NewContestStudentMapper;
import com.xiyou.main.entity.contest.ContestStudent;
import com.xiyou.main.entity.newcontest.NewContestStudent;
import com.xiyou.main.params.contest.ContestStudentParam;
import com.xiyou.main.service.newcontest.NewContestStudentService;
import com.xiyou.main.vo.contest.ContestScore;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author tangcan
 * @since 2019-08-25
 */
@Service
public class NewContestStudentServiceImpl extends ServiceImpl<NewContestStudentMapper, NewContestStudent> implements NewContestStudentService {

    @Autowired
    private NewContestStudentMapper newContestStudentMapper;

    @Override
    public int insertBatch(Integer contestId, List<Integer> studentIds) {
        if (studentIds == null || studentIds.size() == 0) {
            return 0;
        }
        return newContestStudentMapper.insertBatch(contestId, studentIds);
    }

    @Override
    public void remove(Integer contestId) {
        QueryWrapper<NewContestStudent> wrapper = new QueryWrapper<>();
        wrapper.eq("contest_id", contestId);
        this.remove(wrapper);
    }

    @Override
    public NewContestStudent get(Integer contestId, Integer studentId) {
        QueryWrapper<NewContestStudent> wrapper = new QueryWrapper<>();
        wrapper.eq("contest_id", contestId).eq("student_id", studentId);
        return this.getOne(wrapper);
    }

    @Override
    public Page<NewContestStudent> getPage(ContestStudentParam contestStudentParam) {
        Page<NewContestStudent> page = new Page<>(contestStudentParam.getPage(), contestStudentParam.getLimit());
        return newContestStudentMapper.getPage(page, contestStudentParam);
    }

    @Override
    public List<Integer> getStudentIdList(Integer contestId) {
        return newContestStudentMapper.getStudentIdList(contestId);
    }

    @Override
    public void updateScore(Integer id, Double score) {
        newContestStudentMapper.updateScore(id, score);
    }

    @Override
    public List<ContestScore> getScoreList(Integer contestId) {
        return newContestStudentMapper.getScoreList(contestId);
    }

    @Override
    public void updateErrorReportYear(NewContestStudent newContestStudent, Integer errorYear) {
        if (newContestStudent != null && errorYear != null) {
            String errorReportYear = newContestStudent.getErrorReportYear() == null ? "" : newContestStudent.getErrorReportYear();
            if (!errorReportYear.contains(String.valueOf(errorYear))) {
                // 不重复统计错误年份
                if (errorReportYear.length() > 0) {
                    errorReportYear += "," + errorYear;
                } else {
                    errorReportYear += errorYear;
                }
                newContestStudent.setErrorReportYear(errorReportYear);
                this.updateErrorReportYear(newContestStudent);
            }
        }
    }

    @Override
    public void updateErrorReportYear(NewContestStudent newContestStudent) {
        newContestStudentMapper.updateErrorReportYear(newContestStudent);
    }
}
