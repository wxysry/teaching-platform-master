package com.xiyou.main.service.newcontest.impl;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.xiyou.main.dao.newcontest.NewCtBankLoanMapper;
import com.xiyou.main.dao.newcontest.NewCtGzCsMapper;
import com.xiyou.main.entity.contest.CtBankLoan;
import com.xiyou.main.entity.newcontest.NewCtBankLoan;
import com.xiyou.main.service.newcontest.NewCtBankLoanService;
import com.xiyou.main.vo.contest.CashFolowEntity;
import com.xiyou.main.vo.newcontest.NewCashFolowEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author tangcan
 * @since 2019-07-22
 */
@Service
public class NewCtBankLoanServiceImpl extends ServiceImpl<NewCtBankLoanMapper, NewCtBankLoan> implements NewCtBankLoanService {
    @Autowired
    NewCtBankLoanMapper newCtBankLoanMapper;
    @Autowired
    NewCtGzCsMapper newCtGzCsMapper;

    @Override
    public List<NewCtBankLoan> list(Integer studentId, Integer contestId) {
        QueryWrapper<NewCtBankLoan> wrapper = new QueryWrapper<>();
        wrapper.eq("student_id", studentId).eq("contest_id", contestId);
        return this.list(wrapper);
    }

    @Override
    public Integer getCurrentMoney(NewCashFolowEntity cashFolowEntity) {
        return newCtBankLoanMapper.getCurrentMoney(cashFolowEntity);
    }

}
