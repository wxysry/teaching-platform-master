package com.xiyou.main.service.newcontest.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.xiyou.main.dao.newcontest.NewCtCashflowMapper;
import com.xiyou.main.entity.newcontest.NewCtCashflow;
import com.xiyou.main.service.newcontest.NewCtCashflowService;
import com.xiyou.main.vo.newcontest.NewCashFolowEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author wangxingyu
 * @since 2023-06-11
 */
@Service
public class NewCtCashflowServiceImpl extends ServiceImpl<NewCtCashflowMapper, NewCtCashflow> implements NewCtCashflowService {

    @Autowired
    NewCtCashflowMapper newCtCashflowMapper;


    @Override
    public NewCashFolowEntity getStudentFinanceInfor(Integer studentId, Integer contestId) {
        return null;
    }

    @Override
    public Integer getStudentLastCash(NewCashFolowEntity newCashFolowEntity) {
        return newCtCashflowMapper.getStudentLastCash(newCashFolowEntity);
    }


    @Override
    public Integer getCash(Integer userId, Integer contestId) {
        return newCtCashflowMapper.getCash(userId, contestId);
    }

    @Override
    public List<NewCtCashflow> list(Integer studentId, Integer contestId, Integer year, List<String> actionList) {
        return newCtCashflowMapper.list(studentId, contestId, year, actionList);
    }
}
