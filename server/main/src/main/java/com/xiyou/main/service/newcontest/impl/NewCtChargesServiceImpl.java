package com.xiyou.main.service.newcontest.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.xiyou.main.dao.newcontest.NewCtChargesMapper;
import com.xiyou.main.entity.contest.CtCharges;
import com.xiyou.main.entity.newcontest.NewCtCharges;
import com.xiyou.main.service.newcontest.NewCtChargesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author tangcan
 * @since 2019-07-22
 */
@Service
public class NewCtChargesServiceImpl extends ServiceImpl<NewCtChargesMapper, NewCtCharges> implements NewCtChargesService {
    @Autowired
    private NewCtChargesMapper newCtChargesMapper;

    @Override
    public int addBySys(NewCtCharges charges) {
        return newCtChargesMapper.addBySys(charges);
    }

    @Override
    public int updateTotal(Integer chargesId) {
        return newCtChargesMapper.updateTotal(chargesId);
    }

    @Override
    public NewCtCharges getSys(Integer studentId, Integer contestId, int year) {
        QueryWrapper<NewCtCharges> wrapper = new QueryWrapper<>();
        wrapper.eq("student_id", studentId)
                .eq("contest_id", contestId)
                .eq("c_date", year)
                .eq("bs_isxt", 1);
        return this.getOne(wrapper);
    }


    @Override
    public NewCtCharges getTemp(Integer studentId, Integer contestId, int year) {
        QueryWrapper<NewCtCharges> wrapper = new QueryWrapper<>();
        wrapper.eq("student_id", studentId)
                .eq("contest_id", contestId)
                .eq("c_date", year)
                .eq("bs_isxt", 0);
        return this.getOne(wrapper);
    }


    @Override
    public List<NewCtCharges> getCurrentYear(NewCtCharges charges) {
        if (charges == null) {
            return new ArrayList<>();
        }
        QueryWrapper<NewCtCharges> wrapper = new QueryWrapper<>();
        wrapper.eq("student_id", charges.getStudentId())
                .eq("contest_id", charges.getContestId())
                .eq("c_date", charges.getCDate());
        return this.list(wrapper);
    }

    @Override
    public Integer getTotal(Integer studentId, Integer contestId, int year, int isxt) {
        return newCtChargesMapper.getTotal(studentId, contestId, year, isxt);
    }

    @Override
    public NewCtCharges getOne(NewCtCharges ctCharges) {
        QueryWrapper<NewCtCharges> wrapper = new QueryWrapper<>();
        if (ctCharges.getStudentId() != null) {
            wrapper.eq("student_id", ctCharges.getStudentId());
        }
        if (ctCharges.getContestId() != null) {
            wrapper.eq("contest_id", ctCharges.getContestId());
        }
        if (ctCharges.getBsIsxt() != null) {
            wrapper.eq("bs_isxt", ctCharges.getBsIsxt());
        }
        if (ctCharges.getCDate() != null) {
            wrapper.eq("c_date", ctCharges.getCDate());
        }
        return this.getOne(wrapper);
    }
}
