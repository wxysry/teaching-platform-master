package com.xiyou.main.service.newcontest.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.xiyou.main.dao.contest.CtFeeMapper;
import com.xiyou.main.dao.newcontest.NewCtFeeMapper;
import com.xiyou.main.entity.contest.CtFee;
import com.xiyou.main.entity.newcontest.NewCtFee;
import com.xiyou.main.service.contest.CtFeeService;
import com.xiyou.main.service.newcontest.NewCtFeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author tangcan
 * @since 2019-07-22
 */
@Service
public class NewCtFeeServiceImpl extends ServiceImpl<NewCtFeeMapper, NewCtFee> implements NewCtFeeService {

    @Autowired
    CtFeeMapper ctFeeMapper;

    @Override
    public int getFeeSum(NewCtFee fee) {
        if (fee == null) {
            return 0;
        }
        QueryWrapper<NewCtFee> wrapper = new QueryWrapper<>();
        wrapper.eq("student_id", fee.getStudentId())
                .eq("contest_id", fee.getContestId());
        if (fee.getType() != null) {
            wrapper.eq("type", fee.getType());
        }
        List<NewCtFee> feeList = this.list(wrapper);
        return feeList.stream().mapToInt(NewCtFee::getRFee).sum();
    }
}
