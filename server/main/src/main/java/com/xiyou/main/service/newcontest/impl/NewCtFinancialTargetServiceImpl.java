package com.xiyou.main.service.newcontest.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.xiyou.main.entity.newcontest.NewCtBalance;
import com.xiyou.main.entity.newcontest.NewCtFinancialTarget;
import com.xiyou.main.dao.newcontest.NewCtFinancialTargetMapper;
import com.xiyou.main.service.newcontest.NewCtFinancialTargetService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author wangxingyu
 * @since 2023-06-28
 */
@Service
public class NewCtFinancialTargetServiceImpl extends ServiceImpl<NewCtFinancialTargetMapper, NewCtFinancialTarget> implements NewCtFinancialTargetService {


    @Override
    public NewCtFinancialTarget get(NewCtFinancialTarget ctFinancialTarget) {
        QueryWrapper<NewCtFinancialTarget> wrapper = new QueryWrapper<>();
        if (ctFinancialTarget.getStudentId() != null) {
            wrapper.eq("student_id", ctFinancialTarget.getStudentId());
        }
        if (ctFinancialTarget.getContestId() != null) {
            wrapper.eq("contest_id", ctFinancialTarget.getContestId());
        }
        if (ctFinancialTarget.getFtYear() != null) {
            wrapper.eq("ft_year", ctFinancialTarget.getFtYear());
        }
        wrapper.eq("ft_isxt", 1);
        return this.getOne(wrapper);
    }



    @Override
    public NewCtFinancialTarget getTemp(Integer studentId, Integer contestId, int year) {
        QueryWrapper<NewCtFinancialTarget> wrapper = new QueryWrapper<>();
        wrapper.eq("student_id", studentId);
        wrapper.eq("contest_id", contestId);
        wrapper.eq("ft_year", year);
        wrapper.eq("ft_isxt", 0);
        return this.getOne(wrapper);
    }
}
