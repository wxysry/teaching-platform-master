package com.xiyou.main.service.newcontest.impl;

import com.xiyou.main.entity.newcontest.NewCtGzAd;
import com.xiyou.main.dao.newcontest.NewCtGzAdMapper;
import com.xiyou.main.service.newcontest.NewCtGzAdService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author wangxingyu
 * @since 2023-06-06
 */
@Service
public class NewCtGzAdServiceImpl extends ServiceImpl<NewCtGzAdMapper, NewCtGzAd> implements NewCtGzAdService {

}
