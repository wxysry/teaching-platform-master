package com.xiyou.main.service.newcontest.impl;

import com.xiyou.main.entity.newcontest.NewCtGzClasses;
import com.xiyou.main.dao.newcontest.NewCtGzClassesMapper;
import com.xiyou.main.service.newcontest.NewCtGzClassesService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author wangxingyu
 * @since 2023-06-06
 */
@Service
public class NewCtGzClassesServiceImpl extends ServiceImpl<NewCtGzClassesMapper, NewCtGzClasses> implements NewCtGzClassesService {

}
