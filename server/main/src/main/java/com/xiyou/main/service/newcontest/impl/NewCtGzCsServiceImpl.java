package com.xiyou.main.service.newcontest.impl;

import com.xiyou.main.entity.newcontest.NewCtGzCs;
import com.xiyou.main.dao.newcontest.NewCtGzCsMapper;
import com.xiyou.main.service.newcontest.NewCtGzCsService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.xiyou.main.vo.newcontest.NewCashFolowEntity;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author wangxingyu
 * @since 2023-06-06
 */
@Service
public class NewCtGzCsServiceImpl extends ServiceImpl<NewCtGzCsMapper, NewCtGzCs> implements NewCtGzCsService {

    @Override
    public NewCtGzCs getByContestId(Integer contestId) {
        return baseMapper.getByContestId(contestId);
    }


    @Override
    public NewCtGzCs getBySubjectNumber(Integer subjectNumber) {

        return baseMapper.getBySubjectNumber(subjectNumber);
    }
}
