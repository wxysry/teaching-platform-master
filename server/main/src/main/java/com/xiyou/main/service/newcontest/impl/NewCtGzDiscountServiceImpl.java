package com.xiyou.main.service.newcontest.impl;

import com.xiyou.main.entity.newcontest.NewCtGzDiscount;
import com.xiyou.main.dao.newcontest.NewCtGzDiscountMapper;
import com.xiyou.main.service.newcontest.NewCtGzDiscountService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author wangxingyu
 * @since 2023-06-06
 */
@Service
public class NewCtGzDiscountServiceImpl extends ServiceImpl<NewCtGzDiscountMapper, NewCtGzDiscount> implements NewCtGzDiscountService {

}
