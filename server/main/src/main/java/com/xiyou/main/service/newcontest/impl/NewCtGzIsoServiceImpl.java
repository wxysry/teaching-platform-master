package com.xiyou.main.service.newcontest.impl;

import com.xiyou.main.entity.newcontest.NewCtGzIso;
import com.xiyou.main.dao.newcontest.NewCtGzIsoMapper;
import com.xiyou.main.service.newcontest.NewCtGzIsoService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author wangxingyu
 * @since 2023-06-06
 */
@Service
public class NewCtGzIsoServiceImpl extends ServiceImpl<NewCtGzIsoMapper, NewCtGzIso> implements NewCtGzIsoService {

}
