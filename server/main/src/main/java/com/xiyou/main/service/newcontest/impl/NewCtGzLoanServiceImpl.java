package com.xiyou.main.service.newcontest.impl;

import com.xiyou.main.entity.newcontest.NewCtGzLoan;
import com.xiyou.main.dao.newcontest.NewCtGzLoanMapper;
import com.xiyou.main.service.newcontest.NewCtGzLoanService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author wangxingyu
 * @since 2023-06-06
 */
@Service
public class NewCtGzLoanServiceImpl extends ServiceImpl<NewCtGzLoanMapper, NewCtGzLoan> implements NewCtGzLoanService {


    @Autowired
    NewCtGzLoanMapper newCtGzLoanMapper;
    @Override
    public List<NewCtGzLoan> listCtGzLoan(Integer newContestId) {
        return newCtGzLoanMapper.listCtGzLoan(newContestId);
    }
}
