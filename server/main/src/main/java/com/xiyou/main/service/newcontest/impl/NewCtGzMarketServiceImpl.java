package com.xiyou.main.service.newcontest.impl;

import com.xiyou.main.entity.newcontest.NewCtGzMarket;
import com.xiyou.main.dao.newcontest.NewCtGzMarketMapper;
import com.xiyou.main.service.newcontest.NewCtGzMarketService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author wangxingyu
 * @since 2023-06-06
 */
@Service
public class NewCtGzMarketServiceImpl extends ServiceImpl<NewCtGzMarketMapper, NewCtGzMarket> implements NewCtGzMarketService {

}
