package com.xiyou.main.service.newcontest.impl;

import com.xiyou.main.entity.newcontest.NewCtGzMaterial;
import com.xiyou.main.dao.newcontest.NewCtGzMaterialMapper;
import com.xiyou.main.service.newcontest.NewCtGzMaterialService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author wangxingyu
 * @since 2023-06-06
 */
@Service
public class NewCtGzMaterialServiceImpl extends ServiceImpl<NewCtGzMaterialMapper, NewCtGzMaterial> implements NewCtGzMaterialService {

}
