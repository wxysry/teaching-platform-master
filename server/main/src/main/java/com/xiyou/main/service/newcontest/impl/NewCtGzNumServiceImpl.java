package com.xiyou.main.service.newcontest.impl;

import com.xiyou.main.entity.newcontest.NewCtGzNum;
import com.xiyou.main.dao.newcontest.NewCtGzNumMapper;
import com.xiyou.main.service.newcontest.NewCtGzNumService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author wangxingyu
 * @since 2023-06-06
 */
@Service
public class NewCtGzNumServiceImpl extends ServiceImpl<NewCtGzNumMapper, NewCtGzNum> implements NewCtGzNumService {

}
