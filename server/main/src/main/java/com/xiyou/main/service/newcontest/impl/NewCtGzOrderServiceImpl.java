package com.xiyou.main.service.newcontest.impl;

import com.xiyou.main.entity.newcontest.NewCtGzOrder;
import com.xiyou.main.dao.newcontest.NewCtGzOrderMapper;
import com.xiyou.main.service.newcontest.NewCtGzOrderService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author wangxingyu
 * @since 2023-06-06
 */
@Service
public class NewCtGzOrderServiceImpl extends ServiceImpl<NewCtGzOrderMapper, NewCtGzOrder> implements NewCtGzOrderService {

}
