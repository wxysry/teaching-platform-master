package com.xiyou.main.service.newcontest.impl;

import com.xiyou.main.entity.newcontest.NewCtGzProducing;
import com.xiyou.main.dao.newcontest.NewCtGzProducingMapper;
import com.xiyou.main.service.newcontest.NewCtGzProducingService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author wangxingyu
 * @since 2023-06-06
 */
@Service
public class NewCtGzProducingServiceImpl extends ServiceImpl<NewCtGzProducingMapper, NewCtGzProducing> implements NewCtGzProducingService {

}
