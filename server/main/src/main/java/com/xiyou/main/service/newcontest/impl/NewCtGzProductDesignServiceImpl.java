package com.xiyou.main.service.newcontest.impl;

import com.xiyou.main.entity.newcontest.NewCtGzProductDesign;
import com.xiyou.main.dao.newcontest.NewCtGzProductDesignMapper;
import com.xiyou.main.service.newcontest.NewCtGzProductDesignService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author wangxingyu
 * @since 2023-06-06
 */
@Service
public class NewCtGzProductDesignServiceImpl extends ServiceImpl<NewCtGzProductDesignMapper, NewCtGzProductDesign> implements NewCtGzProductDesignService {

}
