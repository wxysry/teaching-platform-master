package com.xiyou.main.service.newcontest.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.xiyou.main.dao.contest.CtGzProductLineMapper;
import com.xiyou.main.entity.contest.CtGzProductLine;
import com.xiyou.main.entity.newcontest.NewCtGzProductLine;
import com.xiyou.main.dao.newcontest.NewCtGzProductLineMapper;
import com.xiyou.main.service.newcontest.NewCtGzProductLineService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.xiyou.main.vo.contest.CashFolowEntity;
import com.xiyou.main.vo.newcontest.NewCashFolowEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author wangxingyu
 * @since 2023-06-06
 */
@Service
public class NewCtGzProductLineServiceImpl extends ServiceImpl<NewCtGzProductLineMapper, NewCtGzProductLine> implements NewCtGzProductLineService {

    @Autowired
    NewCtGzProductLineMapper ctGzProductLineMapper;

    @Override
    public List<NewCtGzProductLine> getProductLineTypeList(Integer contestId) {
        return ctGzProductLineMapper.getProductLineTypeList(contestId);
    }

    @Override
    public Integer getProductLineInfo(NewCashFolowEntity cashFolowEntity) {

        return ctGzProductLineMapper.getProductLineInfo(cashFolowEntity);
    }

    @Override
    public List<NewCtGzProductLine> getListByCplids(List<Integer> cplids) {
        QueryWrapper<NewCtGzProductLine> wrapper = new QueryWrapper<>();
        wrapper.in("id", cplids);
        return this.list(wrapper);
    }

}
