package com.xiyou.main.service.newcontest.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.xiyou.main.dao.contest.CtGzProductMapper;
import com.xiyou.main.entity.contest.CtGzProduct;
import com.xiyou.main.entity.newcontest.NewCtGzProduct;
import com.xiyou.main.dao.newcontest.NewCtGzProductMapper;
import com.xiyou.main.service.newcontest.NewCtGzProductService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.xiyou.main.vo.contest.YfProductVO;
import com.xiyou.main.vo.newcontest.NewYfProductVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author wangxingyu
 * @since 2023-06-06
 */
@Service
public class NewCtGzProductServiceImpl extends ServiceImpl<NewCtGzProductMapper, NewCtGzProduct> implements NewCtGzProductService {

    @Autowired
    NewCtGzProductMapper mapper;

    @Override
    public List<String> getProductCPName(Integer contestId) {
        return mapper.getProductCPName(contestId);
    }

    @Override
    public List<NewYfProductVO> listYfProduct(Integer userId, Integer contestId) {
        return mapper.listYfProduct(userId, contestId);
    }

    @Override
    public List<NewCtGzProduct> getList(Integer contestId) {
        return mapper.getList(contestId);
    }

    @Override
    public List<NewCtGzProduct> getListByIds(List<Integer> cpIds) {
        if (cpIds == null || cpIds.size() == 0) {
            return new ArrayList<>();
        }
        QueryWrapper<NewCtGzProduct> wrapper = new QueryWrapper<>();
        wrapper.in("id", cpIds);
        return this.list(wrapper);
    }
}
