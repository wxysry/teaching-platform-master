package com.xiyou.main.service.newcontest.impl;

import com.xiyou.main.entity.newcontest.NewCtGzWorkerIncentive;
import com.xiyou.main.dao.newcontest.NewCtGzWorkerIncentiveMapper;
import com.xiyou.main.service.newcontest.NewCtGzWorkerIncentiveService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author wangxingyu
 * @since 2023-06-06
 */
@Service
public class NewCtGzWorkerIncentiveServiceImpl extends ServiceImpl<NewCtGzWorkerIncentiveMapper, NewCtGzWorkerIncentive> implements NewCtGzWorkerIncentiveService {

}
