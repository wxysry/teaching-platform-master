package com.xiyou.main.service.newcontest.impl;

import com.xiyou.main.entity.newcontest.NewCtGzWorkerRecruit;
import com.xiyou.main.dao.newcontest.NewCtGzWorkerRecruitMapper;
import com.xiyou.main.service.newcontest.NewCtGzWorkerRecruitService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author wangxingyu
 * @since 2023-06-06
 */
@Service
public class NewCtGzWorkerRecruitServiceImpl extends ServiceImpl<NewCtGzWorkerRecruitMapper, NewCtGzWorkerRecruit> implements NewCtGzWorkerRecruitService {

}
