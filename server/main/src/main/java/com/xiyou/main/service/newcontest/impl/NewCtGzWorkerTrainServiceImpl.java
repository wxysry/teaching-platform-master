package com.xiyou.main.service.newcontest.impl;

import com.xiyou.main.entity.newcontest.NewCtGzWorkerTrain;
import com.xiyou.main.dao.newcontest.NewCtGzWorkerTrainMapper;
import com.xiyou.main.service.newcontest.NewCtGzWorkerTrainService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author wangxingyu
 * @since 2023-06-06
 */
@Service
public class NewCtGzWorkerTrainServiceImpl extends ServiceImpl<NewCtGzWorkerTrainMapper, NewCtGzWorkerTrain> implements NewCtGzWorkerTrainService {

}
