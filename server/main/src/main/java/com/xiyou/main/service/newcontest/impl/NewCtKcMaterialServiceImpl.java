package com.xiyou.main.service.newcontest.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.xiyou.main.dao.newcontest.NewCtKcMaterialMapper;
import com.xiyou.main.entity.contest.CtGzCs;
import com.xiyou.main.entity.newcontest.NewCtGzCs;
import com.xiyou.main.entity.newcontest.NewCtKcMaterial;
import com.xiyou.main.service.newcontest.NewCtGzCsService;
import com.xiyou.main.service.newcontest.NewCtKcMaterialService;
import com.xiyou.main.vo.contest.MaterialVo;
import com.xiyou.main.vo.newcontest.NewCashFolowEntity;
import com.xiyou.main.vo.newcontest.NewMaterialVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author tangcan
 * @since 2019-07-22
 */
@Service
public class NewCtKcMaterialServiceImpl extends ServiceImpl<NewCtKcMaterialMapper, NewCtKcMaterial> implements NewCtKcMaterialService {
    @Autowired
    private NewCtKcMaterialMapper ctKcMaterialMapper;
    @Autowired
    private NewCtGzCsService ctGzCsService;

    @Override
    public int getMaterialSum(Integer studentId, Integer contestId) {
        return ctKcMaterialMapper.getMaterialSum(studentId, contestId);
    }

    @Override
    public List<NewCtKcMaterial> listNeed(Integer contestId, List<Integer> lineIds) {
        if (lineIds.size() == 0) {
            return new ArrayList<>();
        }
        return ctKcMaterialMapper.listNeed(contestId, lineIds);
    }


    @Override
    public void updateKcMaterial(NewCashFolowEntity cashFolowEntity) {
        ctKcMaterialMapper.updateKcMaterial(cashFolowEntity);
    }

    @Override
    public List<NewCtKcMaterial> listKc(Integer contestId, Integer userId) {
        List<NewCtKcMaterial> list = ctKcMaterialMapper.listKc(contestId, userId);
        NewCtGzCs ctGzCs = ctGzCsService.getByContestId(contestId);
        if (list.size() > 0) {
            for (NewCtKcMaterial l : list) {
                l.setMaterialPrice(l.getMaterialPrice() * ctGzCs.getEmergenceMultipleMaterial());
            }
        }
        return list;
    }

    @Override
    public List<NewCtKcMaterial> listSellKc(Integer contestId, Integer userId) {
        List<NewCtKcMaterial> list = ctKcMaterialMapper.listSellKc(contestId, userId);
        NewCtGzCs ctGzCs = ctGzCsService.getByContestId(contestId);
        if (list.size() > 0) {
//            for (NewCtKcMaterial l : list) {
//                l.setMaterialPrice(l.getMaterialPrice() * ctGzCs.getEmergenceMultipleMaterial());
//                l.setSellPrice((double) l.getDirectCost() * ((double) ctGzCs.getInventoryDiscountMaterial() / 100.0));
//            }
        }
        return list;
    }
}
