package com.xiyou.main.service.newcontest.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.xiyou.main.dao.contest.CtKcProductMapper;
import com.xiyou.main.dao.newcontest.NewCtKcProductMapper;
import com.xiyou.main.entity.contest.CtGzCs;
import com.xiyou.main.entity.contest.CtKcProduct;
import com.xiyou.main.entity.newcontest.NewCtGzCs;
import com.xiyou.main.entity.newcontest.NewCtKcProduct;
import com.xiyou.main.service.contest.CtGzCsService;
import com.xiyou.main.service.contest.CtKcProductService;
import com.xiyou.main.service.newcontest.NewCtGzCsService;
import com.xiyou.main.service.newcontest.NewCtKcProductService;
import com.xiyou.main.vo.contest.ProductVo;
import com.xiyou.main.vo.newcontest.NewProductVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author tangcan
 * @since 2019-07-22
 */
@Service
public class NewCtKcProductServiceImpl extends ServiceImpl<NewCtKcProductMapper, NewCtKcProduct> implements NewCtKcProductService {
    @Autowired
    private NewCtKcProductMapper ctKcProductMapper;
    @Autowired
    private NewCtGzCsService ctGzCsService;


    @Override
    public int getProductSum(Integer studentId, Integer contestId) {
        return ctKcProductMapper.getProductSum(studentId, contestId);
    }



    @Override
    public List<NewProductVo> listSellKc(Integer contestId, Integer userId) {
        List<NewProductVo> list = ctKcProductMapper.listSellKc(contestId, userId);
        NewCtGzCs ctGzCs = ctGzCsService.getByContestId(contestId);
        if (list.size() > 0) {
            for (NewProductVo l : list) {
                l.setUrgentPrice(l.getDirectCost() * ctGzCs.getEmergenceMultipleProduct());
                l.setSellPrice((double) l.getDirectCost() * ((double) ctGzCs.getInventoryDiscountProduct() / 100.0));
            }
        }
        return list;
    }
}
