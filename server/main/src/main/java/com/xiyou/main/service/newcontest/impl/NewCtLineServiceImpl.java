package com.xiyou.main.service.newcontest.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.xiyou.main.dao.contest.CtLineMapper;
import com.xiyou.main.dao.newcontest.NewCtLineMapper;
import com.xiyou.main.entity.contest.CtGzProductLine;
import com.xiyou.main.entity.contest.CtLine;
import com.xiyou.main.entity.newcontest.NewCtGzProductLine;
import com.xiyou.main.entity.newcontest.NewCtLine;
import com.xiyou.main.service.contest.CtLineService;
import com.xiyou.main.service.newcontest.NewCtLineService;
import com.xiyou.main.vo.contest.OnlineLine;
import com.xiyou.main.vo.newcontest.NewOnlineLine;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author tangcan
 * @since 2019-07-22
 */
@Service
public class NewCtLineServiceImpl extends ServiceImpl<NewCtLineMapper, NewCtLine> implements NewCtLineService {
    @Autowired
    NewCtLineMapper ctLineMapper;

    @Override
    public List<NewOnlineLine> listOnline(Integer userId, Integer contestId, Integer date) {
        return ctLineMapper.listOnline(userId, contestId, date);
    }

    @Override
    public List<NewCtGzProductLine> getLineInfo(List<Integer> lineIds) {
        return ctLineMapper.getLineInfo(lineIds);
    }

    @Override
    public List<NewOnlineLine> listTransfer(Integer userId, Integer contestId) {
        return ctLineMapper.listTransfer(userId, contestId);
    }

    @Override
    public int getMaintenanceFee(Integer studentId, Integer contestId, Integer date) {
        return ctLineMapper.getMaintenanceFee(studentId, contestId, date);
    }

    @Override
    public void updateDepFee(Integer studentId, Integer contestId, Integer date) {
        ctLineMapper.updateDepFee(studentId, contestId, date);
    }

    @Override
    public Integer getDepTotal(Integer studentId, Integer contestId, int date) {
        return ctLineMapper.getDepTotal(studentId, contestId, date);
    }

    @Override
    public int getProductInProcess(NewCtLine line) {
        if (line == null) {
            return 0;
        }
        return ctLineMapper.getProductInProcess(line);
    }

    @Override
    public int getEquipmentSum(Integer studentId, Integer contestId) {
        return ctLineMapper.getEquipmentSum(studentId, contestId);
    }

    @Override
    public List<NewCtLine> list(Integer studentId, Integer contestId) {
        QueryWrapper<NewCtLine> wrapper = new QueryWrapper<>();
        wrapper.eq("student_id", studentId).eq("contest_id", contestId);
        return this.list(wrapper);
    }

    @Override
    public List<NewOnlineLine> listNoProducting(Integer userId, Integer contestId) {
        return ctLineMapper.listNoProducting(userId, contestId);
    }

    @Override
    public List<NewOnlineLine> getListOnlineInfo(Integer userId, Integer contestId) {
        return ctLineMapper.getListOnline(userId, contestId);
    }


}
