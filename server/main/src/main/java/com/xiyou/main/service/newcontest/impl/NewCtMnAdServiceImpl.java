package com.xiyou.main.service.newcontest.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.xiyou.main.dao.newcontest.NewCtMnAdMapper;
import com.xiyou.main.entity.contest.CtMnAd;
import com.xiyou.main.entity.newcontest.NewCtMnAd;
import com.xiyou.main.service.newcontest.NewCtMnAdService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author tangcan
 * @since 2019-07-25
 */
@Service
public class NewCtMnAdServiceImpl extends ServiceImpl<NewCtMnAdMapper, NewCtMnAd> implements NewCtMnAdService {
    @Autowired
    private NewCtMnAdMapper newCtMnAdMapper;

    @Override
    public List<NewCtMnAd> getAdList(Integer contestId, Integer studentId, Integer year ,Integer quarterly) {
        return newCtMnAdMapper.getAdList(contestId, studentId, year,quarterly);
    }

    @Override
    public List<NewCtMnAd> getAdListByGroupNum(Integer contestId, Integer studentId, String groupNum) {
        QueryWrapper<NewCtMnAd> wrapper = new QueryWrapper<>();
        wrapper.eq("student_id", studentId)
                .eq("contest_id", contestId)
                .eq("group_num", groupNum)
                .eq("is_submit","Y")
                .orderByAsc("year","quarterly");
        return this.list(wrapper);
    }
}
