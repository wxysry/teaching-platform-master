package com.xiyou.main.service.newcontest.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.xiyou.main.dao.contest.CtMnChooseMapper;
import com.xiyou.main.dao.newcontest.NewCtMnChooseMapper;
import com.xiyou.main.entity.contest.CtMnChoose;
import com.xiyou.main.entity.newcontest.NewCtMnChoose;
import com.xiyou.main.service.contest.CtMnChooseService;
import com.xiyou.main.service.newcontest.NewCtMnChooseService;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author tangcan
 * @since 2019-08-27
 */
@Service
public class NewCtMnChooseServiceImpl extends ServiceImpl<NewCtMnChooseMapper, NewCtMnChoose> implements NewCtMnChooseService {

}
