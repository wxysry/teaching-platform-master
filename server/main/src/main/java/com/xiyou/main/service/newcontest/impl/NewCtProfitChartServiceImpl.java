package com.xiyou.main.service.newcontest.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.xiyou.main.dao.contest.CtProfitChartMapper;
import com.xiyou.main.dao.newcontest.NewCtProfitChartMapper;
import com.xiyou.main.entity.contest.CtProfitChart;
import com.xiyou.main.entity.newcontest.NewCtProfitChart;
import com.xiyou.main.service.contest.CtProfitChartService;
import com.xiyou.main.service.newcontest.NewCtProfitChartService;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author tangcan
 * @since 2019-07-22
 */
@Service
public class NewCtProfitChartServiceImpl extends ServiceImpl<NewCtProfitChartMapper, NewCtProfitChart> implements NewCtProfitChartService {
    @Override
    public List<NewCtProfitChart> list(NewCtProfitChart profitChart) {
        if (profitChart == null) {
            return new ArrayList<>();
        }
        QueryWrapper<NewCtProfitChart> wrapper = new QueryWrapper<>();
        wrapper.eq("student_id", profitChart.getStudentId())
                .eq("contest_id", profitChart.getContestId())
                .eq("bs_isxt", 1);
        if (profitChart.getPcDate() != null) {
            wrapper.eq("pc_date", profitChart.getPcDate());
        }
        return this.list(wrapper);
    }

    @Override
    public NewCtProfitChart getSys(NewCtProfitChart profitChart) {
        if (profitChart == null) {
            return null;
        }
        QueryWrapper<NewCtProfitChart> wrapper = new QueryWrapper<>();
        wrapper.eq("student_id", profitChart.getStudentId())
                .eq("contest_id", profitChart.getContestId())
                .eq("pc_date", profitChart.getPcDate())
                .eq("bs_isxt", 1);
        return this.getOne(wrapper);
    }


    @Override
    public NewCtProfitChart getTemp(Integer studentId, Integer contestId, Integer year) {
        QueryWrapper<NewCtProfitChart> wrapper = new QueryWrapper<>();
        wrapper.eq("student_id", studentId)
                .eq("contest_id", contestId)
                .eq("pc_date", year)
                .eq("bs_isxt", 0);
        return this.getOne(wrapper);
    }


    @Override
    public List<NewCtProfitChart> getCurrentYear(NewCtProfitChart profitChart) {
        if (profitChart == null) {
            return new ArrayList<>();
        }
        QueryWrapper<NewCtProfitChart> wrapper = new QueryWrapper<>();
        wrapper.eq("student_id", profitChart.getStudentId())
                .eq("contest_id", profitChart.getContestId())
                .eq("pc_date", profitChart.getPcDate());
        return this.list(wrapper);
    }

    @Override
    public NewCtProfitChart getOne(NewCtProfitChart ctProfitChart) {
        QueryWrapper<NewCtProfitChart> wrapper = new QueryWrapper<>();
        if (ctProfitChart.getStudentId() != null) {
            wrapper.eq("student_id", ctProfitChart.getStudentId());
        }
        if (ctProfitChart.getContestId() != null) {
            wrapper.eq("contest_id", ctProfitChart.getContestId());
        }
        if (ctProfitChart.getBsIsxt() != null) {
            wrapper.eq("bs_isxt", ctProfitChart.getBsIsxt());
        }
        if (ctProfitChart.getPcDate() != null) {
            wrapper.eq("pc_date", ctProfitChart.getPcDate());
        }
        return this.getOne(wrapper);
    }
}
