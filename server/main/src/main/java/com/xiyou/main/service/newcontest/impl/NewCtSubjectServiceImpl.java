package com.xiyou.main.service.newcontest.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.xiyou.main.dao.newcontest.NewCtSubjectMapper;
import com.xiyou.main.entity.newcontest.NewCtSubject;
import com.xiyou.main.params.contest.SubjectParam;
import com.xiyou.main.service.newcontest.NewCtSubjectService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author tangcan
 * @since 2019-08-25
 */
@Service
public class NewCtSubjectServiceImpl extends ServiceImpl<NewCtSubjectMapper, NewCtSubject> implements NewCtSubjectService {
    @Autowired
    private NewCtSubjectMapper newCtSubjectMapper;

    @Override
    public Page<NewCtSubject> listByAdmin(SubjectParam subjectParam) {
        Page<NewCtSubject> page = new Page<>(subjectParam.getPage(), subjectParam.getLimit());
        return newCtSubjectMapper.listByAdmin(page, subjectParam);
    }

    @Override
    public Page<NewCtSubject> listByTeacher(SubjectParam subjectParam) {
        Page<NewCtSubject> page = new Page<>(subjectParam.getPage(), subjectParam.getLimit());
        return newCtSubjectMapper.listByTeacher(page, subjectParam);
    }

    @Override
    public List<NewCtSubject> listByTeacher(Integer groupId) {
        QueryWrapper<NewCtSubject> wrapper = new QueryWrapper<>();
        wrapper.eq("group_id", groupId)
                .eq("upload", 1);
        return this.list(wrapper);
    }
}
