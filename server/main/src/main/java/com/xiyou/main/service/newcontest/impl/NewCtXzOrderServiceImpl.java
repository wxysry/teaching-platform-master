package com.xiyou.main.service.newcontest.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.xiyou.main.dao.newcontest.NewCtXzOrderMapper;
import com.xiyou.main.entity.newcontest.NewCtXzOrder;
import com.xiyou.main.service.newcontest.NewCtXzOrderService;
import com.xiyou.main.vo.newcontest.NewXzOrderVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author tangcan
 * @since 2019-07-22
 */
@Service
public class NewCtXzOrderServiceImpl extends ServiceImpl<NewCtXzOrderMapper, NewCtXzOrder> implements NewCtXzOrderService {
    @Autowired
    private NewCtXzOrderMapper ctXzOrderMapper;

    @Override
    public List<NewCtXzOrder> list(NewCtXzOrder order) {
        return ctXzOrderMapper.list(order);
    }

    @Override
    public Map<String, BigDecimal> getSalesAndDirectCost(Integer studentId, Integer contestId, int year) {
        return ctXzOrderMapper.getSalesAndDirectCost(studentId, contestId, year);
    }

    @Override
    public List<NewXzOrderVo> listDeliveryOrder(Map<String, Object> queryMap) {
        return ctXzOrderMapper.listDeliveryOrder(queryMap);
    }
}
