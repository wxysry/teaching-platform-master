package com.xiyou.main.service.newcontest.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.xiyou.main.dao.contest.CtYfMarketMapper;
import com.xiyou.main.dao.newcontest.NewCtYfMarketMapper;
import com.xiyou.main.entity.contest.CtYfMarket;
import com.xiyou.main.entity.newcontest.NewCtYfMarket;
import com.xiyou.main.service.contest.CtYfMarketService;
import com.xiyou.main.service.newcontest.NewCtYfMarketService;
import com.xiyou.main.vo.contest.MarketVo;
import com.xiyou.main.vo.newcontest.NewMarketVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author tangcan
 * @since 2019-07-22
 */
@Service
public class NewCtYfMarketServiceImpl extends ServiceImpl<NewCtYfMarketMapper, NewCtYfMarket> implements NewCtYfMarketService {
    @Autowired
    private NewCtYfMarketMapper ctYfMarketMapper;

    @Override
    public void update(NewCtYfMarket yfMarket) {
        ctYfMarketMapper.update(yfMarket);
    }

    @Override
    public List<String> getHadYfFinishMarkets(Integer studentId, Integer contestId) {
        return ctYfMarketMapper.getHadYfFinishMarkets(studentId, contestId);
    }

    @Override
    public List<NewMarketVo> listYfMarket(Integer studentId, Integer contestId) {
        return ctYfMarketMapper.listYfMarket(studentId, contestId);
    }
}
