package com.xiyou.main.service.newcontest.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.xiyou.main.dao.contest.CtYfProductMapper;
import com.xiyou.main.dao.newcontest.NewCtYfProductMapper;
import com.xiyou.main.entity.contest.CtYfProduct;
import com.xiyou.main.entity.newcontest.NewCtYfProduct;
import com.xiyou.main.service.contest.CtYfProductService;
import com.xiyou.main.service.newcontest.NewCtYfProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author tangcan
 * @since 2019-07-22
 */
@Service
public class NewCtYfProductServiceImpl extends ServiceImpl<NewCtYfProductMapper, NewCtYfProduct> implements NewCtYfProductService {
    @Autowired
    private NewCtYfProductMapper ctYfProductMapper;

    @Override
    public void update(NewCtYfProduct yfProduct) {
        ctYfProductMapper.update(yfProduct);
    }

    @Override
    public void updateEndingSeason(Integer userId, Integer contestId, Integer date) {
        ctYfProductMapper.updateEndingSeason(userId, contestId, date);
    }
}
