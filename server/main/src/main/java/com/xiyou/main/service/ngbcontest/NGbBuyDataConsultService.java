package com.xiyou.main.service.ngbcontest;

import com.xiyou.main.entity.ngbcontest.NGbBuyDataConsult;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author wangxingyu
 * @since 2024-04-12
 */
public interface NGbBuyDataConsultService extends IService<NGbBuyDataConsult> {

}
