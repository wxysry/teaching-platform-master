package com.xiyou.main.service.ngbcontest;

import com.baomidou.mybatisplus.extension.service.IService;
import com.xiyou.main.entity.ngbcontest.NGbContestGroup;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author tangcan
 * @since 2019-07-09
 */
@Service
public interface NGbContestGroupService extends IService<NGbContestGroup> {

    int insert(NGbContestGroup gbContestGroup);

    boolean checkNameExist(NGbContestGroup gbContestGroup);

    List<NGbContestGroup> getListByTeacher(Integer teacherId);
}
