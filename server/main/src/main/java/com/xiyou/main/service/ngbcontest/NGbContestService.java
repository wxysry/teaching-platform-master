package com.xiyou.main.service.ngbcontest;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.xiyou.main.entity.ngbcontest.NGbContest;
import com.xiyou.main.entity.ngbcontest.NGbContest;
import com.xiyou.main.params.contest.ContestParam;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author tangcan
 * @since 2019-08-25
 */
public interface NGbContestService extends IService<NGbContest> {

    Page<NGbContest> getPage(ContestParam contestParam);

    Page<NGbContest> getPageAdmin(ContestParam contestParam);

    int getUnEndContest(Integer teacherId);

    int publish(Integer teacherId, Integer contestId);

    Page<NGbContest> getStudentContestPage(ContestParam contestParam);

    boolean isSyn(Integer contestId);
}
