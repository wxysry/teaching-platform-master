package com.xiyou.main.service.ngbcontest;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.xiyou.main.entity.ngbcontest.NGbContestStudent;
import com.xiyou.main.params.contest.ContestStudentParam;
import com.xiyou.main.vo.contest.ContestScore;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author tangcan
 * @since 2019-08-25
 */
public interface NGbContestStudentService extends IService<NGbContestStudent> {

    int insertBatch(Integer contestId, List<Integer> studentIds);

    void remove(Integer contestId);

    NGbContestStudent get(Integer contestId, Integer studentId);

    Page<NGbContestStudent> getPage(ContestStudentParam contestStudentParam);

    List<Integer> getStudentIdList(Integer contestId);

    void updateScore(Integer id, Double score);

    List<ContestScore> getScoreList(Integer contestId);

    void updateErrorReportYear(NGbContestStudent gbContestStudent, Integer errorYear);

    void updateErrorReportYear(NGbContestStudent gbContestStudent);

    void insertStudentEndTime(Integer contestId,Integer studentId,Integer date);

    String getEndTime(Integer contestId,Integer studentId,Integer date);

    String getEndTimeByStr(String endTimeStr,Integer date);

    int getMaxDate(Integer contestId);

    Map<Integer,String> getStudentIdAndAccountMap(Integer contestId);

    /**
     * 获取最大的完成年份
     * @param contestId
     * @return
     */
    Integer getMaxFinishDate(Integer contestId);

    /**
     * 获取最大的广告投放年份
     * @param contestId
     * @return
     */
    Integer getMaxAdDate(Integer contestId);


    /**
     * 获取学生的经营进度
     */
    Integer getStudentDate(Integer contestId,Integer studentId);
}
