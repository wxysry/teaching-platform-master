package com.xiyou.main.service.ngbcontest;

import com.xiyou.main.entity.ngbcontest.NGbCtAvgSal;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author wangxingyu
 * @since 2024-05-17
 */
public interface NGbCtAvgSalService extends IService<NGbCtAvgSal> {

}
