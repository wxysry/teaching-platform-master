package com.xiyou.main.service.ngbcontest;

import com.baomidou.mybatisplus.extension.service.IService;
import com.xiyou.main.entity.ngbcontest.NGbCtBalance;
import com.xiyou.main.vo.ngbcontest.NGbCashFolowEntity;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author wangxingyu
 * @since 2023-06-11
 */
public interface NGbCtBalanceService extends IService<NGbCtBalance> {

    /**
     * P8&P9 申请长贷款&申请短贷-获取最大贷款额度和当前贷款总和
     *
     * @param cashFolowEntity
     * @return
     */
    Map<String,Integer> getAllMaxLoanAmount(NGbCashFolowEntity cashFolowEntity);

    /**
     * P8&P9 申请长贷款&申请短贷-获取最大贷款额度和当前贷款总和
     *
     * @param cashFolowEntity
     * @return
     */
    Integer getMaxLoanAmount(NGbCashFolowEntity cashFolowEntity);

    NGbCtBalance get(NGbCtBalance balance);

    NGbCtBalance getTemp(Integer studentId, Integer contestId, Integer year);

    /**
     * @Author: tangcan
     * @Description: 最近一个所得税>0的年份的权益
     * @Param: [studentId, contestId]
     * @date: 2019/7/24
     */
    Integer getRecentYEarHaveEquity(Integer studentId, Integer contestId, Integer nowYear);

    /**
     * @Author: tangcan
     * @Description: max(之前年份所有者权益 ）
     * @Param: [studentId, contestId, currentYear]
     * @date: 2019/7/24
     */
    int maxTotalEquityPreYear(Integer studentId, Integer contestId, int currentYear);

    List<NGbCtBalance> getCurrentYear(NGbCtBalance balance);

    NGbCtBalance getOne(NGbCtBalance ctBalance);
}
