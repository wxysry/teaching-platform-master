package com.xiyou.main.service.ngbcontest;

import com.baomidou.mybatisplus.extension.service.IService;
import com.xiyou.main.entity.ngbcontest.NGbCtBankLoan;
import com.xiyou.main.vo.ngbcontest.NGbCashFolowEntity;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author tangcan
 * @since 2019-07-22
 */
public interface NGbCtBankLoanService extends IService<NGbCtBankLoan> {

//    /**
//     * bank_loan表中贷款类型为2且归还时间=当前时间的金额
//     * @param cashFolowEntity
//     * @return
//     */
//    Integer getCurrentMoney(NGbCashFolowEntity cashFolowEntity);


    List<NGbCtBankLoan> list(Integer studentId, Integer contestId);
}
