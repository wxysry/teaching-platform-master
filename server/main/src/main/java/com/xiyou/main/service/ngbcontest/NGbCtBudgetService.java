package com.xiyou.main.service.ngbcontest;

import com.xiyou.main.entity.ngbcontest.NGbCtBudget;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author wangxingyu
 * @since 2024-02-18
 */
public interface NGbCtBudgetService extends IService<NGbCtBudget> {

    /**
     * 获取当前赛季的预算使用
     * @param contestId
     * @param userId
     * @param date
     * @param type
     * @return
     */
    Integer getSeasonUsed(Integer contestId, Integer userId, Integer date, String type);
}
