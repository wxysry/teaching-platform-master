package com.xiyou.main.service.ngbcontest;

import com.baomidou.mybatisplus.extension.service.IService;
import com.xiyou.main.entity.ngbcontest.NGbCtCashflow;
import com.xiyou.main.vo.ngbcontest.NGbCashFolowEntity;

import java.util.List;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author wangxingyu
 * @since 2023-06-11
 */
public interface NGbCtCashflowService extends IService<NGbCtCashflow> {


    /**
     * 用户当前现金
     *
     * @param studentId
     * @param contestId
     * @return
     */
    NGbCashFolowEntity getStudentFinanceInfor(Integer studentId, Integer contestId);

    /**
     * student上一条现金
     *
     * @param gbCashFolowEntity
     * @return
     */
    Integer getStudentLastCash(NGbCashFolowEntity gbCashFolowEntity);

    /**
     * 查出用户当前现金余额
     *
     * @param userId
     * @param contestId
     * @return
     */
    Integer getCash(Integer userId, Integer contestId);

    List<NGbCtCashflow> list(Integer studentId, Integer contestId, Integer year, List<String> actionList);

}
