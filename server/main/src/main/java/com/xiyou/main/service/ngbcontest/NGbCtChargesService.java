package com.xiyou.main.service.ngbcontest;

import com.baomidou.mybatisplus.extension.service.IService;
import com.xiyou.main.entity.ngbcontest.NGbCtCharges;

import java.util.List;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author tangcan
 * @since 2019-07-22
 */
public interface NGbCtChargesService extends IService<NGbCtCharges> {

    int addBySys(NGbCtCharges charges);

    int updateTotal(Integer chargesId);

    /**
     * @Author: tangcan
     * @Description: 获取系统填入的
     * @Param: [studentId, contestId, year]
     * @date: 2019/9/10
    */
    NGbCtCharges getSys(Integer studentId, Integer contestId, int year);


    NGbCtCharges getTemp(Integer studentId, Integer contestId, int year);

    List<NGbCtCharges> getCurrentYear(NGbCtCharges charges);

    Integer getTotal(Integer studentId, Integer contestId, int year, int isxt);

    NGbCtCharges getOne(NGbCtCharges ctCharges);
}
