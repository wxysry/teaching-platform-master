package com.xiyou.main.service.ngbcontest;

import com.xiyou.main.entity.ngbcontest.NGbCtDesignFeature;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author wangxingyu
 * @since 2024-03-26
 */
public interface NGbCtDesignFeatureService extends IService<NGbCtDesignFeature> {

}
