package com.xiyou.main.service.ngbcontest;

import com.baomidou.mybatisplus.extension.service.IService;
import com.xiyou.main.entity.ngbcontest.NGbCtFinancialTarget;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author wangxingyu
 * @since 2023-06-28
 */
public interface NGbCtFinancialTargetService extends IService<NGbCtFinancialTarget> {

    NGbCtFinancialTarget get(NGbCtFinancialTarget ctFinancialTarget);

    /**
     * 获取暂存数据
     * @return
     */
    NGbCtFinancialTarget getTemp(Integer studentId, Integer contestId, int year);
}
