package com.xiyou.main.service.ngbcontest;

import com.baomidou.mybatisplus.extension.service.IService;
import com.xiyou.main.entity.ngbcontest.NGbCtGzCs;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author wangxingyu
 * @since 2023-06-06
 */
public interface NGbCtGzCsService extends IService<NGbCtGzCs> {
    NGbCtGzCs getByContestId(Integer contestId);

    NGbCtGzCs getBySubjectNumber(Integer subjectNumber);
}
