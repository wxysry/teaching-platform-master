package com.xiyou.main.service.ngbcontest;

import com.baomidou.mybatisplus.extension.service.IService;
import com.xiyou.main.entity.ngbcontest.NGbCtGzLoan;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author wangxingyu
 * @since 2023-06-06
 */
public interface NGbCtGzLoanService extends IService<NGbCtGzLoan> {
    List<NGbCtGzLoan> listCtGzLoan(Integer newContestId);
}
