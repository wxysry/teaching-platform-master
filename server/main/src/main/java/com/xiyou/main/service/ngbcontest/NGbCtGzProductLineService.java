package com.xiyou.main.service.ngbcontest;

import com.baomidou.mybatisplus.extension.service.IService;
import com.xiyou.main.entity.ngbcontest.NGbCtGzProductLine;
import com.xiyou.main.vo.ngbcontest.NGbCashFolowEntity;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author wangxingyu
 * @since 2023-06-06
 */
public interface NGbCtGzProductLineService extends IService<NGbCtGzProductLine> {
    /**
     * 选择生产线类型
     * @param contestId
     * @return
     */
    List<NGbCtGzProductLine> getProductLineTypeList(Integer contestId);

    /**
     * 该生产线的投资时间
     * @param cashFolowEntity
     * @return
     */
    Integer getProductLineInfo(NGbCashFolowEntity cashFolowEntity);

    List<NGbCtGzProductLine> getListByCplids(List<Integer> cplids);

}
