package com.xiyou.main.service.ngbcontest;

import com.baomidou.mybatisplus.extension.service.IService;
import com.xiyou.main.entity.ngbcontest.NGbCtGzProduct;
import com.xiyou.main.vo.ngbcontest.NGbYfProductVO;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author wangxingyu
 * @since 2023-06-06
 */
public interface NGbCtGzProductService extends IService<NGbCtGzProduct> {
    /**
     * 查出产品研发列表
     * @param userId
     * @param contestId
     * @return
     */
    List<NGbYfProductVO> listYfProduct(Integer userId, Integer contestId);

    /**
     * 产品名称
     * @param contestId
     * @return
     */
    List<String> getProductCPName(Integer contestId);


    List<NGbCtGzProduct> getList(Integer contestId);

    List<NGbCtGzProduct> getListByIds(List<Integer> cpIds);

}
