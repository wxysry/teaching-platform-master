package com.xiyou.main.service.ngbcontest;

import com.baomidou.mybatisplus.extension.service.IService;
import com.xiyou.main.entity.ngbcontest.NGbCtKcMaterial;
import com.xiyou.main.vo.ngbcontest.NGbCashFolowEntity;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author tangcan
 * @since 2019-07-22
 */
public interface NGbCtKcMaterialService extends IService<NGbCtKcMaterial> {


    int getMaterialSum(Integer studentId, Integer contestId);

}
