package com.xiyou.main.service.ngbcontest;

import com.baomidou.mybatisplus.extension.service.IService;
import com.xiyou.main.entity.ngbcontest.NGbCtKcProduct;
import com.xiyou.main.vo.ngbcontest.NGbProductVo;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author tangcan
 * @since 2019-07-22
 */
public interface NGbCtKcProductService extends IService<NGbCtKcProduct> {

    int getProductSum(Integer studentId, Integer contestId);

//    List<NGbProductVo> listSellKc(Integer contestId, Integer userId);
}
