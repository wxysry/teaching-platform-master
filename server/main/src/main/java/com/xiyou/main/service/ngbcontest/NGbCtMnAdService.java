package com.xiyou.main.service.ngbcontest;

import com.baomidou.mybatisplus.extension.service.IService;
import com.xiyou.main.entity.ngbcontest.NGbCtMnAd;

import java.util.List;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author tangcan
 * @since 2019-07-25
 */
public interface NGbCtMnAdService extends IService<NGbCtMnAd> {

    List<NGbCtMnAd> getAdList(Integer contestId, Integer studentId, Integer year,Integer quarterly);

    List<NGbCtMnAd> getAdListByGroupNum(Integer contestId, Integer studentId, String name);


    /**
     * 获取所有学生本市场的广告投放排名
     * @param contestId
     * @param year
     * @param quarterly
     * @param cmId
     * @return
     */
    List<NGbCtMnAd> getAdRank(Integer contestId, Integer year, Integer quarterly , Integer cmId,String cmName);
}
