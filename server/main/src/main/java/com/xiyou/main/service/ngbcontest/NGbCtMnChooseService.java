package com.xiyou.main.service.ngbcontest;

import com.baomidou.mybatisplus.extension.service.IService;
import com.xiyou.main.entity.ngbcontest.NGbCtMnChoose;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author tangcan
 * @since 2019-08-27
 */
public interface NGbCtMnChooseService extends IService<NGbCtMnChoose> {

}
