package com.xiyou.main.service.ngbcontest;

import com.xiyou.main.entity.ngbcontest.NGbCtPdca;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author wangxingyu
 * @since 2024-04-10
 */
public interface NGbCtPdcaService extends IService<NGbCtPdca> {

}
