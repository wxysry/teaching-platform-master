package com.xiyou.main.service.ngbcontest;

import com.baomidou.mybatisplus.extension.service.IService;
import com.xiyou.main.entity.ngbcontest.NGbCtProfitChart;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author tangcan
 * @since 2019-07-22
 */
public interface NGbCtProfitChartService extends IService<NGbCtProfitChart> {

    List<NGbCtProfitChart> list(NGbCtProfitChart profitChart);

    NGbCtProfitChart getSys(NGbCtProfitChart profitChart);


    NGbCtProfitChart getTemp(Integer studentId, Integer contestId, Integer year);


    List<NGbCtProfitChart> getCurrentYear(NGbCtProfitChart profitChart);

    NGbCtProfitChart getOne(NGbCtProfitChart ctProfitChart);
}
