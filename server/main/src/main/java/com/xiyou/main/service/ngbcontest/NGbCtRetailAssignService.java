package com.xiyou.main.service.ngbcontest;

import com.xiyou.main.entity.ngbcontest.NGbCtRetailAssign;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author wangxingyu
 * @since 2024-05-25
 */
public interface NGbCtRetailAssignService extends IService<NGbCtRetailAssign> {

}
