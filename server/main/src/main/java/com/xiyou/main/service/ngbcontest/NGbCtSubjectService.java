package com.xiyou.main.service.ngbcontest;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.xiyou.main.entity.ngbcontest.NGbCtSubject;
import com.xiyou.main.params.contest.SubjectParam;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author tangcan
 * @since 2019-08-25
 */
public interface NGbCtSubjectService extends IService<NGbCtSubject> {

    Page<NGbCtSubject> listByAdmin(SubjectParam subjectParam);

    Page<NGbCtSubject> listByTeacher(SubjectParam subjectParam);

    List<NGbCtSubject> listByTeacher(Integer teacherId,Integer groupId);

}
