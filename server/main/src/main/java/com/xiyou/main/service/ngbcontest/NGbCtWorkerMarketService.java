package com.xiyou.main.service.ngbcontest;

import com.xiyou.main.entity.ngbcontest.NGbCtWorkerMarket;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author wangxingyu
 * @since 2024-03-21
 */
public interface NGbCtWorkerMarketService extends IService<NGbCtWorkerMarket> {

}
