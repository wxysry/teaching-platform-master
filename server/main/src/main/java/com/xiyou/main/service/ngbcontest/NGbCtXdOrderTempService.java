package com.xiyou.main.service.ngbcontest;

import com.baomidou.mybatisplus.extension.service.IService;
import com.xiyou.main.entity.ngbcontest.NGbCtXdOrderTemp;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author wangxingyu
 * @since 2023-09-14
 */
public interface NGbCtXdOrderTempService extends IService<NGbCtXdOrderTemp> {

}
