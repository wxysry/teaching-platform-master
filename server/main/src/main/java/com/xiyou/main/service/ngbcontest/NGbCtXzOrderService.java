package com.xiyou.main.service.ngbcontest;

import com.baomidou.mybatisplus.extension.service.IService;
import com.xiyou.main.entity.ngbcontest.NGbCtXzOrder;
import com.xiyou.main.vo.ngbcontest.NGbXzOrderVo;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author tangcan
 * @since 2019-07-22
 */
public interface NGbCtXzOrderService extends IService<NGbCtXzOrder> {

    List<NGbCtXzOrder> list(NGbCtXzOrder order);

    Map<String, BigDecimal> getSalesAndDirectCost(Integer studentId, Integer contestId, int year);

    /**
     * 查出交货订单
     * @param queryMap
     * @return
     */
    List<NGbXzOrderVo> listDeliveryOrder(Map<String, Object> queryMap);
}
