package com.xiyou.main.service.ngbcontest;

import com.baomidou.mybatisplus.extension.service.IService;
import com.xiyou.main.entity.ngbcontest.NGbCtYfMarket;
import com.xiyou.main.vo.ngbcontest.NGbMarketVo;

import java.util.List;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author tangcan
 * @since 2019-07-22
 */
public interface NGbCtYfMarketService extends IService<NGbCtYfMarket> {

    void update(NGbCtYfMarket yfMarket);

    /**
     * @Author: tangcan
     * @Description: 获取已经开发结束的市场名称
     * @Param: [studentId, contestId]
     * @date: 2019/7/25
     */
    List<String> getHadYfFinishMarkets(Integer studentId, Integer contestId);

    List<NGbMarketVo> listYfMarket(Integer studentId, Integer contestId);
}
