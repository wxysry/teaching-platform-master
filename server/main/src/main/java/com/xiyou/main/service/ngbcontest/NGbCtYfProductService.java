package com.xiyou.main.service.ngbcontest;

import com.baomidou.mybatisplus.extension.service.IService;
import com.xiyou.main.entity.ngbcontest.NGbCtYfProduct;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author tangcan
 * @since 2019-07-22
 */
public interface NGbCtYfProductService extends IService<NGbCtYfProduct> {

    void update(NGbCtYfProduct yfProduct);

    /**
     * 当季结束更新产品研发
     * @param userId
     * @param contestId
     */
    void updateEndingSeason(Integer userId, Integer contestId, Integer date);
}
