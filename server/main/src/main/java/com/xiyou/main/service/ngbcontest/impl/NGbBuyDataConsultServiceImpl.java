package com.xiyou.main.service.ngbcontest.impl;

import com.xiyou.main.entity.ngbcontest.NGbBuyDataConsult;
import com.xiyou.main.dao.ngbcontest.NGbBuyDataConsultMapper;
import com.xiyou.main.service.ngbcontest.NGbBuyDataConsultService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author wangxingyu
 * @since 2024-04-12
 */
@Service
public class NGbBuyDataConsultServiceImpl extends ServiceImpl<NGbBuyDataConsultMapper, NGbBuyDataConsult> implements NGbBuyDataConsultService {

}
