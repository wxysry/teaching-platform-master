package com.xiyou.main.service.ngbcontest.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.xiyou.main.dao.ngbcontest.NGbContestGroupMapper;
import com.xiyou.main.entity.exam.SysUser;
import com.xiyou.main.entity.ngbcontest.NGbContestGroup;
import com.xiyou.main.service.exam.SysUserService;
import com.xiyou.main.service.ngbcontest.NGbContestGroupService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author tangcan
 * @since 2019-07-09
 */
@Service
public class NGbContestGroupServiceImpl extends ServiceImpl<NGbContestGroupMapper, NGbContestGroup> implements NGbContestGroupService {
    @Autowired
    private NGbContestGroupMapper ngbContestGroupMapper;

    @Override
    public int insert(NGbContestGroup ngbContestGroup) {
        return ngbContestGroupMapper.insert(ngbContestGroup);
    }

    @Override
    public boolean checkNameExist(NGbContestGroup contestGroup) {
        QueryWrapper<NGbContestGroup> wrapper = new QueryWrapper<>();
        wrapper.ne("id", contestGroup.getId())
                .eq("group_name", contestGroup.getGroupName());
        return this.getOne(wrapper) != null;
    }

    @Autowired
    SysUserService sysUserService;

    @Override
    public List<NGbContestGroup> getListByTeacher(Integer teacherId) {
        List<NGbContestGroup> gbContestGroups = new ArrayList<>();

        SysUser sysUser = sysUserService.get(new SysUser().setId(teacherId));
        //判断当前用户的身份是老师还是管理员
        if(sysUser.getRoleId()==1){
            gbContestGroups = ngbContestGroupMapper.getListByAdmin();
        }else{
            gbContestGroups = ngbContestGroupMapper.getListByTeacher(teacherId);

        }
        return gbContestGroups;
    }
}
