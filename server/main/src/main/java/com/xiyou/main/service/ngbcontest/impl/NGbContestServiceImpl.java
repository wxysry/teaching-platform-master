package com.xiyou.main.service.ngbcontest.impl;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.xiyou.main.biz.ngbcontest.NGbActionBiz;
import com.xiyou.main.biz.ngbcontest.NGbBusinessBiz;
import com.xiyou.main.biz.ngbcontest.NGbContestBiz;
import com.xiyou.main.dao.ngbcontest.NGbContestMapper;
import com.xiyou.main.dao.ngbcontest.NGbCtGzOrderMapper;
import com.xiyou.main.dao.ngbcontest.NGbCtMnChooseMapper;
import com.xiyou.main.entity.ngbcontest.NGbContest;
import com.xiyou.main.entity.ngbcontest.NGbCtGzWorkerRecruit;
import com.xiyou.main.entity.ngbcontest.NGbCtMnChoose;
import com.xiyou.main.entity.ngbcontest.NGbCtWorkerMarket;
import com.xiyou.main.params.contest.ContestParam;
import com.xiyou.main.service.exam.TeachClassService;
import com.xiyou.main.service.ngbcontest.NGbContestService;
import com.xiyou.main.service.ngbcontest.NGbCtGzWorkerRecruitService;
import com.xiyou.main.service.ngbcontest.NGbCtWorkerMarketService;
import com.xiyou.main.utils.ChineseNameGenerator;
import io.swagger.models.auth.In;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author tangcan
 * @since 2019-08-25
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class NGbContestServiceImpl extends ServiceImpl<NGbContestMapper, NGbContest> implements NGbContestService {
    @Autowired
    private NGbContestMapper nGbContestMapper;
    @Autowired
    private TeachClassService teachClassService;
    @Autowired
    private NGbCtMnChooseMapper nGbCtMnChooseMapper;
    @Autowired
    private NGbCtGzOrderMapper nGbCtGzOrderMapper;
    @Autowired
    private NGbActionBiz gbActionBiz;
    @Autowired
    private NGbContestBiz gbContestBiz;

    @Autowired
    private NGbCtWorkerMarketService nGbCtWorkerMarketService;

    @Autowired
    private NGbCtGzWorkerRecruitService nGbCtGzWorkerRecruitService;

    @Override
    public Page<NGbContest> getPage(ContestParam contestParam) {
        Page<NGbContest> page = new Page<>(contestParam.getPage(), contestParam.getLimit());
        return nGbContestMapper.getPage(page, contestParam);
    }

    @Override
    public Page<NGbContest> getPageAdmin(ContestParam contestParam) {
        Page<NGbContest> page = new Page<>(contestParam.getPage(), contestParam.getLimit());
        return nGbContestMapper.getPageAdmin(page, contestParam);
    }

    @Override
    public int getUnEndContest(Integer teacherId) {
        return nGbContestMapper.getUnEndContest(teacherId);
    }


    @Autowired
    NGbBusinessBiz gbBusinessBiz;

    @Override
    public int publish(Integer teacherId, Integer contestId) {

        NGbContest nGbContest = nGbContestMapper.selectById(contestId);
        Integer subjectNumber = nGbContest.getSubjectNumber();

        List<Integer> studentIdList = teachClassService.getStudentIdList(teacherId);

//        // 选单mn_choose新增数据=市场定单gz_order为该场考试的
//        List<NGbCtMnChoose> ctMnChooseList = nGbCtGzOrderMapper.getList(contestId);
//        if (ctMnChooseList != null) {
//            for (NGbCtMnChoose ctMnChoose : ctMnChooseList) {
//                //选中轮次和选走组号都为空
//                ctMnChoose.setXzGroup(null);
//                ctMnChoose.setXzRound(null);
//                ctMnChoose.setContestId(contestId);
//            }
//            if (ctMnChooseList.size() > 0) {
//                nGbCtMnChooseMapper.insertBatch(ctMnChooseList);
//            }
//        }


        //先获取规则
        List<NGbCtGzWorkerRecruit> nGbCtGzWorkerRecruitList = nGbCtGzWorkerRecruitService.list(new LambdaQueryWrapper<NGbCtGzWorkerRecruit>()
                .eq(NGbCtGzWorkerRecruit::getSubjectNum,subjectNumber)
        );

        //生产1-6年的人才市场数据
        for(int year =1;year<=6;year++){
            for(int season=1;season<=4;season++){
                List<NGbCtWorkerMarket> workerList = new ArrayList<>();
                for (NGbCtGzWorkerRecruit item : nGbCtGzWorkerRecruitList) {
                    //先判断每个季度的工人数量
                    Integer qtyPerQtr = item.getQtyPerQtr();
                    //假设有50个人，就生成50个员工
                    for(int i=1;i<=qtyPerQtr;i++){
                        NGbCtWorkerMarket workerMarket = new NGbCtWorkerMarket();
                        workerMarket.setContestId(contestId);
                        //生成一个随机的中文名
                        workerMarket.setWorkerName(ChineseNameGenerator.getName());
                        workerMarket.setDate(year*10+season);
                        //获取等级名称
                        workerMarket.setRecruitName(item.getRecruitName());
                        //获取等级编码
                        workerMarket.setRecruitNum(item.getRecruitNum());
                        //期望基本薪资  80%-120%的随机数
                        workerMarket.setInitSal(generateRandomSalary(item.getInitSalExpect()));
                        //倍数加成  80%-120%的随机数
                        workerMarket.setMultBonus(generateRandomSalary(item.getMultBonus()));
                        //计件工资
                        workerMarket.setPiece(item.getPiece());
                        workerList.add(workerMarket);
                    }
                }
                if(workerList.size()>0){
                    nGbCtWorkerMarketService.saveBatch(workerList);
                }
            }
        }


        //每个学生生成一份数据
        for (Integer studentId : studentIdList) {
            gbBusinessBiz.start(studentId,contestId);
        }

        NGbContest gbContest  = new NGbContest();
        gbContest.setIsPublish(1);
        gbContest.setContestId(contestId);
        //剩余时间
        int seasonTime =gbContestBiz.getSeasonTime(contestId, 11);
        gbContest.setRemainTime(seasonTime);
        return nGbContestMapper.updateById(gbContest);
    }



    public  int generateRandomSalary(int initSalExpect) {
        Random random = new Random();
        // 计算薪资范围
        int minSalary = (int) (initSalExpect * 0.8);
        int maxSalary = (int) (initSalExpect * 1.2);

        // 确保minSalary和maxSalary之间的差值大于0，避免在除以0时出错
        if (maxSalary - minSalary <= 0) {
            return minSalary; // 或者你可以选择抛出异常或进行其他处理
        }

        // 生成一个随机整数
        int randomSalary = minSalary + random.nextInt(maxSalary - minSalary + 1);
        return randomSalary;
    }



    @Override
    public Page<NGbContest> getStudentContestPage(ContestParam contestParam) {
        Page<NGbContest> page = new Page<>(contestParam.getPage(), contestParam.getLimit());
        return nGbContestMapper.getStudentContestPage(page, contestParam);
    }

    @Override
    public boolean isSyn(Integer contestId) {
        NGbContest contest = nGbContestMapper.getObjByContestId(contestId);
        return contest.getIsSyn() == 1;
    }


}
