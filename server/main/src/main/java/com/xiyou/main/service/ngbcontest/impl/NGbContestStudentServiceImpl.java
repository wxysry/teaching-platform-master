package com.xiyou.main.service.ngbcontest.impl;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.xiyou.main.dao.ngbcontest.NGbContestStudentMapper;
import com.xiyou.main.dao.ngbcontest.NGbCtBalanceMapper;
import com.xiyou.main.dao.ngbcontest.NGbCtMnAdMapper;
import com.xiyou.main.entity.ngbcontest.NGbContestStudent;
import com.xiyou.main.entity.ngbcontest.NGbCtBalance;
import com.xiyou.main.entity.ngbcontest.NGbCtMnAd;
import com.xiyou.main.params.contest.ContestStudentParam;
import com.xiyou.main.service.ngbcontest.NGbContestStudentService;
import com.xiyou.main.vo.contest.ContestScore;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author tangcan
 * @since 2019-08-25
 */
@Service
public class NGbContestStudentServiceImpl extends ServiceImpl<NGbContestStudentMapper, NGbContestStudent> implements NGbContestStudentService {

    @Autowired
    private NGbContestStudentMapper gbContestStudentMapper;

    @Autowired
    private NGbCtBalanceMapper gbCtBalanceMapper;


    @Autowired
    private NGbCtMnAdMapper gbCtMnAdMapper;

    @Override
    public int insertBatch(Integer contestId, List<Integer> studentIds) {
        if (studentIds == null || studentIds.size() == 0) {
            return 0;
        }
        return gbContestStudentMapper.insertBatch(contestId, studentIds);
    }

    @Override
    public void remove(Integer contestId) {
        QueryWrapper<NGbContestStudent> wrapper = new QueryWrapper<>();
        wrapper.eq("contest_id", contestId);
        this.remove(wrapper);
    }

    @Override
    public NGbContestStudent get(Integer contestId, Integer studentId) {
        QueryWrapper<NGbContestStudent> wrapper = new QueryWrapper<>();
        wrapper.eq("contest_id", contestId).eq("student_id", studentId);
        return this.getOne(wrapper);
    }


    /**
     * 获取学生的经营进度
     *
     * @param contestId
     * @param studentId
     */
    @Override
    public Integer getStudentDate(Integer contestId, Integer studentId) {
        QueryWrapper<NGbContestStudent> wrapper = new QueryWrapper<>();
        wrapper.eq("contest_id", contestId).eq("student_id", studentId);
        NGbContestStudent gbContestStudent  = this.getOne(wrapper);
        return gbContestStudent.getDate();
    }



    @Override
    public Page<NGbContestStudent> getPage(ContestStudentParam contestStudentParam) {
        Page<NGbContestStudent> page = new Page<>(contestStudentParam.getPage(), contestStudentParam.getLimit());
        return gbContestStudentMapper.getPage(page, contestStudentParam);
    }

    @Override
    public List<Integer> getStudentIdList(Integer contestId) {
        return gbContestStudentMapper.getStudentIdList(contestId);
    }

    @Override
    public void updateScore(Integer id, Double score) {
        gbContestStudentMapper.updateScore(id, score);
    }

    @Override
    public List<ContestScore> getScoreList(Integer contestId) {
        return gbContestStudentMapper.getScoreList(contestId);
    }

    @Override
    public void updateErrorReportYear(NGbContestStudent gbContestStudent, Integer errorYear) {
        if (gbContestStudent != null && errorYear != null) {
            String errorReportYear = gbContestStudent.getErrorReportYear() == null ? "" : gbContestStudent.getErrorReportYear();
            if (!errorReportYear.contains(String.valueOf(errorYear))) {
                // 不重复统计错误年份
                if (errorReportYear.length() > 0) {
                    errorReportYear += "," + errorYear;
                } else {
                    errorReportYear += errorYear;
                }
                gbContestStudent.setErrorReportYear(errorReportYear);
                this.updateErrorReportYear(gbContestStudent);
            }
        }
    }

    @Override
    public void updateErrorReportYear(NGbContestStudent gbContestStudent) {
        gbContestStudentMapper.updateErrorReportYear(gbContestStudent);
    }



    @Override
    public void insertStudentEndTime(Integer contestId,Integer studentId,Integer date){
        NGbContestStudent gbContestStudent = gbContestStudentMapper.get(contestId, studentId);
        String endTimeStr = gbContestStudent.getEverySeasonEndTime();
        JSONArray jsonArray ;
        if(endTimeStr != null){
            jsonArray = JSONArray.parseArray(endTimeStr);
        }else {
            jsonArray = new JSONArray();
        }
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("date",date);
        jsonObject.put("time", LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")));
        jsonArray.add(jsonObject);
        gbContestStudent.setEverySeasonEndTime(jsonArray.toJSONString());
        gbContestStudentMapper.updateById(gbContestStudent);
    }


    @Override
    public String getEndTime(Integer contestId, Integer studentId, Integer date){
        NGbContestStudent gbContestStudent = gbContestStudentMapper.get(contestId, studentId);
        String endTimeStr = gbContestStudent.getEverySeasonEndTime();
        return getEndTimeByStr(endTimeStr,date);
    }


    @Override
    public String getEndTimeByStr(String endTimeStr, Integer date){
        JSONArray jsonArray ;
        if(endTimeStr != null){
            jsonArray = JSONArray.parseArray(endTimeStr);
            for (Object obj : jsonArray) {
                JSONObject jsObj = (JSONObject) obj;
                Integer endDate = Integer.parseInt(jsObj.get("date").toString());
                if(endDate.equals(date)){
                    return jsObj.get("time").toString();
                }
            }
        }
        return null;
    }

    @Override
    public int getMaxDate(Integer contestId) {
        QueryWrapper<NGbContestStudent> qw = new QueryWrapper<>();
        qw.eq("contest_id",contestId);
        List<NGbContestStudent> list = this.list(qw);
        int maxDate = 11;
        for (NGbContestStudent gbContestStudent : list) {
            int date = gbContestStudent.getDate() / 10;
            if(date > maxDate){
                maxDate = date;
            }
        }
        return maxDate;
    }

    /**
     * 获取最大的完成年份
     *
     * @param contestId
     * @return
     */
    @Override
    public Integer getMaxFinishDate(Integer contestId) {
        NGbCtBalance gbCtBalance = gbCtBalanceMapper.selectOne(new LambdaQueryWrapper<NGbCtBalance>()
                .eq(NGbCtBalance::getContestId,contestId)
                .eq(NGbCtBalance::getIsSubmit,"Y")
                .eq(NGbCtBalance::getBsIsxt,0)
                .orderByDesc(NGbCtBalance::getBsYear)
                .last("limit 1")
        );
        //如果全部都没有完成就默认为1
        if(gbCtBalance!=null){
            return gbCtBalance.getBsYear();
        }else{
            return 1;
        }
    }

    @Override

    public Integer getMaxAdDate(Integer contestId) {
        NGbCtMnAd gbCtMnAd= gbCtMnAdMapper.selectOne(new LambdaQueryWrapper<NGbCtMnAd>()
                .eq(NGbCtMnAd::getContestId,contestId)
                .eq(NGbCtMnAd::getIsSubmit,"Y")
                .orderByDesc(NGbCtMnAd::getYear)
                .orderByDesc(NGbCtMnAd::getQuarterly)
                .last("limit 1")
        );
        if(gbCtMnAd!=null){
            return gbCtMnAd.getYear()*10 + gbCtMnAd.getQuarterly();
        }else{
            return 11;
        }
    }




    @Override
    public Map<Integer, String> getStudentIdAndAccountMap(Integer contestId) {
        //学生和账号的map
        List<NGbContestStudent> studentList = gbContestStudentMapper.getStudentIdAndAccountList(contestId);
        Map<Integer,String> studentMap = new HashMap<>();
        for (NGbContestStudent gbContestStudent : studentList) {
            studentMap.put(gbContestStudent.getStudentId(),gbContestStudent.getAccount());
        }
        return studentMap;
    }


}
