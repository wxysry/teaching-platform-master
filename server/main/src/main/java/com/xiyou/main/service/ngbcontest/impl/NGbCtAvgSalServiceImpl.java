package com.xiyou.main.service.ngbcontest.impl;

import com.xiyou.main.entity.ngbcontest.NGbCtAvgSal;
import com.xiyou.main.dao.ngbcontest.NGbCtAvgSalMapper;
import com.xiyou.main.service.ngbcontest.NGbCtAvgSalService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author wangxingyu
 * @since 2024-05-17
 */
@Service
public class NGbCtAvgSalServiceImpl extends ServiceImpl<NGbCtAvgSalMapper, NGbCtAvgSal> implements NGbCtAvgSalService {

}
