package com.xiyou.main.service.ngbcontest.impl;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.xiyou.main.dao.ngbcontest.NGbCtBalanceMapper;
import com.xiyou.main.dao.ngbcontest.NGbCtBankLoanMapper;
import com.xiyou.main.dao.ngbcontest.NGbCtGzCsMapper;
import com.xiyou.main.entity.ngbcontest.NGbCtBalance;
import com.xiyou.main.service.ngbcontest.NGbCtBalanceService;
import com.xiyou.main.vo.ngbcontest.NGbCashFolowEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author wangxingyu
 * @since 2023-06-11
 */
@Service
public class NGbCtBalanceServiceImpl extends ServiceImpl<NGbCtBalanceMapper, NGbCtBalance> implements NGbCtBalanceService {
    @Autowired
    private NGbCtBalanceMapper gbCtBalanceMapper;
    @Autowired
    NGbCtGzCsMapper gbCtGzCsMapper;
    @Autowired
    NGbCtBankLoanMapper gbCtBankLoanMapper;

    @Override
    public NGbCtBalance get(NGbCtBalance balance) {
        QueryWrapper<NGbCtBalance> wrapper = new QueryWrapper<>();
        if (balance.getStudentId() != null) {
            wrapper.eq("student_id", balance.getStudentId());
        }
        if (balance.getContestId() != null) {
            wrapper.eq("contest_id", balance.getContestId());
        }
        if (balance.getBsYear() != null) {
            wrapper.eq("bs_year", balance.getBsYear());
        }
        wrapper.eq("bs_isxt", 1);
        return this.getOne(wrapper);
    }


    @Override
    public NGbCtBalance getTemp(Integer studentId, Integer contestId, Integer year) {
        QueryWrapper<NGbCtBalance> wrapper = new QueryWrapper<>();
        wrapper.eq("student_id", studentId);
        wrapper.eq("contest_id", contestId);
        wrapper.eq("bs_year", year);
        wrapper.eq("bs_isxt", 0);
        return this.getOne(wrapper);
    }


    @Override
    public Integer getRecentYEarHaveEquity(Integer studentId, Integer contestId, Integer nowYear) {
        return gbCtBalanceMapper.getRecentYEarHaveEquity(studentId, contestId, nowYear);
    }

    @Override
    public int maxTotalEquityPreYear(Integer studentId, Integer contestId, int currentYear) {
        return gbCtBalanceMapper.maxTotalEquityPreYear(studentId, contestId, currentYear);
    }


    @Override
    public Map<String,Integer> getAllMaxLoanAmount(NGbCashFolowEntity cashFolowEntity) {
        Integer userRights = gbCtBalanceMapper.getBalanceMaxLoanLimit(cashFolowEntity);
        NGbCashFolowEntity entity = new NGbCashFolowEntity()
                .setStudentId(cashFolowEntity.getStudentId())
                .setContestId(cashFolowEntity.getContestId())
                .setCurrentTime(cashFolowEntity.getCurrentTime())
                .setCurrentYear(cashFolowEntity.getCurrentYear());
        // gz_cs中Loan_Ceiling
        Integer loadCeiling = gbCtGzCsMapper.getCSLoanCeiling(entity);
        Integer loanType1 = gbCtBankLoanMapper.getBackLoanAmount(entity);
        HashMap<String, Integer> map = new HashMap<>();
        // 最大贷款额度
        map.put("maxLoan",Math.max(0,userRights * loadCeiling));
        //目前所有贷款金额总和
        map.put("currLoan",Math.max(0,loanType1));
        return map;
    }

    @Override
    public Integer getMaxLoanAmount(NGbCashFolowEntity cashFolowEntity) {
        Integer userRights = gbCtBalanceMapper.getBalanceMaxLoanLimit(cashFolowEntity);
        NGbCashFolowEntity entity = new NGbCashFolowEntity()
                .setStudentId(cashFolowEntity.getStudentId())
                .setContestId(cashFolowEntity.getContestId())
                .setCurrentTime(cashFolowEntity.getCurrentTime())
                .setCurrentYear(cashFolowEntity.getCurrentYear());
        // gz_cs中Loan_Ceiling
        Integer loadCeiling = gbCtGzCsMapper.getCSLoanCeiling(entity);

        entity.setBlType(1);
        Integer loanType1 = gbCtBankLoanMapper.getBackLoanAmount(entity);
        // 最大贷款额度
        return Math.max(0, userRights * loadCeiling - loanType1);
    }

    @Override
    public List<NGbCtBalance> getCurrentYear(NGbCtBalance balance) {
        if (balance == null) {
            return new ArrayList<>();
        }
        QueryWrapper<NGbCtBalance> wrapper = new QueryWrapper<>();
        wrapper.eq("student_id", balance.getStudentId())
                .eq("contest_id", balance.getContestId())
                .eq("bs_year", balance.getBsYear());
        return this.list(wrapper);
    }

    @Override
    public NGbCtBalance getOne(NGbCtBalance gbCtBalance) {
        QueryWrapper<NGbCtBalance> wrapper = new QueryWrapper<>();
        if (gbCtBalance.getStudentId() != null) {
            wrapper.eq("student_id", gbCtBalance.getStudentId());
        }
        if (gbCtBalance.getContestId() != null) {
            wrapper.eq("contest_id", gbCtBalance.getContestId());
        }
        if (gbCtBalance.getBsIsxt() != null) {
            wrapper.eq("bs_isxt", gbCtBalance.getBsIsxt());
        }
        if (gbCtBalance.getBsYear() != null) {
            wrapper.eq("bs_year", gbCtBalance.getBsYear());
        }
        return this.getOne(wrapper);
    }
}
