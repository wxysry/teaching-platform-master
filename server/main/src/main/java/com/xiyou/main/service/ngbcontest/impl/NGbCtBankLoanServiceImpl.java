package com.xiyou.main.service.ngbcontest.impl;


import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.xiyou.main.dao.ngbcontest.NGbCtBankLoanMapper;
import com.xiyou.main.dao.ngbcontest.NGbCtGzCsMapper;
import com.xiyou.main.entity.ngbcontest.NGbCtBankLoan;
import com.xiyou.main.service.ngbcontest.NGbCtBankLoanService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author tangcan
 * @since 2019-07-22
 */
@Service
public class NGbCtBankLoanServiceImpl extends ServiceImpl<NGbCtBankLoanMapper, NGbCtBankLoan> implements NGbCtBankLoanService {
    @Autowired
    NGbCtBankLoanMapper gbCtBankLoanMapper;
    @Autowired
    NGbCtGzCsMapper gbCtGzCsMapper;

    @Override
    public List<NGbCtBankLoan> list(Integer studentId, Integer contestId) {
        LambdaQueryWrapper<NGbCtBankLoan> wrapper = new LambdaQueryWrapper<NGbCtBankLoan>();
        wrapper.eq(NGbCtBankLoan::getContestId,contestId)
        .eq(NGbCtBankLoan::getStudentId,studentId)
        .eq(NGbCtBankLoan::getIsDelete,0);
        return this.list(wrapper);
    }

//    @Override
//    public Integer getCurrentMoney(NGbCashFolowEntity cashFolowEntity) {
//        return gbCtBankLoanMapper.getCurrentMoney(cashFolowEntity);
//    }

}
