package com.xiyou.main.service.ngbcontest.impl;

import com.xiyou.main.entity.ngbcontest.NGbCtBudgetApply;
import com.xiyou.main.dao.ngbcontest.NGbCtBudgetApplyMapper;
import com.xiyou.main.service.ngbcontest.NGbCtBudgetApplyService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author wangxingyu
 * @since 2024-02-18
 */
@Service
public class NGbCtBudgetApplyServiceImpl extends ServiceImpl<NGbCtBudgetApplyMapper, NGbCtBudgetApply> implements NGbCtBudgetApplyService {

}
