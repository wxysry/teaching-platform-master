package com.xiyou.main.service.ngbcontest.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.xiyou.main.dao.ngbcontest.NGbCtCashflowMapper;
import com.xiyou.main.entity.ngbcontest.NGbCtBudget;
import com.xiyou.main.dao.ngbcontest.NGbCtBudgetMapper;
import com.xiyou.main.entity.ngbcontest.NGbCtCashflow;
import com.xiyou.main.enums.CashFlowActionEnum;
import com.xiyou.main.service.ngbcontest.NGbCtBudgetService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author wangxingyu
 * @since 2024-02-18
 */
@Service
public class NGbCtBudgetServiceImpl extends ServiceImpl<NGbCtBudgetMapper, NGbCtBudget> implements NGbCtBudgetService {


    @Autowired
    private NGbCtCashflowMapper nGbCtCashflowMapper;

    /**
     * 获取当前赛季的预算使用
     *
     * @param contestId
     * @param userId
     * @param date
     * @param type
     * @return
     */
    @Override
    public Integer getSeasonUsed(Integer contestId, Integer userId, Integer date, String type) {

        //根据部门类型
        List<String> actionList = NGbCtCashflow.getActionListByDeptType(type);

        List<NGbCtCashflow> nGbCtCashflows= nGbCtCashflowMapper.selectList(new LambdaQueryWrapper<NGbCtCashflow>()
                .eq(NGbCtCashflow::getContestId,contestId)
                .eq(NGbCtCashflow::getStudentId,userId)
                .eq(NGbCtCashflow::getCDate,date)
                .in(NGbCtCashflow::getCAction,actionList)
        );
        if(nGbCtCashflows!=null && nGbCtCashflows.size()>0){
            Integer sum = 0;
            for (NGbCtCashflow cashflow : nGbCtCashflows) {
                sum +=cashflow.getCOut();
            }
            return sum;
        }else{
            return 0;
        }
    }
}
