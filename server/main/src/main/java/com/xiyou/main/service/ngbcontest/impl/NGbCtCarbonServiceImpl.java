package com.xiyou.main.service.ngbcontest.impl;

import com.xiyou.main.entity.ngbcontest.NGbCtCarbon;
import com.xiyou.main.dao.ngbcontest.NGbCtCarbonMapper;
import com.xiyou.main.service.ngbcontest.NGbCtCarbonService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author wangxingyu
 * @since 2024-04-19
 */
@Service
public class NGbCtCarbonServiceImpl extends ServiceImpl<NGbCtCarbonMapper, NGbCtCarbon> implements NGbCtCarbonService {

}
