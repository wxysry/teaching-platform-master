package com.xiyou.main.service.ngbcontest.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.xiyou.main.dao.ngbcontest.NGbCtCashflowMapper;
import com.xiyou.main.entity.ngbcontest.NGbCtCashflow;
import com.xiyou.main.service.ngbcontest.NGbCtCashflowService;
import com.xiyou.main.vo.ngbcontest.NGbCashFolowEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author wangxingyu
 * @since 2023-06-11
 */
@Service
public class NGbCtCashflowServiceImpl extends ServiceImpl<NGbCtCashflowMapper, NGbCtCashflow> implements NGbCtCashflowService {

    @Autowired
    NGbCtCashflowMapper gbCtCashflowMapper;


    @Override
    public NGbCashFolowEntity getStudentFinanceInfor(Integer studentId, Integer contestId) {
        return null;
    }

    @Override
    public Integer getStudentLastCash(NGbCashFolowEntity gbCashFolowEntity) {
        return gbCtCashflowMapper.getStudentLastCash(gbCashFolowEntity);
    }


    @Override
    public Integer getCash(Integer userId, Integer contestId) {
        return gbCtCashflowMapper.getCash(userId, contestId);
    }

    @Override
    public List<NGbCtCashflow> list(Integer studentId, Integer contestId, Integer year, List<String> actionList) {
        return gbCtCashflowMapper.list(studentId, contestId, year, actionList);
    }
}
