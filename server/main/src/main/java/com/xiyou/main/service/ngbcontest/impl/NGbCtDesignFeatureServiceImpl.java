package com.xiyou.main.service.ngbcontest.impl;

import com.xiyou.main.entity.ngbcontest.NGbCtDesignFeature;
import com.xiyou.main.dao.ngbcontest.NGbCtDesignFeatureMapper;
import com.xiyou.main.service.ngbcontest.NGbCtDesignFeatureService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author wangxingyu
 * @since 2024-03-26
 */
@Service
public class NGbCtDesignFeatureServiceImpl extends ServiceImpl<NGbCtDesignFeatureMapper, NGbCtDesignFeature> implements NGbCtDesignFeatureService {

}
