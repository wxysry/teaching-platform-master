package com.xiyou.main.service.ngbcontest.impl;

import com.xiyou.main.entity.ngbcontest.NGbCtDesignProgress;
import com.xiyou.main.dao.ngbcontest.NGbCtDesignProgressMapper;
import com.xiyou.main.service.ngbcontest.NGbCtDesignProgressService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author wangxingyu
 * @since 2024-03-26
 */
@Service
public class NGbCtDesignProgressServiceImpl extends ServiceImpl<NGbCtDesignProgressMapper, NGbCtDesignProgress> implements NGbCtDesignProgressService {

}
