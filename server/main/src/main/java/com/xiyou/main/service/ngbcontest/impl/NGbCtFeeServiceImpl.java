package com.xiyou.main.service.ngbcontest.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.xiyou.main.dao.ngbcontest.NGbCtFeeMapper;
import com.xiyou.main.entity.ngbcontest.NGbCtFee;
import com.xiyou.main.service.ngbcontest.NGbCtFeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author tangcan
 * @since 2019-07-22
 */
@Service
public class NGbCtFeeServiceImpl extends ServiceImpl<NGbCtFeeMapper, NGbCtFee> implements NGbCtFeeService {

    @Autowired
    NGbCtFeeMapper ctFeeMapper;

    @Override
    public int getFeeSum(NGbCtFee fee) {
        if (fee == null) {
            return 0;
        }
        QueryWrapper<NGbCtFee> wrapper = new QueryWrapper<>();
        wrapper.eq("student_id", fee.getStudentId())
                .eq("contest_id", fee.getContestId());
        if (fee.getType() != null) {
            wrapper.eq("type", fee.getType());
        }
        List<NGbCtFee> feeList = this.list(wrapper);
        return feeList.stream().mapToInt(NGbCtFee::getRFee).sum();
    }
}
