package com.xiyou.main.service.ngbcontest.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.xiyou.main.dao.ngbcontest.NGbCtFinancialTargetMapper;
import com.xiyou.main.entity.ngbcontest.NGbCtFinancialTarget;
import com.xiyou.main.service.ngbcontest.NGbCtFinancialTargetService;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author wangxingyu
 * @since 2023-06-28
 */
@Service
public class NGbCtFinancialTargetServiceImpl extends ServiceImpl<NGbCtFinancialTargetMapper, NGbCtFinancialTarget> implements NGbCtFinancialTargetService {


    @Override
    public NGbCtFinancialTarget get(NGbCtFinancialTarget ctFinancialTarget) {
        QueryWrapper<NGbCtFinancialTarget> wrapper = new QueryWrapper<>();
        if (ctFinancialTarget.getStudentId() != null) {
            wrapper.eq("student_id", ctFinancialTarget.getStudentId());
        }
        if (ctFinancialTarget.getContestId() != null) {
            wrapper.eq("contest_id", ctFinancialTarget.getContestId());
        }
        if (ctFinancialTarget.getFtYear() != null) {
            wrapper.eq("ft_year", ctFinancialTarget.getFtYear());
        }
        wrapper.eq("ft_isxt", 1);
        return this.getOne(wrapper);
    }



    @Override
    public NGbCtFinancialTarget getTemp(Integer studentId, Integer contestId, int year) {
        QueryWrapper<NGbCtFinancialTarget> wrapper = new QueryWrapper<>();
        wrapper.eq("student_id", studentId);
        wrapper.eq("contest_id", contestId);
        wrapper.eq("ft_year", year);
        wrapper.eq("ft_isxt", 0);
        return this.getOne(wrapper);
    }
}
