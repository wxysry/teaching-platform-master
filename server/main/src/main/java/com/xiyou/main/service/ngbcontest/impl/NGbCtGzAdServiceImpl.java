package com.xiyou.main.service.ngbcontest.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.xiyou.main.dao.ngbcontest.NGbCtGzAdMapper;
import com.xiyou.main.entity.ngbcontest.NGbCtGzAd;
import com.xiyou.main.service.ngbcontest.NGbCtGzAdService;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author wangxingyu
 * @since 2023-06-06
 */
@Service
public class NGbCtGzAdServiceImpl extends ServiceImpl<NGbCtGzAdMapper, NGbCtGzAd> implements NGbCtGzAdService {

}
