package com.xiyou.main.service.ngbcontest.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.xiyou.main.dao.ngbcontest.NGbCtGzClassesMapper;
import com.xiyou.main.entity.ngbcontest.NGbCtGzClasses;
import com.xiyou.main.service.ngbcontest.NGbCtGzClassesService;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author wangxingyu
 * @since 2023-06-06
 */
@Service
public class NGbCtGzClassesServiceImpl extends ServiceImpl<NGbCtGzClassesMapper, NGbCtGzClasses> implements NGbCtGzClassesService {

}
