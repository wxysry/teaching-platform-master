package com.xiyou.main.service.ngbcontest.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.xiyou.main.dao.ngbcontest.NGbCtGzCsMapper;
import com.xiyou.main.entity.ngbcontest.NGbCtGzCs;
import com.xiyou.main.service.ngbcontest.NGbCtGzCsService;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author wangxingyu
 * @since 2023-06-06
 */
@Service
public class NGbCtGzCsServiceImpl extends ServiceImpl<NGbCtGzCsMapper, NGbCtGzCs> implements NGbCtGzCsService {

    @Override
    public NGbCtGzCs getByContestId(Integer contestId) {
        return baseMapper.getByContestId(contestId);
    }


    @Override
    public NGbCtGzCs getBySubjectNumber(Integer subjectNumber) {

        return baseMapper.getBySubjectNumber(subjectNumber);
    }
}
