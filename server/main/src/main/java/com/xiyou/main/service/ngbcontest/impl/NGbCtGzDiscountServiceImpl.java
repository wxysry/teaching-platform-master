package com.xiyou.main.service.ngbcontest.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.xiyou.main.dao.ngbcontest.NGbCtGzDiscountMapper;
import com.xiyou.main.entity.ngbcontest.NGbCtGzDiscount;
import com.xiyou.main.service.ngbcontest.NGbCtGzDiscountService;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author wangxingyu
 * @since 2023-06-06
 */
@Service
public class NGbCtGzDiscountServiceImpl extends ServiceImpl<NGbCtGzDiscountMapper, NGbCtGzDiscount> implements NGbCtGzDiscountService {

}
