package com.xiyou.main.service.ngbcontest.impl;

import com.xiyou.main.entity.ngbcontest.NGbCtGzDrainage;
import com.xiyou.main.dao.ngbcontest.NGbCtGzDrainageMapper;
import com.xiyou.main.service.ngbcontest.NGbCtGzDrainageService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author wangxingyu
 * @since 2024-05-25
 */
@Service
public class NGbCtGzDrainageServiceImpl extends ServiceImpl<NGbCtGzDrainageMapper, NGbCtGzDrainage> implements NGbCtGzDrainageService {

}
