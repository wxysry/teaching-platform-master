package com.xiyou.main.service.ngbcontest.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.xiyou.main.dao.ngbcontest.NGbCtGzIsoMapper;
import com.xiyou.main.entity.ngbcontest.NGbCtGzIso;
import com.xiyou.main.service.ngbcontest.NGbCtGzIsoService;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author wangxingyu
 * @since 2023-06-06
 */
@Service
public class NGbCtGzIsoServiceImpl extends ServiceImpl<NGbCtGzIsoMapper, NGbCtGzIso> implements NGbCtGzIsoService {

}
