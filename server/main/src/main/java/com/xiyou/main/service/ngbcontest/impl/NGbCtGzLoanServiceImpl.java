package com.xiyou.main.service.ngbcontest.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.xiyou.main.dao.ngbcontest.NGbCtGzLoanMapper;
import com.xiyou.main.entity.ngbcontest.NGbCtGzLoan;
import com.xiyou.main.service.ngbcontest.NGbCtGzLoanService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author wangxingyu
 * @since 2023-06-06
 */
@Service
public class NGbCtGzLoanServiceImpl extends ServiceImpl<NGbCtGzLoanMapper, NGbCtGzLoan> implements NGbCtGzLoanService {


    @Autowired
    NGbCtGzLoanMapper gbCtGzLoanMapper;
    @Override
    public List<NGbCtGzLoan> listCtGzLoan(Integer gbContestId) {
        return gbCtGzLoanMapper.listCtGzLoan(gbContestId);
    }
}
