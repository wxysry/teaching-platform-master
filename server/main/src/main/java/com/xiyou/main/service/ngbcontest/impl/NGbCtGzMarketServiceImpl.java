package com.xiyou.main.service.ngbcontest.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.xiyou.main.dao.ngbcontest.NGbCtGzMarketMapper;
import com.xiyou.main.entity.ngbcontest.NGbCtGzMarket;
import com.xiyou.main.service.ngbcontest.NGbCtGzMarketService;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author wangxingyu
 * @since 2023-06-06
 */
@Service
public class NGbCtGzMarketServiceImpl extends ServiceImpl<NGbCtGzMarketMapper, NGbCtGzMarket> implements NGbCtGzMarketService {

}
