package com.xiyou.main.service.ngbcontest.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.xiyou.main.dao.ngbcontest.NGbCtGzMaterialMapper;
import com.xiyou.main.entity.ngbcontest.NGbCtGzMaterial;
import com.xiyou.main.service.ngbcontest.NGbCtGzMaterialService;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author wangxingyu
 * @since 2023-06-06
 */
@Service
public class NGbCtGzMaterialServiceImpl extends ServiceImpl<NGbCtGzMaterialMapper, NGbCtGzMaterial> implements NGbCtGzMaterialService {

}
