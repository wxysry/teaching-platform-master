package com.xiyou.main.service.ngbcontest.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.xiyou.main.dao.ngbcontest.NGbCtGzNumMapper;
import com.xiyou.main.entity.ngbcontest.NGbCtGzNum;
import com.xiyou.main.service.ngbcontest.NGbCtGzNumService;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author wangxingyu
 * @since 2023-06-06
 */
@Service
public class NGbCtGzNumServiceImpl extends ServiceImpl<NGbCtGzNumMapper, NGbCtGzNum> implements NGbCtGzNumService {

}
