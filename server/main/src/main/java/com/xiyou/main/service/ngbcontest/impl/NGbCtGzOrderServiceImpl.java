package com.xiyou.main.service.ngbcontest.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.xiyou.main.dao.ngbcontest.NGbCtGzOrderMapper;
import com.xiyou.main.entity.ngbcontest.NGbCtGzOrder;
import com.xiyou.main.service.ngbcontest.NGbCtGzOrderService;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author wangxingyu
 * @since 2023-06-06
 */
@Service
public class NGbCtGzOrderServiceImpl extends ServiceImpl<NGbCtGzOrderMapper, NGbCtGzOrder> implements NGbCtGzOrderService {

}
