package com.xiyou.main.service.ngbcontest.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.xiyou.main.dao.ngbcontest.NGbCtGzProducingMapper;
import com.xiyou.main.entity.ngbcontest.NGbCtGzProducing;
import com.xiyou.main.service.ngbcontest.NGbCtGzProducingService;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author wangxingyu
 * @since 2023-06-06
 */
@Service
public class NGbCtGzProducingServiceImpl extends ServiceImpl<NGbCtGzProducingMapper, NGbCtGzProducing> implements NGbCtGzProducingService {

}
