package com.xiyou.main.service.ngbcontest.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.xiyou.main.dao.ngbcontest.NGbCtGzProductDesignMapper;
import com.xiyou.main.entity.ngbcontest.NGbCtGzProductDesign;
import com.xiyou.main.service.ngbcontest.NGbCtGzProductDesignService;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author wangxingyu
 * @since 2023-06-06
 */
@Service
public class NGbCtGzProductDesignServiceImpl extends ServiceImpl<NGbCtGzProductDesignMapper, NGbCtGzProductDesign> implements NGbCtGzProductDesignService {

}
