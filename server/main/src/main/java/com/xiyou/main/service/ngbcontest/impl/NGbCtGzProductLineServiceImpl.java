package com.xiyou.main.service.ngbcontest.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.xiyou.main.dao.ngbcontest.NGbCtGzProductLineMapper;
import com.xiyou.main.entity.ngbcontest.NGbCtGzProductLine;
import com.xiyou.main.service.ngbcontest.NGbCtGzProductLineService;
import com.xiyou.main.vo.ngbcontest.NGbCashFolowEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author wangxingyu
 * @since 2023-06-06
 */
@Service
public class NGbCtGzProductLineServiceImpl extends ServiceImpl<NGbCtGzProductLineMapper, NGbCtGzProductLine> implements NGbCtGzProductLineService {

    @Autowired
    NGbCtGzProductLineMapper ctGzProductLineMapper;

    @Override
    public List<NGbCtGzProductLine> getProductLineTypeList(Integer contestId) {
        return ctGzProductLineMapper.getProductLineTypeList(contestId);
    }

    @Override
    public Integer getProductLineInfo(NGbCashFolowEntity cashFolowEntity) {

        return ctGzProductLineMapper.getProductLineInfo(cashFolowEntity);
    }

    @Override
    public List<NGbCtGzProductLine> getListByCplids(List<Integer> cplids) {
        QueryWrapper<NGbCtGzProductLine> wrapper = new QueryWrapper<>();
        wrapper.in("id", cplids);
        return this.list(wrapper);
    }

}
