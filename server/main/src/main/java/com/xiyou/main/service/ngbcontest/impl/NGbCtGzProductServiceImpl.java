package com.xiyou.main.service.ngbcontest.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.xiyou.main.dao.ngbcontest.NGbCtGzProductMapper;
import com.xiyou.main.entity.ngbcontest.NGbCtGzProduct;
import com.xiyou.main.service.ngbcontest.NGbCtGzProductService;
import com.xiyou.main.vo.ngbcontest.NGbYfProductVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author wangxingyu
 * @since 2023-06-06
 */
@Service
public class NGbCtGzProductServiceImpl extends ServiceImpl<NGbCtGzProductMapper, NGbCtGzProduct> implements NGbCtGzProductService {

    @Autowired
    NGbCtGzProductMapper mapper;

    @Override
    public List<String> getProductCPName(Integer contestId) {
        return mapper.getProductCPName(contestId);
    }

    @Override
    public List<NGbYfProductVO> listYfProduct(Integer userId, Integer contestId) {
        return mapper.listYfProduct(userId, contestId);
    }

    @Override
    public List<NGbCtGzProduct> getList(Integer contestId) {
        return mapper.getList(contestId);
    }

    @Override
    public List<NGbCtGzProduct> getListByIds(List<Integer> cpIds) {
        if (cpIds == null || cpIds.size() == 0) {
            return new ArrayList<>();
        }
        QueryWrapper<NGbCtGzProduct> wrapper = new QueryWrapper<>();
        wrapper.in("id", cpIds);
        return this.list(wrapper);
    }
}
