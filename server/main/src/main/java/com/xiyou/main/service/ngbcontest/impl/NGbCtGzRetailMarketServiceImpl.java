package com.xiyou.main.service.ngbcontest.impl;

import com.xiyou.main.entity.ngbcontest.NGbCtGzRetailMarket;
import com.xiyou.main.dao.ngbcontest.NGbCtGzRetailMarketMapper;
import com.xiyou.main.service.ngbcontest.NGbCtGzRetailMarketService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author wangxingyu
 * @since 2024-05-25
 */
@Service
public class NGbCtGzRetailMarketServiceImpl extends ServiceImpl<NGbCtGzRetailMarketMapper, NGbCtGzRetailMarket> implements NGbCtGzRetailMarketService {

}
