package com.xiyou.main.service.ngbcontest.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.xiyou.main.dao.ngbcontest.NGbCtGzWorkerIncentiveMapper;
import com.xiyou.main.entity.ngbcontest.NGbCtGzWorkerIncentive;
import com.xiyou.main.service.ngbcontest.NGbCtGzWorkerIncentiveService;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author wangxingyu
 * @since 2023-06-06
 */
@Service
public class NGbCtGzWorkerIncentiveServiceImpl extends ServiceImpl<NGbCtGzWorkerIncentiveMapper, NGbCtGzWorkerIncentive> implements NGbCtGzWorkerIncentiveService {

}
