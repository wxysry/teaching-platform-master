package com.xiyou.main.service.ngbcontest.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.xiyou.main.dao.ngbcontest.NGbCtGzWorkerRecruitMapper;
import com.xiyou.main.entity.ngbcontest.NGbCtGzWorkerRecruit;
import com.xiyou.main.service.ngbcontest.NGbCtGzWorkerRecruitService;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author wangxingyu
 * @since 2023-06-06
 */
@Service
public class NGbCtGzWorkerRecruitServiceImpl extends ServiceImpl<NGbCtGzWorkerRecruitMapper, NGbCtGzWorkerRecruit> implements NGbCtGzWorkerRecruitService {

}
