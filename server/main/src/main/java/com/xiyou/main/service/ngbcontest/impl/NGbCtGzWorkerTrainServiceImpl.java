package com.xiyou.main.service.ngbcontest.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.xiyou.main.dao.ngbcontest.NGbCtGzWorkerTrainMapper;
import com.xiyou.main.entity.ngbcontest.NGbCtGzWorkerTrain;
import com.xiyou.main.service.ngbcontest.NGbCtGzWorkerTrainService;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author wangxingyu
 * @since 2023-06-06
 */
@Service
public class NGbCtGzWorkerTrainServiceImpl extends ServiceImpl<NGbCtGzWorkerTrainMapper, NGbCtGzWorkerTrain> implements NGbCtGzWorkerTrainService {

}
