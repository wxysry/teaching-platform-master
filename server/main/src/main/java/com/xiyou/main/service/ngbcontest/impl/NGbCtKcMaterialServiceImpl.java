package com.xiyou.main.service.ngbcontest.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.xiyou.main.dao.ngbcontest.NGbCtKcMaterialMapper;
import com.xiyou.main.entity.ngbcontest.NGbCtGzCs;
import com.xiyou.main.entity.ngbcontest.NGbCtKcMaterial;
import com.xiyou.main.service.ngbcontest.NGbCtGzCsService;
import com.xiyou.main.service.ngbcontest.NGbCtKcMaterialService;
import com.xiyou.main.vo.ngbcontest.NGbCashFolowEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author tangcan
 * @since 2019-07-22
 */
@Service
public class NGbCtKcMaterialServiceImpl extends ServiceImpl<NGbCtKcMaterialMapper, NGbCtKcMaterial> implements NGbCtKcMaterialService {
    @Autowired
    private NGbCtKcMaterialMapper ctKcMaterialMapper;
    @Autowired
    private NGbCtGzCsService ctGzCsService;

    @Override
    public int getMaterialSum(Integer studentId, Integer contestId) {
        return ctKcMaterialMapper.getMaterialSum(studentId, contestId);
    }


}
