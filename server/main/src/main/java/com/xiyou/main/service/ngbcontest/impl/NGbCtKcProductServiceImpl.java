package com.xiyou.main.service.ngbcontest.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.xiyou.main.dao.ngbcontest.NGbCtKcProductMapper;
import com.xiyou.main.entity.ngbcontest.NGbCtGzCs;
import com.xiyou.main.entity.ngbcontest.NGbCtKcProduct;
import com.xiyou.main.service.ngbcontest.NGbCtGzCsService;
import com.xiyou.main.service.ngbcontest.NGbCtKcProductService;
import com.xiyou.main.vo.ngbcontest.NGbProductVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author tangcan
 * @since 2019-07-22
 */
@Service
public class NGbCtKcProductServiceImpl extends ServiceImpl<NGbCtKcProductMapper, NGbCtKcProduct> implements NGbCtKcProductService {
    @Autowired
    private NGbCtKcProductMapper ctKcProductMapper;
    @Autowired
    private NGbCtGzCsService ctGzCsService;


    @Override
    public int getProductSum(Integer studentId, Integer contestId) {
        return ctKcProductMapper.getProductSum(studentId, contestId);
    }



//    @Override
//    public List<NGbProductVo> listSellKc(Integer contestId, Integer userId) {
//        List<NGbProductVo> list = ctKcProductMapper.listSellKc(contestId, userId);
//        NGbCtGzCs ctGzCs = ctGzCsService.getByContestId(contestId);
//        if (list.size() > 0) {
//            for (NGbProductVo l : list) {
//                l.setUrgentPrice(l.getDirectCost() * ctGzCs.getEmergenceMultipleProduct());
//                l.setSellPrice((double) l.getDirectCost() * ((double) ctGzCs.getInventoryDiscountProduct() / 100.0));
//            }
//        }
//        return list;
//    }
}
