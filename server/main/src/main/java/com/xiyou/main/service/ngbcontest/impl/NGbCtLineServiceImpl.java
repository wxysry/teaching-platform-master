package com.xiyou.main.service.ngbcontest.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.xiyou.main.dao.ngbcontest.NGbCtLineMapper;
import com.xiyou.main.entity.ngbcontest.NGbCtGzProductLine;
import com.xiyou.main.entity.ngbcontest.NGbCtLine;
import com.xiyou.main.service.ngbcontest.NGbCtLineService;
import com.xiyou.main.vo.ngbcontest.NGbOnlineLine;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author tangcan
 * @since 2019-07-22
 */
@Service
public class NGbCtLineServiceImpl extends ServiceImpl<NGbCtLineMapper, NGbCtLine> implements NGbCtLineService {
    @Autowired
    NGbCtLineMapper ctLineMapper;

    @Override
    public List<NGbOnlineLine> listOnline(Integer userId, Integer contestId, Integer date) {
        return ctLineMapper.listOnline(userId, contestId, date);
    }

    @Override
    public List<NGbCtGzProductLine> getLineInfo(List<Integer> lineIds) {
        return ctLineMapper.getLineInfo(lineIds);
    }

    @Override
    public List<NGbOnlineLine> listTransfer(Integer userId, Integer contestId) {
        return ctLineMapper.listTransfer(userId, contestId);
    }

    @Override
    public int getMaintenanceFee(Integer studentId, Integer contestId, Integer date) {
        return ctLineMapper.getMaintenanceFee(studentId, contestId, date);
    }

    @Override
    public void updateDepFee(Integer studentId, Integer contestId, Integer date) {
        ctLineMapper.updateDepFee(studentId, contestId, date);
    }

    @Override
    public Integer getDepTotal(Integer studentId, Integer contestId, int date) {
        return ctLineMapper.getDepTotal(studentId, contestId, date);
    }

    @Override
    public int getProductInProcess(NGbCtLine line) {
        if (line == null) {
            return 0;
        }
        return ctLineMapper.getProductInProcess(line);
    }

    @Override
    public int getEquipmentSum(Integer studentId, Integer contestId) {
        return ctLineMapper.getEquipmentSum(studentId, contestId);
    }

    @Override
    public List<NGbCtLine> list(Integer studentId, Integer contestId) {
        QueryWrapper<NGbCtLine> wrapper = new QueryWrapper<>();
        wrapper.eq("student_id", studentId).eq("contest_id", contestId);
        return this.list(wrapper);
    }

    @Override
    public List<NGbOnlineLine> listNoProducting(Integer userId, Integer contestId) {
        return ctLineMapper.listNoProducting(userId, contestId);
    }

    @Override
    public List<NGbOnlineLine> getListOnlineInfo(Integer userId, Integer contestId) {
        return ctLineMapper.getListOnline(userId, contestId);
    }


}
