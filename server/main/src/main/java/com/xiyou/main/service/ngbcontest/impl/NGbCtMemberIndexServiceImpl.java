package com.xiyou.main.service.ngbcontest.impl;

import com.xiyou.main.entity.ngbcontest.NGbCtMemberIndex;
import com.xiyou.main.dao.ngbcontest.NGbCtMemberIndexMapper;
import com.xiyou.main.service.ngbcontest.NGbCtMemberIndexService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author wangxingyu
 * @since 2024-05-25
 */
@Service
public class NGbCtMemberIndexServiceImpl extends ServiceImpl<NGbCtMemberIndexMapper, NGbCtMemberIndex> implements NGbCtMemberIndexService {

}
