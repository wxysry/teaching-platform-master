package com.xiyou.main.service.ngbcontest.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.xiyou.main.dao.ngbcontest.NGbCtMnAdMapper;
import com.xiyou.main.entity.ngbcontest.NGbCtMnAd;
import com.xiyou.main.service.ngbcontest.NGbCtMnAdService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.sql.DataSource;
import java.util.List;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author tangcan
 * @since 2019-07-25
 */
@Service
public class NGbCtMnAdServiceImpl extends ServiceImpl<NGbCtMnAdMapper, NGbCtMnAd> implements NGbCtMnAdService {
    @Autowired
    private NGbCtMnAdMapper gbCtMnAdMapper;

    @Override
    public List<NGbCtMnAd> getAdList(Integer contestId, Integer studentId, Integer year ,Integer quarterly) {
        return gbCtMnAdMapper.getAdList(contestId, studentId, year,quarterly);
    }

    @Override
    public List<NGbCtMnAd> getAdListByGroupNum(Integer contestId, Integer studentId, String name) {
        QueryWrapper<NGbCtMnAd> wrapper = new QueryWrapper<>();
        wrapper.eq("student_id", studentId)
                .eq("contest_id", contestId)
                .eq("group_num", name)
                .eq("is_submit","Y")
                .orderByAsc("year","quarterly");
        return this.list(wrapper);
    }



    /**
     * 获取所有学生本市场的广告投放排名
     *
     * @param contestId
     * @param year
     * @param quarterly
     * @param cmId
     * @return
     */
    @Override
    public List<NGbCtMnAd> getAdRank(Integer contestId, Integer year, Integer quarterly, Integer cmId,String cmName) {

         List<NGbCtMnAd>  nGbCtMnAds = this.gbCtMnAdMapper.getAdRank(contestId,year,quarterly,cmId);
         //补全信息
        for (NGbCtMnAd nGbCtMnAd : nGbCtMnAds) {
            nGbCtMnAd.setYear(year);
            nGbCtMnAd.setQuarterly(quarterly);
            nGbCtMnAd.setCmId(cmId);
            nGbCtMnAd.setCmName(cmName);
        }
        return nGbCtMnAds;
    }
}
