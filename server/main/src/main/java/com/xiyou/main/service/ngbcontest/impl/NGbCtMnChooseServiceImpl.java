package com.xiyou.main.service.ngbcontest.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.xiyou.main.dao.ngbcontest.NGbCtMnChooseMapper;
import com.xiyou.main.entity.ngbcontest.NGbCtMnChoose;
import com.xiyou.main.service.ngbcontest.NGbCtMnChooseService;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author tangcan
 * @since 2019-08-27
 */
@Service
public class NGbCtMnChooseServiceImpl extends ServiceImpl<NGbCtMnChooseMapper, NGbCtMnChoose> implements NGbCtMnChooseService {

}
