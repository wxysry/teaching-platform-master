package com.xiyou.main.service.ngbcontest.impl;

import com.xiyou.main.entity.ngbcontest.NGbCtOrderApply;
import com.xiyou.main.dao.ngbcontest.NGbCtOrderApplyMapper;
import com.xiyou.main.service.ngbcontest.NGbCtOrderApplyService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author wangxingyu
 * @since 2024-03-30
 */
@Service
public class NGbCtOrderApplyServiceImpl extends ServiceImpl<NGbCtOrderApplyMapper, NGbCtOrderApply> implements NGbCtOrderApplyService {

}
