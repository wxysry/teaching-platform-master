package com.xiyou.main.service.ngbcontest.impl;

import com.xiyou.main.entity.ngbcontest.NGbCtPdca;
import com.xiyou.main.dao.ngbcontest.NGbCtPdcaMapper;
import com.xiyou.main.service.ngbcontest.NGbCtPdcaService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author wangxingyu
 * @since 2024-04-10
 */
@Service
public class NGbCtPdcaServiceImpl extends ServiceImpl<NGbCtPdcaMapper, NGbCtPdca> implements NGbCtPdcaService {

}
