package com.xiyou.main.service.ngbcontest.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.xiyou.main.dao.ngbcontest.NGbCtProfitChartMapper;
import com.xiyou.main.entity.ngbcontest.NGbCtProfitChart;
import com.xiyou.main.service.ngbcontest.NGbCtProfitChartService;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author tangcan
 * @since 2019-07-22
 */
@Service
public class NGbCtProfitChartServiceImpl extends ServiceImpl<NGbCtProfitChartMapper, NGbCtProfitChart> implements NGbCtProfitChartService {
    @Override
    public List<NGbCtProfitChart> list(NGbCtProfitChart profitChart) {
        if (profitChart == null) {
            return new ArrayList<>();
        }
        QueryWrapper<NGbCtProfitChart> wrapper = new QueryWrapper<>();
        wrapper.eq("student_id", profitChart.getStudentId())
                .eq("contest_id", profitChart.getContestId())
                .eq("bs_isxt", 1);
        if (profitChart.getPcDate() != null) {
            wrapper.eq("pc_date", profitChart.getPcDate());
        }
        return this.list(wrapper);
    }

    @Override
    public NGbCtProfitChart getSys(NGbCtProfitChart profitChart) {
        if (profitChart == null) {
            return null;
        }
        QueryWrapper<NGbCtProfitChart> wrapper = new QueryWrapper<>();
        wrapper.eq("student_id", profitChart.getStudentId())
                .eq("contest_id", profitChart.getContestId())
                .eq("pc_date", profitChart.getPcDate())
                .eq("bs_isxt", 1);
        return this.getOne(wrapper);
    }


    @Override
    public NGbCtProfitChart getTemp(Integer studentId, Integer contestId, Integer year) {
        QueryWrapper<NGbCtProfitChart> wrapper = new QueryWrapper<>();
        wrapper.eq("student_id", studentId)
                .eq("contest_id", contestId)
                .eq("pc_date", year)
                .eq("bs_isxt", 0);
        return this.getOne(wrapper);
    }


    @Override
    public List<NGbCtProfitChart> getCurrentYear(NGbCtProfitChart profitChart) {
        if (profitChart == null) {
            return new ArrayList<>();
        }
        QueryWrapper<NGbCtProfitChart> wrapper = new QueryWrapper<>();
        wrapper.eq("student_id", profitChart.getStudentId())
                .eq("contest_id", profitChart.getContestId())
                .eq("pc_date", profitChart.getPcDate());
        return this.list(wrapper);
    }

    @Override
    public NGbCtProfitChart getOne(NGbCtProfitChart ctProfitChart) {
        QueryWrapper<NGbCtProfitChart> wrapper = new QueryWrapper<>();
        if (ctProfitChart.getStudentId() != null) {
            wrapper.eq("student_id", ctProfitChart.getStudentId());
        }
        if (ctProfitChart.getContestId() != null) {
            wrapper.eq("contest_id", ctProfitChart.getContestId());
        }
        if (ctProfitChart.getBsIsxt() != null) {
            wrapper.eq("bs_isxt", ctProfitChart.getBsIsxt());
        }
        if (ctProfitChart.getPcDate() != null) {
            wrapper.eq("pc_date", ctProfitChart.getPcDate());
        }
        return this.getOne(wrapper);
    }
}
