package com.xiyou.main.service.ngbcontest.impl;

import com.xiyou.main.entity.ngbcontest.NGbCtRetailApply;
import com.xiyou.main.dao.ngbcontest.NGbCtRetailApplyMapper;
import com.xiyou.main.service.ngbcontest.NGbCtRetailApplyService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author wangxingyu
 * @since 2024-05-25
 */
@Service
public class NGbCtRetailApplyServiceImpl extends ServiceImpl<NGbCtRetailApplyMapper, NGbCtRetailApply> implements NGbCtRetailApplyService {

}
