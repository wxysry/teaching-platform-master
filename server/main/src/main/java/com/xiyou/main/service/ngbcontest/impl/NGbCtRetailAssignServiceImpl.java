package com.xiyou.main.service.ngbcontest.impl;

import com.xiyou.main.entity.ngbcontest.NGbCtRetailAssign;
import com.xiyou.main.dao.ngbcontest.NGbCtRetailAssignMapper;
import com.xiyou.main.service.ngbcontest.NGbCtRetailAssignService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author wangxingyu
 * @since 2024-05-25
 */
@Service
public class NGbCtRetailAssignServiceImpl extends ServiceImpl<NGbCtRetailAssignMapper, NGbCtRetailAssign> implements NGbCtRetailAssignService {

}
