package com.xiyou.main.service.ngbcontest.impl;

import com.xiyou.main.entity.ngbcontest.NGbCtScore;
import com.xiyou.main.dao.ngbcontest.NGbCtScoreMapper;
import com.xiyou.main.service.ngbcontest.NGbCtScoreService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author wangxingyu
 * @since 2024-05-08
 */
@Service
public class NGbCtScoreServiceImpl extends ServiceImpl<NGbCtScoreMapper, NGbCtScore> implements NGbCtScoreService {

}
