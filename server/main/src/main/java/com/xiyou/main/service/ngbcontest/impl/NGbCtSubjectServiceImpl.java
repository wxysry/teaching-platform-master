package com.xiyou.main.service.ngbcontest.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.xiyou.main.dao.ngbcontest.NGbCtSubjectMapper;
import com.xiyou.main.entity.ngbcontest.NGbCtSubject;
import com.xiyou.main.params.contest.SubjectParam;
import com.xiyou.main.service.ngbcontest.NGbCtSubjectService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author tangcan
 * @since 2019-08-25
 */
@Service
public class NGbCtSubjectServiceImpl extends ServiceImpl<NGbCtSubjectMapper, NGbCtSubject> implements NGbCtSubjectService {
    @Autowired
    private NGbCtSubjectMapper gbCtSubjectMapper;

    @Override
    public Page<NGbCtSubject> listByAdmin(SubjectParam subjectParam) {
        Page<NGbCtSubject> page = new Page<>(subjectParam.getPage(), subjectParam.getLimit());
        return gbCtSubjectMapper.listByAdmin(page, subjectParam);
    }

    @Override
    public Page<NGbCtSubject> listByTeacher(SubjectParam subjectParam) {
        Page<NGbCtSubject> page = new Page<>(subjectParam.getPage(), subjectParam.getLimit());
        return gbCtSubjectMapper.listByTeacher(page, subjectParam);
    }

    @Override
    public List<NGbCtSubject> listByTeacher(Integer teacherId,Integer groupId) {
        QueryWrapper<NGbCtSubject> wrapper = new QueryWrapper<>();
        wrapper.eq("group_id", groupId)
                .eq("create_user_id",teacherId)
                .eq("upload", 1);
        return this.list(wrapper);
    }
}
