package com.xiyou.main.service.ngbcontest.impl;

import com.xiyou.main.entity.ngbcontest.NGbCtWorkerMarket;
import com.xiyou.main.dao.ngbcontest.NGbCtWorkerMarketMapper;
import com.xiyou.main.service.ngbcontest.NGbCtWorkerMarketService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author wangxingyu
 * @since 2024-03-21
 */
@Service
public class NGbCtWorkerMarketServiceImpl extends ServiceImpl<NGbCtWorkerMarketMapper, NGbCtWorkerMarket> implements NGbCtWorkerMarketService {

}
