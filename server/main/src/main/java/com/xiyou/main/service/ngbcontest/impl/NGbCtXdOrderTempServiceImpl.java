package com.xiyou.main.service.ngbcontest.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.xiyou.main.dao.ngbcontest.NGbCtXdOrderTempMapper;
import com.xiyou.main.entity.ngbcontest.NGbCtXdOrderTemp;
import com.xiyou.main.service.ngbcontest.NGbCtXdOrderTempService;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author wangxingyu
 * @since 2023-09-14
 */
@Service
public class NGbCtXdOrderTempServiceImpl extends ServiceImpl<NGbCtXdOrderTempMapper, NGbCtXdOrderTemp> implements NGbCtXdOrderTempService {

}
