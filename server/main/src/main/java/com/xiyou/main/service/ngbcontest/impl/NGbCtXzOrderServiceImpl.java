package com.xiyou.main.service.ngbcontest.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.xiyou.main.dao.ngbcontest.NGbCtXzOrderMapper;
import com.xiyou.main.entity.ngbcontest.NGbCtXzOrder;
import com.xiyou.main.service.ngbcontest.NGbCtXzOrderService;
import com.xiyou.main.vo.ngbcontest.NGbXzOrderVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author tangcan
 * @since 2019-07-22
 */
@Service
public class NGbCtXzOrderServiceImpl extends ServiceImpl<NGbCtXzOrderMapper, NGbCtXzOrder> implements NGbCtXzOrderService {
    @Autowired
    private NGbCtXzOrderMapper ctXzOrderMapper;

    @Override
    public List<NGbCtXzOrder> list(NGbCtXzOrder order) {
        return ctXzOrderMapper.list(order);
    }

    @Override
    public Map<String, BigDecimal> getSalesAndDirectCost(Integer studentId, Integer contestId, int year) {
        return ctXzOrderMapper.getSalesAndDirectCost(studentId, contestId, year);
    }

    @Override
    public List<NGbXzOrderVo> listDeliveryOrder(Map<String, Object> queryMap) {
        return ctXzOrderMapper.listDeliveryOrder(queryMap);
    }
}
