package com.xiyou.main.service.ngbcontest.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.xiyou.main.dao.ngbcontest.NGbCtYfMarketMapper;
import com.xiyou.main.entity.ngbcontest.NGbCtYfMarket;
import com.xiyou.main.service.ngbcontest.NGbCtYfMarketService;
import com.xiyou.main.vo.ngbcontest.NGbMarketVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author tangcan
 * @since 2019-07-22
 */
@Service
public class NGbCtYfMarketServiceImpl extends ServiceImpl<NGbCtYfMarketMapper, NGbCtYfMarket> implements NGbCtYfMarketService {
    @Autowired
    private NGbCtYfMarketMapper ctYfMarketMapper;

    @Override
    public void update(NGbCtYfMarket yfMarket) {
        ctYfMarketMapper.update(yfMarket);
    }

    @Override
    public List<String> getHadYfFinishMarkets(Integer studentId, Integer contestId) {
        return ctYfMarketMapper.getHadYfFinishMarkets(studentId, contestId);
    }

    @Override
    public List<NGbMarketVo> listYfMarket(Integer studentId, Integer contestId) {
        return ctYfMarketMapper.listYfMarket(studentId, contestId);
    }
}
