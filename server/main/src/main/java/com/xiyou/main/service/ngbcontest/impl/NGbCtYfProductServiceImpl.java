package com.xiyou.main.service.ngbcontest.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.xiyou.main.dao.ngbcontest.NGbCtYfProductMapper;
import com.xiyou.main.entity.ngbcontest.NGbCtYfProduct;
import com.xiyou.main.service.ngbcontest.NGbCtYfProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author tangcan
 * @since 2019-07-22
 */
@Service
public class NGbCtYfProductServiceImpl extends ServiceImpl<NGbCtYfProductMapper, NGbCtYfProduct> implements NGbCtYfProductService {
    @Autowired
    private NGbCtYfProductMapper ctYfProductMapper;

    @Override
    public void update(NGbCtYfProduct yfProduct) {
        ctYfProductMapper.update(yfProduct);
    }

    @Override
    public void updateEndingSeason(Integer userId, Integer contestId, Integer date) {
        ctYfProductMapper.updateEndingSeason(userId, contestId, date);
    }
}
