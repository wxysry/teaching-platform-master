package com.xiyou.main.shiro;

import com.alibaba.fastjson.JSONObject;
import com.xiyou.common.shiro.jwt.JWTToken;
import com.xiyou.common.shiro.jwt.JWTUtil;
import com.xiyou.common.shiro.service.JWTTokenService;
import com.xiyou.main.entity.exam.SysRole;
import com.xiyou.main.service.exam.SysRoleService;
import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.AuthenticationInfo;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.authc.SimpleAuthenticationInfo;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.authz.SimpleAuthorizationInfo;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.subject.PrincipalCollection;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Date;

/**
 * @program: attendance
 * @description: 管理员、教师、辅导员/班主任
 * @author: tangcan
 * @create: 2018-12-03 18:45
 **/
@Component
@Slf4j
public class ShiroRealm extends AuthorizingRealm {

    @Autowired
    private JWTTokenService jwtTokenService;
    @Autowired
    private SysRoleService roleService;

    public ShiroRealm() {
        setAuthenticationTokenClass(JWTToken.class);
    }

    @Override
    public boolean supports(AuthenticationToken token) {
        return token instanceof JWTToken;
    }

    /**
     * @Author: tangcan
     * @Description: 权限认证
     * @Param: [principalCollection]
     * @date: 2019/3/26
     */
    @Override
    protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principalCollection) {
        JWTToken jwtToken = (JWTToken) principalCollection.getPrimaryPrincipal();
        SysRole role = roleService.getByUserAccount(jwtToken.getAccount());
        // 授权操作
        SimpleAuthorizationInfo authorizationInfo = new SimpleAuthorizationInfo();
        authorizationInfo.addRole(role.getRoleCode());

        return authorizationInfo;
    }

    /**
     * @Author: tangcan
     * @Description: 登陆认证
     * @Param: [authenticationToken]
     * @date: 2019/3/26
     */
    @Override
    protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken authenticationToken) throws AuthenticationException {

        String token = (String) authenticationToken.getCredentials();
        String account = JWTUtil.getAccount(token);
        if (account == null) {
            log.error("登录验证失败:account为空");
            return null;
        }
        /*
         判断是否登陆过期
         更新登陆过期时间
         */
        JWTToken jwtToken = jwtTokenService.get(token);
        if (jwtToken == null) {
            log.error("登录验证失败,account:{},该token已不存在:{}",account);
            return null;
        }
        else if(jwtToken.getActiveTime().compareTo(new Date()) < 0){
            log.error("登录验证失败,account:{},该token已过期,token:{},当前时间",account, JSONObject.toJSONString(jwtToken),new Date());
            return null;
        }
        else {
            Date activeTime = new Date(System.currentTimeMillis() + JWTUtil.ACCESS_TIME_SECOND * 1000);
            jwtToken.setActiveTime(activeTime);
            jwtTokenService.update(jwtToken);
        }
        /*
        principal：token(包含用户基本信息)
        credentials：凭证
         */
        return new SimpleAuthenticationInfo(jwtToken, token, "jwtRealm");
    }

}
