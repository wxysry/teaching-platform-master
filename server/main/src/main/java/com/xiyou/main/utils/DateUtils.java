package com.xiyou.main.utils;

/**
 * @author 吕辉耀
 * @PROJECT_NAME: teaching-platform-master-new
 * @Description:
 * @creat 2023-06-30 18
 */
public class DateUtils {

    /**
     * 系统时间添加周期
     * @param oldTime//原时间 如 11 ，12,43
     * @param addTime//周期 如 1 2 3 4 5
     * @return
     */
    public static Integer addYearAndSeasonTime(Integer oldTime,Integer addTime){
        oldTime = oldTime/10*4 + oldTime%10;
        int time = oldTime+addTime;
        if(time%4 == 0){
            time = (time/4-1)*10 + 4;
        }else {
            time = time/4*10 + time%4;
        }
        return time;
    }

    /**
     * 系统时间相减
     * @param oldTime//原时间 如 11 ，12,43
     * @param cutTime//周期 如 1 2 3 4 5
     * @return
     */
    public static Integer cutSysYearAndSeasonTime(Integer oldTime,Integer cutTime){
        if(oldTime <= cutTime){
            return 0;
        }

        oldTime = oldTime/10*4 + oldTime%10;
        cutTime = cutTime/10*4 + cutTime%10;
        return (oldTime - cutTime)/4*10 +  (oldTime - cutTime)%4;

    }


    /**
     * 计算时间差
     * @param start
     * @param end
     * @return
     */
    public static Integer calSeasonInterval(Integer start,Integer end){
        Integer startYear = start/10; // 提取开始时间的年份
        Integer startQuarter = start%10; // 提取开始时间的季度
        Integer endYear = end/10; // 提取结束时间的年份
        Integer endQuarter = end%10; // 提取结束时间的季度
        Integer startTotalQuarter = startYear* 4 + startQuarter; // 计算开始时间总共经过的季度数
        Integer endTotalQuarter = endYear * 4 + endQuarter; // 计算结束时间总共经过的季度数
        return endTotalQuarter - startTotalQuarter; // 计算季度间隔
    }


    public static void main(String[] args) {
        Integer integer = addYearAndSeasonTime(21, -1);
        System.out.println(integer);
        System.out.println(calSeasonInterval(11,21));
    }

    /**
     * 将时间转成成对应的中文，例如11转化成第一年第一季度
     * @param date
     * @return
     */
    public static String dateToCN(Integer date){
        return "第"+date/10+"年"+date%10+"季";
    }

}
