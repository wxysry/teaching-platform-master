package com.xiyou.main.vo.contest;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * @program: multi-module
 * @description:
 * @author: tangcan
 * @create: 2019-10-28 14:18
 **/
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class ContestScore {
    private String title;
    private String name;
    private String account;
    private String school;
    private Integer equity;
    private Double score;
}
