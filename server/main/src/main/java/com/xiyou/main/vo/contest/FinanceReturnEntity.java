package com.xiyou.main.vo.contest;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;


/**
 * @author: zhengxiaodong
 * @description: 财务信息相关返回类
 * @since: 2019-07-22 19:52:51
 **/
@Data
@ApiModel(value = "财务信息返回webPPT-03")
public class FinanceReturnEntity {


    @ApiModelProperty(value = "当前现金")
    private Integer currentMoney;

    @ApiModelProperty(value = "应收账款")
    private Integer receivableMoney;

    @ApiModelProperty(value = "长贷总额")
    private Integer longTermLoansMoney;

    @ApiModelProperty(value = "短贷总额")
    private Integer shortTermLoanMoney;

    @ApiModelProperty(value = "贴息")
    private Integer discountMoney;

    @ApiModelProperty(value = "利息")
    private Integer interest;

    @ApiModelProperty(value = "销售收入")
    private Integer salesProfit;

    @ApiModelProperty(value = "维修费")
    private Integer repairCost;

    @ApiModelProperty(value = "转产费")
    private Integer transferCost;

    @ApiModelProperty(value = "租金")
    private Integer rentCost;

    @ApiModelProperty(value = "管理费")
    private Integer manageFee;

    @ApiModelProperty(value = "广告费")
    private Integer adCost;

    @ApiModelProperty(value = "信息费")
    private Integer informationCost;

    @ApiModelProperty(value = "损失")
    private Integer lossCost;

    @ApiModelProperty(value = "直接成本")
    private Integer directCost;

    @ApiModelProperty(value = "ISO认真")
    private Integer ISOCost;

    @ApiModelProperty(value = "产品研发")
    private Integer productDevCost;

    @ApiModelProperty(value = "市场开拓")
    private Integer marketDevCost;


}
