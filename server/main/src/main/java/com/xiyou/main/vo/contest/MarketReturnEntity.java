package com.xiyou.main.vo.contest;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

/**
 * @author: zhengxiaodong
 * @description: 研发认证信息返回
 * @since: 2019-07-23 13:35:05
 **/
@Data
public class MarketReturnEntity {

    @ApiModelProperty(value = "市场准入")
    private List<String> market;

    @ApiModelProperty(value = "生产资格")
    private List<String> product;

    @ApiModelProperty(value = "ISO认证")
    private List<String> iso;


    @ApiModelProperty(value = "已研发时间")
    private Integer nowDate;

    @ApiModelProperty(value = "总研发时间")
    private Integer totalDate;

    @ApiModelProperty(value = "研发完成时间")
    private Integer finishDate;

    @ApiModelProperty(value = "本地")
    private String local;

    @ApiModelProperty(value = "本地研发完成时间")
    private Integer localEndDate;

    @ApiModelProperty(value = "区域")
    private String area;

    @ApiModelProperty(value = "区域研发完成时间")
    private Integer areaEndDate;

    @ApiModelProperty(value = "国内")
    private String home;

    @ApiModelProperty(value = "国内研发完成时间")
    private Integer homeEndDate;

    @ApiModelProperty(value = "亚洲")
    private String asia;

    @ApiModelProperty(value = "亚洲研发完成时间")
    private Integer asiaEndDate;

    @ApiModelProperty(value = "国际")
    private String international;

    @ApiModelProperty(value = "国际研发完成时间")
    private Integer internationalEndDate;

    private String P1;

    private Integer p1EndDate;

    private String P2;

    private Integer p2EndDate;

    private String P3;

    private Integer p3EndDate;

    private String P4;

    private Integer p4EndDate;

    private String P5;

    private Integer p5EndDate;

    private String iso9000;

    private Integer iso9000EndDate;

    private String iso14000;

    private Integer iso14000EndDate;

}
