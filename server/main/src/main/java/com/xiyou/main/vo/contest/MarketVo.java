package com.xiyou.main.vo.contest;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author dingyoumeng
 * @since 2019/09/01
 */
@Data
public class MarketVo {

    private Integer marketId;

    @ApiModelProperty(value = "市场名称")
    private String cmName;

    @ApiModelProperty(value = "研发费用")
    private Integer cmDevelopFee;

    @ApiModelProperty(value = "研发周期")
    private Integer cmDevelopDate;

    @ApiModelProperty(value = "剩余时间")
    private Integer dmRemainDate;

    private Integer dmNowDate;
}
