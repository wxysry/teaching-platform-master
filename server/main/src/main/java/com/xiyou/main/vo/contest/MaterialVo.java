package com.xiyou.main.vo.contest;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * @author dingyoumeng
 * @since 2019/08/29
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class MaterialVo {

    //材料id
    private Integer imCmId;
    //库存数量
    private Integer imNum;
    //材料名字
    private String materialName;
    //紧急采购价格
    private Integer urgentPrice;

    private Integer materialId;

    private Double sellPrice;

    // 直接成本
    private Integer directCost;


}
