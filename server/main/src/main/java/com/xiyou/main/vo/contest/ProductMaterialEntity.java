package com.xiyou.main.vo.contest;

import lombok.Data;

/**
 * @author: zhengxiaodong
 * @description: 库存采购信息返回实体类
 * @since: 2019-07-23 15:04:37
 **/
@Data
public class ProductMaterialEntity {

    // 第一行
    private Integer KP1;
    private Integer KP2;
    private Integer KP3;
    private Integer KP4;
    private Integer KP5;

    // 第二行
    private Integer KR1;
    private Integer KR2;
    private Integer KR3;
    private Integer KR4;
    private Integer KR5;

    // 黑色车
    private Integer BR1;
    private Integer BR2;
    private Integer BR3;
    private Integer BR4;
    private Integer BR5;

    // 灰色车
    private Integer GR1;
    private Integer GR2;
    private Integer GR3;
    private Integer GR4;
    private Integer GR5;

}
