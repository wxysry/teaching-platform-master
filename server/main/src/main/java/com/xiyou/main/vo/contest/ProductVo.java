package com.xiyou.main.vo.contest;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * @author dingyoumeng
 * @since 2019/08/29
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class ProductVo {

    private Integer kcProductId;

    private Integer ipCpId;

    private Integer ipNum;

    private String productName;

    // 成本
    private Integer directCost;

    private Integer urgentPrice;

    private Double sellPrice;

}
