package com.xiyou.main.vo.contest;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * @program: multi-module
 * @description:
 * @author: tangcan
 * @create: 2019-08-27 11:34
 **/
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class SubjectVo {
    /*
    题库号subjectNumber
     */
    private Integer id;
    private String name;
}
