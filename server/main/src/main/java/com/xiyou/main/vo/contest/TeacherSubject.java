package com.xiyou.main.vo.contest;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.util.List;

/**
 * @program: multi-module
 * @description: 教师的所有题库
 * @author: tangcan
 * @create: 2019-08-27 11:08
 **/
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class TeacherSubject {
    private Integer id;
    private String name;
    private List<SubjectVo> subjects;
}
