package com.xiyou.main.vo.contest;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * @program: multi-module
 * @description: 用户广告额：用于订货会
 * @author: tangcan
 * @create: 2019-08-29 13:33
 **/
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class UserAdVO {
    @ApiModelProperty(value = "选单id")
    private Integer chooseId;

    @ApiModelProperty(value = "用户")
    private String user;

    @ApiModelProperty(value = "产品广告")
    private Integer productAd;

    @ApiModelProperty(value = "市场广告")
    private Integer marketAd;

    @ApiModelProperty(value = "次数")
    private Integer number;

    @ApiModelProperty(value = "该用户是否是自己")
    private Integer isMe;

    @ApiModelProperty(value = "上一年销售额")
    private Integer sales;
}
