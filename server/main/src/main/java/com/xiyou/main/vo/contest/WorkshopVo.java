package com.xiyou.main.vo.contest;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * @author dingyoumeng
 * @since 2019/07/25
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class WorkshopVo {

    private Integer workshopId;

    private String cwName;

    private Integer cwCapacity;


    private Integer wStatus;

    private Integer wSurplusCapacity;

    private Integer wPayDate;
    private Integer cwBuyFee;
    private Integer cwRentFee;

}
