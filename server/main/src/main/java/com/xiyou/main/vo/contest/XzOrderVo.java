package com.xiyou.main.vo.contest;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * @author dingyoumeng
 * @since 2019/07/25
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class XzOrderVo {

    private Integer orderId;

    private String coId;

    private Integer isoId;

    private String cpName;

    private String ciName;

    private String marketName;

    private Integer num;

    private Integer date;

    private Integer totalPrice;

    private Integer paymentDate;

    private Integer deliveryDate;
}
