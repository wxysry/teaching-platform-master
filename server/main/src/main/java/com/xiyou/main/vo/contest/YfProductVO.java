package com.xiyou.main.vo.contest;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * @author dingyoumeng
 * @since 2019/07/26
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class YfProductVO {

    private Integer cpId;

    private String cpName;

    private Integer cpProcessingFee;

    private Integer cpDevelopDate;

    private Integer dpRemainDate;

    private Integer dpFinishDate;
}
