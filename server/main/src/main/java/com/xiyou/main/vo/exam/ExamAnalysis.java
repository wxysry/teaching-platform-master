package com.xiyou.main.vo.exam;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * @program: multi-module
 * @description: 考试分析
 * @author: tangcan
 * @create: 2019-07-18 23:43
 **/
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class ExamAnalysis {
    private Integer problemId;
    private Double score;
    private Double avgScore;
    private Double studentScoreSum;
    private String title;
    private String intro;
    private String typeName;
    private String groupName;
}
