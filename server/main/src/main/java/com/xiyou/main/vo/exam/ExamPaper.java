package com.xiyou.main.vo.exam;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.time.LocalDateTime;

/**
 * @program: multi-module
 * @description: 考试试卷
 * @author: tangcan
 * @create: 2019-07-10 21:34
 **/
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class ExamPaper {
    @ApiModelProperty(value = "学生的考试的id")
    private Integer id;

    @ApiModelProperty(value = "试卷id")
    private Integer paperId;

    @ApiModelProperty(value = "考生id")
    private Integer studentId;

    @ApiModelProperty(value = "教师id")
    private Integer teacherId;

    @ApiModelProperty(value = "教师")
    private String teacherName;

    @ApiModelProperty(value = "考试成绩")
    private Integer grade;

    @ApiModelProperty(value = "考试时间（分钟）")
    private long examTime;

    @ApiModelProperty(value = "剩余时间（秒）")
    private long restTime;

    @ApiModelProperty(value = "是否已交卷,0表示未交卷，1表示已交卷")
    private Integer isHandIn;

    @ApiModelProperty(value = "试题总数")
    private Integer problemNum;

    @ApiModelProperty(value = "已批卷题数")
    private Integer correctProblemNumber;

    @ApiModelProperty(value = "批卷完成时间，为空表示未完成")
    private LocalDateTime correctCompleteTime;

    private LocalDateTime createTime;

    @ApiModelProperty(value = "试卷标题")
    private String title;

    @ApiModelProperty(value = "试卷封面图")
    private String cover;

    @ApiModelProperty(value = "试卷开放起始时间")
    private LocalDateTime openTimeStart;

    @ApiModelProperty(value = "试卷开放结束时间")
    private LocalDateTime openTimeEnd;

    @ApiModelProperty(value = "及格分数")
    private Double passGrade;

    @ApiModelProperty(value = "试卷总分")
    private Double totalScore;

    @ApiModelProperty(value = "是否发布试卷")
    private Integer isPublish;

    @ApiModelProperty(value = "考试说明")
    private String examExplain;

    @ApiModelProperty(value = "阅卷完成后，是否向学生公开答案，0表示不公开，1表示公开")
    private Integer openAnswer;

    @ApiModelProperty(value = "阅卷完成后，学生查看试题范围，0表示只查看错题，1表示查看全部")
    private Integer showAllProblem;

    @ApiModelProperty(value = "试卷状态")
    private Integer paper;

    @ApiModelProperty(value = "考试状态")
    private Integer exam;
}
