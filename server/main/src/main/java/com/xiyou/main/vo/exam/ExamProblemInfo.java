package com.xiyou.main.vo.exam;

import com.xiyou.main.entity.exam.ProblemOption;
import com.xiyou.main.entity.exam.Resources;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.hibernate.validator.constraints.Length;

import java.time.LocalDateTime;
import java.util.List;

/**
 * @program: multi-module
 * @description:
 * @author: tangcan
 * @create: 2019-07-11 16:29
 **/
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value = "试题信息")
public class ExamProblemInfo {
    @ApiModelProperty(value = "试题id")
    private Integer problemId;

    @ApiModelProperty(value = "试卷id")
    private Integer paperId;

    @ApiModelProperty(value = "是否自动判题")
    private Integer autoJudge;

    @ApiModelProperty(value = "是否是选择题")
    private Integer isChoice;

    @ApiModelProperty(value = "题型id")
    private Integer typeId;

    @ApiModelProperty(value = "题型名称")
    private String typeName;

    @ApiModelProperty(value = "分组名称")
    private String groupName;

    @ApiModelProperty(value = "题目")
    private String title;

    @ApiModelProperty(value = "题目简介")
    @Length(max = 100, message = "简介长度不能超过{max}")
    private String intro;

    @ApiModelProperty(value = "选项")
    private List<ProblemOption> optionList;

    @ApiModelProperty(value = "答案")
    private String answer;

    @ApiModelProperty(value = "学生答案")
    private String studentAnswer;

    @ApiModelProperty(value = "本题分数")
    private Double score;

    @ApiModelProperty(value = "学生分数")
    private Double studentScore;

    @ApiModelProperty(value = "解析")
    private String analysis;

    @ApiModelProperty(value = "评分要点")
    private String scorePoint;

    @ApiModelProperty(value = "图片")
    private String pic;

    @ApiModelProperty(value = "附件")
    private String attachment;

    @ApiModelProperty(value = "图片")
    private List<Resources> picList;

    @ApiModelProperty(value = "附件")
    private List<Resources> attachmentList;

    @ApiModelProperty(value = "创建时间")
    private LocalDateTime createTime;

    @ApiModelProperty(value = "交单情况表的订单号")
    private String orderNum;
}
