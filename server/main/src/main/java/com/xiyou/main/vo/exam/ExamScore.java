package com.xiyou.main.vo.exam;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * @program: multi-module
 * @description:
 * @author: tangcan
 * @create: 2019-10-28 14:18
 **/
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class ExamScore {
    private String title;
    private String name;
    private String account;
    private String school;
    private Double score;
}
