package com.xiyou.main.vo.exam;

import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.time.LocalDateTime;

/**
 * @program: multi-module
 * @description:
 * @author: tangcan
 * @create: 2019-07-18 10:13
 **/
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value = "考生信息")
public class ExamStudent {
    private Integer examId;
    private Integer paperId;
    private Integer studentId;
    private Integer teacherId;
    private Integer examTime;
    private Integer isHandIn;
    private Integer problemNum;
    private LocalDateTime correctCompleteTime;
    private Double passGrade;
    private Integer totalScore;
    private String studentName;
    private String account;
    private Double grade;
    private int status;
}
