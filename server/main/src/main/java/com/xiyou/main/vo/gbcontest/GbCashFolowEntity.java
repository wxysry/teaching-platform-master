package com.xiyou.main.vo.gbcontest;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotNull;

/**
 * @author: wangxingyu
 * @description: 财务信息
 * @since: 2023-06-11
 **/
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value = "财务信息PPT-03")
public class GbCashFolowEntity {

    @ApiModelProperty(value = "学生ID")
    private Integer studentId;

    @ApiModelProperty(value = "竞赛ID")
    @NotNull(message = "竞赛ID不能为空")
    private Integer contestId;

    @ApiModelProperty(value = "当前时间")
    @NotNull(message = "当前时间不能为空")
    private Integer currentTime;

    @ApiModelProperty(value = "贷款类型:1表示长贷，2表示短贷")
    private Integer blType;

    @ApiModelProperty(value = "关键字")
    private String keyword;

    @ApiModelProperty(value = "当前年份")
    private Integer currentYear;

    @ApiModelProperty(value = "上一年")
    private Integer lastYear;

    @ApiModelProperty(value = "贷款时长")
    private Integer loanTerm;

    @ApiModelProperty(value = "贷款金额")
    @NotNull(message = "贷款金额不能为空")
    private Integer loanAmount;

    @ApiModelProperty(value = "最大贷款额度")
    private Integer maxLoanAmount;

    @ApiModelProperty(value = "付现金额")
    private Integer payCashMoney;

    @ApiModelProperty(value = "厂房cwid")
    private Integer cwid;

    @ApiModelProperty(value = "订购方式[0:买，1:租]")
    private Integer buyPattern;

    @ApiModelProperty(value = "生产线名称")
    private String CPLName;

    @ApiModelProperty(value = "产品名称")
    private String CPName;

    @ApiModelProperty(value = "产品ID")
    private Integer CPId;

    @ApiModelProperty(value = "生产线类型id")
    private Integer cplId;

    @ApiModelProperty(value = "贷款规则表id")
    private Integer gzLoanId;

}
