package com.xiyou.main.vo.gbcontest;

import lombok.Data;

/**
 * @author 吕辉耀
 * @PROJECT_NAME: teaching-platform-master-new
 * @Description:
 * @creat 2023-07-06 16
 */
@Data
public class GbCtKcMaterialNumVo {
    private Integer materialNum;//材料编号
    private Integer num;//数量
}
