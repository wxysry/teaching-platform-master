package com.xiyou.main.vo.gbcontest;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;


/**
 * @author wangxingyu
 * @since 2023-06-11
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class GbCtMnAdVo {

    @ApiModelProperty(value = "广告ID")
    private Integer id;

    @ApiModelProperty(value = "学生id")
    private Integer studentId;

    @ApiModelProperty(value = "考试id")
    private Integer contestId;

    @ApiModelProperty(value = "账号")
    private String groupNum;

    @ApiModelProperty(value = "广告投放时间")
    private LocalDateTime createTime;


    @ApiModelProperty(value = "本地P1")
    private Double localP1;

    @ApiModelProperty(value = "区域P1")
    private Double regionalP1;

    @ApiModelProperty(value = "国内P1")
    private Double nationalP1;

    @ApiModelProperty(value = "亚洲P1")
    private Double asianP1;

    @ApiModelProperty(value = "国际P1")
    private Double internationalP1;

    @ApiModelProperty(value = "本地P2")
    private Double localP2;

    @ApiModelProperty(value = "区域P2")
    private Double regionalP2;

    @ApiModelProperty(value = "国内P2")
    private Double nationalP2;

    @ApiModelProperty(value = "亚洲P2")
    private Double asianP2;

    @ApiModelProperty(value = "国际P2")
    private Double internationalP2;

    @ApiModelProperty(value = "本地P3")
    private Double localP3;

    @ApiModelProperty(value = "区域P3")
    private Double regionalP3;

    @ApiModelProperty(value = "国内P3")
    private Double nationalP3;

    @ApiModelProperty(value = "亚洲P3")
    private Double asianP3;

    @ApiModelProperty(value = "国际P3")
    private Double internationalP3;

    @ApiModelProperty(value = "本地P4")
    private Double localP4;

    @ApiModelProperty(value = "区域P4")
    private Double regionalP4;

    @ApiModelProperty(value = "国内P4")
    private Double nationalP4;

    @ApiModelProperty(value = "亚洲P4")
    private Double asianP4;

    @ApiModelProperty(value = "国际P4")
    private Double internationalP4;

    @ApiModelProperty(value = "本地P5")
    private Double localP5;

    @ApiModelProperty(value = "区域P5")
    private Double regionalP5;

    @ApiModelProperty(value = "国内P5")
    private Double nationalP5;

    @ApiModelProperty(value = "亚洲P5")
    private Double asianP5;

    @ApiModelProperty(value = "国际P5")
    private Double internationalP5;





}
