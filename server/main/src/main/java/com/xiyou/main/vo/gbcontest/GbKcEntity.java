package com.xiyou.main.vo.gbcontest;

import lombok.Data;

@Data
public class GbKcEntity {
    private String name;
    private String num;
}
