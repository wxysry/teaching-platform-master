package com.xiyou.main.vo.gbcontest;

import lombok.Data;

@Data
public class GbMarketForecast {
    private String date;
    private String product;
    private String local;
    private String regional;
    private String national;
    private String asian;
    private String international;
}
