package com.xiyou.main.vo.gbcontest;

import com.xiyou.main.entity.gbcontest.GbCtYfIso;
import com.xiyou.main.entity.gbcontest.GbCtYfMarket;
import com.xiyou.main.entity.gbcontest.GbCtYfProduct;
import com.xiyou.main.entity.newcontest.NewCtYfIso;
import com.xiyou.main.entity.newcontest.NewCtYfMarket;
import com.xiyou.main.entity.newcontest.NewCtYfProduct;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

/**
 * @author wangxingyu
 * @since 2023-06-11
 * @description: 研发认证信息返回
 **/
@Data
public class GbMarketReturnEntity {

    @ApiModelProperty(value = "市场准入")
    private List<GbCtYfMarket> market;

    @ApiModelProperty(value = "生产资格")
    private List<GbCtYfProduct> product;

    @ApiModelProperty(value = "ISO认证")
    private List<GbCtYfIso> iso;
}
