package com.xiyou.main.vo.gbcontest;

import com.xiyou.main.vo.newcontest.KcEntity;
import lombok.Data;

import java.util.List;

/**
 * @author: zhengxiaodong
 * @description: 库存采购信息返回实体类
 * @since: 2019-07-23 15:04:37
 **/
@Data
public class GbProductMaterialEntity {

    //产品库存
    List<GbKcEntity> KP;
    //原材料库存
    List<GbKcEntity> KR;
    //黑车 剩余时间为0的数量
    List<GbKcEntity> BR;
    //灰车  剩余时间为1 的数量
    List<GbKcEntity> GR;

}
