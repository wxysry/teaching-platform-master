package com.xiyou.main.vo.gbcontest;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * @author wangxingyu
 * @since 2023-06-11
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class GbWorkshopVo {

    private Integer workshopId;

    private String cwName;

    private Integer cwCapacity;


    private Integer wStatus;

    private Integer wSurplusCapacity;

    private Integer wPayDate;
    private Integer cwBuyFee;
    private Integer cwRentFee;

}
