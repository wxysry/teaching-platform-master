package com.xiyou.main.vo.gbcontest;

import com.xiyou.main.entity.gbcontest.GbCtXzOrder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;


/**
 * @author wangxingyu
 * @since 2023-06-11
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class GbXzOrderVo extends GbCtXzOrder {


}
