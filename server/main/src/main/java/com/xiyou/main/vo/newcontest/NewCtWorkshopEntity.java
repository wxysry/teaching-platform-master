package com.xiyou.main.vo.newcontest;

import com.xiyou.main.entity.contest.CtLine;
import lombok.Data;

import java.util.List;

/**
 * @author wangxingyu
 * @since 2023-06-11
 * @description: workshop相关
 **/
@Data
public class NewCtWorkshopEntity {

    /*
    厂房id
     */
    private Integer workshopId;

    /*
     * 厂房编号
     */
    private Integer cwId;

    /*
    租售类型:0表示BUY,1表示RENT
     */
    private Integer wStatus;

    /*
    剩余容量
     */
    private Integer wSurplusCapacity;

    /*
    最后付租
     */
    private Integer wPayDate;

    /*
     * 厂房名称
     */
    private String cwName;

    /*
    购买费用
     */
    private Integer cwBuyFee;

    /*
    租金
     */
    private Integer cwRentFee;

    /*
    售价
     */
    private Integer cwSellFee;

    /*
    生产线容量
     */
    private Integer cwCapacity;

    /*
    置办时间
     */
    private Integer getDate;


    /*
    所有生产线
     */
    List<CtLine> lineList;
}
