package com.xiyou.main.vo.newcontest;

import com.xiyou.main.entity.newcontest.NewCtYfIso;
import com.xiyou.main.entity.newcontest.NewCtYfMarket;
import com.xiyou.main.entity.newcontest.NewCtYfProduct;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

/**
 * @author wangxingyu
 * @since 2023-06-11
 * @description: 研发认证信息返回
 **/
@Data
public class NewMarketReturnEntity {

    @ApiModelProperty(value = "市场准入")
    private List<NewCtYfMarket> market;

    @ApiModelProperty(value = "生产资格")
    private List<NewCtYfProduct> product;

    @ApiModelProperty(value = "ISO认证")
    private List<NewCtYfIso> iso;
}
