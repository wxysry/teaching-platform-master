package com.xiyou.main.vo.newcontest;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author wangxingyu
 * @since 2023-06-11
 */
@Data
public class NewMarketVo {

    private Integer marketId;

    @ApiModelProperty(value = "市场名称")
    private String cmName;

    @ApiModelProperty(value = "研发费用")
    private Integer cmDevelopFee;

    @ApiModelProperty(value = "研发周期")
    private Integer cmDevelopDate;

    @ApiModelProperty(value = "剩余时间")
    private Integer dmRemainDate;

    @ApiModelProperty(value = "申请时间")
    private Integer dmStartDate;

    @ApiModelProperty(value = "申请时间")
    private Integer dmFinishDate;

    @ApiModelProperty(value = "状态")
    private String dmState;

    private Integer dmNowDate;
}
