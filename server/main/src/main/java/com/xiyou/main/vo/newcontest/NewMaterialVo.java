package com.xiyou.main.vo.newcontest;

import io.swagger.annotations.ApiModelProperty;
import io.swagger.models.auth.In;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * @author wangxingyu
 * @since 2023-06-11
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class NewMaterialVo {

    private Integer cmId;

    private String cnName;

    private Integer cmLeadDate;//采购周期

    private Integer cmBuyFee;//费用

    private Integer cmPayDate;//付款周期

    private Integer num;//数量

    private Integer contestId;

    private Integer currentTime;
}
