package com.xiyou.main.vo.newcontest;

import lombok.Data;

import java.util.List;

/**
 * @author: zhengxiaodong
 * @description: 库存采购信息返回实体类
 * @since: 2019-07-23 15:04:37
 **/
@Data
public class NewProductMaterialEntity {

    //产品库存
    List<KcEntity> KP;
    //原材料库存
    List<KcEntity> KR;
    //黑车 剩余时间为0的数量
    List<KcEntity> BR;
    //灰车  剩余时间为1 的数量
    List<KcEntity> GR;

}
