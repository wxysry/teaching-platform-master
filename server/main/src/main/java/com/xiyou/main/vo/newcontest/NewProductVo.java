package com.xiyou.main.vo.newcontest;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * @author wangxingyu
 * @since 2023-06-11
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class NewProductVo {

    private Integer kcProductId;

    private Integer ipCpId;

    private Integer ipNum;

    private String productName;

    // 成本
    private Integer directCost;

    private Integer urgentPrice;

    private Double sellPrice;

}
