package com.xiyou.main.vo.newcontest;


import com.xiyou.main.entity.newcontest.NewCtXzOrder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * @author wangxingyu
 * @since 2023-06-11
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class NewXzOrderVo extends NewCtXzOrder {

}
