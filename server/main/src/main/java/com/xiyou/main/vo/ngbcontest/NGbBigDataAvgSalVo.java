package com.xiyou.main.vo.ngbcontest;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;


/**
 * @author wangxingyu
 * @since 2023-06-11
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class NGbBigDataAvgSalVo {

    @ApiModelProperty(value = "学生id")
    private Integer studentId;

    @ApiModelProperty(value = "账号")
    private String groupNum;

    @ApiModelProperty(value = "总工资")
    private Integer salTotal;

    @ApiModelProperty(value = "工人数量")
    private Integer workerNum;

    @ApiModelProperty(value = "平均工资")
    private Integer avgSal;

}
