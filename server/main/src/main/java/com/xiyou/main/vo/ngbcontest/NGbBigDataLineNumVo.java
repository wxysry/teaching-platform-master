package com.xiyou.main.vo.ngbcontest;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;


/**
 * @author wangxingyu
 * @since 2023-06-11
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class NGbBigDataLineNumVo {

    @ApiModelProperty(value = "学生id")
    private Integer studentId;

    @ApiModelProperty(value = "账号")
    private String groupNum;

    @ApiModelProperty(value = "生产线数量")
    private Integer lineNum;


}
