package com.xiyou.main.vo.ngbcontest;


import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;
import java.math.BigDecimal;

/**
 * <p>
 *
 * </p>
 *
 * @author wangxingyu
 * @since 2024-02-18
 */
@Data
@Accessors(chain = true)
public class NGbCtBudgetVo {

    @ApiModelProperty(value = "部门")
    private String department;

    @ApiModelProperty(value = "类型 1、市场营销部 2、生产设计 3、人力资源")
    private String type;

    @ApiModelProperty(value = "上季度预算")
    private Integer lastSeasonBudget;

    @ApiModelProperty(value = "上季度使用")
    private Integer lastSeasonUsed;

    @ApiModelProperty(value = "上季度使用率")
    private BigDecimal lastSeasonUsedRate;


    @ApiModelProperty(value = "本季度预算")
    private Integer initBudget;

    @ApiModelProperty(value = "本季度预算")
    private Integer budget;

    @ApiModelProperty(value = "本季度使用")
    private Integer used;

    @ApiModelProperty(value = "本季度使用率")
    private BigDecimal usedRate;


}
