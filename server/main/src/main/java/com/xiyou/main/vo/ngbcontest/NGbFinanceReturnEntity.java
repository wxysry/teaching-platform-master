package com.xiyou.main.vo.ngbcontest;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Range;

import javax.validation.constraints.NotNull;


/**
 * @author wangxingyu
 * @since 2023-06-11
 * @description: 财务信息相关返回类
 **/
@Data
@ApiModel(value = "财务信息返回webPPT-03")
public class NGbFinanceReturnEntity {


    @ApiModelProperty(value = "当前现金")
    private Integer currentMoney;

    @ApiModelProperty(value = "应收账款")
    private Integer receivableMoney;

    @ApiModelProperty(value = "长贷总额")
    private Integer longTermLoansMoney;

    @ApiModelProperty(value = "短贷总额")
    private Integer shortTermLoanMoney;

    @ApiModelProperty(value = "贴息")
    private Integer discountMoney;

    @ApiModelProperty(value = "利息")
    private Integer interest;

    @ApiModelProperty(value = "销售收入")
    private Integer salesProfit;

    @ApiModelProperty(value = "维修费")
    private Integer repairCost;

    @ApiModelProperty(value = "转产费")
    private Integer transferCost;


    @ApiModelProperty(value = "管理费")
    private Integer manageFee;

    @ApiModelProperty(value = "广告费")
    private Integer adCost;

    @ApiModelProperty(value = "信息费")
    private Integer informationCost;


    @ApiModelProperty(value = "直接成本")
    private Integer directCost;

    @ApiModelProperty(value = "ISO认真")
    private Integer ISOCost;

    @ApiModelProperty(value = "产品研发")
    private Integer productDevCost;

    @ApiModelProperty(value = "市场开拓")
    private Integer marketDevCost;

    @ApiModelProperty(value = "数字化研发")
    private Integer digitalDevCost;


    @ApiModelProperty(value = "产品设计费")
    private Integer productDesign;

    @ApiModelProperty(value = "辞退福利")
    private Integer dismissFee;

    @ApiModelProperty(value = "培训费")
    private Integer trainFee;


    @ApiModelProperty(value = "激励费")
    private Integer incentiveFee;

    @ApiModelProperty(value = "人力费")
    private Integer hr;


    @ApiModelProperty(value = "碳中和费")
    private Integer carbon;


    @ApiModelProperty(value = "特性研发")
    private Integer feature;


    @ApiModelProperty(value = "实时权益")
    private Integer ssqy;


}
