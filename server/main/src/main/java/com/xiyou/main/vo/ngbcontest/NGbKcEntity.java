package com.xiyou.main.vo.ngbcontest;

import lombok.Data;

@Data
public class NGbKcEntity {
    private String name;
    private String num;
}
