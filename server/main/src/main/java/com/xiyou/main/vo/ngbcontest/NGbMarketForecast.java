package com.xiyou.main.vo.ngbcontest;

import lombok.Data;

@Data
public class NGbMarketForecast {

    /**
     * 时间
     */
    private String date;

    /**
     * 市场名
     */
    private String cmName;


    /**
     * 产品名
     */
    private String cpName;


    /**
     * 特征名
     */
    private String featureName;


    /**
     * 总量
     */
    private String zl;

    /**
     * 总价
     */
    private String zj;

    /**
     * 平均价格
     */
    private String pjjj;

}
