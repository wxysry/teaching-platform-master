package com.xiyou.main.vo.ngbcontest;

import com.xiyou.main.entity.gbcontest.GbCtYfIso;
import com.xiyou.main.entity.gbcontest.GbCtYfMarket;
import com.xiyou.main.entity.gbcontest.GbCtYfProduct;
import com.xiyou.main.entity.ngbcontest.NGbCtYfIso;
import com.xiyou.main.entity.ngbcontest.NGbCtYfMarket;
import com.xiyou.main.entity.ngbcontest.NGbCtYfProduct;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

/**
 * @author wangxingyu
 * @since 2023-06-11
 * @description: 研发认证信息返回
 **/
@Data
public class NGbMarketReturnEntity {

    @ApiModelProperty(value = "市场准入")
    private List<NGbCtYfMarket> market;

    @ApiModelProperty(value = "生产资格")
    private List<NGbCtYfProduct> product;

    @ApiModelProperty(value = "ISO认证")
    private List<NGbCtYfIso> iso;
}
