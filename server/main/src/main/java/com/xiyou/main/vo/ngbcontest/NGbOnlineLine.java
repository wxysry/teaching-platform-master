package com.xiyou.main.vo.ngbcontest;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * @author dingyoumeng
 * @since 2019/07/22
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class NGbOnlineLine {

    private Integer lineId;

    private Integer workshopId;

    private Integer plCplid;

    private String cwName;

    private String cplName;

    private String cpName;

    private Integer plInvest;

    private Integer plDepTotal;

    private Integer plRemainDate;

    private Integer plAddTime;

    private Integer cplTransferDate;

    private Integer cplTransferFee;

    private Integer plFinishDate;
}
