package com.xiyou.main.vo.ngbcontest;

import lombok.Data;

import java.util.List;

/**
 * @author: zhengxiaodong
 * @description: 库存采购信息返回实体类
 * @since: 2019-07-23 15:04:37
 **/
@Data
public class NGbProductMaterialEntity {

    //产品库存
    List<NGbKcEntity> KP;
    //原材料库存
    List<NGbKcEntity> KR;
    //黑车 剩余时间为0的数量
    List<NGbKcEntity> BR;
    //灰车  剩余时间为1 的数量
    List<NGbKcEntity> GR;

}
