package com.xiyou.main.vo.ngbcontest;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * @program: multi-module
 * @description:
 * @author: wangxingyu
 * @create: 2023-06-11 11:34
 **/
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class NGbSubjectVo {
    /*
    题库号subjectNumber
     */
    private Integer id;
    private String name;
}
