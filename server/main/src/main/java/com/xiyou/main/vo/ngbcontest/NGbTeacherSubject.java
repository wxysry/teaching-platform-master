package com.xiyou.main.vo.ngbcontest;

import com.xiyou.main.vo.contest.SubjectVo;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.util.List;

/**
 * @program: multi-module
 * @description: 教师的所有题库
 * @author: wangxingyu
 * @create: 2023-06-11 11:08
 **/
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class NGbTeacherSubject {
    private Integer id;
    private String name;
    private List<SubjectVo> subjects;
}
