package com.xiyou.main.vo.ngbcontest;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * @program: multi-module
 * @description: 用户广告额：用于订货会
 * @author: wangxingyu
 * @create: 2023-06-27 13:33
 **/
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class NGbUserAdVO {
    @ApiModelProperty(value = "选单id")
    private Integer chooseId;

    @ApiModelProperty(value = "学生id")
    private Integer studentId;

    @ApiModelProperty(value = "用户")
    private String user;

    @ApiModelProperty(value = "产品广告")
    private Integer productAd;

    @ApiModelProperty(value = "市场广告")
    private Integer marketAd;

    @ApiModelProperty(value = "剩余选单次数")
    private Integer number;

    @ApiModelProperty(value = "该用户是否是自己")
    private Integer isMe;

    @ApiModelProperty(value = "上一年销售额")
    private Integer sales;

    @ApiModelProperty(value = "本轮选单次数,0或1")
    private Integer chooseNum;



}
