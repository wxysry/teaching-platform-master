package com.xiyou.main.vo.ngbcontest;

import com.xiyou.main.entity.ngbcontest.NGbCtXzOrder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;


/**
 * @author wangxingyu
 * @since 2023-06-11
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class NGbXzOrderVo extends NGbCtXzOrder {


}
