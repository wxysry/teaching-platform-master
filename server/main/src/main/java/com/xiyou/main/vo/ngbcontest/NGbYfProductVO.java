package com.xiyou.main.vo.ngbcontest;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * @author wangxingyu
 * @since 2023/06/11
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class NGbYfProductVO {

    private Integer cpId;

    private String cpName;

    private Integer cpProcessingFee;

    private Integer cpDevelopDate;

    private Integer dpRemainDate;

    private Integer dpFinishDate;

    private Integer dpStartDate;

    private String  dpState;
}
