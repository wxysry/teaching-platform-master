import request from '@/utils/request'

/**
 * 禁止用户登录
 * @param params
 */
export function forbiddenUserLogin(params) {
  return request({
    url: '/tp/sysUser/forbid',
    method: 'get',
    params: params
  })
}

/**
 * 删除用户
 * @param params
 */
export function deleteUserInfo(params) {
  return request({
    url: '/tp/sysUser/delete',
    method: 'get',
    params: params
  })
}

/**
 * 使用账号密码进行登录
 * 参数如下：
 * account 账号
 * code 验证码
 * password 密码
 * @param data
 */
export function login(data) {
  return request({
    url: '/tp/sysUser/login',
    method: 'post',
    data: data
  })
}

export function logout() {
  return request({
    url: '/tp/sysUser/logout',
    method: 'get',
  })
}

/**
 * 获取验证码
 */
export function getCodeImgs() {
  return request({
    url: '/tp/captcha/get',
    method: 'get'
  })
}

export function getUserInfo() {
  return request({
    url: '/tp/sysUser/roles/get',
    method: 'get'
  })
}
