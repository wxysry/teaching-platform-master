import request from '@/utils/request'

/**
 * 获取财务信息
 * @param params
 */
export function getFinancialInfo(params) {
  return request({
    url: "/tp/gbFinance/statistical/info",
    method: 'get',
    params: params
  })
}

/**
 * 获取研发认证信息
 * @param params
 */
export function getResearchAndAuthInfo(params) {
  return request({
    url: "/tp/gbFinance/researchAndAuth/info",
    method: 'get',
    params: params
  })
}

/**
 * 获取库存信息
 * @param params
 */
export function GetStockInfo(params) {
  return request({
    url: '/tp/gbFinance/stockPurchase/info',
    method: 'get',
    params: params
  })
}




/**
 * 根据竞赛ID获取融资规则
 * @param params
 */
export function getGzLoanByContestId(params) {
  return request({
    url: '/tp/gbFinance/loan/gz/info',
    method: 'get',
    params: params
  })
}


/**
 * 获取融资现状
 * @param params
 */
export function getBankloan(params) {
  return request({
    url: '/tp/gbFinance/loan/bank/info',
    method: 'get',
    params: params
  })
}



/**
 * 获取贷款额度
 * @param params
 */
export function getLoanLimit(params) {
  return request({
    url: '/tp/gbFinance/loan/info',
    method: 'get',
    params: params
  })
}

/**
 * 确认贷款
 * @param params
 */
export function confirmLoan(params) {
  return request({
    url: "/tp/gbFinance/loan/commit",
    method: 'post',
    data: params
  })
}






/**
 * 原材料-下单
 * @param params
 */
export function confirmOrderMaterial(params) {
  return request({
    url: '/tp/gbFinance/material/confirmOrder',
    method: 'post',
    data: params
  })
}


/**
 * 原材料-订单收货
 * @param params
 */
export function receiveMaterialGoods(params) {
  return request({
    url: '/tp/gbFinance/material/to/inventory',
    method: 'get',
    params: params
  })
}



/**
 * 获取班次规则
 * @param params
 */
export function getGzClasses(params) {
  return request({
    url: '/tp/gbFinance/classes/list',
    method: 'get',
    params: params
  })
}

/**
 * 获取原料市场
 * @param params
 */
export function getMaterialShop(params) {
  return request({
    url: '/tp/gbFinance/material/shop',
    method: 'get',
    params: params
  })
}


/**
 * 获取原料订单
 * @param params
 */
export function getMaterialOrder(params) {
  return request({
    url: '/tp/gbFinance/material/order',
    method: 'get',
    params: params
  })
}


/**
 * 获取原料库存
 * @param params
 */
export function getMaterialInventory(params) {
  return request({
    url: '/tp/gbFinance/material/inventory',
    method: 'get',
    params: params
  })
}



/**
 * 获取产品库存
 * @param params
 */
export function getProductInventory(params) {
  return request({
    url: '/tp/gbFinance/product/inventory',
    method: 'get',
    params: params
  })
}







/**
 * 获取人力资源市场
 * @param params
 */
export function getWokerMarket(params) {
  return request({
    url: '/tp/gbFinance/wokerMarket/list',
    method: 'get',
    params: params
  })
}


/**
 * 发送OFFER
 * @param params
 */
export function sendOffer(params) {
  return request({
    url: '/tp/gbFinance/sendOffer',
    method: 'post',
    data: params
  })
}



/**
 * 获取厂房信息
 * @param params
 */
export function getWorkShopList(params) {
  return request({
    url: '/tp/gbFinance/workshop/info',
    method: 'get',
    params: params
  })
}



/**
 * 获取新建生产线基本信息
 * @param params
 */
export function getNewLineInfo(params) {
  return request({
    url: '/tp/gbContest/line/info',
    method: 'get',
    params: params
  })
}

/**
 * 确认新建生产线
 * @param params
 */
export function confirmNewLineInfo(params) {
  return request({
    url: '/tp/gbContest/line/add',
    method: 'post',
    data: params
  })
}

/**
 * 学生获取竞赛模拟列表-
 * @param params
 */
export function getContestListByStudent(params) {
  return request({
    url: "/tp/gbContest/list/student",
    method: 'post',
    data: params
  })
}

/**
 * 开始经营
 * @param params
 */
export function startContestByStudent(params) {
  return request({
    url: '/tp/gbBusiness/start',
    method: 'get',
    params: params
  })
}

/**
 * 获取当前竞赛进度
 * @param params
 */
export function getContestProcess(params) {
  return request({
    url: '/tp/gbBusiness/progress/get',
    method: 'get',
    params: params
  })
}

/**
 * 保存经营进度
 * @param params
 */
export function saveContestProcess(params) {
  return request({
    url: '/tp/gbBusiness/progress/save',
    method: 'get',
    params: params
  })
}



/**
 * 当年结束开始下一个季度
 * @param params
 */
export function nextCurrentSeason(params) {
  return request({
    url: "/tp/gbYearEnd/nextSeason",
    method: 'get',
    params: params
  })
}



/**
 * 获取费用管理数据
 * @param params
 */
export function getFeeManageData(params) {
  return request({
    url: "/tp/gbFinance/feeManageData/info",
    method: 'get',
    params: params
  })
}



/**
 * 缴纳费用
 * @param params
 */
export function payFee(params) {
  return request({
    url: "/tp/gbFinance/payFee",
    method: 'get',
    params: params
  })
}




/**
 * 获取生产线列表 - 包含空闲、转产、在产、在建 各个状态的生产线
 * @param params
 */
export function getLineList(params) {
  return request({
    url: '/tp/gbContest/line/listAll',
    method: 'get',
    params: params
  })
}


/**
 * 工人管理-设备管理保存生产线配置
 * @param params
 */
export function saveLineConfig(params) {
  return request({
    url: '/tp/gbContest/line/save',
    method: 'post',
    data: params
  })
}



/**
 * 获取已招聘的-工人列表
 * @param params
 */
export function getWorkerList(params) {
  return request({
    url: '/tp/gbFinance/worker/onTheJob/list',
    method: 'get',
    params: params
  })
}




/**
 * 转产生产线-确认
 * @param params
 */
export function changeLineProduce(params) {
  return request({
    url: '/tp/gbContest/line/transfer',
    method: 'post',
    params: params
  })
}



/**
 * 出售生产线
 * @param params
 */
export function sellLint(params) {
  return request({
    url: '/tp/gbContest/line/sell',
    method: 'post',
    data: params
  })
}



/**
 * 生产线开产
 * @param params
 */
export function startNextProducing(params) {
  return request({
    url: '/tp/gbContest/line/producting',
    method: 'post',
    data: params
  })
}



/**
 * 应收账款收款-确认
 * @param params
 */
export function confirmAccountReceivables(params) {
  return request({
    url: '/tp/gbContest/receivables/confirm',
    method: 'get',
    params: params
  })
}



/**
 * 应付账款付款-确认
 * @param params
 */
export function confirmPay(params) {
  return request({
    url: '/tp/gbContest/pay/confirm',
    method: 'get',
    params: params
  })
}





/**
 * 获取交货订单列表
 * @param params
 */
export function getDeliveryOrderList(params) {
  return request({
    url: '/tp/gbContest/order/list',
    method: 'get',
    params: params
  })
}

/**
 * 确认交货订单
 * @param params
 */
export function confirmDeliveryOrder(params) {
  return request({
    url: '/tp/gbContest/order/delivery',
    method: 'get',
    params: params
  })
}





/**
 * 产品研发-列表
 * @param params
 */
export function getProductDevelopmentList(params) {
  return request({
    url: '/tp/gbContest/product/yfList',
    method: 'get',
    params: params
  })
}

/**
 * 产品研发-确认
 * @param params
 */
export function confirmProductDevelopment(params) {
  return request({
    url: '/tp/gbContest/product/yf',
    method: 'post',
    data: params
  })
}

/**
 * 当季结束
 * @param params
 */
export function endCurrentSeason(params) {
  return request({
    url: '/tp/gbContest/endingSeason',
    method: 'get',
    params: params
  })
}

/**
 * 结束当年
 * @param params
 */
export function endCurrentYear(params) {
  return request({
    url: '/tp/gbYearEnd/confirm',
    method: 'get',
    params: params
  })
}

/**
 * 获取间谍附件
 * @param params
 */
export function getSpy(params) {
  return request({
    url: '/tp/gbContest/attachment/spy/get',
    method: 'get',
    params: params
  })
}

/**
 * 获取规则说明附件
 * @param params
 */
export function getRuleIntroduce(params) {
  return request({
    url: '/tp/gbContest/attachment/rule/get',
    method: 'get',
    params: params
  })
}



/**
 * 确认紧急采购材料
 * @param params
 */
export function confirmRushOrderMaterial(params) {
  return request({
    url: '/tp/gbContest/urgent/material',
    method: 'post',
    data: params
  })
}

/**
 * 确认紧急采购产成品
 * @param params
 */
export function confirmRushOrderProduct(params) {
  return request({
    url: '/tp/gbContest/urgent/product',
    method: 'post',
    data: params
  })
}


/**
 * 获取社会责任得分
 * @param params
 */
export function getSocialResponsibilityScore(params) {
  return request({
    url: '/tp/gbContest/social/score',
    method: 'get',
    params: params
  })
}






/**
 * 获取贴现列表
 * @param params
 */
export function getDiscountList(params) {
  return request({
    url: '/tp/gbContest/show/fee',
    method: 'get',
    params: params
  })
}



/**
 * 获取应付款列表
 * @param params
 */
export function getPayList(params) {
  return request({
    url: '/tp/gbContest/show/pay',
    method: 'get',
    params: params
  })
}



/**
 * 确认贴现
 * @param params
 */
export function confirmDiscount(params) {
  return request({
    url: '/tp/gbContest/discount',
    method: 'post',
    data: params
  })
}



/**
 * 出售库存-出售原料
 * @param params
 */
export function sellMaterial(params) {
  return request({
    url: '/tp/gbContest/sell/material',
    method: 'post',
    data: params
  })
}

/**
 * 出售库存-出售产品
 * @param params
 */
export function sellProduct(params) {
  return request({
    url: '/tp/gbContest/sell/product',
    method: 'post',
    data: params
  })
}





/**
 * 还原本年数据到第一季度
 * @param params
 */
export function restoreCurrentYearStart(params) {
  return request({
    url: '/tp/gbContest/data/restore',
    method: 'get',
    params: params
  })
}

/**
 * 还原当季
 * @param params
 */
export function restoreCurrentSeasonStart(params) {
  return request({
    url: '/tp/gbContest/data/restore/restoreSeason',
    method: 'get',
    params: params
  })
}




/**
 * 获取ISO列表
 * @param params
 */
export function getISOInvestList(params) {
  return request({
    url: '/tp/gbContest/isoInvest/list',
    method: 'get',
    params: params
  })
}

/**
 * 确认ISO投资
 * @param params
 */
export function confirmISOInvest(params) {
  return request({
    url: '/tp/gbContest/isoInvest/commit',
    method: "post",
    data: params
  })
}

/**
 * 获取市场开拓列表
 * @param params
 */
export function getMarketExploreList(params) {
  return request({
    url: '/tp/gbContest/market/list',
    method: 'get',
    params: params
  })
}

/**
 * 确认市场开拓
 * @param params
 */
export function confirmMarketExplore(params) {
  return request({
    url: '/tp/gbContest/market/yf',
    method: 'post',
    data: params
  })
}



/**
 * 填写四张报表
 * @param params
 */
export function fillReport(params) {
  return request({
    url: "/tp/newFillReport/batch/fill",
    method: 'post',
    data: params
  })
}



/**
 * 获取之前填写的内容
 * @param params
 */
export function getTempFill(params) {
  return request({
    url: '/tp/gbFillReport/getTemp',
    method: 'get',
    params: params
  })
}

/**
 * 获取系统自动生成的报表数据
 * @param params
 */
export function getAutoFill(params) {
  return request({
    url: '/tp/gbFillReport/getAutoFill',
    method: 'get',
    params: params
  })
}


/**
 * 获取已开拓市场列表
 * @param params
 */
export function getDevelopedMarketList(params) {
  return request({
    url: '/tp/gbDeliverAds/market/yf/finish',
    method: 'get',
    params: params
  })
}

/**
 * 填写广告报表
 * @param params
 */
export function fillAdForm(params) {
  return request({
    url: '/tp/gbDeliverAds/mnads/deliver',
    method: 'post',
    data: params
  })
}

/**
 * 获取用户和订单
 * @param params
 */
export function getUserAndOrderList(params) {
  return request({
    url: '/tp/gbTradeFair/getUserAndOrder',
    method: 'post',
    data: params
  })
}

/**
 * 选单
 * @param params
 */
export function chooseOrder(params) {
  return request({
    url: '/tp/gbTradeFair/order/choose',
    method: 'post',
    data: params
  })
}




/**
 * 结束当年经营
 * @param params
 */
export function endContestByStudent(params) {
  return request({
    url: '/tp/gbBusiness/end',
    method: 'get',
    params: params
  })
}

/**
 * 下载学生结果
 * @param params
 */
export function downloadContestResult(params) {
  return request({
    url: '/tp/gbContest/download/reslut/download',
    method: 'get',
    params: params
  })
}

/**
 * 获取选单进度
 * @param params
 */
export function getOrderMeetingStatus(params) {
  return request({
    url: '/tp/gbBusiness/xzorder/get',
    method: 'get',
    params: params
  })
}

/**
 * 保存选单进度
 * @param params
 */
export function saveOrderMeetingStatus(params) {
  return request({
    url: "/tp/gbBusiness/xzorder/save",
    method: 'post',
    data: params
  })
}

/**
 * 确定学生竞赛成绩
 * @param params
 */
export function confirmStudentContestGrade(params) {
  return request({
    url: '/tp/gbContest/student/score',
    method: 'get',
    params: params
  })
}

/**
 * 学生重新开始竞赛
 * @param params
 */
export function restartContestByStudent(params) {
  return request({
    url: '/tp/gbBusiness/restart',
    method: 'get',
    params: params
  })
}

/**
 * 学生还原选单
 * @param params
 */
export function restoreOrderByStudent(params) {
  return request({
    url: '/tp/gbTradeFair/order/restore',
    method: 'get',
    params: params
  })
}

/**
 * 获取规则表数据
 * @param params
 */
export function getCompetitonRule(params) {
  return request({
    url: '/tp/gbGz/getRuleBySubjectNumber',
    method: 'get',
    params: params
  })
}

/**
 * 获取数字化研发进度
 * @param params
 */
export function getNumYfResult(params) {
  return request({
    url: '/tp/gbContest/num/list',
    method: 'get',
    params: params
  })
}


/**
 * 数字化研发
 * @param params
 */
export function numYfCommit(params) {
  return request({
    url: '/tp/gbContest/num/commit',
    method: 'post',
    data: params
  })
}


/**
 * 开启智能生产
 * @param params
 */
export function startProduct(params) {
  return request({
    url: '/tp/gbContest/start/digitalize',
    method: 'post',
    data: params
  })
}



