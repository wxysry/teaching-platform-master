import request from '@/utils/request'

export function getStudentList (params) {
  return request({
    url: '/tp/student/list',
    method: 'post',
    data: params
  })
}

/**
 * 删除学生
 */
export function deleteStudentById (params) {
  const url = `/tp/student/delete/${params}`
  return request({
    url: url,
    method: 'get'
  })
}

/**
 * 更新学生信息
 * @param params
 */
export function updateStudentAccountByTeacher (params) {
  return request({
    url: '/tp/student/update',
    method: 'post',
    data: params
  })
}

/**
 * 老师获取视频课程列表
 * @param params
 */
export function getVideoCourseListByTeacher (params) {
  return request({
    url: '/tp/videoCourse/teacher/list',
    method: 'post',
    data: params
  })
}

/**
 * 老师获取视频分组
 */
export function getVideoGroupByTeacher () {
  return request({
    url: '/tp/videoGroup/teacher/list',
    method: 'get',
  })
}

/**
 * 老师获取ppt列表
 * @param params
 */
export function getPPTCourseListByTeacher (params) {
  return request({
    url: '/tp/pptCourse/teacher/list',
    method: 'post',
    data: params
  })
}

/**
 * 老师获取ppt分组
 */
export function getPPTGroupByTeacher () {
  return request({
    url: '/tp/pptGroup/teacher/list',
    method: 'get'
  })
}

/**
 * 获取教师个人信息
 */
export function getTeacherInfo () {
  return request({
    url: '/tp/sysUser/get',
    method: 'get'
  })
}

/**
 * 教师获取试卷列表
 * @param params
 */
export function getPaperList (params) {
  return request({
    url: '/tp/paper/list',
    method: 'post',
    data: params
  })
}

/**
 * 教师创建试卷
 * @param params
 */
export function addPaperByTeacher (params) {
  return request({
    url: '/tp/paper/addOrUpdate',
    method: 'post',
    data: params
  })
}

/**
 * 教师获取题目分组级联
 */
export function getAllProblemGroupByTeacher () {
  return request({
    url: '/tp/problemGroup/list/all/teacher',
    method: 'get'
  })
}

/**
 * 教师获取题型列表
 */
export function getProblemTypes () {
  return request({
    url: '/tp/problemType/list',
    method: 'get'
  })
}

/**
 * 教师获取能查看的试题
 * @param params
 */
export function getProblemListByTeacher (params) {
  return request({
    url: '/tp/problem/listByTeacher',
    method: 'post',
    data: params
  })
}

/**
 * 教师获取试卷基础信息
 * @param params
 */
export function getPaperBasicInfoByTeacher (params) {
  return request({
    url: '/tp/paper/get',
    method: 'get',
    params: params
  })
}

/**
 * 教师获取试卷试题列表
 * @param params
 */
export function getPaperProblemListByTeacher (params) {
  return request({
    url: '/tp/paper/problem/get',
    method: 'get',
    params: params
  })
}

/**
 * 教师添加试卷试题
 * @param params
 */
export function addPaperProblemsByTeacher (params) {
  return request({
    url: '/tp/paper/problem/add',
    method: 'post',
    data: params
  })
}

/**
 *教师获取全部学生
 */
export function getAllStudentListByTeacher () {
  return request({
    url: '/tp/student/list/all',
    method: 'post'
  })
}

/**
 *教师向试卷添加考生
 * @param params
 */
export function addPaperStudent (params) {
  return request({
    url: '/tp/paper/student/add',
    method: 'post',
    data: params
  })
}

/**
 * 教师获取试卷考生列表
 * @param params
 */
export function getPaperStudentListByTeacher (params) {
  return request({
    url: '/tp/paper/student/get',
    method: 'get',
    params: params
  })
}

/**
 * 教师发布试卷
 * @param params
 */
export function publishPaperByTeacher (params) {
  return request({
    url: '/tp/paper/publish',
    method: 'get',
    params: params
  })
}

export function deletePaperByTeacher (params) {
  return request({
    url: '/tp/paper/delete',
    method: 'get',
    params: params
  })
}

/**
 * 教师查看单个试题
 * @param params
 */
export function getProblemByTeacher (params) {
  return request({
    url: '/tp/problem/teacher/get',
    method: 'get',
    params: params
  })
}

/**
 * 教师获取某张试卷参与学生列表
 * @param params
 */
export function getExamStudentListByTeacher (params) {
  return request({
    url: '/tp/exam/student/list',
    method: 'post',
    data: params
  })
}

/**
 * 预览试卷
 * @param params
 */
export function startPreviewExam (params) {
  return request({
    url: '/tp/paper/preview',
    method: 'get',
    params: params
  })
}

/**
 * 教师获取某张试卷学生答案
 * @param params
 */
export function getStudentExamProblemByTeacher (params) {
  return request({
    url: '/tp/examProblem/listByTeacher',
    method: 'get',
    params: params
  })
}

/**
 * 教师批改某张试卷某道题
 * @param params
 */
export function correctProblemByTeacher (params) {
  return request({
    url: '/tp/examProblem/teacher/correct',
    method: 'get',
    params: params
  })
}

/**
 * 教师确认批改试卷
 * @param params
 */
export function confirmCorrectExamPaperByTeacher (params) {
  return request({
    url: '/tp/examProblem/teacher/correct/complete',
    method: 'get',
    params: params
  })
}

/**
 * 教师查看某次考试分析
 * @param params
 */
export function getExamPaperAnalysisByTeacher (params) {
  return request({
    url: '/tp/exam/analysis',
    method: 'get',
    params
  })
}

/**
 * 教师获取竞赛分组-题库级联
 */
export function getContestSubjectGroup () {
  return request({
    url: '/tp/ctSubject/cascade/teacher',
    method: 'get'
  })
}

/**
 * 教师获取竞赛模拟列表
 * @param params
 */
export function getContestListByTeacher (params) {
  return request({
    url: '/tp/contest/list/teacher',
    method: 'post',
    data: params
  })
}




/**
 * 教师删除竞赛
 * @param params
 */
export function deleteContestByTeacher (params) {
  return request({
    url: '/tp/contest/delete',
    method: 'get',
    params: params
  })
}



/**
 * 教师添加竞赛
 * @param params
 */
export function addContestByTeacher (params) {
  return request({
    url: '/tp/contest/add',
    method: 'post',
    data: params
  })
}


/**
 * 教师更新竞赛
 * @param params
 */
export function updateContestInfoByTeacher (params) {
  return request({
    url: '/tp/contest/update',
    method: 'post',
    data: params
  })
}











/**
 * 获取竞赛基本信息
 * @param params
 */
export function getContestInfoByTeacher (params) {
  return request({
    url: '/tp/contest/get',
    method: 'get',
    params: params
  })
}




/**
 * 教师发布竞赛
 * @param params
 */
export function publishContestByTeacher (params) {
  return request({
    url: '/tp/contest/publish',
    method: 'get',
    params: params
  })
}






/**
 * 教师获取竞赛模拟分组的列表
 */
export function getContestGroupByTeacher () {
  return request({
    url: '/tp/contestGroup/listByteacher',
    method: 'get',

  })
}



/**
 * 教师获取题库列表
 * @param params
 */
export function getContestSubjectByTeacher (params) {
  return request({
    url: '/tp/ctSubject/list/teacher',
    method: 'get',
    params: params
  })
}





/**
 * 教师获取竞赛进度
 * @param {*} params
 */
export function getContestProgressByTeacher (params) {
  return request({
    url: '/tp/contest/student/list',
    method: 'post',
    data: params
  })
}




/**
 * 获取初始权益
 * @param params
 */
export function getEquity (params) {
  return request({
    url: '/tp/contest/equity/get',
    method: 'get',
    params: params
  })
}






/**
 * 还原本年数据到第一季度-教师操作
 * @param params
 */
export function restoreCurrentByTeacher (params) {
  return request({
    url: '/tp/contest/data/restore/teacher',
    method: 'get',
    params: params
  })
}




/**
 * 重新经营-教师操作
 * @param params
 */
export function restartContestByTeacher (params) {
  return request({
    url: '/tp/business/restart/teacher',
    method: 'get',
    params: params
  })
}





/**
 * 拥有该视频权限的学生
 * @param params
 */
export function getVideoStudentByTeacher (params) {
  return request({
    url: '/tp/userVideo/student/list',
    method: 'get',
    params: params
  })
}

/**
 * 教师给学生添加视频教程权限
 * @param params
 */
export function shareVideoToStudentByTeacher (params) {
  return request({
    url: '/tp/userVideo/add',
    method: 'post',
    data: params
  })
}

/**
 * 教师获取拥有该ppt权限的学生
 * @param params
 */
export function getPPTStudentByTeacher (params) {
  return request({
    url: '/tp/userPpt/student/list',
    method: 'get',
    params: params
  })
}

/**
 * 教师给学生添加ppt教程权限
 * @param params
 */
export function sharePPTToStudentByTeacher (params) {
  return request({
    url: '/tp/userPpt/add',
    method: 'post',
    data: params
  })
}

/**
 * 保存经营进度-教师
 * @param params
 */
export function saveContestProgressByTeacher (params) {
  return request({
    url: '/tp/business/progress/save/teacher',
    method: 'get',
    params: params
  })
}



/**
 * 保存选单进度-教师
 * @param params
 */
export function saveChooseMeetingProgressByTeacher (params) {
  return request({
    url: '/tp/business/xzorder/save/teacher',
    method: 'post',
    data: params
  })
}






/*新平台新平台新平台新平台新平台新平台新平台新平台新平台新平台新平台新平台*/

/**
 * 新平台模拟-教师获取题库列表
 * @param params
 */
export function getNewContestSubjectByTeacher (params) {
  return request({
    url: '/tp/newCtSubject/list/teacher',
    method: 'get',
    params: params
  })
}


/**
 * 新平台模拟 - 获取初始权益
 * @param params
 */
export function getNewEquity (params) {
  return request({
    url: '/tp/newContest/equity/get',
    method: 'get',
    params: params
  })
}


/**
 * 教师获取新平台模拟列表
 * @param params
 */
export function getNewContestListByTeacher (params) {
  return request({
    url: '/tp/newContest/list/teacher',
    method: 'post',
    data: params
  })
}


/**
 * 教师删除新平台模拟-竞赛
 * @param params
 */
export function deleteNewContestByTeacher (params) {
  return request({
    url: '/tp/newContest/delete',
    method: 'get',
    params: params
  })
}


/**
 * 新平台模拟-教师添加-竞赛
 * @param params
 */
export function addNewContestByTeacher (params) {
  return request({
    url: '/tp/newContest/add',
    method: 'post',
    data: params
  })
}

/**
 * 新平台模拟-教师更新-竞赛
 * @param params
 */
export function updateNewContestInfoByTeacher (params) {
  return request({
    url: '/tp/newContest/update',
    method: 'post',
    data: params
  })
}


/**
 * 新平台模拟-获取基本信息
 * @param params
 */
export function getNewContestInfoByTeacher (params) {
  return request({
    url: '/tp/newContest/get',
    method: 'get',
    params: params
  })
}


/**
 * 教师发布新平台模拟-竞赛
 * @param params
 */
export function publishNewContestByTeacher (params) {
  return request({
    url: '/tp/newContest/publish',
    method: 'get',
    params: params
  })
}


/**
 * 教师获取新平台模拟分组的列表
 */
export function getNewContestGroupByTeacher () {
  return request({
    url: '/tp/newContestGroup/listByteacher',
    method: 'get',

  })
}

/**
 * 新平台模拟教师获取竞赛进度
 * @param {*} params
 */
export function getNewContestProgressByTeacher (params) {
  return request({
    url: '/tp/newContest/student/list',
    method: 'post',
    data: params
  })
}

/**
 * 新平台模拟-教师操作
 * @param params
 */
export function restartNewContestByTeacher (params) {
  return request({
    url: '/tp/newBusiness/restart/teacher',
    method: 'get',
    params: params
  })
}


/**
 *  新平台模拟-保存经营进度-教师
 * @param params
 */
export function saveNewContestProgressByTeacher (params) {
  return request({
    url: '/tp/newBusiness/progress/save/teacher',
    method: 'get',
    params: params
  })
}


/**
 * 新平台模拟-保存选单进度-教师
 * @param params
 */
export function saveNewChooseMeetingProgressByTeacher (params) {
  return request({
    url: '/tp/newBusiness/xzorder/save/teacher',
    method: 'post',
    data: params
  })
}


/**
 * 新平台模拟-还原本年数据到第一季度-教师操作
 * @param params
 */
export function restoreNewCurrentByTeacher (params) {
  return request({
    url: '/tp/newContest/data/restore/teacher',
    method: 'get',
    params: params
  })
}








//////////组间对抗

/**
 * 教师获取组间对抗题库列表
 * @param params
 */
export function getGbCtSubjectListByTeacher(params) {
  return request({
    url: '/tp/gbCtSubject/list/teacher',
    method: 'post',
    data: params
  })
}


/**
 * 教师获取组间对抗分组
 */
export function getGbContestGroupByTeacher () {
  return request({
    url: '/tp/gbContestGroup/listByteacher',
    method: 'get',

  })
}




/**
 * 添加组间对抗题库
 * @param params
 */
export function addGbSubject(params) {
  return request({
    url: '/tp/gbCtSubject/add',
    method: 'post',
    data: params
  })
}


/**
 * 删除组间对抗题库
 * @param params
 */
export function deleteGbSubject(params) {
  return request({
    url: '/tp/gbCtSubject/delete',
    method: 'get',
    params: params
  })
}



/**
 * 更新组间对抗题库
 * @param params
 */
export function updateGbSubject(params) {
  return request({
    url: '/tp/gbCtSubject/update',
    method: 'post',
    data: params
  })
}



/**
 * 教师获取组间对抗竞赛列表
 * @param params
 */
export function getGbContestListByTeacher (params) {
  return request({
    url: '/tp/gbContest/list/teacher',
    method: 'post',
    data: params
  })
}



/**
 * 教师删除组间对抗-竞赛
 * @param params
 */
export function deleteGbContestByTeacher (params) {
  return request({
    url: '/tp/gbContest/delete',
    method: 'get',
    params: params
  })
}


/**
 * 教师发布组间对抗-竞赛
 * @param params
 */
export function publishGbContestByTeacher (params) {
  return request({
    url: '/tp/gbContest/publish',
    method: 'get',
    params: params
  })
}


/**
 * 新增组间对抗竞赛
 * @param params
 */
export function addGbContestByTeacher (params) {
  return request({
    url: '/tp/gbContest/add',
    method: 'post',
    data: params
  })
}



/**
 * 教师更新组间对抗-竞赛
 * @param params
 */
export function updateGbContestInfoByTeacher (params) {
  return request({
    url: '/tp/gbContest/update',
    method: 'post',
    data: params
  })
}


/**
 * 组间对抗-获取初始权益
 * @param params
 */
export function getGbEquity (params) {
  return request({
    url: '/tp/gbContest/equity/get',
    method: 'get',
    params: params
  })
}


/**
 * 组间对抗竞赛-获取基本信息
 * @param params
 */
export function getGbContestInfoByTeacher (params) {
  return request({
    url: '/tp/gbContest/get',
    method: 'get',
    params: params
  })
}


/**
 * 组间对抗竞赛-设置各季度时间
 * @param params
 */
export function setGbContestSeasonTime (params) {
  return request({
    url: '/tp/gbContest/update/season/time',
    method: 'post',
    data: params
  })
}



/**
 * 组间对抗竞赛-下一回合
 * @param params
 */
export function nextRound (params) {
  return request({
    url: '/tp/gbContest/next/round',
    method: 'get',
    params: params
  })
}


/**
 * 组间对抗竞赛-上一回合
 * @param params
 */
export function lastRound(params) {
  return request({
    url: '/tp/gbContest/last/round',
    method: 'get',
    params: params
  })
}



/**
 * 组间对抗竞赛-暂停比赛
 * @param params
 */
export function pauseGbContest(params) {
  return request({
    url: '/tp/gbContest/pause/contest',
    method: 'get',
    params: params
  })
}


/**
 * 组间对抗竞赛-恢复比赛
 * @param params
 */
export function recoverGbContest(params) {
  return request({
    url: '/tp/gbContest/recover/contest',
    method: 'get',
    params: params
  })
}



/**
 * 组间对抗-教师获取学生进度
 * @param {*} params
 */
export function getGbContestProgressByTeacher (params) {
  return request({
    url: '/tp/gbContest/student/list',
    method: 'post',
    data: params
  })
}



/**
 * 组间对抗-企业数据调整
 * @param {*} params
 */
export function GbContestDataAdjust (params) {
  return request({
    url: '/tp/gbContest/add/capital',
    method: 'post',
    data: params
  })
}
