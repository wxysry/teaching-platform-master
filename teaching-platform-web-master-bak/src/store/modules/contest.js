const SET_YEAR = 'SET_YEAR'
const SET_SEASON = 'SET_SEASON'
const SET_STATUS = 'SET_STATUS'
const SET_BUTTON_VISIBLE = 'SET_BUTTON_VISIBLE'
const SET_MARKET_LIST = 'SET_MARKET_LIST'
const SET_ACTIVE_NAME = "SET_ACTIVE_NAME"
const state = {
  year: 1,
  season: 1,
  status: 1,
  buttonVisible: {
    applyShortLoan: true,
    orderRawMaterial: true,
    buildingLine: true,
    productDevelopment: true,
    marketDevelopment: true,
    ISOAuth: true,
    fillReportForm: true,
    adLaunch: true,
    attendOrderMeeting: true
  },
  marketList: [
    {'name': '本地', status: 0, product: 'P1', round: 1},
    {'name': '区域', status: 0, product: 'P1', round: 1},
    {'name': '国内', status: 0, product: 'P1', round: 1},
    {'name': '亚洲', status: 0, product: 'P1', round: 1},
    {'name': '国际', status: 0, product: 'P1', round: 1}],
  activeName: '本地'
}

const mutations = {
  SET_YEAR: (state, payload) => {
    state.year = payload
  },
  SET_SEASON: (state, payload) => {
    state.season = payload
  },
  SET_STATUS: (state, payload) => {
    state.status = payload
  },
  SET_BUTTON_VISIBLE: (state, payload) => {
    state.buttonVisible = payload
  },
  SET_MARKET_LIST: (state, payload) => {
    state.marketList = payload
  },
  SET_ACTIVE_NAME: (state, payload) => {
    state.activeName = payload
  }
}

const actions = {
  setYear({commit}, payload) {
    commit(SET_YEAR, payload)
  },
  setSeason({commit}, payload) {
    commit(SET_SEASON, payload)
  },
  setStatus({commit}, payload) {
    commit(SET_STATUS, payload)
  },
  setButtonVisible({commit}, payload) {
    commit(SET_BUTTON_VISIBLE, payload)
  },
  setMarketList({commit}, payload) {
    commit(SET_MARKET_LIST, payload)
  },
  setActiveName({commit}, payload) {
    commit(SET_ACTIVE_NAME, payload)
  }
}

export default {
  namespaced: true,
  state,
  mutations,
  actions
}
