var timer;
export function debounce(fn, time) {
    var time = time || 200;

    return function (...args) {
        var _this = this
        if (timer) {
            clearTimeout(timer)
        }
        timer = setTimeout(function () {
            timer = null
            fn.apply(_this, args)
        }, time)
    };
}
