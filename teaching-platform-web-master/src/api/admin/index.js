import request from '@/utils/request'

export function getTeacherList(params) {
  return request({
    url: '/tp/teacher/list',
    method: 'post',
    data: params
  })
}

/**
 * 添加教师
 * @param params
 */
export function addTeacher(params) {
  return request({
    url: '/tp/teacher/add',
    method: 'post',
    data: params
  })
}

/**
 * 更新教师信息
 * @param params
 */
export function updateTeacher(params) {
  return request({
    url: '/tp/teacher/update',
    method: 'post',
    data: params
  })
}

/**
 * 更新分组权限
 * @param params
 */
export function updateGroupPermission(params) {
  return request({
    url: '/tp/teacher/group/update',
    method: 'post',
    data: params
  })
}

/**
 * 管理员获取题库分组列表-第一级
 */
export function getProblemGroup() {
  return request({
    url: '/tp/problemGroup/list/admin',
    method: 'get'
  })
}

/**
 * 管理员获取分组级联
 */
export function getAllProblemGroup() {
  return request({
    url: '/tp/problemGroup/list/all/admin',
    method: 'get'
  })
}

/**
 * 管理员获取视频分组列表
 */
export function getVideoGroupByAdmin() {
  return request({
    url: '/tp/videoGroup/list',
    method: 'get'
  })
}

/**
 * 管理员获取PPT分组列表
 */
export function getPPTGroupByAdmin() {
  return request({
    url: '/tp/pptGroup/list',
    method: 'get'
  })

}

/**
 * 管理员获取所有竞赛模拟分组的列表
 */
export function getContestGroup() {
  return request({
    url: '/tp/contestGroup/list',
    method: 'get',
  })
}


/**
 * 管理员获取所有新平台模拟分组的列表
 */
export function getNewContestGroup() {
  return request({
    url: '/tp/newContestGroup/list',
    method: 'get',
  })
}


/**
 * 管理员获取所有组间对抗分组的列表
 */
export function getGbContestGroup() {
  return request({
    url: '/tp/gbContestGroup/list',
    method: 'get',
  })
}


/**
 * 管理员获取所有组间对抗(新)分组的列表
 */
export function getNGbContestGroup() {
  return request({
    url: '/tp/ngbContestGroup/list',
    method: 'get',
  })
}


/**
 * 根据type更新教师分组权限
 * @param params
 */
export function updateTeacherPermission(params) {
  return request({
    url: '/tp/teacher/group/update',
    method: 'post',
    data: params
  })
}


/**
 * fatherId 父级分组的id，等于0或者null表示第一级
 * groupName 分组名称
 分组名称
 * 添加题目分组
 * @param params
 */
export function addProblemGroup(params) {
  return request({
    url: '/tp/problemGroup/add',
    method: 'post',
    data: params
  })
}

/**
 * fatherId 父级分组的id，等于0或者null表示第一级
 * groupName 分组名称
 * id 分组的id
 * 更新题目分组
 * @param params
 */
export function updateProblemGroup(params) {
  return request({
    url: '/tp/problemGroup/update',
    method: 'post',
    data: params
  })
}

/**
 *
 * 删除题目分组
 * @param params
 */
export function deleteProblemGroup(id) {
  const url = `/tp/problemGroup/delete/${id}`
  return request({
    url: url,
    method: 'get'
  })
}

/**
 * 添加竞赛模拟分组
 * @param params
 */
export function addContestGroup(params) {
  return request({
    url: '/tp/contestGroup/add',
    method: 'post',
    data: params
  })
}

/**
 * 添加新平台模拟分组
 * @param params
 */
export function addNewContestGroup(params) {
  return request({
    url: '/tp/newContestGroup/add',
    method: 'post',
    data: params
  })
}


/**
 * 添加组间对抗分组
 * @param params
 */
export function addGbContestGroup(params) {
  return request({
    url: '/tp/gbContestGroup/add',
    method: 'post',
    data: params
  })
}



/**
 * 添加组间对抗分组
 * @param params
 */
export function addNGbContestGroup(params) {
  return request({
    url: '/tp/ngbContestGroup/add',
    method: 'post',
    data: params
  })
}


/**
 * groupName 分组名称
 * id 视频分组的id
 * 添加视频分组
 * @param params
 */
export function addVideoGroup(params) {
  return request({
    url: '/tp/videoGroup/add',
    method: 'post',
    data: params
  })
}

/**
 * groupName 分组名称
 * id 视频分组的id
 * 更新视频分组
 * @param params
 */
export function updateVideoGroup(params) {
  return request({
    url: '/tp/videoGroup/update',
    method: 'post',
    data: params
  })
}

/**
 *
 * 删除视频分组
 * @param id
 */
export function deleteVideoGroup(id) {
  const url = `/tp/videoGroup/delete/${id}`
  return request({
    url: url,
    method: 'get'
  })
}

/**
 * 添加ppt分组
 * @param params
 */
export function addPPTGroup(params) {
  return request({
    url: '/tp/pptGroup/add',
    method: 'post',
    data: params
  })
}

/**
 * 更新ppt分组
 * @param params
 */
export function updatePPTGroup(params) {
  return request({
    url: '/tp/pptGroup/update',
    method: 'post',
    data: params
  })
}

/**
 * 删除ppt分组
 * @param id
 */
export function deletePPTGroup(id) {
  const url = `/tp/pptGroup/delete/${id}`
  return request({
    url: url,
    method: 'get'
  })
}

/**
 * 更新竞赛模拟分组
 * @param params
 */
export function updateContestGroup(params) {
  return request({
    url: '/tp/contestGroup/update',
    method: 'post',
    data: params
  })
}


/**
 * 更新新平台模拟分组
 * @param params
 */
export function updateNewContestGroup(params) {
  return request({
    url: '/tp/newContestGroup/update',
    method: 'post',
    data: params
  })
}


/**
 * 更新组间对抗分组
 * @param params
 */
export function updateGbContestGroup(params) {
  return request({
    url: '/tp/gbContestGroup/update',
    method: 'post',
    data: params
  })
}


/**
 * 更新组间对抗(新)分组
 * @param params
 */
export function updateNGbContestGroup(params) {
  return request({
    url: '/tp/ngbContestGroup/update',
    method: 'post',
    data: params
  })
}

/**
 * 删除竞赛模拟分组
 * @param params
 */
export function deleteContestGroup(params) {
  const url = `/tp/contestGroup/delete/${params}`
  return request({
    url: url,
    method: 'get'
  })
}


/**
 * 删除新平台模拟分组
 * @param params
 */
export function deleteNewContestGroup(params) {
  const url = `/tp/newContestGroup/delete/${params}`
  return request({
    url: url,
    method: 'get'
  })
}


/**
 * 删除组间对抗分组
 * @param params
 */
export function deleteGbContestGroup(params) {
  const url = `/tp/gbContestGroup/delete/${params}`
  return request({
    url: url,
    method: 'get'
  })
}


/**
 * 删除组间对抗分组
 * @param params
 */
export function deleteNGbContestGroup(params) {
  const url = `/tp/ngbContestGroup/delete/${params}`
  return request({
    url: url,
    method: 'get'
  })
}


/**
 * 管理员查询试题列表
 */
export function getProblemList(params) {
  return request({
    url: '/tp/problem/listByAdmin',
    method: 'post',
    data: params
  })
}

/**
 * 获取单个试题信息
 * @param params
 */
export function getProblem(params) {
  return request({
    url: '/tp/problem/admin/get',
    method: 'get',
    params: params
  })
}

/**
 * 获取题型列表
 * @param params
 */
export function getProblemTypeList(params) {
  return request({
    url: '/tp/problemType/list',
    method: 'get'
  })
}

/**
 * 批量删除试题
 * @param ids
 */
export function deleteProblem(ids) {
  const url = `/tp/problem/delete/${ids}`
  return request({
    url: url,
    method: 'get'
  })
}

/**
 * 管理员查看PPT列表
 * @param params
 */
export function getPPTCourseListByAdmin(params) {
  return request({
    url: '/tp/pptCourse/list',
    method: 'post',
    data: params
  })
}

export function deletePPTCourse(id) {
  const url = `/tp/pptCourse/delete/${id}`
  return request({
    url: url,
    method: 'get'
  })
}

/**
 * 管理员查看视频列表
 * @param params
 */
export function getVideoCourseListByAdmin(params) {
  return request({
    url: '/tp/videoCourse/list',
    method: 'post',
    data: params
  })
}

/**
 * 管理员更新ppt信息
 * @param params
 */
export function updatePPTCourseByAdmin(params) {
  return request({
    url: '/tp/pptCourse/update',
    method: 'post',
    data: params
  })
}

export function deleteVideoCourseByAdmin(ids) {
  const url = `/tp/videoCourse/delete/${ids}`
  return request({
    url: url,
    method: 'get'
  })
}

/**
 * 管理员更新视频信息
 * @param params
 */
export function updateVideoCourseByAdmin(params) {
  return request({
    url: '/tp/videoCourse/update',
    method: 'post',
    data: params
  })
}

/**
 *管理员更新试题信息
 * @param params
 */
export function updateProblemByAdmin(params) {
  return request({
    url: '/tp/problem/update',
    method: 'post',
    data: params
  })
}

/**
 * 管理员新增试题
 * @param params
 */
export function addProblemByAdmin(params) {
  return request({
    url: '/tp/problem/add',
    method: 'post',
    data: params
  })
}

/**
 * 管理员获取附件列表
 * @param params
 */
export function getAttachmentList(params) {
  return request({
    url: '/tp/resources/attachment/list',
    method: 'post',
    data: params
  })
}

/**
 * 管理员获取图片列表
 * @param params
 */
export function getPicList(params) {
  return request({
    url: '/tp/resources/pic/list',
    method: 'post',
    data: params
  })
}


export function getPPTImages(params) {
  const url = `/tp/pptCourse/show/ppt/${params.id}?folderPath=${params.folderPath}`
  return request({
    url: url,
    method: 'get'
  })
}

/**
 * 管理员获取竞赛模拟列表
 * @param params
 */
export function getContestListByAdmin(params) {
  return request({
    url: '/tp/ctSubject/list/admin',
    method: 'post',
    data: params
  })
}


/**
 * 管理员获取新平台模拟列表
 * @param params
 */
export function getNewContestListByAdmin(params) {
  return request({
    url: '/tp/newCtSubject/list/admin',
    method: 'post',
    data: params
  })
}


/**
 * 组间对抗-管理员获取考试列表
 * @param params
 */
export function getGbContestListByAdmin(params) {
  return request({
    url: '/tp/gbContest/list/admin',
    method: 'post',
    data: params
  })
}


/**
 * 获取题库新本信息
 * @param params
 */
export function getSubjectInfo(params) {
  return request({
    url: '/tp/ctSubject/get',
    method: 'get',
    params: params
  })
}

/**
 * 添加题库
 * @param params
 */
export function addSubject(params) {
  return request({
    url: '/tp/ctSubject/add',
    method: 'post',
    data: params
  })
}


/**
 * 添加题库
 * @param params
 */
export function addNewSubject(params) {
  return request({
    url: '/tp/newCtSubject/add',
    method: 'post',
    data: params
  })
}


/**
 * 更新题库信息
 * @param params
 */
export function updateSubject(params) {
  return request({
    url: '/tp/ctSubject/update',
    method: 'post',
    data: params
  })
}


/**
 * 更新题库信息
 * @param params
 */
export function updateNewSubject(params) {
  return request({
    url: '/tp/newCtSubject/update',
    method: 'post',
    data: params
  })
}


/**
 * 删除题库
 * @param params
 */
export function deleteSubject(params) {
  return request({
    url: '/tp/ctSubject/delete',
    method: 'get',
    params: params
  })
}


/**
 * 删除新平台题库
 * @param params
 */
export function deleteNewSubject(params) {
  return request({
    url: '/tp/newCtSubject/delete',
    method: 'get',
    params: params
  })
}



/**
 * 组间对抗-生成复盘数据
 * @param params
 */
export function generateSubjectData(params) {
  return request({
    url: '/tp/gbContest/generate/subject',
    method: 'get',
    params: params
  })
}
