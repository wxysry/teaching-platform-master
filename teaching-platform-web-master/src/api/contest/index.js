import request from '@/utils/request'

/**
 * 获取财务信息
 * @param params
 */
export function getFinancialInfo(params) {
  return request({
    url: "/tp/finance/statistical/info",
    method: 'get',
    params: params
  })
}

/**
 * 获取研发认证信息
 * @param params
 */
export function getResearchAndAuthInfo(params) {
  return request({
    url: "/tp/finance/researchAndAuth/info",
    method: 'get',
    params: params
  })
}

/**
 * 获取库存信息
 * @param params
 */
export function GetStockInfo(params) {
  return request({
    url: '/tp/finance/stockPurchase/info',
    method: 'get',
    params: params
  })
}

/**
 * 获取长期贷款额度
 * @param params
 */
export function getLongTermLoanLimit(params) {
  return request({
    url: '/tp/finance/longloan/info',
    method: 'get',
    params: params
  })
}

/**
 * 确认长贷贷款金额
 * @param params
 */
export function confirmLongTermLoanMoney(params) {
  return request({
    url: "/tp/finance/longloan/commit",
    method: 'post',
    data: params
  })
}

/**
 * 获取短贷额度限制
 * @param params
 */
export function getShortTermLoanLimit(params) {
  return request({
    url: '/tp/finance/shortloan/info',
    method: 'get',
    params: params
  })
}

/**
 * 确认短贷金额
 * @param params
 */
export function confirmShortTermLoanMoney(params) {
  return request({
    url: '/tp/finance/shortloan/commit',
    method: 'post',
    data: params
  })
}

/**
 * 获取规则厂房列表
 * @param params
 */
export function getGzWorkShopList(params) {
  return request({
    url: '/tp/finance/factory/list',
    method: 'get',
    params: params
  })
}

/**
 * 购租厂房-确认
 * @param params
 */
export function confirmWorkShop(params) {
  return request({
    url: '/tp/finance/factory/confirm',
    method: 'get',
    params: params
  })
}

/**
 * 更新原料-现付现金
 * @param params
 */
export function getUpdateRawMaterialCost(params) {
  return request({
    url: '/tp/finance/material/update',
    method: 'get',
    params: params
  })
}

/**
 * 确认更新原料
 * @param params
 */
export function confirmUpdateRawMaterial(params) {
  return request({
    url: '/tp/finance/material/confirm',
    method: 'get',
    params: params
  })
}

/**
 * 获取原料规则
 * @param params
 */
export function getGzRawMaterial(params) {
  return request({
    url: '/tp/finance/material/list',
    method: 'get',
    params: params
  })
}






/**
 * 确认订购原料
 * @param params
 */
export function confirmOrderRawMaterial(params) {
  return request({
    url: '/tp/finance/material/buy',
    method: 'post',
    data: params
  })
}

/**
 * 获取厂房信息
 * @param params
 */
export function getWorkShopList(params) {
  return request({
    url: '/tp/finance/workshop/info',
    method: 'get',
    params: params
  })
}

/**
 * 获取新建生产线基本信息
 * @param params
 */
export function getNewLineInfo(params) {
  return request({
    url: '/tp/contest/line/info',
    method: 'get',
    params: params
  })
}

/**
 * 确认新建生产线
 * @param params
 */
export function confirmNewLineInfo(params) {
  return request({
    url: '/tp/contest/line/add',
    method: 'post',
    data: params
  })
}

/**
 * 学生获取竞赛模拟列表-
 * @param params
 */
export function getContestListByStudent(params) {
  return request({
    url: "/tp/contest/list/student",
    method: 'post',
    data: params
  })
}

/**
 * 开始经营
 * @param params
 */
export function startContestByStudent(params) {
  return request({
    url: '/tp/business/start',
    method: 'get',
    params: params
  })
}

/**
 * 获取当前竞赛进度
 * @param params
 */
export function getContestProcess(params) {
  return request({
    url: '/tp/business/progress/get',
    method: 'get',
    params: params
  })
}

/**
 * 保存经营进度
 * @param params
 */
export function saveContestProcess(params) {
  return request({
    url: '/tp/business/progress/save',
    method: 'get',
    params: params
  })
}

/**
 * 当季开始
 * @param params
 */
export function startCurrentSeason(params) {
  return request({
    url: "/tp/finance/currentQuarter/start",
    method: 'get',
    params: params
  })
}

/**
 * 获取在建生产线列表
 * @param params
 */
export function getBuildingProductLineList(params) {
  return request({
    url: '/tp/contest/line/listOnline',
    method: 'get',
    params: params
  })
}

/**
 * 确认在建生产线
 * @param params
 */
export function confirmBuildingProductLine(params) {
  return request({
    url: '/tp/contest/line/online/confirm',
    method: 'post',
    data: params
  })
}

/**
 * 转产生产线/出售生产线列表
 * @param params
 */
export function getChangeOrSellLineList(params) {
  return request({
    url: '/tp/contest/line/list/transfer',
    method: 'get',
    params: params
  })
}

/**
 * 转产生产线-确认
 * @param params
 */
export function changeLineProduce(params) {
  return request({
    url: '/tp/contest/line/transfer',
    method: 'post',
    data: params
  })
}

/**
 *  产品信息获取
 * @param params
 */
export function getProductInfo(params) {
  return request({
    url: '/tp/contest/line/product/info',
    method: 'get',
    params: params
  })
}

/**
 * 出售生产线
 * @param params
 */
export function sellLint(params) {
  return request({
    url: '/tp/contest/line/sell',
    method: 'post',
    data: params
  })
}

/**
 * 获取下一批生产名单
 * @param params
 */
export function getNextProducingList(params) {
  return request({
    url: '/tp/contest/line/list/noProducting',
    method: 'get',
    params: params
  })
}

/**
 * 开始下一批生产
 * @param params
 */
export function startNextProducing(params) {
  return request({
    url: '/tp/contest/line/producting',
    method: 'post',
    data: params
  })
}

/**
 * 获取应收款项
 * @param params
 */
export function getAccountReceivables(params) {
  return request({
    url: "/tp/contest/receivables/check",
    method: 'get',
    params: params
  })
}

/**
 * 应收款更新-确认
 * @param params
 */
export function confirmAccountReceivables(params) {
  return request({
    url: '/tp/contest/receivables/confirm',
    method: 'post',
    data: params
  })
}

/**
 * 获取交货订单列表
 * @param params
 */
export function getDeliveryOrderList(params) {
  return request({
    url: '/tp/contest/order/list',
    method: 'get',
    params: params
  })
}

/**
 * 确认交货订单
 * @param params
 */
export function confirmDeliveryOrder(params) {
  return request({
    url: '/tp/contest/order/delivery',
    method: 'get',
    params: params
  })
}

/**
 * 厂房处理-卖出(买转租)列表
 * @param params
 */
export function getWorkShopBuyToRentList(params) {
  return request({
    url: '/tp/contest/workshop/sellList',
    method: 'get',
    params: params
  })
}

/**
 * 确认厂房买转租
 * @param params
 */
export function confirmWorkShopBuyToRent(params) {
  return request({
    url: '/tp/contest/workshop/sell',
    method: 'get',
    params: params
  })
}

/**
 * 厂房处理-退租-列表
 * @param params
 */
export function getWorkShopSurrenderList(params) {
  return request({
    url: '/tp/contest/workshop/surrenderList',
    method: 'get',
    params: params
  })
}

/**
 * 厂房处理-退租-确认
 * @param params
 */
export function confirmWorkShopSurrender(params) {
  return request({
    url: '/tp/contest/workshop/surrender',
    method: 'get',
    params: params
  })
}

/**
 * 厂房处理-租转卖-列表
 * @param params
 */
export function getWorkShopRentToBuyList(params) {
  return request({
    url: '/tp/contest/workshop/zzmList',
    method: 'get',
    params: params
  })
}

/**
 * 厂房处理-租转麦-确认
 * @param params
 */
export function confirmWorkShopRentToBuyList(params) {
  return request({
    url: '/tp/contest/workshop/zzm',
    method: 'get',
    params: params
  })
}

/**
 * 产品研发-列表
 * @param params
 */
export function getProductDevelopmentList(params) {
  return request({
    url: '/tp/contest/product/yfList',
    method: 'get',
    params: params
  })
}

/**
 * 产品研发-确认
 * @param params
 */
export function confirmProductDevelopment(params) {
  return request({
    url: '/tp/contest/product/yf',
    method: 'post',
    data: params
  })
}

/**
 * 当季结束
 * @param params
 */
export function endCurrentSeason(params) {
  return request({
    url: '/tp/contest/endingSeason',
    method: 'get',
    params: params
  })
}

/**
 * 结束当年
 * @param params
 */
export function endCurrentYear(params) {
  return request({
    url: '/tp/yearEnd/confirm',
    method: 'get',
    params: params
  })
}

/**
 * 获取间谍附件
 * @param params
 */
export function getSpy(params) {
  return request({
    url: '/tp/contest/attachment/spy/get',
    method: 'get',
    params: params
  })
}

/**
 * 获取规则说明附件
 * @param params
 */
export function getRuleIntroduce(params) {
  return request({
    url: '/tp/contest/attachment/rule/get',
    method: 'get',
    params: params
  })
}

/**
 * 获取紧急采购项目
 * @param params
 */
export function getRushOrderList(params) {
  return request({
    url: '/tp/contest/list/kc',
    method: 'get',
    params: params
  })
}

/**
 * 确认紧急采购材料
 * @param params
 */
export function confirmRushOrderMaterial(params) {
  return request({
    url: '/tp/contest/urgent/material',
    method: 'post',
    data: params
  })
}

/**
 * 确认紧急采购产成品
 * @param params
 */
export function confirmRushOrderProduct(params) {
  return request({
    url: '/tp/contest/urgent/product',
    method: 'post',
    data: params
  })
}

/**
 * 获取贴现列表
 * @param params
 */
export function getDiscountList(params) {
  return request({
    url: '/tp/contest/show/fee',
    method: 'get',
    params: params
  })
}

/**
 * 确认贴现
 * @param params
 */
export function confirmDiscount(params) {
  return request({
    url: '/tp/contest/discount',
    method: 'post',
    data: params
  })
}

/**
 * 出售库存-获取库存信息
 * @param params
 */
export function getStockInfo(params) {
  return request({
    url: '/tp/contest/list/sell/kc',
    method: 'get',
    params: params
  })
}

/**
 * 出售库存-出售原料
 * @param params
 */
export function sellMaterial(params) {
  return request({
    url: '/tp/contest/sell/material',
    method: 'post',
    data: params
  })
}

/**
 * 出售库存-出售产品
 * @param params
 */
export function sellProduct(params) {
  return request({
    url: '/tp/contest/sell/product',
    method: 'post',
    data: params
  })
}

/**
 * 厂房贴现
 * @param params
 */
export function workShopDiscount(params) {
  return request({
    url: "/tp/contest/workshop/discount",
    method: 'get',
    params: params
  })
}

/**
 * 厂房贴现-确认
 * @param params
 */
export function confirmWorkShopDiscount(params) {
  return request({
    url: '/tp/contest/workshop/discount/confirm',
    method: 'post',
    data: params
  })
}

/**
 * 还原本年数据到第一季度
 * @param params
 */
export function restoreCurrentYearStart(params) {
  return request({
    url: '/tp/contest/data/restore',
    method: 'get',
    params: params
  })
}

/**
 * 还原当季
 * @param params
 */
 export function restoreCurrentSeasonStart(params) {
  return request({
    url: '/tp/contest/data/restore/restoreSeason',
    method: 'get',
    params: params
  })
}


export function t() {

}

/**
 * 获取ISO列表
 * @param params
 */
export function getISOInvestList(params) {
  return request({
    url: '/tp/contest/isoInvest/list',
    method: 'get',
    params: params
  })
}

/**
 * 确认ISO投资
 * @param params
 */
export function confirmISOInvest(params) {
  return request({
    url: '/tp/contest/isoInvest/commit',
    method: "post",
    data: params
  })
}

/**
 * 获取市场开拓列表
 * @param params
 */
export function getMarketExploreList(params) {
  return request({
    url: '/tp/contest/market/list',
    method: 'get',
    params: params
  })
}

/**
 * 确认市场开拓
 * @param params
 */
export function confirmMarketExplore(params) {
  return request({
    url: '/tp/contest/market/yf',
    method: 'post',
    data: params
  })
}

/**
 * 填写综合费用表
 * @param params
 */
export function fillComprehensiveCostForm(params) {
  return request({
    url: "/tp/fillReport/charges/add",
    method: 'post',
    data: params
  })
}

/**
 * 填写利润表
 * @param params
 */
export function fillProfitForm(params) {
  return request({
    url: '/tp/fillReport/profitChart/add',
    method: 'post',
    data: params
  })
}

/**
 * 填写资产负债表
 * @param params
 */
export function fillAssetsDebtForm(params) {
  return request({
    url: '/tp/fillReport/balance/add',
    method: 'post',
    data: params
  })
}

/**
 * 获取已开拓市场列表
 * @param params
 */
export function getDevelopedMarketList(params) {
  return request({
    url: '/tp/deliverAds/market/yf/finish',
    method: 'get',
    params: params
  })
}

/**
 * 填写广告报表
 * @param params
 */
export function fillAdForm(params) {
  return request({
    url: '/tp/deliverAds/mnads/deliver',
    method: 'post',
    data: params
  })
}

/**
 * 获取用户和订单
 * @param params
 */
export function getUserAndOrderList(params) {
  return request({
    url: '/tp/tradeFair/getUserAndOrder',
    method: 'post',
    data: params
  })
}

/**
 * 选单
 * @param params
 */
export function chooseOrder(params) {
  return request({
    url: '/tp/tradeFair/order/choose',
    method: 'post',
    data: params
  })
}

/**
 * 结束订货会
 * @param params
 */
export function endOrderMeeting(params) {
  return request({
    url: '/tp/tradeFair/end',
    method: 'get',
    params: params
  })
}

/**
 * 获取所有订单信息
 * @param params
 */
export function getOrderInfoList(params) {
  return request({
    url: '/tp/contest/xzorder',
    method: 'get',
    params: params
  })
}

/**
 * 结束当年经营
 * @param params
 */
export function endContestByStudent(params) {
  return request({
    url: '/tp/business/end',
    method: 'get',
    params: params
  })
}

/**
 * 下载学生结果
 * @param params
 */
export function downloadContestResult(params) {
  return request({
    url: '/tp/contest/download/reslut/download',
    method: 'get',
    params: params
  })
}

/**
 * 获取选单进度
 * @param params
 */
export function getOrderMeetingStatus(params) {
  return request({
    url: '/tp/business/xzorder/get',
    method: 'get',
    params: params
  })
}

/**
 * 保存选单进度
 * @param params
 */
export function saveOrderMeetingStatus(params) {
  return request({
    url: "/tp/business/xzorder/save",
    method: 'post',
    data: params
  })
}

/**
 * 确定学生竞赛成绩
 * @param params
 */
export function confirmStudentContestGrade(params) {
  return request({
    url: '/tp/contest/student/score',
    method: 'get',
    params: params
  })
}

/**
 * 学生重新开始竞赛
 * @param params
 */
export function restartContestByStudent(params) {
  return request({
    url: '/tp/business/restart',
    method: 'get',
    params: params
  })
}

/**
 * 学生还原选单
 * @param params
 */
export function restoreOrderByStudent(params) {
  return request({
    url: '/tp/tradeFair/order/restore',
    method: 'get',
    params: params
  })
}
/**
 * 获取规则表数据
 * @param params
 */
 export function getCompetitonRule(params) {
  return request({
    url: '/tp/gz/getRuleBySubjectNumber',
    method: 'get',
    params: params
  })
}
