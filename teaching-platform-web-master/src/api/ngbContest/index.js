import request from '@/utils/request'

/**
 * 获取财务信息
 * @param params
 */
export function getFinancialInfo(params) {
  return request({
    url: "/tp/ngbFinance/statistical/info",
    method: 'get',
    params: params
  })
}

/**
 * 获取研发认证信息
 * @param params
 */
export function getResearchAndAuthInfo(params) {
  return request({
    url: "/tp/ngbFinance/researchAndAuth/info",
    method: 'get',
    params: params
  })
}

/**
 * 获取库存信息
 * @param params
 */
export function GetStockInfo(params) {
  return request({
    url: '/tp/ngbFinance/stockPurchase/info',
    method: 'get',
    params: params
  })
}




/**
 * 根据竞赛ID获取融资规则
 * @param params
 */
export function getGzLoanByContestId(params) {
  return request({
    url: '/tp/ngbFinance/loan/gz/info',
    method: 'get',
    params: params
  })
}


/**
 * 获取融资现状
 * @param params
 */
export function getBankloan(params) {
  return request({
    url: '/tp/ngbFinance/loan/bank/info',
    method: 'get',
    params: params
  })
}



/**
 * 获取贷款额度
 * @param params
 */
export function getLoanLimit(params) {
  return request({
    url: '/tp/ngbFinance/loan/info',
    method: 'get',
    params: params
  })
}

/**
 * 确认贷款
 * @param params
 */
export function confirmLoan(params) {
  return request({
    url: "/tp/ngbFinance/loan/commit",
    method: 'post',
    data: params
  })
}






/**
 * 原材料-下单
 * @param params
 */
export function confirmOrderMaterial(params) {
  return request({
    url: '/tp/ngbFinance/material/confirmOrder',
    method: 'post',
    data: params
  })
}


/**
 * 原材料-订单收货
 * @param params
 */
export function receiveMaterialGoods(params) {
  return request({
    url: '/tp/ngbFinance/material/to/inventory',
    method: 'get',
    params: params
  })
}



/**
 * 获取班次规则
 * @param params
 */
export function getGzClasses(params) {
  return request({
    url: '/tp/ngbFinance/classes/list',
    method: 'get',
    params: params
  })
}

/**
 * 获取原料市场
 * @param params
 */
export function getMaterialShop(params) {
  return request({
    url: '/tp/ngbFinance/material/shop',
    method: 'get',
    params: params
  })
}


/**
 * 获取原料订单
 * @param params
 */
export function getMaterialOrder(params) {
  return request({
    url: '/tp/ngbFinance/material/order',
    method: 'get',
    params: params
  })
}


/**
 * 获取原料库存
 * @param params
 */
export function getMaterialInventory(params) {
  return request({
    url: '/tp/ngbFinance/material/inventory',
    method: 'get',
    params: params
  })
}



/**
 * 获取产品库存
 * @param params
 */
export function getProductInventory(params) {
  return request({
    url: '/tp/ngbFinance/product/inventory',
    method: 'get',
    params: params
  })
}







/**
 * 获取人力资源市场
 * @param params
 */
export function getWokerMarket(params) {
  return request({
    url: '/tp/ngbFinance/wokerMarket/list',
    method: 'get',
    params: params
  })
}


/**
 * 发送OFFER
 * @param params
 */
export function sendOffer(params) {
  return request({
    url: '/tp/ngbFinance/sendOffer',
    method: 'post',
    data: params
  })
}



/**
 * 取消offer
 * @param params
 */
export function cancelOffer(params) {
  return request({
    url: '/tp/ngbFinance/cancelOffer',
    method: 'post',
    data: params
  })
}




/**
 * 工人培训
 * @param params
 */
export function workerTrain(params) {
  return request({
    url: '/tp/ngbFinance/worker/train',
    method: 'post',
    data: params
  })
}


/**
 * 开除工人
 * @param params
 */
export function workerDismiss(params) {
  return request({
    url: '/tp/ngbFinance/worker/dismiss',
    method: 'post',
    data: params
  })
}


/**
 * 给工人发工资
 * @param params
 */
export function workerPay(params) {
  return request({
    url: '/tp/ngbFinance/worker/pay',
    method: 'post',
    data: params
  })
}


/**
 * 给工人发工资-一键发薪
 * @param params
 */
export function workerPayBatch(params) {
  return request({
    url: '/tp/ngbFinance/worker/payBatch',
    method: 'get',
    params: params
  })
}




/**
 * 激励工人
 * @param params
 */
export function workerIncentive(params) {
  return request({
    url: '/tp/ngbFinance/worker/incentive',
    method: 'post',
    data: params
  })
}



/**
 * 激励工人-一键激励
 * @param params
 */
export function workerIncentiveBatch(params) {
  return request({
    url: '/tp/ngbFinance/worker/incentiveBatch',
    method: 'post',
    data: params
  })
}


/**
 * 激励工人-一键激励获取所需的预算
 * @param params
 */
export function getIncentiveBatchMoney(params) {
  return request({
    url: '/tp/ngbFinance/worker/incentiveBatchMoney',
    method: 'post',
    data: params
  })
}




/**
 * 查看特征研发进度
 * @param params
 */
export function designProcessList(params) {
  return request({
    url: '/tp/ngbContest/design/progress/list',
    method: 'get',
    params: params
  })
}


/**
 * 特征研发
 * @param params
 */
export function designProgressResearch(params) {
  return request({
    url: '/tp/ngbContest/design/progress/research',
    method: 'post',
    data: params
  })
}



/**
 * 查询产品特性
 * @param params
 */
export function designFeatureList(params) {
  return request({
    url: '/tp/ngbContest/design/feature/list',
    method: 'get',
    params: params
  })
}



/**
 * 添加产品特性
 * @param params
 */
export function designFeatureAdd(params) {
  return request({
    url: '/tp/ngbContest/design/feature/add',
    method: 'post',
    data: params
  })
}



/**
 * 获取对应产品最新的特性
 * @param params
 */
export function getZxFeature(params) {
  return request({
    url: '/tp/ngbContest/feature/getZx',
    method: 'post',
    data: params
  })
}




/**
 * 网络营销-查看网络投放以及会员指数
 * @param params
 */
export function getNetworkInfo(params) {
  return request({
    url: '/tp/ngbTradeFair/network/getInfo',
    method: 'get',
    params: params
  })
}


/**
 * 网络营销-提交网络营销
 * @param params
 */
export function submitNetwork(params) {
  return request({
    url: '/tp/ngbTradeFair/network/submit',
    method: 'post',
    data: params
  })
}


/**
 * 网络营销-查看网络投放以及会员指数
 * @param params
 */
export function getNetworkRetailOrder(params) {
  return request({
    url: '/tp/ngbTradeFair/network/retailOrder',
    method: 'get',
    params: params
  })
}






/**
 * 获取厂房信息
 * @param params
 */
export function getWorkShopList(params) {
  return request({
    url: '/tp/ngbFinance/workshop/info',
    method: 'get',
    params: params
  })
}



/**
 * 获取新建生产线基本信息
 * @param params
 */
export function getNewLineInfo(params) {
  return request({
    url: '/tp/ngbContest/line/info',
    method: 'get',
    params: params
  })
}

/**
 * 确认新建生产线
 * @param params
 */
export function confirmNewLineInfo(params) {
  return request({
    url: '/tp/ngbContest/line/add',
    method: 'post',
    data: params
  })
}



/**
 * 更新产线的BOM
 * @param params
 */
export function updateLineBOM(params) {
  return request({
    url: '/tp/ngbContest/line/updateBOM',
    method: 'post',
    data: params
  })
}



/**
 * 学生获取竞赛模拟列表-
 * @param params
 */
export function getContestListByStudent(params) {
  return request({
    url: "/tp/ngbContest/list/student",
    method: 'post',
    data: params
  })
}

/**
 * 开始经营
 * @param params
 */
export function startContestByStudent(params) {
  return request({
    url: '/tp/ngbBusiness/start',
    method: 'get',
    params: params
  })
}

/**
 * 获取当前竞赛进度
 * @param params
 */
export function getContestProcess(params) {
  return request({
    url: '/tp/ngbBusiness/progress/get',
    method: 'get',
    params: params
  })
}

/**
 * 保存经营进度
 * @param params
 */
export function saveContestProcess(params) {
  return request({
    url: '/tp/ngbBusiness/progress/save',
    method: 'get',
    params: params
  })
}


/**
 * 学生点击继续按钮
 * @param params
 */
export function nextStatus(params) {
  return request({
    url: '/tp/ngbBusiness/nextStatus',
    method: 'get',
    params: params
  })
}



/**
 * 当年结束开始下一个季度
 * @param params
 */
export function nextCurrentSeason(params) {
  return request({
    url: "/tp/ngbYearEnd/nextSeason",
    method: 'get',
    params: params
  })
}


/**
 * 结束订货会
 * @param params
 */
export function endOrderMeeting(params) {
  return request({
    url: '/tp/ngbTradeFair/end',
    method: 'get',
    params: params
  })
}




/**
 * 获取费用管理数据
 * @param params
 */
export function getFeeManageData(params) {
  return request({
    url: "/tp/ngbFinance/feeManageData/info",
    method: 'get',
    params: params
  })
}



/**
 * 缴纳费用
 * @param params
 */
export function payFee(params) {
  return request({
    url: "/tp/ngbFinance/payFee",
    method: 'get',
    params: params
  })
}


/**
 * 缴纳费用-批量缴费
 * @param params
 */
export function batchPayFee(params) {
  return request({
    url: "/tp/ngbFinance/batchPayFee",
    method: 'get',
    params: params
  })
}





/**
 * 获取生产线列表 - 包含空闲、转产、在产、在建 各个状态的生产线
 * @param params
 */
export function getLineList(params) {
  return request({
    url: '/tp/ngbContest/line/listAll',
    method: 'get',
    params: params
  })
}


/**
 * 工人管理-设备管理保存生产线配置
 * @param params
 */
export function saveLineConfig(params) {
  return request({
    url: '/tp/ngbContest/line/save',
    method: 'post',
    data: params
  })
}



/**
 * 获取已招聘的-工人列表
 * @param params
 */
export function getWorkerList(params) {
  return request({
    url: '/tp/ngbFinance/worker/onTheJob/list',
    method: 'get',
    params: params
  })
}




/**
 * 转产生产线-确认
 * @param params
 */
export function changeLineProduce(params) {
  return request({
    url: '/tp/ngbContest/line/transfer',
    method: 'post',
    params: params
  })
}



/**
 * 出售生产线
 * @param params
 */
export function sellLint(params) {
  return request({
    url: '/tp/ngbContest/line/sell',
    method: 'post',
    data: params
  })
}



/**
 * 生产线开产
 * @param params
 */
export function startNextProducing(params) {
  return request({
    url: '/tp/ngbContest/line/producting',
    method: 'post',
    data: params
  })
}



/**
 * 应收账款收款-确认
 * @param params
 */
export function confirmAccountReceivables(params) {
  return request({
    url: '/tp/ngbContest/receivables/confirm',
    method: 'get',
    params: params
  })
}




/**
 * 应收账款收款-批量收款
 * @param params
 */
export function batchConfirmAccountReceivables(params) {
  return request({
    url: '/tp/ngbContest/receivables/batchConfirm',
    method: 'get',
    params: params
  })
}




/**
 * 应付账款付款-确认
 * @param params
 */
export function confirmPay(params) {
  return request({
    url: '/tp/ngbContest/pay/confirm',
    method: 'get',
    params: params
  })
}



/**
 * 应付账款付款-批量付款
 * @param params
 */
export function batchConfirmPay(params) {
  return request({
    url: '/tp/ngbContest/pay/batchConfirm',
    method: 'get',
    params: params
  })
}







/**
 * 获取交货订单列表
 * @param params
 */
export function getDeliveryOrderList(params) {
  return request({
    url: '/tp/ngbContest/order/list',
    method: 'get',
    params: params
  })
}

/**
 * 确认交货订单
 * @param params
 */
export function confirmDeliveryOrder(params) {
  return request({
    url: '/tp/ngbContest/order/delivery',
    method: 'get',
    params: params
  })
}





/**
 * 产品研发-列表
 * @param params
 */
export function getProductDevelopmentList(params) {
  return request({
    url: '/tp/ngbContest/product/yfList',
    method: 'get',
    params: params
  })
}

/**
 * 产品研发-确认
 * @param params
 */
export function confirmProductDevelopment(params) {
  return request({
    url: '/tp/ngbContest/product/yf',
    method: 'post',
    data: params
  })
}

/**
 * 当季结束
 * @param params
 */
export function endCurrentSeason(params) {
  return request({
    url: '/tp/ngbContest/endingSeason',
    method: 'get',
    params: params
  })
}

/**
 * 结束当年
 * @param params
 */
export function endCurrentYear(params) {
  return request({
    url: '/tp/ngbYearEnd/confirm',
    method: 'get',
    params: params
  })
}

/**
 * 获取间谍附件
 * @param params
 */
export function getSpy(params) {
  return request({
    url: '/tp/ngbContest/attachment/spy/get',
    method: 'get',
    params: params
  })
}

/**
 * 获取规则说明附件
 * @param params
 */
export function getRuleIntroduce(params) {
  return request({
    url: '/tp/ngbContest/attachment/rule/get',
    method: 'get',
    params: params
  })
}

/**
 * 获取规则说明附件
 * @param params
 */
export function getRuleIntroduceBySubjectNumber(params) {
  return request({
    url: '/tp/ngbContest/attachment/rule/getBySubjectNumber',
    method: 'get',
    params: params
  })
}



/**
 * 确认紧急采购材料
 * @param params
 */
export function confirmRushOrderMaterial(params) {
  return request({
    url: '/tp/ngbContest/urgent/material',
    method: 'post',
    data: params
  })
}

/**
 * 确认紧急采购产成品
 * @param params
 */
export function confirmRushOrderProduct(params) {
  return request({
    url: '/tp/ngbContest/urgent/product',
    method: 'post',
    data: params
  })
}


/**
 * 获取社会责任得分
 * @param params
 */
export function getSocialResponsibilityScore(params) {
  return request({
    url: '/tp/ngbContest/social/score',
    method: 'get',
    params: params
  })
}



/**
 * 获取社会责任得分明细
 * @param params
 */
export function getSocialResponsibilityScoreDetail(params) {
  return request({
    url: '/tp/ngbContest/social/scoreDetail',
    method: 'get',
    params: params
  })
}



/**
 * 获取碳排放信息
 * @param params
 */
export function getCarbonInfo(params) {
  return request({
    url: '/tp/ngbContest/carbon/info',
    method: 'get',
    params: params
  })
}



/**
 * 碳排放 - 碳中和
 * @param params
 */
export function carbonNeutraliza(params) {
  return request({
    url: '/tp/ngbContest/carbon/neutraliza',
    method: 'post',
    data: params
  })
}




/**
 * 获取贴现列表
 * @param params
 */
export function getDiscountList(params) {
  return request({
    url: '/tp/ngbContest/show/fee',
    method: 'get',
    params: params
  })
}



/**
 * 获取应付款列表
 * @param params
 */
export function getPayList(params) {
  return request({
    url: '/tp/ngbContest/show/pay',
    method: 'get',
    params: params
  })
}



/**
 * 确认贴现
 * @param params
 */
export function confirmDiscount(params) {
  return request({
    url: '/tp/ngbContest/discount',
    method: 'post',
    data: params
  })
}



/**
 * 出售库存-出售原料
 * @param params
 */
export function sellMaterial(params) {
  return request({
    url: '/tp/ngbContest/sell/material',
    method: 'post',
    data: params
  })
}

/**
 * 出售库存-出售产品
 * @param params
 */
export function sellProduct(params) {
  return request({
    url: '/tp/ngbContest/sell/product',
    method: 'post',
    data: params
  })
}





/**
 * 还原本年数据到第一季度
 * @param params
 */
export function restoreCurrentYearStart(params) {
  return request({
    url: '/tp/ngbContest/data/restore',
    method: 'get',
    params: params
  })
}

/**
 * 还原当季
 * @param params
 */
export function restoreCurrentSeasonStart(params) {
  return request({
    url: '/tp/ngbContest/data/restore/restoreSeason',
    method: 'get',
    params: params
  })
}




/**
 * 获取ISO列表
 * @param params
 */
export function getISOInvestList(params) {
  return request({
    url: '/tp/ngbContest/isoInvest/list',
    method: 'get',
    params: params
  })
}

/**
 * 确认ISO投资
 * @param params
 */
export function confirmISOInvest(params) {
  return request({
    url: '/tp/ngbContest/isoInvest/commit',
    method: "post",
    data: params
  })
}

/**
 * 获取市场开拓列表
 * @param params
 */
export function getMarketExploreList(params) {
  return request({
    url: '/tp/ngbContest/market/list',
    method: 'get',
    params: params
  })
}

/**
 * 确认市场开拓
 * @param params
 */
export function confirmMarketExplore(params) {
  return request({
    url: '/tp/ngbContest/market/yf',
    method: 'post',
    data: params
  })
}


/**
 * 填写四张报表
 * @param params
 */
export function fillReport(params) {
  return request({
    url: "/tp/ngbFillReport/batch/fill",
    method: 'post',
    data: params
  })
}



/**
 * 获取之前填写的内容
 * @param params
 */
export function getTempFill(params) {
  return request({
    url: '/tp/ngbFillReport/getTemp',
    method: 'get',
    params: params
  })
}

/**
 * 获取系统自动生成的报表数据
 * @param params
 */
export function getAutoFill(params) {
  return request({
    url: '/tp/ngbFillReport/getAutoFill',
    method: 'get',
    params: params
  })
}



/**
 * 查询本年填写的PDCA数据
 * @param params
 */
export function getPdca(params) {
  return request({
    url: '/tp/ngbContest/pdca/get',
    method: 'post',
    data: params
  })
}


/**
 * 提交本年的PDCA
 * @param params
 */
export function submitPdca(params) {
  return request({
    url: '/tp/ngbContest/pdca/submit',
    method: 'post',
    data: params
  })
}




/**
 * 获取已开拓市场列表
 * @param params
 */
export function getDevelopedMarketList(params) {
  return request({
    url: '/tp/ngbDeliverAds/market/yf/finish',
    method: 'get',
    params: params
  })
}

/**
 * 填写广告报表
 * @param params
 */
export function fillAdForm(params) {
  return request({
    url: '/tp/ngbDeliverAds/mnads/deliver',
    method: 'post',
    data: params
  })
}

/**
 * 获取已填写的广告
 * @param params
 */
export function getFillAd(params) {
  return request({
    url: '/tp/ngbDeliverAds/mnads/getFillAd',
    method: 'get',
    params: params
  })
}




/**
 * 获取用户和订单
 * @param params
 */
export function getUserAndOrderList(params) {
  return request({
    url: '/tp/ngbTradeFair/getUserAndOrder',
    method: 'get',
    params: params
  })
}



/**
 * 获取当前季度各市场的订单
 * @param params
 */
export function getMarketOrderList(params) {
  return request({
    url: '/tp/ngbTradeFair/getMarketOrderList',
    method: 'get',
    params: params
  })
}


/**
 * 获取已申报的订单
 * @param params
 */
export function getApplyOrderList(params) {
  return request({
    url: '/tp/ngbTradeFair/getApplyOrderList',
    method: 'get',
    params: params
  })
}


/**
 * 查询本季度所有已分配的订单
 * @param params
 */
export function getAssignedOrderList(params) {
  return request({
    url: '/tp/ngbTradeFair/getAssignedOrderList',
    method: 'post',
    data: params
  })
}


/**
 * 申请订单
 * @param params
 */
export function applyOrder(params) {
  return request({
    url: '/tp/ngbTradeFair/applyOrder',
    method: 'post',
    data: params
  })
}



/**
 * 结束当年经营
 * @param params
 */
export function endContestByStudent(params) {
  return request({
    url: '/tp/ngbBusiness/end',
    method: 'get',
    params: params
  })
}

/**
 * 下载学生结果
 * @param params
 */
export function downloadContestResult(params) {
  return request({
    url: '/tp/ngbContest/download/reslut/download',
    method: 'get',
    params: params
  })
}



/**
 * 保存选单进度
 * @param params
 */
export function saveOrderMeetingStatus(params) {
  return request({
    url: "/tp/ngbBusiness/xzorder/save",
    method: 'post',
    data: params
  })
}

/**
 * 确定学生竞赛成绩
 * @param params
 */
export function confirmStudentContestGrade(params) {
  return request({
    url: '/tp/ngbContest/student/score',
    method: 'get',
    params: params
  })
}

/**
 * 学生重新开始竞赛
 * @param params
 */
export function restartContestByStudent(params) {
  return request({
    url: '/tp/ngbBusiness/restart',
    method: 'get',
    params: params
  })
}

/**
 * 重新选单
 * @param params
 */
export function restoreOrder(params) {
  return request({
    url: '/tp/ngbTradeFair/order/restore',
    method: 'get',
    params: params
  })
}


/**
 * 暂停-继续选单
 * @param params
 */
export function pauseOrContinueOrder(params) {
  return request({
    url: '/tp/ngbTradeFair/pause/start/trade',
    method: 'get',
    params: params
  })
}



/**
 * 放弃选单
 * @param params
 */
export function giveUpOrder(params) {
  return request({
    url: '/tp/ngbTradeFair/order/give/up',
    method: 'get',
    params: params
  })
}





/**
 * 获取规则表数据
 * @param params
 */
export function getCompetitonRule(params) {
  return request({
    url: '/tp/ngbGz/getRuleBySubjectNumber',
    method: 'get',
    params: params
  })
}



/**
 * 获取市场预测表
 * @param params
 */
export function getMarketForecast(params) {
  return request({
    url: '/tp/ngbContest/marketForecast',
    method: 'get',
    params: params
  })
}




/**
 * 获取企业经营排名
 * @param params
 */
export function getOperateRanking(params) {
  return request({
    url: '/tp/ngbContest/operateRanking',
    method: 'get',
    params: params
  })
}


/**
 * 查看成绩
 * @param params
 */
export function getViewScore(params) {
  return request({
    url: '/tp/ngbContest/viewScore',
    method: 'get',
    params: params
  })
}




/**
 * 获取数字化研发进度
 * @param params
 */
export function getNumYfResult(params) {
  return request({
    url: '/tp/ngbContest/num/list',
    method: 'get',
    params: params
  })
}


/**
 * 数字化研发
 * @param params
 */
export function numYfCommit(params) {
  return request({
    url: '/tp/ngbContest/num/commit',
    method: 'post',
    data: params
  })
}


/**
 * 开启智能生产
 * @param params
 */
export function startProduct(params) {
  return request({
    url: '/tp/ngbContest/start/digitalize',
    method: 'post',
    data: params
  })
}



/**
 * 获取公告信息
 * @param params
 */
export function getSystemNotice(params) {
  return request({
    url: '/tp/ngbContest/get/announcement/info',
    method: 'get',
    params: params
  })
}



/**
 * 下发系统公告
 * @param params
 */
export function sendSystemNotice(params) {
  return request({
    url: '/tp/ngbContest/send/announcement/info',
    method: 'post',
    data: params
  })
}


/**
 * 获取该年各组报表填写情况
 * @param params
 */
export function getFillReportResult(params) {
  return request({
    url: '/tp/ngbFillReport/fillReport/result',
    method: 'get',
    params: params
  })
}


/**
 * 查询公共信息
 * @param params
 */
export function getPublicInformation(params) {
  return request({
    url: '/tp/ngbContest/get/public/info',
    method: 'get',
    params: params
  })
}



/**
 * 获取对应季度所有人的广告投放信息
 * @param params
 */
export function getSeasonAllAd(params) {
  return request({
    url: '/tp/ngbTradeFair/ad/list',
    method: 'get',
    params: params
  })
}


/**
 * 大数据看板-获取各小组广告投放累计
 * @param params
 */
export function getAllAdTotal(params) {
  return request({
    url: '/tp/ngbContest/bigData/allAdTotal',
    method: 'get',
    params: params
  })
}


/**
 * 大数据看板-获取各小组本季度的平均工资
 * @param params
 */
export function getAverageSal(params) {
  return request({
    url: '/tp/ngbContest/bigData/averageSal',
    method: 'get',
    params: params
  })
}


/**
 * 大数据看板-获取各小组的产线数量
 * @param params
 */
export function getLineNum(params) {
  return request({
    url: '/tp/ngbContest/bigData/lineNum',
    method: 'get',
    params: params
  })
}


/**
 * 大数据看板-获取各小组的累计销售额
 * @param params
 */
export function getSumSaleAmount(params) {
  return request({
    url: '/tp/ngbContest/bigData/sumSaleAmount',
    method: 'get',
    params: params
  })
}





/**
 * 更新订货会状态
 * @param params
 */
export function updateFairStatus(params) {
  return request({
    url: '/tp/ngbTradeFair/start/competeOrder',
    method: 'get',
    params: params
  })
}


/**
 * 更新订货会状态
 * @param params
 */
export function reAssignOrder(params) {
  return request({
    url: '/tp/ngbTradeFair/reAssignOrder',
    method: 'get',
    params: params
  })
}



/**
 * 更新订货会状态
 * @param params
 */
export function getOrderDate(params) {
  return request({
    url: '/tp/ngbTradeFair/getOrderDate',
    method: 'get',
    params: params
  })
}



/**
 * 获取本季度的预算
 * @param params
 */
export function getBudget(params) {
  return request({
    url: '/tp/ngbContest/get/budget',
    method: 'get',
    params: params
  })
}


/**
 * 提交本季度的预算
 * @param params
 */
export function submitBudget(params) {
  return request({
    url: '/tp/ngbContest/submit/budget',
    method: 'post',
    data: params
  })
}

/**
 * 预算申请
 * @param params
 */
export function applyBudget(params) {
  return request({
    url: '/tp/ngbContest/apply/budget',
    method: 'post',
    data: params
  })
}

/**
 * 预算审批
 * @param params
 */
export function approveBudget(params) {
  return request({
    url: '/tp/ngbContest/approve/budget',
    method: 'post',
    data: params
  })
}


/**
 * 获取本季度的预算审批记录
 * @param params
 */
export function getBudgetApplyList(params) {
  return request({
    url: '/tp/ngbContest/getBudgetApplyList',
    method: 'post',
    data: params
  })
}





/**
 * 数据咨询-情报购买
 * @param params
 */
export function buyDataConsult(params) {
  return request({
    url: '/tp/ngbContest/buy/dataConsult',
    method: 'post',
    data: params
  })
}


/**
 * 数据咨询-获取购买情报了的用户
 * @param params
 */
export function getDataConsultList(params) {
  return request({
    url: '/tp/ngbContest/get/dataConsultList',
    method: 'post',
    data: params
  })
}



/**
 * 数据咨询-获取情报
 * @param {*} params
 */
export function getDataConsultInfo (params) {
  return request({
    url: '/tp/ngbContest/get/dataConsultInfo',
    method: 'post',
    data: params
  })
}
