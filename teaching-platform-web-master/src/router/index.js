import Vue from 'vue'
import Router from 'vue-router'
import store from '@/store'

Vue.use(Router)

/* Layout */
import Layout from '@/layout'

/**
 * Note: sub-menu only appear when route children.length >= 1
 * Detail see: https://panjiachen.github.io/vue-element-admin-site/guide/essentials/router-and-nav.html
 *
 * hidden: true                   if set true, item will not show in the sidebar(default is false)
 * alwaysShow: true               if set true, will always show the root menu
 *                                if not set alwaysShow, when item has more than one children route,
 *                                it will becomes nested mode, otherwise not show the root menu
 * redirect: noRedirect           if set noRedirect will no redirect in the breadcrumb
 * name:'router-name'             the name is used by <keep-alive> (must set!!!)
 * meta : {
    roles: ['admin','teacher']    control the page roles (you can set multiple roles)
    title: 'title'               the name show in sidebar and breadcrumb (recommend set)
    icon: 'svg-name'             the icon show in the sidebar
    breadcrumb: false            if set false, the item will hidden in breadcrumb(default is true)
    activeMenu: '/example/list'  if set path, the sidebar will highlight the path you set
  }
 */

/**
 * constantRoutes
 * a base page that does not have permission requirements
 * all roles can be accessed
 */
export const constantRoutes = [
  {
    path: '/',
    component: () => import('@/views/login/login'),
    hidden: true
  },
  {
    path: '/redirect',
    component: () => import('@/views/redirect/index')
  },
  {
    path: '/404',
    component: () => import('@/views/404'),
    hidden: true
  },
  {
    path: '/403',
    component: () => import('@/views/403'),
    hidden: true
  }
  // 添加权限后删除

  // {
  //   path: '/example',
  //   component: Layout,
  //   redirect: '/example/table',
  //   name: 'Example',
  //   meta: { title: 'Example', icon: 'example', roles: ['teacher'] },
  //   children: [
  //     {
  //       path: 'table',
  //       name: 'Table',
  //       component: () => import('@/views/table/index'),
  //       meta: { title: 'Table', icon: 'table', roles: ['teacher'] }
  //     },
  //     {
  //       path: 'tree',
  //       name: 'Tree',
  //       component: () => import('@/views/tree/index'),
  //       meta: { title: 'Tree', icon: 'tree', roles: ['teacher'] }
  //     }
  //   ]
  // },

]

/**
 * asyncRoutes
 * the routes that need to be dynamically loaded based on user roles
 */
export const asyncRoutes = [
  {
    path: '/admin',
    component: Layout,
    redirect: '/admin/account',
    meta: { title: '管理员', icon: 'admin', roles: ['admin'] },
    children: [{
      path: 'personal',
      name: 'adminPersonal',
      component: () => import('@/views/admin/personal/index'),
      meta: { title: '个人资料管理', icon: 'teacher', roles: ['admin'] }
    }, {
      path: 'account',
      name: 'teacherAccountManagement',
      component: () => import('@/views/admin/account/index'),
      meta: { title: '教师账号管理', icon: 'teacher', roles: ['admin'] }
    },
    {
      path: 'permission',
      name: 'teacherPermissionManagement',
      component: () => import('@/views/admin/permission/index'),
      meta: { title: '权限管理', icon: 'permission', roles: ['admin'] }
    },
    {
      path: 'problem',
      component: () => import('@/views/container'),
      meta: { title: '题库管理', icon: 'problem', roles: ['admin'] },
      children: [{
        path: '',
        name: 'problemLibraryManagement',
        hidden: true,
        component: () => import('@/views/admin/problem/index'),
        meta: { title: '', icon: 'study', roles: ['admin'] }
      }, {
        path: 'upload',
        name: 'problemLibraryUpload',
        hidden: true,
        component: () => import('@/views/admin/problem/upload'),
        meta: { title: '试题上传', icon: 'study', roles: ['admin'], activeMenu: '/admin/problem'}
      }]

    },
    {
      path: 'course',
      name: 'courseLibraryManagement',
      component: () => import('@/views/admin/course/index'),
      meta: { title: '课程管理', icon: 'study', roles: ['admin'] }
    },
    // {
    //   path: 'contest',
    //   component: () => import('@/views/container'),
    //   meta: { title: '创业者', icon: 'contest', roles: ['admin'] },
    //   children: [{
    //     path: '',
    //     name: 'contestManagementAdmin',
    //     hidden: true,
    //     component: () => import('@/views/admin/contest/index'),
    //     meta: { title: '', icon: 'contest', roles: ['admin'], activeMenu: '/admin/contest' },
    //   }, {
    //     path: 'upload/:subjectNumber',
    //     name: 'contestGzUpload',
    //     hidden: true,
    //     component: () => import('@/views/admin/contest/upload'),
    //     meta: { title: '规则表上传', icon: 'contest', roles: ['admin'], activeMenu: '/admin/contest' }
    //   }]
    //  },
    // {
    //   path: 'newContest',
    //   component: () => import('@/views/container'),
    //   meta: { title: '人机对抗', icon: 'contest', roles: ['admin'] },
    //   children: [{
    //     path: '',
    //     name: 'newContestManagementAdmin',
    //     hidden: true,
    //     component: () => import('@/views/admin/newContest/index'),
    //     meta: { title: '', icon: 'contest', roles: ['admin'], activeMenu: '/admin/newContest' },
    //   }, {
    //     path: 'upload/:subjectNumber',
    //     name: 'newContestGzUpload',
    //     hidden: true,
    //     component: () => import('@/views/admin/newContest/upload'),
    //     meta: { title: '规则表上传', icon: 'contest', roles: ['admin'], activeMenu: '/admin/newContest' }
    //   }]
    // },
    /*组间对抗数据导出*/
    // {
    //   path: 'gbContest',
    //   name: 'gbContestExport',
    //   component: () => import('@/views/admin/gbContest/index'),
    //   meta: { title: '组间对抗', icon: 'contest', roles: ['admin'] }
    // }
    ,
    /*组间对抗数据导出*/
    // {
    //   path: 'ngbContest',
    //   name: 'ngbContestExport',
    //   component: () => import('@/views/admin/ngbContest/index'),
    //   meta: { title: '组间对抗(新)', icon: 'contest', roles: ['admin'] }
    // }
    // ,
    {
      path: 'ngbSubject',
      component: () => import('@/views/container'),
      meta: { title: '竞赛题库', icon: 'contest', roles: ['admin'] },
      children: [{
        path: '',
        name: 'ngbSubjectManagement',
        hidden: true,
        component: () => import('@/views/teacher/ngbSubject/index'),
        meta: { title: '', icon: 'contest', roles: ['admin'], activeMenu: '/teacher/ngbSubject' },
      }, {
        path: 'upload/:subjectNumber',
        name: 'ngbSubjectGzUpload',
        hidden: true,
        component: () => import('@/views/teacher/ngbSubject/upload'),
        meta: { title: '规则表上传', icon: 'contest', roles: ['admin'], activeMenu: '/teacher/ngbSubject' }
      }]
    },
    {
      path: 'group',
      name: 'groupManagement',
      component: () => import('@/views/admin/group/index'),
      meta: { title: '分组管理', icon: 'group', roles: ['admin'] }
    },
    {
      path: 'resource',
      name: 'resourceManagement',
      component: () => import('@/views/admin/resource/index'),
      meta: { title: '资源管理', icon: 'resource', roles: ['admin'] }
    },
    ]
  },
  {
    path: '/teacher',
    component: Layout,
    redirect: '/teacher/account',
    meta: { title: '教师中心', icon: 'teacher', roles: ['teacher'] },
    children: [
      {
        path: 'account',
        component: () => import('@/views/container'),
        meta: { title: '学生账号管理', icon: 'user', roles: ['teacher'] },
        children: [{
          path: '',
          name: 'studentAccount',
          component: () => import('@/views/teacher/account/index'),
          hidden: true,
          meta: { title: '', icon: 'user', roles: ['teacher'], activeMenu: '/teacher/account' }
        }, {
          path: 'upload',
          name: 'studentUpload',
          hidden: true,
          component: () => import('@/views/teacher/account/upload'),
          meta: { title: '上传学生', roles: ['teacher'], activeMenu: '/teacher/account' }
        }]
      },
      {
        path: 'personal',
        name: 'teacherPersonal',
        component: () => import('@/views/teacher/personal/index'),
        meta: { title: '个人资料管理', icon: 'information', roles: ['teacher'] }
      },
      {
        path: 'study',
        name: 'study',
        component: () => import('@/views/teacher/study/index'),
        meta: { title: '学习中心', icon: 'study', roles: ['teacher'] }
      },
      {
        path: 'exam',
        redirect: '/teacher/exam/management',
        component: () => import('@/views/container'),
        meta: { title: '考试中心', icon: 'exam', roles: ['teacher'] },
        children: [{
          path: 'management',
          component: () => import('@/views/container'),
          hidden: true,
          meta: { title: '试卷管理', icon: 'teacher', roles: ['teacher'] },
          children: [{
            path: '',
            name: 'examPaperManagement',
            hidden: true,
            component: () => import('@/views/teacher/exam/index'),
            meta: { title: '', icon: 'teacher', roles: ['teacher'], activeMenu: '/teacher/exam' }
          }, {
            path: 'add',
            name: 'addExamPaper',
            hidden: true,
            component: () => import('@/views/teacher/exam/addOrEdit/index'),
            meta: { title: '创建试卷', icon: 'teacher', roles: ['teacher'], activeMenu: '/teacher/exam' }
          }, {
            path: 'edit/:paperId',
            name: 'editExamPaper',
            hidden: true,
            component: () => import('@/views/teacher/exam/addOrEdit/index'),
            meta: { title: '编辑试卷', icon: 'teacher', roles: ['teacher'], activeMenu: '/teacher/exam' }
          }, {
            path: 'preview/:paperId',
            name: 'previewExamPaper',
            hidden: true,
            component: () => import('@/views/teacher/exam/preview/index'),
            meta: { title: '预览试卷', icon: 'teacher', roles: ['teacher'], activeMenu: '/teacher/exam' }
          },
            {
              path: 'score/:paperId',
              component: () => import('@/views/container'),
              hidden: true,
              meta: { title: '成绩管理', icon: 'teacher', roles: ['teacher'] },
              children: [{
                path: '',
                name: 'scoreExamPaper',
                component: () => import('@/views/teacher/exam/score/index'),
                meta: { title: '', icon: 'teacher', roles: ['teacher'], activeMenu: '/teacher/exam' }
              },
                {
                  path: 'check/:examId',
                  name: 'checkExamPaper',
                  hidden: true,
                  component: () => import('@/views/teacher/exam/score/check/index'),
                  meta: { title: '批改试卷', icon: 'teacher', roles: ['teacher'], activeMenu: '/teacher/exam' }
                }, {
                  path: 'view/:examId',
                  name: 'viewExamPaper',
                  hidden: true,
                  component: () => import('@/views/teacher/exam/score/view/index'),
                  meta: { title: '查看试卷', icon: 'teacher', roles: ['teacher'], activeMenu: '/teacher/exam' }
                }
              ]
            }, {
              path: 'analysis/:paperId',
              component: () => import('@/views/container'),
              meta: { title: '试题分析', icon: 'teacher', roles: ['teacher'], activeMenu: '/teacher/exam' },
              children: [
                {
                  path: '',
                  name: 'analysisExamPaper',
                  component: () => import('@/views/teacher/exam/analysis/index'),
                  meta: { title: '', icon: 'teacher', roles: ['teacher'], activeMenu: '/teacher/exam' }
                }, {
                  path: 'problem/:problemId',
                  name: 'viewProblemDetail',
                  component: () => import('@/views/teacher/exam/analysis/components/viewProblemDetail'),
                  meta: { title: '题目详情', icon: 'teacher', roles: ['teacher'], activeMenu: '/teacher/exam' }
                }
              ]
            },
          ]
        }
        ]
      },
      // {
      //   path: 'contest',
      //   component: () => import('@/views/container'),
      //   meta: { title: '创业者', icon: 'contest', roles: ['teacher'] },
      //   children: [{
      //     path: '',
      //     name: 'contestManagementTeacher',
      //     hidden: true,
      //     component: () => import('@/views/teacher/contest/index'),
      //     meta: { title: '', icon: 'contest', roles: ['teacher'], activeMenu: '/teacher/contest' },
      //   },
      //     {
      //       path: 'add',
      //       name: 'addContest',
      //       hidden: true,
      //       component: () => import('@/views/teacher/contest/addOrEdit'),
      //       meta: { title: '添加竞赛', icon: 'contest', roles: ['teacher'], activeMenu: '/teacher/contest' },
      //     },
      //     {
      //       path: 'edit/:contestId',
      //       name: 'editContest',
      //       hidden: true,
      //       component: () => import('@/views/teacher/contest/addOrEdit'),
      //       meta: { title: '编辑竞赛信息', icon: 'contest', roles: ['teacher'], activeMenu: '/teacher/contest' },
      //     }, {
      //       path: 'result/:contestId',
      //       name: 'contestResult',
      //       hidden: true,
      //       component: () => import('@/views/teacher/contest/result'),
      //       meta: { title: '竞赛模拟结果', icon: 'contest', roles: ['teacher'], activeMenu: '/teacher/contest' },
      //     }]
      // },
      // {
      //   path: 'newContest',
      //   component: () => import('@/views/container'),
      //   meta: { title: '人机对抗', icon: 'contest', roles: ['teacher'] },
      //   children: [{
      //     path: '',
      //     name: 'newContestManagementTeacher',
      //     hidden: true,
      //     component: () => import('@/views/teacher/newContest/index'),
      //     meta: { title: '', icon: 'contest', roles: ['teacher'], activeMenu: '/teacher/newContest' },
      //   },
      //     {
      //       path: 'add',
      //       name: 'addNewContest',
      //       hidden: true,
      //       component: () => import('@/views/teacher/newContest/addOrEdit'),
      //       meta: { title: '添加竞赛', icon: 'contest', roles: ['teacher'], activeMenu: '/teacher/newContest' },
      //     },
      //     {
      //       path: 'edit/:contestId',
      //       name: 'editNewContest',
      //       hidden: true,
      //       component: () => import('@/views/teacher/newContest/addOrEdit'),
      //       meta: { title: '编辑竞赛信息', icon: 'contest', roles: ['teacher'], activeMenu: '/teacher/newContest' },
      //     }, {
      //       path: 'result/:contestId',
      //       name: 'newContestResult',
      //       hidden: true,
      //       component: () => import('@/views/teacher/newContest/result'),
      //       meta: { title: '竞赛模拟结果', icon: 'contest', roles: ['teacher'], activeMenu: '/teacher/newContest' },
      //     }]
      // },
      //组间对抗题目
      // {
      //   path: 'gbSubject',
      //   component: () => import('@/views/container'),
      //   meta: { title: '对抗题库', icon: 'contest', roles: ['teacher'] },
      //   children: [{
      //     path: '',
      //     name: 'gbSubjectManagement',
      //     hidden: true,
      //     component: () => import('@/views/teacher/gbSubject/index'),
      //     meta: { title: '', icon: 'contest', roles: ['teacher'], activeMenu: '/teacher/gbSubject' },
      //   }, {
      //     path: 'upload/:subjectNumber',
      //     name: 'gbSubjectGzUpload',
      //     hidden: true,
      //     component: () => import('@/views/teacher/gbSubject/upload'),
      //     meta: { title: '规则表上传', icon: 'contest', roles: ['teacher'], activeMenu: '/teacher/gbSubject' }
      //   }]
      // },
      // {
      //   path: 'gbContest',
      //   component: () => import('@/views/container'),
      //   meta: { title: '组间对抗', icon: 'contest', roles: ['teacher'] },
      //   children: [{
      //     path: '',
      //     name: 'gbContestManagement',
      //     hidden: true,
      //     component: () => import('@/views/teacher/gbContest/index'),
      //     meta: { title: '', icon: 'contest', roles: ['teacher'], activeMenu: '/teacher/gbContest' },
      //   },
      //     {
      //       path: 'add',
      //       name: 'addGbContest',
      //       hidden: true,
      //       component: () => import('@/views/teacher/gbContest/addOrEdit'),
      //       meta: { title: '添加竞赛', icon: 'contest', roles: ['teacher'], activeMenu: '/teacher/gbContest' },
      //     },
      //     {
      //       path: 'edit/:contestId',
      //       name: 'editGbContest',
      //       hidden: true,
      //       component: () => import('@/views/teacher/gbContest/addOrEdit'),
      //       meta: { title: '编辑竞赛信息', icon: 'contest', roles: ['teacher'], activeMenu: '/teacher/gbContest' },
      //     }, {
      //       path: 'result/:contestId',
      //       name: 'gbContestResult',
      //       hidden: true,
      //       component: () => import('@/views/teacher/gbContest/result'),
      //       meta: { title: '竞赛模拟结果', icon: 'contest', roles: ['teacher'], activeMenu: '/teacher/gbContest' },
      //     }]
      // },

      //竞赛
      {
        path: 'ngbSubject',
        component: () => import('@/views/container'),
        meta: { title: '竞赛题库', icon: 'contest', roles: ['teacher'] },
        children: [{
          path: '',
          name: 'ngbSubjectManagement',
          hidden: true,
          component: () => import('@/views/teacher/ngbSubject/index'),
          meta: { title: '', icon: 'contest', roles: ['teacher'], activeMenu: '/teacher/ngbSubject' },
        }, {
          path: 'upload/:subjectNumber',
          name: 'ngbSubjectGzUpload',
          hidden: true,
          component: () => import('@/views/teacher/ngbSubject/upload'),
          meta: { title: '规则表上传', icon: 'contest', roles: ['teacher'], activeMenu: '/teacher/ngbSubject' }
        }]
      },
      {
        path: 'ngbContest',
        component: () => import('@/views/container'),
        meta: { title: '竞赛对抗', icon: 'contest', roles: ['teacher'] },
        children: [{
          path: '',
          name: 'ngbContestManagement',
          hidden: true,
          component: () => import('@/views/teacher/ngbContest/index'),
          meta: { title: '', icon: 'contest', roles: ['teacher'], activeMenu: '/teacher/ngbContest' },
        },
          {
            path: 'add',
            name: 'addNGbContest',
            hidden: true,
            component: () => import('@/views/teacher/ngbContest/addOrEdit'),
            meta: { title: '添加竞赛', icon: 'contest', roles: ['teacher'], activeMenu: '/teacher/ngbContest' },
          },
          {
            path: 'edit/:contestId',
            name: 'editNGbContest',
            hidden: true,
            component: () => import('@/views/teacher/ngbContest/addOrEdit'),
            meta: { title: '编辑竞赛信息', icon: 'contest', roles: ['teacher'], activeMenu: '/teacher/ngbContest' },
          }, {
            path: 'result/:contestId',
            name: 'ngbContestResult',
            hidden: true,
            component: () => import('@/views/teacher/ngbContest/result'),
            meta: { title: '竞赛模拟结果', icon: 'contest', roles: ['teacher'], activeMenu: '/teacher/ngbContest' },
          }]
      },
      ]
  },
  {
    path: '/student',
    component: Layout,
    redirect: '/student/personal',
    meta: { title: '学生中心', icon: 'user', roles: ['student'] },
    children: [
      {
        path: 'personal',
        name: 'studentPersonal',
        component: () => import('@/views/student/personal/index'),
        meta: { title: '个人资料管理', icon: 'information', roles: ['student'] }
      }, {
        path: 'study',
        name: 'study',
        component: () => import('@/views/student/study/index'),
        meta: { title: '学习中心', icon: 'study', roles: ['student'] }
      }, {
        path: 'exam',
        component: () => import('@/views/container'),
        meta: { title: '考试中心', icon: 'exam', roles: ['student'] },
        children: [{
          path: '',
          name: 'examStudent',
          hidden: true,
          component: () => import('@/views/student/exam/index'),
          meta: { title: '', icon: 'dashboard', roles: ['student'], activeMenu: '/student/exam' }
        }, {
          path: 'test/:examId',
          hidden: true,
          name: 'studentTest',
          component: () => import('@/views/student/exam/exam'),
          meta: { title: '开始考试', icon: 'dashboard', roles: ['student'], activeMenu: '/student/exam' }
        }, {
          path: 'view/:examId',
          hidden: true,
          name: 'viewPaperResult',
          component: () => import('@/views/student/exam/view'),
          meta: { title: '考试结果', roles: ['student'], activeMenu: '/student/exam' }

        }]
      },
      // {
      //   path: 'contest',
      //   component: () => import('@/views/container'),
      //   meta: { title: '创业者', icon: 'contest', roles: ['student'], activeMenu: '/student/contest' },
      //   children: [{
      //     path: '',
      //     name: 'contestTable',
      //     hidden: true,
      //     component: () => import('@/views/student/contest/index'),
      //     meta: { title: '', icon: '', roles: ['student'], activeMenu: '/student/contest' }
      //   }, {
      //     path: 'sandTable/:contestId&:subjectNumber',
      //     hidden: true,
      //     name: 'sandTable',
      //     component: () => import('@/views/student/contest/sandTable'),
      //     meta: { title: '开始竞赛', icon: 'dashboard', roles: ['student'], activeMenu: '/student/contest' }
      //   }]
      // },
      // {
      //   path: 'newContest',
      //   component: () => import('@/views/container'),
      //   meta: { title: '人机对抗', icon: 'contest', roles: ['student'], activeMenu: '/student/newContest' },
      //   children: [{
      //     path: '',
      //     name: 'newContestTable',
      //     hidden: true,
      //     component: () => import('@/views/student/newContest/index'),
      //     meta: { title: '', icon: '', roles: ['student'], activeMenu: '/student/newContest' }
      //   }, {
      //     path: 'newSandTable/:contestId&:subjectNumber',
      //     hidden: true,
      //     name: 'newSandTable',
      //     component: () => import('@/views/student/newContest/newSandTable'),
      //     meta: { title: '开始竞赛', icon: 'dashboard', roles: ['student'], activeMenu: '/student/newContest' }
      //   },
      //     //新的前端页面
      //     {
      //       path: 'newSandTableNew',
      //       hidden: true,
      //       name: 'newSandTableNew',
      //       component: () => import('@/views/student/newContest/newSandTableNew'),
      //       meta: { title: '开始竞赛', icon: 'dashboard', roles: ['student'], activeMenu: '/student/newContest' }
      //     }
      //   ],
      // },
      // {
      //   path: 'gbContest',
      //   component: () => import('@/views/container'),
      //   meta: { title: '组间对抗', icon: 'contest', roles: ['student'], activeMenu: '/student/gbContest' },
      //   children: [{
      //     path: '',
      //     name: 'gbContestTable',
      //     hidden: true,
      //     component: () => import('@/views/student/gbContest/index'),
      //     meta: { title: '', icon: '', roles: ['student'], activeMenu: '/student/gbContest' }
      //   }, {
      //     path: 'gbSandTable/:contestId&:subjectNumber',
      //     hidden: true,
      //     name: 'gbSandTable',
      //     component: () => import('@/views/student/gbContest/gbSandTable'),
      //     meta: { title: '开始竞赛', icon: 'dashboard', roles: ['student'], activeMenu: '/student/gbContest' }
      //   },
      //   ],
      // },
      {
        path: 'ngbContest',
        component: () => import('@/views/container'),
        meta: { title: '竞赛对抗', icon: 'contest', roles: ['student'], activeMenu: '/student/ngbContest' },
        children: [{
          path: '',
          name: 'ngbContestTable',
          hidden: true,
          component: () => import('@/views/student/ngbContest/index'),
          meta: { title: '', icon: '', roles: ['student'], activeMenu: '/student/ngbContest' }
        }, {
          path: 'ngbSandTable/:contestId&:subjectNumber',
          hidden: true,
          name: 'ngbSandTable',
          component: () => import('@/views/student/ngbContest/ngbSandTable'),
          meta: { title: '开始竞赛', icon: 'dashboard', roles: ['student'], activeMenu: '/student/ngbContest' }
        },
        ],
      }

      ]
  },
  { path: '*', redirect: '/404', hidden: true }
]

const createRouter = () => new Router({
  // mode: 'history', // require service support
  scrollBehavior: () => ({ y: 0 }),
  routes: constantRoutes
})

const router = createRouter()

// Detail see: https://github.com/vuejs/vue-router/issues/1234#issuecomment-357941465
export function resetRouter () {
  const newRouter = createRouter()
  router.matcher = newRouter.matcher // reset router
}

export default router
