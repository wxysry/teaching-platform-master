import Cookies from 'js-cookie'

const TokenKey = 'token'

export function getToken () {
  return window.sessionStorage.getItem(TokenKey)
}

export function setToken (token) {
  //console.log(token)
  Cookies.set(TokenKey, token)
  return window.sessionStorage.setItem(TokenKey, token)

}

export function removeToken () {
  Cookies.remove(TokenKey)
  return window.sessionStorage.removeItem(TokenKey)
}
