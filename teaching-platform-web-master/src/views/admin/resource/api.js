import request from './request'

/**
 * 管理员上传附件
 * @param data
 */
export function uploadAttachment(data) {
  return request({
    url: '/tp/resources/attachment/upload',
    method: 'post',
    data: data
  })
}

/**
 * 管理员上传图片
 * @param data
 */
export function uploadPics(data) {
  return request({
    url: '/tp/resources/pic/upload',
    method: 'post',
    data: data
  })
}

