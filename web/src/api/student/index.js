import request from '@/utils/request'

/**
 * 学生获取考试试卷列表
 * @param params
 */
export function getExamPaperListByStudent(params) {
  return request({
    url: '/tp/exam/paper/list',
    method: 'post',
    data: params
  })
}

/**
 * 学生获取试卷基础信息
 * @param params
 */
export function getExamPaperBasicInfoByStudent(params) {
  return request({
    url: '/tp/exam/get',
    method: 'get',
    params: params
  })
}

/**
 * 学生提交试卷
 * @param params
 */
export function submitExamPaperByStudent(params) {
  return request({
    url: '/tp/exam/handIn',
    method: 'get',
    params: params
  })
}

/**
 * 学生中断考试
 * @param params
 */
export function interruptExamPaperByStudent(params) {
  return request({
    url: '/tp/exam/interrupt',
    method: 'get',
    params: params
  })
}

/**
 * 学生保存答案
 * @param params
 */
export function submitProblemAnswerByStudent(params) {
  return request({
    url: '/tp/examProblem/student/save',
    method: 'post',
    data: params
  })
}

/**
 * 学生中断考试
 * @param params
 */
export function interruptExamByStudent(params) {
  return request({
    url: '/tp/exam/interrupt',
    method: 'get',
    params: params
  })
}

/**
 * 学生开始考试
 * @param params
 */
export function startExamByStudent(params) {
  return request({
    url: '/tp/exam/start',
    method: 'get',
    params: params
  })
}

/**
 * 学生获取某张试卷考试题目
 * @param params
 */
export function getAllExamProblemByStudent(params) {
  return request({
    url: '/tp/examProblem/listByStudent',
    method: 'get',
    params: params
  })
}

