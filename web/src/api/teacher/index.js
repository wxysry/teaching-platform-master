import request from '@/utils/request'

export function getStudentList(params) {
  return request({
    url: '/tp/student/list',
    method: 'post',
    data: params
  })
}

/**
 * 删除学生
 */
export function deleteStudentById(params) {
  const url = `/tp/student/delete/${params}`
  return request({
    url: url,
    method: 'get'
  })
}

/**
 * 老师获取视频课程列表
 * @param params
 */
export function getVideoCourseListByTeacher(params) {
  return request({
    url: '/tp/videoCourse/teacher/list',
    method: 'post',
    data: params
  })
}

/**
 * 老师获取视频分组
 */
export function getVideoGroupByTeacher() {
  return request({
    url: '/tp/videoGroup/teacher/list',
    method: 'get',
  })
}


/**
 * 老师获取ppt列表
 * @param params
 */
export function getPPTCourseListByTeacher(params) {
  return request({
    url: '/tp/pptCourse/teacher/list',
    method: 'post',
    data: params
  })
}

/**
 * 老师获取ppt分组
 */
export function getPPTGroupByTeacher() {
  return request({
    url: '/tp/pptGroup/teacher/list',
    method: 'get'
  })
}

/**
 * 获取教师个人信息
 */
export function getTeacherInfo() {
  return request({
    url: '/tp/sysUser/get',
    method: 'get'
  })
}

/**
 * 教师获取试卷列表
 * @param params
 */
export function getPaperList(params) {
  return request({
    url: '/tp/paper/list',
    method: 'post',
    data: params
  })
}

/**
 * 教师创建试卷
 * @param params
 */
export function addPaperByTeacher(params) {
  return request({
    url: '/tp/paper/addOrUpdate',
    method: 'post',
    data: params
  })
}

/**
 * 教师获取题目分组级联
 */
export function getAllProblemGroupByTeacher() {
  return request({
    url: '/tp/problemGroup/list/all/teacher',
    method: 'get'
  })
}

/**
 * 教师获取题型列表
 */
export function getProblemTypes() {
  return request({
    url: '/tp/problemType/list',
    method: 'get'
  })
}

/**
 * 教师获取能查看的试题
 * @param params
 */
export function getProblemListByTeacher(params) {
  return request({
    url: '/tp/problem/listByTeacher',
    method: 'post',
    data: params
  })
}

/**
 * 教师获取试卷基础信息
 * @param params
 */
export function getPaperBasicInfoByTeacher(params) {
  return request({
    url: '/tp/paper/get',
    method: 'get',
    params: params
  })
}

/**
 * 教师获取试卷试题列表
 * @param params
 */
export function getPaperProblemListByTeacher(params) {
  return request({
    url: '/tp/paper/problem/get',
    method: 'get',
    params: params
  })
}

/**
 * 教师添加试卷试题
 * @param params
 */
export function addPaperProblemsByTeacher(params) {
  return request({
    url: '/tp/paper/problem/add',
    method: 'post',
    data: params
  })
}

/**
 *教师获取全部学生
 */
export function getAllStudentListByTeacher() {
  return request({
    url: '/tp/student/list/all',
    method: 'post'
  })
}

/**
 *教师向试卷添加考生
 * @param params
 */
export function addPaperStudent(params) {
  return request({
    url: '/tp/paper/student/add',
    method: 'post',
    data: params
  })
}

/**
 * 教师获取试卷考生列表
 * @param params
 */
export function getPaperStudentListByTeacher(params) {
  return request({
    url: '/tp/paper/student/get',
    method: 'get',
    params: params
  })
}

/**
 * 教师发布试卷
 * @param params
 */
export function publishPaperByTeacher(params) {
  return request({
    url: '/tp/paper/publish',
    method: 'get',
    params: params
  })
}

/**
 * 教师查看单个试题
 * @param params
 */
export function getProblemByTeacher(params) {
  return request({
    url: '/tp/problem/teacher/get',
    method: 'get',
    params: params
  })
}

/**
 * 教师获取某张试卷参与学生列表
 * @param params
 */
export function getExamStudentListByTeacher(params) {
  return request({
    url: '/tp/exam/student/list',
    method: 'post',
    data: params
  })
}

/**
 * 预览试卷
 * @param params
 */
export function startPreviewExam(params) {
  return request({
    url: '/tp/paper/preview',
    method: 'get',
    params: params
  })
}

/**
 * 教师获取某张试卷学生答案
 * @param params
 */
export function getStudentExamProblemByTeacher(params) {
  return request({
    url: '/tp/examProblem/listByTeacher',
    method: 'get',
    params: params
  })
}

/**
 * 教师批改某张试卷某道题
 * @param params
 */
export function correctProblemByTeacher(params) {
  return request({
    url: '/tp/examProblem/teacher/correct',
    method: 'get',
    params: params
  })
}

/**
 * 教师确认批改试卷
 * @param params
 */
export function confirmCorrectExamPaperByTeacher(params) {
  return request({
    url: '/tp/examProblem/teacher/correct/complete',
    method: 'get',
    params: params
  })
}

/**
 * 教师查看某次考试分析
 * @param params
 */
export function getExamPaperAnalysisByTeacher(params) {
  return request({
    url: '/tp/exam/analysis',
    method: 'get',
    params
  })
}
