import Vue from 'vue'
import Router from 'vue-router'
import store from '@/store'

Vue.use(Router)

/* Layout */
import Layout from '@/layout'

/**
 * Note: sub-menu only appear when route children.length >= 1
 * Detail see: https://panjiachen.github.io/vue-element-admin-site/guide/essentials/router-and-nav.html
 *
 * hidden: true                   if set true, item will not show in the sidebar(default is false)
 * alwaysShow: true               if set true, will always show the root menu
 *                                if not set alwaysShow, when item has more than one children route,
 *                                it will becomes nested mode, otherwise not show the root menu
 * redirect: noRedirect           if set noRedirect will no redirect in the breadcrumb
 * name:'router-name'             the name is used by <keep-alive> (must set!!!)
 * meta : {
    roles: ['admin','teacher']    control the page roles (you can set multiple roles)
    title: 'title'               the name show in sidebar and breadcrumb (recommend set)
    icon: 'svg-name'             the icon show in the sidebar
    breadcrumb: false            if set false, the item will hidden in breadcrumb(default is true)
    activeMenu: '/example/list'  if set path, the sidebar will highlight the path you set
  }
 */

/**
 * constantRoutes
 * a base page that does not have permission requirements
 * all roles can be accessed
 */
export const constantRoutes = [
  {
    path: '/',
    component: () => import('@/views/login/login'),
    hidden: true
  },
  {
    path: '/1',
    component: () => import('@/components/tableTemplate/test_one'),
    hidden: true
  },
  {
    path: '/2',
    component: () => import('@/components/tableTemplate/test_two'),
    hidden: true
  },
  {
    path: '/3',
    component: () => import('@/components/tableTemplate/test_three'),
    hidden: true
  },
  {
    path: '/4',
    component: () => import('@/components/tableTemplate/test_four'),
    hidden: true
  },
  {
    path: '/5',
    component: () => import('@/components/tableTemplate/test_five'),
    hidden: true
  }, {
    path: '/6',
    component: () => import('@/components/tableTemplate/test_six'),
    hidden: true
  }, {
    path: '/7',
    component: () => import('@/components/tableTemplate/test_seven'),
    hidden: true
  },
  {
    path: '/redirect',
    component: () => import('@/views/redirect/index')
  },
  {
    path: '/404',
    component: () => import('@/views/404'),
    hidden: true
  },
  // 添加权限后删除

  // {
  //   path: '/example',
  //   component: Layout,
  //   redirect: '/example/table',
  //   name: 'Example',
  //   meta: { title: 'Example', icon: 'example', roles: ['teacher'] },
  //   children: [
  //     {
  //       path: 'table',
  //       name: 'Table',
  //       component: () => import('@/views/table/index'),
  //       meta: { title: 'Table', icon: 'table', roles: ['teacher'] }
  //     },
  //     {
  //       path: 'tree',
  //       name: 'Tree',
  //       component: () => import('@/views/tree/index'),
  //       meta: { title: 'Tree', icon: 'tree', roles: ['teacher'] }
  //     }
  //   ]
  // },

]

/**
 * asyncRoutes
 * the routes that need to be dynamically loaded based on user roles
 */
export const asyncRoutes = [
  {
    path: '/admin',
    component: Layout,
    redirect: '/admin/account',
    meta: {title: '管理员', icon: 'admin', roles: ['admin']},
    children: [{
      path: 'account',
      name: 'teacherAccountManagement',
      component: () => import('@/views/admin/account/index'),
      meta: {title: '教师账号管理', icon: 'teacher', roles: ['admin']}
    }, {
      path: 'permission',
      name: 'teacherPermissionManagement',
      component: () => import('@/views/admin/permission/index'),
      meta: {title: '权限管理', icon: 'permission', roles: ['admin']}
    }, {
      path: 'problem',
      component: () => import('@/views/container'),
      meta: {title: '题库管理', icon: 'problem', roles: ['admin']},
      children: [{
        path: '',
        name: 'problemLibraryManagement',
        hidden: true,
        component: () => import('@/views/admin/problem/index'),
        meta: {title: '', icon: 'study', roles: ['admin']}
      }, {
        path: 'upload',
        name: 'problemLibraryUpload',
        hidden: true,
        component: () => import('@/views/admin/problem/upload'),
        meta: {title: '试题上传', icon: 'study', roles: ['admin']}
      }]

    }, {
      path: 'course',
      name: 'courseLibraryManagement',
      component: () => import('@/views/admin/course/index'),
      meta: {title: '课程管理', icon: 'study', roles: ['admin']}
    }, {
      path: 'group',
      name: 'groupManagement',
      component: () => import('@/views/admin/group/index'),
      meta: {title: '分组管理', icon: 'group', roles: ['admin']}
    }, {
      path: 'resource',
      name: 'resourceManagement',
      component: () => import('@/views/admin/resource/index'),
      meta: {title: '资源管理', icon: 'resource', roles: ['admin']}
    }]
  },
  {
    path: '/teacher',
    component: Layout,
    redirect: '/teacher/dashboard',
    meta: {title: '教师中心', icon: 'teacher', roles: ['teacher']},
    children: [{
      path: 'dashboard',
      name: 'teacherDashboard',
      component: () => import('@/views/teacher/dashboard/index'),
      meta: {title: '概况', icon: 'teacher', roles: ['teacher']}
    },
      {
        path: 'account',
        component: () => import('@/views/container'),
        meta: {title: '学生账号管理', icon: 'user', roles: ['teacher']},
        children: [{
          path: '',
          name: 'studentAccount',
          component: () => import('@/views/teacher/account/index'),
          hidden: true,
          meta: {title: '', icon: 'user', roles: ['teacher'], activeMenu: '/teacher/account'}
        }, {
          path: 'upload',
          name: 'studentUpload',
          hidden: true,
          component: () => import('@/views/teacher/account/upload'),
          meta: {title: '上传学生', roles: ['teacher'], activeMenu: '/teacher/account'}
        }]
      }, {
        path: 'personal',
        name: 'personal',
        component: () => import('@/views/teacher/personal/index'),
        meta: {title: '个人资料设置', icon: 'information', roles: ['teacher']}
      }, {
        path: 'study',
        name: 'study',
        component: () => import('@/views/teacher/study/index'),
        meta: {title: '学习中心', icon: 'study', roles: ['teacher']}
      }, {
        path: 'exam',
        redirect: '/teacher/exam/management',
        component: () => import('@/views/container'),
        meta: {title: '考试中心', icon: 'exam', roles: ['teacher']},
        children: [{
          path: 'management',
          component: () => import('@/views/container'),
          hidden: true,
          meta: {title: '试卷管理', icon: 'teacher', roles: ['teacher']},
          children: [{
            path: '',
            name: 'examPaperManagement',
            hidden: true,
            component: () => import('@/views/teacher/exam/index'),
            meta: {title: '', icon: 'teacher', roles: ['teacher'], activeMenu: '/teacher/exam'}
          }, {
            path: 'add',
            name: 'addExamPaper',
            hidden: true,
            component: () => import('@/views/teacher/exam/addOrEdit/index'),
            meta: {title: '创建试卷', icon: 'teacher', roles: ['teacher'], activeMenu: '/teacher/exam'}
          }, {
            path: 'edit/:paperId',
            name: 'editExamPaper',
            hidden: true,
            component: () => import('@/views/teacher/exam/addOrEdit/index'),
            meta: {title: '编辑试卷', icon: 'teacher', roles: ['teacher'], activeMenu: '/teacher/exam'}
          }, {
            path: 'preview/:paperId',
            name: 'previewExamPaper',
            hidden: true,
            component: () => import('@/views/teacher/exam/preview/index'),
            meta: {title: '预览试卷', icon: 'teacher', roles: ['teacher'], activeMenu: '/teacher/exam'}
          },
            {
              path: 'score/:paperId',
              component: () => import('@/views/container'),
              hidden: true,
              meta: {title: '成绩管理', icon: 'teacher', roles: ['teacher']},
              children: [{
                path: '',
                name: 'scoreExamPaper',
                component: () => import('@/views/teacher/exam/score/index'),
                meta: {title: '', icon: 'teacher', roles: ['teacher'], activeMenu: '/teacher/exam'}
              },
                {
                  path: 'check/:examId',
                  name: 'checkExamPaper',
                  hidden: true,
                  component: () => import('@/views/teacher/exam/score/check/index'),
                  meta: {title: '批改试卷', icon: 'teacher', roles: ['teacher'], activeMenu: '/teacher/exam'}
                }, {
                  path: 'view/:examId',
                  name: 'viewExamPaper',
                  hidden: true,
                  component: () => import('@/views/teacher/exam/score/view/index'),
                  meta: {title: '查看试卷', icon: 'teacher', roles: ['teacher'], activeMenu: '/teacher/exam'}
                }
              ]
            }, {
              path: 'analysis/:paperId',
              name: 'analysisExamPaper',
              component: () => import('@/views/teacher/exam/analysis/index'),
              meta: {title: '试题分析', icon: 'teacher', roles: ['teacher'], activeMenu: '/teacher/exam'}
            },
          ]
        }
        ]
      }]
  },
  {
    path: '/student',
    component: Layout,
    redirect: '/student/exam',
    meta: {title: '学生中心', icon: 'user', roles: ['student']},
    children: [{
      path: 'exam',
      component: () => import('@/views/container'),
      meta: {title: '考试中心', icon: 'exam', roles: ['student']},
      children: [{
        path: '',
        name: 'examStudent',
        hidden: true,
        component: () => import('@/views/student/exam/index'),
        meta: {title: '', icon: 'dashboard', roles: ['student'], activeMenu: '/student/exam'}
      }, {
        path: 'test/:examId',
        hidden: true,
        name: 'studentTest',
        component: () => import('@/views/student/exam/exam'),
        meta: {title: '开始考试', icon: 'dashboard', roles: ['student'], activeMenu: '/student/exam'}
      }]
    }, {
      path: 'contest',
      component: () => import('@/views/container'),
      meta: {title: '竞赛模拟', icon: 'student', roles: ['student']},
      children: [{
        path: '',
        name: 'contestTable',
        component: () => import('@/views/student/contest/index'),
        meta: {title: '竞赛模拟', icon: 'dashboard', roles: ['student']}
      }, {
        path: 'sandTable',
        hidden: true,
        name: 'sandTable',
        component: () => import('@/views/student/contest/sandTable'),
        meta: {title: '开始竞赛', icon: 'dashboard', roles: ['student'], activeMenu: '/student/contest'}
      }]
    }]
  },

  {path: '*', redirect: '/404', hidden: true}
]

const createRouter = () => new Router({
  // mode: 'history', // require service support
  scrollBehavior: () => ({y: 0}),
  routes: constantRoutes
})

const router = createRouter()

// Detail see: https://github.com/vuejs/vue-router/issues/1234#issuecomment-357941465
export function resetRouter() {
  const newRouter = createRouter()
  router.matcher = newRouter.matcher // reset router
}

export default router
