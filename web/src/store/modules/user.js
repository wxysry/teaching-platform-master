import {logout, getUserInfo} from '@/api/account'
import {getToken, setToken, removeToken} from '@/utils/auth'
import {resetRouter} from '@/router'

const state = {
  token: getToken(),
  name: '',
  roles: [],
  school: '',
}

const mutations = {
  SET_TOKEN: (state, token) => {
    state.token = token
  },
  SET_NAME: (state, name) => {
    state.name = name
  },
  SET_ROLES: (state, roles) => {
    state.roles = roles
  },
  SET_SCHOOL: (state, school) => {
    state.school = school
  }
}

const actions = {
  // user login
  SaveMyToken({commit}, token) {
    return new Promise((resolve, reject) => {
      try {
        commit('SET_TOKEN', token)
        setToken(token)
        resolve()
      } catch (error) {
        reject(error)
      }
    })
  },

  // get user info
  getInfo({commit, state}) {
    return new Promise((resolve, reject) => {
      getUserInfo().then(response => {
        if (response.status === 200) {
          commit('SET_ROLES', response.roles)
          commit('SET_NAME', response.name)
          commit('SET_SCHOOL', response.school)
          resolve(response)
        } else {
          reject('Verification failed, please Login again.')
        }
      }).catch(error => {
        reject(error)
      })
    })
  },

  // user logout
  logout({commit, state}) {
    return new Promise((resolve, reject) => {
      logout().then(() => {
        commit('SET_TOKEN', '')
        commit('SET_ROLES', [])
        removeToken()
        resetRouter()
        resolve()
      }).catch(error => {
        reject(error)
      })
    })
  },

  // remove token
  resetToken({commit}) {
    return new Promise(resolve => {
      commit('SET_TOKEN', '')
      commit('SET_ROLES', [])
      removeToken()
      resolve()
    })
  }
}

export default {
  namespaced: true,
  state,
  mutations,
  actions
}
