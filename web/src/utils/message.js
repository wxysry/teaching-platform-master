import { Message } from 'element-ui'

export function showSuccessInfo(msg) {
  Message({
    message: msg,
    type: 'success'
  })
}

export function showErrorInfo(msg) {
  Message({
    message: msg,
    type: 'error'
  })
}
